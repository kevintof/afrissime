<?php
/* Smarty version 3.1.33, created on 2019-10-31 15:25:19
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\modules\jmspagebuilder\views\templates\hook\addoninstagram.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbafcdf486358_20491465',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '315c4b3d4fd12045ccc2921f9d75fdc8d70774f9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\modules\\jmspagebuilder\\views\\templates\\hook\\addoninstagram.tpl',
      1 => 1572458744,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbafcdf486358_20491465 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
 type="text/javascript">
	var items = <?php if ($_smarty_tpl->tpl_vars['cols']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['cols']->value, ENT_QUOTES, 'UTF-8');
} else { ?>4<?php }?>,
        inst_itemsDesktop = <?php if ($_smarty_tpl->tpl_vars['cols']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['cols']->value, ENT_QUOTES, 'UTF-8');
} else { ?>4<?php }?>,
        inst_itemsDesktopSmall = <?php if ($_smarty_tpl->tpl_vars['cols_md']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['cols_md']->value, ENT_QUOTES, 'UTF-8');
} else { ?>3<?php }?>,
        inst_itemsTablet = <?php if ($_smarty_tpl->tpl_vars['cols_sm']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['cols_sm']->value, ENT_QUOTES, 'UTF-8');
} else { ?>2<?php }?>,
        inst_itemsMobile = <?php if ($_smarty_tpl->tpl_vars['cols_xs']->value) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['cols_xs']->value, ENT_QUOTES, 'UTF-8');
} else { ?>1<?php }?>;
        inst_nav = <?php if ($_smarty_tpl->tpl_vars['navigation']->value == '1') {?>true<?php } else { ?>false<?php }?>;
        inst_pag = <?php if ($_smarty_tpl->tpl_vars['pagination']->value == '1') {?>true<?php } else { ?>false<?php }?>;
        inst_autoplay = <?php if ($_smarty_tpl->tpl_vars['autoplay']->value == '1') {?>true<?php } else { ?>false<?php }?>;
        inst_space = 10;
<?php echo '</script'; ?>
>
<div class="instagram-wrapper">
    <?php if ($_smarty_tpl->tpl_vars['addon_title']->value || $_smarty_tpl->tpl_vars['addon_desc']->value) {?>
        <div class="addon-title custom-title">
            <?php if ($_smarty_tpl->tpl_vars['addon_title']->value) {?>
                <h3>
                    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['addon_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 
                </h3>       
            <?php }?>
            <?php if ($_smarty_tpl->tpl_vars['addon_desc']->value) {?>
                <p class="addon-desc"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['addon_desc']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</p>
            <?php }?>
            <span class="b-title_separator"><span></span></span>
        </div>
    <?php }?>
    <div class="instagram-images">
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insta_images']->value, 'insta');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['insta']->value) {
?>
            <div class="item ">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['insta']->value, 'insta_image');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['insta_image']->value) {
?>
                    <div class="image-box img-zoom">
                        <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['insta_image']->value['link'], ENT_QUOTES, 'UTF-8');?>
">
                            <img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['insta_image']->value['url'], ENT_QUOTES, 'UTF-8');?>
" alt="Instagram Image" />
                        </a>
                    </div>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            </div>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
    </div>
</div>
<?php }
}
