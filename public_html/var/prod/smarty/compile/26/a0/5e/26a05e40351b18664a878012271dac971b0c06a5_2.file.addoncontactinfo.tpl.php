<?php
/* Smarty version 3.1.33, created on 2019-10-31 15:25:31
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\modules\jmspagebuilder\views\templates\hook\addoncontactinfo.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbafceb8df005_85500244',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '26a05e40351b18664a878012271dac971b0c06a5' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\modules\\jmspagebuilder\\views\\templates\\hook\\addoncontactinfo.tpl',
      1 => 1572458744,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbafceb8df005_85500244 (Smarty_Internal_Template $_smarty_tpl) {
?>


<div class="contact-info-wrapper">

	<?php if ($_smarty_tpl->tpl_vars['addon_title']->value) {?>

		<div class="addon-title">

			<h3><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['addon_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</h3>

		</div>

	<?php }?>

	<?php if ($_smarty_tpl->tpl_vars['addon_desc']->value) {?>

		<p class="addon-desc" style="text-align:justify"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['addon_desc']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</p>

	<?php }?>

	<div class="contact-info<?php if ($_smarty_tpl->tpl_vars['box_class']->value) {?> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['box_class']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>">

		<ul>

	      	<?php if ($_smarty_tpl->tpl_vars['ci_address']->value != '') {?>

	       		<li>

	       			<i class="fa fa-map-marker"></i>

	       			<span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['ci_address']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>

	       		</li>

	       	<?php }?>
       			<li>
       				<img src="https://www.afrissime.com/themes/jms_basel/assets/img/icon/boite.png" style="margin-right:4px;"/>
       				<span>07 BP 12 075 Lomé - Togo</span>
       			</li>

	       	<?php if ($_smarty_tpl->tpl_vars['phone']->value != '') {?>

	       		<li>

	       			<i class="fa fa-mobile"></i>

	       			<span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['phone']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>

	       		</li>

	       	<?php }?>

		   <?php if ($_smarty_tpl->tpl_vars['email']->value != '') {?>

		   		<li>

		   			<i class="fa fa-envelope" aria-hidden="true"></i>

		   			<span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['email']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>

		   		</li>

		   	<?php }?>

		   <?php if ($_smarty_tpl->tpl_vars['opentime']->value != '') {?>

		   		<li>

		   			<i class="fa fa-clock-o" aria-hidden="true"></i>

		   			<span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['opentime']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>

		   		</li>

		   	<?php }?>

	    </ul>

	</div>

</div>





	



<?php }
}
