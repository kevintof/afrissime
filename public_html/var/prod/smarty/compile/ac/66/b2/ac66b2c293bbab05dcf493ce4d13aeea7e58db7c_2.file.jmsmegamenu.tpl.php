<?php
/* Smarty version 3.1.33, created on 2019-10-31 15:25:31
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\modules\jmsmegamenu\views\templates\hook\jmsmegamenu.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbafceb03bda3_99645322',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ac66b2c293bbab05dcf493ce4d13aeea7e58db7c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\modules\\jmsmegamenu\\views\\templates\\hook\\jmsmegamenu.tpl',
      1 => 1572458742,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbafceb03bda3_99645322 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="jms-megamenu-wrap">
	<button class="menu-button hidden-lg hidden-md" id="open-button">
		<span class="icon-menu"></span>
	</button>
	<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['menu_html']->value,'' ));?>

</div>
<?php echo '<script'; ?>
 type="text/javascript">
	var jmmm_event = '<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['JMSMM_MOUSEEVENT']->value, ENT_QUOTES, 'UTF-8');?>
';
	var jmmm_duration = <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['JMSMM_DURATION']->value, ENT_QUOTES, 'UTF-8');?>
;	
<?php echo '</script'; ?>
><?php }
}
