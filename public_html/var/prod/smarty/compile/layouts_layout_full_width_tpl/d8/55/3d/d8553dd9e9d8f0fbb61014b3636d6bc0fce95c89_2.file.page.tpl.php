<?php
/* Smarty version 3.1.33, created on 2019-10-31 15:25:28
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbafce8668f17_87643831',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd8553dd9e9d8f0fbb61014b3636d6bc0fce95c89' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\templates\\page.tpl',
      1 => 1572458750,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbafce8668f17_87643831 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
 
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value, ENT_QUOTES, 'UTF-8');?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_12723533355dbafce865e719_10260626', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_content_top'} */
class Block_5178652285dbafce8664870_82016776 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_5568297195dbafce8665b55_97742641 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_21026671485dbafce865f6a0_91741036 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'acheter') && ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'pageshop')) {?>card-block <?php }?> row">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5178652285dbafce8664870_82016776', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5568297195dbafce8665b55_97742641', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer_container'} */
class Block_6462973485dbafce86676c5_55484995 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

     
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_12723533355dbafce865e719_10260626 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_12723533355dbafce865e719_10260626',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_21026671485dbafce865f6a0_91741036',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_5178652285dbafce8664870_82016776',
  ),
  'page_content' => 
  array (
    0 => 'Block_5568297195dbafce8665b55_97742641',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_6462973485dbafce86676c5_55484995',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">
	
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_21026671485dbafce865f6a0_91741036', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6462973485dbafce86676c5_55484995', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
