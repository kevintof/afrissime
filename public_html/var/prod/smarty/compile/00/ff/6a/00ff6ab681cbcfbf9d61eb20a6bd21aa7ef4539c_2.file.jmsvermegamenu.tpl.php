<?php
/* Smarty version 3.1.33, created on 2019-10-31 15:25:08
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\modules\jmsvermegamenu\views\templates\hook\jmsvermegamenu.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbafcd4b04183_79108169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '00ff6ab681cbcfbf9d61eb20a6bd21aa7ef4539c' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\modules\\jmsvermegamenu\\views\\templates\\hook\\jmsvermegamenu.tpl',
      1 => 1572458746,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbafcd4b04183_79108169 (Smarty_Internal_Template $_smarty_tpl) {
?>




<div class="ver-menu-wrapper">

	<div class="ver-title">

		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Categories','d'=>'Modules.JmsVermegamenu'),$_smarty_tpl ) );?>



	</div>

	<div class="ver-content">

		<?php echo $_smarty_tpl->tpl_vars['vermenu_html']->value;?>


	</div>

</div>

<?php echo '<script'; ?>
 type="text/javascript">

	var jmvmm_event = '<?php echo $_smarty_tpl->tpl_vars['JMSVMM_MOUSEEVENT']->value;?>
';

	var jmvmm_duration = '<?php echo $_smarty_tpl->tpl_vars['JMSVMM_DURATION']->value;?>
';	

<?php echo '</script'; ?>
>

<style type="text/css">

.jms-vermegamenu .dropdown-menu {    

	transition:all <?php echo $_smarty_tpl->tpl_vars['JMSVMM_DURATION']->value;?>
ms;

}

</style><?php }
}
