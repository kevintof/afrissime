<?php
/* Smarty version 3.1.33, created on 2019-10-31 15:25:19
  from 'module:psemailsubscriptionviewst' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbafcdfe81db6_47952288',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '307dc6bd4724d29d1572cc301dd7148e962604ef' => 
    array (
      0 => 'module:psemailsubscriptionviewst',
      1 => 1572458748,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbafcdfe81db6_47952288 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Block Newsletter module-->

	<div id="newsletter_block_left" class="block">	
		<div class="block_content">
			<div class="home1-text hidden">
				<div class="cap font-italic">
					<?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>Restons connectés!<?php } else { ?>Stay in touch!<?php }?>
				</div>
				<h3>Inscrivez vous à notre newsletter</h3>
				<div class="desc">
					<?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>
					Obtenez nos mises à jour hebdomadaire.
					<?php } else { ?>
					Have our weekly updates.
					<?php }?>
				</div>
			</div>
			<div class="popup-text hidden">
				<div class="cap" style="margin-bottom:0px;">
					<?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>Restons connectés!<?php } else { ?>Stay in touch!<?php }?>
				</div>
				<div class="desc">
					<?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>
					Obtenez nos mises à jour hebdomadaire.
					<?php } else { ?>
					Have our weekly updates.
					<?php }?>
				</div>
			</div>
			<div class="news_content">
	         	<div  class="block_content block_c_right">
					<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
#footer" method="post" name="emailForm">
						<div class="form-group<?php if (isset($_smarty_tpl->tpl_vars['msg']->value) && $_smarty_tpl->tpl_vars['msg']->value) {?> <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>form-error<?php } else { ?>form-ok<?php }
}?>" >
							<div class="input-email">
								<input class="inputNew form-control grey newsletter-input" id="newsletter-input" type="text" name="email" size="18" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['value']->value, ENT_QUOTES, 'UTF-8');?>
" placeholder="<?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>Entrez votre email<?php } else { ?>Enter your e-mail<?php }?>" required="required" />
							</div>
							<input type="hidden" name="action" value="0" />
							<button type="submit" name="submitNewsletter" class="btn btn-default btn-effect">
								OK
							</button>
						</div>
					</form>
				</div>
	       </div>
	       <?php if ($_smarty_tpl->tpl_vars['msg']->value) {?>
		        <div class="alert <?php if ($_smarty_tpl->tpl_vars['nw_error']->value) {?>alert-danger<?php } else { ?>alert-success<?php }?>">
		            <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['msg']->value, ENT_QUOTES, 'UTF-8');?>

		        </div>
    		<?php }?>
		</div>
		
	</div>
<?php }
}
