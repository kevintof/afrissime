<?php
/* Smarty version 3.1.33, created on 2019-10-31 17:10:28
  from 'C:\xampp\htdocs\afrissime\public_html\modules\hifacebookconnect\views\templates\hook\dashboard_zone.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbb1584aa9172_80974546',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8327ada243023b7b405bfaed28c60922789006e2' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\modules\\hifacebookconnect\\views\\templates\\hook\\dashboard_zone.tpl',
      1 => 1572458708,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbb1584aa9172_80974546 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['show_module']->value) {?>
	<?php if ($_smarty_tpl->tpl_vars['psv']->value >= 1.6) {?>
		<div class="col-lg-12">
			<div class="panel clearfix">
				<div class="panel-heading"> <i class="icon-cogs"></i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Check our modules','mod'=>'hifacebookconnect'),$_smarty_tpl ) );?>
</div>
	<?php } else { ?>
		<fieldset id="fieldset_0" class="module_advertising">
			<legend><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Check our modules','mod'=>'hifacebookconnect'),$_smarty_tpl ) );?>
</legend>
	<?php }?>
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['modules']->value, 'module', false, 'k');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['module']->value) {
?>
			<div class="module_info">
				<a class="addons-style-module-link" href="https://hipresta.com/module/hiprestashopapi/prestashopapi?secure_key=6db77b878f95ee7cb56d970e4f52f095&redirect=1&module_key=<?php echo $_smarty_tpl->tpl_vars['k']->value;?>
&dashboard=1" target="_blank">
					<div class="media addons-style-module panel">
						<div class="media-body addons-style-media-body">
							<h4 class="media-heading addons-style-media-heading"><?php echo $_smarty_tpl->tpl_vars['module']->value->display_name;?>
</h4>
						</div>
						<div class="addons-style-theme-preview center-block">
							<img class="addons-style-img_preview-theme" src="<?php echo $_smarty_tpl->tpl_vars['module']->value->image_link;?>
" style="max-width: 100%">
							<p class="btn btn-default">
								<?php if ($_smarty_tpl->tpl_vars['psv']->value >= 1.6) {?>
									<i class="icon-shopping-cart"></i>
								<?php } else { ?>
									<img src="../img/t/AdminParentOrders.gif" alt="">
								<?php }?>
								<?php echo $_smarty_tpl->tpl_vars['module']->value->price;?>

							</p>
						</div>
					</div>
				</a>
			</div>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	<?php if ($_smarty_tpl->tpl_vars['psv']->value >= 1.6) {?>
			</div>
		</div>
	<?php } else { ?>
		</fieldset>
		<?php echo '<script'; ?>
 type="text/javascript">
			$(document).ready(function() {
				var content = $('fieldset.module_advertising').clone();
				$('fieldset.module_advertising').remove();
				if ($('#column_left').find('#adminpresentation').length != 0) {
					$('#column_left').find('#adminpresentation').next().prepend(content);
				} else {
					$('#column_left').find('#blockNewVersionCheck').next().prepend(content);
				}
			});
		<?php echo '</script'; ?>
>
	<?php }
}?>
<div class="clearfix"></div>
<?php }
}
