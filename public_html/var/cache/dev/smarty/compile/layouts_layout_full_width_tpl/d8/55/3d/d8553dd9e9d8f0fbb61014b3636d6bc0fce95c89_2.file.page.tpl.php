<?php
/* Smarty version 3.1.33, created on 2019-11-01 09:17:38
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\templates\page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbbf8325d0278_88935577',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd8553dd9e9d8f0fbb61014b3636d6bc0fce95c89' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\templates\\page.tpl',
      1 => 1572458750,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbbf8325d0278_88935577 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
 
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value, ENT_QUOTES, 'UTF-8');?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15656883345dbbf8325cd529_24341437', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_content_top'} */
class Block_2089557155dbbf8325cecc3_80945664 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_13082804095dbbf8325cf284_00566528 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_8886178995dbbf8325cd9b3_61855720 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'acheter') && ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'pageshop')) {?>card-block <?php }?> row">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2089557155dbbf8325cecc3_80945664', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13082804095dbbf8325cf284_00566528', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer_container'} */
class Block_11862446155dbbf8325cfae9_30796790 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

     
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_15656883345dbbf8325cd529_24341437 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_15656883345dbbf8325cd529_24341437',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_8886178995dbbf8325cd9b3_61855720',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_2089557155dbbf8325cecc3_80945664',
  ),
  'page_content' => 
  array (
    0 => 'Block_13082804095dbbf8325cf284_00566528',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_11862446155dbbf8325cfae9_30796790',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">
	
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8886178995dbbf8325cd9b3_61855720', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11862446155dbbf8325cfae9_30796790', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
