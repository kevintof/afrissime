<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__a822f07b5bb9d8e6b230f783b9b2215ef7cbee9865629b29b06ad1e912df67a2 */
class __TwigTemplate_671a3536578829e04f8444f09995926a36e96a161d6c3e16db747b13d8c3d6da extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'stylesheets' => [$this, 'block_stylesheets'],
            'extra_stylesheets' => [$this, 'block_extra_stylesheets'],
            'content_header' => [$this, 'block_content_header'],
            'content' => [$this, 'block_content'],
            'content_footer' => [$this, 'block_content_footer'],
            'sidebar_right' => [$this, 'block_sidebar_right'],
            'javascripts' => [$this, 'block_javascripts'],
            'extra_javascripts' => [$this, 'block_extra_javascripts'],
            'translate_javascripts' => [$this, 'block_translate_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"fr\">
<head>
  <meta charset=\"utf-8\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">
<meta name=\"robots\" content=\"NOFOLLOW, NOINDEX\">

<link rel=\"icon\" type=\"image/x-icon\" href=\"/afrissime/public_html/img/favicon.ico\" />
<link rel=\"apple-touch-icon\" href=\"/afrissime/public_html/img/app_icon.png\" />

<title>Gestionnaire de modules • Afrissime</title>

  <script type=\"text/javascript\">
    var help_class_name = 'AdminModulesManage';
    var iso_user = 'fr';
    var lang_is_rtl = '0';
    var full_language_code = 'fr';
    var full_cldr_language_code = 'fr-FR';
    var country_iso_code = 'TG';
    var _PS_VERSION_ = '1.7.6.0';
    var roundMode = 2;
    var youEditFieldFor = '';
        var new_order_msg = 'Une nouvelle commande a été passée sur votre boutique.';
    var order_number_msg = 'Numéro de commande : ';
    var total_msg = 'Total : ';
    var from_msg = 'Du ';
    var see_order_msg = 'Afficher cette commande';
    var new_customer_msg = 'Un nouveau client s\\\\\\'est inscrit sur votre boutique';
    var customer_name_msg = 'Nom du client : ';
    var new_msg = 'Un nouveau message a été posté sur votre boutique.';
    var see_msg = 'Lire le message';
    var token = 'e47fc08b2ebec4aae5780e6ab2c18be5';
    var token_admin_orders = '2b9dd75674a3596a8e92687eb8d3efe8';
    var token_admin_customers = '7641882037034df65d6595c14a430d09';
    var token_admin_customer_threads = '97d59f5ddbc1d61b1f42e9fa2fb4ca06';
    var currentIndex = 'index.php?controller=AdminModulesManage';
    var employee_token = '9260fefa528945eb47f54a805925c41b';
    var choose_language_translate = 'Choisissez la langue';
    var default_language = '1';
    var admin_modules_link = '/afrissime/public_html/admin076ejqieb/index.php/improve/modules/catalog/recommended?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ';
    var admin_notification_get_link = '/afrissime/public_html/admin076ejqieb/index.php/common/notifications?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ';
    var admin_notification_push_link = '/afrissime/public_html/admin076ejqieb/index.php/common/notifications/ack?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ';
    var tab_modules_list = '';
    var update_success_msg = 'Mise à jour réussie';
    var errorLogin = 'PrestaShop n\\\\\\'a pas pu se connecter à Addons. Veuillez vérifier vos identifiants et votre connexion Internet.';
    var search_product_msg = 'Rechercher un produit';
  </script>

      <link href=\"/afrissime/public_html/modules/spmgsnipreview/views/css/tab.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/afrissime/public_html/admin076ejqieb/themes/new-theme/public/theme.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/afrissime/public_html/modules/spmgsnipreview/views/css/admin_header.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/afrissime/public_html/js/jquery/plugins/chosen/jquery.chosen.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/afrissime/public_html/modules/gamification/views/css/gamification.css\" rel=\"stylesheet\" type=\"text/css\"/>
      <link href=\"/afrissime/public_html/js/jquery/plugins/fancybox/jquery.fancybox.css\" rel=\"stylesheet\" type=\"text/css\"/>
  
  <script type=\"text/javascript\">
var baseAdminDir = \"\\/afrissime\\/public_html\\/admin076ejqieb\\/\";
var baseDir = \"\\/afrissime\\/public_html\\/\";
var changeFormLanguageUrl = \"\\/afrissime\\/public_html\\/admin076ejqieb\\/index.php\\/configure\\/advanced\\/employees\\/change-form-language?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\";
var currency = {\"iso_code\":\"EUR\",\"sign\":\"\\u20ac\",\"name\":\"Euro\",\"format\":null};
var currency_specifications = {\"symbol\":[\".\",\",\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"currencyCode\":\"EUR\",\"currencySymbol\":\"\\u20ac\",\"positivePattern\":\"#,##0.00\\u00a0\\u00a4\",\"negativePattern\":\"-#,##0.00\\u00a0\\u00a4\",\"maxFractionDigits\":2,\"minFractionDigits\":2,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var host_mode = false;
var number_specifications = {\"symbol\":[\".\",\",\",\";\",\"%\",\"-\",\"+\",\"E\",\"\\u00d7\",\"\\u2030\",\"\\u221e\",\"NaN\"],\"positivePattern\":\"#,##0.###\",\"negativePattern\":\"-#,##0.###\",\"maxFractionDigits\":3,\"minFractionDigits\":0,\"groupingUsed\":true,\"primaryGroupSize\":3,\"secondaryGroupSize\":3};
var show_new_customers = \"1\";
var show_new_messages = false;
var show_new_orders = \"1\";
</script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/modules/ps_faviconnotificationbo/views/js/favico.js\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/admin076ejqieb/themes/new-theme/public/main.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/js/jquery/plugins/jquery.chosen.js\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/js/admin.js?v=1.7.6.0\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/admin076ejqieb/themes/new-theme/public/cldr.bundle.js\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/js/tools.js?v=1.7.6.0\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/admin076ejqieb/public/bundle.js\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/js/vendor/d3.v3.min.js\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/modules/gamification/views/js/gamification_bt.js\"></script>
<script type=\"text/javascript\" src=\"/afrissime/public_html/js/jquery/plugins/fancybox/jquery.fancybox.js\"></script>

  <script type=\"text/javascript\">
/*
 * Return total of notification per checkbox checked
 * @param  int nbNewCustomer
 * @param  int nbNewOrder
 * @param  int nbNewMessage
 * @return int result        Total of Notification
 */
function getNotification(nbNewCustomer, nbNewOrder, nbNewMessage) {
    let result = 0;
    //if radiobutton is checked
     result += nbNewOrder;      result += nbNewCustomer;      result += nbNewMessage; 
    return result;
}

function loadAjax(adminController) {
    \$.ajax({
        type: 'POST',
        dataType: 'JSON',
        url: adminController,
        data: {
            ajax: true,
            action: \"GetNotifications\",
        },

        success: function(data) {

            let nbNewCustomers = parseInt(data.customer.total);
            let nbNewOrders = parseInt(data.order.total);
            let nbNewCustomerMessages = parseInt(data.customer_message.total);

            let nbTotalNotification = getNotification(nbNewCustomers, nbNewOrders, nbNewCustomerMessages);

            favicon.badge(nbTotalNotification);
        },
        error: function(err) {
            console.log(err);
            console.log(adminController);
        },
    });
}

function updateNotifications(type) {
  \$.post(
    baseAdminDir + \"ajax.php\",
    {
      \"updateElementEmployee\": \"1\",
      \"updateElementEmployeeType\": type
    }
  );
}

\$(document).ready(function() {
    adminController = adminController.replace(/\\amp;/g, '');
    //set the configuration of the favicon
    window.favicon = new Favico({
        animation: 'popFade',
        bgColor: BgColor,
        textColor: TxtColor,
    });
    loadAjax(adminController)
    setInterval(function() {
        loadAjax(adminController);
    }, 60000); //refresh notification every 60 seconds

    //update favicon when you click on the customer tab into your backoffice
    \$(document).on('click', '#subtab-AdminCustomers', function (e) {
        updateNotifications('customer');
    });
    //update favicon when you click on the customer service tab into your backoffice
    \$(document).on('click', '#subtab-AdminCustomerThreads', function (e) {
        updateNotifications('customer_message');
    });
    //update favicon when you click on the order tab into your backoffice
    \$(document).on('click', '#subtab-AdminOrders', function (e) {
        updateNotifications('order');
    });
});
</script>

<script type=\"text/javascript\">
    let BgColor = \"#DF0067\";
    let TxtColor = \"#ffffff\";
    let CheckBoxOrder = \"1\";
    let CheckBoxCustomer = \"1\";
    let CheckBoxMessage = \"1\";
    let adminController = \"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminAjaxFaviconBO&amp;token=f1acc396b9f1fd27e396174a62a9a57b\";
</script>

<script>
            var admin_gamification_ajax_url = \"http:\\/\\/localhost\\/afrissime\\/public_html\\/admin076ejqieb\\/index.php?controller=AdminGamification&token=602fe674a46c031fa2d905b2c3b094d9\";
            var current_id_tab = 45;
        </script>

";
        // line 174
        $this->displayBlock('stylesheets', $context, $blocks);
        $this->displayBlock('extra_stylesheets', $context, $blocks);
        echo "</head>

<body class=\"lang-fr adminmodulesmanage\">

  <header id=\"header\">

    <nav id=\"header_infos\" class=\"main-header\">
      <button class=\"btn btn-primary-reverse onclick btn-lg unbind ajax-spinner\"></button>

            <i class=\"material-icons js-mobile-menu\">menu</i>
      <a id=\"header_logo\" class=\"logo float-left\" href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminDashboard&amp;token=c8bcb86eb0df6b1755fd76e497110ff1\"></a>
      <span id=\"shop_version\">1.7.6.0</span>

      <div class=\"component\" id=\"quick-access-container\">
        <div class=\"dropdown quick-accesses\">
  <button class=\"btn btn-link btn-sm dropdown-toggle\" type=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\" id=\"quick_select\">
    Accès rapide
  </button>
  <div class=\"dropdown-menu\">
          <a class=\"dropdown-item\"
         href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminModules&amp;configure=allinone_rewards&amp;tab_module=&amp;module_name=allinone_rewards&amp;token=73219d156c8ba89172b0666e44f768e2\"
                 data-item=\"All-in-one Rewards\"
      >All-in-one Rewards</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminOrders&amp;token=2b9dd75674a3596a8e92687eb8d3efe8\"
                 data-item=\"Commandes\"
      >Commandes</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminStats&amp;module=statscheckup&amp;token=329af56a449d606248ededb529cf7c05\"
                 data-item=\"Évaluation du catalogue\"
      >Évaluation du catalogue</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php/improve/modules/manage?token=a1e730a688f9ce9791f3cbc0d96d37d0\"
                 data-item=\"Modules installés\"
      >Modules installés</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminCartRules&amp;addcart_rule&amp;token=f27fec9832ac400272bffb4e48eadc6c\"
                 data-item=\"Nouveau bon de réduction\"
      >Nouveau bon de réduction</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php/sell/catalog/products/new?token=a1e730a688f9ce9791f3cbc0d96d37d0\"
                 data-item=\"Nouveau produit\"
      >Nouveau produit</a>
          <a class=\"dropdown-item\"
         href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminCategories&amp;addcategory&amp;token=ca5558a034998b25642cdb46a892b90d\"
                 data-item=\"Nouvelle catégorie\"
      >Nouvelle catégorie</a>
        <div class=\"dropdown-divider\"></div>
          <a
        class=\"dropdown-item js-quick-link\"
        href=\"#\"
        data-rand=\"198\"
        data-icon=\"icon-AdminModulesSf\"
        data-method=\"add\"
        data-url=\"index.php/improve/modules/manage?-hDQ\"
        data-post-link=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminQuickAccesses&token=e87aa6814bdfcbb3187edc02f21fdd36\"
        data-prompt-text=\"Veuillez nommer ce raccourci :\"
        data-link=\"Modules - Liste\"
      >
        <i class=\"material-icons\">add_circle</i>
        Ajouter cette page à l'Accès Rapide
      </a>
        <a class=\"dropdown-item\" href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminQuickAccesses&token=e87aa6814bdfcbb3187edc02f21fdd36\">
      <i class=\"material-icons\">settings</i>
      Gérer les accès rapides
    </a>
  </div>
</div>
      </div>
      <div class=\"component\" id=\"header-search-container\">
        <form id=\"header_search\"
      class=\"bo_search_form dropdown-form js-dropdown-form collapsed\"
      method=\"post\"
      action=\"/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSearch&amp;token=379a63a61908437070abdffb47ec41f5\"
      role=\"search\">
  <input type=\"hidden\" name=\"bo_search_type\" id=\"bo_search_type\" class=\"js-search-type\" />
    <div class=\"input-group\">
    <input type=\"text\" class=\"form-control js-form-search\" id=\"bo_query\" name=\"bo_query\" value=\"\" placeholder=\"Rechercher (ex. : référence produit, nom du client, etc.)\">
    <div class=\"input-group-append\">
      <button type=\"button\" class=\"btn btn-outline-secondary dropdown-toggle js-dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
        Partout
      </button>
      <div class=\"dropdown-menu js-items-list\">
        <a class=\"dropdown-item\" data-item=\"Partout\" href=\"#\" data-value=\"0\" data-placeholder=\"Que souhaitez-vous trouver ?\" data-icon=\"icon-search\"><i class=\"material-icons\">search</i> Partout</a>
        <div class=\"dropdown-divider\"></div>
        <a class=\"dropdown-item\" data-item=\"Catalogue\" href=\"#\" data-value=\"1\" data-placeholder=\"Nom du produit, unité de gestion des stocks (SKU), référence...\" data-icon=\"icon-book\"><i class=\"material-icons\">store_mall_directory</i> Catalogue</a>
        <a class=\"dropdown-item\" data-item=\"Clients par nom\" href=\"#\" data-value=\"2\" data-placeholder=\"E-mail, nom...\" data-icon=\"icon-group\"><i class=\"material-icons\">group</i> Clients par nom</a>
        <a class=\"dropdown-item\" data-item=\"Clients par adresse IP\" href=\"#\" data-value=\"6\" data-placeholder=\"123.45.67.89\" data-icon=\"icon-desktop\"><i class=\"material-icons\">desktop_mac</i> Clients par adresse IP</a>
        <a class=\"dropdown-item\" data-item=\"Commandes\" href=\"#\" data-value=\"3\" data-placeholder=\"ID commande\" data-icon=\"icon-credit-card\"><i class=\"material-icons\">shopping_basket</i> Commandes</a>
        <a class=\"dropdown-item\" data-item=\"Factures\" href=\"#\" data-value=\"4\" data-placeholder=\"Numéro de facture\" data-icon=\"icon-book\"><i class=\"material-icons\">book</i> Factures</a>
        <a class=\"dropdown-item\" data-item=\"Paniers\" href=\"#\" data-value=\"5\" data-placeholder=\"ID panier\" data-icon=\"icon-shopping-cart\"><i class=\"material-icons\">shopping_cart</i> Paniers</a>
        <a class=\"dropdown-item\" data-item=\"Modules\" href=\"#\" data-value=\"7\" data-placeholder=\"Nom du module\" data-icon=\"icon-puzzle-piece\"><i class=\"material-icons\">extension</i> Modules</a>
      </div>
      <button class=\"btn btn-primary\" type=\"submit\"><span class=\"d-none\">RECHERCHE</span><i class=\"material-icons\">search</i></button>
    </div>
  </div>
</form>

<script type=\"text/javascript\">
 \$(document).ready(function(){
    \$('#bo_query').one('click', function() {
    \$(this).closest('form').removeClass('collapsed');
  });
});
</script>
      </div>

      
      
      <div class=\"component\" id=\"header-shop-list-container\">
          <div class=\"shop-list\">
    <a class=\"link\" id=\"header_shopname\" href=\"http://localhost/afrissime/public_html/\" target= \"_blank\">
      <i class=\"material-icons\">visibility</i>
      Voir ma boutique
    </a>
  </div>
      </div>

              <div class=\"component header-right-component\" id=\"header-notifications-container\">
          <div id=\"notif\" class=\"notification-center dropdown dropdown-clickable\">
  <button class=\"btn notification js-notification dropdown-toggle\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">notifications_none</i>
    <span id=\"notifications-total\" class=\"count hide\">0</span>
  </button>
  <div class=\"dropdown-menu dropdown-menu-right js-notifs_dropdown\">
    <div class=\"notifications\">
      <ul class=\"nav nav-tabs\" role=\"tablist\">
                          <li class=\"nav-item\">
            <a
              class=\"nav-link active\"
              id=\"orders-tab\"
              data-toggle=\"tab\"
              data-type=\"order\"
              href=\"#orders-notifications\"
              role=\"tab\"
            >
              Commandes<span id=\"_nb_new_orders_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"customers-tab\"
              data-toggle=\"tab\"
              data-type=\"customer\"
              href=\"#customers-notifications\"
              role=\"tab\"
            >
              Clients<span id=\"_nb_new_customers_\"></span>
            </a>
          </li>
                                    <li class=\"nav-item\">
            <a
              class=\"nav-link \"
              id=\"messages-tab\"
              data-toggle=\"tab\"
              data-type=\"customer_message\"
              href=\"#messages-notifications\"
              role=\"tab\"
            >
              Messages<span id=\"_nb_new_messages_\"></span>
            </a>
          </li>
                        </ul>

      <!-- Tab panes -->
      <div class=\"tab-content\">
                          <div class=\"tab-pane active empty\" id=\"orders-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Pas de nouvelle commande pour le moment :(<br>
              Et pourquoi pas lancer des promotions de saison ?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"customers-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Aucun nouveau client pour l'instant :(<br>
              Avez-vous fait une campagne d'acquisition par e-mail récemment ?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                                    <div class=\"tab-pane  empty\" id=\"messages-notifications\" role=\"tabpanel\">
            <p class=\"no-notification\">
              Pas de nouveau message pour l'instant.<br>
              Pas de nouvelle, bonne nouvelle, n'est-ce pas ?
            </p>
            <div class=\"notification-elements\"></div>
          </div>
                        </div>
    </div>
  </div>
</div>

  <script type=\"text/html\" id=\"order-notification-template\">
    <a class=\"notif\" href='order_url'>
      #_id_order_ -
      de <strong>_customer_name_</strong> (_iso_code_)_carrier_
      <strong class=\"float-sm-right\">_total_paid_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"customer-notification-template\">
    <a class=\"notif\" href='customer_url'>
      #_id_customer_ - <strong>_customer_name_</strong>_company_ - enregistré le <strong>_date_add_</strong>
    </a>
  </script>

  <script type=\"text/html\" id=\"message-notification-template\">
    <a class=\"notif\" href='message_url'>
    <span class=\"message-notification-status _status_\">
      <i class=\"material-icons\">fiber_manual_record</i> _status_
    </span>
      - <strong>_customer_name_</strong> (_company_) - <i class=\"material-icons\">access_time</i> _date_add_
    </a>
  </script>
        </div>
      
      <div class=\"component\" id=\"header-employee-container\">
        <div class=\"dropdown employee-dropdown\">
  <div class=\"rounded-circle person\" data-toggle=\"dropdown\">
    <i class=\"material-icons\">account_circle</i>
  </div>
  <div class=\"dropdown-menu dropdown-menu-right\">
    <div class=\"employee-wrapper-avatar\">
      
      <span class=\"employee_avatar\"><img class=\"avatar rounded-circle\" src=\"http://profile.prestashop.com/garrickervin%40gmail.com.jpg\" /></span>
      <span class=\"employee_profile\">Ravi de vous revoir kevin</span>
      <a class=\"dropdown-item employee-link profile-link\" href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/employees/2/edit?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\">
      <i class=\"material-icons\">settings</i>
      Votre profil
    </a>
    </div>
    
    <p class=\"divider\"></p>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/fr/ressources/documentation?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-fr&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">book</i> Documentation</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/fr/formation?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-fr&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">school</i> Formation</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/fr/experts?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-fr&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">person_pin_circle</i> Trouver un expert</a>
    <a class=\"dropdown-item\" href=\"https://addons.prestashop.com/fr/?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=addons-fr&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">extension</i> Place de marché de PrestaShop</a>
    <a class=\"dropdown-item\" href=\"https://www.prestashop.com/fr/contact?utm_source=back-office&amp;utm_medium=profile&amp;utm_campaign=resources-fr&amp;utm_content=download17\" target=\"_blank\"><i class=\"material-icons\">help</i> Centre d'assistance</a>
    <p class=\"divider\"></p>
    <a class=\"dropdown-item employee-link text-center\" id=\"header_logout\" href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminLogin&amp;logout=1&amp;token=9a288983d99c542adea845345e282ffc\">
      <i class=\"material-icons d-lg-none\">power_settings_new</i>
      <span>Déconnexion</span>
    </a>
  </div>
</div>
      </div>
    </nav>

    <div style=\"float: left\" class=\"badge_header_main\">

    <a href=\"index.php?controller=AdminSpmgsnipreviewreviews&amp;token=06bea5358bde79b5ab44bb1b911f1baf\" title=\"index.php?controller=AdminSpmgsnipreviewreviews&amp;token=06bea5358bde79b5ab44bb1b911f1baf\">
        <img src=\"http://localhost/afrissime/public_html/modules/spmgsnipreview/views/img/btn/ico-star.png\" alt=\"\" style=\"height:20px;\"/>
                <span class=\"badge_header\"
                      style=\"vertical-align: top; margin-left: -13px; margin-top: -2px;\"
                        >(4)</span>
    </a>


        <a href=\"index.php?controller=AdminSpmgsnipreviewstorereviews&amp;token=11328805a9e95775532c643db6ec34e2\" title=\"index.php?controller=AdminSpmgsnipreviewstorereviews&amp;token=11328805a9e95775532c643db6ec34e2\">
            <img src=\"http://localhost/afrissime/public_html/modules/spmgsnipreview/views/img/storereviews-logo.png\" alt=\"\" style=\"height:20px;\"/>
            <span class=\"badge_header\"
                  style=\"vertical-align: top; margin-left: -13px; margin-top: -2px;\"
                    >(0)</span>
        </a>

</div>
  </header>

  <nav class=\"nav-bar d-none d-md-block\">
  <span class=\"menu-collapse\" data-toggle-url=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/employees/toggle-navigation?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\">
    <i class=\"material-icons\">chevron_left</i>
    <i class=\"material-icons\">chevron_left</i>
  </span>

  <ul class=\"main-menu\">

          
                
                
        
          <li class=\"link-levelone \" data-submenu=\"1\" id=\"tab-AdminDashboard\">
            <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminDashboard&amp;token=c8bcb86eb0df6b1755fd76e497110ff1\" class=\"link\" >
              <i class=\"material-icons\">trending_up</i> <span>Tableau de bord</span>
            </a>
          </li>

        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"2\" id=\"tab-SELL\">
              <span class=\"title\">Vendre</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"3\" id=\"subtab-AdminParentOrders\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminOrders&amp;token=2b9dd75674a3596a8e92687eb8d3efe8\" class=\"link\">
                    <i class=\"material-icons mi-shopping_basket\">shopping_basket</i>
                    <span>
                    Commandes
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-3\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"4\" id=\"subtab-AdminOrders\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminOrders&amp;token=2b9dd75674a3596a8e92687eb8d3efe8\" class=\"link\"> Commandes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"5\" id=\"subtab-AdminInvoices\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/orders/invoices/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Factures
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"6\" id=\"subtab-AdminSlip\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSlip&amp;token=fc0ea88624be2eb5e9eef2b6ec8339b5\" class=\"link\"> Avoirs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"7\" id=\"subtab-AdminDeliverySlip\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/orders/delivery-slips/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Bons de livraison
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"8\" id=\"subtab-AdminCarts\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminCarts&amp;token=2c01527f5749ee31653377d4ffb3ed85\" class=\"link\"> Paniers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"152\" id=\"subtab-AdminDhlOrders\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminDhlOrders&amp;token=47181bb3d30886adf3769800e831f243\" class=\"link\"> DHL Commandes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"153\" id=\"subtab-AdminDhlLabel\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminDhlLabel&amp;token=8857297472f2c43ed0272dfc3156bdcf\" class=\"link\"> DHL Etiquettes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"154\" id=\"subtab-AdminDhlBulkLabel\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminDhlBulkLabel&amp;token=501043c6f840b57858f96a5109445d7a\" class=\"link\"> DHL Etiquettes en masse
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"155\" id=\"subtab-AdminDhlPickup\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminDhlPickup&amp;token=4066686e0bd6ddeeace653e9b167becc\" class=\"link\"> DHL Enlèvement
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"156\" id=\"subtab-AdminDhlManifest\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminDhlManifest&amp;token=6f43814c469e5f22c1b0cc6ba524aa0a\" class=\"link\"> DHL Manifeste
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"9\" id=\"subtab-AdminCatalog\">
                  <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/catalog/products?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\">
                    <i class=\"material-icons mi-store\">store</i>
                    <span>
                    Catalogue
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-9\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"10\" id=\"subtab-AdminProducts\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/catalog/products?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Produits
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"11\" id=\"subtab-AdminCategories\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/catalog/categories?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Catégories
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"12\" id=\"subtab-AdminTracking\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminTracking&amp;token=5d20140ff3c8ad2bc8e7d845e3ffc0e0\" class=\"link\"> Suivi
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"13\" id=\"subtab-AdminParentAttributesGroups\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminAttributesGroups&amp;token=992b0ac93ffe9122d7a9b92bf8abc86b\" class=\"link\"> Attributs &amp; caractéristiques
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"16\" id=\"subtab-AdminParentManufacturers\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/catalog/brands/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Marques et fournisseurs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"19\" id=\"subtab-AdminAttachments\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminAttachments&amp;token=f3cc27717f1f302a38e12ed1e6f3e728\" class=\"link\"> Fichiers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"20\" id=\"subtab-AdminParentCartRules\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminCartRules&amp;token=f27fec9832ac400272bffb4e48eadc6c\" class=\"link\"> Réductions
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"23\" id=\"subtab-AdminStockManagement\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/stocks/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Stocks
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"24\" id=\"subtab-AdminParentCustomer\">
                  <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/customers/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\">
                    <i class=\"material-icons mi-account_circle\">account_circle</i>
                    <span>
                    Clients
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-24\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"25\" id=\"subtab-AdminCustomers\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/sell/customers/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Clients
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"26\" id=\"subtab-AdminAddresses\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminAddresses&amp;token=423603de8a9f5c604712e9db7b357949\" class=\"link\"> Adresses
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"28\" id=\"subtab-AdminParentCustomerThreads\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminCustomerThreads&amp;token=97d59f5ddbc1d61b1f42e9fa2fb4ca06\" class=\"link\">
                    <i class=\"material-icons mi-chat\">chat</i>
                    <span>
                    SAV
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-28\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"29\" id=\"subtab-AdminCustomerThreads\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminCustomerThreads&amp;token=97d59f5ddbc1d61b1f42e9fa2fb4ca06\" class=\"link\"> SAV
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"30\" id=\"subtab-AdminOrderMessage\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminOrderMessage&amp;token=135f671db12af0b02f376f50a0b0993a\" class=\"link\"> Messages prédéfinis
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"31\" id=\"subtab-AdminReturn\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminReturn&amp;token=915b69ca282a4adc08792ba01776be9a\" class=\"link\"> Retours produits
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone\" data-submenu=\"32\" id=\"subtab-AdminStats\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminStats&amp;token=329af56a449d606248ededb529cf7c05\" class=\"link\">
                    <i class=\"material-icons mi-assessment\">assessment</i>
                    <span>
                    Statistiques
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                    </li>
                                                    
        
                
                                  
                
        
          <li class=\"category-title -active\" data-submenu=\"42\" id=\"tab-IMPROVE\">
              <span class=\"title\">Personnaliser</span>
          </li>

                          
                
                                                
                                                    
                <li class=\"link-levelone has_submenu -active open ul-open\" data-submenu=\"43\" id=\"subtab-AdminParentModulesSf\">
                  <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/modules/manage?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\">
                    <i class=\"material-icons mi-extension\">extension</i>
                    <span>
                    Modules
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_up
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-43\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo -active\" data-submenu=\"44\" id=\"subtab-AdminModulesSf\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/modules/manage?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Module Manager
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"48\" id=\"subtab-AdminParentModulesCatalog\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminPsMboModule&amp;token=8f66db12139120a51a8103de4e18c43d\" class=\"link\"> Catalogue de modules
                              </a>
                            </li>

                                                                                                                          </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"52\" id=\"subtab-AdminParentThemes\">
                  <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/design/themes/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\">
                    <i class=\"material-icons mi-desktop_mac\">desktop_mac</i>
                    <span>
                    Apparence
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-52\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"126\" id=\"subtab-AdminThemesParent\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/design/themes/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Thème et logo
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"134\" id=\"subtab-AdminPsMboTheme\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminPsMboTheme&amp;token=51b3cee71cb06080a65d42cb7093e3cd\" class=\"link\"> Catalogue de thèmes
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"55\" id=\"subtab-AdminMailThemeParent\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/design/mail_theme/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Thème d&#039;email
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"57\" id=\"subtab-AdminCmsContent\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/design/cms-pages/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Pages
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"58\" id=\"subtab-AdminModulesPositions\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/design/modules/positions/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Positions
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"59\" id=\"subtab-AdminImages\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminImages&amp;token=fd2a8365f25592fec33034be63bf283b\" class=\"link\"> Images
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"125\" id=\"subtab-AdminLinkWidget\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/modules/link-widget/list?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Link Widget
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"60\" id=\"subtab-AdminParentShipping\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminCarriers&amp;token=e07e83afe3514c41b0085a8cdeb84bac\" class=\"link\">
                    <i class=\"material-icons mi-local_shipping\">local_shipping</i>
                    <span>
                    Livraison
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-60\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"61\" id=\"subtab-AdminCarriers\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminCarriers&amp;token=e07e83afe3514c41b0085a8cdeb84bac\" class=\"link\"> Transporteurs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"62\" id=\"subtab-AdminShipping\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/shipping/preferences?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Préférences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"63\" id=\"subtab-AdminParentPayment\">
                  <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/payment/payment_methods?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\">
                    <i class=\"material-icons mi-payment\">payment</i>
                    <span>
                    Paiement
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-63\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"64\" id=\"subtab-AdminPayment\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/payment/payment_methods?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Modes de paiement
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"65\" id=\"subtab-AdminPaymentPreferences\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/payment/preferences?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Préférences
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"66\" id=\"subtab-AdminInternational\">
                  <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/international/localization/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\">
                    <i class=\"material-icons mi-language\">language</i>
                    <span>
                    International
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-66\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"67\" id=\"subtab-AdminParentLocalization\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/international/localization/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Localisation
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"72\" id=\"subtab-AdminParentCountries\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminZones&amp;token=ac8a3af7133ffff557db8274bc1a86a4\" class=\"link\"> Zones géographiques
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"76\" id=\"subtab-AdminParentTaxes\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/international/taxes/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Taxes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"79\" id=\"subtab-AdminTranslations\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/international/translations/settings?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Traductions
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"80\" id=\"tab-CONFIGURE\">
              <span class=\"title\">Configurer</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"81\" id=\"subtab-ShopParameters\">
                  <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/shop/preferences/preferences?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\">
                    <i class=\"material-icons mi-settings\">settings</i>
                    <span>
                    Paramètres de la boutique
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-81\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"82\" id=\"subtab-AdminParentPreferences\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/shop/preferences/preferences?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Paramètres généraux
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"85\" id=\"subtab-AdminParentOrderPreferences\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/shop/order-preferences/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Commandes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"88\" id=\"subtab-AdminPPreferences\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/shop/product-preferences/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Produits
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"89\" id=\"subtab-AdminParentCustomerPreferences\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/shop/customer-preferences/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Clients
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"93\" id=\"subtab-AdminParentStores\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/shop/contacts/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Contact
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"96\" id=\"subtab-AdminParentMeta\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/shop/seo-urls/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Trafic et SEO
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"100\" id=\"subtab-AdminParentSearchConf\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSearchConf&amp;token=56bb4d2679d85a53021c5f09cf72ed51\" class=\"link\"> Rechercher
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"130\" id=\"subtab-AdminGamification\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminGamification&amp;token=602fe674a46c031fa2d905b2c3b094d9\" class=\"link\"> Merchant Expertise
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"103\" id=\"subtab-AdminAdvancedParameters\">
                  <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/system-information/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\">
                    <i class=\"material-icons mi-settings_applications\">settings_applications</i>
                    <span>
                    Paramètres avancés
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-103\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"104\" id=\"subtab-AdminInformation\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/system-information/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Informations
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"105\" id=\"subtab-AdminPerformance\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/performance/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Performances
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"106\" id=\"subtab-AdminAdminPreferences\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/administration/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Administration
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"107\" id=\"subtab-AdminEmails\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/emails/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Email
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"108\" id=\"subtab-AdminImport\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/import/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Importer
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"109\" id=\"subtab-AdminParentEmployees\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/employees/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Équipe
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"113\" id=\"subtab-AdminParentRequestSql\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/sql-requests/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Base de données
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"116\" id=\"subtab-AdminLogs\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/logs/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Logs
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"117\" id=\"subtab-AdminWebservice\">
                              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/configure/advanced/webservice-keys/?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" class=\"link\"> Webservice
                              </a>
                            </li>

                                                                                                                                                                            </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"136\" id=\"tab-JMS-MODULES\">
              <span class=\"title\">Jms Modules</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"137\" id=\"subtab-AdminJmsmegamenuDashboard\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsmegamenuManager&amp;token=9eb6c4c188167a488bd9aeb14f5f40a2\" class=\"link\">
                    <i class=\"material-icons mi-menu\">menu</i>
                    <span>
                    Jms MegaMenu
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-137\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"138\" id=\"subtab-AdminJmsmegamenuManager\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsmegamenuManager&amp;token=9eb6c4c188167a488bd9aeb14f5f40a2\" class=\"link\"> Menu Manager
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"139\" id=\"subtab-AdminJmsmegamenuStyle\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsmegamenuStyle&amp;token=5fb194d8342d3a56a4c0aa7d60c75184\" class=\"link\"> Menu Style
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"140\" id=\"subtab-AdminJmsvermegamenuDashboard\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsvermegamenuManager&amp;token=f3418c363486a1dc35c71f436ed5e04a\" class=\"link\">
                    <i class=\"material-icons mi-menu\">menu</i>
                    <span>
                    Jms Vertical MegaMenu
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-140\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"141\" id=\"subtab-AdminJmsvermegamenuManager\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsvermegamenuManager&amp;token=f3418c363486a1dc35c71f436ed5e04a\" class=\"link\"> Menu Manager
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"142\" id=\"subtab-AdminJmsvermegamenuStyle\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsvermegamenuStyle&amp;token=3f69851c286f96aea8dae3b102ebcbed\" class=\"link\"> Menu Style
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"143\" id=\"subtab-AdminJmsblogDashboard\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsblogCategories&amp;token=11e82fc0cd4614a9117f4dd6a016fba4\" class=\"link\">
                    <i class=\"material-icons mi-\"></i>
                    <span>
                    Jms Blog
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-143\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"144\" id=\"subtab-AdminJmsblogCategories\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsblogCategories&amp;token=11e82fc0cd4614a9117f4dd6a016fba4\" class=\"link\"> Catégories
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"145\" id=\"subtab-AdminJmsblogPost\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsblogPost&amp;token=6e7a7e74520c6771cf3c15d79f60170a\" class=\"link\"> Post
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"146\" id=\"subtab-AdminJmsblogComment\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsblogComment&amp;token=7e0e0ab9ec106fa065be325df0b10289\" class=\"link\"> Comments
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"147\" id=\"subtab-AdminJmsblogSetting\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmsblogSetting&amp;token=909108c96d8ea8fbe3e41b2e6d2d7282\" class=\"link\"> Setting
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                                        
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"148\" id=\"subtab-AdminJmspagebuilderDashboard\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmspagebuilderHomepages&amp;token=689abd7627a4d6a92f342902fa9e90cc\" class=\"link\">
                    <i class=\"material-icons mi-\"></i>
                    <span>
                    Jms Page Builder
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-148\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"149\" id=\"subtab-AdminJmspagebuilderHomepages\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmspagebuilderHomepages&amp;token=689abd7627a4d6a92f342902fa9e90cc\" class=\"link\"> Home Pages
                              </a>
                            </li>

                                                                                                                              
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"151\" id=\"subtab-AdminJmspagebuilderSetting\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminJmspagebuilderSetting&amp;token=c2ccd1f5f0f0830c9e23fa78b4c96dce\" class=\"link\"> Setting
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"158\" id=\"tab-AdminSpmgsnipreviewreview\">
              <span class=\"title\">Avis</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"159\" id=\"subtab-AdminSpmgsnipreviewreviewtest\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSpmgsnipreviewreviews&amp;token=06bea5358bde79b5ab44bb1b911f1baf\" class=\"link\">
                    <i class=\"material-icons mi-\"></i>
                    <span>
                    Avis
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-159\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"160\" id=\"subtab-AdminSpmgsnipreviewreviews\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSpmgsnipreviewreviews&amp;token=06bea5358bde79b5ab44bb1b911f1baf\" class=\"link\"> Modéré Produit Avis
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"161\" id=\"subtab-AdminSpmgsnipreviewstorereviews\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSpmgsnipreviewstorereviews&amp;token=11328805a9e95775532c643db6ec34e2\" class=\"link\"> Modéré Magasin Avis
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"category-title \" data-submenu=\"163\" id=\"tab-AdminMarketplace\">
              <span class=\"title\">Marketplace</span>
          </li>

                          
                
                                                
                
                <li class=\"link-levelone has_submenu\" data-submenu=\"164\" id=\"subtab-AdminMarketplaceManagement\">
                  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminMarketplaceGeneralSettings&amp;token=c0131de73812bb4a8be549d02003e015\" class=\"link\">
                    <i class=\"material-icons mi-shopping_cart\">shopping_cart</i>
                    <span>
                    Marketplace
                    </span>
                                                <i class=\"material-icons sub-tabs-arrow\">
                                                                keyboard_arrow_down
                                                        </i>
                                        </a>
                                          <ul id=\"collapse-164\" class=\"submenu panel-collapse\">
                                                  
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"165\" id=\"subtab-AdminManageConfiguration\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminMarketplaceGeneralSettings&amp;token=c0131de73812bb4a8be549d02003e015\" class=\"link\"> Paramètres
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"170\" id=\"subtab-AdminManageSellerDetails\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSellerInfoDetail&amp;token=a077a84d0dd271f4fcaff11a07ce566c\" class=\"link\"> Sellers
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"173\" id=\"subtab-AdminManageSellerProduct\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSellerProductDetail&amp;token=0c5693cfe764702a84c72a3c16f9dfb9\" class=\"link\"> Produits
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"175\" id=\"subtab-AdminManageSellerOrders\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSellerOrders&amp;token=23f5b395a7f308acc844cb17a0fb0ac4\" class=\"link\"> Commandes
                              </a>
                            </li>

                                                                            
                            
                                                        
                            <li class=\"link-leveltwo \" data-submenu=\"177\" id=\"subtab-AdminManageSellerTransactions\">
                              <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminSellerTransactions&amp;token=554d895b66b2145238836a1260274478\" class=\"link\"> Transactions
                              </a>
                            </li>

                                                                        </ul>
                                    </li>
                          
        
                
                                  
                
        
          <li class=\"link-levelone \" data-submenu=\"188\" id=\"tab-PRESTASMS\">
            <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminPrestaSmsDashboardDefault&amp;token=20dd05fb7b1f160b085525e0ffdcf492\" class=\"link\" >
              <i class=\"material-icons\">mail_outline</i> <span>PrestaSMS</span>
            </a>
          </li>

        
            </ul>
  
</nav>

<div id=\"main-div\">
          
<div class=\"header-toolbar\">
  <div class=\"container-fluid\">

    
      <nav aria-label=\"Breadcrumb\">
        <ol class=\"breadcrumb\">
                      <li class=\"breadcrumb-item\">Module Manager</li>
          
                      <li class=\"breadcrumb-item active\">
              <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/modules/manage?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" aria-current=\"page\">Modules</a>
            </li>
                  </ol>
      </nav>
    

    <div class=\"title-row\">
      
          <h1 class=\"title\">
            Gestionnaire de modules          </h1>
      

      
        <div class=\"toolbar-icons\">
          <div class=\"wrapper\">
             

<script>
    
    var isSymfonyContext = true;
    var admin_module_ajax_url_psmbo = 'http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminPsMboModule&token=8f66db12139120a51a8103de4e18c43d';
    var controller = 'AdminModulesManage';
    
    if (isSymfonyContext === false) {
        
        \$(document).ready(function() {
            
            \$('.process-icon-modules-list').parent('a').prop('href', admin_module_ajax_url_psmbo);
            
            \$('.fancybox-quick-view').fancybox({
                type: 'ajax',
                autoDimensions: false,
                autoSize: false,
                width: 600,
                height: 'auto',
                helpers: {
                    overlay: {
                        locked: false
                    }
                }
            });
        });
    }
\t
\t\$(document).on('click', '#page-header-desc-configuration-modules-list', function(event) {
\t\tevent.preventDefault();
\t\topenModalOrRedirect(isSymfonyContext);
\t});
\t
\t\$('.process-icon-modules-list').parent('a').unbind().bind('click', function (event) {
\t\tevent.preventDefault();
\t\topenModalOrRedirect(isSymfonyContext);
\t});
    
    function openModalOrRedirect(isSymfonyContext) {
        if (isSymfonyContext === false) {
            \$('#modules_list_container').modal('show');
            openModulesList();
        } else {
            window.location.href = admin_module_ajax_url_psmbo;
        }
    }
\t
    function openModulesList() {
        \$.ajax({
            type: 'POST',
            url: admin_module_ajax_url_psmbo,
            data: {
                ajax : true,
                action : 'GetTabModulesList',
                controllerName: controller
            },
            success : function(data) {
                \$('#modules_list_container_tab_modal').html(data).slideDown();
                \$('#modules_list_loader').hide();
            },
        });
    }
\t
\t
</script>

                                                          <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-add_module\"
                  href=\"#\"                  title=\"Installer un module\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">cloud_upload</i>                  Installer un module
                </a>
                                                                        <a
                  class=\"btn btn-primary  pointer\"                  id=\"page-header-desc-configuration-addons_connect\"
                  href=\"#\"                  title=\"Se connecter à la marketplace Addons\"                  data-toggle=\"pstooltip\"
                  data-placement=\"bottom\"                >
                  <i class=\"material-icons\">vpn_key</i>                  Se connecter à la marketplace Addons
                </a>
                                      
            
                              <a class=\"btn btn-outline-secondary btn-help btn-sidebar\" href=\"#\"
                   title=\"Aide\"
                   data-toggle=\"sidebar\"
                   data-target=\"#right-sidebar\"
                   data-url=\"/afrissime/public_html/admin076ejqieb/index.php/common/sidebar/https%253A%252F%252Fhelp.prestashop.com%252Ffr%252Fdoc%252FAdminModules%253Fversion%253D1.7.6.0%2526country%253Dfr/Aide?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\"
                   id=\"product_form_open_help\"
                >
                  Aide
                </a>
                                    </div>
        </div>
      
    </div>
  </div>

  
      <div class=\"page-head-tabs\" id=\"head_tabs\">
      <ul class=\"nav nav-pills\">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              <li class=\"nav-item\">
                    <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/modules/manage?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" id=\"subtab-AdminModulesManage\" class=\"nav-link tab active current\" data-submenu=\"45\">
                      Modules
                      <span class=\"notification-container\">
                        <span class=\"notification-counter\"></span>
                      </span>
                    </a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/modules/alerts?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" id=\"subtab-AdminModulesNotifications\" class=\"nav-link tab \" data-submenu=\"46\">
                      Alertes
                      <span class=\"notification-container\">
                        <span class=\"notification-counter\"></span>
                      </span>
                    </a>
                  </li>
                                                                <li class=\"nav-item\">
                    <a href=\"/afrissime/public_html/admin076ejqieb/index.php/improve/modules/updates?_token=ottmbDwTQySXnu8X5NS6u8xfnbVzdSAai6fLZzV-hDQ\" id=\"subtab-AdminModulesUpdates\" class=\"nav-link tab \" data-submenu=\"47\">
                      Mises à jour
                      <span class=\"notification-container\">
                        <span class=\"notification-counter\"></span>
                      </span>
                    </a>
                  </li>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        </ul>
    </div>
    
</div>
      
      <div class=\"content-div  with-tabs\">

        

                                                        
        <div class=\"row \">
          <div class=\"col-sm-12\">
            <div id=\"ajax_confirmation\" class=\"alert alert-success\" style=\"display: none;\"></div>


  ";
        // line 1597
        $this->displayBlock('content_header', $context, $blocks);
        // line 1598
        echo "                 ";
        $this->displayBlock('content', $context, $blocks);
        // line 1599
        echo "                 ";
        $this->displayBlock('content_footer', $context, $blocks);
        // line 1600
        echo "                 ";
        $this->displayBlock('sidebar_right', $context, $blocks);
        // line 1601
        echo "
             
<div class=\"modal fade\" id=\"modules_list_container\">
\t<div class=\"modal-dialog\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
\t\t\t\t<h3 class=\"modal-title\">Modules et services recommandés</h3>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div id=\"modules_list_container_tab_modal\" style=\"display:none;\"></div>
\t\t\t\t<div id=\"modules_list_loader\"><i class=\"icon-refresh icon-spin\"></i></div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
          </div>
        </div>

      </div>
    </div>

  <div id=\"non-responsive\" class=\"js-non-responsive\">
  <h1>Oh non !</h1>
  <p class=\"mt-3\">
    La version mobile de cette page n'est pas encore disponible.
  </p>
  <p class=\"mt-2\">
    En attendant que cette page soit adaptée au mobile, vous êtes invité à la consulter sur ordinateur.
  </p>
  <p class=\"mt-2\">
    Merci.
  </p>
  <a href=\"http://localhost/afrissime/public_html/admin076ejqieb/index.php?controller=AdminDashboard&amp;token=c8bcb86eb0df6b1755fd76e497110ff1\" class=\"btn btn-primary py-1 mt-3\">
    <i class=\"material-icons\">arrow_back</i>
    Précédent
  </a>
</div>
  <div class=\"mobile-layer\"></div>

      <div id=\"footer\" class=\"bootstrap\">
    
</div>
  

      <div class=\"bootstrap\">
      <div class=\"modal fade\" id=\"modal_addons_connect\" tabindex=\"-1\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>
\t\t\t\t<h4 class=\"modal-title\"><i class=\"icon-puzzle-piece\"></i> <a target=\"_blank\" href=\"https://addons.prestashop.com/?utm_source=back-office&utm_medium=modules&utm_campaign=back-office-FR&utm_content=download\">PrestaShop Addons</a></h4>
\t\t\t</div>
\t\t\t
\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<!--start addons login-->
\t\t\t<form id=\"addons_login_form\" method=\"post\" >
\t\t\t\t<div>
\t\t\t\t\t<a href=\"https://addons.prestashop.com/fr/login?email=garrickervin%40gmail.com&amp;firstname=kevin&amp;lastname=TOFFA&amp;website=http%3A%2F%2Flocalhost%2Fafrissime%2Fpublic_html%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-FR&amp;utm_content=download#createnow\"><img class=\"img-responsive center-block\" src=\"/afrissime/public_html/admin076ejqieb/themes/default/img/prestashop-addons-logo.png\" alt=\"Logo PrestaShop Addons\"/></a>
\t\t\t\t\t<h3 class=\"text-center\">Connectez-vous à la place de marché de PrestaShop afin d'importer automatiquement tous vos achats.</h3>
\t\t\t\t\t<hr />
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Vous n'avez pas de compte ?</h4>
\t\t\t\t\t\t<p class='text-justify'>Les clés pour réussir votre boutique sont sur PrestaShop Addons ! Découvrez sur la place de marché officielle de PrestaShop plus de 3 500 modules et thèmes pour augmenter votre trafic, optimiser vos conversions, fidéliser vos clients et vous faciliter l’e-commerce.</p>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<h4>Connectez-vous à PrestaShop Addons</h4>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-user\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"username_addons\" name=\"username_addons\" type=\"text\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t<div class=\"input-group-prepend\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-text\"><i class=\"icon-key\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t<input id=\"password_addons\" name=\"password_addons\" type=\"password\" value=\"\" autocomplete=\"off\" class=\"form-control ac_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<a class=\"btn btn-link float-right _blank\" href=\"//addons.prestashop.com/fr/forgot-your-password\">Mot de passe oublié</a>
\t\t\t\t\t\t\t<br>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div class=\"row row-padding-top\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<a class=\"btn btn-default btn-block btn-lg _blank\" href=\"https://addons.prestashop.com/fr/login?email=garrickervin%40gmail.com&amp;firstname=kevin&amp;lastname=TOFFA&amp;website=http%3A%2F%2Flocalhost%2Fafrissime%2Fpublic_html%2F&amp;utm_source=back-office&amp;utm_medium=connect-to-addons&amp;utm_campaign=back-office-FR&amp;utm_content=download#createnow\">
\t\t\t\t\t\t\t\tCréer un compte
\t\t\t\t\t\t\t\t<i class=\"icon-external-link\"></i>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t<button id=\"addons_login_button\" class=\"btn btn-primary btn-block btn-lg\" type=\"submit\">
\t\t\t\t\t\t\t\t<i class=\"icon-unlock\"></i> Connexion
\t\t\t\t\t\t\t</button>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>

\t\t\t\t<div id=\"addons_loading\" class=\"help-block\"></div>

\t\t\t</form>
\t\t\t<!--end addons login-->
\t\t\t</div>


\t\t\t\t\t</div>
\t</div>
</div>

    </div>
  
";
        // line 1722
        $this->displayBlock('javascripts', $context, $blocks);
        $this->displayBlock('extra_javascripts', $context, $blocks);
        $this->displayBlock('translate_javascripts', $context, $blocks);
        echo "</body>
</html>";
    }

    // line 174
    public function block_stylesheets($context, array $blocks = [])
    {
    }

    public function block_extra_stylesheets($context, array $blocks = [])
    {
    }

    // line 1597
    public function block_content_header($context, array $blocks = [])
    {
    }

    // line 1598
    public function block_content($context, array $blocks = [])
    {
    }

    // line 1599
    public function block_content_footer($context, array $blocks = [])
    {
    }

    // line 1600
    public function block_sidebar_right($context, array $blocks = [])
    {
    }

    // line 1722
    public function block_javascripts($context, array $blocks = [])
    {
    }

    public function block_extra_javascripts($context, array $blocks = [])
    {
    }

    public function block_translate_javascripts($context, array $blocks = [])
    {
    }

    public function getTemplateName()
    {
        return "__string_template__a822f07b5bb9d8e6b230f783b9b2215ef7cbee9865629b29b06ad1e912df67a2";
    }

    public function getDebugInfo()
    {
        return array (  1812 => 1722,  1807 => 1600,  1802 => 1599,  1797 => 1598,  1792 => 1597,  1783 => 174,  1775 => 1722,  1652 => 1601,  1649 => 1600,  1646 => 1599,  1643 => 1598,  1641 => 1597,  214 => 174,  39 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "__string_template__a822f07b5bb9d8e6b230f783b9b2215ef7cbee9865629b29b06ad1e912df67a2", "");
    }
}
