<?php
/* Smarty version 3.1.33, created on 2019-11-01 08:37:49
  from 'module:pslanguageselectorpslangu' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbbeedd4859e5_14516747',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1c00f78dace25d509ec3a1f54176b7ae2000accf' => 
    array (
      0 => 'module:pslanguageselectorpslangu',
      1 => 1572458748,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbbeedd4859e5_14516747 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- Block languages module -->
<?php if ($_smarty_tpl->tpl_vars['jpb_homepage']->value == 1 || $_smarty_tpl->tpl_vars['jpb_homepage']->value == 3 || $_smarty_tpl->tpl_vars['jpb_homepage']->value == 6) {?>
	<?php if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
		<div class="top-list">
			<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['base_url'], ENT_QUOTES, 'UTF-8');?>
" method="get">
				<ul>
					<li>
						<?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>
						<a href="https://www.afrissime.com/ouvrir-une-boutique">Vendre sur Afrissime</a>
						<?php } else { ?>
						<a href="https://www.afrissime.com/create-your-shop">Sell on Afrissime</a>

						<?php }?>
					</li>
					<li><a href="#" class="dropdown-btn"><?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>Aide <?php } else { ?> Help <?php }?><i class="fa fa-caret-down"></i></a>
						<ul id="dropdown-container">
							<li style="padding-left: 20px;"><a href="#"> <?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>Service client <?php } else { ?> Customer service <?php }?></a></li>
							<li><a href="#"> Litige </a></li>
							<li><a href="#"> <?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>Effectuer un retour Produit <?php } else { ?> Return a product <?php }?></a></li>
							<li><a href="#">Protection de l'acheteur</a></li>
							<li>
						<a href="#" style="border-right:none;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Litiges / réclamations:','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
</a>
					</li>
						</ul>
					</li>
				</ul>
				
					<ul class="switcher">
						<li>
						<a href="#" class="dropdown-btn"> <img src="https://www.afrissime.com/themes/jms_basel/assets/img/icon/countries/<?php if (!empty($_COOKIE['pays_visiteur'])) {?> <?php echo htmlspecialchars($_COOKIE['pays_visiteur'], ENT_QUOTES, 'UTF-8');?>
.png <?php } else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['pays_visiteur']->value, ENT_QUOTES, 'UTF-8');?>
.png<?php }?>"/> / <span><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['current_language']->value['name_simple'],3,'' )), ENT_QUOTES, 'UTF-8');?>
</span> / <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['currency']->value['iso_code'], ENT_QUOTES, 'UTF-8');?>
</span> <i class="fa fa-caret-down"></i></a>
						
								<ul id="dropdown-switcher">
									<li>
										<span style="font-size:12px; color:#000;padding-left:20px;"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Localisation:','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
 </span>
										<div style="overflow:hidden; padding-left:20px;padding-bottom:20px;">
												<select name="id_country" id="id_country" class="form-control form-control-select" style="width:200px">
												<option value=""><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select Country','mod'=>'marketplace'),$_smarty_tpl ) );?>
</option>
												<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['country']->value, 'countrydetail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['countrydetail']->value) {
?>
													<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countrydetail']->value['id_country'], ENT_QUOTES, 'UTF-8');?>
">
														<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countrydetail']->value['name'], ENT_QUOTES, 'UTF-8');?>

													</option>
												<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
											</select>
										</div>
									</li>
									<li>
										<span style="color:#000; font-size:12px"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Language','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
</span>
										<div style="overflow:hidden;padding-left:20px">
										<select name="id_lang" class="form-control form-control-select" style="width:200px">
											<option value=""><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select Language','mod'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
</option>
											<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['language']->value) {
?>
												<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['id_lang'], ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['name_simple'], ENT_QUOTES, 'UTF-8');?>

											    </option>
											<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
										</select>
											
										</div>
									</li>
									<li>
										<span style="font-size:12px; color:#000"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Monnaie:','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
 </span>
										
											<div style="overflow:hidden; padding-left:20px;padding-bottom:20px;">
												<select name="id_currency" id="currency" class="form-control form-control-select" style="width:200px;margin-bottom:20px;">
													<option value="1">€ Euro - EUR</option>
													<option value="2">$ Dollar US - USD</option>
													<option value="3">CFA XOF</option>
													<option value="5">FCFA XAF</option>
													<option value="6">£ GBP</option>
												</select>
												<div>
													<button name="button" type="submit" class="btn btn-active btn-effect" style="width:200px;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ENREGISTRER','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
</button>
												</div>
												<input type="hidden" name="SubmitCurrency" value="1"/>
											</div>	
										
									</li>
								</ul>
							
						</li>
					</ul>
				
				<ul style="padding-left:10px">
					<li>
						<a href="https://www.afrissime.com/jmsblog/1_news.html">Blog</a>
					</li>
				</ul>
			</form>
		</div>
		<?php echo '<script'; ?>
>
			//* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
			var dropdown = document.getElementsByClassName("dropdown-btn");
			var i;

			for (i = 0; i < dropdown.length; i++) {
			  dropdown[i].addEventListener("click", function() {
			    this.classList.toggle("active");
			    var dropdownContent = this.nextElementSibling;
			    if (dropdownContent.style.display === "block") {
			      dropdownContent.style.display = "none";
			    } else {
			      dropdownContent.style.display = "block";
			      dropdownContent.style.width = "250px";
			      dropdownContent.style.top = "42px";
			      dropdownContent.style.right = "0px";
			      dropdownContent.style.position = "absolute";
			    }
			  });
			} 
		<?php echo '</script'; ?>
>
	<?php }?>	
<?php } elseif ($_smarty_tpl->tpl_vars['jpb_homepage']->value == 4 || $_smarty_tpl->tpl_vars['jpb_homepage']->value == 7) {?>
	<?php if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
		<div class="btn-group compact-hidden languages-info">
			<a href="#"  class="btn-xs dropdown-toggle" data-toggle="dropdown">
				<span class="btn-name"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['current_language']->value['name_simple'],3,'' )), ENT_QUOTES, 'UTF-8');?>
</span>
			
			</a>
			<ul class="dropdown-menu" role="menu">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language', false, 'k', 'languages', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['language']->value) {
?>
					<li <?php if ($_smarty_tpl->tpl_vars['language']->value['id_lang'] == $_smarty_tpl->tpl_vars['current_language']->value['id_lang']) {?> class="current" <?php }?>>
						<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'language','id'=>$_smarty_tpl->tpl_vars['language']->value['id_lang']),$_smarty_tpl ) );?>
" class="dropdown-item">
							<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['language']->value['name_simple'],3,'' )), ENT_QUOTES, 'UTF-8');?>

						</a>
					</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>		
			</ul>
		</div>
		<div class="btn-group compact-hidden languages-info">
			<a href="#"  class="btn-xs dropdown-toggle" data-toggle="dropdown">
				<span class="btn-name"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['current_language']->value['name_simple'],3,'' )), ENT_QUOTES, 'UTF-8');?>
</span>
			
			</a>
			<ul class="dropdown-menu" role="menu">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language', false, 'k', 'languages', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['language']->value) {
?>
					<li <?php if ($_smarty_tpl->tpl_vars['language']->value['id_lang'] == $_smarty_tpl->tpl_vars['current_language']->value['id_lang']) {?> class="current" <?php }?>>
						<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'language','id'=>$_smarty_tpl->tpl_vars['language']->value['id_lang']),$_smarty_tpl ) );?>
" class="dropdown-item">
							<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'truncate' ][ 0 ], array( $_smarty_tpl->tpl_vars['language']->value['name_simple'],3,'' )), ENT_QUOTES, 'UTF-8');?>

						</a>
					</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>		
			</ul>
		</div>
	<?php }
} else {
if (count($_smarty_tpl->tpl_vars['languages']->value) > 1) {?>
		<div class="btn-group compact-hidden languages-info">
			<a href="#"  class="btn-xs dropdown-toggle" data-toggle="dropdown">
				<span class="btn-name"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Languages:','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
</span>			
			</a>
			<ul>
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['languages']->value, 'language', false, 'k', 'languages', array (
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['k']->value => $_smarty_tpl->tpl_vars['language']->value) {
?>
					<li <?php if ($_smarty_tpl->tpl_vars['language']->value['id_lang'] == $_smarty_tpl->tpl_vars['current_language']->value['id_lang']) {?> class="current" <?php }?>>
						<a href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'language','id'=>$_smarty_tpl->tpl_vars['language']->value['id_lang']),$_smarty_tpl ) );?>
" class="dropdown-item">
							<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['language']->value['name_simple'], ENT_QUOTES, 'UTF-8');?>

						</a>
					</li>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>		
			</ul>
		</div>
	<?php }
}?>
<!-- /Block languages module -->
<?php }
}
