<?php
/* Smarty version 3.1.33, created on 2019-11-01 08:37:49
  from 'C:\xampp\htdocs\afrissime\public_html\modules\allinone_rewards\views\templates\hook\presta-1.7\facebook_confirmation.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbbeedda5a9b6_15055759',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '48f4892c213be84d4f75939aa03d941e17cfbeb9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\modules\\allinone_rewards\\views\\templates\\hook\\presta-1.7\\facebook_confirmation.tpl',
      1 => 1572458705,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbbeedda5a9b6_15055759 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- MODULE allinone_rewards -->
<div id="rewards_facebook_confirm">
	<div id="rewards_facebook_confirm_content">
		<?php echo $_smarty_tpl->tpl_vars['facebook_confirm_txt']->value;?>

		<?php if ($_smarty_tpl->tpl_vars['facebook_code']->value) {?>
		<center><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Code :','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <span id="rewards_facebook_code"></span></center>
		<?php }?>
	</div>
</div>
<?php echo '<script'; ?>
>
var url_facebook_api="//connect.facebook.net/<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['facebook_language']->value, ENT_QUOTES, 'UTF-8');?>
/all.js#xfbml=1";
var url_allinone_facebook="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'module','name'=>'allinone_rewards','controller'=>'facebook'),$_smarty_tpl ) );?>
";
<?php echo '</script'; ?>
>
<!-- END : MODULE allinone_rewards --><?php }
}
