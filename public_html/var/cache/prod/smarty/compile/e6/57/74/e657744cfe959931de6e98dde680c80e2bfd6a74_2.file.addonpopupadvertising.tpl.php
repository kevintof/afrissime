<?php
/* Smarty version 3.1.33, created on 2019-11-01 08:37:48
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\modules\jmspagebuilder\views\templates\hook\addonpopupadvertising.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbbeedcb77d08_31682410',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e657744cfe959931de6e98dde680c80e2bfd6a74' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\modules\\jmspagebuilder\\views\\templates\\hook\\addonpopupadvertising.tpl',
      1 => 1572458744,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbbeedcb77d08_31682410 (Smarty_Internal_Template $_smarty_tpl) {
if (!$_smarty_tpl->tpl_vars['customer']->value['newsletter']) {?>
<div class="jms-popup-overlay hidden-sm hidden-xs hidden" style="display:none;">

	<div class="jms-popup">		

		<div class="jms-popup-content">

			<div class="html-content col-lg-6 col-md-6">

				<?php echo $_smarty_tpl->tpl_vars['html_content']->value;?>


			</div>

			<div class="module-content col-lg-6 col-md-6">

				<?php echo $_smarty_tpl->tpl_vars['popup_content']->value;?>


				<div class="dontshow">

					<input type="checkbox" name="dontshowagain" value="1" id="dontshowagain" /> 

					<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Dont show this box again','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
</label>

				</div>

			</div>

		</div>						

		<a class="popup-close icon-close"></a>

		<input type="hidden" name="width_default" id="width-default" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['popup_width']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />

		<input type="hidden" name="height_default" id="height-default" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['popup_height']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />

		<input type="hidden" name="loadtime" id="loadtime" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['loadtime']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />

	</div>

</div>
<?php }
}
}
