<?php
/* Smarty version 3.1.33, created on 2019-11-01 08:37:45
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\modules\jmspagebuilder\views\templates\hook\addonproductcarousel.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbbeed9dd07a0_04239336',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21988d3caf7a1936a92cff32f6728c654c686206' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\modules\\jmspagebuilder\\views\\templates\\hook\\addonproductcarousel.tpl',
      1 => 1572458744,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:catalog/_partials/miniatures/product.tpl' => 1,
  ),
),false)) {
function content_5dbbeed9dd07a0_04239336 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_assignInScope('unique_id', mt_rand(1,1000));
echo '<script'; ?>
 type="text/javascript">
	if(p_unique)
		p_unique.push(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
);
	else 
		var p_unique = [];
		p_unique.push(<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
);

	if(p_items)
		p_items[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>4<?php }?>;
	else
		var p_items = [];
		p_items[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>4<?php }?>;
	if(p_itemsDesktop)
		p_itemsDesktop[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>4<?php }?>;
	else
		var p_itemsDesktop = [];
		p_itemsDesktop[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>4<?php }?>;

	if(p_itemsDesktopSmall)
		p_itemsDesktopSmall[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols_md']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols_md']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>3<?php }?>;
	else
		var p_itemsDesktopSmall = [];
		p_itemsDesktopSmall[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols_md']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols_md']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>3<?php }?>;

	if(p_itemsTablet)
		p_itemsTablet[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols_sm']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols_sm']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>2<?php }?>;
	else
		var p_itemsTablet = [];
		p_itemsTablet[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols_sm']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols_sm']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>2<?php }?>;
	if(p_itemsMobile)
		p_itemsMobile[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols_xs']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols_xs']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>1<?php }?>;
	else
		var p_itemsMobile = [];
		p_itemsMobile[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['cols_xs']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['cols_xs']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
} else { ?>1<?php }?>;

	if(p_nav)
		p_nav[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['navigation']->value == 1) {?>true<?php } else { ?>false<?php }?>;
	else
		var p_nav = [];
		p_nav[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['navigation']->value == 1) {?>true<?php } else { ?>false<?php }?>;

	if(p_pag)
		p_pag[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['pagination']->value == 1) {?>true<?php } else { ?>false<?php }?>;
	else
		var p_pag = [];
		p_pag[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['pagination']->value == 1) {?>true<?php } else { ?>false<?php }?>;
	if(p_auto)
		p_auto[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['autoplay']->value == 1) {?>true<?php } else { ?>false<?php }?>;
	else
		var p_auto = [];
		p_auto[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['autoplay']->value == 1) {?>true<?php } else { ?>false<?php }?>;
	if(p_rewind)
		p_rewind[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['rewind']->value == 1) {?>true<?php } else { ?>false<?php }?>;
	else
		var p_rewind = [];
		p_rewind[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['rewind']->value == 1) {?>true<?php } else { ?>false<?php }?>;
	if(p_slideby)
		p_slideby[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['slidebypage']->value == 1) {?>'page'<?php } else { ?>1<?php }?>;
	else
		var p_slideby = [];
		p_slideby[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
]=<?php if ($_smarty_tpl->tpl_vars['slidebypage']->value == 1) {?>'page'<?php } else { ?>1<?php }?>;
<?php echo '</script'; ?>
>
<div class="product-carousel-wrapper default-style">
	<?php if ($_smarty_tpl->tpl_vars['addon_title']->value || $_smarty_tpl->tpl_vars['addon_desc']->value) {?>
		<div class="addon-title custom-title">
			<?php if ($_smarty_tpl->tpl_vars['addon_title']->value) {?>
				<h3>
					<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['addon_title']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
	
				</h3>		
			<?php }?>
			<?php if ($_smarty_tpl->tpl_vars['addon_desc']->value) {?>
				<p class="addon-desc"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['addon_desc']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</p>
			<?php }?>
			<span class="b-title_separator"><span></span></span>
		</div>
	<?php }?>
	
	
	<div class="product-carousel product-carousel-<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['unique_id']->value, ENT_QUOTES, 'UTF-8');?>
">	
		<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products_slides']->value, 'products_slide');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['products_slide']->value) {
?>
			<div class="item ajax_block_product">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['products_slide']->value, 'product');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['product']->value) {
?>
					<?php $_smarty_tpl->_subTemplateRender("file:catalog/_partials/miniatures/product.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('product'=>$_smarty_tpl->tpl_vars['product']->value), 0, true);
?>
				<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
			</div>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
	</div>
</div><?php }
}
