<?php
/* Smarty version 3.1.33, created on 2019-11-01 08:37:49
  from 'C:\xampp\htdocs\afrissime\public_html\themes\jms_basel\modules\jmsajaxsearch\views\templates\hook\jmsajaxsearch.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5dbbeedd4db446_52091161',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e350d07734b54a5c2fc70fc76733bd9e6265bbbd' => 
    array (
      0 => 'C:\\xampp\\htdocs\\afrissime\\public_html\\themes\\jms_basel\\modules\\jmsajaxsearch\\views\\templates\\hook\\jmsajaxsearch.tpl',
      1 => 1572458742,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5dbbeedd4db446_52091161 (Smarty_Internal_Template $_smarty_tpl) {
?>


<div id="jms_ajax_search" class="btn-group compact-hidden default">	

	<div class="search-wrapper">

		<form method="get" action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getPageLink('search'),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" id="searchbox">

			<input type="hidden" name="controller" value="search" />

			<input type="hidden" name="orderby" value="position" />

			<input type="hidden" name="orderway" value="desc" />

			<input type="text" id="ajax_search" name="search_query" placeholder="<?php if ($_smarty_tpl->tpl_vars['language']->value['language_code'] == 'fr') {?>Que recherchez vous?<?php } else { ?> What are you looking for ?<?php }?>" class="form-control" />		

			<span class="icon-magnifier ic2"></span>

		</form>

		<div id="search_result">

		</div>

	</div>	

</div>

<?php }
}
