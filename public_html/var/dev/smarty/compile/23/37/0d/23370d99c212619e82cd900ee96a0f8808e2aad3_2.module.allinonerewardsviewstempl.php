<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:14:34
  from 'module:allinonerewardsviewstempl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db705da47a527_51212963',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '23370d99c212619e82cd900ee96a0f8808e2aad3' => 
    array (
      0 => 'module:allinonerewardsviewstempl',
      1 => 1571236966,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db705da47a527_51212963 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/afrissim/public_html/modules/allinone_rewards/views/templates/front/presta-1.7/sponsorship.tpl -->
<div id="rewards_sponsorship" class="rewards">
	<?php if ($_smarty_tpl->tpl_vars['error']->value && isset($_smarty_tpl->tpl_vars['aior_popup']->value)) {?>
	<p class="alert alert-danger">
		<?php if ($_smarty_tpl->tpl_vars['error']->value == 'email invalid') {?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'At least one email address is invalid!','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php } elseif ($_smarty_tpl->tpl_vars['error']->value == 'name invalid') {?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'At least one first name or last name is invalid!','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php } elseif ($_smarty_tpl->tpl_vars['error']->value == 'email exists') {?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Someone with this email address has already been sponsored','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
: <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['mails_exists']->value, 'mail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['mail']->value) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['mail']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?><br>
		<?php } elseif ($_smarty_tpl->tpl_vars['error']->value == 'no revive checked') {?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Please mark at least one checkbox','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php } elseif ($_smarty_tpl->tpl_vars['error']->value == 'bad phone') {?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The mobile phone is invalid','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php } elseif ($_smarty_tpl->tpl_vars['error']->value == 'sms already sent') {?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This mobile phone has already been invited during last 10 days, please retry later.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php } elseif ($_smarty_tpl->tpl_vars['error']->value == 'sms impossible') {?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'An error occured, the SMS has not been sent','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php }?>
	</p>
	<?php }?>
	<?php if (($_smarty_tpl->tpl_vars['invitation_sent']->value || $_smarty_tpl->tpl_vars['sms_sent']->value) && isset($_smarty_tpl->tpl_vars['aior_popup']->value)) {?>
	<p class="popup">
		<?php if ($_smarty_tpl->tpl_vars['sms_sent']->value) {?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'A SMS has been sent to your friend!','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php } elseif ($_smarty_tpl->tpl_vars['nbInvitation']->value > 1) {?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Emails have been sent to your friends!','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php } else { ?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'An email has been sent to your friend!','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php }?>
	</p>
	<?php } else { ?>
		<?php if (!isset($_smarty_tpl->tpl_vars['aior_popup']->value)) {?>
	<ul class="idTabs">
		<li class="col-xs-12 col-sm-3"><a href="#idTab1" <?php if ($_smarty_tpl->tpl_vars['activeTab']->value == 'sponsor') {?>class="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sponsor my friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</a></li>
		<li class="col-xs-12 col-sm-3"><a href="#idTab2" <?php if ($_smarty_tpl->tpl_vars['activeTab']->value == 'pending') {?>class="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Pending friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</a></li>
		<li class="col-xs-12 col-sm-3"><a href="#idTab3" <?php if ($_smarty_tpl->tpl_vars['activeTab']->value == 'subscribed') {?>class="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Friends I sponsored','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</a></li>
			<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value || $_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?>
		<li class="col-xs-12 col-sm-3"><a href="#idTab4" <?php if ($_smarty_tpl->tpl_vars['activeTab']->value == 'statistics') {?>class="selected"<?php }?>><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Statistics','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</a></li>
			<?php }?>
	</ul>
	<div class="sheets">
		<div id="idTab1" class="sponsorshipBlock">
		<?php } else { ?>
		<div class="sponsorshipBlock sponsorshipPopup">
		<?php }?>

		<?php if (isset($_smarty_tpl->tpl_vars['text']->value)) {?>
			<div id="sponsorship_text" <?php if (isset($_smarty_tpl->tpl_vars['aior_popup']->value) && $_smarty_tpl->tpl_vars['afterSubmit']->value) {?>style="display: none"<?php }?>>
				<?php echo $_smarty_tpl->tpl_vars['text']->value;?>

			<?php if (isset($_smarty_tpl->tpl_vars['aior_popup']->value)) {?>
				<div align="center">
					<input id="invite" type="button" class="btn btn-primary" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Invite my friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
" />
					<input id="noinvite" type="button" class="btn btn-primary" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No, thanks','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
" />
				</div>
			<?php }?>
			</div>
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['canSendInvitations']->value || isset($_smarty_tpl->tpl_vars['aior_popup']->value)) {?>
			<div id="sponsorship_form"  <?php if (isset($_smarty_tpl->tpl_vars['aior_popup']->value) && !$_smarty_tpl->tpl_vars['afterSubmit']->value) {?>style="display: none"<?php }?>>
				<div>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sponsorship is quick and easy. You can invite your friends in different ways :','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

				<ul>
					<li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Propose your sponsorship on the social networks, by clicking the following links','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
<br>
						&nbsp;<a href="https://www.facebook.com/sharer.php?u=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link_sponsorship_fb']->value, ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Facebook','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
"><img src='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rewards_path']->value, ENT_QUOTES, 'UTF-8');?>
img/facebook.png' height='20'></a>
						&nbsp;<a href="https://twitter.com/share?url=<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link_sponsorship_twitter']->value, ENT_QUOTES, 'UTF-8');?>
" target="_blank" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Twitter','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
"><img src='<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rewards_path']->value, ENT_QUOTES, 'UTF-8');?>
img/twitter.png' height='20'></a>
					</li>
					<li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Give this sponsorship link to your friends, or post it on internet (forums, blog...)','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
<br><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link_sponsorship']->value, ENT_QUOTES, 'UTF-8');?>
</li>
					<li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Give them your mail','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['email']->value, ENT_QUOTES, 'UTF-8');?>
</b> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'or your sponsor code','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <b><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['code']->value, ENT_QUOTES, 'UTF-8');?>
</b> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'to enter in the registration form.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</li>
			<?php if ($_smarty_tpl->tpl_vars['sms']->value) {?>
					<li>
						<form id="sms_form" method="post" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_sponsorship']->value, ENT_QUOTES, 'UTF-8');?>
" style="display: inline"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Enter their mobile phone (international format) and send them a SMS','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <input id="phone" name="phone" maxlength="16" type="text" placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'e.g. +33612345678','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
" />
							<input type="image" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['rewards_path']->value, ENT_QUOTES, 'UTF-8');?>
img/sendsms.gif" id="submitSponsorSMS" name="submitSponsorSMS" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Send SMS','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Send SMS','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
" align="absmiddle" />
						</form>
					</li>
			<?php }?>
					<li><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Fill in the following form and they will receive an mail.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</li>
				</ul>
				</div>
				<div>
					<form id="list_contacts_form" method="post" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_sponsorship']->value, ENT_QUOTES, 'UTF-8');?>
">
						<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your message (optional)','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</label><br/>
						<textarea name="message" class="form-control"><?php if (isset($_smarty_tpl->tpl_vars['message']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['message']->value, ENT_QUOTES, 'UTF-8');
}?></textarea>
						<table class="table table-bordered">
							<thead class="thead-default">
								<tr>
									<th>&nbsp;</th>
									<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Last name','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
									<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'First name','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
									<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
								</tr>
							</thead>
							<tbody>
								<?php
$__section_friends_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['nbFriends']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_friends_0_start = min(0, $__section_friends_0_loop);
$__section_friends_0_total = min(($__section_friends_0_loop - $__section_friends_0_start), $__section_friends_0_loop);
$_smarty_tpl->tpl_vars['__smarty_section_friends'] = new Smarty_Variable(array());
if ($__section_friends_0_total !== 0) {
for ($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['iteration'] = 1, $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] = $__section_friends_0_start; $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['iteration'] <= $__section_friends_0_total; $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['iteration']++, $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']++){
?>
								<tr>
									<td class="align_right"><?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['iteration'] : null), ENT_QUOTES, 'UTF-8');?>
</td>
									<td><input type="text" class="form-control" name="friendsLastName[<?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null), ENT_QUOTES, 'UTF-8');?>
]" size="20" value="<?php if ((($_smarty_tpl->tpl_vars['friendsLastName']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null)] !== null ))) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['friendsLastName']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null)], ENT_QUOTES, 'UTF-8');
}?>" /></td>
									<td><input type="text" class="form-control" name="friendsFirstName[<?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null), ENT_QUOTES, 'UTF-8');?>
]" size="20" value="<?php if ((($_smarty_tpl->tpl_vars['friendsFirstName']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null)] !== null ))) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['friendsFirstName']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null)], ENT_QUOTES, 'UTF-8');
}?>" /></td>
									<td><input type="text" class="form-control" name="friendsEmail[<?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null), ENT_QUOTES, 'UTF-8');?>
]" size="20" value="<?php if ((($_smarty_tpl->tpl_vars['friendsEmail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null)] !== null ))) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['friendsEmail']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_friends']->value['index'] : null)], ENT_QUOTES, 'UTF-8');
}?>" /></td>
								</tr>
								<?php
}
}
?>
							</tbody>
						</table>
						<p>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Important: Your friends\' email addresses will only be used in the sponsorship program. They will never be used for other purposes.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

						</p>
						<p class="checkbox">
							<input class="cgv" type="checkbox" name="conditionsValided" id="conditionsValided" value="1" <?php if (isset($_POST['conditionsValided']) && $_POST['conditionsValided'] == 1) {?>checked="checked"<?php }?> />&nbsp;
							<label for="conditionsValided"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'I agree to the terms of service and adhere to them unconditionally.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</label>
							<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_sponsorship_rules']->value, ENT_QUOTES, 'UTF-8');?>
" class="fancybox rules" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Conditions of the sponsorship program','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Read conditions','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</a>
						</p>
						<p>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Preview','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_sponsorship_email']->value, ENT_QUOTES, 'UTF-8');?>
" class="fancybox mail" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Invitation email','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'the default email','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</a> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'that will be sent to your friends.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

						</p>

						<footer class="form-footer clearfix">
        					<button class="btn btn-primary" id="submitSponsorFriends" name="submitSponsorFriends" type="submit"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Send invitations','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</button>
    					</footer>
					</form>
				</div>
			</div>
		<?php } else { ?>
			<div>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'To become a sponsor, you need to have completed at least','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['orderQuantityS']->value, ENT_QUOTES, 'UTF-8');?>
 <?php if ($_smarty_tpl->tpl_vars['orderQuantityS']->value > 1) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'orders','mod'=>'allinone_rewards'),$_smarty_tpl ) );
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'order','mod'=>'allinone_rewards'),$_smarty_tpl ) );
}?>.
			</div>
		<?php }?>
		</div>

		<?php if (!isset($_smarty_tpl->tpl_vars['aior_popup']->value)) {?>
		<div id="idTab2" class="sponsorshipBlock">
			<?php if ($_smarty_tpl->tpl_vars['pendingFriends']->value && count($_smarty_tpl->tpl_vars['pendingFriends']->value) > 0) {?>
			<div>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'These friends have not yet registered on this website since you sponsored them, but you can try again! To do so, mark the checkboxes of the friend(s) you want to remind, then click on the button "Remind my friends".','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

			</div>
			<div>
				<form method="post" action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['url_sponsorship']->value, ENT_QUOTES, 'UTF-8');?>
" class="std">
					<table class="table table-bordered">
					<thead class="thead-default">
						<tr>
							<th>&nbsp;</th>
							<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Last name','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
							<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'First name','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
							<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
							<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Last invitation','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						</tr>
					</thead>
					<tbody>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['pendingFriends']->value, 'pendingFriend', false, NULL, 'myLoop', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['pendingFriend']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration']++;
?>
						<tr>
							<td><input type="checkbox" name="friendChecked[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pendingFriend']->value['id_sponsorship'], ENT_QUOTES, 'UTF-8');?>
]" id="friendChecked[<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pendingFriend']->value['id_sponsorship'], ENT_QUOTES, 'UTF-8');?>
]" value="1" /></td>
							<td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pendingFriend']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
</td>
							<td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pendingFriend']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
</td>
							<td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['pendingFriend']->value['email'], ENT_QUOTES, 'UTF-8');?>
</td>
							<td><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0], array( array('date'=>$_smarty_tpl->tpl_vars['pendingFriend']->value['date_upd'],'full'=>0),$_smarty_tpl ) );?>
</td>
						</tr>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					</tbody>
					</table>
					<footer class="form-footer clearfix">
    					<button class="btn btn-primary" id="revive" name="revive" type="submit"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Remind my friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</button>
					</footer>
				</form>
			</div>
			<?php } else { ?>
			<div>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You have not sponsored any friends.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

			</div>
			<?php }?>
		</div>

		<div id="idTab3" class="sponsorshipBlock">
			<?php if ($_smarty_tpl->tpl_vars['subscribeFriends']->value && count($_smarty_tpl->tpl_vars['subscribeFriends']->value) > 0) {?>
			<div>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Here are sponsored friends who have accepted your invitation:','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

			</div>
			<div>
				<table class="table table-bordered">
				<thead class="thead-default">
					<tr>
						<th>&nbsp;</th>
						<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Last name','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'First name','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Channel','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Inscription date','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
					</tr>
				</thead>
				<tbody>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['subscribeFriends']->value, 'subscribeFriend', false, NULL, 'myLoop', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['subscribeFriend']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration']++;
?>
					<tr class="<?php if (((isset($_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration'] : null)%2) == 0) {?>item<?php } else { ?>alternate_item<?php }?>">
						<td><?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration'] : null), ENT_QUOTES, 'UTF-8');?>
.</td>
						<td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subscribeFriend']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
</td>
						<td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subscribeFriend']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
</td>
						<td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['subscribeFriend']->value['email'], ENT_QUOTES, 'UTF-8');?>
</td>
						<td><?php if ($_smarty_tpl->tpl_vars['subscribeFriend']->value['channel'] == 1) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email invitation','mod'=>'allinone_rewards'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['subscribeFriend']->value['channel'] == 2) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sponsorship link','mod'=>'allinone_rewards'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['subscribeFriend']->value['channel'] == 3) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Facebook','mod'=>'allinone_rewards'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['subscribeFriend']->value['channel'] == 4) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Twitter','mod'=>'allinone_rewards'),$_smarty_tpl ) );
} elseif ($_smarty_tpl->tpl_vars['subscribeFriend']->value['channel'] == 5) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Google +1','mod'=>'allinone_rewards'),$_smarty_tpl ) );
}?></td>
						<td><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0], array( array('date'=>$_smarty_tpl->tpl_vars['subscribeFriend']->value['date_upd'],'full'=>0),$_smarty_tpl ) );?>
</td>
					</tr>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
				</tbody>
				</table>
			</div>
			<?php } else { ?>
			<div>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No sponsored friends have accepted your invitation yet.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

			</div>
			<?php }?>
		</div>
			<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value || $_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?>
		<div id="idTab4" class="sponsorshipBlock">
			<div class="title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Details by registration channel','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</div>
			<div>
				<table class="table table-bordered">
					<thead class="thead-default">
						<tr>
							<th colspan="2" class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Channels','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
							<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
							<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Orders','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
							<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value) {?><th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Rewards for orders','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?><th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Rewards for registrations','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th><?php }?>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="left" rowspan="5"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My direct friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
							<td class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email invitation','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['direct_nb1']), ENT_QUOTES, 'UTF-8');?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel1']), ENT_QUOTES, 'UTF-8');?>
</td>
							<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_orders1'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_registrations1'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
						</tr>
						<tr>
							<td class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sponsorship link','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['direct_nb2']), ENT_QUOTES, 'UTF-8');?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel2']), ENT_QUOTES, 'UTF-8');?>
</td>
							<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_orders2'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_registrations2'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
						</tr>
						<tr>
							<td class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Facebook','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['direct_nb3']), ENT_QUOTES, 'UTF-8');?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel3']), ENT_QUOTES, 'UTF-8');?>
</td>
							<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_orders3'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_registrations3'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
						</tr>
						<tr>
							<td class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Twitter','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['direct_nb4']), ENT_QUOTES, 'UTF-8');?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel4']), ENT_QUOTES, 'UTF-8');?>
</td>
							<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_orders4'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_registrations4'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
						</tr>
						<tr>
							<td class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Google +1','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['direct_nb5']), ENT_QUOTES, 'UTF-8');?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel5']), ENT_QUOTES, 'UTF-8');?>
</td>
							<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_orders5'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['direct_rewards_registrations5'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
						</tr>
				<?php if ($_smarty_tpl->tpl_vars['statistics']->value['maxlevel'] > 1) {?>
						<tr>
							<td class="left" colspan="2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Indirect friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['indirect_nb']), ENT_QUOTES, 'UTF-8');?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['indirect_nb_orders']), ENT_QUOTES, 'UTF-8');?>
</td>
							<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['indirect_rewards'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?><td class="right">-</td><?php }?>
						</tr>
				<?php }?>
						<tr class="total">
							<td class="left" colspan="2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Total','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['direct_nb1']+$_smarty_tpl->tpl_vars['statistics']->value['direct_nb2']+$_smarty_tpl->tpl_vars['statistics']->value['direct_nb3']+$_smarty_tpl->tpl_vars['statistics']->value['direct_nb4']+$_smarty_tpl->tpl_vars['statistics']->value['direct_nb5']+$_smarty_tpl->tpl_vars['statistics']->value['indirect_nb']), ENT_QUOTES, 'UTF-8');?>
</td>
							<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel1']+$_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel2']+$_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel3']+$_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel4']+$_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel5']+$_smarty_tpl->tpl_vars['statistics']->value['indirect_nb_orders']), ENT_QUOTES, 'UTF-8');?>
</td>
							<?php if ($_smarty_tpl->tpl_vars['reward_order_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['total_orders'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
							<?php if ($_smarty_tpl->tpl_vars['reward_registration_allowed']->value) {?><td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['total_registrations'], ENT_QUOTES, 'UTF-8');?>
</td><?php }?>
						</tr>
					</tbody>
				</table>
			</div>

				<?php if ($_smarty_tpl->tpl_vars['statistics']->value['maxlevel'] > 1 && $_smarty_tpl->tpl_vars['statistics']->value['sponsored1']) {?>
			<div class="title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Details by sponsorship level','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</div>
			<table class="table table-bordered">
				<thead class="thead-default">
					<tr>
						<th class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Level','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Orders','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Rewards','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
					</tr>
				</thead>
				<tbody>
					<?php
$__section_levels_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['statistics']->value['maxlevel']) ? count($_loop) : max(0, (int) $_loop));
$__section_levels_1_start = min(0, $__section_levels_1_loop);
$__section_levels_1_total = min(($__section_levels_1_loop - $__section_levels_1_start), $__section_levels_1_loop);
$_smarty_tpl->tpl_vars['__smarty_section_levels'] = new Smarty_Variable(array());
if ($__section_levels_1_total !== 0) {
for ($_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration'] = 1, $_smarty_tpl->tpl_vars['__smarty_section_levels']->value['index'] = $__section_levels_1_start; $_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration'] <= $__section_levels_1_total; $_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration']++, $_smarty_tpl->tpl_vars['__smarty_section_levels']->value['index']++){
?>
						<?php $_smarty_tpl->_assignInScope('indiceFriends', "nb".((string)(isset($_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration'] : null)));?>
						<?php $_smarty_tpl->_assignInScope('indiceOrders', "nb_orders".((string)(isset($_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration'] : null)));?>
						<?php $_smarty_tpl->_assignInScope('indiceRewards', "rewards".((string)(isset($_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration'] : null)));?>
					<tr>
						<td class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Level','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <?php echo htmlspecialchars((isset($_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration']) ? $_smarty_tpl->tpl_vars['__smarty_section_levels']->value['iteration'] : null), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="center"><?php if (isset($_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceFriends']->value])) {
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceFriends']->value]), ENT_QUOTES, 'UTF-8');
} else { ?>0<?php }?></td>
						<td class="center"><?php if (isset($_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceOrders']->value])) {
echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceOrders']->value]), ENT_QUOTES, 'UTF-8');
} else { ?>0<?php }?></td>
						<td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceRewards']->value], ENT_QUOTES, 'UTF-8');?>
</td>
					</tr>
					<?php
}
}
?>
					<tr class="total">
						<td class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Total','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
						<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['direct_nb1']+$_smarty_tpl->tpl_vars['statistics']->value['direct_nb2']+$_smarty_tpl->tpl_vars['statistics']->value['direct_nb3']+$_smarty_tpl->tpl_vars['statistics']->value['direct_nb4']+$_smarty_tpl->tpl_vars['statistics']->value['direct_nb5']+$_smarty_tpl->tpl_vars['statistics']->value['indirect_nb']), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel1']+$_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel2']+$_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel3']+$_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel4']+$_smarty_tpl->tpl_vars['statistics']->value['nb_orders_channel5']+$_smarty_tpl->tpl_vars['statistics']->value['indirect_nb_orders']), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['total_global'], ENT_QUOTES, 'UTF-8');?>
</td>
					</tr>
				</tbody>
			</table>
				<?php }?>

				<?php if ($_smarty_tpl->tpl_vars['statistics']->value['sponsored1']) {?>
			<div class="title"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Details for my direct friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</div>
			<table class="table table-bordered">
				<thead class="thead-default">
					<tr>
						<th class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Name','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Orders','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Rewards','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
					<?php if ($_smarty_tpl->tpl_vars['statistics']->value['maxlevel'] > 1) {?>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Friends','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Friends\' orders','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Rewards','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
						<th class="center"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Total','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</th>
					<?php }?>
					</tr>
				</thead>
				<tbody>
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['statistics']->value['sponsored1'], 'sponsored', false, NULL, 'myLoop', array (
  'iteration' => true,
));
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['sponsored']->value) {
$_smarty_tpl->tpl_vars['__smarty_foreach_myLoop']->value['iteration']++;
?>
						<?php $_smarty_tpl->_assignInScope('indiceDirect', "direct_customer".((string)$_smarty_tpl->tpl_vars['sponsored']->value['id_customer']));?>
						<?php $_smarty_tpl->_assignInScope('indiceIndirect', "indirect_customer".((string)$_smarty_tpl->tpl_vars['sponsored']->value['id_customer']));?>
						<?php if (isset($_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceDirect']->value])) {?>
							<?php $_smarty_tpl->_assignInScope('valueDirect', $_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceDirect']->value]);?>
						<?php } else { ?>
							<?php $_smarty_tpl->_assignInScope('valueDirect', 0);?>
						<?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceIndirect']->value])) {?>
							<?php $_smarty_tpl->_assignInScope('valueIndirect', $_smarty_tpl->tpl_vars['statistics']->value[$_smarty_tpl->tpl_vars['indiceIndirect']->value]);?>
						<?php } else { ?>
							<?php $_smarty_tpl->_assignInScope('valueIndirect', 0);?>
						<?php }?>
					<tr>
						<td class="left"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sponsored']->value['lastname'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sponsored']->value['firstname'], ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['sponsored']->value['direct_orders']), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sponsored']->value['direct'], ENT_QUOTES, 'UTF-8');?>
</td>
						<?php if ($_smarty_tpl->tpl_vars['statistics']->value['maxlevel'] > 1) {?>
						<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['valueDirect']->value+$_smarty_tpl->tpl_vars['valueIndirect']->value), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['sponsored']->value['indirect_orders']), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sponsored']->value['indirect'], ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="total right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sponsored']->value['total'], ENT_QUOTES, 'UTF-8');?>
</td>
						<?php }?>
					</tr>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
					<tr class="total">
						<td class="left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Total','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</td>
						<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['total_direct_orders']), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['total_direct_rewards'], ENT_QUOTES, 'UTF-8');?>
</td>
						<?php if ($_smarty_tpl->tpl_vars['statistics']->value['maxlevel'] > 1) {?>
						<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['indirect_nb']), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="center"><?php echo htmlspecialchars(intval($_smarty_tpl->tpl_vars['statistics']->value['total_indirect_orders']), ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['total_indirect_rewards'], ENT_QUOTES, 'UTF-8');?>
</td>
						<td class="right"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['statistics']->value['total_global'], ENT_QUOTES, 'UTF-8');?>
</td>
						<?php }?>
					</tr>
				</tbody>
			</table>
				<?php }?>
		</div>
			<?php }?>
	</div>
		<?php }?>
	<?php }?>
</div><!-- end /home/afrissim/public_html/modules/allinone_rewards/views/templates/front/presta-1.7/sponsorship.tpl --><?php }
}
