<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:57:09
  from '/home/afrissim/public_html/themes/jms_basel/templates/catalog/_partials/product-add-to-cart.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70fd532c433_97203149',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '82f43b9ff16b8d4a26e0de5c0117880bb3691d16' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/templates/catalog/_partials/product-add-to-cart.tpl',
      1 => 1572278201,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db70fd532c433_97203149 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<div class="product-add-to-cart">
  <?php if (isset($_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) && !$_smarty_tpl->tpl_vars['configuration']->value['is_catalog']) {?>
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9774416495db70fd52ef414_84462244', 'product_quantity');
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3871324305db70fd53274a3_30019425', 'product_minimal_quantity');
?>

  <?php }?>
</div>
<?php }
/* {block 'product_availability'} */
class Block_4446990455db70fd52fa434_04864097 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	  				  <span id="product-availability">
	            		<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Availability :','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</label>
	  					<?php if ($_smarty_tpl->tpl_vars['product']->value['show_availability'] && $_smarty_tpl->tpl_vars['product']->value['availability_message']) {?>
	  					  <?php if ($_smarty_tpl->tpl_vars['product']->value['availability'] == 'available') {?>
	  						<i class="material-icons product-available">&#xE5CA;</i>
	  					  <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['availability'] == 'last_remaining_items') {?>
	  						<i class="material-icons product-last-items">&#xE002;</i>
	  					  <?php } else { ?>
	  						<i class="material-icons product-unavailable">&#xE14B;</i>
	  					  <?php }?>
	  					  <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['availability_message'], ENT_QUOTES, 'UTF-8');?>
</span>
	  					<?php }?>
	  				  </span>
    				<?php
}
}
/* {/block 'product_availability'} */
/* {block 'product_quantities'} */
class Block_8477058775db70fd53010b8_20235810 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

              <?php if ($_smarty_tpl->tpl_vars['product']->value['show_quantities']) {?>
                <div class="product-quantities">
                  <label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Quantities :','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</label>
                  <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity_label'], ENT_QUOTES, 'UTF-8');?>
</span>
                </div>
              <?php }?>
            <?php
}
}
/* {/block 'product_quantities'} */
/* {block 'product_availability'} */
class Block_9097386145db70fd5311a23_50543259 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

	  				  <span id="product-availability">
	            		<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Availability :','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</label>
	  					<?php if ($_smarty_tpl->tpl_vars['product']->value['show_availability'] && $_smarty_tpl->tpl_vars['product']->value['availability_message']) {?>
	  					  <?php if ($_smarty_tpl->tpl_vars['product']->value['availability'] == 'available') {?>
	  						<i class="material-icons product-available">&#xE5CA;</i>
	  					  <?php } elseif ($_smarty_tpl->tpl_vars['product']->value['availability'] == 'last_remaining_items') {?>
	  						<i class="material-icons product-last-items">&#xE002;</i>
	  					  <?php } else { ?>
	  						<i class="material-icons product-unavailable">&#xE14B;</i>
	  					  <?php }?>
	  					  <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['availability_message'], ENT_QUOTES, 'UTF-8');?>
</span>
	  					<?php }?>
	  				  </span>
    				<?php
}
}
/* {/block 'product_availability'} */
/* {block 'product_quantities'} */
class Block_18767799525db70fd53205f1_26482129 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

              <?php if ($_smarty_tpl->tpl_vars['product']->value['show_quantities']) {?>
                <div class="product-quantities">
                  <label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Quantities :','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</label>
                  <span><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity'], ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity_label'], ENT_QUOTES, 'UTF-8');?>
</span>
                </div>
              <?php }?>
            <?php
}
}
/* {/block 'product_quantities'} */
/* {block 'product_quantity'} */
class Block_9774416495db70fd52ef414_84462244 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_quantity' => 
  array (
    0 => 'Block_9774416495db70fd52ef414_84462244',
  ),
  'product_availability' => 
  array (
    0 => 'Block_4446990455db70fd52fa434_04864097',
    1 => 'Block_9097386145db70fd5311a23_50543259',
  ),
  'product_quantities' => 
  array (
    0 => 'Block_8477058775db70fd53010b8_20235810',
    1 => 'Block_18767799525db70fd53205f1_26482129',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <div class="product-quantity">
			<div class="qty">
			  <input
				type="text"
				name="qty"
				id="quantity_wanted"
				value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['quantity_wanted'], ENT_QUOTES, 'UTF-8');?>
"
				class="input-group"
				min="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['minimal_quantity'], ENT_QUOTES, 'UTF-8');?>
"
			  />
			</div>
        <div class="add">
			
				<button class="add-to-cart btn" data-button-action="add-to-cart" type="submit" <?php if (!$_smarty_tpl->tpl_vars['product']->value['add_to_cart_url']) {?>disabled<?php }?>>
					<span class="fa fa-check icon" aria-hidden="true"></span>
					<span class="text-addcart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
	</span>		
					<span class="text-outofstock"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sold out','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</span>
				</button>
					
				<ul class="other-info">
						<?php if ($_smarty_tpl->tpl_vars['product']->value['reference']) {?>
							<!-- number of item in stock -->
	  						<li id="product_reference">
	  							<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Code:','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</label>
	  							<span class="editable"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['reference'], ENT_QUOTES, 'UTF-8');?>
</span>
	  						</li>
						<?php }?>
						<li>
  						<?php if ($_smarty_tpl->tpl_vars['product']->value['additional_shipping_cost'] > 0) {?>
  							<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Shipping tax: '),$_smarty_tpl ) );?>
</label>
  								<span class="shipping_cost"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['additional_shipping_cost'], ENT_QUOTES, 'UTF-8');?>
</span>
  						<?php } else { ?>
  							<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Shippingtax:','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</label><span class="shipping_cost"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>' Free','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</span>
  						<?php }?>
					   </li>
					</ul>
					<div class="availability_block">
    				<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4446990455db70fd52fa434_04864097', 'product_availability', $this->tplIndex);
?>

    				<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_8477058775db70fd53010b8_20235810', 'product_quantities', $this->tplIndex);
?>

			   </div>
			
        </div>
      </div>
	      <div class="row" style="margin-top:10px;">
	     		<a href="https://wa.me/2289262694?text=<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Bonjour Afrissime, je voudrais acheter ce produit *','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );
echo htmlspecialchars($_smarty_tpl->tpl_vars['page']->value['meta']['title'], ENT_QUOTES, 'UTF-8');?>
" target="_blank" class="buyWhatsapp_btn"><span class="fa fa-whatsapp"> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Acheter sur Whatsapp','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>
</span></a>
	      </div>
      
    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayProductButtons','product'=>$_smarty_tpl->tpl_vars['product']->value),$_smarty_tpl ) );?>

    <?php if (isset($_smarty_tpl->tpl_vars['jpb_wishlist']->value) && $_smarty_tpl->tpl_vars['jpb_wishlist']->value) {?>
    	<div class="action-box">
	        <a class="addToWishlist product-btn"  onclick="WishlistCart('wishlist_block_list', 'add', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['id_product'],'html' )), ENT_QUOTES, 'UTF-8');?>
', false, 1); return false;" data-id-product="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product']->value['id_product'],'html' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to Wishlist'),$_smarty_tpl ) );?>
">
	          <i class="fa fa-heart-o" aria-hidden="true"></i>
	          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to wishlist','d'=>'Shop.Theme.Global'),$_smarty_tpl ) );?>

	        </a>
    	</div>
    <?php }?>
		<div class="add-bottom">
        	<div class="add">
			
				<button class="btn-effect add-to-cart btn-default btn-active" data-button-action="add-to-cart" type="submit" <?php if (!$_smarty_tpl->tpl_vars['product']->value['add_to_cart_url']) {?>disabled<?php }?>>
					  <span class="text-addcart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add to cart','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
	</span>		
					  <span class="text-outofstock"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sold out','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</span>
				</button>
				<button class="btn-effect add-to-cart btn-default btn-active" data-button-action="add-to-cart" type="submit" <?php if (!$_smarty_tpl->tpl_vars['product']->value['add_to_cart_url']) {?>disabled<?php }?>>
					  <span class="text-addcart"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Acheter ce produit','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
	</span>		
					  <span class="text-outofstock"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sold out','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>
</span>
				</button>
			
					<ul class="other-info">
						<?php if ($_smarty_tpl->tpl_vars['product']->value['reference']) {?>
							<!-- number of item in stock -->
  						<li id="product_reference">
  							<label><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Code:','d'=>'Shop.Theme.Catalog'),$_smarty_tpl ) );?>
</label>
  							<span class="editable"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['product']->value['reference'], ENT_QUOTES, 'UTF-8');?>
</span>
  						</li>
						<?php }?>
						
					</ul>
					<div class="availability_block">
    				<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_9097386145db70fd5311a23_50543259', 'product_availability', $this->tplIndex);
?>

    				<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18767799525db70fd53205f1_26482129', 'product_quantities', $this->tplIndex);
?>

			   </div>
			
        </div>
        </div>
      <div class="clearfix"></div>
    <?php
}
}
/* {/block 'product_quantity'} */
/* {block 'product_minimal_quantity'} */
class Block_3871324305db70fd53274a3_30019425 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'product_minimal_quantity' => 
  array (
    0 => 'Block_3871324305db70fd53274a3_30019425',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <p class="product-minimal-quantity">
        <?php if ($_smarty_tpl->tpl_vars['product']->value['minimal_quantity'] > 1) {?>
          <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The minimum purchase order quantity for the product is %quantity%.','d'=>'Shop.Theme.Checkout','sprintf'=>array('%quantity%'=>$_smarty_tpl->tpl_vars['product']->value['minimal_quantity'])),$_smarty_tpl ) );?>

        <?php }?>
      </p>
    <?php
}
}
/* {/block 'product_minimal_quantity'} */
}
