<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:13:17
  from '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/presta-1.7/product.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db7058d851355_66361270',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'e10d015aeda889bb863b2a61dc3e9b86d5e246e3' => 
    array (
      0 => '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/presta-1.7/product.tpl',
      1 => 1571236966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db7058d851355_66361270 (Smarty_Internal_Template $_smarty_tpl) {
if (!isset($_smarty_tpl->tpl_vars['ajax_loyalty']->value)) {?>
<!-- MODULE allinone_rewards -->
<?php echo '<script'; ?>
 type="text/javascript">
//<![CDATA[
	var url_allinone_loyalty = "<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'module','name'=>'allinone_rewards','controller'=>'loyalty'),$_smarty_tpl ) );?>
";
//]]>
<?php echo '</script'; ?>
>
<div id="loyalty" class="align_justify"></div>
<!-- END : MODULE allinone_rewards -->
<?php } else { ?>
	<?php if ($_smarty_tpl->tpl_vars['display_credits']->value) {?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Buying this product you will collect ','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <b><span id="loyalty_credits"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['credits']->value, ENT_QUOTES, 'UTF-8');?>
</span></b> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>' with our loyalty program.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your cart will total','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <b><span id="total_loyalty_credits"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['total_credits']->value, ENT_QUOTES, 'UTF-8');?>
</span></b> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'that can be converted into a voucher for a future purchase.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

	<?php } else { ?>
		<?php if (isset($_smarty_tpl->tpl_vars['no_pts_discounted']->value) && $_smarty_tpl->tpl_vars['no_pts_discounted']->value == 1) {?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No reward credits for this product because there\'s already a discount.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php } else { ?>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your basket must contain at least %s of products in order to get loyalty rewards.','sprintf'=>array($_smarty_tpl->tpl_vars['minimum']->value),'mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

		<?php }?>
	<?php }
}
}
}
