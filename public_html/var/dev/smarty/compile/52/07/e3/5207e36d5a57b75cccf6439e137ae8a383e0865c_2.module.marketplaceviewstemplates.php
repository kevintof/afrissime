<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:14:30
  from 'module:marketplaceviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db705d6967e88_16894594',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5207e36d5a57b75cccf6439e137ae8a383e0865c' => 
    array (
      0 => 'module:marketplaceviewstemplates',
      1 => 1567446696,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db705d6967e88_16894594 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/afrissim/public_html/modules/marketplace/views/templates/hook/mpmyaccountmenu.tpl -->
<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayMpMyAccountMenuTop"),$_smarty_tpl ) );?>

<h1 style="text-align: center;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your Shop','mod'=>'marketplace'),$_smarty_tpl ) );?>
</h1>
<p style="text-align: center" class="info-account"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Here you can manage marketplace shop.','mod'=>'marketplace'),$_smarty_tpl ) );?>
</p>
<?php if ($_smarty_tpl->tpl_vars['is_seller']->value == 1) {?>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Dashboard','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php if (isset($_smarty_tpl->tpl_vars['dashboard_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['dashboard_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','dashboard'), ENT_QUOTES, 'UTF-8');
}?>">
		<span class="link-item">
			<i class="material-icons">&#xE871;</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Dashboard','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Edit Profile','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php if (isset($_smarty_tpl->tpl_vars['edit_profile_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['edit_profile_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','editprofile'), ENT_QUOTES, 'UTF-8');
}?>">
		<span class="link-item">
			<i class="material-icons">&#xE254;</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Edit Profile','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Seller Profile','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php if (isset($_smarty_tpl->tpl_vars['seller_profile_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['seller_profile_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','sellerprofile',array('mp_shop_name'=>$_smarty_tpl->tpl_vars['name_shop']->value)), ENT_QUOTES, 'UTF-8');
}?>">
		<span class="link-item">
			<i class="material-icons">&#xE851;</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Seller Profile','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View Shop','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php if (isset($_smarty_tpl->tpl_vars['shop_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['shop_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','shopstore',array('mp_shop_name'=>$_smarty_tpl->tpl_vars['name_shop']->value)), ENT_QUOTES, 'UTF-8');
}?>">
		<span class="link-item">
			<i class="material-icons">&#xE8D1;</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Shop','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product List','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php if (isset($_smarty_tpl->tpl_vars['product_list_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product_list_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','productlist'), ENT_QUOTES, 'UTF-8');
}?>">
		<span class="link-item">
			<i class="material-icons">&#xE149;</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Orders','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php if (isset($_smarty_tpl->tpl_vars['my_order_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['my_order_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','mporder'), ENT_QUOTES, 'UTF-8');
}?>">
		<span class="link-item">
			<i class="material-icons">&#xE8F6;</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Orders','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Transaction','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php if (isset($_smarty_tpl->tpl_vars['my_transaction_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['my_transaction_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','mptransaction'), ENT_QUOTES, 'UTF-8');
}?>">
		<span class="link-item">
			<i class="material-icons">swap_horiz</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Transaction','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Payment Detail','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php if (isset($_smarty_tpl->tpl_vars['payment_detail_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['payment_detail_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','mppayment'), ENT_QUOTES, 'UTF-8');
}?>">
		<span class="link-item">
			<i class="material-icons">&#xE8A1;</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Payment Detail','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<?php if (Configuration::get('WK_MP_PRESTA_ATTRIBUTE_ACCESS')) {?>
		<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','productattribute'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Attributes','mod'=>'marketplace'),$_smarty_tpl ) );?>
">
			<span class="link-item">
				<i class="material-icons">&#xE839;</i>
				<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Attributes','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span>
			</span>
		</a>
	<?php }?>
	<?php if (Configuration::get('WK_MP_PRESTA_FEATURE_ACCESS')) {?>
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','productfeature'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Features','mod'=>'marketplace'),$_smarty_tpl ) );?>
" class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
			<span class="link-item">
				<i class="material-icons">&#xE8D0;</i>
				<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Features','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span>
			</span>
		</a>
	<?php }?>
	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayMpMyAccountMenuActiveSeller"),$_smarty_tpl ) );?>

<?php } elseif ($_smarty_tpl->tpl_vars['is_seller']->value == 0) {?>
	<?php if (isset($_smarty_tpl->tpl_vars['mpSellerShopSettings']->value) && $_smarty_tpl->tpl_vars['mpSellerShopSettings']->value && $_smarty_tpl->tpl_vars['shop_approved']->value) {?>
		<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Re-Activate Your Shop','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','editprofile',array('reactivate'=>1)), ENT_QUOTES, 'UTF-8');?>
">
			<span class="link-item">
				<i class="material-icons">&#xE871;</i>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Re-Activate Your Shop','mod'=>'marketplace'),$_smarty_tpl ) );?>

			</span>
		</a>
	<?php } else { ?>
		<div class="col-md-12">
			<div class="alert alert-info" role="alert">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your request has been already sent to admin. Please wait for admin approval','mod'=>'marketplace'),$_smarty_tpl ) );?>

			</div>
		</div>
	<?php }?>
	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayMpMyAccountMenuInactiveSeller"),$_smarty_tpl ) );?>

<?php } elseif ($_smarty_tpl->tpl_vars['is_seller']->value == -1) {?>
	<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click Here for Seller Request','mod'=>'marketplace'),$_smarty_tpl ) );?>
" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','sellerrequest'), ENT_QUOTES, 'UTF-8');?>
">
		<span class="link-item">
			<i class="material-icons">&#xE15E;</i>
			<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Click Here for Seller Request','mod'=>'marketplace'),$_smarty_tpl ) );?>

		</span>
	</a>
	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayMpMyAccountMenuSellerRequest"),$_smarty_tpl ) );?>

<?php }?>
<!-- end /home/afrissim/public_html/modules/marketplace/views/templates/hook/mpmyaccountmenu.tpl --><?php }
}
