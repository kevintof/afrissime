<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:16:01
  from '/home/afrissim/public_html/themes/jms_basel/modules/jmspagebuilder/views/templates/hook/addonspace.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f821838bf3_10894895',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a588802061fd3fa708b1de59a70ff29368dd645e' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/modules/jmspagebuilder/views/templates/hook/addonspace.tpl',
      1 => 1566831866,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6f821838bf3_10894895 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="jms-empty-space clearfix<?php if (isset($_smarty_tpl->tpl_vars['space_class']->value) && $_smarty_tpl->tpl_vars['space_class']->value) {?> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['space_class']->value, ENT_QUOTES, 'UTF-8');
}?>" style="margin-bottom:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['spacegap']->value, ENT_QUOTES, 'UTF-8');?>
px;"></div>
<?php }
}
