<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:57:09
  from '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/presta-1.7/product-sponsorship.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70fd5418f76_03153072',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a5d3da0aa4b22b00d4c34e23e4142fc972222111' => 
    array (
      0 => '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/presta-1.7/product-sponsorship.tpl',
      1 => 1571236966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db70fd5418f76_03153072 (Smarty_Internal_Template $_smarty_tpl) {
?><div id="sponsorship_link">
	<a class="fancybox.inline" href="#sponsorship_product"><i class="material-icons">&#xE80D;</i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sponsor for this product','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</a>
</div>
<div style="display:none">
	<div id="sponsorship_product">
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You can share the URL of this product with your sponsorship included.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
<br/>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Just copy / paste the following link :','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
<br/><br/>
		<span id="link_to_share"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['sponsorship_link']->value, ENT_QUOTES, 'UTF-8');?>
</span><br><br>
		<button class="btn btn-primary" data-clipboard-target="#link_to_share"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Copy to clipboard','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</button>
	</div>
</div><?php }
}
