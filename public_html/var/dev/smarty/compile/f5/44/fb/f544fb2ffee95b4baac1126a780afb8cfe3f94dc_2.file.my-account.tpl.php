<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:14:30
  from '/home/afrissim/public_html/themes/jms_basel/modules/jmswishlist/views/templates/hook/my-account.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db705d687bf85_69206760',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'f544fb2ffee95b4baac1126a780afb8cfe3f94dc' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/modules/jmswishlist/views/templates/hook/my-account.tpl',
      1 => 1566831868,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db705d687bf85_69206760 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- MODULE WishList -->
<a class=" wishlist_top lnk_wishlist col-lg-4 col-md-6 col-sm-6 col-xs-12" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('jmswishlist','mywishlist',array(),true),'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My wishlists','d'=>'Shop.Theme'),$_smarty_tpl ) );?>
">
  <span class="link-item">
		<i class="fa fa-heart"></i>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My wishlists','d'=>'Shop.Theme.CustomerAccount'),$_smarty_tpl ) );?>

	</span>
</a>
<!-- END : MODULE WishList --><?php }
}
