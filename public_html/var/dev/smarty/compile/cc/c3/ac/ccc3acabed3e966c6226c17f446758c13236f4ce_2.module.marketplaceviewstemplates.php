<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:57:05
  from 'module:marketplaceviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70fd1e4dca2_26413552',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ccc3acabed3e966c6226c17f446758c13236f4ce' => 
    array (
      0 => 'module:marketplaceviewstemplates',
      1 => 1567446696,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db70fd1e4dca2_26413552 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/afrissim/public_html/modules/marketplace/views/templates/hook/mpmenu.tpl -->
<div class="wk_menu_item">
	<?php if ($_smarty_tpl->tpl_vars['is_seller']->value == 1) {?>
		<div class="list_content">
			<ul>
				<li><span class="menutitle"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Marketplace','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span></li>
				<li <?php if ($_smarty_tpl->tpl_vars['logic']->value == 1) {?>class="menu_active"<?php }?>>
					<span>
						<a href="<?php if (isset($_smarty_tpl->tpl_vars['dashboard_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['dashboard_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','dashboard')), ENT_QUOTES, 'UTF-8');
}?>">
							<i class="material-icons">&#xE871;</i>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Dashboard','mod'=>'marketplace'),$_smarty_tpl ) );?>

						</a>
					</span>
				</li>
				<li <?php if ($_smarty_tpl->tpl_vars['logic']->value == 2) {?>class="menu_active"<?php }?>>
					<span>
						<a href="<?php if (isset($_smarty_tpl->tpl_vars['edit_profile_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['edit_profile_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','editprofile')), ENT_QUOTES, 'UTF-8');
}?>">
							<i class="material-icons">&#xE254;</i>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Edit Profile','mod'=>'marketplace'),$_smarty_tpl ) );?>

						</a>
					</span>
				</li>
				<li>
					<span>
						<a href="<?php if (isset($_smarty_tpl->tpl_vars['seller_profile_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['seller_profile_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','sellerprofile',array('mp_shop_name'=>$_smarty_tpl->tpl_vars['name_shop']->value))), ENT_QUOTES, 'UTF-8');
}?>">
							<i class="material-icons">&#xE851;</i>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Seller Profile','mod'=>'marketplace'),$_smarty_tpl ) );?>

						</a>
					</span>
				</li>
				<li>
					<span>
						<a href="<?php if (isset($_smarty_tpl->tpl_vars['shop_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['shop_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','shopstore',array('mp_shop_name'=>$_smarty_tpl->tpl_vars['name_shop']->value))), ENT_QUOTES, 'UTF-8');
}?>">
							<i class="material-icons">&#xE8D1;</i>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Shop','mod'=>'marketplace'),$_smarty_tpl ) );?>

						</a>
					</span>
				</li>
				<li <?php if ($_smarty_tpl->tpl_vars['logic']->value == 3) {?>class="menu_active"<?php }?>>
					<span>
						<a href="<?php if (isset($_smarty_tpl->tpl_vars['product_list_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['product_list_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','productlist')), ENT_QUOTES, 'UTF-8');
}?>">
							<i class="material-icons">&#xE149;</i>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product','mod'=>'marketplace'),$_smarty_tpl ) );?>

							<span class="wkbadge-primary" style="float:right;"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['totalSellerProducts']->value, ENT_QUOTES, 'UTF-8');?>
</span>
							<div class="clearfix"></div>
						</a>
					</span>
				</li>
				<li <?php if ($_smarty_tpl->tpl_vars['logic']->value == 4) {?>class="menu_active"<?php }?>>
					<span>
						<a href="<?php if (isset($_smarty_tpl->tpl_vars['my_order_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['my_order_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','mporder')), ENT_QUOTES, 'UTF-8');
}?>">
							<i class="material-icons">&#xE8F6;</i>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Orders','mod'=>'marketplace'),$_smarty_tpl ) );?>

						</a>
					</span>
				</li>
				<li <?php if ($_smarty_tpl->tpl_vars['logic']->value == 5) {?>class="menu_active"<?php }?>>
					<span>
						<a href="<?php if (isset($_smarty_tpl->tpl_vars['my_transaction_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['my_transaction_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','mptransaction')), ENT_QUOTES, 'UTF-8');
}?>">
							<i class="material-icons">swap_horiz</i>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Transaction','mod'=>'marketplace'),$_smarty_tpl ) );?>

						</a>
					</span>
				</li>
				<li <?php if ($_smarty_tpl->tpl_vars['logic']->value == 6) {?>class="menu_active"<?php }?>>
					<span>
						<a href="<?php if (isset($_smarty_tpl->tpl_vars['payment_detail_link']->value)) {
echo htmlspecialchars($_smarty_tpl->tpl_vars['payment_detail_link']->value, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(addslashes($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','mppayment')), ENT_QUOTES, 'UTF-8');
}?>">
							<i class="material-icons">&#xE8A1;</i>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Payment Detail','mod'=>'marketplace'),$_smarty_tpl ) );?>

						</a>
					</span>
				</li>
				<?php if (Configuration::get('WK_MP_PRESTA_ATTRIBUTE_ACCESS')) {?>
					<li <?php if ($_smarty_tpl->tpl_vars['logic']->value == 'mp_prod_attribute') {?>class="menu_active"<?php }?>>
						<span>
							<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','productattribute'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Attributes','mod'=>'marketplace'),$_smarty_tpl ) );?>
">
								<i class="material-icons">&#xE839;</i>
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Attributes','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</a>
						</span>
					</li>
				<?php }?>
				<?php if (Configuration::get('WK_MP_PRESTA_FEATURE_ACCESS')) {?>
					<li <?php if ($_smarty_tpl->tpl_vars['logic']->value == 'mp_prod_features') {?>class="menu_active"<?php }?>>
						<span>
							<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','productfeature'), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Features','mod'=>'marketplace'),$_smarty_tpl ) );?>
">
								<i class="material-icons">&#xE8D0;</i>
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Product Features','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</a>
						</span>
					</li>
				<?php }?>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayMPMenuBottom"),$_smarty_tpl ) );?>

			</ul>
		</div>
	<?php } else { ?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayMPStaffMenu"),$_smarty_tpl ) );?>

	<?php }?>
</div><!-- end /home/afrissim/public_html/modules/marketplace/views/templates/hook/mpmenu.tpl --><?php }
}
