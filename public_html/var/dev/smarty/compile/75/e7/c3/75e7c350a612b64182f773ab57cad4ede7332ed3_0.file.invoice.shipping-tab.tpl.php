<?php
/* Smarty version 3.1.33, created on 2019-10-28 16:02:57
  from '/home/afrissim/public_html/pdf/invoice.shipping-tab.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db71131e2ba98_59638230',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '75e7c350a612b64182f773ab57cad4ede7332ed3' => 
    array (
      0 => '/home/afrissim/public_html/pdf/invoice.shipping-tab.tpl',
      1 => 1566825292,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db71131e2ba98_59638230 (Smarty_Internal_Template $_smarty_tpl) {
?><table id="shipping-tab" width="100%">
	<tr>
		<td class="shipping center small grey bold" width="44%"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Carrier','d'=>'Shop.Pdf','pdf'=>'true'),$_smarty_tpl ) );?>
</td>
		<td class="shipping center small white" width="56%"><?php echo $_smarty_tpl->tpl_vars['carrier']->value->name;?>
</td>
	</tr>
</table>
<?php }
}
