<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:16:17
  from '/home/afrissim/public_html/themes/jms_basel/modules/jmsvermegamenu/views/templates/hook/jmsvermegamenu.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f831778dd2_67644375',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4ccd436420507ea2751f5cba388501650aab8fa2' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/modules/jmsvermegamenu/views/templates/hook/jmsvermegamenu.tpl',
      1 => 1567420892,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6f831778dd2_67644375 (Smarty_Internal_Template $_smarty_tpl) {
?>




<div class="ver-menu-wrapper">

	<div class="ver-title">

		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Categories','d'=>'Modules.JmsVermegamenu'),$_smarty_tpl ) );?>



	</div>

	<div class="ver-content">

		<?php echo $_smarty_tpl->tpl_vars['vermenu_html']->value;?>


	</div>

</div>

<?php echo '<script'; ?>
 type="text/javascript">

	var jmvmm_event = '<?php echo $_smarty_tpl->tpl_vars['JMSVMM_MOUSEEVENT']->value;?>
';

	var jmvmm_duration = '<?php echo $_smarty_tpl->tpl_vars['JMSVMM_DURATION']->value;?>
';	

<?php echo '</script'; ?>
>

<style type="text/css">

.jms-vermegamenu .dropdown-menu {    

	transition:all <?php echo $_smarty_tpl->tpl_vars['JMSVMM_DURATION']->value;?>
ms;

}

</style><?php }
}
