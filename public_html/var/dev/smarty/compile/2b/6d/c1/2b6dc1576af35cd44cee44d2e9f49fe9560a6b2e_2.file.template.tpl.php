<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:16:16
  from '/home/afrissim/public_html/modules/baproductzoommagnifier/views/templates/front/template.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f83082a7d9_27257610',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2b6dc1576af35cd44cee44d2e9f49fe9560a6b2e' => 
    array (
      0 => '/home/afrissim/public_html/modules/baproductzoommagnifier/views/templates/front/template.tpl',
      1 => 1568829838,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6f83082a7d9_27257610 (Smarty_Internal_Template $_smarty_tpl) {
echo '<script'; ?>
>   
	var linkjq = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['jquery']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
	
	if (!window.jQuery){
		var jq = document.createElement("script");
		jq.type = "text/javascript";
		jq.src = linkjq;
		document.getElementsByTagName('head')[0].appendChild(jq);
		console.log("Added jQuery!");
	} else {
		console.log("jQuery already exists.");
	}
	window.onload = function(){
		$('body').append('<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['zoomple']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" type="text/javascript" charset="utf-8">');
		var ba1 = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['zoomfix']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
		var ba2 = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['zoomfixs']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
		var ba3 = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['zoomfixss']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
		if (ba1 != '') {
			$('body').append('<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['zoomfix']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" type="text/javascript" charset="utf-8">');
		}
		if (ba2 != '') {
			$('body').append('<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['zoomfixs']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" type="text/javascript" charset="utf-8">');
		}
		if (ba3 != '') {
			$('body').append('<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['zoomfixss']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" type="text/javascript" charset="utf-8">');
		}
	}
<?php echo '</script'; ?>
>

<style type="text/css" media="screen">
	#index ul.product_list.tab-pane > li,ul.product_list.grid > li{
		height: auto !important;
	}
	.image_wrap img{
 		width:<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['width_img']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
px !important;
 		height:<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['height_img']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
px !important;

	}
	.image_wrap>img,#zoomple_image_overlay{
		max-width: none !important;
	}
	.magnify > .magnify-lens{
		box-shadow: 0 0 20px 4px #000;
	}
	.caption-wrap{
		display: none;
	}
	.image_wrap{
		opacity: <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['opacity']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
	}
	
	.container1{
  max-width:500px;
  position:relative;
  border:solid;
  font-size:0;
  overflow:hidden;
  }
.origin{
  width:100%;
}

.zoom{
  width:140px;
  height:140px;
  border-radius:50%;
  position:absolute;
  top:0;z-index:2;
  border:solid 4px #ccc;
  z-index: 9999;

}
.span_link{
	display: none !important;
}
</style>
<?php echo '<script'; ?>
 type="text/javascript">
	var linkurl = "<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['sass']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
	if (navigator.appVersion.indexOf("Win")!=-1){
		
		var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));
		if(isSafari == true ){
			$('body').append(linkurl + '/views/css/safaw.css');
		}
		
	}
<?php echo '</script'; ?>
><?php }
}
