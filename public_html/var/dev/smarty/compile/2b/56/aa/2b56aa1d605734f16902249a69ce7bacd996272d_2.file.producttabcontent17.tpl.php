<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:13:18
  from '/home/afrissim/public_html/modules/spmgsnipreview/views/templates/hooks/producttabcontent17.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db7058e452013_82698557',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2b56aa1d605734f16902249a69ce7bacd996272d' => 
    array (
      0 => '/home/afrissim/public_html/modules/spmgsnipreview/views/templates/hooks/producttabcontent17.tpl',
      1 => 1567445957,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'module:spmgsnipreview/views/templates/front/list_reviews.tpl' => 1,
  ),
),false)) {
function content_5db7058e452013_82698557 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['spmgsnipreviewptabs_type']->value == 1) {?>

    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrvis_on']->value == 1) {?>

        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1) {?>

            <h3 class="page-product-heading" id="#idTab777"><img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="title-rating-one-star" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reviews','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
" />&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reviews','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 <span id="count-review-tab">(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)</span></h3>

        <?php }?>

    <?php }?>

<?php }?>



<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrvis_on']->value == 1) {?>

    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1) {?>

        <div id="idTab777" <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>class="block-categories <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewptabs_type']->value == 3) {?>tab-pane fade in<?php }?>"<?php } else { ?>class="tab-pane"<?php }?>>


            <!-- reviews template -->

            <div id="shopify-product-reviews">

                <div class="spr-container row-custom">




                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_add']->value == 0) {?>
                        <div class="text-align-center margin-bottom-20">
                            <span class="spr-summary-actions">

                              <a class="btn-spmgsnipreview btn-primary-spmgsnipreview" href="javascript:void(0)"  onclick="show_form_review(1)">
                                <span>
                                    <i class="icon-pencil"></i>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Write a Review','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                </span>
                              </a>

                            </span>
                        </div>
                    <?php }?>


                    <div class="spr-content col-sm-12-custom" id="spr-content">




                        
                        <?php echo '<script'; ?>
 type="text/javascript">
                            var module_dir = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['module_dir']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
                            var spmgsnipreview_star_active = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
                            var spmgsnipreview_star_noactive = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
                            var spmgsnipreview_star_half_active = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestarh']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';

                            var spmgsnipreview_min_star_par = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewmin_star_par']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
                            var spmgsnipreview_max_star_par = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewmax_star_par']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
                        <?php echo '</script'; ?>
>
                        



                        
                        <?php echo '<script'; ?>
 type="text/javascript">
                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>document.addEventListener("DOMContentLoaded", function(event) { <?php }?>
                                jQuery(document).ready(init_rating);

                                $("#idTab777-my-click").click(function() {
                                    $('.total-info-tool-product-page .btn-spmgsnipreview').parent().hide();
                                });

                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>}); <?php }?>






                        <?php echo '</script'; ?>
>
                        


                        <div id="add-review-block" style="display: none">
                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewwhocanadd']->value == 'reg') {?>
                                <div class="no-registered">
                                    <div class="text-no-reg">
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You cannot post a review because you are not logged as a customer','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                    </div>
                                    <br/>
                                    <div class="no-reg-button">
                                        <a href="<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewm_acc']->value;?>
"
                                           class="btn-spmgsnipreview btn-primary-spmgsnipreview" ><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Log in / sign up','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</a>
                                    </div>

                                </div>
                                                        <?php } else { ?>

                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_add']->value == 1) {?>

                                <div class="advertise-text-review">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'You have already add review for this product','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                </div>

                            <?php } else { ?>


                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewwhocanadd']->value == 'buy') {?>

                                <div class="text-no-reg alert alert-warning">
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Only customers that have purchased a product can give a review.','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                </div>
                            <?php }?>


                                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewvis_on']->value && $_smarty_tpl->tpl_vars['spmgsnipreviewis_show_voucher']->value == 1) {?>
                                <div class="advertise-text-review">
                                <span>
                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                         alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Write a review and get voucher for discount','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
" />
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Write a review and get voucher for discount','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                    <b><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewdiscount']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b> <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewvaluta']->value != '%') {?><b>(<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtax']->value == 1) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Tax Included','mod'=>'spmgsnipreview'),$_smarty_tpl ) );
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Tax Excluded','mod'=>'spmgsnipreview'),$_smarty_tpl ) );
}?>)</b><?php }?>
                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_show_min']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewisminamount']->value) {?>
                                        <b>(<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Minimum amount','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 : <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewminamount']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewcurtxt']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)</b>
                                    <?php }?>
                                    ,
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'valid for','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewsdvvalid']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewdays']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                                </span>
                                </div>
                            <?php }?>


                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewvis_onfb']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis_show_fb_voucher']->value == 1) {?>
                            <br/>
                                <div class="advertise-text-review" id="facebook-share-review-block">
                                <span>
                                    <img width="16" height="16" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/btn/ico-facebook.png"
                                         alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Share your review on Facebook and get voucher for discount','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
"/>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Share your review on Facebook and get voucher for discount','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                    <b><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewdiscountfb']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b> <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewvalutafb']->value != '%') {?><b>(<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtaxfb']->value == 1) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Tax Included','mod'=>'spmgsnipreview'),$_smarty_tpl ) );
} else {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Tax Excluded','mod'=>'spmgsnipreview'),$_smarty_tpl ) );
}?>)</b><?php }?>

                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_show_minfb']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewisminamountfb']->value) {?>
                                        <b>(<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Minimum amount','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 : <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewminamountfb']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewcurtxtfb']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)</b>
                                    <?php }?>

                                    ,
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'valid for','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewsdvvalidfb']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewdaysfb']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                                </span>
                                </div>
                            <?php }?>
                                







                                                                <div id="add-review-form-review">
                                    <form method="post" action="<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewupload']->value;?>
" enctype="multipart/form-data"
                                          id="add_review_item_form" name="add_review_item_form">

                                        <input type="hidden" name="action" value="add" />
                                        <input type="hidden" name="id_product" id="id_product_spmgsnipreview" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <input type="hidden" name="id_category_spmgsnipreview" id="id_category_spmgsnipreview" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_category']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <input type="hidden" name="id_customer" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />


                                        <div class="title-rev">
                                            <div class="title-form-text-left">
                                                <b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Write Your Review','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</b>
                                            </div>

                                            <input type="button" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'close','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
" class="btn-spmgsnipreview btn-primary-spmgsnipreview title-form-text-right" onclick="show_form_review(0)">
                                            <div class="clear-spmgsnipreview"></div>
                                        </div>

                                        <div id="body-add-review-form-review">


                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1) {?>
                                                <br/>

                                                <?php if (count($_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value) > 0) {?>

                                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value, 'criterion');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->value) {
?>

                                                        <label for="rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                               class="float-left"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
<sup class="required">*</sup></label>

                                                        <div class="rat rating-stars-dynamic">
                                                        <span onmouseout="read_rating_review_shop('rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');">

                                                            <img  onmouseover="_rating_efect_rev(1,0,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onmouseout="_rating_efect_rev(1,1,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onclick = "rating_review_shop('rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
',1); rating_checked<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
=true; "
                                                                  src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                                  alt="1" id="img_rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
_1" />

                                                            <img  onmouseover="_rating_efect_rev(2,0,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onmouseout="_rating_efect_rev(2,1,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onclick = "rating_review_shop('rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
',2); rating_checked<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
=true;"
                                                                  src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                                  alt="2" id="img_rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
_2" />

                                                            <img  onmouseover="_rating_efect_rev(3,0,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onmouseout="_rating_efect_rev(3,1,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onclick = "rating_review_shop('rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
',3); rating_checked<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
=true;"
                                                                  src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                                  alt="3"  id="img_rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
_3" />
                                                            <img  onmouseover="_rating_efect_rev(4,0,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onmouseout="_rating_efect_rev(4,1,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onclick = "rating_review_shop('rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
',4); rating_checked<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
=true;"
                                                                  src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                                  alt="4"  id="img_rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
_4" />
                                                            <img  onmouseover="_rating_efect_rev(5,0,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onmouseout="_rating_efect_rev(5,1,'rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
')"
                                                                  onclick = "rating_review_shop('rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
',5); rating_checked<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
=true;"
                                                                  src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                                  alt="5"  id="img_rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
_5" />
                                                        </span>
                                                            <?php if (strlen($_smarty_tpl->tpl_vars['criterion']->value['description']) > 0) {?>
                                                                <div class="clear"></div>
                                                                <div class="tip-criterion-description"><?php echo $_smarty_tpl->tpl_vars['criterion']->value['description'];?>
</div>
                                                            <?php }?>
                                                        </div>
                                                        <input type="hidden" id="rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" name="rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" value="0"/>
                                                        <div class="clr"></div>
                                                        <div class="errorTxtAdd" id="error_rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"></div>

                                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                                <?php } else { ?>
                                                    <label for="rat_rel" class="float-left"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Rating','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>

                                                    <div class="rat rating-stars-dynamic">
                                                        <span onmouseout="read_rating_review_shop('rat_rel');">
                                                            <img  onmouseover="_rating_efect_rev(1,0,'rat_rel')" onmouseout="_rating_efect_rev(1,1,'rat_rel')"
                                                                  onclick = "rating_review_shop('rat_rel',1); rating_checked=true; "
                                                                  src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                                                  alt="1"
                                                                  id="img_rat_rel_1" />
                                                            <img  onmouseover="_rating_efect_rev(2,0,'rat_rel')" onmouseout="_rating_efect_rev(2,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',2); rating_checked=true;" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="2"  id="img_rat_rel_2" />
                                                            <img  onmouseover="_rating_efect_rev(3,0,'rat_rel')" onmouseout="_rating_efect_rev(3,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',3); rating_checked=true;" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="3"  id="img_rat_rel_3" />
                                                            <img  onmouseover="_rating_efect_rev(4,0,'rat_rel')" onmouseout="_rating_efect_rev(4,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',4); rating_checked=true;" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="4"  id="img_rat_rel_4" />
                                                            <img  onmouseover="_rating_efect_rev(5,0,'rat_rel')" onmouseout="_rating_efect_rev(5,1,'rat_rel')" onclick = "rating_review_shop('rat_rel',5); rating_checked=true;" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="5"  id="img_rat_rel_5" />
                                                        </span>
                                                    </div>
                                                    <input type="hidden" id="rat_rel" name="rat_rel" value="0"/>
                                                    <div class="clr"></div>
                                                    <div class="errorTxtAdd" id="error_rat_rel"></div>
                                                <?php }?>
                                                <br/>
                                            <?php }?>

                                            <label for="name-review"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Name','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
                                            <input type="text" name="name-review" id="name-review" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewc_name']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"  onkeyup="check_inpNameReview();" onblur="check_inpNameReview();" />
                                            <div class="errorTxtAdd" id="error_name-review"></div>

                                            <label for="email-review"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
                                            <input type="text" name="email-review" id="email-review" value="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewc_email']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" onkeyup="check_inpEmailReview();" onblur="check_inpEmailReview();"  />
                                            <div id="error_email-review" class="errorTxtAdd"></div>

                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_avatarr']->value == 1) {?>
                                                <label for="avatar-review"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Avatar','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</label>

                                                <?php if (strlen($_smarty_tpl->tpl_vars['spmgsnipreviewc_avatar']->value) > 0) {?>
                                                    <div class="avatar-block-rev-form">
                                                        <input type="radio" name="post_images" checked="" style="display: none">
                                                        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewc_avatar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewc_name']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                                    </div>
                                                <?php }?>

                                                <input type="file" name="avatar-review"
                                                       id="avatar-review"
                                                       class="testimonials-input"
                                                        />
                                                <div class="avatar-guid">
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Allow formats','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
: *.jpg; *.jpeg; *.png; *.gif.
                                                </div>
                                                <div class="errorTxtAdd" id="error_avatar-review"></div>
                                            <?php }?>



                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1) {?>
                                                <label for="subject-review"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Title','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
                                                <input type="text" name="subject-review" id="subject-review" onkeyup="check_inpSubjectReview();" onblur="check_inpSubjectReview();" />
                                                <div id="error_subject-review" class="errorTxtAdd"></div>
                                            <?php }?>

                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1) {?>
                                                <label for="text-review"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Text','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
                                                <textarea id="text-review" name="text-review" cols="42" rows="7" onkeyup="check_inpTextReview();" onblur="check_inpTextReview();"></textarea>
                                                <div id="textarea_feedback"></div>
                                                <div id="error_text-review" class="errorTxtAdd"></div>
                                            <?php }?>


                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_filesr']->value == 1) {?>
                                                <label for="text-files"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Files','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</label>
                                                <span class="file-upload-rev" id="file-upload-rev">
                                            <input type="file" name="files[]" multiple />
                                            <div class="progress-files-bar">
                                                <div class="progress-files"></div>
                                            </div>
                                            <div id="file-files-list"></div>
                                        </span>


                                                <div class="avatar-guid">
                                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Maximum files to upload','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 - <b><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewruploadfiles']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b><br/> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Allow formats','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
: *.jpg; *.jpeg; *.png; *.gif.
                                                </div>
                                                <div id="error_text-files" class="errorTxtAdd"></div>
                                            <?php }?>


                                                                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayGDPRConsent','mod'=>'psgdpr','id_module'=>$_smarty_tpl->tpl_vars['id_module']->value),$_smarty_tpl ) );?>

                                            
                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                                <label for="inpCaptchaReview"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Captcha','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
<sup class="required">*</sup></label>
                                                <div class="clr"></div>
                                                <img width="100" height="26" class="float-left" id="secureCodReview" src="<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewcaptcha_url']->value;?>
" alt="Captcha"/>
                                                <input type="text" class="inpCaptchaReview float-left" id="inpCaptchaReview" size="6" name="captcha"
                                                       onkeyup="check_inpCaptchaReview();" onblur="check_inpCaptchaReview();"/>
                                                <div class="clr"></div>

                                                <div id="error_inpCaptchaReview" class="errorTxtAdd"></div>
                                            <?php }?>



                                        </div>

                                        <div id="footer-add-review-form-review">
                                            <button type="submit" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add review','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
" class="btn btn-success"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add review','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</button>
                                            &nbsp;
                                            <button onclick="show_form_review(0)" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Cancel','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
" class="btn btn-danger"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Cancel','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</button>
                                        </div>







                                    </form>
                                </div>


                            
                                <?php echo '<script'; ?>
 type="text/javascript">


                                    // gdpr
                                    setTimeout(function() {
                                        $('#footer-add-review-form-review').find('button[type="submit"]').removeAttr('disabled');
                                    }, 1000);
                                    // gdpr


                                    function field_gdpr_change_spmgsnipreview(){
                                        // gdpr
                                        var gdpr_spmgsnipreview = $('#psgdpr_consent_checkbox_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['id_module']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');

                                        var is_gdpr_spmgsnipreview = 1;

                                        if(gdpr_spmgsnipreview.length>0){

                                            if(gdpr_spmgsnipreview.prop('checked') == true) {
                                                $('.gdpr_module_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['id_module']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 .psgdpr_consent_message').removeClass('error-label');
                                            } else {
                                                $('.gdpr_module_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['id_module']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 .psgdpr_consent_message').addClass('error-label');
                                                is_gdpr_spmgsnipreview = 0;
                                            }

                                            $('#psgdpr_consent_checkbox_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['id_module']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
').on('click', function(){
                                                if(gdpr_spmgsnipreview.prop('checked') == true) {
                                                    $('.gdpr_module_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['id_module']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 .psgdpr_consent_message').removeClass('error-label');
                                                } else {
                                                    $('.gdpr_module_<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['id_module']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 .psgdpr_consent_message').addClass('error-label');
                                                }
                                            });

                                        }

                                        //gdpr

                                        return is_gdpr_spmgsnipreview;
                                    }
                                    

                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>
                                    var baseDir = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
                                    <?php }?>


                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_filesr']->value == 1) {?>

                                    var file_upload_url_spmgsnipreview = '<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewupload']->value;?>
';
                                    var file_max_files_spmgsnipreview = <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewruploadfiles']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
                                    var file_max_message_spmgsnipreview = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg13_1']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 '+file_max_files_spmgsnipreview+' <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg13_2']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
                                    var file_path_upload_url_spmgsnipreview = baseDir + '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewfpath']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
tmp/';

                                    <?php }?>


                                    var text_min = <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewrminc']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;

                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>document.addEventListener("DOMContentLoaded", function(event) { <?php }?>
                                        $(document).ready(function(){


                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1) {?>
                                            $('#textarea_feedback').html($('#text-review').val().length + ' <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg11']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
. <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg12']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 '+text_min+' <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg11']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');

                                            $('#text-review').keyup(function() {
                                                var text_length_val = trim(document.getElementById('text-review').value);
                                                var text_length = text_length_val.length;

                                                if(text_length<text_min)
                                                    $('#textarea_feedback').css('color','red');
                                                else
                                                    $('#textarea_feedback').css('color','green');

                                                $('#textarea_feedback').html(text_length + ' <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg11']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
. <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg12']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 '+text_min+' <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg11']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                            });

                                            <?php }?>

                                            /* clear form fields */

                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1) {?>

                                            <?php if (count($_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value) > 0) {?>

                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value, 'criterion');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->value) {
?>

                                            $('#rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
').val(0);

                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                            <?php } else { ?>

                                            $('#rat_rel').val(0);

                                            <?php }?>

                                            <?php }?>

                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                            $('#name-review').val('');
                                            $('#email-review').val('');
                                            <?php }?>

                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1) {?>
                                            $('#subject-review').val('');
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1) {?>
                                            $('#text-review').val('');
                                            <?php }?>
                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                            $('#inpCaptchaReview').val('');
                                            <?php }?>

                                            /* clear form fields */
                                        });

                                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>}); <?php }?>




                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                    function check_inpCaptchaReview()
                                    {

                                        var inpCaptchaReview = trim(document.getElementById('inpCaptchaReview').value);

                                        if (inpCaptchaReview.length != 6)
                                        {
                                            field_state_change('inpCaptchaReview','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg1']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                            return false;
                                        }
                                        field_state_change('inpCaptchaReview','success', '');
                                        return true;
                                    }
                                    <?php }?>


                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1) {?>
                                    function check_inpTextReview()
                                    {

                                        var text_review = trim(document.getElementById('text-review').value);

                                        if (text_review.length == 0 || text_review.length<text_min)
                                        {
                                            field_state_change('text-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg2']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                            return false;
                                        }
                                        field_state_change('text-review','success', '');
                                        return true;
                                    }
                                    <?php }?>


                                    function check_inpNameReview()
                                    {

                                        var name_review = trim(document.getElementById('name-review').value);

                                        if (name_review.length == 0)
                                        {
                                            field_state_change('name-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg3']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                            return false;
                                        }
                                        field_state_change('name-review','success', '');
                                        return true;
                                    }


                                    function check_inpEmailReview()
                                    {

                                        var email_review = trim(document.getElementById('email-review').value);

                                        if (email_review.length == 0)
                                        {
                                            field_state_change('email-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg4']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                            return false;
                                        }
                                        field_state_change('email-review','success', '');
                                        return true;
                                    }


                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1) {?>
                                    function check_inpSubjectReview()
                                    {

                                        var subject_review = trim(document.getElementById('subject-review').value);

                                        if (subject_review.length == 0)
                                        {
                                            field_state_change('subject-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg5']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                            return false;
                                        }
                                        field_state_change('subject-review','success', '');
                                        return true;
                                    }
                                    <?php }?>





                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1) {?>



                                    <?php if (count($_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value) > 0) {?>

                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value, 'criterion');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->value) {
?>

                                    var rating_checked<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 = false;

                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                    <?php } else { ?>

                                    var rating_checked = false;

                                    <?php }?>





                                    <?php if (count($_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value) > 0) {?>



                                    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value, 'criterion');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->value) {
?>

                                    function check_inpRatingReview<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
()
                                    {

                                        if(!rating_checked<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
){
                                            field_state_change('rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg6']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                            return false;
                                        }
                                        field_state_change('rat_rel<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
','success', '');
                                        return true;


                                    }
                                    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                    <?php } else { ?>
                                    function check_inpRatingReview()
                                    {
                                        if(!rating_checked){
                                            field_state_change('rat_rel','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg7']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                            return false;
                                        }
                                        field_state_change('rat_rel', 'success', '');
                                        return true;

                                    }


                                    <?php }?>

                                    <?php }?>


                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>document.addEventListener("DOMContentLoaded", function(event) { <?php }?>
                                        $(document).ready(function (e) {
                                            $("#add_review_item_form").on('submit',(function(e) {



                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_avatarr']->value == 1) {?>
                                                field_state_change('avatar-review','success', '');
                                                <?php }?>




                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1) {?>
                                                <?php if (count($_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value) > 0) {?>

                                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value, 'criterion');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->value) {
?>

                                                var is_rating<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 = check_inpRatingReview<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
();

                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                                <?php } else { ?>

                                                var is_rating = check_inpRatingReview();

                                                <?php }?>
                                                <?php }?>

                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1) {?>
                                                var is_subject = check_inpSubjectReview();
                                                <?php }?>

                                                var is_name = check_inpNameReview();
                                                var is_email = check_inpEmailReview();

                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1) {?>
                                                var is_text =  check_inpTextReview();
                                                <?php }?>
                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                                var is_captcha = check_inpCaptchaReview();
                                                <?php }?>


                                                // gdpr
                                                var is_gdpr_spmgsnipreview = field_gdpr_change_spmgsnipreview();


                                                if(is_gdpr_spmgsnipreview && //gdpr
                                                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1) {?>
                                                        <?php if (count($_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value) > 0) {?>

                                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['spmgsnipreviewcriterions']->value, 'criterion');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->value) {
?>

                                                is_rating<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['id_spmgsnipreview_review_criterion'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 &&

                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                                                <?php } else { ?>

                                                is_rating &&

                                                <?php }?>
                                                <?php }?>

                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1) {?>
                                                is_subject &&
                                                <?php }?>
                                                is_name &&
                                                is_email &&
                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1) {?>
                                                is_text &&
                                                <?php }?>
                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                                is_captcha &&
                                                <?php }?>
                                                true
                                                ){

                                                    $('#reviews-list').css('opacity',0.5);
                                                    $('#add-review-form-review').css('opacity',0.5);
                                                    $('#footer-add-review-form-review button').attr('disabled','disabled');

                                                    e.preventDefault();
                                                    $.ajax({
                                                        url: '<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewreviews_url']->value;?>
',
                                                        type: "POST",
                                                        data:  new FormData(this),
                                                        contentType: false,
                                                        cache: false,
                                                        processData:false,
                                                        dataType: 'json',
                                                        success: function(data)
                                                        {


                                                            $('#reviews-list').css('opacity',1);
                                                            $('#add-review-form-review').css('opacity',1);

                                                            if (data.status == 'success') {




                                                                $('#gsniprev-list').html('');
                                                                var paging = $('#gsniprev-list').prepend(data.params.content);
                                                                $(paging).hide();
                                                                $(paging).fadeIn('slow');

                                                                $('#gsniprev-nav').html('');
                                                                var paging = $('#gsniprev-nav').prepend(data.params.paging);
                                                                $(paging).hide();
                                                                $(paging).fadeIn('slow');


                                                                var count_review = data.params.count_reviews;

                                                                $('#count-review-tab').html('');
                                                                $('#count-review-tab').html('('+count_review+')');





                                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>

                                                                var count = Math.random();
                                                                document.getElementById('secureCodReview').src = "";
                                                                document.getElementById('secureCodReview').src = "<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewcaptcha_url']->value;?>
?re=" + count;
                                                                $('#inpCaptchaReview').val('');

                                                                <?php }?>

                                                                jQuery(document).ready(init_rating);



                                                                $('.advertise-text-review').css('opacity','0.2');
                                                                $('#add-review-block').css('opacity','0.2');





                                                                var voucher_html_suggestion = data.params.voucher_html_suggestion;



                                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis_show_voucher']->value) {?>
                                                                /* voucher */

                                                                var voucher_html = data.params.voucher_html;



                                                                if ($('div#fb-con-wrapper').length == 0)
                                                                {
                                                                    conwrapper = '<div id="fb-con-wrapper" class="voucher-data"><\/div>';
                                                                    $('body').append(conwrapper);
                                                                } else {
                                                                    $('#fb-con-wrapper').html('');
                                                                }

                                                                if ($('div#fb-con').length == 0)
                                                                {
                                                                    condom = '<div id="fb-con"><\/div>';
                                                                    $('body').append(condom);
                                                                }



                                                                $('div#fb-con').fadeIn(function(){

                                                                    $(this).css('filter', 'alpha(opacity=70)');
                                                                    $(this).bind('click dblclick', function(){
                                                                        $('div#fb-con-wrapper').hide();
                                                                        $(this).fadeOut();

                                                                        showSocialSuggestion(voucher_html_suggestion);


                                                                    });
                                                                });



                                                                $('div#fb-con-wrapper').html('<a id="button-close" style="display: inline;"><\/a>'+voucher_html).fadeIn();



                                                                $("a#button-close").click(function() {
                                                                    $('div#fb-con-wrapper').hide();
                                                                    $('div#fb-con').fadeOut();
                                                                    showSocialSuggestion(voucher_html_suggestion);


                                                                });



                                                                /* voucher */
                                                                <?php } else { ?>


                                                                showSocialSuggestion(voucher_html_suggestion);


                                                                <?php }?>




                                                            } else {

                                                                var error_type = data.params.error_type;
                                                                $('#footer-add-review-form-review button').removeAttr('disabled');


                                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                                                if(error_type == 3){
                                                                    field_state_change('inpCaptchaReview','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg8']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');

                                                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                                                    var count = Math.random();
                                                                    document.getElementById('secureCodReview').src = "";
                                                                    document.getElementById('secureCodReview').src = "<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewcaptcha_url']->value;?>
?re=" + count;
                                                                    $('#inpCaptchaReview').val('');
                                                                    <?php }?>

                                                                    return false;

                                                                }
                                                                <?php }?>

                                                                if(error_type == 2){
                                                                    field_state_change('email-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg9']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                                                    field_state_change('inpCaptchaReview','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg1']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');

                                                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                                                    var count = Math.random();
                                                                    document.getElementById('secureCodReview').src = "";
                                                                    document.getElementById('secureCodReview').src = "<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewcaptcha_url']->value;?>
?re=" + count;
                                                                    $('#inpCaptchaReview').val('');
                                                                    <?php }?>

                                                                    return false;
                                                                }

                                                                if(error_type == 1){
                                                                    alert("<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg10']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
");
                                                                    window.location.reload();
                                                                }

                                                                if(error_type == 8){
                                                                    field_state_change('avatar-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewava_msg8']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                                                    return false;
                                                                } else if(error_type == 9){
                                                                    field_state_change('avatar-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewava_msg9']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                                                    return false;
                                                                } else if(error_type == 10){
                                                                    field_state_change_store('email-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg14']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                                                    field_state_change_store('name-review','failed', '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewptc_msg14']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
');
                                                                    return false;
                                                                }

                                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_captcha']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewid_customer']->value == 0) {?>
                                                                var count = Math.random();
                                                                document.getElementById('secureCodReview').src = "";
                                                                document.getElementById('secureCodReview').src = "<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewcaptcha_url']->value;?>
?re=" + count;
                                                                $('#inpCaptchaReview').val('');
                                                                <?php }?>


                                                            }

                                                        }
                                                    });





                                                } else {
                                                    return false;
                                                }

                                            }));

                                        });
                                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>}); <?php }?>



                                    //}


                                    function showSocialSuggestion(voucher_html){
                                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewvis_onfb']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis_show_fb_voucher']->value == 1) {?>
                                        if ($('div#fb-con-wrapper').length == 0)
                                        {
                                            conwrapper = '<div id="fb-con-wrapper"><\/div>';
                                            $('body').append(conwrapper);
                                        } else {
                                            $('#fb-con-wrapper').html('');
                                        }

                                        if ($('div#fb-con').length == 0)
                                        {
                                            condom = '<div id="fb-con"><\/div>';
                                            $('body').append(condom);
                                        }

                                        $('div#fb-con').fadeIn(function(){

                                            $(this).css('filter', 'alpha(opacity=70)');
                                            $(this).bind('click dblclick', function(){
                                                $('div#fb-con-wrapper').hide();
                                                $(this).fadeOut();
                                                window.location.reload();
                                            });
                                        });

                                        $('div#fb-con-wrapper').html('<a id="button-close" style="display: inline;"><\/a>'+voucher_html).fadeIn();

                                        $("a#button-close").click(function() {
                                            $('div#fb-con-wrapper').hide();
                                            $('div#fb-con').fadeOut();
                                            window.location.reload();
                                        });
                                        <?php } else { ?>
                                        window.location.reload();
                                        <?php }?>
                                    }


                                <?php echo '</script'; ?>
>
                            

                            <?php }?>



                            <?php }?>

                        </div>








                        <div class="row-custom total-info-tool-product-page">
                            <div class="col-sm-5-custom first-block-ti">



                                

                 <span class="spr-starrating spr-summary-starrating">

                                    

                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewhooktodisplay']->value == "none") {?><div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"><?php }?>
                                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewhooktodisplay']->value == "none") {?>
                                            <meta content="1" itemprop="worstRating">
                                            <meta content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" itemprop="ratingCount">
                                        <?php }?>


                                    <b class="spr-summary-actions-togglereviews gsniprev-block-ratings-text">
                                        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Based on','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 <span class="font-weight-bold" <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewhooktodisplay']->value == "none") {?>itemprop="reviewCount"<?php }?>><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span> <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewtext_reviews']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

                                    </b>



                    -

                    <span>
                    <?php $_smarty_tpl->_assignInScope('test_rating', $_smarty_tpl->tpl_vars['spmgsnipreviewavg_rating']->value);?>
                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_ratid'] = new Smarty_Variable(array());
if (true) {
for ($__section_ratid_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] = 0; $__section_ratid_0_iteration <= 5; $__section_ratid_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']++){
?>

                            <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null) < $_smarty_tpl->tpl_vars['spmgsnipreviewavg_rating']->value) {?>

                                <?php if ($_smarty_tpl->tpl_vars['test_rating']->value <= $_smarty_tpl->tpl_vars['spmgsnipreviewmax_star_par']->value && $_smarty_tpl->tpl_vars['test_rating']->value >= $_smarty_tpl->tpl_vars['spmgsnipreviewmin_star_par']->value) {?>
                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestarh']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                         alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                  <?php } else { ?>
                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                         alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                <?php }?>

                              <?php } else { ?>
                                  <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                       alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                            <?php }?>
                            <?php $_smarty_tpl->_assignInScope('test_rating', $_smarty_tpl->tpl_vars['test_rating']->value-1);?>
                        <?php
}
}
?>
                    </span>

                    <span class="gsniprev-block-ratings-text">
                        <span <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewhooktodisplay']->value == "none") {?>itemprop="ratingValue"<?php }?>><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewavg_decimal']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>/<span <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewhooktodisplay']->value == "none") {?>itemprop="bestRating"<?php }?>>5</span>
                    </span>

                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewhooktodisplay']->value == "none") {?></div><?php }?>

                </span>





                            </div>
                            <div class="col-sm-6-custom b-search-items">

                                <form onsubmit="return false;" method="post" action="#">

                                    <fieldset>
                                        <input type="submit" value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'go','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
" class="button_mini_custom" onclick="go_page_spmgsnipreviewr(0,'productpagereviews','',1,<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)">
                                        <input type="text" class="txt" name="searchr" id="searchr"
                                               onfocus="if(this.value == '<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
') {this.value='';};"
                                               onblur="if(this.value == '') {this.value='<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
';};"
                                               value="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Search','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
" />

                                        <a rel="nofollow" href="javascript:void(0)" id="clear-search-reviews" class="clear-search-items display-none"
                                           onclick="go_page_spmgsnipreviewr(0,'productpagereviews','','',<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)"
                                                >
                                            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Clear search','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                        </a>


                                    </fieldset>
                                </form>


                            </div>

                        </div>





                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_filterp']->value == 1) {?>
                            <!-- filters and ratings -->

                            <div class="spr-header spr-summary row-custom">



                                <div class="col-sm-12-custom">

                                    <div class="row-custom filter-reviews-spmgsnipreview product-reviews-filter-block">

                                        <div class="col-sm-1-custom">
                                            <b class="filter-txt-items-block"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Filter','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
:</b>
                                        </div>
                                        <div class="col-sm-2-custom <?php if (isset($_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value) && $_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value == 5) {?>active-items-block<?php }?>">
                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewfive']->value > 0) {?>
                                            <a rel="nofollow"

                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',5,'<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_search']->value == 1) {?>1<?php }?>',<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)"
                                               href="javascript:void(0)"
                                               id="reviews-rating-5"

                                                    >
                                                <?php }?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_1_iteration <= 5; $__section_test_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                                <?php
}
}
?>
                                                <span class="count-items-block <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewfive']->value == 0) {?>text-decoration-none<?php }?>">(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewfive']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)</span>
                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewfive']->value > 0) {?>
                                            </a>
                                            <?php }?>
                                        </div>
                                        <div class="col-sm-2-custom <?php if (isset($_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value) && $_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value == 4) {?>active-items-block<?php }?>">
                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewfour']->value > 0) {?>
                                            <a rel="nofollow"


                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',4,'<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_search']->value == 1) {?>1<?php }?>', <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)"
                                               href="javascript:void(0)"
                                               id="reviews-rating-4"

                                                    >
                                                <?php }?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_2_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_2_iteration <= 4; $__section_test_2_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                                <?php
}
}
?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_3_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_3_iteration <= 1; $__section_test_3_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                                <?php
}
}
?>

                                                <span class="count-items-block <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewfour']->value == 0) {?>text-decoration-none<?php }?>">(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewfour']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)</span>
                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewfour']->value > 0) {?>
                                            </a>
                                            <?php }?>
                                        </div>
                                        <div class="col-sm-2-custom <?php if (isset($_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value) && $_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value == 3) {?>active-items-block<?php }?>">
                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewthree']->value > 0) {?>
                                            <a rel="nofollow"

                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',3,'<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_search']->value == 1) {?>1<?php }?>', <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)"
                                               href="javascript:void(0)"
                                               id="reviews-rating-3"

                                                    >
                                                <?php }?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_4_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_4_iteration <= 3; $__section_test_4_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                                <?php
}
}
?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_5_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_5_iteration <= 2; $__section_test_5_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                                <?php
}
}
?>
                                                <span class="count-items-block <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewthree']->value == 0) {?>text-decoration-none<?php }?>">(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewthree']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)</span>
                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewthree']->value > 0) {?>
                                            </a>
                                            <?php }?>
                                        </div>
                                        <div class="col-sm-2-custom <?php if (isset($_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value) && $_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value == 2) {?>active-items-block<?php }?>">
                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtwo']->value > 0) {?>
                                            <a rel="nofollow"

                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',2,'<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_search']->value == 1) {?>1<?php }?>', <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)"
                                               href="javascript:void(0)"
                                               id="reviews-rating-2"
                                                    >
                                                <?php }?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_6_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_6_iteration <= 2; $__section_test_6_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                                <?php
}
}
?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_7_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_7_iteration <= 3; $__section_test_7_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                                <?php
}
}
?>

                                                <span class="count-items-block <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtwo']->value == 0) {?>text-decoration-none<?php }?>">(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewtwo']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)</span>
                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtwo']->value > 0) {?>
                                            </a>
                                            <?php }?>
                                        </div>
                                        <div class="col-sm-2-custom <?php if (isset($_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value) && $_smarty_tpl->tpl_vars['spmgsnipreviewfrat']->value == 1) {?>active-items-block<?php }?>">
                                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewone']->value > 0) {?>
                                            <a rel="nofollow"

                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews',1,'<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_search']->value == 1) {?>1<?php }?>', <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)"
                                               href="javascript:void(0)"
                                               id="reviews-rating-1"
                                                    >
                                                <?php }?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_8_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_8_iteration <= 1; $__section_test_8_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                                <?php
}
}
?>
                                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_9_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_9_iteration <= 4; $__section_test_9_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                                    <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                                <?php
}
}
?>
                                                <span class="count-items-block <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewone']->value == 0) {?>text-decoration-none<?php }?>">(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewone']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)</span>
                                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewone']->value > 0) {?>
                                            </a>
                                            <?php }?>
                                        </div>


                                        <div class="col-sm-1-custom">
                                            <a rel="nofollow" href="javascript:void(0)"
                                               onclick="go_page_spmgsnipreviewr(0,'productpagereviews','','',<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewid_product']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)"
                                               id="clear-rating-reviews" class="reset-items-block display-none">
                                                <i class="fa fa-refresh"></i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reset','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                            </a>
                                        </div>



                                    </div>

                                </div>
                                <div class="clear"></div>
                            </div>



                            <!-- filters and ratings -->
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_search']->value == 1) {?>
                            <h3 class="search-result-item"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Results for','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 <b>"<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewsearch']->value,'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"</b></h3>
                            <br/>
                        <?php }?>


                        <?php if ($_smarty_tpl->tpl_vars['reviews']->value && $_smarty_tpl->tpl_vars['spmgsnipreviewis_sortf']->value == 1) {?>
                            <div class="search-result-item float-right">

                                <b>
                                    <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sort by','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
:
                                </b>
                                <select id="select_spmgsnipreview_sort" data-action="productpagereviews">
                                    <option value="time_add:desc" selected="selected">--</option>
                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1) {?>
                                        <option value="rating:asc">Rating: Lowest first</option>
                                        <option value="rating:desc">Rating: Highest first</option>
                                    <?php }?>
                                    <option value="time_add:asc">Date: Oldest first</option>
                                    <option value="time_add:desc">Date: Newest first</option>
                                </select>

                            </div>
                            <div class="clear"></div>
                            <br/>
                        <?php }?>




                        <?php if ($_smarty_tpl->tpl_vars['reviews']->value) {?>
                            <div class="spr-reviews" id="reviews-list">


                                <?php $_smarty_tpl->_subTemplateRender("module:spmgsnipreview/views/templates/front/list_reviews.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



                            </div>


                                                        <div id="page_navr"><?php echo $_smarty_tpl->tpl_vars['paging']->value;?>
</div>
                            


                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewgp']->value > 1) {?>
                        
                            <?php echo '<script'; ?>
 type="text/javascript">
                                $(document).ready(function() {
                                    spmgsnipreview_open_tab();
                                });
                            <?php echo '</script'; ?>
>
                        
                        <?php }?>


                        <?php } else { ?>


                            <div class="advertise-text-review advertise-text-review-text-align" id="no-customers-reviews">
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No reviews for the product','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>


                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value == 0) {?>
                                    <br/><br/>
                                    <a href="javascript:void(0)" class="btn-spmgsnipreview btn-primary-spmgsnipreview" onclick="show_form_review(1)">
                                        <b id="button-addreview-blockreview">
                                            <i class="icon-pencil"></i>&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Be the first to write your review !','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                        </b>
                                    </a>
                                <?php }?>
                            </div>




                        <?php }?>

                    </div>

                </div></div>

            <div class="clear-spmgsnipreview"></div>
            <!-- reviews template -->



        </div>



    <?php }?>

<?php }?>







<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>

    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrvis_on']->value == 1) {?>

        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1) {?>

            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewhooktodisplay']->value == "product_footer") {?>

                <div class="clear-spmgsnipreview"></div>

                <div class="<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {?>gsniprev-block-16<?php } else { ?>gsniprev-block<?php }?>">
                    <b class="title-rating-block">
                        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Total Rating','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
" />&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Total Rating','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</b><span class="ratings-block-punct">:</span>
                    <br/><br/>

                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?><div itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating"><?php }?>
                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>
                            <meta content="1" itemprop="worstRating">
                            <meta content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" itemprop="ratingCount">
                        <?php }?>

                        <div class="rating"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewavg_rating']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</div>
                        <div class="gsniprev-block-reviews-text">
                            <span <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="ratingValue"<?php }?>><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewavg_decimal']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>/<span <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="bestRating"<?php }?>>5</span> - <span id="count_review_block" <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="reviewCount"<?php }?>><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span> <span id="reviews_text_block"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewtext_reviews']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
                        </div>
                        <div class="clear-spmgsnipreview"></div>
                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewcount_reviews']->value > 0 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?></div><?php }?>
                    <br/>


                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewstarratingon']->value == 1) {?>

                        <a href="javascript:void(0)" onclick="$('.gsniprev-rating-block').toggle();" class="view-ratings"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View ratings','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</a>
                        <br/>
                        <div class="gsniprev-rating-block">
                            <table class="gsniprev-rating-block-table">
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_10_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_10_iteration <= 5; $__section_test_10_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="five-blockreview"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewfive']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b></td>
                                </tr>
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_11_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_11_iteration <= 4; $__section_test_11_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_12_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_12_iteration <= 1; $__section_test_12_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="four-blockreview"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewfour']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b></td>
                                </tr>
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_13_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_13_iteration <= 3; $__section_test_13_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_14_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_14_iteration <= 2; $__section_test_14_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="three-blockreview"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewthree']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b></td>
                                </tr>
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_15_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_15_iteration <= 2; $__section_test_15_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_16_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_16_iteration <= 3; $__section_test_16_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="two-blockreview"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewtwo']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b></td>
                                </tr>
                                <tr>
                                    <td class="gsniprev-rating-block-left">
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_17_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_17_iteration <= 1; $__section_test_17_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                        <?php
$_smarty_tpl->tpl_vars['__smarty_section_test'] = new Smarty_Variable(array());
if (true) {
for ($__section_test_18_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] = 0; $__section_test_18_iteration <= 4; $__section_test_18_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']++){
?>
                                            <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_test']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_test']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
                                        <?php
}
}
?>
                                    </td>
                                    <td class="gsniprev-rating-block-right"><b id="one-blockreview"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewone']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b></td>
                                </tr>
                            </table>
                        </div>

                        <br/>
                    <?php }?>


                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_add']->value != 1) {?>
                        <a class="btn-spmgsnipreview btn-primary-spmgsnipreview" href="#idTab777" id="idTab777-my-click" >
        <span>
            <i class="icon-pencil"></i>&nbsp;

            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add Review','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>


        </span>
                        </a>
                    <?php }?>


                    <a class="btn-spmgsnipreview btn-default-spmgsnipreview" href="#idTab777" >
        <span>
            <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="title-rating-one-star" alt="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View Reviews','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
"/>
            <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View Reviews','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

        </span>
                    </a>




                </div>




            <?php }?>

        <?php }?>

    <?php }?>

    <?php if (isset($_smarty_tpl->tpl_vars['spmgsnipreviewproductfooter']->value)) {?>
        <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewproductfooter']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

    <?php }?>


    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewpinvis_on']->value == 1 && isset($_smarty_tpl->tpl_vars['spmgsnipreview_productFooter']->value) && $_smarty_tpl->tpl_vars['spmgsnipreview_productFooter']->value == 'productFooter') {?>
        <a href="//www.pinterest.com/pin/create/button/?
		url=http://<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['HTTP_HOST'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['REQUEST_URI'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

		&media=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_image']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>

		&description=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['meta_description']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
           data-pin-do="buttonPin" data-pin-config="<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewpinterestbuttons']->value == 'firston') {?>above<?php }
if ($_smarty_tpl->tpl_vars['spmgsnipreviewpinterestbuttons']->value == 'secondon') {?>beside<?php }
if ($_smarty_tpl->tpl_vars['spmgsnipreviewpinterestbuttons']->value == 'threeon') {?>none<?php }?>">
            <img src="//assets.pinterest.com/images/pidgets/pin_it_button.png" alt="Pinterest" />
        </a>
    <?php }?>

<?php }
}
}
