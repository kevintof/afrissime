<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:16:16
  from '/home/afrissim/public_html/modules/spmgsnipreview/views/templates/hooks/head.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f8307daed7_00909377',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0bf123dcbf58e9712ab5bb85d28e25454d671bec' => 
    array (
      0 => '/home/afrissim/public_html/modules/spmgsnipreview/views/templates/hooks/head.tpl',
      1 => 1567445957,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6f8307daed7_00909377 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_r_p']->value != 0) {?>
    <meta property="og:title" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewname']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
    <meta property="og:image" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewimg']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
    <meta property="og:description" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewdescr']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
    <meta property="og:url" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewreview_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
    <meta property="og:type" content="product"/>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewpinvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis_product_page']->value != 0) {?>

<meta property="og:title" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_name']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:description" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewpindesc']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:type" content="product" />
<meta property="og:url" content="<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_ssl']->value == 1) {?>https<?php } else { ?>http<?php }?>://<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['HTTP_HOST'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_SERVER['REQUEST_URI'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:site_name" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['shop_name']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:price:amount" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['product_price_custom']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:price:currency" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['currency_custom']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" />
<meta property="og:availability" content="<?php if ($_smarty_tpl->tpl_vars['stock_string']->value == 'in_stock') {?>instock<?php } else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['stock_string']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>" />

<?php }?>


<?php echo '<script'; ?>
 type="text/javascript">
    var is_mobile_spmgsnipreview = '<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value;?>
';
<?php echo '</script'; ?>
>


<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrvis_on']->value == 1) {?>

<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrsson']->value == 1) {?>
<link rel="alternate" type="application/rss+xml" href="<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewrss_url']->value;?>
" />
<?php }?>


    <?php echo '<script'; ?>
 type="text/javascript">

        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>
            var baseDir = '<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
';
        <?php }?>

        var spmgsnipreview_is_rewrite = '<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewis_rewrite']->value;?>
';

        var ajax_productreviews_url_spmgsnipreview = '<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewreviews_url']->value;?>
';

    <?php echo '</script'; ?>
>



<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1) {?>



<style type="text/css">
.page-item.active .page-link, .page-item.active .page-link:focus, .page-item.active .page-link:hover
{
    background-color:<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewstylecolor']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
    border-color:<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewstylecolor']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
}
a.page-link:hover {
    background-color:<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewstylecolor']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
!important  ;
    color:#fff;
    border-color:<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewstylecolor']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
}

    </style>

<?php }?>

<?php }?>

<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrsoc_on']->value == 1) {?>
<!-- facebook button -->

    <?php echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewfbliburl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo '</script'; ?>
>

<!-- facebook button -->
<?php }?>







<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_uprof']->value == 1) {?>


    <?php echo '<script'; ?>
 type="text/javascript">

        var ajax_users_url_spmgsnipreview = '<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewajax_users_url']->value;?>
';

        
<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_show']->value == 1) {?>

    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewislogged']->value != 0) {?>
    document.addEventListener("DOMContentLoaded", function(event) {
        $('document').ready( function() {
            var count1 = Math.random();
            var ph =  '<img class="avatar-header-spmgsnipreview" '+
                    ' src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewavatar_thumb']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
?re=' + count1+'"'+
                    ' />';



            if($('#header_user_info span'))
                $('#header_user_info span:last').append(ph);

            // for PS 1.6 >
            if($('.header_user_info')){
                $('.header_user_info .account:last').append(ph);

            }

            // for ps 1.7 >
            if($('.user-info')){
                $('.user-info .account:last').append(ph);

            }


        });
    });
    <?php }
}?>
        
    <?php echo '</script'; ?>
>

<?php }?>





<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_storerev']->value == 1) {?>



    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrssontestim']->value == 1) {?>
        <link rel="alternate" type="application/rss+xml" href="<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewrss_testimonials_url']->value;?>
" />
    <?php }?>



    <style type="text/css">
        .ps15-color-background{background-color:<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewBGCOLOR_T']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;}


        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_leftside']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewt_rightside']->value == 1) {?>




        /* testimonials widget */
        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_leftside']->value == 1) {?>


        div#spmgsnipreview-box.left_shopreviews .belt {

            border-radius: 5px;


        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 0) {?>
            background-color: <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewBGCOLOR_TIT']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
            color: white;
            font-size: 15px;
            padding: 5px;
            right: 67px;
            text-align: center;
            top: 68px;
            box-sizing:border-box;
            width: 171px;
            height: 33px;
        <?php } else { ?>
            background: <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewBGCOLOR_TIT']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 url("<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/t-left<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewlang']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
.png") repeat scroll 0 0;
            width: 33px;
            height: 151px;
        <?php }?>
        }

        table.spmgsnipreview-widgets td.facebook_block{
            height: 151px;
        }

        <?php }?>


        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_rightside']->value == 1) {?>
        #spmgsnipreview-box.right_shopreviews .belt{

            border-radius: 5px;


        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 0) {?>

            -webkit-transform: rotate(270deg);
            -moz-transform: rotate(270deg);
            -ms-transform: rotate(270deg);
            -o-transform: rotate(270deg);
            transform: rotate(270deg);
            background-color: <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewBGCOLOR_TIT']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
;
            color: white;
            font-size: 15px;
            padding: 5px;
            right: -67px;
            text-align: center;
            top: 68px;
            box-sizing:border-box;
            width: 171px;
            height: 33px;

        <?php } else { ?>
            background: <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewBGCOLOR_TIT']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 url("<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/t-right<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewlang']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
.png") repeat scroll 0 0;
            width: 33px;
            height: 151px;
        <?php }?>
        }

        table.spmgsnipreview-widgets td.facebook_block{
            height: 151px;
        }

        <?php }?>



        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_leftside']->value == 1) {?>
        .spmgsnipreview-widgets .left_shopreviews{
            right: auto;

        }
        <?php }
if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_rightside']->value == 1) {?>
        .spmgsnipreview-widgets .right_shopreviews {
            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:111px;<?php }?>
            right: -<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+23, ENT_QUOTES, 'UTF-8');
}?>px;
        }
        <?php }?>

        div#spmgsnipreview-box.left_shopreviews{
            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:111px;<?php }?>
            left: -<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+23, ENT_QUOTES, 'UTF-8');
}?>px;
        }
        #spmgsnipreview-box .outside{
        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_leftside']->value == 1) {?>
            float: right;
        <?php } else { ?>
            float: right;
        <?php }?>
        }

        /* testimonials widget */

        

        <?php }?>
        

    </style>







    <?php echo '<script'; ?>
 type="text/javascript">

        var ajax_storereviews_url_spmgsnipreview = '<?php echo $_smarty_tpl->tpl_vars['spmgsnipreviewajax_url']->value;?>
';

        document.addEventListener("DOMContentLoaded", function(event) {
        $(document).ready(function() {


            

            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_leftside']->value == 1 || $_smarty_tpl->tpl_vars['spmgsnipreviewt_rightside']->value == 1) {?>
            /* testimonials widget */

            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_leftside']->value == 1) {?>

            <?php if (($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewmt_leftside']->value == 1) || (!$_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewst_leftside']->value == 1)) {?>

            $(".spmgsnipreview-widgets .left_shopreviews .outside").hover(
                    function () {
                        $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:'auto',<?php }?>left:'0px'}, 500);
                    },
                    function () {
                        $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:'111px',<?php }?>left:'-<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+23, ENT_QUOTES, 'UTF-8');
}?>px'}, 500);

                    }
            );
            $(".spmgsnipreview-widgets .left_shopreviews .belt").hover(

                    function () {
                        $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:'auto',<?php }?>left:'0px'}, 500);
                    },
                    function () {

                        $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:'111px',<?php }?>left:'-<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+23, ENT_QUOTES, 'UTF-8');
}?>px'}, 500);

                    }
            );


            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>


            $( ".spmgsnipreview-widgets .left_shopreviews .belt" ).click(function() {
                var style_belt = $(".spmgsnipreview-widgets .left_shopreviews").attr("style");

                if(style_belt == "left: 0px; height: 111px; overflow: hidden;"
                        ||
                        style_belt == "left: 0px; height: 111px;"){
                    $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({left:'-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');?>
px'}, 500);

                }

                if(style_belt == "left: -<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');?>
px; height: 111px; overflow: hidden;"
                        ||
                        style_belt == "left: -<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');?>
px; height: 111px;"
                ){
                    $(".spmgsnipreview-widgets .left_shopreviews").stop().animate({left:'0px'}, 500);

                }
            });

            <?php }?>





            <?php }?>

            <?php }?>

            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewt_rightside']->value == 1) {?>

            <?php if (($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewmt_rightside']->value == 1) || (!$_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewst_rightside']->value == 1)) {?>

            $(".spmgsnipreview-widgets .right_shopreviews .outside").hover(
                    function () {
                        $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:'auto',<?php }?>right:'0px'}, 500);
                    },
                    function () {
                        $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:'111px',<?php }?>right:'-<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+23, ENT_QUOTES, 'UTF-8');
}?>px'}, 500);

                    }
            );
            $(".spmgsnipreview-widgets .right_shopreviews .belt").hover(
                    function () {
                        $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:'auto',<?php }?>right:'0px'}, 500);
                    },
                    function () {
                        $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>height:'111px',<?php }?>right:'-<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+23, ENT_QUOTES, 'UTF-8');
}?>px'}, 500);

                    }
            );



            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_mobile']->value == 1) {?>

            $( ".spmgsnipreview-widgets .right_shopreviews .belt" ).click(function() {
                var style_belt = $(".spmgsnipreview-widgets .right_shopreviews").attr("style");

                if(style_belt == "right: 0px; height: 111px; overflow: hidden;"
                        ||
                        style_belt == "right: 0px; height: 111px;"){
                    $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({right:'-<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');?>
px'}, 500);

                }

                if(style_belt == "right: -<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');?>
px; height: 111px; overflow: hidden;"
                        ||
                        style_belt == "right: -<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewt_width']->value,'htmlall','UTF-8' ))+5, ENT_QUOTES, 'UTF-8');?>
px; height: 111px;"
                ){
                    $(".spmgsnipreview-widgets .right_shopreviews").stop().animate({right:'0px'}, 500);

                }
            });

            <?php }?>

            



            <?php }?>

            <?php }?>

            /* testimonials widget */
            <?php }?>




        });
        });
    <?php echo '</script'; ?>
>


<?php }
}
}
