<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:15:31
  from '/home/afrissim/public_html/themes/jms_basel/templates/checkout/_partials/steps/unreachable.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db706138b6b06_27891917',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b08e2ea77051c075a505e6e4e7055d4af0b17b66' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/templates/checkout/_partials/steps/unreachable.tpl',
      1 => 1566831868,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db706138b6b06_27891917 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="checkout-step -unreachable" id="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['identifier']->value, ENT_QUOTES, 'UTF-8');?>
">
  <h1 class="step-title">
    <span class="step-number"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['position']->value, ENT_QUOTES, 'UTF-8');?>
</span> <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title']->value, ENT_QUOTES, 'UTF-8');?>

  </h1>
</section>
<?php }
}
