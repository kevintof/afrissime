<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:14:59
  from '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/product-footer.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db705f3292e96_68054347',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '95af62870d81e4ba6315323c60b2388fe7d28c15' => 
    array (
      0 => '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/product-footer.tpl',
      1 => 1571236966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db705f3292e96_68054347 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!-- MODULE allinone_rewards -->
<span id="aior_add_to_cart_available_display"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['aior_total_available_display']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>
<span id="aior_add_to_cart_available_real"><?php echo htmlspecialchars(floatval($_smarty_tpl->tpl_vars['aior_total_available_real']->value), ENT_QUOTES, 'UTF-8');?>
</span>
<?php echo '<script'; ?>
 type="text/javascript">
//<![CDATA[
	var aior_product_purchase_url="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['link']->value->getModuleLink('allinone_rewards','product_purchase'),'javascript','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
";
	var aior_purchase_confirm_message0="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Do you want to use your rewards to buy this product ?','mod'=>'allinone_rewards','js'=>1),$_smarty_tpl ) );?>
";
	var aior_purchase_confirm_message1="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your available balance is','mod'=>'allinone_rewards','js'=>1),$_smarty_tpl ) );?>
";
	var aior_purchase_confirm_message2="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'will be deducted immediately from your rewards account.','mod'=>'allinone_rewards','js'=>1),$_smarty_tpl ) );?>
";
	var aior_purchase_confirm_message3="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your available balance will then be','mod'=>'allinone_rewards','js'=>1),$_smarty_tpl ) );?>
";
	var aior_purchase_confirm_message4="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This action can not be canceled, do you confirm ?','mod'=>'allinone_rewards','js'=>1),$_smarty_tpl ) );?>
";
	var aior_success_message="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This product has been added to your cart.','mod'=>'allinone_rewards','js'=>1),$_smarty_tpl ) );?>
";
	var aior_success_message2="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your available balance is','mod'=>'allinone_rewards','js'=>1),$_smarty_tpl ) );?>
";
//]]>
<?php echo '</script'; ?>
>
<!-- END : MODULE allinone_rewards --><?php }
}
