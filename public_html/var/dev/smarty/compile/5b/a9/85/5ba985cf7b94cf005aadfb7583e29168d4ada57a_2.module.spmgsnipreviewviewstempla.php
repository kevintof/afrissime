<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:51:32
  from 'module:spmgsnipreviewviewstempla' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70e84b1d893_50322899',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5ba985cf7b94cf005aadfb7583e29168d4ada57a' => 
    array (
      0 => 'module:spmgsnipreviewviewstempla',
      1 => 1567445957,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db70e84b1d893_50322899 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/afrissim/public_html/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>
<!-- begin /home/afrissim/public_html/modules/spmgsnipreview/views/templates/front/list_reviews.tpl -->
<?php if (count($_smarty_tpl->tpl_vars['reviews']->value) > 0) {
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['reviews']->value, 'review');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['review']->value) {
?>
    <div class="spr-review pl-animater <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewd_eff_rev']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="review" itemscope itemtype="http://schema.org/Review"<?php }?>>
        <div class="spr-review-header">
            <?php if ($_smarty_tpl->tpl_vars['review']->value['is_active'] == 1) {?>
                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewratings_on']->value == 1 && $_smarty_tpl->tpl_vars['review']->value['rating'] != 0) {?>
                    <span class="spr-starratings spr-review-header-starratings">

                                  <?php
$_smarty_tpl->tpl_vars['__smarty_section_ratid'] = new Smarty_Variable(array());
if (true) {
for ($__section_ratid_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] = 0; $__section_ratid_0_iteration <= 5; $__section_ratid_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']++){
?>
                                      <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null) < $_smarty_tpl->tpl_vars['review']->value['rating']) {?>
                                          <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
								  	<?php } else { ?>
								    	<img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"  alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                      <?php }?>
                                  <?php
}
}
?>

                                </span>
                    <div <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemtype="http://schema.org/Rating" itemscope itemprop="reviewRating"<?php }?> class="rating-stars-total">
                        (<span <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="ratingValue"<?php }?>><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['rating'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>/<span <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="bestRating"<?php }?>>5</span>)&nbsp;
                    </div>


                <?php }?>
                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtitle_on']->value == 1 && strlen($_smarty_tpl->tpl_vars['review']->value['title_review']) > 0) {?>
                    <h3 class="spr-review-header-title" <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="name"<?php }?>><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['title_review'],'htmlall','UTF-8' ));?>
</h3>
                <?php }?>

            <?php }?>

            <div class="clear-spmgsnipreview"></div>

                            <span class="spr-review-header-byline float-left">
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'By','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_avatarr']->value == 1 && strlen($_smarty_tpl->tpl_vars['review']->value['avatar']) > 0 && $_smarty_tpl->tpl_vars['review']->value['is_show_ava'] == 1) {?>
                                    <span class="avatar-block-rev">
                                        <img alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['customer_name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                                             src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['avatar'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                    </span>
                                <?php }?>
                                <?php if (strlen($_smarty_tpl->tpl_vars['review']->value['customer_name']) > 0) {?>
                                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_uprof']->value && $_smarty_tpl->tpl_vars['review']->value['id_customer'] > 0 && $_smarty_tpl->tpl_vars['review']->value['is_show_ava'] == 1) {?><a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewuser_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id_customer'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" title="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['customer_name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="user-link-to-profile"><?php }?><strong <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="author"<?php }?>
                                    ><?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['customer_name'],'htmlall','UTF-8' ));?>
</strong><?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_uprof']->value && $_smarty_tpl->tpl_vars['review']->value['id_customer'] > 0 && $_smarty_tpl->tpl_vars['review']->value['is_show_ava'] == 1) {?></a><?php }?>
                                <?php }?>
                                <?php if (strlen($_smarty_tpl->tpl_vars['review']->value['customer_name']) > 0) {
echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'on','mod'=>'spmgsnipreview'),$_smarty_tpl ) );
}?>&nbsp;<strong><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['review']->value['time_add']),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</strong>

                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?><meta itemprop="datePublished" content="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['date_pub_for_snippets'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/><?php }?>

                                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewip_on']->value == 1 && strlen($_smarty_tpl->tpl_vars['review']->value['ip']) > 0) {?>
                                    (<?php if ($_smarty_tpl->tpl_vars['review']->value['is_no_ip'] == 0) {?><b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'IP','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
:</b>&nbsp;<?php }
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['ip'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)
                                <?php }?>

                                <span class="font-size-10" <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="itemReviewed"<?php }?>><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['product_name'],'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span>

                            </span>

            <?php if ($_smarty_tpl->tpl_vars['review']->value['is_active'] == 1) {?>
                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_helpfulf']->value == 1) {?>
                    <span class="float-right people-folowing-reviews" id="people-folowing-reviews<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><span class="first-helpful" id="block-helpful-yes<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['helpfull_yes'],'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'of','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 <span id="block-helpful-all<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['helpfull_all'],'quotes','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</span> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'people found the following review helpful','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</span>
                <?php }?>
            <?php }?>
            <div class="clear-spmgsnipreview"></div>

            <?php if ($_smarty_tpl->tpl_vars['review']->value['is_buy'] != 0) {?>
                <span class="spr-review-header-byline float-left">
                                <span class="is_buy_product"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Verified Purchase','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</span>
                            </span>
                <div class="clear-spmgsnipreview"></div>
            <?php }?>

        </div>

        <div class="<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {?>row-custom<?php } else { ?>row-list-reviews<?php }?>">

            <?php if ($_smarty_tpl->tpl_vars['review']->value['is_active'] == 1) {?>
                <?php if (count($_smarty_tpl->tpl_vars['review']->value['criterions']) > 0) {?>
                    <div class="spr-review-content <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {?>col-sm-3-custom<?php } else { ?>col-sm-3-list-reviews<?php }?>">

                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['review']->value['criterions'], 'criterion');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['criterion']->value) {
?>
                            <div class="criterion-item-block">
                                <?php if (strlen(trim($_smarty_tpl->tpl_vars['criterion']->value['name'])) > 0) {?>
                                    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['criterion']->value['name'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
:
                                <?php }?>

                                <?php
$_smarty_tpl->tpl_vars['__smarty_section_ratid'] = new Smarty_Variable(array());
if (true) {
for ($__section_ratid_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] = 0; $__section_ratid_1_iteration <= 5; $__section_ratid_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']++){
?>
                                    <?php if ((isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null) < $_smarty_tpl->tpl_vars['criterion']->value['rating']) {?>
                                        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="gsniprev-img-star-list" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                    <?php } else { ?>
                                        <img src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
modules/spmgsnipreview/views/img/<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewnoactivestar']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="gsniprev-img-star-list"  alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( (isset($_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_ratid']->value['index'] : null),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"/>
                                    <?php }?>
                                <?php
}
}
?>

                            </div>
                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>

                    </div>
                <?php }?>
            <?php }?>

            <div class="spr-review-content <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {?>col-sm-<?php if (count($_smarty_tpl->tpl_vars['review']->value['criterions']) > 0 && $_smarty_tpl->tpl_vars['review']->value['is_active'] == 1) {?>9<?php } else { ?>12<?php }?>-custom<?php } else { ?>col-sm-<?php if (count($_smarty_tpl->tpl_vars['review']->value['criterions']) > 0 && $_smarty_tpl->tpl_vars['review']->value['is_active'] == 1) {?>9<?php } else { ?>12<?php }?>-list-reviews<?php }?>">

                <?php if ($_smarty_tpl->tpl_vars['review']->value['is_active'] == 1) {?>
                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewtext_on']->value == 1 && strlen($_smarty_tpl->tpl_vars['review']->value['text_review']) > 0) {?>
                                                <p class="spr-review-content-body" <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewsvis_on']->value == 1 && $_smarty_tpl->tpl_vars['spmgsnipreviewis16_snippet']->value == 1) {?>itemprop="description"<?php }?>><?php echo nl2br($_smarty_tpl->tpl_vars['review']->value['text_review']);?>
</p>
                                                <br/>
                    <?php }?>

                    <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_filesr']->value == 1) {?>
                        <?php if (count($_smarty_tpl->tpl_vars['review']->value['files']) > 0) {?>
                            <div  class="<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {?>row-custom<?php } else { ?>row-list-reviews<?php }?>">
                                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['review']->value['files'], 'file');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['file']->value) {
?>
                                    <div class="col-sm-<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis16']->value == 1) {?>2<?php } else {
if (count($_smarty_tpl->tpl_vars['review']->value['criterions']) > 0) {?>4<?php } else { ?>6<?php }
}?>-custom files-review-spmgsnipreview">
                                        <a class="fancybox shown" data-fancybox-group="other-views" href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file']->value['full_path'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                            <img class="img-responsive" width="105" height="105" src="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['base_dir_ssl']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file']->value['small_path'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" alt="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['file']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"

                                                    >
                                        </a>
                                                                            </div>
                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                            </div>
                            <div class="clear-spmgsnipreview"></div>
                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?><br/><br/><?php }?>
                        <?php }?>
                    <?php }?>

                <?php } else { ?>
                    <p class="spr-review-content-body"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'The customer has rated the product but has not posted a review, or the review is pending moderation','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</p>
                <?php }?>



                <?php if ($_smarty_tpl->tpl_vars['review']->value['is_active'] == 1) {?>
                <?php if (strlen($_smarty_tpl->tpl_vars['review']->value['admin_response']) > 0 && $_smarty_tpl->tpl_vars['review']->value['is_display_old'] == 1) {?>
                    <div class="clear-spmgsnipreview"></div>
                    <div class="shop-owner-reply-on-review">
                        <div class="owner-date-reply"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Shop owner reply','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
 (<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( smarty_modifier_date_format($_smarty_tpl->tpl_vars['review']->value['review_date_update']),'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
): </div>
                        <?php echo nl2br($_smarty_tpl->tpl_vars['review']->value['admin_response']);?>

                    </div>
                <?php }?>
                    <div class="clear-spmgsnipreview"></div>


                    <div class="spr-review-footer row-custom">

                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_helpfulf']->value == 1) {?>
                            <div class="col-sm-<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_abusef']->value == 0) {?>12<?php } else { ?>9<?php }?>-custom margin-bottom-10 block-helpful" id="block-helpful<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
">
                                <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Was this review helpful to you?','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>

                                <a class="btn-success button_padding_spmgsnipreview" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
"
                                   href="javascript:void(0)" onclick="report_helpfull_spmgsnipreview(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,1)" ><b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Yes','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</b></a>
                                <a class="btn-danger button_padding_spmgsnipreview" title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
"
                                   href="javascript:void(0)" onclick="report_helpfull_spmgsnipreview(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
,0)"><b><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</b></a>
                            </div>
                        <?php }?>

                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_abusef']->value == 1) {?>
                            <div class="col-sm-<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_helpfulf']->value == 0) {?>12<?php } else { ?>3<?php }?>-custom margin-bottom-10">
                                <a class="button_padding_spmgsnipreview spr-review-reportreview"
                                   title="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Report abuse','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
"
                                   href="javascript:void(0)" onclick="report_abuse_spmgsnipreview(<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
)"
                                        ><b><i class="fa fa-ban text-primary"></i>&nbsp;<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Report abuse','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>
</b></a>
                            </div>
                        <?php }?>


                        <div class="clear-spmgsnipreview"></div>

                    </div>

                <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrsoc_on']->value == 1) {?>
                    <div class="fb-like valign-top" data-href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewrev_url']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_rewrite']->value == 1) {?>?<?php } else { ?>&<?php }?>rid=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
                         data-show-faces="false" data-width="60" data-send="false" data-layout="<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewrsoccount_on']->value == 1) {?>button_count<?php } else { ?>button<?php }?>"></div>
                                    
                    <?php echo '<script'; ?>
 type="text/javascript">


                        <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>document.addEventListener("DOMContentLoaded", function(event) { <?php }?>
                            $(document).ready(function(){

                                /* Voucher, when a user share review on the Facebook */
                                // like
                                FB.Event.subscribe("edge.create", function(targetUrlReview) {

                                    if(targetUrlReview == '<?php echo call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['spmgsnipreviewrev_url']->value,'htmlall','UTF-8' ));
if ($_smarty_tpl->tpl_vars['spmgsnipreviewis_rewrite']->value == 1) {?>?<?php } else { ?>&<?php }?>rid=<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
'){

                                        addRemoveDiscountShareReview('facebook',<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['review']->value['id'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
);

                                    }
                                });
                                /* Voucher, when a user share review on the Facebook */

                            });
                            <?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewis17']->value == 1) {?>}); <?php }?>

                    <?php echo '</script'; ?>
>
                


                <?php }?>

                <?php }?>

            </div>
            <div class="clear-spmgsnipreview"></div>

        </div>






    </div>
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>



<?php if ($_smarty_tpl->tpl_vars['spmgsnipreviewd_eff_rev']->value != "disable_all_effects") {?>

    <?php echo '<script'; ?>
 type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            $(document).ready(function(){
                spmgsnipreview_init_effects_review();
            });
        });
    <?php echo '</script'; ?>
>

<?php }?>

<?php } else { ?>
    <div class="advertise-text-review advertise-text-review-text-align">
        <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No reviews for the product','mod'=>'spmgsnipreview'),$_smarty_tpl ) );?>


    </div>

<?php }?>
<!-- end /home/afrissim/public_html/modules/spmgsnipreview/views/templates/front/list_reviews.tpl --><?php }
}
