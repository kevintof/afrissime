<?php
/* Smarty version 3.1.33, created on 2019-10-28 16:04:17
  from '/home/afrissim/public_html/modules/payexpresse/views/templates/admin/config.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db71181a8c720_78658877',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dca0962e20a607a3c78ae6a50062e0649ec26aaf' => 
    array (
      0 => '/home/afrissim/public_html/modules/payexpresse/views/templates/admin/config.tpl',
      1 => 1569573406,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db71181a8c720_78658877 (Smarty_Internal_Template $_smarty_tpl) {
echo $_smarty_tpl->tpl_vars['output']->value;?>


<div class="pe_header">
    <div class="pe_headerConfig pe_active" value="1">
        A-propos
    </div>
    <div class="pe_headerConfig" value="2">
        Configuration
    </div>

</div>

<div class="pe_container">
    <div class="pe_produit">
        <div class="pe_contenerLogo">
            <img src="<?php echo $_smarty_tpl->tpl_vars['path']->value;?>
views/img/payexpresse.png">
        </div><br><br><br><br>
        <div class="pe_text">
            PayExpresse la meilleure <br>plateforme de paiement en ligne <br> Web & Mobile
        </div><br><br><br><br>
        <div class="pe_textLineBloc">
            <h1>Paiement rapide</h1>
            PayExpresse propose sa plateforme sécurisée<br> de paiement
            en ligne pour faciliter les transactions <br>entre les professionnels
            et leurs clients, avec ou sans site Web.
        </div>
        <div class="pe_textLineBloc">
           <h1>Service sécurisé</h1>
            Les contrôles de fraude, les mises à jour et <br>
            vérifications nécessaires à la protection et <br>
            à la sécurité de vos transactions
            font partie <br>de nos tâches quotidiennes.
        </div>

    </div>
    <div class="pe_parametre">
        <div>
            <?php echo $_smarty_tpl->tpl_vars['seting']->value;?>

        </div>
        <div class="formGroup">

        </div>
    </div>

    <div class="pe_footerConfig">
    <div style="font-size: 40px;text-align: center;padding-top: 10px">
        Coryright PayExpresse <?php echo date("Y");?>

    </div>
    </div>

</div>
<?php }
}
