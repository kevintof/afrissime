<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:14:30
  from '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/presta-1.7/customer-account-sponsorship.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db705d6993a84_13450223',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'daa89c9418f80542cbf938b7caee2a52a38a9990' => 
    array (
      0 => '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/presta-1.7/customer-account-sponsorship.tpl',
      1 => 1571236966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db705d6993a84_13450223 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- MODULE allinone_rewards -->
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="sponsorship-link" href="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['url'][0], array( array('entity'=>'module','name'=>'allinone_rewards','controller'=>'sponsorship'),$_smarty_tpl ) );?>
"><span class="link-item"><i class="material-icons">&#xE80D;</i><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sponsorship program','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</span></a>
<!-- END : MODULE allinone_rewards --><?php }
}
