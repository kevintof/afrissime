<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:15:20
  from '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/shopping-cart.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70608e69893_28851279',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa98831200a6574be0cba0f38851c22de434d648' => 
    array (
      0 => '/home/afrissim/public_html/modules/allinone_rewards/views/templates/hook/shopping-cart.tpl',
      1 => 1571236966,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db70608e69893_28851279 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- MODULE allinone_rewards -->
<div id="reward_loyalty">
	<?php if ($_smarty_tpl->tpl_vars['display_credits']->value > 0) {?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'By checking out this shopping cart you will collect ','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
 <b><?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['credits']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
</b>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'that can be converted into a voucher for a future purchase.','mod'=>'allinone_rewards'),$_smarty_tpl ) );
if (isset($_smarty_tpl->tpl_vars['guest_checkout']->value) && $_smarty_tpl->tpl_vars['guest_checkout']->value) {?><sup>*</sup><?php }?><br />
		<?php if (isset($_smarty_tpl->tpl_vars['guest_checkout']->value) && $_smarty_tpl->tpl_vars['guest_checkout']->value) {?><span><sup>*</sup> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Not available for Instant checkout order','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>
</span><?php }?>
	<?php } else { ?>
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add some products to your shopping cart to collect some loyalty credits.','mod'=>'allinone_rewards'),$_smarty_tpl ) );?>

	<?php }?>
</div>
<!-- END : MODULE allinone_rewards --><?php }
}
