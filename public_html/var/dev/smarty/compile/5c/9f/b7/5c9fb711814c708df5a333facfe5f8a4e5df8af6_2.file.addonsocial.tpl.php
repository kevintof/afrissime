<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:16:01
  from '/home/afrissim/public_html/themes/jms_basel/modules/jmspagebuilder/views/templates/hook/addonsocial.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f82190c7c8_99483457',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5c9fb711814c708df5a333facfe5f8a4e5df8af6' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/modules/jmspagebuilder/views/templates/hook/addonsocial.tpl',
      1 => 1570556981,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6f82190c7c8_99483457 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div id="social_block">

	<ul class="find-us">

       <?php if ($_smarty_tpl->tpl_vars['facebook_url']->value != '') {?><li class="divider"><a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['facebook_url']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="facebook" target="_blank"><span class="fa fa-facebook"></span></a></li><?php }?>

       <?php if ($_smarty_tpl->tpl_vars['twitter_url']->value != '') {?><li class="divider"><a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['twitter_url']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="twitter" target="_blank"><span class="fa fa-twitter"></span></a></li><?php }?>

       <?php if ($_smarty_tpl->tpl_vars['linkedin_url']->value != '') {?><li class="divider"><a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['linkedin_url']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="linkedin" target="_blank"><span class="fa fa-linkedin"></span></a></li><?php }?>

       <?php if ($_smarty_tpl->tpl_vars['youtube_url']->value != '') {?><li class="divider"><a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['youtube_url']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="youtube" target="_blank"><span class="fa fa-youtube"></span></a></li><?php }?>

       <?php if ($_smarty_tpl->tpl_vars['google_plus_url']->value != '') {?><li class="divider"><a href="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['google_plus_url']->value,'html','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" class="gplus"><span class="fa fa-google-plus"></span></a></li><?php }?>

		<?php if ($_smarty_tpl->tpl_vars['pinterest_url']->value != '') {?><li class="divider"><a href="https://www.pinterest.com/afrissime" class="pinterest" target="_blank"><span class="fa fa-pinterest"></span></a></li><?php }?>            

		 <?php if ($_smarty_tpl->tpl_vars['instagram_url']->value != '') {?>

        <li class="divider">

            <a href="https://www.instagram.com/afrissime" class="instagram" target="_blank">

            <span class="fa fa-instagram"></span>

            </a>

        </li>

        <?php }?>
        <li class="divider"><a href="https://wa.me/22892626943" class="whatsapp" target="_blank"><span class="fa fa-whatsapp"></span></a></li>
        <li class="divider"><a href="https://t.me/Afrissime" class="telegram" target="_blank"><span class="fa fa-telegram"></span></a></li>
        <li class="divider"><a href="https://www.pscp.tv/afrissime" target="_blank"><span class="periscope" ></span></a></li>
        <li class="divider"><a href="https://soundcloud.com/afrissime" target="_blank"><span class="soundcloud"></span></a></li>
        <li class="divider"><a href="https://www.flickr.com/photos/184895339@N06" target="_blank"><span class="flickr"></span></a></li>
    </ul>

</div>

<?php }
}
