<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:16:00
  from '/home/afrissim/public_html/themes/jms_basel/modules/jmspagebuilder/views/templates/hook/addonhtml.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f820b985c2_93316510',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c17459698e2da8be170ee59f83e8be8624786c8d' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/modules/jmspagebuilder/views/templates/hook/addonhtml.tpl',
      1 => 1566831866,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6f820b985c2_93316510 (Smarty_Internal_Template $_smarty_tpl) {
if ($_smarty_tpl->tpl_vars['addon_title']->value) {?>
<div class="addon-title">
	<h3><?php echo $_smarty_tpl->tpl_vars['addon_title']->value;?>
</h3>
</div>
<?php }
if ($_smarty_tpl->tpl_vars['addon_desc']->value) {?>
<p class="addon-desc"><?php echo $_smarty_tpl->tpl_vars['addon_desc']->value;?>
</p>
<?php }
echo $_smarty_tpl->tpl_vars['html_content']->value;
}
}
