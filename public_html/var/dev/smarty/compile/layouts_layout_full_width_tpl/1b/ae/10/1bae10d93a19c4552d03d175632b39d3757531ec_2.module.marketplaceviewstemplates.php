<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:17:26
  from 'module:marketplaceviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f87697b8b5_21018746',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1bae10d93a19c4552d03d175632b39d3757531ec' => 
    array (
      0 => 'module:marketplaceviewstemplates',
      1 => 1567446696,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6f87697b8b5_21018746 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/afrissim/public_html/modules/marketplace/views/templates/front/_partials/mp-form-fields-notification.tpl -->
<label class="wk_formfield_required_notify">
	<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Those fields which has been marked','mod'=>'marketplace'),$_smarty_tpl ) );?>
 (<span class="required">*</span>) <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'are compulsory to be filled by you.','mod'=>'marketplace'),$_smarty_tpl ) );?>

</label><!-- end /home/afrissim/public_html/modules/marketplace/views/templates/front/_partials/mp-form-fields-notification.tpl --><?php }
}
