<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:16:00
  from '/home/afrissim/public_html/themes/jms_basel/templates/page.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f8200e5d81_39585429',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '8884f5711518b11243e9689cf0295524f0cc31a6' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/templates/page.tpl',
      1 => 1571755509,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6f8200e5d81_39585429 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
 
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['layout']->value, ENT_QUOTES, 'UTF-8');?>



<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11427556845db6f8200dc5d4_73666855', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'page_content_top'} */
class Block_10787128005db6f8200e25c3_05502932 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'page_content_top'} */
/* {block 'page_content'} */
class Block_7160683975db6f8200e3344_06812073 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

          <!-- Page content -->
        <?php
}
}
/* {/block 'page_content'} */
/* {block 'page_content_container'} */
class Block_4181472095db6f8200dd897_52236566 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <section id="content" class="page-content card <?php if (($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'acheter') && ($_smarty_tpl->tpl_vars['page']->value['page_name'] != 'pageshop')) {?>card-block <?php }?> row">
        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10787128005db6f8200e25c3_05502932', 'page_content_top', $this->tplIndex);
?>

        <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_7160683975db6f8200e3344_06812073', 'page_content', $this->tplIndex);
?>

      </section>
    <?php
}
}
/* {/block 'page_content_container'} */
/* {block 'page_footer_container'} */
class Block_3326303465db6f8200e4b47_21457725 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

     
    <?php
}
}
/* {/block 'page_footer_container'} */
/* {block 'content'} */
class Block_11427556845db6f8200dc5d4_73666855 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_11427556845db6f8200dc5d4_73666855',
  ),
  'page_content_container' => 
  array (
    0 => 'Block_4181472095db6f8200dd897_52236566',
  ),
  'page_content_top' => 
  array (
    0 => 'Block_10787128005db6f8200e25c3_05502932',
  ),
  'page_content' => 
  array (
    0 => 'Block_7160683975db6f8200e3344_06812073',
  ),
  'page_footer_container' => 
  array (
    0 => 'Block_3326303465db6f8200e4b47_21457725',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">
	
    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4181472095db6f8200dd897_52236566', 'page_content_container', $this->tplIndex);
?>


    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_3326303465db6f8200e4b47_21457725', 'page_footer_container', $this->tplIndex);
?>


  </section>

<?php
}
}
/* {/block 'content'} */
}
