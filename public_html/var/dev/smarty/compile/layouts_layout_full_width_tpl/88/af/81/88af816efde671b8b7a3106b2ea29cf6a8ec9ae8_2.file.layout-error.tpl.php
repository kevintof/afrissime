<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:34:35
  from '/home/afrissim/public_html/themes/jms_basel/templates/layouts/layout-error.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6fc7bc838b1_61125932',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '88af816efde671b8b7a3106b2ea29cf6a8ec9ae8' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/templates/layouts/layout-error.tpl',
      1 => 1566831868,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db6fc7bc838b1_61125932 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_15780869365db6fc7bc7f6a3_51119338', 'head_seo');
?>

    <meta name="viewport" content="width=device-width, initial-scale=1">

      </head>

  <body>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_11318591465db6fc7bc83142_87687662', 'content');
?>


    <!-- Load JS files here -->

  </body>

</html>
<?php }
/* {block 'head_seo_title'} */
class Block_16018803445db6fc7bc80127_07605132 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_title'} */
/* {block 'head_seo_description'} */
class Block_20157943475db6fc7bc81915_27646586 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_description'} */
/* {block 'head_seo_keywords'} */
class Block_19887441715db6fc7bc82143_28939002 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_keywords'} */
/* {block 'head_seo'} */
class Block_15780869365db6fc7bc7f6a3_51119338 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_seo' => 
  array (
    0 => 'Block_15780869365db6fc7bc7f6a3_51119338',
  ),
  'head_seo_title' => 
  array (
    0 => 'Block_16018803445db6fc7bc80127_07605132',
  ),
  'head_seo_description' => 
  array (
    0 => 'Block_20157943475db6fc7bc81915_27646586',
  ),
  'head_seo_keywords' => 
  array (
    0 => 'Block_19887441715db6fc7bc82143_28939002',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <title><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16018803445db6fc7bc80127_07605132', 'head_seo_title', $this->tplIndex);
?>
</title>
      <meta name="description" content="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_20157943475db6fc7bc81915_27646586', 'head_seo_description', $this->tplIndex);
?>
">
      <meta name="keywords" content="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19887441715db6fc7bc82143_28939002', 'head_seo_keywords', $this->tplIndex);
?>
">
    <?php
}
}
/* {/block 'head_seo'} */
/* {block 'content'} */
class Block_11318591465db6fc7bc83142_87687662 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_11318591465db6fc7bc83142_87687662',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <p>Hello world! This is HTML5 Boilerplate.</p>
    <?php
}
}
/* {/block 'content'} */
}
