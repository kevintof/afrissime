<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:57:05
  from 'module:marketplaceviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70fd19f5243_05111533',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '99bf29abc9440bfb87205f0e4eed0552cd7e9beb' => 
    array (
      0 => 'module:marketplaceviewstemplates',
      1 => 1567446696,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:marketplace/views/templates/front/dashboard/_partials/view-graph.tpl' => 1,
    'module:marketplace/views/templates/front/dashboard/_partials/recent-orders.tpl' => 1,
  ),
),false)) {
function content_5db70fd19f5243_05111533 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!-- begin /home/afrissim/public_html/modules/marketplace/views/templates/front/dashboard/dashboard.tpl -->

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19522358645db70fd19c0eb3_06256623', 'content');
?>
<!-- end /home/afrissim/public_html/modules/marketplace/views/templates/front/dashboard/dashboard.tpl --><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'mp-view-graph'} */
class Block_17980068905db70fd19e9a65_42238864 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

							<?php $_smarty_tpl->_subTemplateRender('module:marketplace/views/templates/front/dashboard/_partials/view-graph.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
						<?php
}
}
/* {/block 'mp-view-graph'} */
/* {block 'mp-recent-orders'} */
class Block_4663587835db70fd19ed6d5_73920335 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

									<?php $_smarty_tpl->_subTemplateRender('module:marketplace/views/templates/front/dashboard/_partials/recent-orders.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
								<?php
}
}
/* {/block 'mp-recent-orders'} */
/* {block 'content'} */
class Block_19522358645db70fd19c0eb3_06256623 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_19522358645db70fd19c0eb3_06256623',
  ),
  'mp-view-graph' => 
  array (
    0 => 'Block_17980068905db70fd19e9a65_42238864',
  ),
  'mp-recent-orders' => 
  array (
    0 => 'Block_4663587835db70fd19ed6d5_73920335',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_checkPlugins(array(0=>array('file'=>'/home/afrissim/public_html/vendor/smarty/smarty/libs/plugins/modifier.date_format.php','function'=>'smarty_modifier_date_format',),));
?>

<?php if ($_smarty_tpl->tpl_vars['logged']->value) {?>
	<div class="wk-mp-block">
		<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayMpMenu"),$_smarty_tpl ) );?>

		<div class="wk-mp-content">
			<div class="page-title" style="background-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title_bg_color']->value, ENT_QUOTES, 'UTF-8');?>
;">
				<span style="color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title_text_color']->value, ENT_QUOTES, 'UTF-8');?>
;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'My Dashboard','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span>
			</div>
			<div class="clearfix wk-mp-right-column">
								<div class="row">
					<div class="col-sm-12">
						<p>
							<strong><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Hello','mod'=>'marketplace'),$_smarty_tpl ) );?>
, <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['seller_name']->value, ENT_QUOTES, 'UTF-8');?>
!</strong>
							<br>
							<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'From your My Account Dashboard, You have the ability to view a snapshot of your recent account activity and update your account information.','mod'=>'marketplace'),$_smarty_tpl ) );?>

						</p>
					</div>
				</div>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayMpDashboardTop'),$_smarty_tpl ) );?>

				<div class="dashboard-date-content">
					<div class="row">
						<div class="col-md-7">
							<div class="btn-group margin-btm-10">
																<button data-trigger-function="Day" data-date-range="1" class="btn setPreselectDateRange <?php if ($_smarty_tpl->tpl_vars['preselectDateRange']->value == '1') {?>btn-primary<?php } else { ?>btn-default<?php }?>" type="button">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Day','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</button>
																<button data-trigger-function="Month" data-date-range="2" class="btn setPreselectDateRange <?php if ($_smarty_tpl->tpl_vars['preselectDateRange']->value == '2') {?>btn-primary<?php } else { ?>btn-default<?php }?>" type="button">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Month','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</button>
																<button data-trigger-function="Year" data-date-range="3" class="btn setPreselectDateRange <?php if ($_smarty_tpl->tpl_vars['preselectDateRange']->value == '3') {?>btn-primary<?php } else { ?>btn-default<?php }?>" type="button">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Year','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</button>
																<button data-trigger-function="PreviousDay" data-date-range="4" class="btn setPreselectDateRange <?php if ($_smarty_tpl->tpl_vars['preselectDateRange']->value == '4') {?>btn-primary<?php } else { ?>btn-default<?php }?>" type="button">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Day-1','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</button>
																<button data-trigger-function="PreviousMonth" data-date-range="5" class="btn setPreselectDateRange <?php if ($_smarty_tpl->tpl_vars['preselectDateRange']->value == '5') {?>btn-primary<?php } else { ?>btn-default<?php }?>" type="button">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Month-1','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</button>
																<button data-trigger-function="PreviousYear" data-date-range="6" class="btn setPreselectDateRange <?php if ($_smarty_tpl->tpl_vars['preselectDateRange']->value == '6') {?>btn-primary<?php } else { ?>btn-default<?php }?>" type="button">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Year-1','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</button>
							</div>
						</div>
						<div class="col-md-5">
							<input type="hidden" id="dashboardDateFrom" name="dashboardDateFrom" value="<?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['dateFrom']->value,"%Y-%m-%d"), ENT_QUOTES, 'UTF-8');?>
">
							<input type="hidden" id="dashboardDateTo" name="dashboardDateTo" value="<?php echo htmlspecialchars(smarty_modifier_date_format($_smarty_tpl->tpl_vars['dateTo']->value,"%Y-%m-%d"), ENT_QUOTES, 'UTF-8');?>
">
							<input type="hidden" name="preselectDateRange" id="preselectDateRange" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['preselectDateRange']->value, ENT_QUOTES, 'UTF-8');?>
">
							<div class="input-group">
								<span class="input-group-addon"><i class="material-icons">&#xE8A3;</i></span>
								<input type="text" class="form-control" id="date-range-picker">
							</div>
						</div>
					</div>
				</div>
				<div class="panel">
					<h4><i class="material-icons">&#xE1B8;</i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Stats Graph','mod'=>'marketplace'),$_smarty_tpl ) );?>
</h4>
					<?php if (Configuration::get('WK_MP_DASHBOARD_GRAPH') == '1') {?>
						<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Graph is showing on the basis of Payment Accepted Orders.','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span>
					<?php } elseif (Configuration::get('WK_MP_DASHBOARD_GRAPH') == '2') {?>
						<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Graph is showing on the basis of Confirmed Orders.','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span>
					<?php }?>
					<?php if (Module::isEnabled('mpshipping')) {?>
						<br>
						<span><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sales will calculate on basis of product(tax excl.) price only. Shipping amount will not calculate in sales.','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span>
					<?php }?>
					<br><br>
					<div class="panel-content">
						<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_17980068905db70fd19e9a65_42238864', 'mp-view-graph', $this->tplIndex);
?>

					</div>
				</div>
				<hr>
				<div class="panel">
					<h4><i class="material-icons">&#xE870;</i> <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Recent Orders','mod'=>'marketplace'),$_smarty_tpl ) );?>
</h4>
					<div class="panel-content">
						<div class="row tabs">
							<div class="col-sm-12">
								<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4663587835db70fd19ed6d5_73920335', 'mp-recent-orders', $this->tplIndex);
?>

							</div>
						</div>
					</div>
				</div>
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayMpDashboardBottom'),$_smarty_tpl ) );?>

			</div>
		</div>
	</div>
<?php }?>
<link href="<?php echo htmlspecialchars(@constant('_MODULE_DIR_'), ENT_QUOTES, 'UTF-8');?>
marketplace/views/css/libs/graph/nv.d3.css" rel="stylesheet">
<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars(@constant('_MODULE_DIR_'), ENT_QUOTES, 'UTF-8');?>
marketplace/views/js/libs/graph/d3.v3.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="<?php echo htmlspecialchars(@constant('_MODULE_DIR_'), ENT_QUOTES, 'UTF-8');?>
marketplace/views/js/libs/graph/nv.d3.min.js"><?php echo '</script'; ?>
>
<?php
}
}
/* {/block 'content'} */
}
