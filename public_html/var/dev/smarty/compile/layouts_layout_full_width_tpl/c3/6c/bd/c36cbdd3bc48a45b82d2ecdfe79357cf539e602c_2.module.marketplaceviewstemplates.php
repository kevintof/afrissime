<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:57:05
  from 'module:marketplaceviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70fd1e88da7_84626187',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c36cbdd3bc48a45b82d2ecdfe79357cf539e602c' => 
    array (
      0 => 'module:marketplaceviewstemplates',
      1 => 1567446696,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db70fd1e88da7_84626187 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/afrissim/public_html/modules/marketplace/views/templates/front/dashboard/_partials/view-graph.tpl -->
<div class="row">
    <div class="col-sm-12">
        <section class="panel wk-graph">
            <div>
                <div class="col-xs-6 col-md-6 dashboard-options wk-sales" onclick="selectDashtrendsChart(this, 'sales');" style="border-left: none;background-color: #1777b6;color: #fff;">
                    <div class="dash-item"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sales','mod'=>'marketplace'),$_smarty_tpl ) );?>
</div>
                                        <div class="data_value"><span id="sales_score"></span></div>
                    <div class="dash_trend"><span id="sales_score_trends"></span></div>
                </div>
                <div class="col-xs-6 col-md-6 dashboard-options wk-orders" onclick="selectDashtrendsChart(this, 'orders');">
                    <div class="dash-item"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Orders','mod'=>'marketplace'),$_smarty_tpl ) );?>
</div>
                                        <div class="data_value"><span id="orders_score"></span></div>
                    <div class="dash_trend"><span id="orders_score_trends"></span></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="wk-dashboad-graph-chart">
                <svg></svg>
            </div>
        </section>
    </div>
</div><!-- end /home/afrissim/public_html/modules/marketplace/views/templates/front/dashboard/_partials/view-graph.tpl --><?php }
}
