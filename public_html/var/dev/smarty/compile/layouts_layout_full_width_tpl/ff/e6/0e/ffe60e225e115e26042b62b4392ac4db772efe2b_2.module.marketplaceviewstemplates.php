<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:57:05
  from 'module:marketplaceviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70fd1ebf646_58039287',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ffe60e225e115e26042b62b4392ac4db772efe2b' => 
    array (
      0 => 'module:marketplaceviewstemplates',
      1 => 1567446696,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db70fd1ebf646_58039287 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/afrissim/public_html/modules/marketplace/views/templates/front/dashboard/_partials/recent-orders.tpl -->
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="6%"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'ID','mod'=>'marketplace'),$_smarty_tpl ) );?>
</th>
                <th width="10%"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Reference','mod'=>'marketplace'),$_smarty_tpl ) );?>
</th>
                <th width="15%"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Customer','mod'=>'marketplace'),$_smarty_tpl ) );?>
</th>
                <th width="12%"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Total','mod'=>'marketplace'),$_smarty_tpl ) );?>
</th>
                <th width="20%"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Status','mod'=>'marketplace'),$_smarty_tpl ) );?>
</th>
                <th width="17%"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Payment','mod'=>'marketplace'),$_smarty_tpl ) );?>
</th>
                <th><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Date','mod'=>'marketplace'),$_smarty_tpl ) );?>
</th>
            </tr>
        </thead>
        <tbody>
            <?php if (isset($_smarty_tpl->tpl_vars['recentOrders']->value) && $_smarty_tpl->tpl_vars['recentOrders']->value) {?>
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['recentOrders']->value, 'order');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['order']->value) {
?>
                    <tr class="mp_order_row" is_id_order="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['id_order'], ENT_QUOTES, 'UTF-8');?>
">
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['id_order'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['reference'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['buyer_info']->firstname, ENT_QUOTES, 'UTF-8');?>
 <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['buyer_info']->lastname, ENT_QUOTES, 'UTF-8');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['total_paid'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['order_status'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['order']->value['payment_mode'], ENT_QUOTES, 'UTF-8');?>
</td>
                        <td><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['dateFormat'][0], array( array('date'=>$_smarty_tpl->tpl_vars['order']->value['date_add'],'full'=>1),$_smarty_tpl ) );?>
</td>
                    </tr>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
            <?php } else { ?>
                <tr><td colspan="7"><center><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'No orders found','mod'=>'marketplace'),$_smarty_tpl ) );?>
</center></td>
            <?php }?>
        </tbody>
    </table>
</div>
<?php if ($_smarty_tpl->tpl_vars['totalOrdersCount']->value > 5) {?>
<p class="wk_text_right">
    <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','mporder'), ENT_QUOTES, 'UTF-8');?>
">
        <button class="btn btn-primary btn-sm" type="button"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'View All Orders','mod'=>'marketplace'),$_smarty_tpl ) );?>
</button>
    </a>
</p>
<?php }?><!-- end /home/afrissim/public_html/modules/marketplace/views/templates/front/dashboard/_partials/recent-orders.tpl --><?php }
}
