<?php
/* Smarty version 3.1.33, created on 2019-10-28 15:15:20
  from '/home/afrissim/public_html/themes/jms_basel/templates/checkout/cart.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db70608888318_50417344',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '78a34a0c4538996400ea3e2659ec7bb9b8d95291' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/templates/checkout/cart.tpl',
      1 => 1566831868,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:checkout/_partials/cart-detailed.tpl' => 1,
    'file:checkout/_partials/cart-detailed-totals.tpl' => 1,
    'file:checkout/_partials/cart-detailed-actions.tpl' => 1,
  ),
),false)) {
function content_5db70608888318_50417344 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>


<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_5772391325db7060887c836_98294470', 'content');
?>

<?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block 'cart_overview'} */
class Block_10079913565db7060887e7f2_03480683 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

				<?php $_smarty_tpl->_subTemplateRender('file:checkout/_partials/cart-detailed.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cart'=>$_smarty_tpl->tpl_vars['cart']->value), 0, false);
?>
			  <?php
}
}
/* {/block 'cart_overview'} */
/* {block 'continue_shopping'} */
class Block_16811470695db70608881463_49279926 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

			  <a class="btn-default btn-shopping button-small btn-effect" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['urls']->value['pages']['index'], ENT_QUOTES, 'UTF-8');?>
">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Continue shopping','d'=>'Shop.Theme.Actions'),$_smarty_tpl ) );?>

			  </a>
			<?php
}
}
/* {/block 'continue_shopping'} */
/* {block 'cart_totals'} */
class Block_6840563285db70608885126_71310366 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

				  <?php $_smarty_tpl->_subTemplateRender('file:checkout/_partials/cart-detailed-totals.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cart'=>$_smarty_tpl->tpl_vars['cart']->value), 0, false);
?>
				<?php
}
}
/* {/block 'cart_totals'} */
/* {block 'cart_actions'} */
class Block_19547124505db70608886046_78958797 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

				  <?php $_smarty_tpl->_subTemplateRender('file:checkout/_partials/cart-detailed-actions.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('cart'=>$_smarty_tpl->tpl_vars['cart']->value), 0, false);
?>
				<?php
}
}
/* {/block 'cart_actions'} */
/* {block 'cart_summary'} */
class Block_940621395db70608884510_55487115 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

			  <div class="card cart-summary">

				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayShoppingCart'),$_smarty_tpl ) );?>


				<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_6840563285db70608885126_71310366', 'cart_totals', $this->tplIndex);
?>


				<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_19547124505db70608886046_78958797', 'cart_actions', $this->tplIndex);
?>


			  </div>
			<?php
}
}
/* {/block 'cart_summary'} */
/* {block 'display_reassurance'} */
class Block_14265840255db706088871a5_10494635 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

			  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayReassurance'),$_smarty_tpl ) );?>

			<?php
}
}
/* {/block 'display_reassurance'} */
/* {block 'content'} */
class Block_5772391325db7060887c836_98294470 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_5772391325db7060887c836_98294470',
  ),
  'cart_overview' => 
  array (
    0 => 'Block_10079913565db7060887e7f2_03480683',
  ),
  'continue_shopping' => 
  array (
    0 => 'Block_16811470695db70608881463_49279926',
  ),
  'cart_summary' => 
  array (
    0 => 'Block_940621395db70608884510_55487115',
  ),
  'cart_totals' => 
  array (
    0 => 'Block_6840563285db70608885126_71310366',
  ),
  'cart_actions' => 
  array (
    0 => 'Block_19547124505db70608886046_78958797',
  ),
  'display_reassurance' => 
  array (
    0 => 'Block_14265840255db706088871a5_10494635',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>


  <section id="main">
	 <div class="cart-block">
            <h1><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your Shopping Cart','d'=>'Shop.Theme.Checkout'),$_smarty_tpl ) );?>
</h1>
          </div>
    <div class="cart-grid">
		<div class="row first">
      <!-- Left Block: cart product informations & shpping -->
      <div class="cart-grid-body col-xs-12 col-lg-8">
		<div class="cart-box">
			<!-- cart products detailed -->
			<div class="cart cart-container">
			  <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10079913565db7060887e7f2_03480683', 'cart_overview', $this->tplIndex);
?>

			</div>

			<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_16811470695db70608881463_49279926', 'continue_shopping', $this->tplIndex);
?>


			<!-- shipping informations -->
			<div>
			  <?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayShoppingCartFooter'),$_smarty_tpl ) );?>

			</div>
		</div>
      </div>

      <!-- Right Block: cart subtotal & cart total -->
      <div class="cart-grid-right col-xs-12 col-lg-4">
		<div class="right-box">
			<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_940621395db70608884510_55487115', 'cart_summary', $this->tplIndex);
?>


			<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_14265840255db706088871a5_10494635', 'display_reassurance', $this->tplIndex);
?>

		</div>
      </div>
	</div>
    </div>
  </section>
<?php
}
}
/* {/block 'content'} */
}
