<?php
/* Smarty version 3.1.33, created on 2019-10-28 14:17:26
  from 'module:marketplaceviewstemplates' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db6f87623f736_67252273',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd7acb4a9711afb5332a53e0c27419f55350c8aab' => 
    array (
      0 => 'module:marketplaceviewstemplates',
      1 => 1572271850,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
    'module:marketplace/views/templates/front/_partials/mp-form-fields-notification.tpl' => 1,
  ),
),false)) {
function content_5db6f87623f736_67252273 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, true);
?>
<!-- begin /home/afrissim/public_html/modules/marketplace/views/templates/front/seller/sellerrequest.tpl -->

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_2788802125db6f8761d9570_46873384', "head");
?>

<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_937865db6f8761dc4a0_90381451', 'content');
?>
<!-- end /home/afrissim/public_html/modules/marketplace/views/templates/front/seller/sellerrequest.tpl --><?php $_smarty_tpl->inheritance->endChild($_smarty_tpl, $_smarty_tpl->tpl_vars['layout']->value);
}
/* {block "head"} */
class Block_2788802125db6f8761d9570_46873384 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head' => 
  array (
    0 => 'Block_2788802125db6f8761d9570_46873384',
  ),
);
public $prepend = 'true';
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>



 <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/css/intlTelInput.css" rel="stylesheet" type="text/css"/>
<?php echo '<script'; ?>
>
// hack to get the plugin to work without loading jQuery
window.jQuery = window.$ = function() {
  return {
    on: function() {}
  };
};
window.$.fn = {};
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/js/intlTelInput.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/js/utils.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    $("#phone").intlTelInput({
	 initialCountry: "auto",
          geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
          utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.1.0/js/utils.js" // just for formatting/placeholders etc
    });
  <?php echo '</script'; ?>
> 


<?php
}
}
/* {/block "head"} */
/* {block 'mp-form-fields-notification'} */
class Block_18332891535db6f876217851_41811887 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

						<?php $_smarty_tpl->_subTemplateRender('module:marketplace/views/templates/front/_partials/mp-form-fields-notification.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
					<?php
}
}
/* {/block 'mp-form-fields-notification'} */
/* {block 'content'} */
class Block_937865db6f8761dc4a0_90381451 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_937865db6f8761dc4a0_90381451',
  ),
  'mp-form-fields-notification' => 
  array (
    0 => 'Block_18332891535db6f876217851_41811887',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars(@constant('_MODULE_DIR_'), ENT_QUOTES, 'UTF-8');?>
marketplace/views/js/tinymce/tinymce.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript" src="<?php echo htmlspecialchars(@constant('_MODULE_DIR_'), ENT_QUOTES, 'UTF-8');?>
marketplace/views/js/tinymce/tinymce_wk_setup.js"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
function show_box_gender(){
  $('#box_gender').show();
}
function hide_box_gender(){
  $('#box_gender').hide();
}
function show_payment_box_1(){
	$('#payment_box_1').show();
	$('#payment_box_2').hide();
	$('#payment_box_3').hide();
}
function show_payment_box_2(){
	$('#payment_box_1').hide();
	$('#payment_box_2').show();
	$('#payment_box_3').show();
}
<?php echo '</script'; ?>
>

<input type="hidden" name="token" id="wk-static-token" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['static_token']->value, ENT_QUOTES, 'UTF-8');?>
">
<?php if (isset($_smarty_tpl->tpl_vars['is_seller']->value)) {?>
	
		<?php if ($_smarty_tpl->tpl_vars['is_seller']->value == 0) {?>
			<div class="alert alert-info">
			</div>
		<?php } else { ?>
			<div class="alert alert-info">
				<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Your request has been approved by admin. ','mod'=>'marketplace'),$_smarty_tpl ) );?>

				<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','addproduct'), ENT_QUOTES, 'UTF-8');?>
">
					<button class="btn btn-primary btn-sm">
						<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Add Your First Product','mod'=>'marketplace'),$_smarty_tpl ) );?>

					</button>
				</a>
			</div>
		<?php }
} else { ?>
	<div class="wk-mp-block" style="border:1px solid #d5d5d5;">
		<div class="page-title" style="background-color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title_bg_color']->value, ENT_QUOTES, 'UTF-8');?>
;">
			<span style="color:<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['title_text_color']->value, ENT_QUOTES, 'UTF-8');?>
;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Seller Request','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span>
		</div>
		<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getModuleLink('marketplace','sellerrequest'), ENT_QUOTES, 'UTF-8');?>
" method="post" id="wk_mp_seller_form" enctype="multipart/form-data">
			<div class="wk-mp-right-column">
				<div class="alert alert-danger wk_display_none" id="wk_mp_form_error"></div>
				<!-- progressbar -->
	            <ul id="progressbar">
	                <li class="active">Renseignements personnels</li>
	                <li>Renseignements boutique</li>
	                <li>Conditions générales de vente</li>
	            </ul>
				<fieldset>
					 <h2 class="fs-title">Renseignements personnels</h2>
                	<h3 class="fs-subtitle">Parlez nous un peu de vous</h3>
					<input type="hidden" name="current_lang" id="current_lang" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['current_lang']->value['id_lang'], ENT_QUOTES, 'UTF-8');?>
">
				
					<input type="hidden" name="default_lang" value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['context_language']->value, ENT_QUOTES, 'UTF-8');?>
" />
					
					<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_lastname" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Last Name','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
							<input class="form-control"
							type="text"
							value="<?php if (isset($_POST['seller_lastname'])) {
echo htmlspecialchars($_POST['seller_lastname'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['customer_lastname']->value, ENT_QUOTES, 'UTF-8');
}?>"
							name="seller_lastname"
							id="seller_lastname" required="required"/>
						</div>
						<div class="col-md-6">
							<label for="seller_firstname" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'First Name','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
							<input class="form-control"
							type="text"
							value="<?php if (isset($_POST['seller_firstname'])) {
echo htmlspecialchars($_POST['seller_firstname'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['customer_firstname']->value, ENT_QUOTES, 'UTF-8');
}?>"
							name="seller_firstname"
							id="seller_firstname" required="required"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<div class="row">
								<label for="seller_gender" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Sexe','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
							</div>
							<div class="row">
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="1" checked onclick="hide_box_gender();">
						              <span></span>
						            </span>
						          Homme
						        </label>
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="2" onclick="hide_box_gender();">
						              <span></span>
						            </span>
						           Femme
						        </label>
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="3" onclick="show_box_gender();">
						              <span></span>
						            </span>
						           Personnalisé
						        </label>
					        </div>
					        <div id="box_gender" style="display:none">
					        	<div class="row">
					        		<span>Indiquer votre sexe</span>
					        		<p>Me désigner avec le pronom suivant: </p>
					        		 <input class="form-control" name="seller_title" type="text" value="" placeholder="Pronom">
					        	</div>
					        </div>
					    </div>
					    <div class="col-md-6">
					    	<label for="seller_birthday" class="control-label">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Date d\'anniversaire','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
					        <input class="form-control" name="seller_birthday" type="text" value="" placeholder="DD/MM/YYYY" required="required">
					                  <span class="form-control-comment">
					            (Ex. : 31/05/1970)
					          </span>
					    </div>
				    </div>
			    	<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_nationality" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Nationalité','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
							<select name="seller_nationality" id="seller_nationality" class="form-control form-control-select" required="required" style="height:40px; width:100%">
								<option value=""><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select Country','mod'=>'marketplace'),$_smarty_tpl ) );?>
</option>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['african_country']->value, 'countrydetail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['countrydetail']->value) {
?>
									<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countrydetail']->value['id_country'], ENT_QUOTES, 'UTF-8');?>
">
										<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countrydetail']->value['name'], ENT_QUOTES, 'UTF-8');?>

									</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
							</select>
						</div>
						<div class="col-md-6">
							<label for="residence" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Pays de résidence','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
							<select name="residence" id="residence" required="required" class="form-control form-control-select" style="height:40px; width:100%">
								<option value=""><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select Country','mod'=>'marketplace'),$_smarty_tpl ) );?>
</option>
								<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['country']->value, 'countrydetail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['countrydetail']->value) {
?>
									<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countrydetail']->value['id_country'], ENT_QUOTES, 'UTF-8');?>
">
										<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countrydetail']->value['name'], ENT_QUOTES, 'UTF-8');?>

									</option>
								<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
							</select>
						</div>
					</div>
				    <div class="form-group row">
						<div class="col-md-6">
							<label for="seller_email" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
							<input class="form-control"
							type="email"
							value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['customer_email']->value, ENT_QUOTES, 'UTF-8');?>
"
							name="seller_email"
							id="seller_email"
							required="required"
							/>
							<span class="wk-msg-selleremail"></span>
						</div>
						<div class="col-md-6">
							<label for="phone" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Numéro Téléphone','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
							<input class="form-control"
							type="tel"
							value="<?php if (isset($_POST['phone'])) {
echo htmlspecialchars($_POST['phone'], ENT_QUOTES, 'UTF-8');
}?>"
							name="phone"
							id="phone"
							maxlength="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max_phone_digit']->value, ENT_QUOTES, 'UTF-8');?>
" required="required"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_password" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Mot de passe','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
							<input class="form-control"
							type="password"
							value=""
							name="seller_password"
							id="seller_password"
							/>
							<span class="wk-msg-customeremail"></span>
						</div>
						<div class="col-md-6">
							<label for="customer_confpassword" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Confirmation du mot de passe','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
							<input class="form-control"
							type="password"
							value=""
							name="seller_confpassword"
							id="seller_confpassword"
							 />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<label for="seller_address" class="control-label required">
								<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Adresse','mod'=>'marketplace'),$_smarty_tpl ) );?>

							</label>
						</div>
						<textarea class="form-control" name="address"></textarea>
					</div>
					<?php if ($_smarty_tpl->tpl_vars['seller_country_need']->value) {?>
						<div class="form-group row">
							<div class="col-md-6" id="seller_zipcode">
								<label for="postcode" class="control-label">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Zip/Postal Code','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<input class="form-control"
								type="text"
								value="<?php if (isset($_POST['postcode'])) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_POST['postcode'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"
								name="postcode"
								id="postcode" />
							</div>
							<div class="col-md-6">
								<label for="city" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'City','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<input class="form-control"
								type="text"
								value="<?php if (isset($_POST['city'])) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_POST['city'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"
								name="city"
								id="city"
								maxlength="64" />
							</div>
						</div>
					<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['tax_identification_number']->value) {?>
						<div class="form-group row">
							<div class="col-md-6" id="seller_tax_identification_number">
								<label for="tax_identification_number" class="control-label">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Tax Identification Number/VAT','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<input class="form-control"
								type="text"
								value="<?php if (isset($_POST['tax_identification_number'])) {
echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_POST['tax_identification_number'],'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');
}?>"
								name="tax_identification_number"
								id="tax_identification_number"
								maxlength="64" />
							</div>
						</div>
					<?php }?>
					

					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>"displayMpSellerRequestFooter"),$_smarty_tpl ) );?>

					<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['hook'][0], array( array('h'=>'displayGDPRConsent','mod'=>'psgdpr','id_module'=>$_smarty_tpl->tpl_vars['id_module']->value),$_smarty_tpl ) );?>


					<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_18332891535db6f876217851_41811887', 'mp-form-fields-notification', $this->tplIndex);
?>

					<input type="button" name="next" class="next action-button" value="Suivant"/>
				</fieldset>
				<fieldset>
					  <h2 class="fs-title">Renseignements boutique</h2>
                		<h3 class="fs-subtitle">Donnez des informations sur votre boutique</h3>
                		<div class="form-group row">
                			 <div class="avatar-upload">
						        <div class="avatar-edit">
						            <input type='file' id="imageUpload" name="shopimage[]" accept=".png, .jpg, .jpeg" />
						            <label for="imageUpload"></label>
						        </div>
						        <div class="avatar-preview">
						            <div id="imagePreview" style="background-image: url(https://www.afrissime.com/modules/marketplace/views/img/shop_img/defaultshopimage.jpg?time=1570007517);">
						            </div>
						        </div>
						    </div>
                		</div>
                		
	                	<div class="form-group row">
		                	<div class="col-md-6">
		                		<div class="form-group seller_shop_name_uniq">
									<label for="shop_name_unique" class="control-label required">
										<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Shop Name','mod'=>'marketplace'),$_smarty_tpl ) );?>

										<div class="wk_tooltip">
											<span class="wk_tooltiptext"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'This name will be used in your shop URL','mod'=>'marketplace'),$_smarty_tpl ) );?>
</span>
										</div>
									</label>
									<img style="display: none;" width="25" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8');?>
marketplace/views/img/loading-small.gif" class="seller-loading-img"/>
									<input class="form-control"
										type="text"
										value="<?php if (isset($_POST['shop_name_unique'])) {
echo htmlspecialchars($_POST['shop_name_unique'], ENT_QUOTES, 'UTF-8');
}?>"
										id="shop_name_unique"
										name="shop_name_unique"
										onblur="onblurCheckUniqueshop();"
										autocomplete="off" />
									<span class="wk-msg-shopnameunique"></span>
									<p><span class="wk-msg-domaineunique"></span><p>
								</div>
							</div>
							<div class="col-md-6">
								<span style="position:relative; right:210px;top:30px;">.afrissime.com</span>
							</div>
						</div>
						
						<div class="form-group row">
							<div class="col-md-12">
								<label for="about_shop" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Description de la boutique','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
							</div>
							<textarea name="about_shop_1" class="form-control" placeholder=""></textarea>
						</div>
						<div style="width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;height:300px;background-image: url(https://www.afrissime.com/modules/marketplace/views/img/shop_banner/default_cover_pageshop.jpg);" id="cover_shop">
							<div style="position:relative; top:160px;">
								<h3 style="text-align:center;padding: 10px;background-color: #fff;opacity: 0.7;"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Image de couverture boutique','mod'=>'marketplace'),$_smarty_tpl ) );?>
</h3>
								<div class="input-group" style="margin: 0 auto;"> 
								    <div class="custom-file">
									    <input type="file" name="profilebannerimage[]" id="inputGroupFile01" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
									 </div>
						      	</div>
					      	</div>
				      	</div>
				      <div class="form-group row">
							<div class="col-md-6">
								<label for="business_email" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Email boutique','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<input class="form-control"
								type="email"
								value="<?php if (isset($_POST['business_email'])) {
echo htmlspecialchars($_POST['business_email'], ENT_QUOTES, 'UTF-8');
} else {
echo htmlspecialchars($_smarty_tpl->tpl_vars['customer_email']->value, ENT_QUOTES, 'UTF-8');
}?>"
								name="business_email"
								id="business_email"
								onblur="onblurCheckUniqueSellerEmail();" />
								<span class="wk-msg-selleremail"></span>
								<span>Pour recevoir un email à chaque vente</span>
							</div>
							<div class="col-md-6">
								<label for="phone_shop" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Phone','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<input class="form-control"
								type="text"
								value="<?php if (isset($_POST['phone'])) {
echo htmlspecialchars($_POST['phone'], ENT_QUOTES, 'UTF-8');
}?>"
								name="phone_shop"
								id="phone_shop"
								maxlength="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['max_phone_digit']->value, ENT_QUOTES, 'UTF-8');?>
" />
								<span>Pour recevoir un SMS à chaque vente</span>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="id_card" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Pièce d\'identité du titulaire de la boutique','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<input type="file" name="id_card"/>
							</div>
							<div class="col-md-6">
								<label for="payment_method" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Canal de versement des paiements','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<div class="row">
									<input type="checkbox" name="bank_transfer" value="1" style="width:10%" onclick="show_payment_box_1();"/> Virement bancaire </br>
									<div id="payment_box_1" style="display:none">
										<p>Informations bancaires</p>
										<div class="col-md-6">
											<label for="bank_name" class="control-label required">
											<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Nom de la banque','mod'=>'marketplace'),$_smarty_tpl ) );?>

											</label>
											<input type="text" name="bank_name" class="form-control" placeholder="Nom de la banque"/>
										</div>
										<div class="col-md-6">
											<label for="bank_name" class="control-label required">
											<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'RIB','mod'=>'marketplace'),$_smarty_tpl ) );?>

											</label>
											<input type="text" name="rib" class="form-control" placeholder="RIB"/>
										</div>
									</div>
								</div>
								<div class="row">
									<input type="checkbox" name="credit_card" value="2" style="width:10%" onclick="show_payment_box_2();"/> Carte de débit ou de crédit </br>
									<div id="payment_box_2" style="display:none">
										<p>Informations de la carte Visa ou Mastercard</p>
										<div>
											<label for="name_card" class="control-label required">
											<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Nom du titulaire de la carte','mod'=>'marketplace'),$_smarty_tpl ) );?>

											</label>
											<input type="text" name="name_card" class="form-control" placeholder="Nom"/>
										</div>
										<div>
											<div class="col-md-6">
												<label for="num_card" class="control-label required">
												<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Numéro de la carte','mod'=>'marketplace'),$_smarty_tpl ) );?>

												</label>
												<input type="text" name="num_card" class="form-control" placeholder="Ex: 4000 1234 5678 9101"/>
											</div>
											<div class="col-md-6">
												<label for="card_expiration" class="control-label required">
												<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Expire le','mod'=>'marketplace'),$_smarty_tpl ) );?>

												</label>
												<input type="text" name="card_expiration" class="form-control" placeholder=" Ex: 12/21"/>
											</div>
										</div>
									</div>
								</div>
									<div class="row">
										<input type="checkbox" name="payment_method" value="3" style="width:10%" onclick="show_payment_box_3();"/> Mobile money </br>
										<div id="payment_box_3" style="display:none">
										<p>Numéro pour le débit ou le crédit</p>
										<div class="">
											<label for="name_card" class="control-label required">
											<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Numéro','mod'=>'marketplace'),$_smarty_tpl ) );?>

											</label>
											<input type="text" name="num_mobile" class="form-control" placeholder="Numéro"/>
										</div>
									</div>
								</div>
								<input type="checkbox" name="cash" value="4" style="width:10%"/> Cash </br>
								<input type="checkbox" name="bitcoin" value="5" style="width:10%"/> Bitcoin </br>
								<input type="checkbox" name="bank_wallet" value="6" style="width:10%"/> Bank wallet </br>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="id_country" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Localisation de votre boutique','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<select name="id_country" id="id_country" class="form-control form-control-select" required="required" style="height:40px; width:100%">
									<option value=""><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select Country','mod'=>'marketplace'),$_smarty_tpl ) );?>
</option>
									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['country']->value, 'countrydetail');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['countrydetail']->value) {
?>
										<option value="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countrydetail']->value['id_country'], ENT_QUOTES, 'UTF-8');?>
">
											<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['countrydetail']->value['name'], ENT_QUOTES, 'UTF-8');?>

										</option>
									<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
								</select>
								<span class="wk-msg-sellerlocalisation"></span>
							</div>
							<div class="col-md-6">
								<div id="wk_seller_state_div" class="col-md-6">
									<label for="id_state" class="control-label required">
										<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'State','mod'=>'marketplace'),$_smarty_tpl ) );?>

									</label>
									<select name="id_state" id="id_state" class="form-control form-control-select">
										<option value=""><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Select State','mod'=>'marketplace'),$_smarty_tpl ) );?>
</option>
									</select>
									<input type="hidden" name="state_available" id="state_available" value="0" />
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="seller_town" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Ville','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<input class="form-control"
								type="text"
								value=""
								name="seller_town"
								id="seller_town"
								maxlength="" />
								<span class="wk-msg-sellertown"></span>
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="seller_district" class="control-label required">
										<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Quartier','mod'=>'marketplace'),$_smarty_tpl ) );?>

									</label>
									<input class="form-control"
									type="text"
									value=""
									name="seller_district"
									id="seller_district"
									maxlength="" />
									<span class="wk-msg-sellerdistrict"></span>
								</div>
								<div class="col-md-6">
									<label for="seller_postalcode" class="control-label required">
										<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Adresse postale','mod'=>'marketplace'),$_smarty_tpl ) );?>

									</label>
									<input class="form-control"
									type="text"
									value=""
									name="seller_postalcode"
									id="seller_postalcode"
									maxlength="" />
									<span class="wk-msg-sellerpostalcode"></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<h3><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Détails de livraison','mod'=>'marketplace'),$_smarty_tpl ) );?>
</h3>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="transporter" class="control-label required">
									<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Transporteur','mod'=>'marketplace'),$_smarty_tpl ) );?>

								</label>
								<input class="form-control"
								type="text"
								value=""
								name="transporter"
								id="transporter"
								maxlength="" />
								<span class="wk-msg-transporter"></span>
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="delivery_way" class="control-label required">
										<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Voie','mod'=>'marketplace'),$_smarty_tpl ) );?>

									</label>
									<select name="delivery_way" id="voie_livraison" class="form-control form-control-select">
										<option value="1"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Maritime','mod'=>'marketplace'),$_smarty_tpl ) );?>
</option>
										<option value="2"><?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Aerienne','mod'=>'marketplace'),$_smarty_tpl ) );?>
</option>
									</select>
									<span class="wk-msg-voielivraison"></span>
								</div>
								<div class="col-md-6">
									<label for="duration" class="control-label required">
										<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Durée','mod'=>'marketplace'),$_smarty_tpl ) );?>

									</label>
									<input class="form-control"
									type="text"
									value=""
									name="duration"
									id="duration"
									placeholder="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Ex: 2 à 4 jours','mod'=>'marketplace'),$_smarty_tpl ) );?>
" />
								</div>
							</div>
						</div>
						 <input type="button" name="previous" class="previous action-button-previous" value="Précédent"/>
                		<input type="button" name="next" class="next action-button" value="Suivant"/>
				</fieldset>
				<fieldset>
					<h2 class="fs-title">Conditions générales de vente</h2>
                		<h3 class="fs-subtitle">En savoir plus sur les conditions générales de vente</h3>
                		<p style="text-align:justify">Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semper</p>
                		<?php if ($_smarty_tpl->tpl_vars['terms_and_condition_active']->value) {?>
							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" style="width:10px;"/>
										<span>
											<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'I agree to the','mod'=>'marketplace'),$_smarty_tpl ) );?>

											<?php if (isset($_smarty_tpl->tpl_vars['linkCmsPageContent']->value)) {?>
												<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['linkCmsPageContent']->value, ENT_QUOTES, 'UTF-8');?>
" class="wk_terms_link">
													<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'terms and condition','mod'=>'marketplace'),$_smarty_tpl ) );?>

												</a>
											<?php } else { ?>
												<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'terms and condition','mod'=>'marketplace'),$_smarty_tpl ) );?>

											<?php }?>
											<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'and will adhere to them unconditionally.','mod'=>'marketplace'),$_smarty_tpl ) );?>

										</span>
									</label>
								</div>
							</div>
							<?php if (isset($_smarty_tpl->tpl_vars['linkCmsPageContent']->value)) {?>
								<div class="modal fade" id="wk_terms_condtion_div" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div id="wk_terms_condtion_content" class="modal-body"></div>
										</div>
									</div>
								</div>
							<?php }?>
						<?php }?>
						<input type="button" name="previous" class="previous action-button-previous" value="Précédent" style="float:left;"/>
							<div data-action="<?php echo call_user_func_array( $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['l'][0], array( array('s'=>'Register','mod'=>'marketplace'),$_smarty_tpl ) );?>
" id="wk-seller-submit" style="width: 120px;float: left;">
							<img class="wk_product_loader" src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['modules_dir']->value, ENT_QUOTES, 'UTF-8');?>
marketplace/views/img/loader.gif" width="25" />
							<input type="submit" name="sellerRequest" id="sellerRequest" class="action-button" value="Enregistrer"/>
							</div>
				</fieldset>
			</div>
		</form>
	</div>
<?php }
}
}
/* {/block 'content'} */
}
