<?php
/* Smarty version 3.1.33, created on 2019-10-28 16:01:21
  from 'module:wecashupviewstemplatesfro' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db710d1bd1643_44039978',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '291bd2c3ad4f3b2e931ef5f9a8db43d15fb2327b' => 
    array (
      0 => 'module:wecashupviewstemplatesfro',
      1 => 1572278468,
      2 => 'module',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db710d1bd1643_44039978 (Smarty_Internal_Template $_smarty_tpl) {
?><!-- begin /home/afrissim/public_html/modules/wecashup/views/templates/front/wecashup_form.tpl --><form action="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['action']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
" method="POST" id="wecashup">
    <?php echo '<script'; ?>
 async src="https://www.wecashup.com/library/MobileMoney.js" class="wecashup_button"
    data-demo
    data-sender-lang="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['LAN']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
    data-sender-phonenumber=""
    data-receiver-uid="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['MUID']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
    data-receiver-public-key="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['MPK']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
    data-transaction-parent-uid=""
    data-transaction-receiver-total-amount="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['AMT']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
    data-transaction-receiver-reference="XVT2VBF"
    data-transaction-sender-reference="XVT2VBF"
    data-sender-firstname="Test"
    data-sender-lastname="Test"
    data-transaction-method="pull"
    data-image="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['DIMG']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
    data-name="<?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['APPN']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
"
    data-crypto="true"
    data-cash="true"
    data-telecom="true"
    data-m-wallet="true"
    data-split="true"
    configuration-id="3"
    data-marketplace-mode="false"
    data-product-1-description="France is in the Air"
    <?php echo htmlspecialchars(call_user_func_array($_smarty_tpl->registered_plugins[ 'modifier' ][ 'escape' ][ 0 ], array( $_smarty_tpl->tpl_vars['OUTPUT']->value,'htmlall','UTF-8' )), ENT_QUOTES, 'UTF-8');?>
 >
    <?php echo '</script'; ?>
>
</form>


<!-- end /home/afrissim/public_html/modules/wecashup/views/templates/front/wecashup_form.tpl --><?php }
}
