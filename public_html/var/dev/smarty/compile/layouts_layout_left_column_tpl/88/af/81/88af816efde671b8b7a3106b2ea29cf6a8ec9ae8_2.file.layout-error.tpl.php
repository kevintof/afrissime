<?php
/* Smarty version 3.1.33, created on 2019-10-28 16:11:08
  from '/home/afrissim/public_html/themes/jms_basel/templates/layouts/layout-error.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5db7131cd1a715_31195406',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '88af816efde671b8b7a3106b2ea29cf6a8ec9ae8' => 
    array (
      0 => '/home/afrissim/public_html/themes/jms_basel/templates/layouts/layout-error.tpl',
      1 => 1566831868,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5db7131cd1a715_31195406 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_loadInheritance();
$_smarty_tpl->inheritance->init($_smarty_tpl, false);
?>
<!doctype html>
<html lang="">

  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4907958825db7131cd11355_66675465', 'head_seo');
?>

    <meta name="viewport" content="width=device-width, initial-scale=1">

      </head>

  <body>

    <?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_4668971795db7131cd15d71_45958417', 'content');
?>


    <!-- Load JS files here -->

  </body>

</html>
<?php }
/* {block 'head_seo_title'} */
class Block_10361489335db7131cd11d63_40517451 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_title'} */
/* {block 'head_seo_description'} */
class Block_10758924295db7131cd137b1_63929615 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_description'} */
/* {block 'head_seo_keywords'} */
class Block_13041571555db7131cd14676_36051813 extends Smarty_Internal_Block
{
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
}
}
/* {/block 'head_seo_keywords'} */
/* {block 'head_seo'} */
class Block_4907958825db7131cd11355_66675465 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'head_seo' => 
  array (
    0 => 'Block_4907958825db7131cd11355_66675465',
  ),
  'head_seo_title' => 
  array (
    0 => 'Block_10361489335db7131cd11d63_40517451',
  ),
  'head_seo_description' => 
  array (
    0 => 'Block_10758924295db7131cd137b1_63929615',
  ),
  'head_seo_keywords' => 
  array (
    0 => 'Block_13041571555db7131cd14676_36051813',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <title><?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10361489335db7131cd11d63_40517451', 'head_seo_title', $this->tplIndex);
?>
</title>
      <meta name="description" content="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_10758924295db7131cd137b1_63929615', 'head_seo_description', $this->tplIndex);
?>
">
      <meta name="keywords" content="<?php 
$_smarty_tpl->inheritance->instanceBlock($_smarty_tpl, 'Block_13041571555db7131cd14676_36051813', 'head_seo_keywords', $this->tplIndex);
?>
">
    <?php
}
}
/* {/block 'head_seo'} */
/* {block 'content'} */
class Block_4668971795db7131cd15d71_45958417 extends Smarty_Internal_Block
{
public $subBlocks = array (
  'content' => 
  array (
    0 => 'Block_4668971795db7131cd15d71_45958417',
  ),
);
public function callBlock(Smarty_Internal_Template $_smarty_tpl) {
?>

      <p>Hello world! This is HTML5 Boilerplate.</p>
    <?php
}
}
/* {/block 'content'} */
}
