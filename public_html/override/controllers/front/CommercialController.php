<?php

class CommercialController extends FrontController
{

    /**
     * Initialize controller
     * @see FrontController::init()
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
		
        $this->setTemplate('commercial.tpl'); // themes/theme_current/templates/acheter.tpl
    }
    public function getBreadcrumbLinks()
    {
        $breadcrumb = parent::getBreadcrumbLinks();

        $breadcrumb['links'][] = [
            'title' => $this->getTranslator()->trans('Devenir commercial consultant', [], 'Shop.Theme.Global'),
            'url' => $this->context->link->getPageLink('commercial', true),
        ];

        return $breadcrumb;
    }

}