{*
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{assign var=unique_id value=1|mt_rand:1000}
<script type="text/javascript">
	if(cattab1_unique)
		cattab1_unique.push({$unique_id});
	else 
		var cattab1_unique = [];
		cattab1_unique.push({$unique_id});

	if(cattab1_items)
		cattab1_items[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	else
		var cattab1_items = [];
		cattab1_items[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	if(cattab1_itemsDesktop)
		cattab1_itemsDesktop[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};
	else
		var cattab1_itemsDesktop = [];
		cattab1_itemsDesktop[{$unique_id}]={if $cols}{$cols|escape:'htmlall':'UTF-8'}{else}4{/if};

	if(cattab1_itemsDesktopSmall)
		cattab1_itemsDesktopSmall[{$unique_id}]={if $cols_md}{$cols_md|escape:'htmlall':'UTF-8'}{else}3{/if};
	else
		var cattab1_itemsDesktopSmall = [];
		cattab1_itemsDesktopSmall[{$unique_id}]={if $cols_md}{$cols_md|escape:'htmlall':'UTF-8'}{else}3{/if};

	if(cattab1_itemsTablet)
		cattab1_itemsTablet[{$unique_id}]={if $cols_sm}{$cols_sm|escape:'htmlall':'UTF-8'}{else}2{/if};
	else
		var cattab1_itemsTablet = [];
		cattab1_itemsTablet[{$unique_id}]={if $cols_sm}{$cols_sm|escape:'htmlall':'UTF-8'}{else}2{/if};
	if(cattab1_itemsMobile)
		cattab1_itemsMobile[{$unique_id}]={if $cols_xs}{$cols_xs|escape:'htmlall':'UTF-8'}{else}1{/if};
	else
		var cattab1_itemsMobile = [];
		cattab1_itemsMobile[{$unique_id}]={if $cols_xs}{$cols_xs|escape:'htmlall':'UTF-8'}{else}1{/if};

	if(cattab1_nav)
		cattab1_nav[{$unique_id}]={if $navigation ==1}true{else}false{/if};
	else
		var cattab1_nav = [];
		cattab1_nav[{$unique_id}]={if $navigation ==1}true{else}false{/if};

	if(cattab1_pag)
		cattab1_pag[{$unique_id}]={if $pagination ==1}true{else}false{/if};
	else
		var cattab1_pag = [];
		cattab1_pag[{$unique_id}]={if $pagination ==1}true{else}false{/if};
	if(cattab1_auto)
		cattab1_auto[{$unique_id}]={if $autoplay ==1}true{else}false{/if};
	else
		var cattab1_auto = [];
		cattab1_auto[{$unique_id}]={if $autoplay ==1}true{else}false{/if};
	if(cattab1_rewind)
		cattab1_rewind[{$unique_id}]={if $rewind ==1}true{else}false{/if};
	else
		var cattab1_rewind = [];
		cattab1_rewind[{$unique_id}]={if $rewind ==1}true{else}false{/if};
	if(cattab1_slideby)
		cattab1_slideby[{$unique_id}]={if $slidebypage ==1}'page'{else}1{/if};
	else
		var cattab1_slideby = [];
		cattab1_slideby[{$unique_id}]={if $slidebypage ==1}'page'{else}1{/if};
</script>

<div class="categories-tab-wrapper">
	<div class="top-content">
		{if $addon_title || $addon_desc}
			<div class="addon-title">
				{if $addon_title}
					<h3>
						{$addon_title|escape:'htmlall':'UTF-8'}
					</h3>
				{/if}
				{if $addon_desc}
					<p class="addon-desc">{$addon_desc|escape:'htmlall':'UTF-8'}</p>
				{/if}
			</div>
		{/if}
		<div class="jms-tab">
			<ul class="nav nav-tabs">
				{foreach from = $categories key = k item = category}
					<li class="nav-item">
						<a class="{if $k == 0}active{/if}" data-toggle="tab" href="#category-{$category.id_category|escape:'html':'UTF-8'}-{$unique_id}">
							{$category.name}
						</a>
					</li>
				{/foreach}	
			</ul>
		</div>
	</div>
	<div class="tab-content custom-style">
		{foreach from = $categories key = k item = category}
			 <div role="tabpanel" class="tab-pane {if $k == 0}active {/if}" id="category-{$category.id_category|escape:'html':'UTF-8'}-{$unique_id}">
				<div class="categorytab-carousel-{$unique_id}">	
					{foreach from = $category.products item = products_slide}				
						<div class="item">
							{foreach from = $products_slide item = product}
								{include file="catalog/_partials/miniatures/product.tpl" product=$product}
							{/foreach}
						</div>
					{/foreach}
				</div>
			 </div>		
		{/foreach}	
	</div>
</div>

