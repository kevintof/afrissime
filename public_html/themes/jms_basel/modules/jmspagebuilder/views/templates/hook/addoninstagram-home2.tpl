{*
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2017 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<script type="text/javascript">
	var items = {if $cols}{$cols}{else}4{/if},
        inst_itemsDesktop = {if $cols}{$cols}{else}4{/if},
        inst_itemsDesktopSmall = {if $cols_md}{$cols_md}{else}3{/if},
        inst_itemsTablet = {if $cols_sm}{$cols_sm}{else}2{/if},
        inst_itemsMobile = {if $cols_xs}{$cols_xs}{else}1{/if};
        inst_nav = {if $navigation == '1'}true{else}false{/if};
        inst_pag = {if $pagination == '1'}true{else}false{/if};
        inst_autoplay = {if $autoplay == '1'}true{else}false{/if};
        inst_space = 0;
</script>
<div class="instagram-wrapper">
    {if $addon_title || $addon_desc}
        <div class="addon-title custom-title">
            {if $addon_title}
                <h3>
                    {$addon_title|escape:'htmlall':'UTF-8'} 
                </h3>       
            {/if}
            {if $addon_desc}
                <p class="addon-desc">{$addon_desc|escape:'htmlall':'UTF-8'}</p>
            {/if}
            <span class="b-title_separator"><span></span></span>
        </div>
    {/if}
    <div class="instagram-images">
        {foreach from = $insta_images item = insta}
            <div class="item ">
                {foreach from = $insta item = insta_image}
                    <div class="image-box">
                        <a href="{$insta_image.link}">
                            <img src="{$insta_image.url}" alt="Instagram Image" />
                            <div class="hover-mask"></div>
                        </a>
                    </div>
                {/foreach}
            </div>
        {/foreach}
    </div>
</div>
