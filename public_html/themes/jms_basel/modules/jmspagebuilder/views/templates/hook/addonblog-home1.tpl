{*

* 2007-2016 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2016 PrestaShop SA

*  @version  Release: $Revision$

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*}

<div class="blog-box">

	{if $addon_title || $addon_desc}

		<div class="addon-title custom-title">

			{if $addon_title}

				<h3>

					{$addon_title|escape:'htmlall':'UTF-8'}	

				</h3>		

			{/if}

			{if $addon_desc}

				<p class="addon-desc">{$addon_desc|escape:'htmlall':'UTF-8'}</p>

			{/if}

			<span class="b-title_separator"><span></span></span>

		</div>

	{/if}

	{if $adPosts|@count gt 0}



		<div class="row">	

			{foreach from=$adPosts item=arr_post key=k}	

				{foreach from=$arr_post item=post}

					

						{assign var=params value=['post_id' => $post.post_id, 'category_slug' => $post.category_alias, 'slug' => $post.alias]}

						{assign var=catparams value=['category_id' => $post.category_id, 'slug' => $post.category_alias]}



						<div class="blog-item moreBox col-xs-12 col-sm-4 col-md-4 col-xs-6" {if $k > 5} style="display: none;" {/if}>

							{if $post.link_video && ($show_media == '1')}

								<div class="post-thumb">

									{$post.link_video|escape:'htmlall':'UTF-8'}

								</div>

							{elseif $post.image && ($show_media == '1')}

								<div class="post-thumb img-zoom">

									<a href="{jmsblog::getPageLink('jmsblog-post', $params)|escape:'htmlall':'UTF-8'|replace:'&amp;':'&'}">

										<img src="{$image_url|escape:'html':'UTF-8'}{$post.image|escape:'html':'UTF-8'}" alt="{$post.title|escape:'htmlall':'UTF-8'}" class="img-responsive" />

									</a>

									{if $show_category == '1'}

										<div class="cat-box">

											<a href="{jmsblog::getPageLink('jmsblog-category', $catparams)|escape:'htmlall':'UTF-8'|replace:'&amp;':'&'}">

												{$post.category_name|escape:'html':'UTF-8'}

											</a>

										</div>

									{/if}

									{if $show_time == '1'}

										<div class="post-created">

											<span class="date">{$post.created|escape:'html':'UTF-8'|date_format:'%e'}</span>

											<span class="month">{$post.created|escape:'html':'UTF-8'|date_format:'%b'}</span>

										</div>

									{/if}

								</div>

							{/if}	

							<div class="post-info">

									<a href="{jmsblog::getPageLink('jmsblog-post', $params)|escape:'htmlall':'UTF-8'|replace:'&amp;':'&'}" class="post-title">	

										{$post.title|substr:0:50}

									</a>

									<div class="view-comment">

										{if $show_nviews == '1'}

											<span class="view">

												{$post.views|escape:'html':'UTF-8'} {l s='views' d='Modules.JmsPagebuilder'}

											</span>

										{/if}

										<span class="space">/</span>

										{if $show_ncomments == '1'}		

											<span class="comment">

												{$post.comment_count|escape:'html':'UTF-8'} {l s='comments' d='Modules.JmsPagebuilder'}

											</span>

										{/if}

									</div>

								{if $show_introtext == '1'}	

									<div class="post-intro">{$post.introtext nofilter}</div>	

								{/if}

							</div>

						

						</div>

					

				{/foreach}		

			{/foreach}	

		</div>	

		{if $adPosts|@count >5 }

			<div id="loadMore" style="">

	         	<a href="#" class="btn">{l s='Load More Posts' d='Modules.JmsPagebuilder'}</a>

	      	</div>

      	{/if}

	{/if}

</div>





	