{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos.css" />
{/block}
{block name='page_content'}
<div id="pageshop">
  <div id="transcroller-body" class="aos-all">
      <div class="landing-header" style="background-image: url('https://www.afrissime.com/themes/jms_basel/assets/img/seller_cover.jpg');height:600px">
        <div class="container">
          <div class="intro_shop">
            <h2>Achetez des articles authentiques</h2>
            <h3>en consommant des produits fabriqués par des africains</h3>
            <br>
          </div>
        </div>
      </div>
      <div class="main">
       <div class="section landing-section">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          <h3 style="font-weight:700">Comment acheter sur Afrissime?</h3>
          <p class="text-center">
            L'achat sur Afrissime se fait en 4 étapes:
          </p>
            <h4 style="font-weight:600">1. Rechercher votre article</h4>
            <p>Nous avons plusieurs façons de rechercher un produit sur Afrissime</p>
        </div>
      </div>
      <div class="row stepper">
      	<div class="col-md-6">
           <div class="info aos-item" data-aos="fade-in">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/search_step_2.jpg" alt="Recherche à partir du moteur de recherche" >
              </div>
              <div class="description">
                <p><strong>Rechercher à partir du moteur de recherche</strong></p>
                <p class="text-center">Cette méthode est plus rapide et adapté lorsque vous connaissez le nom du produit que vous recherchez. </p>
              </div>
           </div>
        </div>
        <div class="col-md-6">
         <div class="info aos-item" data-aos="fade-in">
            <div>
              <div class="icon icon-primary">
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/search_step_1.jpg" alt="Recherche par catégorie" >
              </div>
              <div class="description">
                <p><strong>Rechercher en parcourant les catégories  </strong></p>
                <p class="text-center">Si vous n'avez pas beaucoup d'information sur le produit que vous recherchez, l'idéal serait de chercher à partir de la catégorie appropriée. </p>
              </div>
            </div>
            
         </div>
        </div>
        
      </div>
      	<div class="row">
	        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
	          <div class="col-md-3">
	            <img src="https://www.afrissime.com/themes/jms_basel/assets/img/add_to_cart.png" alt="Ajouter au panier" width="200px">
	          </div>
	          <div class="col-md-9">
	              <h4 style="font-weight:600">2. Ajouter votre article au panier</h4>
	              <p>
	              	En ajout votre article au panier, vous êtes certains de le retrouver sur la page récapitulatif de votre commande.<br/>
	                  Une fois l'article ajouté au panier, vous pourrez passer soit continuer les achats ou soit procéder au paiement.
	              </p>
	          </div>
	        </div>
	     </div>
       	<div class="row">
	        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
	          <div class="col-md-9">
	              <h4 style="font-weight:600">3. Procéder ensuite au paiement</h4>
	              <p>
	                  Afrissime offre plusieurs méthodes de paiement à savoir :
	                  <ul class="list-unstyled">
	                  	<li>Le paiement par chèque</li>
	                  	<li>Le paiement par virement bancaire</li>
	                  	<li>Le paiement par Paypal et carte bancaire VISA ou Mastercard</li>
	                  	<li>Le paiement en espèce, mobile money et porteuille mobile</li>
	                  	<li>Le paiement à la livraison</li>
	                  </ul>
	              </p>
	          </div>
	          <div class="col-md-3">
	            <img src="https://www.afrissime.com/themes/jms_basel/assets/img/cash-payment.jpg" alt="paiement" width="200px">
	          </div>
	        </div>
	     </div>
	     <div class="row">
		        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
		       	  <div class="col-md-3">
		            <img src="https://www.afrissime.com/themes/jms_basel/assets/img/delivery.png" width="200px" alt="Livraison">
		          </div>
		          <div class="col-md-9">
		              <h4 style="font-weight:600">4. Faîtes vous livrer</h4>
		              <p>
		                  Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semper ...
		              </p>
		          </div>
		          
		        </div>
		      </div>
		  </div>
    </div>
	     
    </div>
</div>
<script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
{/block}
