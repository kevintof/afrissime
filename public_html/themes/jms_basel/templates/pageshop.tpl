{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos.css" />
{/block}
{block name='page_content'}
	<div id="pageshop">
      <div id="transcroller-body" class="aos-all">
      <div class="landing-header" style="background-image: url('https://www.afrissime.com/themes/jms_basel/assets/img/cover_pageshop.jpg');height:600px">
        <div class="container">
          <div class="intro_shop">
            <h2>Vivez de votre savoir faire</h2>
            <h3>Vendez simplement, maintenant et sans engagement.</h3>
            <br>
            	{if $language.language_code == 'fr'}
              		<a href="https://www.afrissime.com/demande-du-vendeur" class="btn  btn-effect" style="color: #000;background-color: #fff;border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
              	{else}
              		<a href="https://www.afrissime.com/seller-request" class="btn  btn-effect" style="color: #000;background-color: #fff;border-radius: 2em;min-width: 7em;">I create my shop</a>
              	{/if}
          </div>
        </div>
      </div>

    <div class="main">
      <div class="section landing-section section-white">
    <div class="container">
      <div class="row text-center">
        <div class="col-md-12">
          <h3 style="font-weight:700">{if $language.language_code == 'fr'}Avantages de la vente sur Afrissime{else}Benefits of the sale on Afrissime{/if}</h3>
          <br>
          <p>
          Notre offre évolue constamment mais certaines choses ne changeront jamais : L’inscription sur notre plateforme est gratuite et le restera toujours.
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-up">
            <div class="icon icon-primary">
              <img src="https://www.afrissime.com/themes/jms_basel/assets/img/visibilite.png" alt="Visibilité" width="80px" height="80px">
            </div>
            <div class="description">
              <h4>Visibilité</h4>
              <p style="text-align:justify">Lorsque vous vendez en ligne sur Afrissime, vos produits sont plus faciles à trouver et plus faciles à acheter. Ouvrir votre boutique sur Afrissime, vous permet en tant que vendeur de toucher des millions d’acheteurs à travers le monde que vous ayez un article à vendre ou des millions.</p>
            </div>
         </div>
        </div>
        <div class="col-md-4">
           <div class="info aos-item" data-aos="fade-up">
              <div class="icon icon-warning">
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/croissance.png" alt="Croissance" width="80px" height="80px">
              </div>
              <div class="description">
                <h4>Croissance</h4>
                <p style="text-align:justify">Vendre sur Afrissime vous permet de développer votre activité. Afrissime vous aide à gérer les tâches fastidieuses associées à votre activité. Profitez d’autres services spécialement conçus pour vous aider à expédier les commandes, stocker les produits, gérer les annonces, etc.</p>
              </div>
           </div>
        </div>
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-up">
          <div class="icon icon-danger">
            <img src="https://www.afrissime.com/themes/jms_basel/assets/img/accompagnement.png" alt="Accompagnement" width="80px" height="80px">
          </div>
          <div class="description">
            <h4>Accompagnement</h4>
            <input type="checkbox" class="read-more-state" id="post-1" />
            <p class="read-more-wrap" style="text-align:justify">En vendant sur Afrissime, vous bénéficiez de prestations offertes par Afrissime à l’instar de shooting photo de vos produits avec ou sans mannequins, une réalisation de spot publicitaire vidéo, des séances de formation et de renforcement de capacité, une publicité et une prospection <span class="read-more-target">auprès de particuliers et professionnels, conceptions d’affiches et autre support à volonté, un service de traduction pour tous vos produits, pour vos acheteurs un service client disponible 24h/24 dans la langue locale.</span></p>
            <label for="post-1" class="read-more-trigger"></label>
          </div>
         </div>
        </div>
      </div>
        <div class="row">
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-up">
            <div class="icon icon-primary">
              <img src="https://www.afrissime.com/themes/jms_basel/assets/img/logistique.png" alt="Logistique" width="80px" height="80px">
            </div>
            <div class="description">
              <h4>Logistique</h4>
              <p style="text-align:justify">Afrissime s’occupe de l’expédition de vos produits au client où qu’il se trouve dans le monde.</p>
            </div>
         </div>
        </div>
        <div class="col-md-4">
           <div class="info aos-item" data-aos="fade-up">
              <div class="icon icon-warning">
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/performance.png" alt="Outils performants" width="80px" height="80px">
              </div>
              <div class="description">
                <h4>Outils performants</h4>
                <input type="checkbox" class="read-more-state" id="post-2" />
                <p class="read-more-wrap" style="text-align:justify">En tant que vendeur, vous découvrirez qu’Afrissime propose des outils et fonctionnalités uniques capables de vous aider à <span class="read-more-target">développer votre entreprise. Ils sont gratuits et peuvent être utilisés pour gérer en souplesse votre activité. Bénéficiez notamment de rapports de commandes et d’informations relatives à vos commandes.</span></p>
                <label for="post-2" class="read-more-trigger"></label>
              </div>
           </div>
        </div>
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-up">
          <div class="icon icon-danger">
            <img src="https://www.afrissime.com/themes/jms_basel/assets/img/sans_engagement.png" alt="sans engagement" width="80px" height="80px">
          </div>
          <div class="description">
            <h4>Sans engagment</h4>
            <p style="text-align:justify">En tant que vendeur sur Afrissime, vous avez l'assurance de pouvoir fermer votre boutique à tout moment.</p>
          </div>
         </div>
        </div>
      </div>
      <div class="row text-center">
        <div class="col-md-12">
        		{if $language.language_code == 'fr'}
            <a href="https://www.afrissime.com/demande-du-vendeur" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
          {else}
            <a href="https://www.afrissime.com/seller-request" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">I create my shop</a>
          {/if}
        </div>
      </div>
    </div>
  </div>

      <div class="section landing-section">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          <h3 style="font-weight:700">Vendez simplement en quelques étapes</h3>
          <p class="text-center">Accédez à des millions de clients et mettez l’expertise de la vente en ligne et les technologies de recherche et de paiement d’Afrissime au service de votre entreprise. Vendre sur Afrissime est un processus simple en 5 étapes.</p>
            <h4 style="font-weight:600">Avant de vous inscrire</h4>
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-6">
         <div class="info aos-item" data-aos="fade-in">
            <div>
              <div class="icon icon-primary">
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/rech.png" alt="Recherche produit" width="80px" height="80px">
              </div>
              <div class="description">
                <p><strong>1. Décidez de ce que vous voulez vendre</strong></p>
                <p class="text-center">Plus de 15 catégories sont ouvertes à tous les vendeurs. </p>
              </div>
            </div>
            
         </div>
         <div style="float:right;">
                <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png"  alt="Next" style="top: 75px;">
            </div>
        </div>
        <div class="col-md-6">
           <div class="info aos-item" data-aos="fade-in">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/ins.png" alt="Inscription" width="80px" height="80px">
              </div>
              <div class="description">
                <p><strong>2. S’inscrire et commencer à répertorier des produits</strong></p>
                <p class="text-center">Créez votre compte sur Afrissime, et accéder à l’interface web sur laquelle vous pourrez gérer votre compte vendeur.</p>
              </div>
           </div>
        </div>
      </div>
      <div class="row">
        <div class="md-12 text-center">
            <h4 style="font-weight:600">Une fois inscrit</h4>
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-3">
         <div class="info aos-item" data-aos="zoom-out">
            <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png" alt="Next" style="right:-40px; top:0px">
            <div class="icon icon-primary">
              <img src="https://www.afrissime.com/themes/jms_basel/assets/img/loupe.png"  alt="Répertorier les articles sur Afrissime" width="80px" height="80px">
            </div>
            <div class="description">
              <p><strong>1. Répertorier</strong></p>
              <p class="text-center">Vous pouvez ajouter des produits au catalogue produits Afrissime Marketplace autant que vous le désirez.</p>
            </div>
         </div>
        </div>
        <div class="col-md-3">
           <div class="info aos-item" data-aos="zoom-out">
              <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png" alt="Next" style="right:-40px; top:0px">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/vente.png" width="80px" height="80px" alt="Vendre sur Afrissime">
              </div>
              <div class="description">
                <p><strong>2. Vendre</strong></p>
                <p class="text-center">Vous disposez de différentes possibilités pour gérer vos commandes, en fonction du nombre de commandes que vous recevez :</p>
                <div class="row">
                  <div class="col-xs-12 col-sm-12">
                    <ul class="list-unstyled">
                      <li>&nbsp;</li>
                      <li style="text-align:center"><strong>•  Outil de gestion des commandes: </strong> </br>visualisez une liste de vos commandes et les détails concernant une commande sélectionnée.</li>
                      <li style="text-align:center"><strong>•  Rapports de commande: </strong> </br>recevez des informations sur le traitement de plusieurs commandes en exécutant un seul rapport.</li>
                    </ul>
                  </div>
                </div>
              </div>
           </div>
        </div>
         <div class="col-md-3">
         <div class="info aos-item" data-aos="zoom-out">
            <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png" alt="Next" style="right:-40px; top:0px">
            <div class="icon icon-primary">
              <img src="https://www.afrissime.com/themes/jms_basel/assets/img/expedition.png" width="80px" height="80px" alt="Expédition">
            </div>
            <div class="description">
              <p><strong>3. Expédition</strong></p>
              <p class="text-center">Afrissime vous alerte quand un client passe commande. Dès lors vous disposez d’un délai de 48h pour traiter (emballage et étiquetage) et expédier le produit.</p>
            </div>
         </div>
        </div>
         <div class="col-md-3">
         <div class="info aos-item" data-aos="zoom-out">
            <div class="icon icon-primary">
              <img src="https://www.afrissime.com/themes/jms_basel/assets/img/encaissement.png" width="80px" height="80px" alt="Encaissement">
            </div>
            <div class="description">
              <p><strong>4. Encaissement</strong></p>
              <p class="text-center">Afrissime vire régulièrement les paiements sur votre compte bancaire (ou tout autre moyen de paiement choisi) et vous informe que le règlement a bien été effectué. Afrissime accepte les paiements dans de nombreuses monnaies et vous paie dans la monnaie de votre pays, afin de simplifier le traitement du paiement.</p>
            </div>
         </div>
        </div>
      </div>
       <div class="row text-center">
        <div class="col-md-12">
            {if $language.language_code == 'fr'}
            <a href="https://www.afrissime.com/demande-du-vendeur" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
          {else}
            <a href="https://www.afrissime.com/seller-request" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">I create my shop</a>
          {/if}
        </div>
      </div>
    </div>
  </div>

  <div class="section landing-section section-white">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          <h3 style="font-weight:700">Avec Afrissime, vendre dans le monde est à la fois simple et facile</h3>
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-12 text-center">
          <div class="description">
            <p class="text-center">
              Comprendre les codes TVA et les réglementations sur les produits, créer des listings produits efficaces et fournir un service client localisé peut s'avérer complexe lorsque vous développez vos activités au-delà des frontières. Tirez parti de notre logistique, de nos outils performants et de notre service après-vente afin de jouir de notre base de clients en constante croissance.
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
          <div class="col-md-3">
            <img src="https://www.afrissime.com/themes/jms_basel/assets/img/compte_vendeur.png" width="200px" height="200px" alt="un seul compte vendeur">
          </div>
          <div class="col-md-9">
              <h4 style="font-weight:600">Un seul compte vendeur pour le monde entier</h4>
              <p>
                  Avec un seul compte, vous toucherez des clients dans les 200 pays du monde, et vous serez en mesure de gérer toutes vos offres ainsi que vos stocks depuis votre interface vendeur.
              </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
          <div class="col-md-9">
              <h4 style="font-weight:600">Développez vos ventes grâce à de nouvelles opportunités</h4>
              <p>
                  Avec plus de 15 catégories de produits disponibles, vous pourrez vendre de nombreux produits sur Afrissime.
              </p>
          </div>
          <div class="col-md-3">
            <img src="https://www.afrissime.com/themes/jms_basel/assets/img/opportunites.png" width="200px" height="200px" alt="Opportunités">
          </div>
        </div>
      </div>
       <div class="row">
        <div style="background-color:#fafafa; width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;" class="aos-item" data-aos="fade-in">
          <div class="col-md-3">
              <img src="https://www.afrissime.com/themes/jms_basel/assets/img/taxes.png" width="200px" height="200px" alt="Taxes et paiements">
          </div>
          <div class="col-md-9">
              <h4 style="font-weight:600">Gérez les taxes et les paiements</h4>
              <p>
                  Si vous vendez des produits dans un pays de l’UE, vous devrez probablement demander un numéro de taxe sur la valeur ajoutée (TVA). La TVA est une taxe européenne sur les dépenses de consommation. Afrissime accepte les paiements dans de nombreuses monnaies et vous paie dans la monnaie de votre pays, afin de simplifier le traitement du paiement.  
              </p>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-12">
              {if $language.language_code == 'fr'}
              <a href="https://www.afrissime.com/demande-du-vendeur" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
            {else}
              <a href="https://www.afrissime.com/seller-request" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">I create my shop</a>
            {/if}
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="section landing-section section-white">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          <h3 style="font-weight:700">Questions - Réponses</h3>
        </div>
      </div>
      <div id="accordion" aos-item" data-aos="fade-down">
      <p class="accordion-toggle minus-cercle">Qu'est-ce que Afrissime ?</p>
      <div class="accordion-content default">
      <p>Afrissime vient de la combinaison de Afrique et Assime (marché en langue mina). Afrissime est une boutique en ligne de vente de tout produit fait par des africains du continent ou de la diaspora.</p>
      </div>
      <p class="accordion-toggle">Quels produits puis-je vendre sur Afrissime ?</p>
      <div class="accordion-content">
      <p>Vous pouvez vendre tout type de produits à condition que le produit réponde aux critères suivants :</p>
      <ul class="list-unstyled"> 
        <li>-Avoir été fait par un africain</li> 
        <li>-Être un produit de qualité</li> 
      </ul>
      </div>
      <p class="accordion-toggle">Quels types de produits ne puis-je pas vendre sur Afrissime ?</p>
      <div class="accordion-content">
      <p>Vous ne pouvez pas vendre de produits illégaux, fabriqués de façon illégale ou qui sont prohibés par les politiques d’Afrissime. Cela inclut les médicaments sur ordonnance, les drogues illicites, les armes à feu, les munitions, les produits interdits de vente par le commerce international, etc.</p>
      </div>
      <p class="accordion-toggle">Quelles informations dois-je fournir pour m'inscrire comme vendeur sur Afrissime ?</p>
      <div class="accordion-content">
      <p>Toutes les informations à fournir sont renseignées dans le formulaire de création de boutique disponible ici.</p>
      </div>
      <p class="accordion-toggle">Comment mettre mes produits en vente sur Afrissime ?</p>
      <div class="accordion-content">
      <p>Vous pouvez utiliser notre interface Web ou un outil de téléchargement des offres. La procédure et les informations requises varient selon vos produits.</p>
      </div>
      <p class="accordion-toggle">Combien coûte la vente sur Afrissime ?</p>
      <div class="accordion-content">
      <p>Vendre sur Afrissime est gratuit. Nous prenons seulement 15% de commission sur chaque vente effectuée.</p>
      </div>
      <p class="accordion-toggle">Comment gérer les commandes ?</p>
      <div class="accordion-content">
      <p>Vous avez deux options pour gérer vos commandes.Vous pouvez utiliser l'interface Web. Vous pouvez télécharger le rapport de commande quotidien, fichier tabulé qui fournit un résumé des commandes reçues et comprend les informations des clients et des expéditions pour que vous puissiez traiter ces commandes.
      </p>
      </div>
      <p class="accordion-toggle">Comment suis-je payé ?</p>
      <div class="accordion-content">
      <p>Afrissime transfère le solde qui vous est dû sur votre compte bancaire (ou tout autre canal de paiement choisi) 7 jours après la réception du produit par le client. Les frais de retours, les réclamations clients ainsi que les remboursements affectent votre solde.
      </p>
      </div>
      <p class="accordion-toggle">Proposez-vous la protection contre la fraude ?</p>
      <div class="accordion-content">
      <p>Oui. La protection contre la fraude de paiement d'Afrissime vous aide à éliminer les commandes frauduleuses passées sur vos produits.
      </p>
      </div>
      <p class="accordion-toggle">Qu'est-ce que la garantie Afrissime ?</p>
      <div class="accordion-content">
      <p>Le programme de garantie Afrissime est conçu pour gérer des situations dans lesquelles un client n'a jamais reçu un produit ou a reçu un produit qui est substantiellement différent de celui commandé ou attendu. Nous demandons aux clients de commencer par contacter le vendeur en cas de problème. Si le vendeur n'est pas en mesure de résoudre le problème, le client peut exercer un recours en garantie. À la réception de la réclamation, Afrissime envoie au vendeur un message automatique détaillant la réclamation et sollicitant des informations sur la commande et le processus de traitement. Afrissime détermine alors le règlement du recours en garantie, qui peut inclure un remboursement de la commande au client, aux frais du vendeur.
      </p>
      </div>
      <p class="accordion-toggle">Les clients peuvent-ils laisser des évaluations ?</p>
      <div class="accordion-content">
      <p>Oui, les clients peuvent laisser des commentaires. Un pourcentage élevé d'avis positifs est un facteur essentiel de réussite sur Afrissime. C'est le meilleur moyen pour les acheteurs de savoir si vous êtes digne de leur confiance. Votre pourcentage d'avis positifs s'affiche à la page des offres et est l'une des premières informations visibles. Les clients sont plus enclins à acheter des produits de vendeurs ayant un pourcentage élevé d'avis positifs. Votre pourcentage d'avis positifs est l'une des statistiques clés utilisées sur Afrissime pour évaluer votre performance.
      </p>
      </div>
       <p class="accordion-toggle">Puis-je proposer des services cadeaux (emballage et messages) à mes
  clients sur Afrissime ?</p>
      <div class="accordion-content">
      <p>Oui. Notre service de messages cadeaux permet aux clients de joindre des messages à des articles individuels ou à des commandes complètes. Notre service d'emballage cadeau permet aux clients de sélectionner un emballage cadeau pour chaque article de leur commande. En tant que vendeur, vous pouvez mettre l'un de ces services, voire les deux, à la disposition des acheteurs de vos produits.
      </p>
      </div>
      <p class="accordion-toggle">A qui puis-je m'adresser si j'ai des questions concernant le programme Vendre sur Afrissime ?</p>
      <div class="accordion-content">
      <p>Notre équipe commerciale sera heureuse de détailler avec vous les avantages du programme. Remplissez le formulaire Contactez-nous.
      </p>
      </div>
    </div>
  </div>
      <div class="section text-center landing-section section-white">
    <div class="container">
      <h3 style="font-weight:700">Ils vendent déjà sur Afrissime</h3>
      <div class="video-container">
        <div class="col-md-8 col-md-offset-2">
      		<iframe width="849" height="534" src="https://www.youtube.com/embed/6ZfMa7h4vbo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
      </div>
        	{if $language.language_code == 'fr'}
            <a href="https://www.afrissime.com/demande-du-vendeur" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
          {else}
            <a href="https://www.afrissime.com/seller-request" class="btn  btn-effect" style="border-radius: 2em;min-width: 7em;">I create my shop</a>
          {/if}
    </div>
  </div>

      <div class="section landing-section">
        <div class="container">
          <div class="row" style="margin-top:20px">
            <div class="col-md-8 col-md-offset-2">
              <h3 style="font-weight:700; text-align:center">Contactez nous</h3>

              <form data-behaviour="disable-btn-on-submit" class="new_contact_form" id="new_contact_form" action="{$urls.pages.contact}" accept-charset="UTF-8" data-remote="true" method="post"><input name="utf8" type="hidden" value="✓">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="required" for="contact_form_email">Email</label>
                      <input class="form-control" placeholder="Email" type="text" value="" name="contact_form[email]" id="contact_form_email">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="required" for="contact_form_name">Name</label>
                      <input class="form-control" placeholder="Prénom/Nom" type="text" name="contact_form[name]" id="contact_form_name">
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="required" for="contact_form_message">Message</label>
                  <textarea class="form-control" rows="4" placeholder="Dites nous tout!" name="contact_form[message]" id="contact_form_message"></textarea>
                </div>

                <div class="row">
                  <div class="col-md-4 col-md-offset-4">
                    <button name="button" type="submit" class="btn btn-warning btn-block btn-lg btn-fill" data-action="loadbtn" data-disable-with="Contactez nous…" style="border-radius: 2em;min-width: 7em;">Contactez nous</button>
                  </div>
                </div>
      </form>      </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
<script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
{/block}
