{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}

{block name='page_header_container'}{/block}

{block name='left_column'}
  <div id="left-column" class="col-xs-12 col-sm-3">
   
  </div>
{/block}

{block name='page_content'}
  <h3 style="font-weight:700;font-size: 1.825em;margin: 20px 0 10px;text-align:center">Signaler un abus</h3>
  <p>Pour signaler n'importe quel type d'abus (spam, phishing, malware, etc...) ne respectant pas les règles d'Afrissime, envoyez nous un mail à support@afrissime.com ou remplissez le formulaire ci-dessous.</p>
  <div class="row">
    <div class="col-md-8">
      <section class="contact-form">
        <form action="{$urls.current_url} " method="post" enctype="multipart/form-data">

          <section class="form-fields">

            <div class="form-group row">
              <label class="col-md-3 form-control-label">{l s='Subject' d='Shop.Forms.Labels'}</label>
              <div class="col-md-6">
                <select name="id_contact" class="form-control form-control-select">
                    <option value="1">{l s='Phishing' d='Shop.Forms.Labels'}</option>
                    <option value="2">{l s='Malware' d='Shop.Forms.Labels'}</option>
                    <option value="3">{l s='Spam' d='Shop.Forms.Labels'}</option>
                    <option value="4">{l s='Contenu inapproprié' d='Shop.Forms.Labels'}</option>
                    <option value="5">{l s='Violation de propriété intellectuelle' d='Shop.Forms.Labels'}</option>
                </select>
              </div>
            </div>

            <div class="form-group row">
              <label class="col-md-3 form-control-label">{l s='Email address' d='Shop.Forms.Labels'}</label>
              <div class="col-md-6">
                <input
                  class="form-control"
                  name="from"
                  type="email"
                  value=""
                  placeholder="{l s='your@email.com' d='Shop.Forms.Help'}"
                  required=""
                >
              </div>
            </div>
              <div class="form-group row">
                <label class="col-md-3 form-control-label">{l s='Attachment' d='Shop.Forms.Labels'}</label>
                <div class="col-md-6">
                  <input type="file" name="fileUpload" class="filestyle">
                </div>
                <span class="col-md-3 form-control-comment">
                  {l s='optional' d='Shop.Forms.Help'}
                </span>
              </div>
            <div class="form-group row">
              <label class="col-md-3 form-control-label">{l s='Message' d='Shop.Forms.Labels'}</label>
              <div class="col-md-9">
                <textarea
                  class="form-control"
                  name="message"
                  placeholder="{l s='How can we help?' d='Shop.Forms.Help'}"
                  rows="3"
                  required=""
                ></textarea>
              </div>
            </div>

          </section>

          <footer class="form-footer text-xs-right">
            <input class="btn btn-default btn-effect" type="submit" name="submitMessage" value="{l s='Send' d='Shop.Theme.Actions'}">
          </footer>

        </form>
      </section>
    </div>
     <div class="col-md-4" style="text-align:justify">
     <h3>Comment nous traitons les plaintes</h3>
      <ul>
          <li> <i class="fa fa-check" aria-hidden="true"></i>
 Notre équipe en charge des abus lira la plainte pour déterminer le type d'abus.</li>
          <li> <i class="fa fa-check" aria-hidden="true"></i>
 En cas de commentaire déplacé à l'endroit d'un utilisateur, un avertissement sera envoyé au concerné. Si le concerné ne supprime pas le contenu non autorisé, son compte sera suspendu.</li>
          <li> <i class="fa fa-check" aria-hidden="true"></i>
 Dans le cas d'abus délibérés tels que : Phishing / Malware / Violation de la propriété intellectuelle, le compte de l'utilisateur sera immédiatement suspendu.</li>
          
      </ul>
    </div>
  </div>
{/block}
