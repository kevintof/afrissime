{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos.css" />
{/block}
{block name='page_content'}
<div id="pageshop">
  <div id="transcroller-body" class="aos-all">
      <div class="landing-header" style="background-image: url('https://www.afrissime.com/themes/jms_basel/assets/img/seller_cover.jpg');height:600px">
        <div class="container">
          <div class="intro_shop">
            <h2>Gagnez de l'argent</h2>
            <h3>en touchant des clients partout dans le monde</h3>
            <br>
              {if $language.language_code == 'fr'}
                  <a href="https://www.afrissime.com/demande-du-vendeur" class="btn  btn-effect" style="color: #000;background-color: #fff;border-radius: 2em;min-width: 7em;">J'ouvre ma boutique gratuitement</a>
                {else}
                  <a href="https://www.afrissime.com/seller-request" class="btn  btn-effect" style="color: #000;background-color: #fff;border-radius: 2em;min-width: 7em;">I create my shop</a>
                {/if}
          </div>
        </div>
      </div>
      <div class="main">
       <div class="section landing-section">
    <div class="container">
      <div class="row">
        <div class="md-12 text-center">
          <h3 style="font-weight:700">Comment vendre sur Afrissime?</h3>
          <p class="text-center">
          La vente sur Afrissime se fait en 3 étapes :
          </p>
            <h4 style="font-weight:600">Création de votre boutique</h4>
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-4">
         <div class="info aos-item" data-aos="fade-in">
            <div>
              <div class="icon icon-primary">
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/shop_step_1.jpg" alt="Recherche produit" >
              </div>
              <div class="description">
                <p><strong>1. Renseignez vos informations personnels </strong></p>
                <p class="text-center">Ces informations serviront à vous créer en même temps un espace personnel sur Afrissime </p>
              </div>
            </div>
            
         </div>
         <div style="float:right;">
                <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png"  alt="Next" >
         </div>
        </div>
        <div class="col-md-4">
           <div class="info aos-item" data-aos="fade-in">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/shop_step_2.jpg" alt="Inscription" >
              </div>
              <div class="description">
                <p><strong>2.Renseignez les informations à propos de la boutique</strong></p>
                <p class="text-center">Donnez plus d'information pour votre boutique afin de permettre aux acheteurs d'en savoir plus vos conditions de livraison et options de paiement</p>
              </div>
           </div>
           <div style="float:right;">
                <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png"  alt="Next" >
           </div>
        </div>
        <div class="col-md-4">
           <div class="info aos-item" data-aos="fade-in">
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/shop_step_3.jpg" alt="Conditions générales de vente" >
              </div>
              <div class="description">
                <p><strong>3.Chochez les conditions générales de vente</strong></p>
                <p class="text-center">Après lecture des conditions générales de vente vous devez cliquer afin de donner votre accord.</p>
              </div>
           </div>
           
        </div>
      </div>
      <div class="row">
        <div class="md-12 text-center">
            <h4 style="font-weight:600">Gestion de vos produits</h4>
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-6">
         <div class="info aos-item" data-aos="zoom-out">
          
            <div class="icon icon-primary">
              <img src="https://www.afrissime.com/themes/jms_basel/assets/img/account_step_1.jpg"  alt="Mon compte" >
            </div>
            <div class="description">
              <p><strong>1. Connectez vous sur Afrissime</strong></p>
              <p class="text-center">Vous devez vous connecter à votre espace personnel avant de pouvoir gérer votre boutique.Une fois connecté, cliquez sur le menu "Produit" pour accéder à la liste de vos produits. </p>
            </div>
         </div>
          <div style="float:right;">
                <img class="next hidden-sm hidden-xs" src="https://www.afrissime.com/themes/jms_basel/assets/img/next.png"  alt="Next" >
           </div>
        </div>
        <div class="col-md-6">
           <div class="info aos-item" data-aos="zoom-out">
             
              <div class="icon icon-warning">
                <!--i class="ti-light-bulb"></i-->
                <img src="https://www.afrissime.com/themes/jms_basel/assets/img/account_step_2.jpg"  alt="Liste de vos produits">
              </div>
              <div class="description">
                <p><strong>2. Gestion de vos articles</strong></p>
                <p class="text-center">Vous pouvez gérer vos produits (ajouter, modifier les informations) à partir de cette page.</p>
              </div>
           </div>
        </div>
      </div>
       <div class="row">
        <div class="md-12 text-center">
            <h4 style="font-weight:600">Faîtes connaitre votre boutique</h4>
        </div>
      </div>
      <div class="row stepper">
        <div class="col-md-12">
         <div class="info aos-item" data-aos="zoom-out">
          
            <div class="icon icon-primary">
              <img src="https://www.afrissime.com/themes/jms_basel/assets/img/shop_step_4.jpg" alt="Ma boutique" >
            </div>
            <div class="description">
              <p><strong>Partagez le lien de votre boutique sur vos réseaux sociaux</strong></p>
              <p class="text-center">Faire connaître votre boutique auprès de vos contacts et auprès de la communauté d'Afrissime, vous permettra de gagner en visibilité. </p>
            </div>
         </div>
        </div>
      </div>
    </div>
  </div>
    </div>
</div>
<script>
      AOS.init({
        easing: 'ease-in-out-sine'
      });
    </script>
{/block}
