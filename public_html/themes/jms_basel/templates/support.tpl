{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
<script type="text/javascript" src="https://www.afrissime.com/themes/jms_basel/assets/js/aos.js"></script>
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos_styles.css" />
 <link rel="stylesheet" href="https://www.afrissime.com/themes/jms_basel/assets/css/aos.css" />
 <style>
    a:hover{
     border-color: #A67E2E;
    }
 </style>
{/block}
{block name='page_header_container'}{/block}

 {block name='page_content_top'}
    
 {/block}
  {block name='page_content'}
   
<div class="container-fluid mx-auto center-block text-center">
  <div class="row center-block text-center">
    
    <div class="col-md-3">
        <a href="#">
       <img  src="https://1stdibs--c.na65.content.force.com/servlet/servlet.ImageServer?id=0150L00000ArO5Q&oid=00DE0000000IfI6&lastMod=1527626854000"/>
        </a>
  </div>
  <div class="col-md-3">
      <a href="https://www.afrissime.com/create-your-shop">
    
        <img src="https://1stdibs--c.na39.content.force.com/servlet/servlet.ImageServer?id=0150L00000ArO5a&oid=00DE0000000IfI6&lastMod=1527626928000"/>
      </a>
  </div>
   <div class="col-md-3">
        <a href="https://www.afrissime.com/en/login?back=my-account">
            <img src="https://1stdibs--c.na39.content.force.com/servlet/servlet.ImageServer?id=0150L00000ArO5L&oid=00DE0000000IfI6&lastMod=1527626816000"/>
        </a>
    </div>  
    <div class="col-md-3">
        <a href="https://www.afrissime.com/en/login?back=my-account">
            <img src="https://1stdibs--c.na39.content.force.com/servlet/servlet.ImageServer?id=0150L00000ArO5G&oid=00DE0000000IfI6&lastMod=1527626750000"/>
        </a>
    </div>    
  </div>
  </div>
  <br/>
  
  {/block}
