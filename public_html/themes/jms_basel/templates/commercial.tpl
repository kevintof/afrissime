{**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2016 PrestaShop SA
 * @license   http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file='page.tpl'}
{block name='head' prepend}
{/block}
{block name='page_header_container'}{/block}

 {block name='page_content_top'}
    
 {/block}
  {block name='page_content'}
   
     <!-- MultiStep Form -->
     <div class="wk-mp-block" style="border:1px solid #d5d5d5;">
        <div style="background-color:#333;overflow: hidden;padding: 20px 25px;text-transform: uppercase;">
          <span style="font-size: 18px;font-weight: 400;color:#FFF;">{l s='Devenir commercial consultant' d='Shop.Theme.Global'}</span>
        </div>
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="" method="post" id="wk_mp_seller_form" enctype="multipart/form-data">
                <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active">Personal Details</li>
                    <li>Social Profiles</li>
                    <li>Account Setup</li>
                </ul>
                <!-- fieldsets -->
                <fieldset>
                    <h2 class="fs-title">Personal Details</h2>
                    <h3 class="fs-subtitle">Tell us something more about you</h3>
                    <input type="text" name="fname" placeholder="First Name"/>
                    <input type="text" name="lname" placeholder="Last Name"/>
                    <input type="text" name="phone" placeholder="Phone"/>
                    <input type="button" name="next" class="next action-button" value="Next"/>
                </fieldset>
                <fieldset>
                    <h2 class="fs-title">Social Profiles</h2>
                    <h3 class="fs-subtitle">Your presence on the social network</h3>
                    <input type="text" name="twitter" placeholder="Twitter"/>
                    <input type="text" name="facebook" placeholder="Facebook"/>
                    <input type="text" name="gplus" placeholder="Google Plus"/>
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                    <input type="button" name="next" class="next action-button" value="Next"/>
                </fieldset>
                <fieldset>
                    <h2 class="fs-title">Create your account</h2>
                    <h3 class="fs-subtitle">Fill in your credentials</h3>
                    <input type="text" name="email" placeholder="Email"/>
                    <input type="password" name="pass" placeholder="Password"/>
                    <input type="password" name="cpass" placeholder="Confirm Password"/>
                    <input type="button" name="previous" class="previous action-button-previous" value="Previous"/>
                    <input type="submit" name="submit" class="submit action-button" value="Submit"/>
                </fieldset>
            </form>
        </div>
      </div>
     </div>
<!-- /.MultiStep Form -->
        
        
  {/block}