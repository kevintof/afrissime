{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

	<input type="hidden" name="default_lang" value="{$mp_seller_info.default_lang}" />

<div class="form-group row">
	<div class="col-md-6">
		<label for="seller_lastname" class="control-label required">
			{l s='Last Name' mod='marketplace'}
		</label>
		<input class="form-control"
		type="text"
		value="{if isset($smarty.post.seller_lastname)}{$smarty.post.seller_lastname}{else}{$mp_seller_info.seller_lastname}{/if}"
		name="seller_lastname"
		id="seller_lastname" />
	</div>
	<div class="col-md-6">
		<label for="seller_firstname" class="control-label required">
			{l s='First Name' mod='marketplace'}
		</label>
		<input class="form-control"
		type="text"
		value="{if isset($smarty.post.seller_firstname)}{$smarty.post.seller_firstname}{else}{$mp_seller_info.seller_firstname}{/if}"
		name="seller_firstname"
		id="seller_firstname" />
	</div>
</div>
<div class="form-group row">
						<div class="col-md-6">
							<div class="row">
								<label for="seller_gender" class="control-label required">
									{l s='Sexe' mod='marketplace'}
								</label>
							</div>
							<div class="row">
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="1" {if $mp_customer_id_gender==1}checked {/if} onclick="hide_box_gender();">
						              <span></span>
						            </span>
						          Homme
						        </label>
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="2" {if $mp_customer_id_gender==2}checked {/if} onclick="hide_box_gender();">
						              <span></span>
						            </span>
						           Femme
						        </label>
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="3" {if $mp_customer_id_gender==3}checked {/if} onclick="show_box_gender();">
						              <span></span>
						            </span>
						           Personnalisé
						        </label>
					        </div>
					        <div id="box_gender" {if $mp_customer_id_gender==3}checked {else} style="display:none" {/if} >
					        	<div class="row">
					        		<span>{l s='Indiquer votre sexe' mod='marketplace'}</span>
					        		<p>{l s='Me désigner avec le pronom suivant:' mod='marketplace'}</p>
					        		 <input class="form-control" name="seller_title" type="text" value="{$mp_seller_info.seller_title}" placeholder="{l s='Pronom' mod='marketplace'}">
					        	</div>
					        </div>
					    </div>
					    <div class="col-md-6">
					    	<label for="seller_birthday" class="control-label">
								{l s='Date d\'anniversaire' mod='marketplace'}
							</label>
					        <input class="form-control" name="seller_birthday" type="text" value="{$customer.birthday}" placeholder=" {l s='DD/MM/YYYY' mod='marketplace'}" required="required">
					                  <span class="form-control-comment">
					            (Ex. : 31/05/1970)
					          </span>
					    </div>
</div>
			    	<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_nationality" class="control-label required">
								{l s='Nationalité' mod='marketplace'}
							</label>
							<select name="seller_nationality" id="seller_nationality" class="form-control form-control-select" required="required" style="height:40px; width:100%">
								<option value="">{l s='Select Country' mod='marketplace'}</option>
								{foreach $african_country as $countrydetail}
									<option value="{$countrydetail.id_country}" {if $mp_seller_info.seller_nationality==$countrydetail.id_country} selected{/if}>
										{$countrydetail.name}
									</option>
								{/foreach}
							</select>
						</div>
						<div class="col-md-6">
							<label for="residence" class="control-label required">
								{l s='Pays de résidence' mod='marketplace'}
							</label>
							<select name="residence" id="residence" required="required" class="form-control form-control-select" style="height:40px; width:100%">
								<option value="">{l s='Select Country' mod='marketplace'}</option>
								{foreach $country as $countrydetail}
									<option value="{$countrydetail.id_country}" {if $mp_seller_info.residence==$countrydetail.id_country} selected{/if}>
										{$countrydetail.name}
									</option>
								{/foreach}
							</select>
						</div>
					</div>
				    <div class="form-group row">
						<div class="col-md-6">
							<label for="seller_email" class="control-label required">
								{l s='Email' mod='marketplace'}
							</label>
							<input class="form-control"
							type="email"
							value="{$customer.email}"
							name="seller_email"
							id="seller_email"
							required="required"
							/>
							<span class="wk-msg-selleremail"></span>
						</div>
						<div class="col-md-6">
							<label for="phone" class="control-label required">
								{l s='Numéro Téléphone' mod='marketplace'}
							</label>
							<input class="form-control"
							type="text"
							value="{if isset($mp_seller_info.phone)}{$mp_seller_info.phone}{/if}"
							name="phone"
							id="phone"
							maxlength="" required="required"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_password" class="control-label required">
								{l s='Mot de passe' mod='marketplace'}
							</label>
							<input class="form-control"
							type="password"
							value=""
							name="seller_password"
							id="seller_password"
							/>
							<span class="wk-msg-customeremail"></span>
						</div>
						<div class="col-md-6">
							<label for="customer_confpassword" class="control-label required">
								{l s='Confirmation du mot de passe' mod='marketplace'}
							</label>
							<input class="form-control"
							type="text"
							value=""
							name="seller_confpassword"
							id="seller_confpassword"
							 />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<label for="address" class="control-label required">
								{l s='Adresse' mod='marketplace'}
							</label>
						</div>
						<textarea class="form-control" name="address"> {if isset($smarty.post.address)}{$smarty.post.address}{else}{$mp_seller_info.address}{/if}</textarea>
					</div>
					<div class="form-group row">
							<div class="col-md-6" id="seller_zipcode">
								<label for="postcode" class="control-label">
									{l s='Zip/Postal Code' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.postcode)}{$smarty.post.postcode|escape:'htmlall':'UTF-8'}{/if}"
								name="postcode"
								id="postcode" />
							</div>
							<div class="col-md-6">
								<label for="city" class="control-label required">
									{l s='City' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.city)}{$smarty.post.city|escape:'htmlall':'UTF-8'}{/if}"
								name="city"
								id="city"
								maxlength="64" />
							</div>
						</div>
{hook h="displayMpEditProfileInformationBottom"}