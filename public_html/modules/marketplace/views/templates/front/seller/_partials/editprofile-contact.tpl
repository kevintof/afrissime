{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}
<div class="form-group row">
	 <div class="avatar-upload">
        <div class="avatar-edit">
            <input type='file' id="imageUpload" name="shopimage[]" accept=".png, .jpg, .jpeg" class="uploadimg_container"/>
            <label for="imageUpload"></label>
        </div>
        <div class="avatar-preview">
            <div id="imagePreview" style="background-image:url({if isset($shop_img_path)}{$shop_img_path}?timestamp={$timestamp}{else} {$shop_default_img_path}?timestamp={$timestamp} {/if});">
            </div>
        </div>
    </div>
</div>
<div class="form-group">
	<div class="col-md-6">
		<label for="shop_name" class="control-label required">
			{l s='Shop Name' mod='marketplace'}
			{block name='mp-form-fields-flag'}
				{include file='module:marketplace/views/templates/front/_partials/mp-form-fields-flag.tpl'}
			{/block}
		</label>
		{foreach from=$languages item=language}
			{assign var="shop_name" value="shop_name_`$language.id_lang`"}
			<input class="form-control shop_name_all wk_text_field_all wk_text_field_{$language.id_lang}
			{if $current_lang.id_lang == $language.id_lang}seller_default_shop{/if}
			{if $current_lang.id_lang != $language.id_lang}wk_display_none{/if}"
			type="text"
			value="{if isset($smarty.post.$shop_name)}{$smarty.post.$shop_name}{else}{$mp_seller_info.shop_name[{$language.id_lang}]}{/if}"
			id="shop_name_{$language.id_lang}"
			name="shop_name_{$language.id_lang}"
			data-lang-name="{$language.name}" />
		{/foreach}
		<span class="wk-msg-shopname"></span>
	</div>
	<div class="col-md-6">
		<span style="position:relative; right:210px;top:30px;">.afrissime.com</span>
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label for="about_shop" class="control-label">
			{l s='About Shop' mod='marketplace'}

			{block name='mp-form-fields-flag'}
				{include file='module:marketplace/views/templates/front/_partials/mp-form-fields-flag.tpl'}
			{/block}
		</label>
		{foreach from=$languages item=language}
			{assign var="about_shop" value="about_shop_`$language.id_lang`"}
			<div id="about_business_div_{$language.id_lang}" class="wk_text_field_all wk_text_field_{$language.id_lang} {if $current_lang.id_lang != $language.id_lang}wk_display_none{/if}">
				<textarea
				name="about_shop_{$language.id_lang}"
				id="about_business_{$language.id_lang}" cols="2" rows="3"
				class="about_business wk_tinymce form-control">{if isset($smarty.post.$about_shop)}{$smarty.post.$about_shop}{else}{$mp_seller_info.about_shop[{$language.id_lang}]}{/if}</textarea>
			</div>
		{/foreach}
	</div>
</div>
<div style="width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;height:300px;background-image: url({if isset($shop_banner_path)}{$shop_banner_path}?timestamp={$timestamp}{else} https://www.afrissime.com/modules/marketplace/views/img/shop_banner/default_cover_pageshop.jpg {/if});" id="cover_shop">
	<div style="position:relative; top:110px;">
	<h3 style="text-align:center;padding: 10px;background-color: #fff;opacity: 0.7;">{l s='Image de couverture boutique' mod='marketplace'}</h3>
		<div class="input-group" style="margin: 0 auto;"> 
		    <div class="custom-file">
			    <input type="file" name="shopbannerimage[]" id="inputGroupFile01" class="imgInp custom-file-input uploadimg_container" aria-describedby="inputGroupFileAddon01">
			 </div>
	  	</div>
	</div>
</div>

<div class="form-group row">
	<div class="col-md-6">
		<label for="business_email" class="control-label required">
			{l s='Business Email' mod='marketplace'}
		</label>
		<input class="form-control"
		type="email"
		value="{if isset($smarty.post.business_email)}{$smarty.post.business_email}{else}{$mp_seller_info.business_email}{/if}"
		name="business_email"
		id="business_email"
		onblur="onblurCheckUniqueSellerEmail();" />
		<span class="wk-msg-selleremail"></span>
	</div>
	<div class="col-md-6">
		<label for="phone" class="control-label required">
			{l s='Phone' mod='marketplace'}
		</label>
		<input class="form-control"
		type="text"
		value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{$mp_seller_info.phone}{/if}"
		name="phone"
		id="phone"
		maxlength="{$max_phone_digit}" />
	</div>
</div>
{if Configuration::get('WK_MP_SELLER_FAX') || Configuration::get('WK_MP_SELLER_TAX_IDENTIFICATION_NUMBER')}
	<div class="form-group row">
		{if Configuration::get('WK_MP_SELLER_FAX')}
			<div class="col-md-6">
				<label for="fax" class="control-label">{l s='Fax' mod='marketplace'}</label>
				<input class="form-control"
				type="text"
				value="{if isset($smarty.post.fax)}{$smarty.post.fax}{else}{$mp_seller_info.fax}{/if}"
				name="fax"
				id="fax" />
			</div>
		{/if}
		{if Configuration::get('WK_MP_SELLER_TAX_IDENTIFICATION_NUMBER')}
			<div class="col-md-6">
				<label for="fax" class="control-label">{l s='Tax Identification Number' mod='marketplace'}</label>
				<input class="form-control"
				type="text"
				value="{if isset($smarty.post.tax_identification_number)}{$smarty.post.tax_identification_number}{else}{$mp_seller_info.tax_identification_number}{/if}"
				name="tax_identification_number"
				id="tax_identification_number" />
			</div>
		{/if}
	</div>
{/if}

<div class="form-group row">
	{if $seller_country_need}
		<div class="col-md-6" id="seller_zipcode">
			<label for="postcode" class="control-label required">
				{l s='Zip/Postal Code' mod='marketplace'}
			</label>
			<input class="form-control"
			type="text"
			value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{$mp_seller_info.postcode}{/if}"
			name="postcode"
			id="postcode" />
		</div>
		<div class="col-md-6">
			<label for="city" class="control-label required">
				{l s='City' mod='marketplace'}
			</label>
			<input class="form-control"
			type="text"
			value="{if isset($smarty.post.city)}{$smarty.post.city}{else}{$mp_seller_info.city}{/if}"
			name="city"
			id="city"
			maxlength="64" />
		</div>
	{/if}
</div>

{if $seller_country_need && isset($country)}
	<div class="form-group row">
		<div class="col-md-6">
			<label for="id_country" class="control-label required">
				{l s='Country' mod='marketplace'}
			</label>
			<select name="id_country" id="id_country" class="form-control form-control-select">
				<option value="">{l s='Select Country' mod='marketplace'}</option>
				{foreach $country as $countrydetail}
					<option value="{$countrydetail.id_country}"
					{if $mp_seller_info.id_country == $countrydetail.id_country}Selected="Selected"{/if}>
						{$countrydetail.name}
					</option>
				{/foreach}
			</select>
		</div>
		<div id="wk_seller_state_div" class="col-md-6 {if !$mp_seller_info['id_state']}wk_display_none{/if}">
			<label for="id_state" class="control-label required">
				{l s='State' mod='marketplace'}
			</label>
			<select name="id_state" id="id_state" class="form-control form-control-select">
				<option value="">{l s='Select State' mod='marketplace'}</option>
			</select>
			<input type="hidden" name="state_available" id="state_available" value="0" />
		</div>
	</div>
{/if}