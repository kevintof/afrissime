{*
* 2010-2019 Webkul.
*
* NOTICE OF LICENSE
*
* All rights is reserved,
* Please go through this link for complete license : https://store.webkul.com/license.html
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to https://store.webkul.com/customisation-guidelines/ for more information.
*
*  @author    Webkul IN <support@webkul.com>
*  @copyright 2010-2019 Webkul IN
*  @license   https://store.webkul.com/license.html
*}

{extends file=$layout}
{block name="head" prepend}

{literal}
 <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/css/intlTelInput.css" rel="stylesheet" type="text/css"/>
<script>
// hack to get the plugin to work without loading jQuery
window.jQuery = window.$ = function() {
  return {
    on: function() {}
  };
};
window.$.fn = {};
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/js/intlTelInput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.0/js/utils.js"></script>
<script>
    $("#phone").intlTelInput({
	 initialCountry: "auto",
          geoIpLookup: function(callback) {
            $.get('https://ipinfo.io', function() {}, "jsonp").always(function(resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              callback(countryCode);
            });
          },
          utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.1.0/js/utils.js" // just for formatting/placeholders etc
    });
  </script> 

{/literal}
{/block}
{block name='content'}
<script type="text/javascript" src="{$smarty.const._MODULE_DIR_}marketplace/views/js/tinymce/tinymce.min.js"></script>
<script type="text/javascript" src="{$smarty.const._MODULE_DIR_}marketplace/views/js/tinymce/tinymce_wk_setup.js"></script>
{literal}
<script type="text/javascript">
function show_box_gender(){
  $('#box_gender').show();
}
function hide_box_gender(){
  $('#box_gender').hide();
}
function show_payment_box_1(){
	$('#payment_box_1').show();
	$('#payment_box_2').hide();
	$('#payment_box_3').hide();
}
function show_payment_box_2(){
	$('#payment_box_1').hide();
	$('#payment_box_2').show();
	$('#payment_box_3').show();
}
</script>
{/literal}
<input type="hidden" name="token" id="wk-static-token" value="{$static_token}">
{if isset($is_seller)}
	
		{if $is_seller == 0}
			<div class="alert alert-info">
			</div>
		{else}
			<div class="alert alert-info">
				{l s='Your request has been approved by admin. ' mod='marketplace'}
				<a href="{$link->getModuleLink('marketplace','addproduct')}">
					<button class="btn btn-primary btn-sm">
						{l s='Add Your First Product' mod='marketplace'}
					</button>
				</a>
			</div>
		{/if}
{else}
	<div class="wk-mp-block" style="border:1px solid #d5d5d5;">
		<div class="page-title" style="background-color:{$title_bg_color};">
			<span style="color:{$title_text_color};">{l s='Seller Request' mod='marketplace'}</span>
		</div>
		<form action="{$link->getModuleLink('marketplace', 'sellerrequest')}" method="post" id="wk_mp_seller_form" enctype="multipart/form-data">
			<div class="wk-mp-right-column">
				<div class="alert alert-danger wk_display_none" id="wk_mp_form_error"></div>
				<!-- progressbar -->
	            <ul id="progressbar">
	                <li class="active">Renseignements personnels</li>
	                <li>Renseignements boutique</li>
	                <li>Conditions générales de vente</li>
	            </ul>
				<fieldset>
					 <h2 class="fs-title">Renseignements personnels</h2>
                	<h3 class="fs-subtitle">Parlez nous un peu de vous</h3>
					<input type="hidden" name="current_lang" id="current_lang" value="{$current_lang.id_lang}">
				
					<input type="hidden" name="default_lang" value="{$context_language}" />
					
					<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_lastname" class="control-label required">
								{l s='Last Name' mod='marketplace'}
							</label>
							<input class="form-control"
							type="text"
							value="{if isset($smarty.post.seller_lastname)}{$smarty.post.seller_lastname}{else}{$customer_lastname}{/if}"
							name="seller_lastname"
							id="seller_lastname" required="required"/>
						</div>
						<div class="col-md-6">
							<label for="seller_firstname" class="control-label required">
								{l s='First Name' mod='marketplace'}
							</label>
							<input class="form-control"
							type="text"
							value="{if isset($smarty.post.seller_firstname)}{$smarty.post.seller_firstname}{else}{$customer_firstname}{/if}"
							name="seller_firstname"
							id="seller_firstname" required="required"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<div class="row">
								<label for="seller_gender" class="control-label required">
									{l s='Sexe' mod='marketplace'}
								</label>
							</div>
							<div class="row">
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="1" checked onclick="hide_box_gender();">
						              <span></span>
						            </span>
						          Homme
						        </label>
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="2" onclick="hide_box_gender();">
						              <span></span>
						            </span>
						           Femme
						        </label>
						        <label class="radio-inline">
						            <span class="custom-radio">
						              <input name="seller_gender" type="radio" value="3" onclick="show_box_gender();">
						              <span></span>
						            </span>
						           Personnalisé
						        </label>
					        </div>
					        <div id="box_gender" style="display:none">
					        	<div class="row">
					        		<span>Indiquer votre sexe</span>
					        		<p>Me désigner avec le pronom suivant: </p>
					        		 <input class="form-control" name="seller_title" type="text" value="" placeholder="Pronom">
					        	</div>
					        </div>
					    </div>
					    <div class="col-md-6">
					    	<label for="seller_birthday" class="control-label">
								{l s='Date d\'anniversaire' mod='marketplace'}
							</label>
					        <input class="form-control" name="seller_birthday" type="text" value="" placeholder="DD/MM/YYYY" required="required">
					                  <span class="form-control-comment">
					            (Ex. : 31/05/1970)
					          </span>
					    </div>
				    </div>
			    	<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_nationality" class="control-label required">
								{l s='Nationalité' mod='marketplace'}
							</label>
							<select name="seller_nationality" id="seller_nationality" class="form-control form-control-select" required="required" style="height:40px; width:100%">
								<option value="">{l s='Select Country' mod='marketplace'}</option>
								{foreach $african_country as $countrydetail}
									<option value="{$countrydetail.id_country}">
										{$countrydetail.name}
									</option>
								{/foreach}
							</select>
						</div>
						<div class="col-md-6">
							<label for="residence" class="control-label required">
								{l s='Pays de résidence' mod='marketplace'}
							</label>
							<select name="residence" id="residence" required="required" class="form-control form-control-select" style="height:40px; width:100%">
								<option value="">{l s='Select Country' mod='marketplace'}</option>
								{foreach $country as $countrydetail}
									<option value="{$countrydetail.id_country}">
										{$countrydetail.name}
									</option>
								{/foreach}
							</select>
						</div>
					</div>
				    <div class="form-group row">
						<div class="col-md-6">
							<label for="seller_email" class="control-label required">
								{l s='Email' mod='marketplace'}
							</label>
							<input class="form-control"
							type="email"
							value="{$customer_email}"
							name="seller_email"
							id="seller_email"
							required="required"
							/>
							<span class="wk-msg-selleremail"></span>
						</div>
						<div class="col-md-6">
							<label for="phone" class="control-label required">
								{l s='Numéro Téléphone' mod='marketplace'}
							</label>
							<input class="form-control"
							type="tel"
							value="{if isset($smarty.post.phone)}{$smarty.post.phone}{/if}"
							name="phone"
							id="phone"
							maxlength="{$max_phone_digit}" required="required"/>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="seller_password" class="control-label required">
								{l s='Mot de passe' mod='marketplace'}
							</label>
							<input class="form-control"
							type="password"
							value=""
							name="seller_password"
							id="seller_password"
							/>
							<span class="wk-msg-customeremail"></span>
						</div>
						<div class="col-md-6">
							<label for="customer_confpassword" class="control-label required">
								{l s='Confirmation du mot de passe' mod='marketplace'}
							</label>
							<input class="form-control"
							type="password"
							value=""
							name="seller_confpassword"
							id="seller_confpassword"
							 />
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-12">
							<label for="seller_address" class="control-label required">
								{l s='Adresse' mod='marketplace'}
							</label>
						</div>
						<textarea class="form-control" name="address"></textarea>
					</div>
					{if $seller_country_need}
						<div class="form-group row">
							<div class="col-md-6" id="seller_zipcode">
								<label for="postcode" class="control-label">
									{l s='Zip/Postal Code' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.postcode)}{$smarty.post.postcode|escape:'htmlall':'UTF-8'}{/if}"
								name="postcode"
								id="postcode" />
							</div>
							<div class="col-md-6">
								<label for="city" class="control-label required">
									{l s='City' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.city)}{$smarty.post.city|escape:'htmlall':'UTF-8'}{/if}"
								name="city"
								id="city"
								maxlength="64" />
							</div>
						</div>
					{/if}
					{if $tax_identification_number}
						<div class="form-group row">
							<div class="col-md-6" id="seller_tax_identification_number">
								<label for="tax_identification_number" class="control-label">
									{l s='Tax Identification Number/VAT' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.tax_identification_number)}{$smarty.post.tax_identification_number|escape:'htmlall':'UTF-8'}{/if}"
								name="tax_identification_number"
								id="tax_identification_number"
								maxlength="64" />
							</div>
						</div>
					{/if}
					

					{hook h="displayMpSellerRequestFooter"}
					{hook h='displayGDPRConsent' mod='psgdpr' id_module=$id_module}

					{block name='mp-form-fields-notification'}
						{include file='module:marketplace/views/templates/front/_partials/mp-form-fields-notification.tpl'}
					{/block}
					<input type="button" name="next" class="next action-button" value="Suivant"/>
				</fieldset>
				<fieldset>
					  <h2 class="fs-title">Renseignements boutique</h2>
                		<h3 class="fs-subtitle">Donnez des informations sur votre boutique</h3>
                		<div class="form-group row">
                			 <div class="avatar-upload">
						        <div class="avatar-edit">
						            <input type='file' id="imageUpload" name="shopimage[]" accept=".png, .jpg, .jpeg" />
						            <label for="imageUpload"></label>
						        </div>
						        <div class="avatar-preview">
						            <div id="imagePreview" style="background-image: url(https://www.afrissime.com/modules/marketplace/views/img/shop_img/defaultshopimage.jpg?time=1570007517);">
						            </div>
						        </div>
						    </div>
                		</div>
                		
	                	<div class="form-group row">
		                	<div class="col-md-6">
		                		<div class="form-group seller_shop_name_uniq">
									<label for="shop_name_unique" class="control-label required">
										{l s='Shop Name' mod='marketplace'}
										<div class="wk_tooltip">
											<span class="wk_tooltiptext">{l s='This name will be used in your shop URL' mod='marketplace'}</span>
										</div>
									</label>
									<img style="display: none;" width="25" src="{$modules_dir}marketplace/views/img/loading-small.gif" class="seller-loading-img"/>
									<input class="form-control"
										type="text"
										value="{if isset($smarty.post.shop_name_unique)}{$smarty.post.shop_name_unique}{/if}"
										id="shop_name_unique"
										name="shop_name_unique"
										onblur="onblurCheckUniqueshop();"
										autocomplete="off" />
									<span class="wk-msg-shopnameunique"></span>
									<p><span class="wk-msg-domaineunique"></span><p>
								</div>
							</div>
							<div class="col-md-6">
								<span style="position:relative; right:210px;top:30px;">.afrissime.com</span>
							</div>
						</div>
						
						<div class="form-group row">
							<div class="col-md-12">
								<label for="about_shop" class="control-label required">
									{l s='Description de la boutique' mod='marketplace'}
								</label>
							</div>
							<textarea name="about_shop_1" class="form-control" placeholder=""></textarea>
						</div>
						<div style="width:100%; overflow:hidden; padding:30px;margin-top:20px;margin-bottom:20px;height:300px;background-image: url(https://www.afrissime.com/modules/marketplace/views/img/shop_banner/default_cover_pageshop.jpg);" id="cover_shop">
							<div style="position:relative; top:160px;">
								<h3 style="text-align:center;padding: 10px;background-color: #fff;opacity: 0.7;">{l s='Image de couverture boutique' mod='marketplace'}</h3>
								<div class="input-group" style="margin: 0 auto;"> 
								    <div class="custom-file">
									    <input type="file" name="profilebannerimage[]" id="inputGroupFile01" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
									 </div>
						      	</div>
					      	</div>
				      	</div>
				      <div class="form-group row">
							<div class="col-md-6">
								<label for="business_email" class="control-label required">
									{l s='Email boutique' mod='marketplace'}
								</label>
								<input class="form-control"
								type="email"
								value="{if isset($smarty.post.business_email)}{$smarty.post.business_email}{else}{$customer_email}{/if}"
								name="business_email"
								id="business_email"
								onblur="onblurCheckUniqueSellerEmail();" />
								<span class="wk-msg-selleremail"></span>
								<span>Pour recevoir un email à chaque vente</span>
							</div>
							<div class="col-md-6">
								<label for="phone_shop" class="control-label required">
									{l s='Phone' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value="{if isset($smarty.post.phone)}{$smarty.post.phone}{/if}"
								name="phone_shop"
								id="phone_shop"
								maxlength="{$max_phone_digit}" />
								<span>Pour recevoir un SMS à chaque vente</span>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="id_card" class="control-label required">
									{l s='Pièce d\'identité du titulaire de la boutique' mod='marketplace'}
								</label>
								<input type="file" name="id_card"/>
							</div>
							<div class="col-md-6">
								<label for="payment_method" class="control-label required">
									{l s='Canal de versement des paiements' mod='marketplace'}
								</label>
								<div class="row">
									<input type="checkbox" name="bank_transfer" value="1" style="width:10%" onclick="show_payment_box_1();"/> Virement bancaire </br>
									<div id="payment_box_1" style="display:none">
										<p>Informations bancaires</p>
										<div class="col-md-6">
											<label for="bank_name" class="control-label required">
											{l s='Nom de la banque' mod='marketplace'}
											</label>
											<input type="text" name="bank_name" class="form-control" placeholder="Nom de la banque"/>
										</div>
										<div class="col-md-6">
											<label for="bank_name" class="control-label required">
											{l s='RIB' mod='marketplace'}
											</label>
											<input type="text" name="rib" class="form-control" placeholder="RIB"/>
										</div>
									</div>
								</div>
								<div class="row">
									<input type="checkbox" name="credit_card" value="2" style="width:10%" onclick="show_payment_box_2();"/> Carte de débit ou de crédit </br>
									<div id="payment_box_2" style="display:none">
										<p>Informations de la carte Visa ou Mastercard</p>
										<div>
											<label for="name_card" class="control-label required">
											{l s='Nom du titulaire de la carte' mod='marketplace'}
											</label>
											<input type="text" name="name_card" class="form-control" placeholder="Nom"/>
										</div>
										<div>
											<div class="col-md-6">
												<label for="num_card" class="control-label required">
												{l s='Numéro de la carte' mod='marketplace'}
												</label>
												<input type="text" name="num_card" class="form-control" placeholder="Ex: 4000 1234 5678 9101"/>
											</div>
											<div class="col-md-6">
												<label for="card_expiration" class="control-label required">
												{l s='Expire le' mod='marketplace'}
												</label>
												<input type="text" name="card_expiration" class="form-control" placeholder=" Ex: 12/21"/>
											</div>
										</div>
									</div>
								</div>
									<div class="row">
										<input type="checkbox" name="payment_method" value="3" style="width:10%" onclick="show_payment_box_3();"/> Mobile money </br>
										<div id="payment_box_3" style="display:none">
										<p>Numéro pour le débit ou le crédit</p>
										<div class="">
											<label for="name_card" class="control-label required">
											{l s='Numéro' mod='marketplace'}
											</label>
											<input type="text" name="num_mobile" class="form-control" placeholder="Numéro"/>
										</div>
									</div>
								</div>
								<input type="checkbox" name="cash" value="4" style="width:10%"/> Cash </br>
								<input type="checkbox" name="bitcoin" value="5" style="width:10%"/> Bitcoin </br>
								<input type="checkbox" name="bank_wallet" value="6" style="width:10%"/> Bank wallet </br>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="id_country" class="control-label required">
									{l s='Localisation de votre boutique' mod='marketplace'}
								</label>
								<select name="id_country" id="id_country" class="form-control form-control-select" required="required" style="height:40px; width:100%">
									<option value="">{l s='Select Country' mod='marketplace'}</option>
									{foreach $country as $countrydetail}
										<option value="{$countrydetail.id_country}">
											{$countrydetail.name}
										</option>
									{/foreach}
								</select>
								<span class="wk-msg-sellerlocalisation"></span>
							</div>
							<div class="col-md-6">
								<div id="wk_seller_state_div" class="col-md-6">
									<label for="id_state" class="control-label required">
										{l s='State' mod='marketplace'}
									</label>
									<select name="id_state" id="id_state" class="form-control form-control-select">
										<option value="">{l s='Select State' mod='marketplace'}</option>
									</select>
									<input type="hidden" name="state_available" id="state_available" value="0" />
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="seller_town" class="control-label required">
									{l s='Ville' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value=""
								name="seller_town"
								id="seller_town"
								maxlength="" />
								<span class="wk-msg-sellertown"></span>
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="seller_district" class="control-label required">
										{l s='Quartier' mod='marketplace'}
									</label>
									<input class="form-control"
									type="text"
									value=""
									name="seller_district"
									id="seller_district"
									maxlength="" />
									<span class="wk-msg-sellerdistrict"></span>
								</div>
								<div class="col-md-6">
									<label for="seller_postalcode" class="control-label required">
										{l s='Adresse postale' mod='marketplace'}
									</label>
									<input class="form-control"
									type="text"
									value=""
									name="seller_postalcode"
									id="seller_postalcode"
									maxlength="" />
									<span class="wk-msg-sellerpostalcode"></span>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<h3>{l s='Détails de livraison' mod='marketplace'}</h3>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="transporter" class="control-label required">
									{l s='Transporteur' mod='marketplace'}
								</label>
								<input class="form-control"
								type="text"
								value=""
								name="transporter"
								id="transporter"
								maxlength="" />
								<span class="wk-msg-transporter"></span>
							</div>
							<div class="col-md-6">
								<div class="col-md-6">
									<label for="delivery_way" class="control-label required">
										{l s='Voie' mod='marketplace'}
									</label>
									<select name="delivery_way" id="voie_livraison" class="form-control form-control-select">
										<option value="1">{l s='Maritime' mod='marketplace'}</option>
										<option value="2">{l s='Aerienne' mod='marketplace'}</option>
									</select>
									<span class="wk-msg-voielivraison"></span>
								</div>
								<div class="col-md-6">
									<label for="duration" class="control-label required">
										{l s='Durée' mod='marketplace'}
									</label>
									<input class="form-control"
									type="text"
									value=""
									name="duration"
									id="duration"
									placeholder="{l s='Ex: 2 à 4 jours' mod='marketplace'}" />
								</div>
							</div>
						</div>
						 <input type="button" name="previous" class="previous action-button-previous" value="Précédent"/>
                		<input type="button" name="next" class="next action-button" value="Suivant"/>
				</fieldset>
				<fieldset>
					<h2 class="fs-title">Conditions générales de vente</h2>
                		<h3 class="fs-subtitle">En savoir plus sur les conditions générales de vente</h3>
                		<p style="text-align:justify">Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semperInteger tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. ivamus elementum semper</p>
                		{if $terms_and_condition_active}
							<div class="form-group">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="terms_and_conditions" id="terms_and_conditions" style="width:10px;"/>
										<span>
											{l s='I agree to the' mod='marketplace'}
											{if isset($linkCmsPageContent)}
												<a href="{$linkCmsPageContent}" class="wk_terms_link">
													{l s='terms and condition' mod='marketplace'}
												</a>
											{else}
												{l s='terms and condition' mod='marketplace'}
											{/if}
											{l s='and will adhere to them unconditionally.' mod='marketplace'}
										</span>
									</label>
								</div>
							</div>
							{if isset($linkCmsPageContent)}
								<div class="modal fade" id="wk_terms_condtion_div" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div id="wk_terms_condtion_content" class="modal-body"></div>
										</div>
									</div>
								</div>
							{/if}
						{/if}
						<input type="button" name="previous" class="previous action-button-previous" value="Précédent" style="float:left;"/>
							<div data-action="{l s='Register' mod='marketplace'}" id="wk-seller-submit" style="width: 120px;float: left;">
							<img class="wk_product_loader" src="{$modules_dir}marketplace/views/img/loader.gif" width="25" />
							<input type="submit" name="sellerRequest" id="sellerRequest" class="action-button" value="Enregistrer"/>
							</div>
				</fieldset>
			</div>
		</form>
	</div>
{/if}
{/block}