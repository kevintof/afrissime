<?php

/**
 * PrestaShop module created by VEKIA, a guy from official PrestaShop community ;-)
 *
 * @author    VEKIA https://www.prestashop.com/forums/user/132608-vekia/
 * @copyright 2010-2019 VEKIA
 * @license   This program is not free software and you can't resell and redistribute it
 *
 * CONTACT WITH DEVELOPER http://mypresta.eu
 * support@mypresta.eu
 */
class freegeo extends Module
{
    public function __construct()
    {
        $this->name = 'freegeo';
        $this->tab = 'pricing_promotion';
        $this->author = 'MyPresta.eu';
        $this->mypresta_link = 'https://mypresta.eu/modules/front-office-features/currency-and-language-by-country.html';
        $this->version = '1.2.4';
        parent::__construct();
        $this->secure_key = Tools::encrypt($this->name);
        $this->bootstrap = true;
        $this->displayName = $this->l('Free geolocation currency + language');
        $this->description = $this->l('Module sets the currency + language of shop depending on visitor origin (by country)');
        $this->checkforupdates();
    }

    public function checkforupdates($display_msg = 0, $form = 0)
    {
        // ---------- //
        // ---------- //
        // VERSION 16 //
        // ---------- //
        // ---------- //
        $this->mkey = "nlc";
        if (@file_exists('../modules/' . $this->name . '/key.php')) {
            @require_once('../modules/' . $this->name . '/key.php');
        } else {
            if (@file_exists(dirname(__FILE__) . $this->name . '/key.php')) {
                @require_once(dirname(__FILE__) . $this->name . '/key.php');
            } else {
                if (@file_exists('modules/' . $this->name . '/key.php')) {
                    @require_once('modules/' . $this->name . '/key.php');
                }
            }
        }
        if ($form == 1) {
            return '
            <div class="panel" id="fieldset_myprestaupdates" style="margin-top:20px;">
            ' . ($this->psversion() == 6 || $this->psversion() == 7 ? '<div class="panel-heading"><i class="icon-wrench"></i> ' . $this->l('MyPresta updates') . '</div>' : '') . '
			<div class="form-wrapper" style="padding:0px!important;">
            <div id="module_block_settings">
                    <fieldset id="fieldset_module_block_settings">
                         ' . ($this->psversion() == 5 ? '<legend style="">' . $this->l('MyPresta updates') . '</legend>' : '') . '
                        <form action="' . $_SERVER['REQUEST_URI'] . '" method="post">
                            <label>' . $this->l('Check updates') . '</label>
                            <div class="margin-form">' . (Tools::isSubmit('submit_settings_updates_now') ? ($this->inconsistency(0) ? '' : '') . $this->checkforupdates(1) : '') . '
                                <button style="margin: 0px; top: -3px; position: relative;" type="submit" name="submit_settings_updates_now" class="button btn btn-default" />
                                <i class="process-icon-update"></i>
                                ' . $this->l('Check now') . '
                                </button>
                            </div>
                            <label>' . $this->l('Updates notifications') . '</label>
                            <div class="margin-form">
                                <select name="mypresta_updates">
                                    <option value="-">' . $this->l('-- select --') . '</option>
                                    <option value="1" ' . ((int)(Configuration::get('mypresta_updates') == 1) ? 'selected="selected"' : '') . '>' . $this->l('Enable') . '</option>
                                    <option value="0" ' . ((int)(Configuration::get('mypresta_updates') == 0) ? 'selected="selected"' : '') . '>' . $this->l('Disable') . '</option>
                                </select>
                                <p class="clear">' . $this->l('Turn this option on if you want to check MyPresta.eu for module updates automatically. This option will display notification about new versions of this addon.') . '</p>
                            </div>
                            <label>' . $this->l('Module page') . '</label>
                            <div class="margin-form">
                                <a style="font-size:14px;" href="' . $this->mypresta_link . '" target="_blank">' . $this->displayName . '</a>
                                <p class="clear">' . $this->l('This is direct link to official addon page, where you can read about changes in the module (changelog)') . '</p>
                            </div>
                            <div class="panel-footer">
                                <button type="submit" name="submit_settings_updates"class="button btn btn-default pull-right" />
                                <i class="process-icon-save"></i>
                                ' . $this->l('Save') . '
                                </button>
                            </div>
                        </form>
                    </fieldset>
                    <style>
                    #fieldset_myprestaupdates {
                        display:block;clear:both;
                        float:inherit!important;
                    }
                    </style>
                </div>
            </div>
            </div>';
        } else {
            if (defined('_PS_ADMIN_DIR_')) {
                if (Tools::isSubmit('submit_settings_updates')) {
                    Configuration::updateValue('mypresta_updates', Tools::getValue('mypresta_updates'));
                }
                if (Configuration::get('mypresta_updates') != 0 || (bool)Configuration::get('mypresta_updates') != false) {
                    if (Configuration::get('update_' . $this->name) < (date("U") - 259200)) {
                        $actual_version = freegeoUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version);
                    }
                    if (freegeoUpdate::version($this->version) < freegeoUpdate::version(Configuration::get('updatev_' . $this->name)) && Tools::getValue('ajax', 'false') == 'false') {
                        $this->context->controller->warnings[] = '<strong>' . $this->displayName . '</strong>: ' . $this->l('New version available, check http://MyPresta.eu for more informations') . ' <a href="' . $this->mypresta_link . '">' . $this->l('More details in changelog') . '</a>';
                        $this->warning = $this->context->controller->warnings[0];
                    }
                } else {
                    if (Configuration::get('update_' . $this->name) < (date("U") - 259200)) {
                        $actual_version = freegeoUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version);
                    }
                }
                if ($display_msg == 1) {
                    if (freegeoUpdate::version($this->version) < freegeoUpdate::version(freegeoUpdate::verify($this->name, (isset($this->mkey) ? $this->mkey : 'nokey'), $this->version))) {
                        return "<span style='color:red; font-weight:bold; font-size:16px; margin-right:10px;'>" . $this->l('New version available!') . "</span>";
                    } else {
                        return "<span style='color:green; font-weight:bold; font-size:16px; margin-right:10px;'>" . $this->l('Module is up to date!') . "</span>";
                    }
                }
            }
        }
    }

    public function inconsistency($ret)
    {
        return true;
    }

    public function install()
    {
        if (parent::install() == false ||
            $this->registerHook('actionAdminControllerSetMedia') == false ||
            $this->registerHook('actionFrontControllerAfterInit') == false ||
            $this->registerHook('actionDispatcherBefore') == false
        ) {
            return false;
        }
        return true;
    }

    public function hookactionAdminControllerSetMedia()
    {
        //HOOK FOR UPDATE NOTIFICATIONS PURPOSES
    }

    public function hookactionDispatcherBefore(){
        return $this->hookactionFrontControllerAfterInit();
    }

    public function hookactionFrontControllerAfterInit()
    {
        $FREEGEO_CURRENCY_ON = Configuration::get('FREEGEO_CURRENCY_ON');
        $FREEGEO_LANGUAGE_ON = Configuration::get('FREEGEO_LANGUAGE_ON');

        if ($this->checkFreeGeoCountry() == true) {
            if ($FREEGEO_CURRENCY_ON == true) {
                $freegeo_currency = Configuration::get('FREEGEO_CURRENCY');
                $freegeo_currency_instance = new Currency($freegeo_currency);
                $freegeo_currency_instance->id_currency = $freegeo_currency;
                $this->context->cookie->id_currency = $freegeo_currency;
                $this->context->currency = $freegeo_currency_instance;
            }

            if ($FREEGEO_LANGUAGE_ON == true) {
                $freegeo_language = Configuration::get('FREEGEO_LANGUAGE');
                $freegeo_language_instance = new Language($freegeo_language);
                $this->context->language = $freegeo_language_instance;
            }
        }
    }

    private function _postProcess()
    {
        if (Tools::isSubmit('btnSubmit')) {
            Configuration::updateValue('FREEGEO_COUNTRY', Tools::getValue('FREEGEO_COUNTRY'));
            Configuration::updateValue('FREEGEO_CURRENCY', Tools::getValue('FREEGEO_CURRENCY'));
            Configuration::updateValue('FREEGEO_CURRENCY_ON', Tools::getValue('FREEGEO_CURRENCY_ON'));
            Configuration::updateValue('FREEGEO_LANGUAGE', Tools::getValue('FREEGEO_LANGUAGE'));
            Configuration::updateValue('FREEGEO_LANGUAGE_ON', Tools::getValue('FREEGEO_LANGUAGE_ON'));
            return $this->displayConfirmation($this->l('Settings updated'));
        }
    }

    public static function checkFreeGeoCountry()
    {
        $geolocation = self::returnUserCountry();
        if ($geolocation != false) {
            $visitor_country = Country::getByIso($geolocation);
            if ($visitor_country != false) {
                if ($visitor_country == Configuration::get('FREEGEO_COUNTRY')) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function returnUserCountry()
    {
        $record = false;
        if (!in_array($_SERVER['SERVER_NAME'], array('localhost', '127.0.0.1'))) {
            /* Check if Maxmind Database exists */
            if (@filemtime(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_)) {
                $reader = new GeoIp2\Database\Reader(_PS_GEOIP_DIR_ . _PS_GEOIP_CITY_FILE_);
                try {
                    $record = $reader->city(Tools::getRemoteAddr());
                    //$record = $reader->city('31.60.47.127');
                } catch (\GeoIp2\Exception\AddressNotFoundException $e) {
                    $record = null;
                }

                if (isset($record->country->isoCode)) {
                    return $record->country->isoCode;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function displayForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-wrench'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $this->l('Select country'),
                        'name' => 'FREEGEO_COUNTRY',
                        'cast' => 'intval',
                        'options' => array(
                            'query' => Country::getCountries(Context::getContext()->language->id, false, false),
                            'id' => 'id_country',
                            'name' => 'name'
                        ),
                        'identifier' => 'value',
                    ),
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6') < 0) ? 'radio' : 'switch',
                        'class' => 't',
                        'label' => $this->l('Turn on language change by visitor\'s origin'),
                        'name' => 'FREEGEO_LANGUAGE_ON',
                        'values' => array(
                            array(
                                'id' => 'FREEGEO_LANGUAGE_ON_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'FREEGEO_LANGUAGE_ON_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Select language'),
                        'name' => 'FREEGEO_LANGUAGE',
                        'desc' => $this->l('This will be the only one available language for visitors from selected country'),
                        'cast' => 'intval',
                        'options' => array(
                            'query' => Language::getLanguages(true),
                            'id' => 'id_lang',
                            'name' => 'name'
                        ),
                        'identifier' => 'value',
                    ),
                    array(
                        'type' => (version_compare(_PS_VERSION_, '1.6') < 0) ? 'radio' : 'switch',
                        'class' => 't',
                        'label' => $this->l('Turn on currency change by visitor\'s origin'),
                        'name' => 'FREEGEO_CURRENCY_ON',
                        'values' => array(
                            array(
                                'id' => 'FREEGEO_CURRENCY_ON_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'FREEGEO_CURRENCY_ON_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Select Currency'),
                        'name' => 'FREEGEO_CURRENCY',
                        'desc' => $this->l('This will be the only one available currency for visitors from selected country'),
                        'cast' => 'intval',
                        'options' => array(
                            'query' => Currency::getCurrencies(),
                            'id' => 'id_currency',
                            'name' => 'name'
                        ),
                        'identifier' => 'value',
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                )
            ),
        );

        $helper = new HelperForm();
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->id = 'freegeoID';
        $helper->identifier = 'freegeo';
        $helper->submit_action = 'btnSubmit';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array($fields_form));

    }

    public function getConfigFieldsValues()
    {
        return array(
            'FREEGEO_COUNTRY' => Tools::getValue('FREEGEO_COUNTRY', Configuration::get('FREEGEO_COUNTRY')),
            'FREEGEO_LANGUAGE' => Tools::getValue('FREEGEO_LANGUAGE', Configuration::get('FREEGEO_LANGUAGE')),
            'FREEGEO_LANGUAGE_ON' => Tools::getValue('FREEGEO_LANGUAGE_ON', Configuration::get('FREEGEO_LANGUAGE_ON')),
            'FREEGEO_CURRENCY' => Tools::getValue('FREEGEO_CURRENCY', Configuration::get('FREEGEO_CURRENCY')),
            'FREEGEO_CURRENCY_ON' => Tools::getValue('FREEGEO_CURRENCY_ON', Configuration::get('FREEGEO_CURRENCY_ON')),
        );
    }

    public function advert()
    {
        return $this->context->smarty->fetch(_PS_MODULE_DIR_ . $this->name . '/views/advert.tpl');
    }

    public function getContent()
    {
        $this->_postProcess();
        $this->context->controller->warnings[] = $this->l('Module to identify customer country uses geolocation.') . ' ' . $this->l('In order to use Geolocation, please download') . ' ' . '<a href="http://geolite.maxmind.com/download/geoip/database/GeoLite2-City.mmdb.gz">' . $this->l('this file') . '</a> ' . $this->l('and extract it (using Winrar or Gzip) into the /app/Resources/geoip/ directory.');
        return $this->advert() . $this->displayForm() . $this->checkforupdates(0, true);
    }

    public function psversion($part = 1)
    {
        $version = _PS_VERSION_;
        $exp = $explode = explode(".", $version);
        if ($part == 1) {
            return $exp[1];
        }
        if ($part == 2) {
            return $exp[2];
        }
        if ($part == 3) {
            return $exp[3];
        }
    }
}

class freegeoUpdate extends freegeo
{
    public static function version($version)
    {
        $version = (int)str_replace(".", "", $version);
        if (strlen($version) == 3) {
            $version = (int)$version . "0";
        }
        if (strlen($version) == 2) {
            $version = (int)$version . "00";
        }
        if (strlen($version) == 1) {
            $version = (int)$version . "000";
        }
        if (strlen($version) == 0) {
            $version = (int)$version . "0000";
        }
        return (int)$version;
    }

    public static function encrypt($string)
    {
        return base64_encode($string);
    }

    public static function verify($module, $key, $version)
    {
        if (ini_get("allow_url_fopen")) {
            if (function_exists("file_get_contents")) {
                $actual_version = @file_get_contents('http://dev.mypresta.eu/update/get.php?module=' . $module . "&version=" . self::encrypt($version) . "&lic=$key&u=" . self::encrypt(_PS_BASE_URL_ . __PS_BASE_URI__));
            }
        }
        Configuration::updateValue("update_" . $module, date("U"));
        Configuration::updateValue("updatev_" . $module, $actual_version);
        return $actual_version;
    }
}

?>