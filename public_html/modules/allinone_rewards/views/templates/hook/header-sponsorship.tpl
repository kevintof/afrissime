{*
* All-in-one Rewards Module
*
* @category  Prestashop
* @category  Module
* @author    Yann BONNAILLIE - ByWEB
* @copyright 2012-2019 Yann BONNAILLIE - ByWEB (http://www.prestaplugins.com)
* @license   Commercial license see license.txt
* Support by mail  : contact@prestaplugins.com
* Support on forum : Patanock
* Support on Skype : Patanock13
*}
<!-- MODULE allinone_rewards -->
{if isset($ogtitle)}
<meta property="og:title" content="{$ogtitle|escape:'htmlall':'UTF-8'}" />
{/if}
{if isset($ogdescription)}
<meta property="og:description" content="{$ogdescription|escape:'htmlall':'UTF-8'}" />
{/if}
{if isset($ogimage)}
<meta property="og:image" content="{$ogimage|escape:'htmlall':'UTF-8'}" />
{/if}
<!-- END : MODULE allinone_rewards -->