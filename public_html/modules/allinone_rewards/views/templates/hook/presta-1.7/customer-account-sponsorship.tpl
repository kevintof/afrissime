{*
* All-in-one Rewards Module
*
* @category  Prestashop
* @category  Module
* @author    Yann BONNAILLIE - ByWEB
* @copyright 2012-2019 Yann BONNAILLIE - ByWEB (http://www.prestaplugins.com)
* @license   Commercial license see license.txt
* Support by mail  : contact@prestaplugins.com
* Support on forum : Patanock
* Support on Skype : Patanock13
*}
<!-- MODULE allinone_rewards -->
<a class="col-lg-4 col-md-6 col-sm-6 col-xs-12" id="sponsorship-link" href="{url entity='module' name='allinone_rewards' controller='sponsorship'}"><span class="link-item"><i class="material-icons">&#xE80D;</i>{l s='Sponsorship program' mod='allinone_rewards'}</span></a>
<!-- END : MODULE allinone_rewards -->