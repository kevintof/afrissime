{*
* All-in-one Rewards Module
*
* @category  Prestashop
* @category  Module
* @author    Yann BONNAILLIE - ByWEB
* @copyright 2012-2019 Yann BONNAILLIE - ByWEB (http://www.prestaplugins.com)
* @license   Commercial license see license.txt
* Support by mail  : contact@prestaplugins.com
* Support on forum : Patanock
* Support on Skype : Patanock13
*}
<!-- MODULE allinone_rewards -->
<div id="reward_facebook_block_column" class="reward_facebook_block block">
	<h6>{$facebook_block_title}</h6>
	<div class="block_content">
		{$facebook_block_txt nofilter}
		<div class="reward_facebook_block_button"><fb:like href="{$facebook_page}" show_faces="false" layout="button_count"></fb:like></div>
	</div>
</div>
<!-- END : MODULE allinone_rewards -->