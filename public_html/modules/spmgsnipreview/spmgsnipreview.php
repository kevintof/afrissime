<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class spmgsnipreview extends Module
{

    private $_data_translate_custom = array();


	private $_is15;
	private $_id_shop;

	private $_is16;
    private $_translate;
    private $_id_instagram_twitter_pinterest_module = 41665;
    private $_id_vk_module = 0;
    private $_is_cloud;
    private $path_img_cloud;
    private $_is_rtl;
    public $is_demo = 0;

    private $_prefix_review = "r";
    private $_prefix_shop_reviews = 'ti';

    ## store reviews ##
    private $_is_mobile = 0;
    private $_t_width = 245;
    private $_step = 10;
    ## store reviews ##

    private $_min_star_param = 0.2;
    private $_max_star_param = 0.8;

    private $_is_bug_product_page = 1;

    private $_template_name;
    private $_template_path = 'views/templates/hooks/';


    public function __construct()
 	{
 	 	$this->name = 'spmgsnipreview';
 	 	$this->version = '1.6.4';
 	 	$this->tab = 'seo';
 	 	$this->author = 'SPM';
		$this->module_key = 'e4188a7b143a5424fee5151658cc95a5';
		$this->confirmUninstall = $this->l('Are you sure you want to remove it ? Your will no longer work. Be careful, all your configuration and your data will be lost');

        $this->_is_rtl = 0;

        $this->bootstrap = true;
        $this->need_instance = 0;
        if(Context::getContext()->language->is_rtl){
            $this->_is_rtl = 1;
        }


		$this->_id_shop = Context::getContext()->shop->id;
		$this->_is15 = 1;

 		$this->_is16 = 1;

        ### store reviews ##
        if(version_compare(_PS_VERSION_, '1.5', '>') && version_compare(_PS_VERSION_, '1.7', '<')){

            require_once(_PS_TOOL_DIR_.'mobile_Detect/Mobile_Detect.php');
            $mobile_detect = new Mobile_Detect();

            //echo "<pre>"; var_dump($this->mobile_detect);exit;
            if ($mobile_detect->isMobile()){
                $this->_is_mobile = 1;
            }

        }

        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $mobile_detect = new \Mobile_Detect();
            if ($mobile_detect->isMobile()) {
                $this->_is_mobile = 1;
            }
        }
        //$this->_is_mobile = 1;
        if($this->_is_mobile){
            $this->_t_width = 205;
        }
        ### store reviews ##


        if (defined('_PS_HOST_MODE_'))
            $this->_is_cloud = 1;
        else
            $this->_is_cloud = 0;

        // for test
        //$this->_is_cloud = 1;
        // for test

        if($this->_is_cloud){
            $this->path_img_cloud = "modules/".$this->name."/upload/";
        } else {
            $this->path_img_cloud = "upload/".$this->name."/";
        }

        $this->_translate = array(
            'name'=>$this->name,
            'title'=>$this->l('Automatically posts Integration'),
            'title_pstwitterpost'=>$this->l('Automatically Social Wall Posts'),
            'title_psvkpost'=>$this->l('Vkontakte Wall Post'),

            'buy_module_pstwitterpost'=>$this->l('This feature requires you to have purchased, installed and correctly configured our Automatically Social Wall Posts module.')
                .' '
                .$this->l('You may purchase it on:')
                .' '
                .$this->_linkAHtml(array('title'=>$this->l('PrestaShop Addons'),'href'=>'http://addons.prestashop.com/product.php?id_product='.$this->_id_instagram_twitter_pinterest_module))
                .' '
                .$this->l('Once this is all set, you will see the configuration options instead of this red text.'),

            'buy_module_psvkpost'=>$this->l('This feature requires you to have purchased, installed and correctly configured our Vkontakte Wall Post module.')
                .' '
                .$this->l('You may purchase it on:')
                .$this->_linkAHtml(array('title'=>$this->l('PrestaShop Addons'),'href'=>'http://addons.prestashop.com/product.php?id_product='.$this->_id_vk_module))
                .$this->l('Once this is all set, you will see the configuration options instead of this red text.'),

            'hint1'=>$this->l('This section lets you integrate with our')
                .' '.$this->_linkAHtml(array('title'=>$this->l('Automatically Social Wall Posts module'),'href'=>'http://addons.prestashop.com/product.php?id_product='.$this->_id_instagram_twitter_pinterest_module,'is_b'=>1))
                /*
                .$this->l('and')
                .' '.$this->_linkAHtml(array('title'=>$this->l('Vkontakte Wall Post module'),'href'=>'http://addons.prestashop.com/product.php?id_product='.$this->_id_vk_module,'is_b'=>1))
                */
                .$this->l(', and allows you to have any ratings and comments posted on a product on your PrestaShop website to be also automatically posted to your')
                .' '
                //.$this->l(' Facebook, Twitter, Vkontakte fan pages').'.'.' ',
                .$this->l(' Twitter, Pinterest fan pages').'.'.' ',

            'hint2'=>$this->l('If you have enabled Require Admin Approval for the "Reviews", it will only be posted once you approve the rating and comment in the moderation interface.'),

            'update_button'=>$this->l('Update settings'),
            'form_action'=>Tools::safeOutput($_SERVER['REQUEST_URI']),

            'enable_psvkpost'=> $this->l('Enable Vkontakte Wall Post'),

            'enable_pstwitterpost_main'=> $this->l('Enable Automatically Social Wall Posts'),

            'enable_pstwitterpost'=> $this->l('Enable Twitter Wall Posts'),
            'enable_pstwitterpost_i'=> $this->l('Enable Instagram Wall Posts'),
            'enable_pstwitterpost_p'=> $this->l('Enable Pinterest Wall Posts'),

            'template_text'=>$this->l('Post template text'),

        );
		
		parent::__construct();
		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Product, Shop Reviews, Reminder, Profile, Rich Snippets');
		$this->description = $this->l('Product, Shop Reviews, Reminder, Profile, Rich Snippets');


        if(version_compare(_PS_VERSION_, '1.7.4', '>') && version_compare(_PS_VERSION_, '1.7.4.2', '<')) {
            $this->_template_name = '';
        } else {
            $this->_template_name = $this->_template_path;
        }

		
		$this->initContext();

        ## prestashop 1.7 ##
        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            require_once(_PS_MODULE_DIR_.$this->name.'/classes/ps17helpspmgsnipreview.class.php');
            $ps17help = new ps17helpspmgsnipreview();
            $ps17help->setMissedVariables();
        } else {
            $smarty = $this->context->smarty;
            $smarty->assign($this->name.'is17' , 0);
        }
        ## prestashop 1.7 ##




	}

    public function getIdModule(){
        return $this->id;
    }

    public function getokencron(){
        $_token_cron_shop = sha1(_COOKIE_KEY_ . $this->name);
        return $_token_cron_shop;
    }

    public function getPrefixProductReviews(){
        return $this->_prefix_review;
    }

    public function getPrefixShopReviews(){
        return $this->_prefix_shop_reviews;
    }

    public function getURLMultiShop(){
            $_http_host = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;
            $current_shop_id = Shop::getContextShopID();

            if($current_shop_id) {

                $is_ssl = false;
                if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (bool)Configuration::get('PS_SSL_ENABLED'))
                    $is_ssl = true;

                $shop_obj = new Shop($current_shop_id);

                $_http_host = $shop_obj->getBaseURL($is_ssl);

            }

        return $_http_host;
    }
 	
	private function initContext()
	{
	  $this->context = Context::getContext();
	  $this->context->currentindex = isset(AdminController::$currentIndex)?AdminController::$currentIndex:'index.php?controller=AdminModules';
	  $this->context->done = isset($this->done)?$this->done:null;



      /*$contextLanguageCode = $this->context->language->language_code;
        if(!empty($contextLanguageCode)) {
            $explodeLanguageCode = explode('-', $contextLanguageCode);
            $first_locate = isset($explodeLanguageCode[0])?$explodeLanguageCode[0]:'';
            $second_locate = isset($explodeLanguageCode[1])?$explodeLanguageCode[1]:'';
            if(Tools::strlen($first_locate)>0 && Tools::strlen($second_locate)>0) {
                $localeOfContextLanguage = $first_locate . '_' . Tools::strtoupper($second_locate);
                setlocale(LC_ALL, $localeOfContextLanguage . '.UTF-8', $localeOfContextLanguage);
                //setlocale(LC_ALL, $localeOfContextLanguage);
            }
        }*/
    }
	
 	public function install()
	{
        // users sliders
        Configuration::updateValue($this->name.'sr_slideru', 1);
        Configuration::updateValue($this->name.'sr_slu', 3);

        Configuration::updateValue($this->name.'sr_sliderhu', 1);
        Configuration::updateValue($this->name.'sr_slhu', 3);

        Configuration::updateValue($this->name.'d_eff_shopu', 'flipInX');

        Configuration::updateValue($this->name.'perpageu'.$this->_prefix_review, 5);

        // users sliders

        // product reviews sliders
        Configuration::updateValue($this->name.'sr_sliderr', 1);
        Configuration::updateValue($this->name.'sr_slr', 2);

        Configuration::updateValue($this->name.'sr_sliderhr', 1);
        Configuration::updateValue($this->name.'sr_slhr', 2);

        Configuration::updateValue($this->name.'d_eff_rev', 'flipInX');
        Configuration::updateValue($this->name.'d_eff_rev_my', 'flipInX');
        Configuration::updateValue($this->name.'d_eff_rev_u', 'flipInX');
        Configuration::updateValue($this->name.'d_eff_rev_all', 'flipInX');

        // store reviews sliders
        Configuration::updateValue($this->name.'sr_slider', 1);
        Configuration::updateValue($this->name.'sr_sl', 2);

        Configuration::updateValue($this->name.'sr_sliderh', 1);
        Configuration::updateValue($this->name.'sr_slh', 2);

        // owl effects store reviews
        Configuration::updateValue($this->name.'d_eff_shop'.$this->_prefix_shop_reviews, 'flipInX');
        Configuration::updateValue($this->name.'d_eff_shop_my'.$this->_prefix_shop_reviews, 'flipInX');
        Configuration::updateValue($this->name.'d_eff_shop_u'.$this->_prefix_shop_reviews, 'flipInX');

        ## ids_groups ##

        $cookie = $this->context->cookie;
        $id_lang =  $cookie->id_lang;

        $ids_groups_array = array();
        foreach(Group::getGroups($id_lang) as $group_item){
            $id_group = $group_item['id_group'];
            $ids_groups_array[] = $id_group;
        }
        $ids_groups = implode(",",$ids_groups_array);

        Configuration::updateValue($this->name.'ids_groups', $ids_groups);
        Configuration::updateValue($this->name.'ids_groupsfb', $ids_groups);
        Configuration::updateValue($this->name.'ids_groups'.$this->_prefix_shop_reviews, $ids_groups);

        ## ids_groups ##

        // voucher settings - store reviews

        Configuration::updateValue($this->name.'vis_on'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'is_show_min'.$this->_prefix_shop_reviews, 1);

        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];
            $iso = Tools::strtoupper(Language::getIsoById($i));

            $coupondesc = $this->displayName;
            Configuration::updateValue($this->name.'coupondesc'.$this->_prefix_shop_reviews.'_'.$i, $coupondesc.' '.$iso);
        }

        Configuration::updateValue($this->name.'vouchercode'.$this->_prefix_shop_reviews, "PRS");
        Configuration::updateValue($this->name.'discount_type'.$this->_prefix_shop_reviews, 2);
        Configuration::updateValue($this->name.'percentage_val'.$this->_prefix_shop_reviews, 1);

        Configuration::updateValue($this->name.'tax'.$this->_prefix_shop_reviews, 1);

        $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
        foreach ($cur AS $_cur){
            if(Configuration::get('PS_CURRENCY_DEFAULT') == $_cur['id_currency']){
                Configuration::updateValue('sdamount'.$this->_prefix_shop_reviews.'_'.(int)$_cur['id_currency'], 1);
            }
        }
        Configuration::updateValue($this->name.'sdvvalid'.$this->_prefix_shop_reviews, 365);

        // cumulable
        Configuration::updateValue($this->name.'cumulativeother'.$this->_prefix_shop_reviews, 0);
        Configuration::updateValue($this->name.'cumulativereduc'.$this->_prefix_shop_reviews, 0);
        // cumulable

        Configuration::updateValue($this->name.'highlight'.$this->_prefix_shop_reviews, 0);
        // categories
        Configuration::updateValue($this->name.'catbox'.$this->_prefix_shop_reviews, $this->getIdsCategories());
        // categories
        // voucher settings - store reviews

        ### store reviews ####

        Configuration::updateValue($this->name.'is_storerev', 1);

        Configuration::updateValue($this->name.'crondelay'.$this->_prefix_shop_reviews, 10);
        Configuration::updateValue($this->name.'cronnpost'.$this->_prefix_shop_reviews, 20);

        Configuration::updateValue($this->name.'is_files'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'ruploadfiles'.$this->_prefix_shop_reviews, 7);

        Configuration::updateValue($this->name.'t_lefts', 1);
        Configuration::updateValue($this->name.'t_rights', 1);
        Configuration::updateValue($this->name.'t_footers', 1);
        Configuration::updateValue($this->name.'t_homes', 1);
        Configuration::updateValue($this->name.'t_leftsides', 1);
        Configuration::updateValue($this->name.'t_rightsides', 1);
        Configuration::updateValue($this->name.'t_tpages', 1);


        Configuration::updateValue($this->name.'mt_left', 1);
        Configuration::updateValue($this->name.'mt_right', 1);
        Configuration::updateValue($this->name.'mt_footer', 1);
        Configuration::updateValue($this->name.'mt_home', 1);
        Configuration::updateValue($this->name.'mt_leftside', 1);
        Configuration::updateValue($this->name.'mt_rightside', 1);

        Configuration::updateValue($this->name.'st_left', 1);
        Configuration::updateValue($this->name.'st_right', 1);
        Configuration::updateValue($this->name.'st_footer', 1);
        Configuration::updateValue($this->name.'st_home', 1);
        Configuration::updateValue($this->name.'st_leftside', 1);
        Configuration::updateValue($this->name.'st_rightside', 1);

        ### reminder ###
        Configuration::updateValue($this->name.'delaysec'.$this->_prefix_shop_reviews, 7);
        Configuration::updateValue($this->name.'remindersec'.$this->_prefix_shop_reviews, 0);


        Configuration::updateValue($this->name.'reminder'.$this->_prefix_shop_reviews, 1);
        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];
            Configuration::updateValue($this->name.'emrem'.$this->_prefix_shop_reviews.'_'.$i, $this->l('Are you satisfied with our products'));
            Configuration::updateValue($this->name.'reminderok'.$this->_prefix_shop_reviews.'_'.$i, $this->l('The emails requests on the reviews was successfully sent'));
            Configuration::updateValue($this->name.'thankyou'.$this->_prefix_shop_reviews.'_'.$i, $this->l('Thank you for your review'));
            Configuration::updateValue($this->name.'newtest'.$this->_prefix_shop_reviews.'_'.$i, $this->l('New Store review from Your Customer'));
            Configuration::updateValue($this->name.'resptest'.$this->_prefix_shop_reviews.'_'.$i, $this->l('Response on the Store review'));

            Configuration::updateValue($this->name.'revvoucr'.$this->_prefix_shop_reviews.'_'.$i, $this->l('You submit a review and get voucher for discount'));

        }

        Configuration::updateValue($this->name.'is_emrem'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'is_reminderok'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'is_thankyou'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'is_newtest'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'is_resptest'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'is_revvoucr'.$this->_prefix_shop_reviews, 1);

        Configuration::updateValue($this->name.'orderstatuses'.$this->_prefix_shop_reviews, implode(",",array(2,5,12)));
        Configuration::updateValue($this->name.'starscat'.$this->_prefix_shop_reviews, 1);

        Configuration::updateValue($this->name.'delay'.$this->_prefix_shop_reviews, 7);


        ### reminder ###

        Configuration::updateValue($this->name.'whocanadd'.$this->_prefix_shop_reviews, 'all');

        Configuration::updateValue($this->name.'tlast_l', 3);
        Configuration::updateValue($this->name.'tlast_ls', 3);
        Configuration::updateValue($this->name.'tlast_r', 3);
        Configuration::updateValue($this->name.'tlast_rs', 3);
        Configuration::updateValue($this->name.'tlast_f', 3);
        Configuration::updateValue($this->name.'tlast_h', 3);

        Configuration::updateValue($this->name.'t_home', 1);
        Configuration::updateValue($this->name.'t_footer', 1);
        Configuration::updateValue($this->name.'BGCOLOR_T', '#fafafa');
        Configuration::updateValue($this->name.'BGCOLOR_TIT', '#c45500');
        Configuration::updateValue($this->name.'t_left', 1);

        Configuration::updateValue($this->name.'t_rightside', 1);

        Configuration::updateValue($this->name.'perpage'.$this->_prefix_shop_reviews, 5);
        Configuration::updateValue($this->name.'perpagemy'.$this->_prefix_shop_reviews, 5);
        Configuration::updateValue($this->name.'perpageu'.$this->_prefix_shop_reviews, 5);


        Configuration::updateValue($this->name.'is_avatar', 1);
        Configuration::updateValue($this->name.'is_captcha'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'is_web', 1);
        Configuration::updateValue($this->name.'is_company', 1);
        Configuration::updateValue($this->name.'is_addr', 1);

        Configuration::updateValue($this->name.'is_country', 1);
        Configuration::updateValue($this->name.'is_city', 1);

        Configuration::updateValue($this->name.'rswitch_lng'.$this->_prefix_shop_reviews, 0);

        Configuration::updateValue($this->name.'is_filterall'.$this->_prefix_shop_reviews, 1);

        Configuration::updateValue($this->name.'is_sortf'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'is_sortfu'.$this->_prefix_shop_reviews, 1);

        Configuration::updateValue($this->name.'noti'.$this->_prefix_shop_reviews, 1);
        Configuration::updateValue($this->name.'mail'.$this->_prefix_shop_reviews, @Configuration::get('PS_SHOP_EMAIL'));

        Configuration::updateValue($this->name.'n_rssitemst', 10);
        Configuration::updateValue($this->name.'rssontestim', 1);

        $this->createAdminTabsStoreReviews();
        ### store reviews ####

        Configuration::updateValue($this->name.'crondelay'.$this->_prefix_review, 10);
        Configuration::updateValue($this->name.'cronnpost'.$this->_prefix_review, 20);

        ## subjects ##
        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];
            Configuration::updateValue($this->name.'reminderok'.$this->_prefix_review.'_'.$i, $this->l('The emails requests on the reviews was successfully sent'));
            Configuration::updateValue($this->name.'thankyou'.$this->_prefix_review.'_'.$i, $this->l('Thank you for your review'));

            Configuration::updateValue($this->name.'emailreminder_'.$i, $this->l('Are you satisfied with our products'));
            Configuration::updateValue($this->name.'subpubem_'.$i, $this->l('Your review has been published'));
            Configuration::updateValue($this->name.'subresem_'.$i, $this->l('The shop admin has replied to your product review'));
            Configuration::updateValue($this->name.'textresem_'.$i, $this->l('Thank you for your product review on our website. We always welcome reviews, whether it is positive or negative. However, we would like to have a chance to invite you to change your review. Here is why:'));

            Configuration::updateValue($this->name.'newrev'.$this->_prefix_review.'_'.$i, $this->l('New review'));
            Configuration::updateValue($this->name.'modrev'.$this->_prefix_review.'_'.$i, $this->l('One of your customers has modified own product review'));
            Configuration::updateValue($this->name.'abuserev'.$this->_prefix_review.'_'.$i, $this->l('Someone send abuse for review'));

            Configuration::updateValue($this->name.'facvouc'.$this->_prefix_review.'_'.$i, $this->l('You share review on Facebook and get voucher for discount'));
            Configuration::updateValue($this->name.'revvouc'.$this->_prefix_review.'_'.$i, $this->l('You submit a review and get voucher for discount'));


            Configuration::updateValue($this->name.'sugvouc'.$this->_prefix_review.'_'.$i, $this->l('Share your review on Facebook and get voucher for discount'));


        }
        ## subjects ##

        Configuration::updateValue($this->name.'is_emailreminder', 1);
        Configuration::updateValue($this->name.'is_reminderok'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'is_thankyou'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'is_newrev'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'is_subresem', 1);
        Configuration::updateValue($this->name.'is_modrev'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'is_subpubem', 1);
        Configuration::updateValue($this->name.'is_abuserev'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'is_revvouc'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'is_facvouc'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'is_sugvouc'.$this->_prefix_review, 1);

        ## user profile ###
        Configuration::updateValue($this->name.'is_uprof', 1);

        Configuration::updateValue($this->name.'is_proftab', 1);

        if($this->_is16 == 1){
            Configuration::updateValue($this->name.'radv_home', 1);
            Configuration::updateValue($this->name.'radv_footer', 1);

        }
        Configuration::updateValue($this->name.'radv_left', 1);

        Configuration::updateValue($this->name.'rshoppers_blc', 5);
        Configuration::updateValue($this->name.'rpage_shoppers', 16);
        ## user profile ###

        Configuration::updateValue($this->name.'is_avatar'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'is_files'.$this->_prefix_review, 1);
        Configuration::updateValue($this->name.'ruploadfiles', 7);
        Configuration::updateValue($this->name.'rminc', 20);


        Configuration::updateValue($this->name.'is_onerev', 1);
        Configuration::updateValue($this->name.'is_sortallf', 1);
        Configuration::updateValue($this->name.'is_sortfu', 1);

		#### posts api ###
		$languages = Language::getLanguages(false);
    	foreach ($languages as $language){
    		$i = $language['id_lang'];
    		$iso = Tools::strtoupper(Language::getIsoById($i));
    		Configuration::updateValue($this->name.'twdesc_'.$i, $this->l('rated product in our shop').' '.$iso);
            Configuration::updateValue($this->name.'idesc_'.$i, $this->l('rated product in our shop').' '.$iso);
            Configuration::updateValue($this->name.'pdesc_'.$i, $this->l('rated product in our shop').' '.$iso);
            Configuration::updateValue($this->name.'vkdesc_'.$i, $this->l('rated product in our shop').' '.$iso);
		}
		#### posts api ###

		// pinterest
		Configuration::updateValue($this->name.'pinvis_on', 1);
		Configuration::updateValue($this->name.'pinterestbuttons', 'threeon');
        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            Configuration::updateValue($this->name . '_productActions', 'productActions');
        } else {
            Configuration::updateValue($this->name . '_extraLeft', 'extraLeft');
        }
		// pinterest
		
		// google rich snippets
		Configuration::updateValue($this->name.'svis_on', 1);
        Configuration::updateValue($this->name.'allinfo_on', 1);

        Configuration::updateValue($this->name.'allinfo_home', 'allinfo_home');
        Configuration::updateValue($this->name.'allinfo_cat', 'allinfo_cat');
        Configuration::updateValue($this->name.'allinfo_man', 'allinfo_man');

        Configuration::updateValue($this->name.'allinfo_home_w', 100);
        Configuration::updateValue($this->name.'allinfo_cat_w', 100);
        Configuration::updateValue($this->name.'allinfo_man_w', 100);

        Configuration::updateValue($this->name.'allinfo_home_pos', 'top');
        Configuration::updateValue($this->name.'allinfo_cat_pos', 'top');
        Configuration::updateValue($this->name.'allinfo_man_pos', 'top');

		// google rich snippets

        Configuration::updateValue($this->name.'reminder', 1);
        Configuration::updateValue($this->name.'delaysec'.$this->_prefix_review, 7);
        Configuration::updateValue($this->name.'remindersec'.$this->_prefix_review, 0);

		
		Configuration::updateValue($this->name.'delay', 7);
        // orderstatuses
        Configuration::updateValue($this->name.'orderstatuses', implode(",",array(2,5,12)));
        // orderstatuses

        Configuration::updateValue($this->name.'breadvis_on', 1);

		// product reviews advanced 
		Configuration::updateValue($this->name.'rvis_on', 1);
        Configuration::updateValue($this->name.'ratings_on', 1);
		Configuration::updateValue($this->name.'text_on', 1);
		Configuration::updateValue($this->name.'title_on', 1);
		Configuration::updateValue($this->name.'ip_on', 1);
		Configuration::updateValue($this->name.'is_captcha', 1);

        Configuration::updateValue($this->name.'is_filterp', 1);
        Configuration::updateValue($this->name.'is_filterall', 1);

        Configuration::updateValue($this->name.'ptabs_type', 1);

        Configuration::updateValue($this->name.'is_abusef', 1);
        Configuration::updateValue($this->name.'is_helpfulf', 1);
        Configuration::updateValue($this->name.'is_sortf', 1);

        Configuration::updateValue($this->name.'rsoc_on', 1);
        Configuration::updateValue($this->name.'rsoccount_on', 1);

        Configuration::updateValue($this->name.'revperpagecus', 5);

        Configuration::updateValue($this->name.'is_blocklr', 1);

        Configuration::updateValue($this->name.'blocklr_home_pos', 'home');
        Configuration::updateValue($this->name.'blocklr_cat_pos', 'leftcol');
        Configuration::updateValue($this->name.'blocklr_man_pos', 'leftcol');
        Configuration::updateValue($this->name.'blocklr_prod_pos', 'leftcol');
        Configuration::updateValue($this->name.'blocklr_oth_pos', 'leftcol');
        Configuration::updateValue($this->name.'blocklr_chook_pos', 'chook');

        Configuration::updateValue($this->name.'blocklr_home_w', 100);
        Configuration::updateValue($this->name.'blocklr_cat_w', 100);
        Configuration::updateValue($this->name.'blocklr_man_w', 100);
        Configuration::updateValue($this->name.'blocklr_prod_w', 100);
        Configuration::updateValue($this->name.'blocklr_oth_w', 100);
        Configuration::updateValue($this->name.'blocklr_chook_w', 100);


        Configuration::updateValue($this->name.'blocklr_home', 'blocklr_home');
        Configuration::updateValue($this->name.'blocklr_cat', 'blocklr_cat');
        Configuration::updateValue($this->name.'blocklr_man', 'blocklr_man');
        Configuration::updateValue($this->name.'blocklr_prod', 'blocklr_prod');
        Configuration::updateValue($this->name.'blocklr_oth', 'blocklr_oth');
        Configuration::updateValue($this->name.'blocklr_chook', 'blocklr_chook');

        Configuration::updateValue($this->name.'blocklr_home_ndr', 3);
        Configuration::updateValue($this->name.'blocklr_cat_ndr', 3);
        Configuration::updateValue($this->name.'blocklr_man_ndr', 3);
        Configuration::updateValue($this->name.'blocklr_prod_ndr', 3);
        Configuration::updateValue($this->name.'blocklr_oth_ndr', 3);
        Configuration::updateValue($this->name.'blocklr_chook_ndr', 3);

        Configuration::updateValue($this->name.'blocklr_home_tr', 250);
        Configuration::updateValue($this->name.'blocklr_cat_tr', 75);
        Configuration::updateValue($this->name.'blocklr_man_tr', 75);
        Configuration::updateValue($this->name.'blocklr_prod_tr', 75);
        Configuration::updateValue($this->name.'blocklr_oth_tr', 75);
        Configuration::updateValue($this->name.'blocklr_chook_tr', 75);

        $img_default = "small"."_"."default";
        Configuration::updateValue($this->name . 'blocklr_home_im', $img_default);
        Configuration::updateValue($this->name . 'blocklr_cat_im', $img_default);
        Configuration::updateValue($this->name . 'blocklr_man_im', $img_default);
        Configuration::updateValue($this->name . 'blocklr_prod_im', $img_default);
        Configuration::updateValue($this->name . 'blocklr_oth_im', $img_default);
        Configuration::updateValue($this->name . 'blocklr_chook_im', $img_default);


        Configuration::updateValue($this->name.'img_size_em', $img_default);


        Configuration::updateValue($this->name.'rswitch_lng', 0);


		Configuration::updateValue($this->name.'revperpage', 5);
		Configuration::updateValue($this->name.'revperpageall', 10);

		Configuration::updateValue($this->name.'whocanadd', 'all');
		Configuration::updateValue($this->name.'is_approval', 0);
		Configuration::updateValue($this->name.'position', 'left');
		//Configuration::updateValue($this->name.'homeon', 1);
        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            Configuration::updateValue($this->name . 'hooktodisplay', 'product_actions');
        } else {
            Configuration::updateValue($this->name . 'hooktodisplay', 'extra_right');
        }
		Configuration::updateValue($this->name.'stylestars', 'style1'); // yellow

        Configuration::updateValue($this->name.'starratingon', 1);
		Configuration::updateValue($this->name.'noti', 1);	
		Configuration::updateValue($this->name.'mail', Configuration::get('PS_SHOP_EMAIL'));
		Configuration::updateValue($this->name.'lastrevitems', 5);

		
		Configuration::updateValue($this->name.'starscat', 1);
        Configuration::updateValue($this->name.'is_starscat', 0);
		// product reviews advanced
		
		// voucher settings

        Configuration::updateValue($this->name.'vis_on', 1);

        Configuration::updateValue($this->name.'is_show_min', 1);


        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];
            $iso = Tools::strtoupper(Language::getIsoById($i));

            $coupondesc = $this->displayName;
            Configuration::updateValue($this->name.'coupondesc_'.$i, $coupondesc.' '.$iso);
        }

        Configuration::updateValue($this->name.'vouchercode', "PRG");
        Configuration::updateValue($this->name.'discount_type', 2);
        Configuration::updateValue($this->name.'percentage_val', 1);

        Configuration::updateValue($this->name.'tax', 1);


        $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
        foreach ($cur AS $_cur){
            if(Configuration::get('PS_CURRENCY_DEFAULT') == $_cur['id_currency']){
                Configuration::updateValue('sdamount_'.(int)$_cur['id_currency'], 1);
            }
        }
        Configuration::updateValue($this->name.'sdvvalid', 365);

        // cumulable
        Configuration::updateValue($this->name.'cumulativeother', 0);
        Configuration::updateValue($this->name.'cumulativereduc', 0);
        // cumulable

        Configuration::updateValue($this->name.'highlight', 0);
        // categories
        Configuration::updateValue($this->name.'catbox', $this->getIdsCategories());
        // categories
        // voucher settings

        // voucher facebook settings

        $this->installVoucherShareReviewSettings();

        // voucher facebook settings

		Configuration::updateValue($this->name.'rsson', 1);
		Configuration::updateValue($this->name.'number_rssitems', 10);

		$languages = Language::getLanguages(false);
    	foreach ($languages as $language){
    		$i = $language['id_lang'];

    		$rssname = Configuration::get('PS_SHOP_NAME');
    		Configuration::updateValue($this->name.'rssname_'.$i, $rssname);
			$rssdesc = Configuration::get('PS_SHOP_NAME');
			Configuration::updateValue($this->name.'rssdesc_'.$i, $rssdesc);
		}
		
		$this->createAdminTabs15();

		if (!parent::install())
	 		return false;
	 	
	 	if (!$this->installTable() OR
            !$this->installCriteriaTable() OR
            !$this->installReviewCriteria() OR
            !$this->installReviewAbuse() OR
            !$this->installReviewHelpfull() OR
            !$this->installSocialShare() OR
            !$this->installReminder2CustomerTable() OR

            !$this->installUserTable() OR
            !$this->installFiles2ReviewTable() OR

            !$this->createShopReviewTable() OR
            !$this->createReminderShopReviewsTable() OR
            !$this->installCriteriaTableShopReviews() OR
            !$this->installReviewCriteria(array('is_shop_reviews'=>1)) OR

            !$this->installFiles2ShopReviewTable() OR
            !$this->installReminder2CustomerShopReviewTable() OR

            !$this->registerHook('leftColumn') OR
			!$this->registerHook('rightColumn') OR
			!$this->registerHook('header') OR 
			!$this->registerHook('ProductTab') OR
			!$this->registerHook('productTabContent') OR
			!$this->registerHook('footer') OR
			!$this->registerHook('extraLeft') OR
		 	!$this->registerHook('extraRight') OR
		 	!$this->registerHook('productfooter') OR

		 	//!$this->registerHook('productActions') OR
            !((version_compare(_PS_VERSION_, '1.7', '>'))? $this->registerHook('displayProductButtons') : $this->registerHook('productActions')) OR

		 	!$this->registerHook('home') OR
            !$this->registerHook('top') OR
		 	!$this->registerHook('customerAccount') OR
	 		!$this->registerHook('myAccountBlock') OR
	 		!$this->registerHook('OrderConfirmation') OR
            !$this->registerHook('displayProductListReviews') OR
            !$this->registerHook('lastReviewsSPM') OR
	 		!$this->registerHook('actionValidateOrder') OR
	 		!($this->_is_cloud? true : $this->createFolderAndSetPermissions()) OR

            !($this->_is_cloud? true : $this->createFolderAndSetPermissionsAvatar()) OR
            !($this->_is_cloud? true : $this->createFolderAndSetPermissionsFiles()) OR

            !($this->_is_cloud? true : $this->createFolderAndSetPermissionsFilesShopReviews()) OR

	 		!((version_compare(_PS_VERSION_, '1.6', '>'))? $this->registerHook('DisplayBackOfficeHeader') : true)

            OR !$this->registerHook('backOfficeTop')

            //GDPR
            || !$this->registerHook('registerGDPRConsent')
            || !$this->registerHook('actionDeleteGDPRCustomer')
            || !$this->registerHook('actionExportGDPRData')
            //GDPR



        )
			return false;
	 	
	 	return true;
	}

    public function getPrefixReviews(){
        return $this->_prefix_review;
    }

    public function hookActionDeleteGDPRCustomer ($customer)
    {
        if (!empty($customer['email']) && Validate::isEmail($customer['email'])) {

            include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
            $obj_storereviews = new storereviews();
            $return = $obj_storereviews->deleteGDPRCustomerData($customer['email']);


            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
            $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
            $return_spmgsnipreviewhelp = $obj_spmgsnipreviewhelp->deleteGDPRCustomerData($customer['email']);


            if ($return && $return_spmgsnipreviewhelp) {
                return json_encode(true);
            }
            return json_encode($this->displayName . ' : ' .$this->l('Unable to delete customer using email.'));
        }
    }


    public function hookActionExportGDPRData ($customer)
    {
        if (!Tools::isEmpty($customer['email']) && Validate::isEmail($customer['email'])) {

            $return = array();
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
            $obj_storereviews = new storereviews();
            $return_storereviews = $obj_storereviews->getGDPRCustomerData($customer['email']);
            if(count($return_storereviews)>0)
                $return[0] = $return_storereviews;

            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
            $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
            $return_spmgsnipreviewhelp = $obj_spmgsnipreviewhelp->getGDPRCustomerData($customer['email']);
            if(count($return_spmgsnipreviewhelp)>0)
                $return[1] = $return_spmgsnipreviewhelp;



            if (count($return)>0) {
                return json_encode($return);
            }
            return json_encode($this->displayName . ' : ' .$this->l('Unable to export customer using email.'));
        }
    }

    public function hookBackOfficeTop($params)
    {
        $name_template = "header_info.tpl";

        $this->context->controller->addCSS(($this->_path).'views/css/admin_header.css', 'all');

        $smarty = $this->context->smarty;
        $_name_controller_store = 'AdminSpmgsnipreviewstorereviews';
        $red_url_store = 'index.php?controller='.$_name_controller_store.'&token='.Tools::getAdminTokenLite($_name_controller_store);

        $_name_controller = 'AdminSpmgsnipreviewreviews';
        $red_url = 'index.php?controller='.$_name_controller.'&token='.Tools::getAdminTokenLite($_name_controller);

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();

        $count_reviews_store = $obj_storereviews->getCountNewReviews();
        $smarty->assign($this->name.'count_new_reviews_store', $count_reviews_store);



        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $count_reviews = $obj_spmgsnipreviewhelp->getCountNewReviews();
        $smarty->assign($this->name.'count_new_reviews', $count_reviews);



        $base_dir = Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__;
        $smarty->assign(
            array(
                $this->name.'red_url' => $red_url,
                $this->name.'red_url_store' => $red_url_store,
                $this->name.'base_dir'=>$base_dir,

            )
        );


        return $this->display(__FILE__, $this->_template_path.$name_template);

    }


	public function hookDisplayBackOfficeHeader()
	{
        $this->context->controller->addCSS($this->_path.'views/css/tab.css');
	}
	
	public function installVoucherShareReviewSettings(){
        // voucher facebook settings

        Configuration::updateValue($this->name.'vis_onfb', 1);

        Configuration::updateValue($this->name.'is_show_minfb', 1);


        $languages = Language::getLanguages(false);
        foreach ($languages as $language){
            $i = $language['id_lang'];
            $iso = Tools::strtoupper(Language::getIsoById($i));

            $coupondesc = $this->displayName;
            Configuration::updateValue($this->name.'coupondescfb_'.$i, $coupondesc.' '.$iso);
        }

        Configuration::updateValue($this->name.'vouchercodefb', "PRF");
        Configuration::updateValue($this->name.'discount_typefb', 2);
        Configuration::updateValue($this->name.'percentage_valfb', 1);

        Configuration::updateValue($this->name.'taxfb', 1);


        $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
        foreach ($cur AS $_cur){
            if(Configuration::get('PS_CURRENCY_DEFAULT') == $_cur['id_currency']){
                Configuration::updateValue('sdamountfb_'.(int)$_cur['id_currency'], 1);
            }
        }
        Configuration::updateValue($this->name.'sdvvalidfb', 365);

        // cumulable
        Configuration::updateValue($this->name.'cumulativeotherfb', 0);
        Configuration::updateValue($this->name.'cumulativereducfb', 0);
        // cumulable

        Configuration::updateValue($this->name.'highlightfb', 0);
        // categories
        Configuration::updateValue($this->name.'catboxfb', $this->getIdsCategories());
        // categories
        // voucher facebook settings
    }
	
	
	public function uninstall()
	{
        $this->uninstallTab15();

        $this->uninstallTabStoreReviews();

		if (!parent::uninstall()
			|| !$this->uninstallTable()
			)
			return false;
		return true;
	}

    public function uninstallTab15(){

            $prefix = '';

		$tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewreview".$prefix);
        if($tab_id){
			$tab = new Tab($tab_id);

			$tab->delete();
		}


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewreviewtest");
            if($tab_id){
                $tab = new Tab($tab_id);
                $tab->delete();
            }
        }


		$tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewreviews".$prefix);
		if($tab_id){
			$tab = new Tab($tab_id);
			$tab->delete();
		}
		
		@unlink(_PS_ROOT_DIR_."/img/t/AdminSpmgsnipreviewreview".$prefix.".gif");
	}


    public function getTabNameByLangISO(){
        return array(

            'AdminSpmgsnipreviewreview16' => array(
                'fr' => 'Produit Avis',
                'en' => 'Product Reviews',
                'es' => 'Producto Opiniones',
                'it' => 'Prodotto Recensioni',
                'nl' => 'Product beoordelingen',
                'pt' => 'Product Opiniões',
                'ru' => 'Отзывы к продукту',
                'bg' => 'Отзиви за продукта',
                'br' => 'Product Opiniões',
                'de' => 'Produkt bewertungen',
            ),

            'AdminSpmgsnipreviewreview17' => array(
                'fr' => 'Avis',
                'en' => 'Reviews',
                'es' => 'Opiniones',
                'it' => 'Recensioni',
                'nl' => 'Beoordelingen',
                'pt' => 'Opiniões',
                'ru' => 'Отзывы',
                'bg' => 'Отзиви',
                'br' => 'Opiniões',
                'de' => 'Bewertungen',
            ),


            'AdminSpmgsnipreviewreviewtest' => array(
                'fr' => 'Avis',
                'en' => 'Reviews',
                'es' => 'Opiniones',
                'it' => 'Recensioni',
                'nl' => 'Beoordelingen',
                'pt' => 'Opiniões',
                'ru' => 'Отзывы',
                'bg' => 'Отзиви',
                'br' => 'Opiniões',
                'de' => 'Bewertungen',
            ),

            'AdminSpmgsnipreviewreviews' => array(
                'fr' => 'Modéré Produit Avis',
                'en' => 'Moderate Product Reviews',
                'es' => 'Moderados Producto Opiniones',
                'it' => 'Moderate Producto Recensioni',
                'nl' => 'Beheer Product Beoordelingen',
                'pt' => 'Moderados Product Opiniões',
                'ru' => 'Модерировать отзывы к продукту',
                'bg' => 'Модерирайте отзивите за продукти',
                'br' => 'Moderados Product Opiniões',
                'de' => 'Moderate Produkt bewertungen',
            ),

            'AdminSpmgsnipreviewstorereview' => array(
                'fr' => 'Magasin Avis',
                'en' => 'Store Reviews',
                'es' => 'Store Opiniones',
                'it' => 'Negozio Recensioni',
                'nl' => 'Winkel Recensies',
                'pt' => 'Loja Opiniões',
                'ru' => 'Отзывы к магазину',
                'bg' => 'Отзиви на магазини',
                'br' => 'Loja Opiniões',
                'de' => 'Store Bewertungen',
            ),

            'AdminSpmgsnipreviewstorereviews' => array(
                'fr' => 'Modéré Magasin Avis',
                'en' => 'Moderate Store Reviews',
                'es' => 'Moderados Store Opiniones',
                'it' => 'Moderate Negozio Recensioni',
                'nl' => 'Beheer Winkel Recensies',
                'pt' => 'Moderados Loja Opiniões',
                'ru' => 'Модерировать отзывы к магазину',
                'bg' => 'Модерирайте отзиви за магазините',
                'br' => 'Moderados Loja Opiniões',
                'de' => 'Moderate Store Bewertungen',
            ),



        );
    }
	
	public function createAdminTabs15(){

            $aTabNameByLang = $this->getTabNameByLangISO();

            $prefix = '';
    		@copy(dirname(__FILE__)."/views/img/AdminSpmgsnipreviewreview".$prefix.".gif",_PS_ROOT_DIR_."/img/t/AdminSpmgsnipreviewreview".$prefix.".gif");
		
		 	$langs = Language::getLanguages();
            
            $tab0 = new Tab();
            $tab0->class_name = "AdminSpmgsnipreviewreview".$prefix;
            $tab0->module = $this->name;
            $tab0->id_parent = 0; 
            foreach($langs as $l){

                    if((version_compare(_PS_VERSION_, '1.7', '<'))) {
                        $tab0->name[$l['id_lang']] = isset($aTabNameByLang["AdminSpmgsnipreviewreview16"][$l['iso_code']])?$aTabNameByLang["AdminSpmgsnipreviewreview16"][$l['iso_code']]:$aTabNameByLang["AdminSpmgsnipreviewreview16"]['en'];
                    } else {
                        $tab0->name[$l['id_lang']] = isset($aTabNameByLang["AdminSpmgsnipreviewreview17"][$l['iso_code']])?$aTabNameByLang["AdminSpmgsnipreviewreview17"][$l['iso_code']]:$aTabNameByLang["AdminSpmgsnipreviewreview17"]['en'];
                    }
            }
            $tab0->save();
            $main_tab_id = $tab0->id;

            unset($tab0);


            if(version_compare(_PS_VERSION_, '1.7', '>')) {
                // create subtab //

                $tab0 = new Tab();
                $tab0->class_name = "AdminSpmgsnipreviewreviewtest";
                $tab0->module = $this->name;
                $tab0->id_parent = $main_tab_id;
                foreach ($langs as $l) {
                    $tab0->name[$l['id_lang']] = isset($aTabNameByLang["AdminSpmgsnipreviewreviewtest"][$l['iso_code']])?$aTabNameByLang["AdminSpmgsnipreviewreviewtest"][$l['iso_code']]:$aTabNameByLang["AdminSpmgsnipreviewreviewtest"]['en'];
                }
                $tab0->save();
                $main_tab_id = $tab0->id;

                unset($tab0);
                // create subtab //
            }

            $tab1 = new Tab();
            $tab1->class_name = "AdminSpmgsnipreviewreviews".$prefix;
            $tab1->module = $this->name;
            $tab1->id_parent = $main_tab_id; 
            foreach($langs as $l){
                    $tab1->name[$l['id_lang']] = isset($aTabNameByLang["AdminSpmgsnipreviewreviews"][$l['iso_code']])?$aTabNameByLang["AdminSpmgsnipreviewreviews"][$l['iso_code']]:$aTabNameByLang["AdminSpmgsnipreviewreviews"]['en'];
            }
            $tab1->save();

            unset($tab1);


            if(version_compare(_PS_VERSION_, '1.7', '>')) {
                $tab1 = new Tab();
                $tab1->class_name = "AdminSpmgsnipreviewstorereviews" . $prefix;
                $tab1->module = $this->name;
                $tab1->id_parent = $main_tab_id;
                foreach ($langs as $l) {
                    $tab1->name[$l['id_lang']] = isset($aTabNameByLang["AdminSpmgsnipreviewstorereviews"][$l['iso_code']])?$aTabNameByLang["AdminSpmgsnipreviewstorereviews"][$l['iso_code']]:$aTabNameByLang["AdminSpmgsnipreviewstorereviews"]['en'];
                }
                $tab1->save();

                unset($tab1);
            }
	}

    public function createAdminTabsStoreReviews(){

        $aTabNameByLang = $this->getTabNameByLangISO();


        if(version_compare(_PS_VERSION_, '1.7', '<')) {
            $prefix = '';

            Tools::copy(dirname(__FILE__) . "/views/img/AdminSpmgsnipreviewstorereview" . $prefix . ".gif", _PS_ROOT_DIR_ . "/img/t/AdminSpmgsnipreviewstorereview" . $prefix . ".gif");

            $langs = Language::getLanguages();


            $tab0 = new Tab();
            $tab0->class_name = "AdminSpmgsnipreviewstorereview" . $prefix;
            $tab0->module = $this->name;
            $tab0->id_parent = 0;
            foreach ($langs as $l) {
                $tab0->name[$l['id_lang']] = isset($aTabNameByLang["AdminSpmgsnipreviewstorereview"][$l['iso_code']])?$aTabNameByLang["AdminSpmgsnipreviewstorereview"][$l['iso_code']]:$aTabNameByLang["AdminSpmgsnipreviewstorereview"]['en'];
            }
            $tab0->save();
            $main_tab_id = $tab0->id;

            unset($tab0);

            $tab1 = new Tab();
            $tab1->class_name = "AdminSpmgsnipreviewstorereviews" . $prefix;
            $tab1->module = $this->name;
            $tab1->id_parent = $main_tab_id;
            foreach ($langs as $l) {
                $tab1->name[$l['id_lang']] = isset($aTabNameByLang["AdminSpmgsnipreviewstorereviews"][$l['iso_code']])?$aTabNameByLang["AdminSpmgsnipreviewstorereviews"][$l['iso_code']]:$aTabNameByLang["AdminSpmgsnipreviewstorereviews"]['en'];
            }
            $tab1->save();

            unset($tab1);
        }


    }

    private function uninstallTabStoreReviews(){

        $prefix = '';

        if(version_compare(_PS_VERSION_, '1.7', '<')) {

            $tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewstorereview" . $prefix);
            if ($tab_id) {
                $tab = new Tab($tab_id);
                $tab->delete();
            }

        }
        $tab_id = Tab::getIdFromClassName("AdminSpmgsnipreviewstorereviews".$prefix);
        if($tab_id){
            $tab = new Tab($tab_id);
            $tab->delete();
        }

        @unlink(_PS_ROOT_DIR_."/img/t/AdminSpmgsnipreviewstorereview".$prefix.".gif");
    }

	private function uninstallTable() {
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        $obj_spmgsnipreviewdraw->uninstallTable();

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();
        $obj_storereviews->uninstallTable();

        return true;
    }

    public function installReviewCriteria($data = null){

        $is_shop_reviews = isset($data['is_shop_reviews'])?$data['is_shop_reviews']:null;

        $languages = Language::getLanguages(false);
        $data_content_lang = array();

        $shops = array();
        foreach(Shop::getShops() as $shop){
            $shops[] = $shop['id_shop'];
        }
        $cat_shop_association = $shops;


        foreach ($languages as $language){
            $id_lang = $language['id_lang'];
            $description = '';
            $name = $this->l('Total Rating');

                $data_content_lang[$id_lang] = array( 'description' => $description,
                                                        'name' => $name
                );
        }


        $data = array(
            'active' => 1,
            'data_content_lang'=>$data_content_lang,
            'cat_shop_association' => $cat_shop_association
        );


        if($is_shop_reviews){
            include_once(_PS_MODULE_DIR_.$this->name . '/classes/storereviews.class.php');
            $obj = new storereviews();
        } else {
            include_once(_PS_MODULE_DIR_.$this->name . '/classes/spmgsnipreviewhelp.class.php');
            $obj = new spmgsnipreviewhelp();
        }
        $obj->saveReviewCriteriaItem($data);

       return true;
    }
	
	public function installTable()
	{
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        return $obj_spmgsnipreviewdraw->installTable();
	}

    public function installSocialShare(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        return $obj_spmgsnipreviewdraw->installSocialShare();
    }

    public function installReviewHelpfull(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        return $obj_spmgsnipreviewdraw->installReviewHelpfull();
    }

    public function installReviewAbuse(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        return $obj_spmgsnipreviewdraw->installReviewAbuse();
    }

    public function installCriteriaTable(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        return $obj_spmgsnipreviewdraw->installCriteriaTable();
    }

    public function installReminder2CustomerTable(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        return $obj_spmgsnipreviewdraw->installReminder2CustomerTable();
    }

    public function installUserTable(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        return $obj_spmgsnipreviewdraw->installUserTable();
    }

    public function installFiles2ReviewTable(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();
        return $obj_spmgsnipreviewdraw->installFiles2ReviewTable();
    }

    public function installFiles2ShopReviewTable(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();
        return $obj_storereviews->installFiles2ShopReviewTable();
    }

    public function installReminder2CustomerShopReviewTable(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();
        return $obj_storereviews->installReminder2CustomerShopReviewTable();
    }

    public function createShopReviewTable()
    {
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();
        return $obj_storereviews->createShopReviewTable();
    }

    public function createReminderShopReviewsTable(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();
        return $obj_storereviews->createReminderShopReviewsTable();
    }

    public function installCriteriaTableShopReviews(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();
        return $obj_storereviews->installCriteriaTableShopReviews();
    }

    public function createFolderAndSetPermissions(){

        $prev_cwd = getcwd();

        $module_dir = dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;
        @chdir($module_dir);


        if(Module::isInstalled('gsnipreview')){

            //folder items
            $module_dir_img_old = $module_dir . "gsnipreview";
            $module_dir_img_new = $module_dir . $this->name;

            @rename($module_dir_img_old, $module_dir_img_new);


        } else {

            //folder items
            $module_dir_img = $module_dir . $this->name . DIRECTORY_SEPARATOR;
            @mkdir($module_dir_img, 0777);


        }

        @chdir($prev_cwd);

        return true;
    }

    public function createFolderAndSetPermissionsAvatar(){

        $prev_cwd = getcwd();

        $module_dir = dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;
        @chdir($module_dir);


        $module_dir_img = $module_dir.$this->name.DIRECTORY_SEPARATOR;


        if(Module::isInstalled('gsnipreview')){

            //folder items
            $module_dir_img_old = $module_dir . "gsnipreview";
            $module_dir_img_new = $module_dir . $this->name;

            @rename($module_dir_img_old, $module_dir_img_new);


        } else {

            //folder avatars
            @mkdir($module_dir_img, 0777);

        }

        @chdir($module_dir_img);

        $module_dir_img_avatar = $module_dir.$this->name.DIRECTORY_SEPARATOR."avatar".DIRECTORY_SEPARATOR;
        @mkdir($module_dir_img_avatar, 0777);

        @chdir($prev_cwd);

        return true;
    }


    public function createFolderAndSetPermissionsFiles(){

        $prev_cwd = getcwd();

        $module_dir = dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;
        @chdir($module_dir);

        $module_dir_img = $module_dir.$this->name.DIRECTORY_SEPARATOR;


        if(Module::isInstalled('gsnipreview')){

            //folder items
            $module_dir_img_old = $module_dir . "gsnipreview";
            $module_dir_img_new = $module_dir . $this->name;

            @rename($module_dir_img_old, $module_dir_img_new);


        } else {

            //folder avatars
            @mkdir($module_dir_img, 0777);

        }

        @chdir($module_dir_img);

        $module_dir_img_files_tmp = $module_dir.$this->name.DIRECTORY_SEPARATOR."tmp".DIRECTORY_SEPARATOR;
        @mkdir($module_dir_img_files_tmp, 0777);

        $module_dir_img_files = $module_dir.$this->name.DIRECTORY_SEPARATOR."files".DIRECTORY_SEPARATOR;
        @mkdir($module_dir_img_files, 0777);

        @chdir($prev_cwd);

        return true;
    }

    public function createFolderAndSetPermissionsFilesShopReviews(){

        $prev_cwd = getcwd();

        $module_dir = dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;
        @chdir($module_dir);


        $module_dir_img = $module_dir.$this->name.DIRECTORY_SEPARATOR;


        if(Module::isInstalled('gsnipreview')){

            //folder items
            $module_dir_img_old = $module_dir . "gsnipreview";
            $module_dir_img_new = $module_dir . $this->name;

            @rename($module_dir_img_old, $module_dir_img_new);


        } else {

            //folder avatars
            @mkdir($module_dir_img, 0777);

        }

        @chdir($module_dir_img);




        $module_dir_img_files_tmp = $module_dir.$this->name.DIRECTORY_SEPARATOR."tmpshopreviews".DIRECTORY_SEPARATOR;
        @mkdir($module_dir_img_files_tmp, 0777);

        $module_dir_img_files = $module_dir.$this->name.DIRECTORY_SEPARATOR."filesshopreviews".DIRECTORY_SEPARATOR;
        @mkdir($module_dir_img_files, 0777);

        @chdir($prev_cwd);

        return true;
    }

    public function getContent()
    {
    	$cookie = $this->context->cookie;
		
    	$currentIndex = $this->context->currentindex;
    	 
    	$_html = '';
        $errors = array();

        $this->addBackOfficeMedia();

        ## product reviews owl sliders and WOW effects ###

        $woweffectssettingsset = Tools::getValue("woweffectssettingsset");
        if (Tools::strlen($woweffectssettingsset)>0) {

            ob_start();
            $number_tab = 58;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('woweffectssettings'))
        {

            Configuration::updateValue($this->name.'d_eff_rev', Tools::getValue('d_eff_rev'));
            Configuration::updateValue($this->name.'d_eff_rev_my', Tools::getValue('d_eff_rev_my'));
            Configuration::updateValue($this->name.'d_eff_rev_u', Tools::getValue('d_eff_rev_u'));
            Configuration::updateValue($this->name.'d_eff_rev_all', Tools::getValue('d_eff_rev_all'));

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //
            $url = $currentIndex . '&conf=6&tab=AdminModules&woweffectssettingsset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
            Tools::redirectAdmin($url);
        }

        $owlcarouselssettingsset = Tools::getValue("owlcarouselssettingsset");
        if (Tools::strlen($owlcarouselssettingsset)>0) {

            ob_start();
            $number_tab = 57;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('owlcarouselssettings'))
        {

            Configuration::updateValue($this->name.'sr_sliderr', Tools::getValue('sr_sliderr'));
            if(!ctype_digit(Tools::getValue('sr_slr')) || Tools::getValue('sr_slr') == NULL) {
                $errors[] = $this->l('Displayed number of items in the slider in the block Last Reviews block in the Left, Right, Footer positions').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'sr_slr', Tools::getValue('sr_slr'));
            }

            Configuration::updateValue($this->name.'sr_sliderhr', Tools::getValue('sr_sliderhr'));
            if(!ctype_digit(Tools::getValue('sr_slhr')) || Tools::getValue('sr_slhr') == NULL) {
                $errors[] = $this->l('Displayed number of items in the slider in the block Last Reviews block on the Home Page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'sr_slhr', Tools::getValue('sr_slhr'));
            }


            if(count($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&owlcarouselssettingsset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {

                ob_start();
                $number_tab = 57;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();

            }

        }

        ## product reviews owl sliders and WOW effects ###

        ### store reviews ###

        $owlcarouselsshopsettingsset = Tools::getValue("owlcarouselsshopsettingsset");
        if (Tools::strlen($owlcarouselsshopsettingsset)>0) {

            ob_start();
            $number_tab = 81;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('owlcarouselsshopsettings'))
        {

            Configuration::updateValue($this->name.'sr_slider', Tools::getValue('sr_slider'));
            if(!ctype_digit(Tools::getValue('sr_sl')) || Tools::getValue('sr_sl') == NULL) {
                $errors[] = $this->l('Displayed number of items in the slider in the block "Store Reviews" widget in the Left, Right, Footer positions').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'sr_sl', Tools::getValue('sr_sl'));
            }

            Configuration::updateValue($this->name.'sr_sliderh', Tools::getValue('sr_sliderh'));
            if(!ctype_digit(Tools::getValue('sr_slh')) || Tools::getValue('sr_slh') == NULL) {
                $errors[] = $this->l('Displayed number of items in the slider in the block "Store Reviews" widget on the Home Page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'sr_slh', Tools::getValue('sr_slh'));
            }


            if(!ctype_digit(Tools::getValue('tlast_l')) || Tools::getValue('tlast_l') == NULL) {
                $errors[] = $this->l('The number of items in the "Store reviews Block" in the Left Column').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'tlast_l', Tools::getValue('tlast_l'));
            }

            if(!ctype_digit(Tools::getValue('tlast_ls')) || Tools::getValue('tlast_ls') == NULL) {
                $errors[] = $this->l('The number of items in the "Store reviews Block" in the Left Side').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'tlast_ls', Tools::getValue('tlast_ls'));
            }

            if(!ctype_digit(Tools::getValue('tlast_r')) || Tools::getValue('tlast_r') == NULL) {
                $errors[] = $this->l('The number of items in the "Store reviews Block" in the Right Column').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'tlast_r', Tools::getValue('tlast_r'));
            }

            if(!ctype_digit(Tools::getValue('tlast_rs')) || Tools::getValue('tlast_rs') == NULL) {
                $errors[] = $this->l('The number of items in the "Store reviews Block" in the Right Side').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'tlast_rs', Tools::getValue('tlast_rs'));
            }

            if(!ctype_digit(Tools::getValue('tlast_f')) || Tools::getValue('tlast_f') == NULL) {
                $errors[] = $this->l('The number of items in the "Store reviews Block" in the Footer').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'tlast_f', Tools::getValue('tlast_f'));
            }

            if(!ctype_digit(Tools::getValue('tlast_h')) || Tools::getValue('tlast_h') == NULL) {
                $errors[] = $this->l('The number of items in the "Store reviews Block" on the home page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'tlast_h', Tools::getValue('tlast_h'));
            }

            if(Tools::strlen(Tools::getValue($this->name.'BGCOLOR_T')) != 7 || Tools::getValue($this->name.'BGCOLOR_T') == NULL) {
                $errors[] = $this->l('Store reviews title color. Only in the positions: Left Side and Right Side').' '.$this->l('is incorrect!');
            } else {
                Configuration::updateValue($this->name.'BGCOLOR_T', Tools::getValue($this->name.'BGCOLOR_T'));
            }

            if(Tools::strlen(Tools::getValue($this->name.'BGCOLOR_TIT')) != 7 || Tools::getValue($this->name.'BGCOLOR_TIT') == NULL) {
                $errors[] = $this->l('Store reviews block background color').' '.$this->l('is incorrect!');
            } else {
                Configuration::updateValue($this->name . 'BGCOLOR_TIT', Tools::getValue($this->name . 'BGCOLOR_TIT'));
            }

            Configuration::updateValue($this->name.'t_homes', Tools::getValue('t_homes'));
            Configuration::updateValue($this->name.'t_lefts', Tools::getValue('t_lefts'));
            Configuration::updateValue($this->name.'t_rights', Tools::getValue('t_rights'));
            Configuration::updateValue($this->name.'t_footers', Tools::getValue('t_footers'));
            Configuration::updateValue($this->name.'t_rightsides', Tools::getValue('t_rightsides'));
            Configuration::updateValue($this->name.'t_leftsides', Tools::getValue('t_leftsides'));
            Configuration::updateValue($this->name.'t_tpages', Tools::getValue('t_tpages'));

            Configuration::updateValue($this->name.'st_left', Tools::getValue('st_left'));
            Configuration::updateValue($this->name.'st_right', Tools::getValue('st_right'));
            Configuration::updateValue($this->name.'st_footer', Tools::getValue('st_footer'));
            Configuration::updateValue($this->name.'st_home', Tools::getValue('st_home'));
            Configuration::updateValue($this->name.'st_leftside', Tools::getValue('st_leftside'));
            Configuration::updateValue($this->name.'st_rightside', Tools::getValue('st_rightside'));

            Configuration::updateValue($this->name.'mt_left', Tools::getValue('mt_left'));
            Configuration::updateValue($this->name.'mt_right', Tools::getValue('mt_right'));
            Configuration::updateValue($this->name.'mt_footer', Tools::getValue('mt_footer'));
            Configuration::updateValue($this->name.'mt_home', Tools::getValue('mt_home'));
            Configuration::updateValue($this->name.'mt_leftside', Tools::getValue('mt_leftside'));
            Configuration::updateValue($this->name.'mt_rightside', Tools::getValue('mt_rightside'));

            Configuration::updateValue($this->name.'t_home', Tools::getValue('t_home'));
            Configuration::updateValue($this->name.'t_left', Tools::getValue('t_left'));
            Configuration::updateValue($this->name.'t_right', Tools::getValue('t_right'));
            Configuration::updateValue($this->name.'t_footer', Tools::getValue('t_footer'));
            Configuration::updateValue($this->name.'t_rightside', Tools::getValue('t_rightside'));
            Configuration::updateValue($this->name.'t_leftside', Tools::getValue('t_leftside'));

            if(count($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&owlcarouselsshopsettingsset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {

                ob_start();
                $number_tab = 81;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();

            }

        }

        $woweffectsshopsettingsset = Tools::getValue("woweffectsshopsettingsset");
        if (Tools::strlen($woweffectsshopsettingsset)>0) {

            ob_start();
            $number_tab = 80;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('woweffectsshopsettings'))
        {
            Configuration::updateValue($this->name.'d_eff_shop'.$this->_prefix_shop_reviews, Tools::getValue('d_eff_shop'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'d_eff_shop_my'.$this->_prefix_shop_reviews, Tools::getValue('d_eff_shop_my'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'d_eff_shop_u'.$this->_prefix_shop_reviews, Tools::getValue('d_eff_shop_u'.$this->_prefix_shop_reviews));

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //
            $url = $currentIndex . '&conf=6&tab=AdminModules&woweffectsshopsettingsset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
            Tools::redirectAdmin($url);
        }

        ### customeraccountreviewspage settings ###
        if (Tools::isSubmit("spmgsnipreviewcriteriaset".$this->_prefix_shop_reviews) && !$this->_is15)
        {

            $url = $currentIndex.'&tab=AdminModules&revcriteriasettings'.$this->_prefix_shop_reviews.'=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);
        }

        $revcriteriasettings = Tools::getValue("revcriteriasettings".$this->_prefix_shop_reviews);

        $addCriteria = Tools::isSubmit("addCriteria".$this->_prefix_shop_reviews);
        $editspmgsnipreview = Tools::isSubmit('editspmgsnipreview'.$this->_prefix_shop_reviews);
        $delete_itemspmgsnipreview = Tools::isSubmit("delete_itemspmgsnipreview".$this->_prefix_shop_reviews);

        if (Tools::strlen($revcriteriasettings)>0
            || $addCriteria || $editspmgsnipreview || $delete_itemspmgsnipreview) {
            ob_start();
            $number_tab = 79;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit("spmgsnipreviewcriteriaset".$this->_prefix_shop_reviews)) {
            ob_start();
            $number_tab = 79;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit("delete_itemspmgsnipreview".$this->_prefix_shop_reviews)) {
            if (Validate::isInt(Tools::getValue("id"))) {
                $data = array('id' => Tools::getValue("id"));
                include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
                $obj_storereviews = new storereviews();
                $obj_storereviews->deleteReviewCriteriaItem($data);

                // clear cache //
                $this->clearSmartyCacheItems(array('is_shop_reviews'=>1));
                // clear cache //

                $url = $currentIndex.'&conf=1&tab=AdminModules&revcriteriasettings'.$this->_prefix_shop_reviews.'=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
                Tools::redirectAdmin($url);
            }
        }

        if (Tools::isSubmit('editcriteriasettings'.$this->_prefix_shop_reviews))
        {
            $id = Tools::getValue("id");

            $cat_shop_association = Tools::getValue("cat_shop_association");

            $languages = Language::getLanguages(false);
            $data_content_lang = array();

            $data_content_lang_name = array();

            foreach ($languages as $language){
                $id_lang = $language['id_lang'];
                $description = Tools::getValue("description_".$id_lang);
                $name = Tools::getValue("name_".$id_lang);
                if(Tools::strlen($name)>0)
                {
                    $data_content_lang[$id_lang] = array('description' => $description,
                        'name' => $name);
                    $data_content_lang_name[$id_lang] = array('name' => $name);
                }
            }

            $active = Tools::getValue("active");

            $data = array('active' => $active,
                'data_content_lang'=>$data_content_lang,
                'cat_shop_association' => $cat_shop_association,
                'id' => $id
            );
            if(sizeof($data_content_lang_name)>0 && $cat_shop_association){
                include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
                $obj_storereviews = new storereviews();
                $obj_storereviews->updateReviewCriteriaItem($data);

            } else {
                $error = 2;
                $errors[] = $this->l('Criterion name or Shop association is empty!');
                //$_html .= $this->_error(array('text' => 'Criterion name or Description is empty!'));
                $this->_criterion_error_ti = 1;

            }

            if($error == 0){

                // clear cache //
                $this->clearSmartyCacheItems(array('is_shop_reviews'=>1));
                // clear cache //

                $url = $currentIndex.'&conf=4&tab=AdminModules&revcriteriasettings'.$this->_prefix_shop_reviews.'=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
                Tools::redirectAdmin($url);
            }
        }

        //echo "<pre>"; var_dump($_POST);var_dumP(Tools::isSubmit('addcriteriasettings'));exit;
        if (Tools::isSubmit('addcriteriasettings'.$this->_prefix_shop_reviews))
        {
            $languages = Language::getLanguages(false);
            $data_content_lang = array();
            $data_content_lang_name = array();

            $cat_shop_association = Tools::getValue("cat_shop_association");

            foreach ($languages as $language){
                $id_lang = $language['id_lang'];
                $description = Tools::getValue("description_".$id_lang);
                $name = Tools::getValue("name_".$id_lang);
                if(Tools::strlen($name)>0)
                {
                    $data_content_lang[$id_lang] = array( 'description' => $description,
                        'name' => $name
                    );
                    $data_content_lang_name[$id_lang] = array('name' => $name);
                }
            }

            $active = Tools::getValue("active");

            $data = array(
                'active' => $active,
                'data_content_lang'=>$data_content_lang,
                'cat_shop_association' => $cat_shop_association
            );

            if(sizeof($data_content_lang_name)>0 && $cat_shop_association){

                include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
                $obj_storereviews = new storereviews();
                $obj_storereviews->saveReviewCriteriaItem($data);

            } else {

                $error = 2;
                $errors[] = $this->l('Criterion name or Shop association is empty!');
                $this->_criterion_error_ti = 1;

            }

            if($error == 0){

                // clear cache //
                $this->clearSmartyCacheItems(array('is_shop_reviews'=>1));
                // clear cache //

                $url = $currentIndex.'&conf=3&tab=AdminModules&revcriteriasettings'.$this->_prefix_shop_reviews.'=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
                Tools::redirectAdmin($url);
            }
        }
        ### addcriteriasettings settings ###

        ### customerremindersettings settings ###
        $shopcustomerreminderset = Tools::getValue("shopcustomerreminderset");
        if (Tools::strlen($shopcustomerreminderset)>0) {
            ob_start();
            $number_tab = 74;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('shopcustomerremindersettings'))
        {
            if(!ctype_digit(Tools::getValue('crondelay'.$this->_prefix_shop_reviews)) || Tools::getValue('crondelay'.$this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Delay between each email in seconds').' '.$this->l('must be digit!');
            } else{
                Configuration::updateValue($this->name.'crondelay'.$this->_prefix_shop_reviews, Tools::getValue('crondelay'.$this->_prefix_shop_reviews));
            }

            if(!ctype_digit(Tools::getValue('cronnpost' . $this->_prefix_shop_reviews)) || Tools::getValue('cronnpost' . $this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Number of emails for each cron call').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'cronnpost' . $this->_prefix_shop_reviews, Tools::getValue('cronnpost' . $this->_prefix_shop_reviews));
            }


            if(!ctype_digit(Tools::getValue('delaysec'.$this->_prefix_shop_reviews)) || Tools::getValue('delaysec'.$this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Days after the first emails were sent').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'delaysec'.$this->_prefix_shop_reviews, Tools::getValue('delaysec'.$this->_prefix_shop_reviews));
            }
            Configuration::updateValue($this->name.'remindersec'.$this->_prefix_shop_reviews, Tools::getValue('remindersec'.$this->_prefix_shop_reviews));


            Configuration::updateValue($this->name.'remrevsec'.$this->_prefix_shop_reviews, Tools::getValue('remrevsec'.$this->_prefix_shop_reviews));


            if(!ctype_digit(Tools::getValue('delay'.$this->_prefix_shop_reviews)) || Tools::getValue('delay'.$this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Delay for sending reminder by email').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'delay'.$this->_prefix_shop_reviews, Tools::getValue('delay'.$this->_prefix_shop_reviews));
            }

            Configuration::updateValue($this->name.'reminder'.$this->_prefix_shop_reviews, Tools::getValue('reminder'.$this->_prefix_shop_reviews));

            // orderstatuses
            $orderstatuses = Tools::getValue('orderstatuses');
            $orderstatuses = implode(",",$orderstatuses);
            Configuration::updateValue($this->name.'orderstatuses'.$this->_prefix_shop_reviews, $orderstatuses);


            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //

                $url = $currentIndex . '&conf=6&tab=AdminModules&shopcustomerreminderset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 74;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }
        ### customerremindersettings settings ###

        ### emailsubjectssettings settings ###
        $emailsubjectsset = Tools::getValue("emailsubjectsset");
        if (Tools::strlen($emailsubjectsset)>0) {

            ob_start();
            $number_tab = 75;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('emailsubjectssettings'))
        {
            $languages = Language::getLanguages(false);
            foreach ($languages as $language){
                $i = $language['id_lang'];
                Configuration::updateValue($this->name.'emrem'.$this->_prefix_shop_reviews.'_'.$i, Tools::getValue('emrem'.$this->_prefix_shop_reviews.'_'.$i));
                Configuration::updateValue($this->name.'reminderok'.$this->_prefix_shop_reviews.'_'.$i, Tools::getValue('reminderok'.$this->_prefix_shop_reviews.'_'.$i));
                Configuration::updateValue($this->name.'thankyou'.$this->_prefix_shop_reviews.'_'.$i, Tools::getValue('thankyou'.$this->_prefix_shop_reviews.'_'.$i));
                Configuration::updateValue($this->name.'newtest'.$this->_prefix_shop_reviews.'_'.$i, Tools::getValue('newtest'.$this->_prefix_shop_reviews.'_'.$i));
                Configuration::updateValue($this->name.'resptest'.$this->_prefix_shop_reviews.'_'.$i, Tools::getValue('resptest'.$this->_prefix_shop_reviews.'_'.$i));
                Configuration::updateValue($this->name.'revvoucr'.$this->_prefix_shop_reviews.'_'.$i, Tools::getValue('revvoucr'.$this->_prefix_shop_reviews.'_'.$i));
            }

            Configuration::updateValue($this->name.'is_emrem'.$this->_prefix_shop_reviews, Tools::getValue('is_emrem'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'is_reminderok'.$this->_prefix_shop_reviews, Tools::getValue('is_reminderok'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'is_thankyou'.$this->_prefix_shop_reviews, Tools::getValue('is_thankyou'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'is_newtest'.$this->_prefix_shop_reviews, Tools::getValue('is_newtest'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'is_resptest'.$this->_prefix_shop_reviews, Tools::getValue('is_resptest'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'is_revvoucr'.$this->_prefix_shop_reviews, Tools::getValue('is_revvoucr'.$this->_prefix_shop_reviews));

            $url = $currentIndex.'&conf=6&tab=AdminModules&emailsubjectsset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);
        }
        ### customerremindersettings settings ###

        $start_end_orders_reviews = Tools::getValue("start_end_orders_reviews".$this->_prefix_shop_reviews);
        if (Tools::strlen($start_end_orders_reviews)>0) {

            ob_start();
            $number_tab = 77;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        $start_date_orders = Tools::getValue('start_date_orders'.$this->_prefix_shop_reviews);
        $end_date_orders = Tools::getValue('end_date_orders'.$this->_prefix_shop_reviews);

        if (($start_date_orders || $end_date_orders) && !$start_end_orders_reviews) {

            $url = $currentIndex.'&conf=6&tab=AdminModules&start_date_orders'.$this->_prefix_shop_reviews.'='.$start_date_orders.'&end_date_orders'.$this->_prefix_shop_reviews.'='.$end_date_orders.'&start_end_orders_reviews'.$this->_prefix_shop_reviews.'=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);
        }

        ### csv import ###
        $csvset = Tools::getValue("csvset".$this->_prefix_shop_reviews);
        if (Tools::strlen($csvset)>0) {

            ob_start();
            $number_tab = 78;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('store_csv')) {

            $name_class =  'csvhelp'.$this->_prefix_shop_reviews;
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/'.$name_class.'.class.php');
            $obj_csv = new $name_class;
            $error_data = $obj_csv->import();

            $error_number = $error_data['error_number'];

            switch($error_number){
                case 1:
                    $errors[] = $this->l('Please select the CSV file');
                    break;
                case 2:
                    $errors[] = $this->l('Your CSV file is empty');
                    break;
            }

            if(sizeof($errors)==0) {

                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //

                $url = $currentIndex . '&conf=18&tab=AdminModules&csvset'.$this->_prefix_shop_reviews.'=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {

                ob_start();
                $number_tab = 78;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }
        ### csv import ###

        $reviewsemailssettingsshopset = Tools::getValue("reviewsemailssettingsshopset");
        if (Tools::strlen($reviewsemailssettingsshopset)>0) {

            ob_start();
            $number_tab = 72;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('reviewsemailssettingsshop'))
        {
            Configuration::updateValue($this->name.'noti'.$this->_prefix_shop_reviews, Tools::getValue('noti'.$this->_prefix_shop_reviews));

            if(!Validate::isEmail(Tools::getValue('mail'.$this->_prefix_shop_reviews))){
                $errors[] = $this->l('Admin email is incorrect!');
            } else {
                Configuration::updateValue($this->name . 'mail' . $this->_prefix_shop_reviews, Tools::getValue('mail' . $this->_prefix_shop_reviews));
            }

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&reviewsemailssettingsshopset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 72;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }

        $testimonials_settingsset = Tools::getValue("testimonials_settingsset");
        if (Tools::strlen($testimonials_settingsset)>0) {

            ob_start();
            $number_tab = 73;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('submit_testimonials'))
        {
            Configuration::updateValue($this->name.'is_storerev', Tools::getValue('is_storerev'));

            Configuration::updateValue($this->name.'is_files'.$this->_prefix_shop_reviews, Tools::getValue('is_files'.$this->_prefix_shop_reviews));

            if(!ctype_digit(Tools::getValue('ruploadfiles' . $this->_prefix_shop_reviews)) || Tools::getValue('ruploadfiles' . $this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Number of files user can add for shop review').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'ruploadfiles' . $this->_prefix_shop_reviews, Tools::getValue('ruploadfiles' . $this->_prefix_shop_reviews));
            }

            if(!ctype_digit(Tools::getValue('perpage' . $this->_prefix_shop_reviews)) || Tools::getValue('perpage' . $this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Store reviews per Page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'perpage' . $this->_prefix_shop_reviews, Tools::getValue('perpage' . $this->_prefix_shop_reviews));
            }

            if(!ctype_digit(Tools::getValue('perpagemy' . $this->_prefix_shop_reviews)) || Tools::getValue('perpagemy' . $this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Store reviews per Page in the My account').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'perpagemy' . $this->_prefix_shop_reviews, Tools::getValue('perpagemy' . $this->_prefix_shop_reviews));
            }

            Configuration::updateValue($this->name.'whocanadd'.$this->_prefix_shop_reviews, Tools::getValue('whocanadd'));

            Configuration::updateValue($this->name.'is_avatar', Tools::getValue('is_avatar'));
            Configuration::updateValue($this->name.'is_captcha'.$this->_prefix_shop_reviews, Tools::getValue('is_captcha'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'is_web', Tools::getValue('is_web'));
            Configuration::updateValue($this->name.'is_company', Tools::getValue('is_company'));
            Configuration::updateValue($this->name.'is_addr', Tools::getValue('is_addr'));

            Configuration::updateValue($this->name.'is_country', Tools::getValue('is_country'));
            Configuration::updateValue($this->name.'is_city', Tools::getValue('is_city'));

            Configuration::updateValue($this->name.'is_sortf'.$this->_prefix_shop_reviews, Tools::getValue('is_sortf'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'is_sortfu'.$this->_prefix_shop_reviews, Tools::getValue('is_sortfu'.$this->_prefix_shop_reviews));

            Configuration::updateValue($this->name.'is_filterall'.$this->_prefix_shop_reviews, Tools::getValue('is_filterall'.$this->_prefix_shop_reviews));

            Configuration::updateValue($this->name.'rswitch_lng'.$this->_prefix_shop_reviews, Tools::getValue('rswitch_lng'.$this->_prefix_shop_reviews));

            if(!ctype_digit(Tools::getValue('n_rssitemst')) || Tools::getValue('n_rssitemst') == NULL) {
                $errors[] = $this->l('Number of items in RSS Feed must be digit!');
            } else {
                Configuration::updateValue($this->name . 'n_rssitemst', Tools::getValue('n_rssitemst'));
            }
            Configuration::updateValue($this->name.'rssontestim', Tools::getValue('rssontestim'));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&testimonials_settingsset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 73;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }

        // voucher settings

        $vouchersetti = Tools::getValue("voucherset".$this->_prefix_shop_reviews);
        if (Tools::strlen($vouchersetti)>0) {
            ob_start();
            $number_tab = 76;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('vouchersettings'.$this->_prefix_shop_reviews))
        {

            ## ids groups ##
            $ids_groups = Tools::getValue("ids_groups".$this->_prefix_shop_reviews);

            if(!($ids_groups)) {
                $errors[] = $this->l('Please select the Customers Groups');
            } else {
                $ids_groups = implode(",",$ids_groups);
                Configuration::updateValue($this->name.'ids_groups'.$this->_prefix_shop_reviews, $ids_groups);

            }
            ## ids groups ##

            Configuration::updateValue($this->name.'vis_on'.$this->_prefix_shop_reviews, Tools::getValue('vis_on'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'is_show_min'.$this->_prefix_shop_reviews, Tools::getValue('is_show_min'.$this->_prefix_shop_reviews));

            $languages = Language::getLanguages(false);
            foreach ($languages as $language){
                $i = $language['id_lang'];
                Configuration::updateValue($this->name.'coupondesc'.$this->_prefix_shop_reviews.'_'.$i, Tools::getValue('coupondesc'.$this->_prefix_shop_reviews.'_'.$i));
            }

            if(Tools::getValue('vouchercode'.$this->_prefix_shop_reviews) == NULL || Tools::strlen(Tools::getValue('vouchercode'.$this->_prefix_shop_reviews))<3) {
                $errors[] = $this->l('Voucher code cannot be empty or must be at least 3 letters long!');
            } else {
                Configuration::updateValue($this->name . 'vouchercode'.$this->_prefix_shop_reviews, Tools::getValue('vouchercode'.$this->_prefix_shop_reviews));
            }

            Configuration::updateValue($this->name.'discount_type'.$this->_prefix_shop_reviews, Tools::getValue('discount_type'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'percentage_val'.$this->_prefix_shop_reviews, Tools::getValue('percentage_val'.$this->_prefix_shop_reviews));

            foreach (Tools::getValue('sdamount'.$this->_prefix_shop_reviews) AS $id => $value){
                Configuration::updateValue('sdamount'.$this->_prefix_shop_reviews.'_'.(int)($id), (float)($value));
            }

            if(Tools::getValue('discount_type'.$this->_prefix_shop_reviews) == 2){
                Configuration::updateValue($this->name.'tax'.$this->_prefix_shop_reviews, Tools::getValue('tax'.$this->_prefix_shop_reviews));
            }

            if(!ctype_digit(Tools::getValue('sdvvalid'.$this->_prefix_shop_reviews)) || Tools::getValue('sdvvalid'.$this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Voucher validity').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'sdvvalid'.$this->_prefix_shop_reviews, Tools::getValue('sdvvalid'.$this->_prefix_shop_reviews));
            }

            if(Tools::getValue($this->name.'isminamount'.$this->_prefix_shop_reviews) == true){
                foreach (Tools::getValue('sdminamount'.$this->_prefix_shop_reviews) AS $id => $value){
                    Configuration::updateValue('sdminamount'.$this->_prefix_shop_reviews.'_'.(int)($id), (float)($value));
                }
            }

            Configuration::updateValue($this->name.'isminamount'.$this->_prefix_shop_reviews, Tools::getValue($this->name.'isminamount'.$this->_prefix_shop_reviews));

            // category
            $categoryBox = Tools::getValue('categoryBox'.$this->_prefix_shop_reviews);
            $categoryBox = implode(",",$categoryBox);
            Configuration::updateValue($this->name.'catbox'.$this->_prefix_shop_reviews, $categoryBox);

            // cumulable
            Configuration::updateValue($this->name.'cumulativeother'.$this->_prefix_shop_reviews, Tools::getValue('cumulativeother'.$this->_prefix_shop_reviews));
            Configuration::updateValue($this->name.'cumulativereduc'.$this->_prefix_shop_reviews, Tools::getValue('cumulativereduc'.$this->_prefix_shop_reviews));

            Configuration::updateValue($this->name.'highlight'.$this->_prefix_shop_reviews, Tools::getValue('highlight'.$this->_prefix_shop_reviews));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&voucherset'.$this->_prefix_shop_reviews.'=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 76;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }
        // voucher settings

        ## store reviews ###
        $start_end_orders_reviews = Tools::getValue("start_end_orders_reviews");
        if (Tools::strlen($start_end_orders_reviews)>0) {

            ob_start();
            $number_tab = 44;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        $start_date_orders = Tools::getValue('start_date_orders');
        $end_date_orders = Tools::getValue('end_date_orders');

        if (($start_date_orders || $end_date_orders) && !$start_end_orders_reviews) {

            $url = $currentIndex.'&conf=6&tab=AdminModules&start_date_orders='.$start_date_orders.'&end_date_orders='.$end_date_orders.'&start_end_orders_reviews=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);
        }

        ### csv import ###
        $csvset = Tools::getValue("csvset");
        if (Tools::strlen($csvset)>0) {

            ob_start();
            $number_tab = 45;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();

        }

        if (Tools::isSubmit('product_csv')) {

            $name_class =  'csvhelp'.$this->_prefix_review;
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/'.$name_class.'.class.php');
            $obj_csv = new $name_class;
            $error_data = $obj_csv->import();

            $error_number = $error_data['error_number'];

            switch($error_number){
                case 1:
                    $errors[] = $this->l('Please select the CSV file');
                    break;
                case 2:
                    $errors[] = $this->l('Your CSV file is empty');
                    break;
            }

            if(sizeof($errors)==0) {

                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //

                $url = $currentIndex . '&conf=18&tab=AdminModules&csvset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {

                ob_start();
                $number_tab = 45;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }
        ### csv import ###

        #### user profile ###
        $userprofilegset = Tools::getValue("userprofilegset");
        if (Tools::strlen($userprofilegset)>0) {

            ob_start();
            $number_tab = 56;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('userprofilegsettings'))
        {
            Configuration::updateValue($this->name.'is_uprof', Tools::getValue('is_uprof'));

            Configuration::updateValue($this->name.'is_proftab', Tools::getValue('is_proftab'));


            if(!ctype_digit(Tools::getValue('perpageu' . $this->_prefix_shop_reviews)) || Tools::getValue('perpageu' . $this->_prefix_shop_reviews) == NULL) {
                $errors[] = $this->l('Store reviews per Page on the User Page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'perpageu' . $this->_prefix_shop_reviews, Tools::getValue('perpageu' . $this->_prefix_shop_reviews));
            }

            if(!ctype_digit(Tools::getValue('perpageu' . $this->_prefix_review)) || Tools::getValue('perpageu' . $this->_prefix_review) == NULL) {
                $errors[] = $this->l('Product reviews per Page on the User Page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'perpageu' . $this->_prefix_review, Tools::getValue('perpageu' . $this->_prefix_review));
            }

            if(!ctype_digit(Tools::getValue('rpage_shoppers')) || Tools::getValue('rpage_shoppers') == NULL) {
                $errors[] = $this->l('Users per page in the list view must be digit!');
            } else {
                Configuration::updateValue($this->name . 'rpage_shoppers', Tools::getValue('rpage_shoppers'));
            }

            Configuration::updateValue($this->name.'radv_home', Tools::getValue('radv_home'));
            Configuration::updateValue($this->name.'radv_left', Tools::getValue('radv_left'));
            Configuration::updateValue($this->name.'radv_right', Tools::getValue('radv_right'));
            Configuration::updateValue($this->name.'radv_footer', Tools::getValue('radv_footer'));

            if(!ctype_digit(Tools::getValue('rshoppers_blc')) || Tools::getValue('rshoppers_blc') == NULL) {
                $errors[] = $this->l('The number of shoppers in the "Block Users"').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'rshoppers_blc', Tools::getValue('rshoppers_blc'));
            }

            Configuration::updateValue($this->name.'sr_slideru', Tools::getValue('sr_slideru'));
            if(!ctype_digit(Tools::getValue('sr_slu')) || Tools::getValue('sr_slu') == NULL) {
                $errors[] = $this->l('Displayed number of items in the slider in the block Block Users block in the Left, Right, Footer positions').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'sr_slu', Tools::getValue('sr_slu'));
            }

            Configuration::updateValue($this->name.'sr_sliderhu', Tools::getValue('sr_sliderhu'));
            if(!ctype_digit(Tools::getValue('sr_slhu')) || Tools::getValue('sr_slhu') == NULL) {
                $errors[] = $this->l('Displayed number of items in the slider in the block Users block on the Home Page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name.'sr_slhu', Tools::getValue('sr_slhu'));
            }

            Configuration::updateValue($this->name.'d_eff_shopu', Tools::getValue('d_eff_shopu'));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&userprofilegset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 56;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }

    	#### posts api ###
    	$vkset = Tools::getValue("vkset");
        if (Tools::strlen($vkset)>0) {

            ob_start();
            $number_tab = 8;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }
    	if (Tools::isSubmit('psvkpostsettings'))
        {
        	Configuration::updateValue($this->name.'vkpost_on', Tools::getValue('vkpost_on'));
        	
        	$languages = Language::getLanguages(false);
        	foreach ($languages as $language){
    			$i = $language['id_lang'];
        		Configuration::updateValue($this->name.'vkdesc_'.$i, Tools::getValue('vkdesc_'.$i));
        	}
        	
        	$url = $currentIndex.'&conf=6&tab=AdminModules&vkset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
        
      	$twset = Tools::getValue("twset");
        if (Tools::strlen($twset)>0) {
            ob_start();
            $number_tab = 8;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }
    	if (Tools::isSubmit('pstwitterpostsettings'))
        {

            $array_wall_posts_items = array('tw',
                //'i',
                'p');

            foreach($array_wall_posts_items as $wall_post_item) {
                Configuration::updateValue($this->name . $wall_post_item.'post_on', Tools::getValue($wall_post_item.'post_on'));


                $languages = Language::getLanguages(false);
                foreach ($languages as $language) {
                    $i = $language['id_lang'];
                    Configuration::updateValue($this->name . $wall_post_item.'desc_' . $i, Tools::getValue($wall_post_item.'desc_' . $i));
                }
            }


        	$url = $currentIndex.'&conf=6&tab=AdminModules&twset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
    	#### posts api ###
    	
    	// pinterest settings
    	$pinterestset = Tools::getValue("pinterestset");
        if (Tools::strlen($pinterestset)>0) {

            ob_start();
            $number_tab = 7;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }
    	if (Tools::isSubmit('pinterestsettings'))
        {
        	Configuration::updateValue($this->name.'pinvis_on', Tools::getValue('pinvis_on'));
        	Configuration::updateValue($this->name.'pinterestbuttons', Tools::getValue('pinterestbuttons'));
        	
        	Configuration::updateValue($this->name.'_leftColumn', Tools::getValue('leftColumn'));
        	Configuration::updateValue($this->name.'_extraLeft', Tools::getValue('extraLeft'));
        	Configuration::updateValue($this->name.'_productFooter', Tools::getValue('productFooter'));
        	Configuration::updateValue($this->name.'_rightColumn', Tools::getValue('rightColumn'));
        	Configuration::updateValue($this->name.'_extraRight', Tools::getValue('extraRight'));
        	Configuration::updateValue($this->name.'_productActions', Tools::getValue('productActions'));

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //

        	$url = $currentIndex.'&conf=6&tab=AdminModules&pinterestset=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
        	Tools::redirectAdmin($url);
        }
    	// pinterest settings
    	
    	// google rich snippets
    	
    	$snippetsset = Tools::getValue("snippetsset");
        if (Tools::strlen($snippetsset)>0) {
            ob_start();
            $number_tab = 2;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('snippetssettings'))
        {
            Configuration::updateValue($this->name.'allinfo_on', Tools::getValue('allinfo_on'));

            Configuration::updateValue($this->name.'allinfo_home_pos', Tools::getValue('pallinfo_home'));
            Configuration::updateValue($this->name.'allinfo_cat_pos', Tools::getValue('pallinfo_cat'));
            Configuration::updateValue($this->name.'allinfo_man_pos', Tools::getValue('pallinfo_man'));

            if(!ctype_digit(Tools::getValue('allinfo_home_w')) || Tools::getValue('allinfo_home_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('for home page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'allinfo_home_w', Tools::getValue('allinfo_home_w'));
            }

            if(!ctype_digit(Tools::getValue('allinfo_cat_w')) || Tools::getValue('allinfo_cat_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('for each category page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'allinfo_cat_w', Tools::getValue('allinfo_cat_w'));
            }

            if(!ctype_digit(Tools::getValue('allinfo_man_w')) || Tools::getValue('allinfo_man_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('for each manufacturer/brand page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'allinfo_man_w', Tools::getValue('allinfo_man_w'));
            }

            Configuration::updateValue($this->name.'allinfo_home', Tools::getValue('allinfo_home'));
            Configuration::updateValue($this->name.'allinfo_cat', Tools::getValue('allinfo_cat'));
            Configuration::updateValue($this->name.'allinfo_man', Tools::getValue('allinfo_man'));

            Configuration::updateValue($this->name.'svis_on', Tools::getValue('svis_on'));
            Configuration::updateValue($this->name.'breadvis_on', Tools::getValue('breadvis_on'));

            Configuration::updateValue($this->name.'pinvis_on', Tools::getValue('pinvis_on'));
            Configuration::updateValue($this->name.'pinterestbuttons', Tools::getValue('pinterestbuttons'));

            Configuration::updateValue($this->name.'_leftColumn', Tools::getValue('leftColumn'));
            Configuration::updateValue($this->name.'_extraLeft', Tools::getValue('extraLeft'));
            Configuration::updateValue($this->name.'_productFooter', Tools::getValue('productFooter'));
            Configuration::updateValue($this->name.'_rightColumn', Tools::getValue('rightColumn'));
            Configuration::updateValue($this->name.'_extraRight', Tools::getValue('extraRight'));
            Configuration::updateValue($this->name.'_productActions', Tools::getValue('productActions'));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&snippetsset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 2;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }
    	// google rich snippets
    	
        ### global settings ###
        $revglobalsettings = Tools::getValue("revglobalsettings");
        if (Tools::strlen($revglobalsettings)>0) {

            ob_start();
            $number_tab = 31;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('globalsettings'))
        {
            Configuration::updateValue($this->name.'rvis_on', Tools::getValue('rvis_on'));
            Configuration::updateValue($this->name.'ratings_on', Tools::getValue('ratings_on'));
            Configuration::updateValue($this->name.'text_on', Tools::getValue('text_on'));
            Configuration::updateValue($this->name.'title_on', Tools::getValue('title_on'));
            Configuration::updateValue($this->name.'ip_on', Tools::getValue('ip_on'));
            Configuration::updateValue($this->name.'is_captcha', Tools::getValue('is_captcha'));

            Configuration::updateValue($this->name.'is_filterp', Tools::getValue('is_filterp'));
            Configuration::updateValue($this->name.'is_filterall', Tools::getValue('is_filterall'));

            Configuration::updateValue($this->name.'is_avatar'.$this->_prefix_review, Tools::getValue('is_avatar'.$this->_prefix_review));
            Configuration::updateValue($this->name.'is_files'.$this->_prefix_review, Tools::getValue('is_files'.$this->_prefix_review));

            if(!ctype_digit(Tools::getValue('ruploadfiles')) || Tools::getValue('ruploadfiles') == NULL) {
                $errors[] = $this->l('Number of files user can add for review').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'ruploadfiles', Tools::getValue('ruploadfiles'));
            }


            if(!ctype_digit(Tools::getValue('rminc')) || Tools::getValue('rminc') == NULL) {
                $errors[] = $this->l('Minimum chars the user must write in the Text field for add review').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'rminc', Tools::getValue('rminc'));
            }

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&revglobalsettings=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 31;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }

        }
        ### global settings ###

        ### product page settings ###
        $revproductpagesettings = Tools::getValue("revproductpagesettings");
        if (Tools::strlen($revproductpagesettings)>0) {

            ob_start();
            $number_tab = 32;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('productpagesettings'))
        {
            Configuration::updateValue($this->name.'ptabs_type', Tools::getValue('ptabs_type'));
            Configuration::updateValue($this->name.'is_abusef', Tools::getValue('is_abusef'));
            Configuration::updateValue($this->name.'is_helpfulf', Tools::getValue('is_helpfulf'));
            Configuration::updateValue($this->name.'is_sortf', Tools::getValue('is_sortf'));

            Configuration::updateValue($this->name.'hooktodisplay', Tools::getValue('hooktodisplay'));
            Configuration::updateValue($this->name.'starratingon', Tools::getValue('starratingon'));
            Configuration::updateValue($this->name.'stylestars', Tools::getValue('stylestars'));

            if(!ctype_digit(Tools::getValue('revperpage')) || Tools::getValue('revperpage') == NULL) {
                $errors[] = $this->l('Number of reviews per page on Product page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'revperpage', Tools::getValue('revperpage'));
            }

            Configuration::updateValue($this->name.'rsoc_on', Tools::getValue('rsoc_on'));
            Configuration::updateValue($this->name.'rsoccount_on', Tools::getValue('rsoccount_on'));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&revproductpagesettings=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 32;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }

        }
        ### product page settings ###

        ### reviewsmanagementsettings settings ###
        $revreviewsmanagementsettings = Tools::getValue("revreviewsmanagementsettings");
        if (Tools::strlen($revreviewsmanagementsettings)>0) {
            ob_start();
            $number_tab = 33;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();

        }


        if (Tools::isSubmit('reviewsmanagementsettings'))
        {
            Configuration::updateValue($this->name.'is_approval', Tools::getValue('is_approval'));
            Configuration::updateValue($this->name.'whocanadd', Tools::getValue('whocanadd'));
            Configuration::updateValue($this->name.'rswitch_lng', Tools::getValue('rswitch_lng'));

            if(!ctype_digit(Tools::getValue('revperpageall')) || Tools::getValue('revperpageall') == NULL) {
                $errors[] = $this->l('Number of reviews per page on All Reviews page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'revperpageall', Tools::getValue('revperpageall'));
            }

            Configuration::updateValue($this->name.'is_onerev', Tools::getValue('is_onerev'));
            Configuration::updateValue($this->name.'is_sortallf', Tools::getValue('is_sortallf'));
            Configuration::updateValue($this->name.'is_sortfu', Tools::getValue('is_sortfu'));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&revreviewsmanagementsettings=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 33;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }

        }
        ### addcriteriasettings settings ###

        ### customeraccountreviewspage settings ###
        if (Tools::isSubmit("spmgsnipreviewcriteriaset") && !$this->_is15)
        {

            $url = $currentIndex.'&tab=AdminModules&revcriteriasettings=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);
        }

        $revcriteriasettings = Tools::getValue("revcriteriasettings");

        $addCriteria = Tools::isSubmit("addCriteria");
        $editspmgsnipreview = Tools::isSubmit('editspmgsnipreview');
        $delete_itemspmgsnipreview = Tools::isSubmit("delete_itemspmgsnipreview");

        if (Tools::strlen($revcriteriasettings)>0
            || $addCriteria || $editspmgsnipreview || $delete_itemspmgsnipreview) {
            ob_start();
            $number_tab = 34;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit("spmgsnipreviewcriteriaset")) {
            ob_start();
            $number_tab = 34;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();

        }

        if (Tools::isSubmit("delete_itemspmgsnipreview")) {
            if (Validate::isInt(Tools::getValue("id"))) {
                $data = array('id' => Tools::getValue("id"));
                include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
                $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
                $obj_spmgsnipreviewhelp->deleteReviewCriteriaItem($data);

                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //

                $url = $currentIndex.'&conf=1&tab=AdminModules&revcriteriasettings=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
                Tools::redirectAdmin($url);
            }

        }

        if (Tools::isSubmit('editcriteriasettings'))
        {
            $id = Tools::getValue("id");

            $cat_shop_association = Tools::getValue("cat_shop_association");

            $languages = Language::getLanguages(false);
            $data_content_lang = array();

            $data_content_lang_name = array();

            foreach ($languages as $language){
                $id_lang = $language['id_lang'];
                $description = Tools::getValue("description_".$id_lang);
                $name = Tools::getValue("name_".$id_lang);
                if(Tools::strlen($name)>0)
                {
                    $data_content_lang[$id_lang] = array('description' => $description,
                                                         'name' => $name);
                    $data_content_lang_name[$id_lang] = array('name' => $name);
                }
            }

            $active = Tools::getValue("active");

            $choose_type = Tools::getValue('criterion_type');
            $categoryBox = Tools::getValue('categoryBoxc');
            $ids_products = Tools::getValue("inputAccessories");

            $data = array('active' => $active,
                          'data_content_lang'=>$data_content_lang,
                          'cat_shop_association' => $cat_shop_association,
                          'id' => $id,

                          'id_product' => $ids_products,
                          'choose_type' => $choose_type,
                          'categoryBox' => $categoryBox,

            );
            if(sizeof($data_content_lang_name)>0 && $cat_shop_association){
                include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
                $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
                $obj_spmgsnipreviewhelp->updateReviewCriteriaItem($data);

            } else {
                $error = 2;
                $errors[] = $this->l('Criterion name or Shop association is empty!');
                //$_html .= $this->_error(array('text' => 'Criterion name or Description is empty!'));
                $this->_criterion_error = 1;
            }

            if($error == 0){

                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //

                $url = $currentIndex.'&conf=4&tab=AdminModules&revcriteriasettings=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
                Tools::redirectAdmin($url);
            }
        }

        if (Tools::isSubmit('addcriteriasettings'))
        {
            $languages = Language::getLanguages(false);
            $data_content_lang = array();
            $data_content_lang_name = array();

            $cat_shop_association = Tools::getValue("cat_shop_association");

            foreach ($languages as $language){
                $id_lang = $language['id_lang'];
                $description = Tools::getValue("description_".$id_lang);
                $name = Tools::getValue("name_".$id_lang);
                if(Tools::strlen($name)>0)
                {
                    $data_content_lang[$id_lang] = array( 'description' => $description,
                                                           'name' => $name
                                                        );
                    $data_content_lang_name[$id_lang] = array('name' => $name);
                }
            }

            $active = Tools::getValue("active");

            $choose_type = Tools::getValue('criterion_type');
            $categoryBox = Tools::getValue('categoryBoxc');
            $ids_products = Tools::getValue("inputAccessories");

            $data = array(
                'active' => $active,
                'data_content_lang'=>$data_content_lang,
                'cat_shop_association' => $cat_shop_association,
                'id_product' => $ids_products,
                'choose_type' => $choose_type,
                'categoryBox' => $categoryBox,
            );

            if(sizeof($data_content_lang_name)>0 && $cat_shop_association){
                include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
                $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
                $obj_spmgsnipreviewhelp->saveReviewCriteriaItem($data);

            } else {
                $error = 2;
                $errors[] = $this->l('Criterion name or Shop association is empty!');
                $this->_criterion_error = 1;

            }

            if($error == 0){

                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //

                $url = $currentIndex.'&conf=3&tab=AdminModules&revcriteriasettings=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
                Tools::redirectAdmin($url);
            }
        }
        ### addcriteriasettings settings ###


        ### customeraccountreviewspage settings ###
        $revcustomeraccountreviewspagesettings = Tools::getValue("revcustomeraccountreviewspagesettings");
        if (Tools::strlen($revcustomeraccountreviewspagesettings)>0) {
            ob_start();
            $number_tab = 35;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('customeraccountreviewspagesettings'))
        {
            if(!ctype_digit(Tools::getValue('revperpagecus')) || Tools::getValue('revperpagecus') == NULL) {
                $errors[] = $this->l('Number of reviews per page on Customer account page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'revperpagecus', Tools::getValue('revperpagecus'));
            }

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&revcustomeraccountreviewspagesettings=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 35;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }

        }
        ### customeraccountreviewspage settings ###

        ### lastreviewsblock settings ###
        $revlastreviewsblocksettings = Tools::getValue("revlastreviewsblocksettings");
        if (Tools::strlen($revlastreviewsblocksettings)>0) {
            ob_start();
            $number_tab = 36;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('lastreviewsblocksettings'))
        {
            Configuration::updateValue($this->name.'is_blocklr', Tools::getValue('is_blocklr'));

            Configuration::updateValue($this->name.'blocklr_home_pos', Tools::getValue('pblocklr_home'));
            Configuration::updateValue($this->name.'blocklr_cat_pos', Tools::getValue('pblocklr_cat'));
            Configuration::updateValue($this->name.'blocklr_man_pos', Tools::getValue('pblocklr_man'));
            Configuration::updateValue($this->name.'blocklr_prod_pos', Tools::getValue('pblocklr_prod'));
            Configuration::updateValue($this->name.'blocklr_oth_pos', Tools::getValue('pblocklr_oth'));
            Configuration::updateValue($this->name.'blocklr_chook_pos', Tools::getValue('pblocklr_chook'));

            if(!ctype_digit(Tools::getValue('blocklr_home_w')) || Tools::getValue('blocklr_home_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('Last Reviews Block').' '.$this->l('for home page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_home_w', Tools::getValue('blocklr_home_w'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_cat_w')) || Tools::getValue('blocklr_cat_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('Last Reviews Block').' '.$this->l('for each category page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_cat_w', Tools::getValue('blocklr_cat_w'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_man_w')) || Tools::getValue('blocklr_man_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('Last Reviews Block').' '.$this->l('for each manufacturer/brand').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_man_w', Tools::getValue('blocklr_man_w'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_prod_w')) || Tools::getValue('blocklr_prod_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('Last Reviews Block').' '.$this->l('for each product page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_prod_w', Tools::getValue('blocklr_prod_w'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_oth_w')) || Tools::getValue('blocklr_oth_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('Last Reviews Block').' '.$this->l('for other pages').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_oth_w', Tools::getValue('blocklr_oth_w'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_chook_w')) || Tools::getValue('blocklr_chook_w') == NULL) {
                $errors[] = $this->l('Width').' '.$this->l('Last Reviews Block').' '.$this->l('for CUSTOM HOOK pages').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_chook_w', Tools::getValue('blocklr_chook_w'));
            }

            Configuration::updateValue($this->name.'blocklr_home', Tools::getValue('blocklr_home'));
            Configuration::updateValue($this->name.'blocklr_cat', Tools::getValue('blocklr_cat'));
            Configuration::updateValue($this->name.'blocklr_man', Tools::getValue('blocklr_man'));
            Configuration::updateValue($this->name.'blocklr_prod', Tools::getValue('blocklr_prod'));
            Configuration::updateValue($this->name.'blocklr_oth', Tools::getValue('blocklr_oth'));
            Configuration::updateValue($this->name.'blocklr_chook', Tools::getValue('blocklr_chook'));

            if(!ctype_digit(Tools::getValue('blocklr_home_ndr')) || Tools::getValue('blocklr_home_ndr') == NULL) {
                $errors[] = $this->l('Number').' '.$this->l('of displayed reviews').' '.$this->l('for home page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_home_ndr', Tools::getValue('blocklr_home_ndr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_cat_ndr')) || Tools::getValue('blocklr_cat_ndr') == NULL) {
                $errors[] = $this->l('Number').' '.$this->l('of displayed reviews').' '.$this->l('for each category page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_cat_ndr', Tools::getValue('blocklr_cat_ndr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_man_ndr')) || Tools::getValue('blocklr_man_ndr') == NULL) {
                $errors[] = $this->l('Number').' '.$this->l('of displayed reviews').' '.$this->l('for each manufacturer/brand page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_man_ndr', Tools::getValue('blocklr_man_ndr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_prod_ndr')) || Tools::getValue('blocklr_prod_ndr') == NULL) {
                $errors[] = $this->l('Number').' '.$this->l('of displayed reviews').' '.$this->l('for each product page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_prod_ndr', Tools::getValue('blocklr_prod_ndr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_oth_ndr')) || Tools::getValue('blocklr_oth_ndr') == NULL) {
                $errors[] = $this->l('Number').' '.$this->l('of displayed reviews').' '.$this->l('for other pages').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_oth_ndr', Tools::getValue('blocklr_oth_ndr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_chook_ndr')) || Tools::getValue('blocklr_chook_ndr') == NULL) {
                $errors[] = $this->l('Number').' '.$this->l('of displayed reviews').' '.$this->l('for CUSTOM HOOK pages').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_chook_ndr', Tools::getValue('blocklr_chook_ndr'));
            }

            ////////

            if(!ctype_digit(Tools::getValue('blocklr_home_tr')) || Tools::getValue('blocklr_home_tr') == NULL) {
                $errors[] = $this->l('Truncate').' '.$this->l('reviews').' '.$this->l('for home page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_home_tr', Tools::getValue('blocklr_home_tr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_cat_tr')) || Tools::getValue('blocklr_cat_tr') == NULL) {
                $errors[] = $this->l('Truncate').' '.$this->l('reviews').' '.$this->l('for each category page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_cat_tr', Tools::getValue('blocklr_cat_tr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_man_tr')) || Tools::getValue('blocklr_man_tr') == NULL) {
                $errors[] = $this->l('Truncate').' '.$this->l('reviews').' '.$this->l('for each manufacturer/brand page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_man_tr', Tools::getValue('blocklr_man_tr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_prod_tr')) || Tools::getValue('blocklr_prod_tr') == NULL) {
                $errors[] = $this->l('Truncate').' '.$this->l('reviews').' '.$this->l('for each product page').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_prod_tr', Tools::getValue('blocklr_prod_tr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_oth_tr')) || Tools::getValue('blocklr_oth_tr') == NULL) {
                $errors[] = $this->l('Truncate').' '.$this->l('reviews').' '.$this->l('for other pages').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_oth_tr', Tools::getValue('blocklr_oth_tr'));
            }

            if(!ctype_digit(Tools::getValue('blocklr_chook_tr')) || Tools::getValue('blocklr_chook_tr') == NULL) {
                $errors[] = $this->l('Truncate').' '.$this->l('reviews').' '.$this->l('for CUSTOM HOOK pages').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'blocklr_chook_tr', Tools::getValue('blocklr_chook_tr'));
            }

            Configuration::updateValue($this->name.'blocklr_home_im', Tools::getValue('iblocklr_home'));
            Configuration::updateValue($this->name.'blocklr_cat_im', Tools::getValue('iblocklr_cat'));
            Configuration::updateValue($this->name.'blocklr_man_im', Tools::getValue('iblocklr_man'));
            Configuration::updateValue($this->name.'blocklr_prod_im', Tools::getValue('iblocklr_prod'));
            Configuration::updateValue($this->name.'blocklr_oth_im', Tools::getValue('iblocklr_oth'));
            Configuration::updateValue($this->name.'blocklr_chook_im', Tools::getValue('iblocklr_chook'));


            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&revlastreviewsblocksettings=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 36;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }
        ### lastreviewsblock settings ###


        ### starslistandsearchsettings settings ###
        $revstarslistandsearchsettings = Tools::getValue("revstarslistandsearchsettings");
        if (Tools::strlen($revstarslistandsearchsettings)>0) {
            ob_start();
            $number_tab = 37;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('starslistandsearchsettings'))
        {

            Configuration::updateValue($this->name.'starscat', Tools::getValue('starscat'));
            Configuration::updateValue($this->name.'is_starscat', Tools::getValue('is_starscat'));

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //

            $url = $currentIndex.'&conf=6&tab=AdminModules&revstarslistandsearchsettings=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);

        }
        ### starslistandsearchsettings settings ###

        ### rssfeed settings ###
        $revrssfeedsettings = Tools::getValue("revrssfeedsettings");
        if (Tools::strlen($revrssfeedsettings)>0) {

            ob_start();
            $number_tab = 38;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }


        if (Tools::isSubmit('rssfeedsettings'))
        {
            Configuration::updateValue($this->name.'rsson', Tools::getValue('rsson'));

            if(!ctype_digit(Tools::getValue('number_rssitems')) || Tools::getValue('number_rssitems') == NULL) {
                $errors[] = $this->l('Number of items in RSS Feed').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'number_rssitems', Tools::getValue('number_rssitems'));
            }

            $languages = Language::getLanguages(false);
            foreach ($languages as $language){
                $i = $language['id_lang'];
                Configuration::updateValue($this->name.'rssname_'.$i, Tools::getValue('rssname_'.$i));
                Configuration::updateValue($this->name.'rssdesc_'.$i, Tools::getValue('rssdesc_'.$i));
            }

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //

                $url = $currentIndex . '&conf=6&tab=AdminModules&revrssfeedsettings=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 38;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }
        ### rssfeed settings ###


        ### importcommentssettings settings ###
        $revimportcommentssettings = Tools::getValue("revimportcommentssettings");
        if (Tools::strlen($revimportcommentssettings)>0) {

            ob_start();
            $number_tab = 39;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('submitcomments'))
        {
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/importhelp.class.php');
            $obj = new importhelp();

            $obj->importComments();

            // clear cache //
            $this->clearSmartyCacheItems();
            // clear cache //

            $url = $currentIndex.'&conf=6&tab=AdminModules&revimportcommentssettings=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);

        }
        ### importcommentssettings settings ###


        // Google Product Review Feeds for Google Shopping
        $revsubmitgooglereviews = Tools::getValue("revsubmitgooglereviews");
        if (Tools::strlen($revsubmitgooglereviews)>0) {
            ob_start();
            $number_tab = 43;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('submitgooglereviews'))
        {
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
            $obj = new spmgsnipreviewhelp();


            $obj->generateGoogleReviews();

            $url = $currentIndex.'&conf=6&tab=AdminModules&revsubmitgooglereviews=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);

        }
        // Google Product Review Feeds for Google Shopping

        ### reviewsemailssettings settings ###
        $revreviewsemailssettings = Tools::getValue("revreviewsemailssettings");
        if (Tools::strlen($revreviewsemailssettings)>0) {

            ob_start();
            $number_tab = 40;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }


        if (Tools::isSubmit('reviewsemailssettings'))
        {
            if(!Validate::isEmail(Tools::getValue('mail'))){
                $errors[] = $this->l('Admin email is incorrect!');
            } else {
                Configuration::updateValue($this->name . 'mail', Tools::getValue('mail'));
            }

            Configuration::updateValue($this->name.'noti', Tools::getValue('noti'));
            Configuration::updateValue($this->name.'img_size_em', Tools::getValue('img_size_em'));


            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&revreviewsemailssettings=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 40;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }

        }
        ### reviewsemailssettings settings ###

        ### revresponseadminemailssettings settings ###
        $revresponseadminemailssettings = Tools::getValue("revresponseadminemailssettings");
        if (Tools::strlen($revresponseadminemailssettings)>0) {
            ob_start();
            $number_tab = 41;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();

        }

        if (Tools::isSubmit('responseadminemailssettings'))
        {
            $languages = Language::getLanguages(false);
            foreach ($languages as $language){
                $i = $language['id_lang'];
                Configuration::updateValue($this->name.'subresem_'.$i, Tools::getValue('subresem_'.$i));
                Configuration::updateValue($this->name.'textresem_'.$i, Tools::getValue('textresem_'.$i));
                Configuration::updateValue($this->name.'emailreminder_'.$i, Tools::getValue('emailreminder_'.$i));

                Configuration::updateValue($this->name.'reminderok'.$this->_prefix_review.'_'.$i, Tools::getValue('reminderok'.$this->_prefix_review.'_'.$i));
                Configuration::updateValue($this->name.'thankyou'.$this->_prefix_review.'_'.$i, Tools::getValue('thankyou'.$this->_prefix_review.'_'.$i));

                Configuration::updateValue($this->name.'newrev'.$this->_prefix_review.'_'.$i, Tools::getValue('newrev'.$this->_prefix_review.'_'.$i));
                Configuration::updateValue($this->name.'subpubem_'.$i, Tools::getValue('subpubem_'.$i));
                Configuration::updateValue($this->name.'modrev'.$this->_prefix_review.'_'.$i, Tools::getValue('modrev'.$this->_prefix_review.'_'.$i));

                Configuration::updateValue($this->name.'abuserev'.$this->_prefix_review.'_'.$i, Tools::getValue('abuserev'.$this->_prefix_review.'_'.$i));

                Configuration::updateValue($this->name.'facvouc'.$this->_prefix_review.'_'.$i, Tools::getValue('facvouc'.$this->_prefix_review.'_'.$i));
                Configuration::updateValue($this->name.'revvouc'.$this->_prefix_review.'_'.$i, Tools::getValue('revvouc'.$this->_prefix_review.'_'.$i));

                Configuration::updateValue($this->name.'sugvouc'.$this->_prefix_review.'_'.$i, Tools::getValue('sugvouc'.$this->_prefix_review.'_'.$i));

            }

            Configuration::updateValue($this->name.'is_emailreminder', Tools::getValue('is_emailreminder'));
            Configuration::updateValue($this->name.'is_reminderok'.$this->_prefix_review, Tools::getValue('is_reminderok'.$this->_prefix_review));
            Configuration::updateValue($this->name.'is_thankyou'.$this->_prefix_review, Tools::getValue('is_thankyou'.$this->_prefix_review));
            Configuration::updateValue($this->name.'is_newrev'.$this->_prefix_review, Tools::getValue('is_newrev'.$this->_prefix_review));
            Configuration::updateValue($this->name.'is_subresem', Tools::getValue('is_subresem'));
            Configuration::updateValue($this->name.'is_modrev'.$this->_prefix_review, Tools::getValue('is_modrev'.$this->_prefix_review));
            Configuration::updateValue($this->name.'is_subpubem', Tools::getValue('is_subpubem'));
            Configuration::updateValue($this->name.'is_abuserev'.$this->_prefix_review, Tools::getValue('is_abuserev'.$this->_prefix_review));
            Configuration::updateValue($this->name.'is_revvouc'.$this->_prefix_review, Tools::getValue('is_revvouc'.$this->_prefix_review));
            Configuration::updateValue($this->name.'is_facvouc'.$this->_prefix_review, Tools::getValue('is_facvouc'.$this->_prefix_review));
            Configuration::updateValue($this->name.'is_sugvouc'.$this->_prefix_review, Tools::getValue('is_sugvouc'.$this->_prefix_review));

            $url = $currentIndex.'&conf=6&tab=AdminModules&revresponseadminemailssettings=1&configure='.$this->name.'&token='.Tools::getAdminToken('AdminModules'.(int)(Tab::getIdFromClassName('AdminModules')).(int)($cookie->id_employee)).'';
            Tools::redirectAdmin($url);

        }
        ### revresponseadminemailssettings settings ###

        ### customerremindersettings settings ###
        $revcustomerremindersettings = Tools::getValue("revcustomerremindersettings");
        if (Tools::strlen($revcustomerremindersettings)>0) {
            ob_start();
            $number_tab = 42;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('customerremindersettings'))
        {

            if(!ctype_digit(Tools::getValue('crondelay' . $this->_prefix_review)) || Tools::getValue('crondelay' . $this->_prefix_review) == NULL) {
                $errors[] = $this->l('Delay between each email in seconds').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'crondelay' . $this->_prefix_review, Tools::getValue('crondelay' . $this->_prefix_review));
            }

            if(!ctype_digit(Tools::getValue('cronnpost' . $this->_prefix_review)) || Tools::getValue('cronnpost' . $this->_prefix_review) == NULL) {
                $errors[] = $this->l('Number of emails for each cron call').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'cronnpost' . $this->_prefix_review, Tools::getValue('cronnpost' . $this->_prefix_review));
            }

            if(!ctype_digit(Tools::getValue('delay')) || Tools::getValue('delay') == NULL) {
                $errors[] = $this->l('Delay for sending reminder by email').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'delay', Tools::getValue('delay'));
            }

            Configuration::updateValue($this->name.'reminder', Tools::getValue('reminder'));

            // orderstatuses
            $orderstatuses = Tools::getValue('orderstatuses');
            $orderstatuses = implode(",",$orderstatuses);
            Configuration::updateValue($this->name.'orderstatuses', $orderstatuses);

            if(!ctype_digit(Tools::getValue('delaysec' . $this->_prefix_review)) || Tools::getValue('delaysec' . $this->_prefix_review) == NULL) {
                $errors[] = $this->l('Days after the first emails were sent').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'delaysec' . $this->_prefix_review, Tools::getValue('delaysec' . $this->_prefix_review));
            }

            Configuration::updateValue($this->name.'remindersec'.$this->_prefix_review, Tools::getValue('remindersec'.$this->_prefix_review));
            Configuration::updateValue($this->name.'remrevsec'.$this->_prefix_review, Tools::getValue('remrevsec'.$this->_prefix_review));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&revcustomerremindersettings=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 42;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }

        }
        ### customerremindersettings settings ###

        // voucher settings
        
    	$voucherset = Tools::getValue("voucherset");
        if (Tools::strlen($voucherset)>0) {
            ob_start();
            $number_tab = 5;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }
        
        if (Tools::isSubmit('vouchersettings'))
        {

            ## ids groups ##
            $ids_groups = Tools::getValue("ids_groups");

            if(!($ids_groups)) {
                $errors[] = $this->l('Please select the Customers Groups');
            } else {
                $ids_groups = implode(",",$ids_groups);
                Configuration::updateValue($this->name.'ids_groups', $ids_groups);

            }
            ## ids groups ##

        	Configuration::updateValue($this->name.'vis_on', Tools::getValue('vis_on'));
            Configuration::updateValue($this->name.'is_show_min', Tools::getValue('is_show_min'));

   			$languages = Language::getLanguages(false);
        	foreach ($languages as $language){
    			$i = $language['id_lang'];
        		Configuration::updateValue($this->name.'coupondesc_'.$i, Tools::getValue('coupondesc_'.$i));
        	}

            if(Tools::getValue('vouchercode') == NULL || Tools::strlen(Tools::getValue('vouchercode'))<3) {
                $errors[] = $this->l('Voucher code cannot be empty or must be at least 3 letters long!');
            } else {
                Configuration::updateValue($this->name . 'vouchercode', Tools::getValue('vouchercode'));
            }
        	
        	Configuration::updateValue($this->name.'discount_type', Tools::getValue('discount_type'));
			Configuration::updateValue($this->name.'percentage_val', Tools::getValue('percentage_val'));
			
        	
        	foreach (Tools::getValue('sdamount') AS $id => $value){
				Configuration::updateValue('sdamount_'.(int)($id), (float)($value));
        	}
            
        	if(Tools::getValue('discount_type') == 2){
        		Configuration::updateValue($this->name.'tax', Tools::getValue('tax'));
        	}


            if(!ctype_digit(Tools::getValue('sdvvalid')) || Tools::getValue('sdvvalid') == NULL) {
                $errors[] = $this->l('Voucher validity').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'sdvvalid', Tools::getValue('sdvvalid'));
            }
            
            
            if(Tools::getValue($this->name.'isminamount') == true){
	        	foreach (Tools::getValue('sdminamount') AS $id => $value){
					Configuration::updateValue('sdminamount_'.(int)($id), (float)($value));
	        	}
        	}
        	
            Configuration::updateValue($this->name.'isminamount', Tools::getValue($this->name.'isminamount'));
        	
            // category
            $categoryBox = Tools::getValue('categoryBox');
            $categoryBox = implode(",",$categoryBox);
            Configuration::updateValue($this->name.'catbox', $categoryBox);
            
            // cumulable
            Configuration::updateValue($this->name.'cumulativeother', Tools::getValue('cumulativeother'));
			Configuration::updateValue($this->name.'cumulativereduc', Tools::getValue('cumulativereduc'));

            Configuration::updateValue($this->name.'highlight', Tools::getValue('highlight'));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&voucherset=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 5;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }
        
        // voucher settings


        // voucher facebook settings

        $vouchersetfb = Tools::getValue("vouchersetfb");
        if (Tools::strlen($vouchersetfb)>0) {
            ob_start();
            $number_tab = 55;
            include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
            $_html .= ob_get_clean();
        }

        if (Tools::isSubmit('vouchersettingsfb'))
        {

            ## ids groups ##
            $ids_groups = Tools::getValue("ids_groupsfb");

            if(!($ids_groups)) {
                $errors[] = $this->l('Please select the Customers Groups');
            } else {
                $ids_groups = implode(",",$ids_groups);
                Configuration::updateValue($this->name.'ids_groupsfb', $ids_groups);

            }
            ## ids groups ##

            Configuration::updateValue($this->name.'vis_onfb', Tools::getValue('vis_onfb'));
            Configuration::updateValue($this->name.'is_show_minfb', Tools::getValue('is_show_minfb'));

            $languages = Language::getLanguages(false);
            foreach ($languages as $language){
                $i = $language['id_lang'];
                Configuration::updateValue($this->name.'coupondescfb_'.$i, Tools::getValue('coupondescfb_'.$i));
            }


            if(Tools::getValue('vouchercodefb') == NULL || Tools::strlen(Tools::getValue('vouchercodefb'))<3) {
                $errors[] = $this->l('Voucher code cannot be empty or must be at least 3 letters long!');
            } else {
                Configuration::updateValue($this->name . 'vouchercodefb', Tools::getValue('vouchercodefb'));
            }

            Configuration::updateValue($this->name.'discount_typefb', Tools::getValue('discount_typefb'));
            Configuration::updateValue($this->name.'percentage_valfb', Tools::getValue('percentage_valfb'));


            foreach (Tools::getValue('sdamountfb') AS $id => $value){
                Configuration::updateValue('sdamountfb_'.(int)($id), (float)($value));
            }

            if(Tools::getValue('discount_typefb') == 2){
                Configuration::updateValue($this->name.'taxfb', Tools::getValue('taxfb'));
            }

            if(!ctype_digit(Tools::getValue('sdvvalidfb')) || Tools::getValue('sdvvalidfb') == NULL) {
                $errors[] = $this->l('Voucher validity').' '.$this->l('must be digit!');
            } else {
                Configuration::updateValue($this->name . 'sdvvalidfb', Tools::getValue('sdvvalidfb'));
            }


            if(Tools::getValue($this->name.'isminamountfb') == true){
                foreach (Tools::getValue('sdminamountfb') AS $id => $value){
                    Configuration::updateValue('sdminamountfb_'.(int)($id), (float)($value));
                }
            }

            Configuration::updateValue($this->name.'isminamountfb', Tools::getValue($this->name.'isminamountfb'));

            // category
            $categoryBox = Tools::getValue('categoryBoxfb');
            $categoryBox = implode(",",$categoryBox);
            Configuration::updateValue($this->name.'catboxfb', $categoryBox);


            // cumulable
            Configuration::updateValue($this->name.'cumulativeotherfb', Tools::getValue('cumulativeotherfb'));
            Configuration::updateValue($this->name.'cumulativereducfb', Tools::getValue('cumulativereducfb'));

            Configuration::updateValue($this->name.'highlightfb', Tools::getValue('highlightfb'));

            if(sizeof($errors)==0) {
                // clear cache //
                $this->clearSmartyCacheItems();
                // clear cache //
                $url = $currentIndex . '&conf=6&tab=AdminModules&vouchersetfb=1&configure=' . $this->name . '&token=' . Tools::getAdminToken('AdminModules' . (int)(Tab::getIdFromClassName('AdminModules')) . (int)($cookie->id_employee)) . '';
                Tools::redirectAdmin($url);
            } else {
                ob_start();
                $number_tab = 55;
                include(dirname(__FILE__).'/views/templates/hooks/js.phtml');
                $_html .= ob_get_clean();
            }
        }

        // voucher facebook settings

        $_html .= $this->_displayForm16(array('errors'=>$errors));

        return $_html;
    }

    private function _error($data){
        $text = $data['text'];
        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }

    protected function addBackOfficeMedia()
    {
        $this->context->controller->addCSS($this->_path.'views/css/font-custom.min.css');
        //CSS files
        $this->context->controller->addCSS($this->_path.'views/css/menu16.css');

        $this->context->controller->addCSS($this->_path.'views/css/admin.css');

        // JS files
        $this->context->controller->addJs($this->_path.'views/js/menu16.js');

        $this->context->controller->addJs($this->_path.'views/js/reminder.js');

        $this->context->controller->addJs($this->_path.'views/js/storereviews.js');
        $this->context->controller->addJs($this->_path.'views/js/reminder-storereviews.js');

        $this->context->controller->addJs($this->_path.'views/js/criterions-admin.js');
    }

    private function _displayForm16($data = null){
        $_html = '';



        $errors = $data['errors'];
        if(count($errors)>0) {
            foreach ($errors as $error_text) {
                $_html .= $this->_error(array('text' => $error_text));
            }
        }

        $this->_data_translate_custom['_displayForm16_welcome'] = $this->l('Welcome');
        $this->_data_translate_custom['_displayForm16_g_righ_snip_rich_pins'] = $this->l('Google Rich Snippets & Rich Pins');
        $this->_data_translate_custom['_displayForm16_reviews'] = $this->l('Reviews');
        $this->_data_translate_custom['_displayForm16_store_reviews'] = $this->l('Store reviews');
        $this->_data_translate_custom['_displayForm16_user_profile'] = $this->l('User profile');
        $this->_data_translate_custom['_displayForm16_soc_netw_integr'] = $this->l('Social networks Integration');
        $this->_data_translate_custom['_displayForm16_help_doc'] = $this->l('Help / Documentation');
        $this->_data_translate_custom['_displayForm16_mitr_mod'] = $this->l('SPM Modules');


        $_html_promo = '';
        ob_start();
        include_once(_PS_MODULE_DIR_.$this->name.'/views/templates/hooks/promo.phtml');
        $_html_promo = ob_get_clean();


        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html_main = ob_get_clean();



        return $_html_promo.$_html_main;
    }

    private function _shopreviews16(){

        $this->_data_translate_custom['_shopreviews16_main_set'] = $this->l('Main settings');
        $this->_data_translate_custom['_shopreviews16_rev_crit'] = $this->l('Review Criteria');
        $this->_data_translate_custom['_shopreviews16_wow_eff_for_items'] = $this->l('WOW effects for items in the list view settings');
        $this->_data_translate_custom['_shopreviews16_blocks_owl'] = $this->l('Blocks and OWL carousels settings');
        $this->_data_translate_custom['_shopreviews16_voucher_set_when_add_rev'] = $this->l('Voucher settings, when a user add review');
        $this->_data_translate_custom['_shopreviews16_rev_emails_set'] = $this->l('Reviews emails settings');
        $this->_data_translate_custom['_shopreviews16_emails_subj_set'] = $this->l('Emails subjects settings');
        $this->_data_translate_custom['_shopreviews16_cust_rem_set'] = $this->l('Customer Reminder settings');
        $this->_data_translate_custom['_shopreviews16_cus_rem_stat'] = $this->l('Customer Reminder Statistics');
        $this->_data_translate_custom['_shopreviews16_csv_imp_exp_set'] = $this->l('CSV import/export Settings');
        $this->_data_translate_custom['_shopreviews16_cron_help_store_rev'] = $this->l('CRON HELP STORE REVIEWS');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }

    private function _owlcarouselsshop(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title'=>$this->l('Blocks settings'),

            'tlast_title_l'=>$this->l('The number of items in the "Store reviews Block" in the Left Column:'),
            'tlast_title_ls'=>$this->l('The number of items in the "Store reviews Block" in the Left Side:'),
            'tlast_title_r'=>$this->l('The number of items in the "Store reviews Block" in the Right Column:'),
            'tlast_title_rs'=>$this->l('The number of items in the "Store reviews Block" in the Right Side:'),
            'tlast_title_f'=>$this->l('The number of items in the "Store reviews Block" in the footer:'),
            'tlast_title_h'=>$this->l('The number of items in the "Store reviews Block" on the home page:'),

            'BGCOLOR_TIT_title'=>$this->l('Store reviews title color. Only in the positions: Left Side and Right Side'),
            'BGCOLOR_TIT_desc'=>$this->l('You can enter Hexadecimal color code for the title like #000000'),
            'BGCOLOR_T_title'=>$this->l('Store reviews block background color'),
            'BGCOLOR_T_desc'=>$this->l('You can enter Hexadecimal color code for the background like #000000'),
            'r_pos_store_reviews_block_title'=>$this->l('Position Store reviews Block:'),
            't_left'=>$this->l('Left column'),
            't_right'=>$this->l('Right column'),
            't_footer'=>$this->l('Footer'),
            't_home'=>$this->l('Home'),
            't_leftside'=>$this->l('Left Side'),
            't_rightside'=>$this->l('Right Side'),
            'r_google_rich_snippets_in_places'=>$this->l('Enable Google Rich snippets in the following places:'),
            't_tpages'=>$this->l('Store reviews page'),

            'title1'=>$this->l('OWL carousels for block Store Reviews widget in the Left, Right, Footer positions settings'),
            'sr_slider'=>$this->l('Enable or Disable Slider for block "Store Reviews" widget in the Left, Right, Footer positions'),
            'yes_title'=> $this->l('Yes'),
            'no_title'=>$this->l('No'),
            'sr_sl'=>$this->l('Displayed number of items in the slider in the block "Store Reviews" widget in the Left, Right, Footer positions'),

            'title2'=>$this->l('OWL carousel for block Store Reviews widget on the Home Page settings'),
            'sr_sliderh'=>$this->l('Enable or Disable Slider for block "Store Reviews" widget on the Home Page'),
            'sr_slh'=>$this->l('Displayed number of items in the slider in the block "Store Reviews" widget on the Home Page'),

            'update_title'=>$this->l('Update Settings'),
        );
        return $obj_shopreviews->owlcarouselsshop($data);
    }

    private function _woweffectsshop(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();

        $data = array(

            'prefix'=>$this->_prefix_shop_reviews,

            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'd_eff_shop_val'=>$this->_effects(array('value' => 'd_eff_shop'.$this->_prefix_shop_reviews)),
            'd_eff_shop_my_val'=>$this->_effects(array('value' => 'd_eff_shop_my'.$this->_prefix_shop_reviews)),
            'd_eff_shop_u_val'=>$this->_effects(array('value' => 'd_eff_shop_u'.$this->_prefix_shop_reviews)),

            'title'=>$this->l('WOW effects for items in the list view settings'),
            'd_eff_shop_title'=>$this->l('Display effect in the list view on the Store Reviews page'),
            'd_eff_shop_my_title'=>$this->l('Display effect in the My account'),
            'd_eff_shop_u_title'=>$this->l('Display effect in the tab Store Reviews on the User Page'),

            'update_title'=>$this->l('Update Settings'),

        );

        return $obj_shopreviews->woweffectsshop($data);
    }

    private function _reviewsemailsshop(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();

        $data = array(

            'prefix'=>$this->_prefix_shop_reviews,

            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title'=>$this->l('Reviews emails settings'),
            'mail_title'=>$this->l('Admin email:'),
            'noti_title'=>$this->l('E-mail notification:'),

            'update_title'=>$this->l('Update Settings'),

        );

        return $obj_shopreviews->reviewsemailsshop($data);
    }



    private function _voucherwhenaddreviewshopsettings16(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();


        // select categories
        $_selected_cat = Configuration::get($this->name.'catbox'.$this->_prefix_shop_reviews);
        $select_categories = explode(",",$_selected_cat);


        $categories = array();
        foreach ($select_categories as $key => $category) {
            $categories[] = $category;
        }

        $root = Category::getRootCategory();
        $tree = new HelperTreeCategories('associated-categories-tree-'.$this->_prefix_shop_reviews, 'Associated categories');
        $tree->setTemplate('tree_associated_categories.tpl')
            ->setHeaderTemplate('tree_associated_header.tpl')
            ->setTemplateDirectory(dirname(__FILE__).'/views/templates/admin/_configure/helpers/tree/')
            ->setRootCategory((int)$root->id)
            ->setUseCheckBox(true)
            ->setInputName('categoryBox'.$this->_prefix_shop_reviews)
            ->setUseSearch(true)
            ->setSelectedCategories($categories);
        $selected_categories = $tree->render();

        $data = array(

            'prefix'=>$this->_prefix_shop_reviews,

            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'selected_categories'=>$selected_categories,

            'title'=>$this->l('Voucher settings, when a user add review'),
            'vis_on_title'=>$this->l('Enable or Disable Voucher'),
            'vis_on_desc'=>$this->l('Enable or Disable Voucher, when a user add review'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'ids_groups_title'=>$this->l('Voucher will be displayed only for selected Customers Groups'),
            'coupondesc_title'=>$this->l('Coupon Description'),
            'coupondesc_desc'=>$this->l('The description is displayed in cart once your customers use their voucher.'),
            'vouchercode_title'=>$this->l('Voucher code'),
            'vouchercode_desc'=>$this->l('Voucher code prefix. It must be at least 3 letters long. Prefix voucher code will be used in the first part of the coupon code, which the user will use to get a discount'),
            'discount_type_title'=>$this->l('Discount Type'),
            'discount_type1'=>$this->l('Percentages'),
            'discount_type2'=>$this->l('Currency'),
            'tax_title'=>$this->l('Tax'),
            'tax1'=>$this->l('Tax Excluded'),
            'tax2'=>$this->l('Tax Included'),
            'percentage_val'=>$this->l('Voucher percentage'),
            'isminamount'=>$this->l('Minimum checkout'),
            'is_show_min_title'=>$this->l('Display the minimum amount in the Front Office in the add review form ?'),
            'is_show_min_desc'=>$this->l('Use to show or not the minimum amount on the Front office in the add review form'),
            'select_cat_title'=>$this->l('Select categories'),
            'select_cat_desc'=>$this->l('Check all box(es) of categories to which the discount is to be applied. No categories checked will apply the voucher on all of them.'),
            'sdvvalid_title'=>$this->l('Voucher validity'),
            'sdvvalid_desc'=>$this->l('Voucher term of validity in days'),
            'highlight_title'=>$this->l('Highlight'),
            'highlight_desc'=>$this->l('If the voucher is not yet in the cart, it will be displayed in the cart summary.'),
            'cumulativeother_title'=>$this->l('Cumulative with others vouchers'),
            'cumulativereduc_title'=>$this->l('Cumulative with price reductions'),

            'save_title'=>$this->l('Save'),

        );

        return $obj_shopreviews->voucherwhenaddreviewshopsettings16($data);
    }



    private $_criterion_error_ti =  0;
    private function _shopreviewcriteria(){

        if (Tools::isSubmit('addCriteria'.$this->_prefix_shop_reviews) || Tools::isSubmit('editspmgsnipreview'.$this->_prefix_shop_reviews) || $this->_criterion_error_ti) {
            return $this->_displayAddShopReviewcriteriaForm16();
        } else {
            return $this->_displayShopReviewcriteriaGrid16();
        }
    }

    private function _displayAddShopReviewcriteriaForm16(){
        $token = Tools::getAdminTokenLite('AdminModules');
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        $current_index = AdminController::$currentIndex;
        if (!isset($back) || empty($back))
            $back = $current_index.'&amp;configure='.$this->name.'&token='.$token;

        $id_block = Tools::getValue('id');

        if (Tools::isSubmit('id') && Tools::isSubmit('editspmgsnipreview'.$this->_prefix_shop_reviews))
        {
            $this->_display = 'edit';

            include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
            $obj_storereviews = new storereviews();
            $_data = $obj_storereviews->getReviewCriteriaItem(array('id'=>(int)$id_block));

            $id_shop = isset($_data['item'][0]['id_shop']) ? explode(",",$_data['item'][0]['id_shop']) : array();

        } else {
            $this->_display = 'add';
            $id_shop = Tools::getValue('cat_shop_association')?Tools::getValue('cat_shop_association'):array();
        }

        $fields_form = array(
            'form' => array(
                'tinymce' => TRUE,
                'legend' => array(
                    'title' => !empty($id_block) ? $this->l('Edit criterion') : $this->l('Add new criterion'),
                    'icon' => !empty($id_block) ? 'icon-edit' : 'icon-plus-square'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Criterion name'),
                        'name' => 'name',
                        'id' => 'name',
                        'lang' => TRUE,
                        'required' => TRUE,
                        'size' => 50,
                        'maxlength' => 50,
                    ),

                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Description'),
                        'name' => 'description',
                        'autoload_rte' => FALSE,
                        'lang' => TRUE,
                        'rows' => 5,
                        'cols' => 40,
                        'desc' => $this->l('If you do not want see description - just leave this field empty'),
                        'hint' => $this->l('If you do not want see description - just leave this field empty'),
                    ),

                   array(
                        'type' => 'cms_shop_association',
                        'label' => $this->l('Shop association'),
                        'name' => 'cat_shop_association',
                        'values'=>Shop::getShops(),
                        'selected_data'=>$id_shop,
                        'required' => TRUE,
                        'is17'=>(version_compare(_PS_VERSION_, '1.7', '>')?1:0),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $this->l('Status'),
                        'name' => 'active',
                        'required' => FALSE,
                        'class' => 't',
                        'is_bool' => TRUE,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'buttons' => array(
                    'cancelBlock' => array(
                        'title' => $this->l('Cancel'),
                        'href' => $back.'&'.$this->name.'criteriaset'.$this->_prefix_shop_reviews.'=1',
                        'icon' => 'process-icon-cancel'
                    )
                ),
                'submit' => array(
                    'name' => ((!$id_block)?'submit':'update_item'),
                    'title' => ((!$id_block)?$this->l('Save'):$this->l('Update')),
                )
            )
        );


        $helper = new HelperForm();
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = $this->initToolbarShopReviews();
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValuesShopReviewCriteriaSettings(array('id_block'=>$id_block)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        if (Tools::isSubmit('id')
            //&& Tools::isSubmit('editspmgsnipreview')
        ) {
            $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&id=' . $id_block .'&'.$this->name.'criteriaset'.$this->_prefix_shop_reviews.'=1';
            $helper->submit_action = 'editcriteriasettings'.$this->_prefix_shop_reviews.'';
        }else {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name.'&'.$this->name.'criteriaset'.$this->_prefix_shop_reviews.'=1';
            $helper->submit_action = 'addcriteriasettings'.$this->_prefix_shop_reviews.'';
        }

        return $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValuesShopReviewCriteriaSettings($data_in){
        $id = $data_in['id_block'];

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();
        $_data = $obj_storereviews->getReviewCriteriaItem(array('id'=>(int)$id));

        $item_data_lng = isset($_data['item']['data'])?$_data['item']['data']:array();
        $active = isset($_data['item'][0]['active'])?$_data['item'][0]['active']:0;

        $languages = Language::getLanguages(false);
        $fields_name = array();
        $fields_description = array();

        foreach ($languages as $lang)
        {
            $fields_name[$lang['id_lang']] = isset($item_data_lng[$lang['id_lang']]['name'])?$item_data_lng[$lang['id_lang']]['name']:Tools::getValue('name_'.$lang['id_lang']);
            $fields_description[$lang['id_lang']] = isset($item_data_lng[$lang['id_lang']]['description'])?$item_data_lng[$lang['id_lang']]['description']:Tools::getValue('description_'.$lang['id_lang']);
        }

        $config_array = array(
            'name' => $fields_name,
            'description' => $fields_description,
            'active' => $active,
        );

        return $config_array;
    }

    private function _displayShopReviewcriteriaGrid16(){
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');

        ## add information ##
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();

        $_data = $obj_storereviews->getReviewCriteriaItems(array('start' => 0,'step'=>1000));
        $_items = $_data['items'];

        if(sizeof($_items)>0) {

            foreach ($_items as $_k=>$_item) {
                ## languages ##
                $ids_lng = isset($_item['ids_lng']) ? $_item['ids_lng'] : array();
                $lang_for_item = array();
                foreach ($ids_lng as $lng_id) {
                    $data_lng = Language::getLanguage($lng_id);
                    $lang_for_item[] = $data_lng['iso_code'];
                }
                $lang_for_item = implode(",", $lang_for_item);

                $_items[$_k]['ids_lng'] = $lang_for_item;
                ## languages ##

                ## shops ##
                $ids_shops = explode(",", $_item['id_shop']);

                $shops = Shop::getShops();
                $name_shop = array();
                foreach ($shops as $_shop) {
                    $id_shop_lists = $_shop['id_shop'];
                    if (in_array($id_shop_lists, $ids_shops))
                        $name_shop[] = $_shop['name'];
                }

                $name_shop = implode(", ", $name_shop);

                $_items[$_k]['id_shop'] = $name_shop;
                ## shops ##

            }
        }
        ## add information ##

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Review Criteria'),
                    'icon' => 'fa fa-reviews fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'cms_blocks_custom',
                        'label' => $this->l('Review Criteria'),
                        'name' => 'cms_blocks_custom',
                        'values' => $_items,
                        'prefix' => $this->_prefix_shop_reviews,
                    )
                ),
                'buttons' => array(
                    'newBlock' => array(
                        'title' => $this->l('Add New Criterion'),
                        'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;addCriteria'.$this->_prefix_shop_reviews.'',
                        'class' => 'pull-right',
                        'icon' => 'process-icon-new'
                    )
                )
            )
        );


        $this->_display = 'index';

        $helper = new HelperForm();
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = $this->initToolbarShopReviews();
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = '';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        return $helper->generateForm(array($fields_form));
    }

    public function initToolbarShopReviews()
    {
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (!isset($back) || empty($back)) {
            $back = $current_index . '&amp;configure=' . $this->name . '&token=' . $token . '&'.$this->name.'criteriaset'.$this->_prefix_shop_reviews.'=1';
        } else {
            $back = $back.'&'.$this->name.'criteriaset'.$this->_prefix_shop_reviews.'=1';
        }

        switch ($this->_display)
        {
            case 'add':
                $this->toolbar_btn_shop['cancel'] = array(
                    'href' => $back,
                    'desc' => $this->l('Cancel')
                );
                break;
            case 'edit':
                $this->toolbar_btn_shop['cancel'] = array(
                    'href' => $back,
                    'desc' => $this->l('Cancel')
                );
                break;
            case 'index':
                $this->toolbar_btn_shop['new'] = array(
                    'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;addCriteria'.$this->_prefix_shop_reviews.'&'.$this->name.'criteriaset'.$this->_prefix_shop_reviews.'=1',
                    'desc' => $this->l('Add New Criterion')
                );
                break;
            default:
                break;
        }
        return $this->toolbar_btn_shop;
    }

    public function _csvImportExport(){
        include_once(_PS_MODULE_DIR_ . $this->name .'/classes/csvhelpti.class.php');
        $obj = new csvhelpti();
        $data_fields = $obj->getAvailableFields();


        require_once(_PS_MODULE_DIR_ . $this->name . '/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs();
        $export_store_url = $data_seo_url['export_store_url'];

        $delimeter_rewrite = "&";
        if(Configuration::get('PS_REWRITING_SETTINGS')){
            $delimeter_rewrite = "?";
        }

        $this->_data_translate_custom['_csvImportExport_csv_imp_exp_set'] = $this->l('CSV import/export Settings');
        $this->_data_translate_custom['_csvImportExport_you_must_respect'] = $this->l('You must respect the following rules to upload the CSV Reviews correctly');
        $this->_data_translate_custom['_csvImportExport_click_here_to_download'] = $this->l('Click here to download an example of CSV file (you can write your reviews directly in it)');
        $this->_data_translate_custom['_csvImportExport_save_your_file'] = $this->l('Save your file in CSV format. If you use Open Office, choose the option "Field separator: semi-colon"');
        $this->_data_translate_custom['_csvImportExport_csv_import'] = $this->l('CSV Import');
        $this->_data_translate_custom['_csvImportExport_import_rev'] = $this->l('Import reviews');
        $this->_data_translate_custom['_csvImportExport_csv_export'] = $this->l('CSV Export');
        $this->_data_translate_custom['_csvImportExport_export_all_rev'] = $this->l('Export all reviews');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }

    public function _shopcustomerreminderstat(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/featureshelptestim.class.php');
        $obj = new featureshelptestim();

        $start_date_orders = Tools::getValue('start_date_orders'.$this->_prefix_shop_reviews);
        $end_date_orders = Tools::getValue('end_date_orders'.$this->_prefix_shop_reviews);

        $end_date = empty($end_date_orders)?date('Y-m-d H:i:s',strtotime("+1 day")):$end_date_orders;
        $start_date = empty($start_date_orders)?date('Y-m-d H:i:s',strtotime("-1 week")):$start_date_orders;

        $delayed_posts_data = $obj->getOrdersForReminder(array('start_date'=>$start_date,'end_date'=>$end_date));
        $delayed_posts = $delayed_posts_data['result'];
        $count_all = $delayed_posts_data['count_all'];

        return $this->drawShopReminderTasks(array('posts'=>$delayed_posts,'count_all'=>$count_all,
            'start_date'=>$start_date,'end_date'=>$end_date));
    }

    public function drawShopReminderTasks($data){

        $cookie = $this->context->cookie;
        $id_lang = (int)($cookie->id_lang);

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/featureshelptestim.class.php');
        $obj = new featureshelptestim();

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();


        require_once(_PS_MODULE_DIR_ . $this->name . '/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
        $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs();
        $ajax_url = $data_seo_url['ajax_url'];


        $this->_data_translate_custom['drawShopReminderTasks_cust_rem_stat'] = $this->l('Customer Reminder Statistics');
        $this->_data_translate_custom['drawShopReminderTasks_send_ema_only'] = $this->l('Send emails only for the orders with the current selected status');
        $this->_data_translate_custom['drawShopReminderTasks_en_dis_send_a_rev_rem'] = $this->l('Enable or Disable Send a review reminder email to customers');
        $this->_data_translate_custom['drawShopReminderTasks_yes'] = $this->l('Yes');
        $this->_data_translate_custom['drawShopReminderTasks_no'] = $this->l('No');
        $this->_data_translate_custom['drawShopReminderTasks_delay_for_send_rem'] = $this->l('Delay for sending reminder by email');
        $this->_data_translate_custom['drawShopReminderTasks_days'] = $this->l('days');
        $this->_data_translate_custom['drawShopReminderTasks_en_dis_send_a_rev_rem_em_to_cust_sec_time'] = $this->l('Enable or Disable Send a review reminder email to customers a second time?');
        $this->_data_translate_custom['drawShopReminderTasks_days_after_the_first_em_were_sent'] = $this->l('Days after the first emails were sent');
        $this->_data_translate_custom['drawShopReminderTasks_en_dis_already_wr_rev'] = $this->l('Enable or Disable Send a review reminder email to customer when customer already write review in shop?');
        $this->_data_translate_custom['drawShopReminderTasks_start_date'] = $this->l('start date');
        $this->_data_translate_custom['drawShopReminderTasks_end_date'] = $this->l('end date');
        $this->_data_translate_custom['drawShopReminderTasks_fil_orders'] = $this->l('Filter orders');
        $this->_data_translate_custom['drawShopReminderTasks_id'] = $this->l('ID');
        $this->_data_translate_custom['drawShopReminderTasks_ref'] = $this->l('Reference');
        $this->_data_translate_custom['drawShopReminderTasks_ad_date'] = $this->l('Adding date');
        $this->_data_translate_custom['drawShopReminderTasks_ord_status'] = $this->l('Order status');
        $this->_data_translate_custom['drawShopReminderTasks_shop'] = $this->l('Shop');
        $this->_data_translate_custom['drawShopReminderTasks_em_sent_once'] = $this->l('Email sent once?');
        $this->_data_translate_custom['drawShopReminderTasks_em_sent_twice'] = $this->l('Email sent twice?');
        $this->_data_translate_custom['drawShopReminderTasks_rev_alr_wr'] = $this->l('Review already written?');
        $this->_data_translate_custom['drawShopReminderTasks_send_ord_man'] = $this->l('Send order manually');
        $this->_data_translate_custom['drawShopReminderTasks_are_sure_send_ord_man'] = $this->l('Are you sure to want Send order manually ');
        $this->_data_translate_custom['drawShopReminderTasks_there_is_no_order'] = $this->l('There is no orders for customer reminder');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }

    private function _emailsubjects16(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();

        $data = array(
            'prefix'=>$this->_prefix_shop_reviews,

            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title'=>$this->l('Emails subjects settings'),
            'yes_title' =>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'is_emrem'=>$this->l('Enable or Disable reminder email'),
            'em_desc1'=>$this->l('You can customize the subject of the e-mail here.'),
            'em_desc2'=>$this->l('If you wish to customize the e-mail message itself, you will need to manually edit the files in the'),
            'em_desc3'=>$this->l(' folder inside the'),
            'em_desc4'=>$this->l('module folder, for each language, both the text and the HTML version each time. '),
            'em_desc5'=>$this->l('FTP Location'),
            'emrem'=>$this->l('Email reminder subject'),
            'is_reminderok'=>$this->l('Enable or Disable admin confirmation email, when emails requests on the reviews was successfully sent'),
            'reminderok'=>$this->l('Admin confirmation subject, when emails requests on the reviews was successfully sent'),
            'is_thankyou'=>$this->l('Enable or Disable thank you email'),
            'thankyou'=>$this->l('Thank you subject'),
            'is_newtest'=>$this->l('Enable or Disable new Store review email'),
            'newtest'=>$this->l('New Store review subject'),
            'is_resptest'=>$this->l('Enable or Disable response on the Store review email'),
            'resptest'=>$this->l('Response on the Store review subject'),
            'is_revvoucr'=>$this->l('Enable or Disable You submit a review and get voucher for discount email'),
            'revvoucr'=>$this->l('You submit a review and get voucher for discount subject'),

            'update_title'=>$this->l('Update Settings'),
        );
        return $obj_shopreviews->emailsubjects16($data);

    }

    private function _shopcustomerremindersettings16(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();

        $data = array(
            'prefix'=>$this->_prefix_shop_reviews,

            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title'=>$this->l('Customer Reminder settings'),
            'yes_title' =>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'reminder_title'=>$this->l('Enable or Disable Send a review reminder by email to customers'),
            'reminder_desc1'=>$this->l('If activated, when a customer purchases a product on your shop, an e-mail will be sent to him after X days (specify below after selecting "yes" here) to invite him to rate the shop.'),
            'reminder_desc2'=>$this->l('IMPORTANT NOTE'),
            'reminder_desc3'=>$this->l('This requires to set a CRON task on your server. '),
            'reminder_desc4'=>$this->l('CRON HELP STORE REVIEWS'),
            'reminder_desc5'=>$this->l('Your CRON URL to call'),
            'crondelay_title'=>$this->l('Delay between each email in seconds'),
            'crondelay_desc'=>$this->l('The delay is intended in order to your server is not blocked the email function'),
            'cronnpost_title'=>$this->l('Number of emails for each cron call'),
            'cronnpost_desc'=>$this->l('This will reduce the load on your server. The more powerful your server - the more emails you can do for each CRON call!'),
            'delay_title'=> $this->l('Delay for sending reminder by email'),
            'delay_desc'=>$this->l('We recommend you enter at least 7 days here to have enough time to process the order and for the customer to receive it.'),
            'orders_import_storereviews_title'=> $this->l('Import your old orders'),
            'orders_import_storereviews_desc'=>$this->l('Please select a date. All orders placed between that start date and end date (today) will be imported.'),
            'sel_statuses_title'=> $this->l('Send emails only for the orders with the current selected status'),
            'sel_statuses_desc'=>$this->l('This feature prevents customers to leave a review for orders that they haven\'t received yet. You must choose at least one status.'),
            'remrevsec_title'=>$this->l('Send a review reminder by email to customer when customer already write review in shop?'),
            'remindersec_title'=>$this->l('Send a review reminder by email to customers a second time?'),
            'remindersec_desc'=>$this->l('This feature allows you to send a second time the opinion request emails that have already been sent.'),
            'delaysec_title'=>$this->l('Days after the first emails were sent'),

            'update_title'=>$this->l('Update Settings'),
        );
        return $obj_shopreviews->shopcustomerremindersettings16($data);
    }

    private function _mainsettings16(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();

        $data = array(
            'prefix'=>$this->_prefix_shop_reviews,

            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title'=>$this->l('Main Settings'),
            'yes_title' =>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'is_storerev_title'=> $this->l('Enable or Disable Store reviews functional on your site'),
            'perpage_title'=>$this->l('Store reviews per Page:'),
            'perpagemy_title' => $this->l('Store reviews per Page in the My account:'),
            'whocanadd_title' =>$this->l('Who can add review?'),
            'rswitch_lng_title'=>$this->l('MULTILANGUAGE. Separates different languages comments depended on the language selected by the customer (e.g. only English comments on the English site)'),
            'is_avatar_title'=>$this->l('Enable Avatar in the submit form'),
            'is_captcha_title'=>$this->l('Enable Captcha in the submit form'),
            'is_web_title'=>$this->l('Enable Web address in the submit form:'),
            'is_company_title'=>$this->l('Enable Company in the submit form:'),
            'is_addr_title'=>$this->l('Enable Address in the submit form:'),
            'is_country_title'=>$this->l('Enable Country in the submit form:'),
            'is_city_title'=>$this->l('Enable City in the submit form:'),
            'is_files_title'=>$this->l('Enable "Files" field in the submit form:'),
            'ruploadfiles_title'=>$this->l('Number of files user can add for shop review'),
            'is_sortf_title'=> $this->l('Enable or Disable Sort by on the Store reviews page functional'),
            'is_sortfu_title' =>$this->l('Enable or Disable Sort by for Store reviews on the User page functional'),

            'title1'=>$this->l('Filter by Ratings settings'),
            'is_filterall_title'=>$this->l('Enable or Disable Filter by Ratings on the Store reviews page'),

            'title2'=>$this->l('RSS Feed'),
            'rssontestim_title'=> $this->l('Enable or Disable RSS Feed:'),
            'n_rssitemst_title'=>$this->l('Number of items in RSS Feed:'),

            'update_title'=>$this->l('Update Settings'),
        );
        return $obj_shopreviews->shopreviews16($data);
    }

    private function _userprofileg16(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/userprofileg.class.php');
        $obj_userprofileg = new userprofileg();

        $data = array(
            'prefix_shop_reviews'=>$this->_prefix_shop_reviews,
            'prefix_review'=>$this->_prefix_review,
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'd_eff_shopu_effects'=>$this->_effects(array('value' => 'd_eff_shopu')),

            'title'=>$this->l('Main Settings'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'uprofile_title'=>$this->l('User profile'),
            'is_uprof_title'=>$this->l('Enable or Disable User profile'),
            'is_uprof_desc'=>$this->l('Enable or Disable User profile functional on your site'),
            'is_proftab'=>$this->l('Enable or Disable Profile tab on the user page'),
            'rshoppers_blc'=> $this->l('The number of shoppers in the "Block Users":'),
            'rproft_left'=>$this->l('Position "Block Users":'),
            'radv_left'=> $this->l('Left column'),
            'radv_right'=>$this->l('Right column'),
            'radv_footer'=>$this->l('Footer'),
            'radv_home'=> $this->l('Home'),
            'rpage_shoppers'=>$this->l('Users per page in the list view:'),
            'perpageuti'=>$this->l('Store reviews per Page on the User Page:'),
            'perpageur'=> $this->l('Product reviews per Page on the User Page:'),
            'owl_title'=>$this->l('OWL carousels for Block Users block in the Left, Right, Footer positions settings'),
            'sr_slideru'=>$this->l('Enable or Disable Slider for Block Users block in the Left, Right, Footer positions'),
            'sr_slu'=>$this->l('Displayed number of items in the slider in the block Block Users block in the Left, Right, Footer positions'),
            'owl_title_home'=>$this->l('OWL carousel for block Block Users block on the Home Page settings'),
            'sr_sliderhu'=>$this->l('Enable or Disable Slider for Block Users block on the Home Page'),
            'sr_slhu'=>$this->l('Displayed number of items in the slider in the block Block Users block on the Home Page'),
            'wow_title'=>$this->l('WOW effects for items in the list view settings'),
            'd_eff_shopu'=>$this->l('Display effect in the list view on the All Users page'),


            'update_title'=>$this->l('Update Settings'),
        );
        return $obj_userprofileg->userprofileg16($data);
    }

    private function _autoposts16(){
        #### posts api ###
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/postshelp.class.php');
        $postshelp = new postshelp();

        return $postshelp->postsSettings(array('translate'=>$this->_translate));
        #### posts api ###
    }

    public function psvkform16($data_in){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/postshelp.class.php');
        $postshelp = new postshelp();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'translate'=>$data_in,
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),

            'update_title'=>$this->l('Update Settings'),
        );
        return $postshelp->psvkform16($data);
    }

    public function pstwitterform16($data_in){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/postshelp.class.php');
        $postshelp = new postshelp();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'translate'=>$data_in,
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),

            'update_title'=>$this->l('Update Settings'),
        );
        return $postshelp->pstwitterform16($data);
    }


    private function getVoucherTranslation(){
        return array(
            'title'=>$this->l('Voucher settings, when a user add review'),
            'title_fb'=>$this->l('Voucher settings, when a user share review on the Facebook'),
            'vis_on_title'=>$this->l('Enable or Disable Voucher'),
            'vis_on_desc'=>$this->l('Enable or Disable Voucher, when a user add review'),
            'vis_on_desc_fb'=>$this->l('Enable or Disable Voucher, when a user share review on the Facebook'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'ids_groups_title'=>$this->l('Voucher will be displayed only for selected Customers Groups'),
            'coupondesc_title'=>$this->l('Coupon Description'),
            'coupondesc_desc'=>$this->l('The description is displayed in cart once your customers use their voucher.'),
            'vouchercode_title'=>$this->l('Voucher code'),
            'vouchercode_desc'=>$this->l('Voucher code prefix. It must be at least 3 letters long. Prefix voucher code will be used in the first part of the coupon code, which the user will use to get a discount'),
            'discount_type_title'=>$this->l('Discount Type'),
            'discount_type1'=>$this->l('Percentages'),
            'discount_type2'=>$this->l('Currency'),
            'tax_title'=>$this->l('Tax'),
            'tax1'=>$this->l('Tax Excluded'),
            'tax2'=>$this->l('Tax Included'),
            'percentage_val'=>$this->l('Voucher percentage'),
            'isminamount'=>$this->l('Minimum checkout'),
            'is_show_min_title'=>$this->l('Display the minimum amount in the Front Office in the add review form ?'),
            'is_show_min_desc'=>$this->l('Use to show or not the minimum amount on the Front office in the add review form'),
            'select_cat_title'=>$this->l('Select categories'),
            'select_cat_desc'=>$this->l('Check all box(es) of categories to which the discount is to be applied. No categories checked will apply the voucher on all of them.'),
            'sdvvalid_title'=>$this->l('Voucher validity'),
            'sdvvalid_desc'=>$this->l('Voucher term of validity in days'),
            'highlight_title'=>$this->l('Highlight'),
            'highlight_desc'=>$this->l('If the voucher is not yet in the cart, it will be displayed in the cart summary.'),
            'cumulativeother_title'=>$this->l('Cumulative with others vouchers'),
            'cumulativereduc_title'=>$this->l('Cumulative with price reductions'),

            'save_title'=>$this->l('Save'));
    }

    private function _voucherwhensharereviewsettings16(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        // select categories
        $_selected_cat = Configuration::get($this->name.'catboxfb');
        $select_categories = explode(",",$_selected_cat);


        $categories = array();
        foreach ($select_categories as $key => $category) {
            $categories[] = $category;
        }

        $root = Category::getRootCategory();
        $tree = new HelperTreeCategories('associated-categories-tree-fb', 'Associated categories when a user share review on the Facebook');
        $tree->setTemplate('tree_associated_categories.tpl')
            ->setHeaderTemplate('tree_associated_header.tpl')
            ->setTemplateDirectory(dirname(__FILE__).'/views/templates/admin/_configure/helpers/tree/')
            ->setRootCategory((int)$root->id)
            ->setInputName('categoryBoxfb')
            ->setUseCheckBox(true)
            ->setUseSearch(true)
            ->setSelectedCategories($categories);
        $selected_categories = $tree->render();


        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),
            'selected_categories'=>$selected_categories,
        );

        $data_translate_custom_voucher = $this->getVoucherTranslation();

        $data = array_merge($data,$data_translate_custom_voucher);

        return $obj_spmgsnipreviewdraw->voucherwhensharereviewsettings16($data);
    }

    private function _voucherwhenaddreviewsettings16(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        // select categories
        $_selected_cat = Configuration::get($this->name.'catbox');
        $select_categories = explode(",",$_selected_cat);


        $categories = array();
        foreach ($select_categories as $key => $category) {
            $categories[] = $category;
        }

        $root = Category::getRootCategory();
        $tree = new HelperTreeCategories('associated-categories-tree', 'Associated categories');
        $tree->setTemplate('tree_associated_categories.tpl')
            ->setHeaderTemplate('tree_associated_header.tpl')
            ->setTemplateDirectory(dirname(__FILE__).'/views/templates/admin/_configure/helpers/tree/')
            ->setRootCategory((int)$root->id)
            ->setUseCheckBox(true)
            ->setUseSearch(true)
            ->setSelectedCategories($categories);
        $selected_categories = $tree->render();


        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'selected_categories'=>$selected_categories,
        );

        $data_translate_custom_voucher = $this->getVoucherTranslation();

        $data = array_merge($data,$data_translate_custom_voucher);

        return $obj_spmgsnipreviewdraw->voucherwhenaddreviewsettings16($data);
    }

    private function _customerreminder(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),
            'prefix_review'=>$this->_prefix_review,
            'token_cron'=>$this->getokencron(),
            'url_multishop'=>$this->getURLMultiShop(),

            'title_main'=>$this->l('Customer Reminder settings'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'reminder_title'=>$this->l('Enable or Disable Send a review reminder by email to customers'),
            'reminder_desc1'=>$this->l('If activated, when a customer purchases a product on your shop, an e-mail will be sent to him after X days (specify below after selecting "yes" here) to invite him to rate the product.'),
            'reminder_desc2'=>$this->l('IMPORTANT NOTE'),
            'reminder_desc3'=>$this->l('This requires to set a CRON task on your server. '),
            'reminder_desc4'=>$this->l('CRON HELP PRODUCT REVIEWS'),
            'reminder_desc5'=>$this->l('Your CRON URL to call'),
            'crondelay_title'=>$this->l('Delay between each email in seconds'),
            'crondelay_desc'=>$this->l('The delay is intended in order to your server is not blocked the email function'),
            'cronnpost_title'=>$this->l('Number of emails for each cron call'),
            'cronnpost_desc'=>$this->l('This will reduce the load on your server. The more powerful your server - the more emails you can do for each CRON call!'),
            'delay_title'=> $this->l('Delay for sending reminder by email'),
            'delay_desc'=>$this->l('We recommend you enter at least 7 days here to have enough time to process the order and for the customer to receive it.'),
            'orders_import_storereviews_title'=> $this->l('Import your old orders'),
            'orders_import_storereviews_desc'=>$this->l('Please select a date. All orders placed between that start date and end date (today) will be imported.'),
            'sel_statuses_title'=> $this->l('Send emails only for the orders with the current selected status'),
            'sel_statuses_desc'=>$this->l('This feature prevents customers to leave a review for orders that they haven\'t received yet. You must choose at least one status.'),
            'remrevsec_title'=>$this->l('Send a review reminder by email to customer when customer already write review in shop?'),
            'remindersec_title'=>$this->l('Send a review reminder by email to customers a second time?'),
            'remindersec_desc'=>$this->l('This feature allows you to send a second time the opinion request emails that have already been sent.'),
            'delaysec_title'=>$this->l('Days after the first emails were sent'),

            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->customerreminder($data);
    }

    private function _responseadminemailsDesc($data){
        $name_template = $data['name'];


        $this->_data_translate_custom['_responseadminemailsDesc_you_can'] = $this->l('You can customize the subject of the e-mail here.');
        $this->_data_translate_custom['_responseadminemailsDesc_if_you_wish'] = $this->l('If you wish to customize the e-mail message itself, you will need to manually edit the files in the');
        $this->_data_translate_custom['_responseadminemailsDesc_folder_inside'] = $this->l(' folder inside the');
        $this->_data_translate_custom['_responseadminemailsDesc_mod_folder'] = $this->l('module folder, for each language, both the text and the HTML version each time. ');
        $this->_data_translate_custom['_responseadminemailsDesc_ftp_loc'] = $this->l('FTP Location');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }

    private function _responseadminemails(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),
            'prefix_review'=>$this->_prefix_review,

            'title_main'=>$this->l('Emails subjects settings'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'is_emailreminder_title'=>$this->l('Enable or Disable reminder email'),
            'emailreminder_title'=>$this->l('Email reminder subject'),
            'emailreminder_desc'=>$this->_responseadminemailsDesc(array('name'=>'customer-reminderserg')),
            'is_reminderok'=>$this->l('Enable or Disable admin confirmation email, when emails requests on the reviews was successfully sent email'),
            'reminderok_title'=>$this->l('Admin confirmation subject, when emails requests on the reviews was successfully sent'),
            'reminderok_desc'=>$this->_responseadminemailsDesc(array('name'=>'customer-reminder-admin-r')),
            'is_thankyou'=>$this->l('Enable or Disable thank you for your review email'),
            'thankyou_title'=>$this->l('Thank you for your review subject'),
            'thankyou_desc'=>$this->_responseadminemailsDesc(array('name'=>'review-thank-you-r')),
            'is_newrev'=>$this->l('Enable or Disable new Review email'),
            'newrev_title'=>$this->l('New Review subject'),
            'newrev_desc'=>$this->_responseadminemailsDesc(array('name'=>'reviewserg')),
            'is_subresem'=>$this->l('Enable or Disable suggest to change review email'),
            'subresem_title'=>$this->l('Suggest to change review subject'),
            'subresem_desc'=>$this->_responseadminemailsDesc(array('name'=>'reviewserg-suggest-to-change')),
            'textresem_title'=>$this->l('Default content of the suggest to change review email'),
            'textresem_desc'=>$this->l('Templates for sending emails when people leave bad reviews and you wish to contact the user and try to convince a user to change his review / rating'),
            'is_modrev'=>$this->l('Enable or Disable email, when one of your customers has modified own product review'),
            'modrev_title'=>$this->l('One of your customers has modified own product review subject'),
            'modrev_desc'=>$this->_responseadminemailsDesc(array('name'=>'reviewserg-customer-change-review')),
            'is_subpubem'=>$this->l('Enable or Disable notification email when a review is published'),
            'subpubem_title'=>$this->l('Notification email when a review is published subject'),
            'subpubem_desc'=>$this->_responseadminemailsDesc(array('name'=>'reviewserg-publish')),
            'is_abuserev'=>$this->l('Enable or Disable, when someone send abuse for review email'),
            'abuserev_title'=>$this->l('Someone send abuse for review subject'),
            'abuserev_desc'=>$this->_responseadminemailsDesc(array('name'=>'abuseserg')),
            'is_revvouc'=>$this->l('Enable or Disable submit a review and get voucher for discount email'),
            'revvouc_title'=>$this->l('You submit a review and get voucher for discount subject'),
            'revvouc_desc'=>$this->_responseadminemailsDesc(array('name'=>'voucherserg')),
            'is_facvouc'=>$this->l('Enable or Disable You share review on Facebook and get voucher for discount email'),
            'facvouc_title'=>$this->l('You share review on Facebook and get voucher for discount subject'),
            'facvouc_desc'=>$this->_responseadminemailsDesc(array('name'=>'voucherserg')),
            'is_sugvouc'=>$this->l('Enable or Disable Share your review on Facebook and get voucher for discount email'),
            'sugvouc_title'=>$this->l('Share your review on Facebook and get voucher for discount subject'),
            'sugvouc_desc'=>$this->_responseadminemailsDesc(array('name'=>'voucherserg-suggest')),

            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->responseadminemails($data);
    }

    private function _reviewsemails(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title_main'=>$this->l('Reviews emails settings'),
            'mail'=>$this->l('Email administrator for notifications'),
            'noti_title'=> $this->l('Email notifications'),
            'noti_desc'=>$this->l('Email notifications when customer add review / rating or review is reported as an abuse'),
            'img_size_em_title'=>$this->l('Image size for products'),
            'img_size_em_desc'=>$this->l('The emails will contain a photo of each product.'),

            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->reviewsemails($data);
    }

    private function _reviews16(){

        $this->_data_translate_custom['_reviews16_gl_set'] = $this->l('Global settings');
        $this->_data_translate_custom['_reviews16_pr_page'] = $this->l('Product page');
        $this->_data_translate_custom['_reviews16_rev_man'] = $this->l('Reviews management');
        $this->_data_translate_custom['_reviews16_rev_crit'] = $this->l('Review Criteria');
        $this->_data_translate_custom['_reviews16_cust_acc_rev_p'] = $this->l('Customer account reviews page');
        $this->_data_translate_custom['_reviews16_wow_eff'] = $this->l('WOW effects for items in the list view settings');
        $this->_data_translate_custom['_reviews16_owl_car'] = $this->l('OWL carousels for Last Reviews Block settings');
        $this->_data_translate_custom['_reviews16_last_rev_block'] = $this->l('Last Reviews Block');
        $this->_data_translate_custom['_reviews16_stars_in_cat_and_search_p'] = $this->l('Stars in Category and Search pages');
        $this->_data_translate_custom['_reviews16_rev_rss_feed'] = $this->l('Reviews RSS Feed');
        $this->_data_translate_custom['_reviews16_import_prod_com'] = $this->l('Import Product Comments');
        $this->_data_translate_custom['_reviews16_g_pr_rev_feed'] = $this->l('Google Product Review Feeds for Google Shopping');
        $this->_data_translate_custom['_reviews16_voucher_set_add_rev'] = $this->l('Voucher settings, when a user add review');
        $this->_data_translate_custom['_reviews16_voucher_set_share_rev_on_f'] = $this->l('Voucher settings, when a user share review on the Facebook');
        $this->_data_translate_custom['_reviews16_rev_em_set'] = $this->l('Reviews emails settings');
        $this->_data_translate_custom['_reviews16_em_subj_set'] = $this->l('Emails subjects settings');
        $this->_data_translate_custom['_reviews16_cust_rem_set'] = $this->l('Customer Reminder settings');
        $this->_data_translate_custom['_reviews16_cust_rem_stat'] = $this->l('Customer Reminder Statistics');
        $this->_data_translate_custom['_reviews16_cron_help_p_rev'] = $this->l('CRON HELP PRODUCT REVIEWS');
        $this->_data_translate_custom['_reviews16_csv_imp_exp_pr'] = $this->l('CSV import/export product reviews settings');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }


    private function _owlcarousels(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title_main'=>$this->l('OWL carousels for Last Reviews block in the Left, Right, Footer positions settings'),
            'sr_sliderr'=>$this->l('Enable or Disable Slider for Last Reviews block in the Left, Right, Footer positions'),
            'sr_slr'=>$this->l('Displayed number of items in the slider in the block Last Reviews block in the Left, Right, Footer positions'),
            'title_main_home'=>$this->l('OWL carousel for block Last Reviews block on the Home Page settings'),
            'sr_sliderhr'=>$this->l('Enable or Disable Slider for Last Reviews block on the Home Page'),
            'sr_slhr'=>$this->l('Displayed number of items in the slider in the block Last Reviews block on the Home Page'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->owlcarousels($data);
    }

    private function _woweffects(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title_main'=>$this->l('WOW effects for items in the list view settings'),
            'd_eff_rev'=>$this->_effects(array('value' => 'd_eff_rev')),
            'd_eff_rev_title'=>$this->l('Display effect in the list view for Reviews on the Product page'),
            'd_eff_rev_my'=>$this->_effects(array('value' => 'd_eff_rev_my')),
            'd_eff_rev_my_title'=>$this->l('Display effect for Reviews in the My account'),
            'd_eff_rev_u'=>$this->_effects(array('value' => 'd_eff_rev_u')),
            'd_eff_rev_u_title'=> $this->l('Display effect in the tab Product Reviews on the User Page'),
            'd_eff_rev_all'=>$this->_effects(array('value' => 'd_eff_rev_all')),
            'd_eff_rev_all_title'=>$this->l('Display effect in the list view for All Reviews page'),

            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->woweffects($data);
    }

    public function _csvImportExportProductReviews(){
        include_once(_PS_MODULE_DIR_ . $this->name .'/classes/csvhelpr.class.php');
        $obj = new csvhelpr();
        $data_fields = $obj->getAvailableFields();
        $_html = '';

        require_once(_PS_MODULE_DIR_ . $this->name . '/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs();
        $export_product_url = $data_seo_url['export_product_url'];

        $delimeter_rewrite = "&";
        if(Configuration::get('PS_REWRITING_SETTINGS')){
            $delimeter_rewrite = "?";
        }

        $this->_data_translate_custom['_csvImportExportProductReviews_you_must_resp'] = $this->l('You must respect the following rules to upload the CSV Product Reviews correctly');
        $this->_data_translate_custom['_csvImportExportProductReviews_csv_imp_exp'] = $this->l('CSV import/export product reviews settings');
        $this->_data_translate_custom['_csvImportExportProductReviews_click_downl'] = $this->l('Click here to download an example of CSV file (you can write your reviews directly in it)');
        $this->_data_translate_custom['_csvImportExportProductReviews_save_in_csv'] = $this->l('Save your file in CSV format. If you use Open Office, choose the option "Field separator: semi-colon"');
        $this->_data_translate_custom['_csvImportExportProductReviews_csv_imp'] = $this->l('CSV Import');
        $this->_data_translate_custom['_csvImportExportProductReviews_imp_rev'] = $this->l('Import reviews');
        $this->_data_translate_custom['_csvImportExportProductReviews_csv_exp'] = $this->l('CSV Export');
        $this->_data_translate_custom['_csvImportExportProductReviews_exp_all_rev'] = $this->l('Export all reviews');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }

    public function _customerreminderstat(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/featureshelp.class.php');
        $obj = new featureshelp();

        $start_date_orders = Tools::getValue('start_date_orders');
        $end_date_orders = Tools::getValue('end_date_orders');

        $end_date = empty($end_date_orders)?date('Y-m-d H:i:s',strtotime("+1 day")):$end_date_orders;
        $start_date = empty($start_date_orders)?date('Y-m-d H:i:s',strtotime("-1 week")):$start_date_orders;

        $delayed_posts_data = $obj->getOrdersForReminder(array('start_date'=>$start_date,'end_date'=>$end_date));
        $delayed_posts = $delayed_posts_data['result'];
        $count_all = $delayed_posts_data['count_all'];

        return $this->drawDelayedPosts(array('posts'=>$delayed_posts,'count_all'=>$count_all,
            'start_date'=>$start_date,'end_date'=>$end_date));
    }

    public function drawDelayedPosts($data){
        $cookie = $this->context->cookie;
        $id_lang = (int)($cookie->id_lang);

        include_once(_PS_MODULE_DIR_ . $this->name.'/classes/featureshelp.class.php');
        $obj = new featureshelp();

        include_once(_PS_MODULE_DIR_ . $this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj_shopreviews = new spmgsnipreviewhelp();

        $data_seo_url = $obj_shopreviews->getSEOURLs();
        $reviews_admin_url = $data_seo_url['reviews_url'];

        $this->_data_translate_custom['drawDelayedPosts_cust_rem_stat'] = $this->l('Customer Reminder Statistics');
        $this->_data_translate_custom['drawDelayedPosts_send_em_with_sel_stat'] = $this->l('Send emails only for the orders with the current selected status');
        $this->_data_translate_custom['drawDelayedPosts_send_a_rev_rem_by_em'] = $this->l('Enable or Disable Send a review reminder by email to customers');
        $this->_data_translate_custom['drawDelayedPosts_yes'] = $this->l('Yes');
        $this->_data_translate_custom['drawDelayedPosts_no'] = $this->l('No');
        $this->_data_translate_custom['drawDelayedPosts_del_for_send_rem_by_em'] = $this->l('Delay for sending reminder by email');
        $this->_data_translate_custom['drawDelayedPosts_days'] = $this->l('days');
        $this->_data_translate_custom['drawDelayedPosts_send_rem_second_time'] = $this->l('Enable or Disable Send a review reminder by email to customers a second time?');
        $this->_data_translate_custom['drawDelayedPosts_days_after_first_em'] = $this->l('Days after the first emails were sent');
        $this->_data_translate_custom['drawDelayedPosts_already_write_rev'] = $this->l('Enable or Disable Send a review reminder by email to customer when customer already write review in shop?');
        $this->_data_translate_custom['drawDelayedPosts_start_date'] = $this->l('start date');
        $this->_data_translate_custom['drawDelayedPosts_end_date'] = $this->l('end date');
        $this->_data_translate_custom['drawDelayedPosts_fil_orders'] = $this->l('Filter orders');
        $this->_data_translate_custom['drawDelayedPosts_id'] = $this->l('ID');
        $this->_data_translate_custom['drawDelayedPosts_ref'] = $this->l('Reference');
        $this->_data_translate_custom['drawDelayedPosts_add_date'] = $this->l('Adding date');
        $this->_data_translate_custom['drawDelayedPosts_ord_stat'] = $this->l('Order status');
        $this->_data_translate_custom['drawDelayedPosts_shop'] = $this->l('Shop');
        $this->_data_translate_custom['drawDelayedPosts_em_sent_once'] = $this->l('Email sent once?');
        $this->_data_translate_custom['drawDelayedPosts_em_sent_twice'] = $this->l('Email sent twice?');
        $this->_data_translate_custom['drawDelayedPosts_rev_alredy_wr'] = $this->l('Review already written?');
        $this->_data_translate_custom['drawDelayedPosts_send_ord_man'] = $this->l('Send order manually');
        $this->_data_translate_custom['drawDelayedPosts_are_sure_send_ord'] = $this->l('Are you sure to want Send order manually ');
        $this->_data_translate_custom['drawDelayedPosts_no_ord'] =$this->l('There is no orders for customer reminder');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();
        return $_html;
    }

    private $_criterion_error =  0;
    private function _reviewcriteria(){
        if (Tools::isSubmit('addCriteria') || Tools::isSubmit('editspmgsnipreview') || $this->_criterion_error) {
            return $this->_displayAddReviewcriteriaForm16();
        } else {
            return $this->_displayReviewcriteriaGrid16();
        }
    }

    private function _displayAddReviewcriteriaForm16(){
        $token = Tools::getAdminTokenLite('AdminModules');
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        $current_index = AdminController::$currentIndex;
        if (!isset($back) || empty($back))
            $back = $current_index.'&amp;configure='.$this->name.'&token='.$token;

        $id_block = Tools::getValue('id');

        if (Tools::isSubmit('id') && Tools::isSubmit('editspmgsnipreview'))
        {
            $this->_display = 'edit';

            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
            $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
            $_data = $obj_spmgsnipreviewhelp->getReviewCriteriaItem(array('id'=>(int)$id_block));

            $id_shop = isset($_data['item'][0]['id_shop']) ? explode(",",$_data['item'][0]['id_shop']) : array();

            $id_category = isset($_data['item'][0]['id_category'])?$_data['item'][0]['id_category']:'';
            $selected_cat = $id_category;
            $is_category = isset($_data['item'][0]['is_category'])?$_data['item'][0]['is_category']:0;

            $id_pr = isset($_data['item'][0]['id_product'])?$_data['item'][0]['id_product']:'';
            $selected_products = (($id_pr!=0) ? $obj_spmgsnipreviewhelp->getProducts($id_pr) : array());
        } else {
            $this->_display = 'add';
            $id_shop = Tools::getValue('cat_shop_association')?Tools::getValue('cat_shop_association'):array();

            $ids_products = Tools::getValue("inputAccessories");
            if($ids_products){

                include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
                $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

                $ids_products = explode("-",$ids_products,-1);
                $ids_products = implode(",",$ids_products);
                $selected_products = (($ids_products!=0) ? $obj_spmgsnipreviewhelp->getProducts($ids_products) : array());

            } else {
                $selected_products = array();
            }
            $is_category = 0;
            $id_category = Tools::getValue('categoryBoxc');
            $selected_cat = $id_category;
        }

        /* design of the categories with checkbox buttons */
        $select_categories = explode(",",$selected_cat);

        $categories = array();
        foreach ($select_categories as $key => $category) {
            $categories[] = $category;
        }

        $root = Category::getRootCategory();
        $tree = new HelperTreeCategories('associated-categories-tree-c', 'Associated categories');
        $tree->setTemplate('tree_associated_categories.tpl')
            ->setHeaderTemplate('tree_associated_header.tpl')
            ->setTemplateDirectory(dirname(__FILE__).'/views/templates/admin/_configure/helpers/tree/')
            ->setRootCategory((int)$root->id)
            ->setInputName('categoryBoxc')
            ->setUseCheckBox(true)
            ->setUseSearch(true)
            ->setSelectedCategories($categories);
        $selected_categories = $tree->render();

        /* design of the categories with checkbox buttons */
        $fields_form = array(
            'form' => array(
                'tinymce' => TRUE,
                'legend' => array(
                    'title' => !empty($id_block) ? $this->l('Edit criterion') : $this->l('Add new criterion'),
                    'icon' => !empty($id_block) ? 'icon-edit' : 'icon-plus-square'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Criterion name'),
                        'name' => 'name',
                        'id' => 'name',
                        'lang' => TRUE,
                        'required' => TRUE,
                        'size' => 50,
                        'maxlength' => 50,
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $this->l('Description'),
                        'name' => 'description',
                        //'required' => TRUE,
                        'autoload_rte' => FALSE,
                        'lang' => TRUE,
                        'rows' => 5,
                        'cols' => 40,
                        'desc' => $this->l('If you do not want see description - just leave this field empty'),
                        'hint' => $this->l('If you do not want see description - just leave this field empty'),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $this->l('Application scope of the criterion'),
                        'name' => 'criterion_type',
                        'class' => 'fixed-width-xxl',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '1',
                                    'name' => $this->l('Restricted to some products')
                                ),
                                array(
                                    'id' => '2',
                                    'name' => $this->l('Restricted to some categories'),
                                ),
                                array(
                                    'id' => '3',
                                    'name' => $this->l('Valid for the entire catalog'),
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'cms_categories',
                        'label' => $this->l('Select categories:'),
                        'hint' => $this->l('Check all box(es) of categories to which the products in categories is to be applied.'),
                        'desc' => $this->l('Check all box(es) of categories to which the products in categories is to be applied.'),
                        'name' => 'select_cat',
                        'values'=> $selected_categories,
                    ),
                    array(
                        'type' => 'text_custom_criteria',
                        'label' => $this->l('Select products:'),
                        'name' => 'selected_products',
                        'id' => 'selected_products',
                        'required' => FALSE,
                        'size' => 50,
                        'maxlength' => 50,
                        'selected_products' => $selected_products,
                    ),
                    array(
                        'type' => 'cms_shop_association',
                        'label' => $this->l('Shop association'),
                        'name' => 'cat_shop_association',
                        'values'=>Shop::getShops(),
                        'selected_data'=>$id_shop,
                        'required' => TRUE,
                        'is17'=>(version_compare(_PS_VERSION_, '1.7', '>')?1:0),

                    ),
                    array(
                        'type' => 'switch',
                        'label' => $this->l('Status'),
                        'name' => 'active',
                        'required' => FALSE,
                        'class' => 't',
                        'is_bool' => TRUE,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $this->l('Enabled')
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $this->l('Disabled')
                            )
                        ),
                    ),
                ),
                'buttons' => array(
                    'cancelBlock' => array(
                        'title' => $this->l('Cancel'),
                        'href' => $back.'&'.$this->name.'criteriaset=1',
                        'icon' => 'process-icon-cancel'
                    )
                ),
                'submit' => array(
                    'name' => ((!$id_block)?'submit':'update_item'),
                    'title' => ((!$id_block)?$this->l('Save'):$this->l('Update')),
                )
            )
        );

        $helper = new HelperForm();
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = $this->initToolbar();
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $this->getPathUri(),
            'fields_value' => $this->getConfigFieldsValuesReviewCriteriaSettings(array('id_block'=>$id_block,'is_category'=>$is_category,'selected_products'=>$selected_products)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        if (Tools::isSubmit('id')
            //&& Tools::isSubmit('editspmgsnipreview')
            ) {
            $helper->currentIndex = AdminController::$currentIndex . '&configure=' . $this->name . '&id=' . $id_block .'&'.$this->name.'criteriaset=1';
            $helper->submit_action = 'editcriteriasettings';
        }else {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false) . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name.'&'.$this->name.'criteriaset=1';
            $helper->submit_action = 'addcriteriasettings';
        }

        $criterion_type = 3;
        if(count($selected_products)>0){
            $criterion_type = 1;
        }
        if($is_category){
            $criterion_type = 2;
        }

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'_js.phtml');
        $_html = ob_get_clean();

        return $_html.$helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValuesReviewCriteriaSettings($data_in){
        $id = $data_in['id_block'];

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
        $_data = $obj_spmgsnipreviewhelp->getReviewCriteriaItem(array('id'=>(int)$id));

        $item_data_lng = isset($_data['item']['data'])?$_data['item']['data']:array();
        $active = isset($_data['item'][0]['active'])?$_data['item'][0]['active']:0;

        $languages = Language::getLanguages(false);
        $fields_name = array();
        $fields_description = array();

        foreach ($languages as $lang)
        {
            $fields_name[$lang['id_lang']] = isset($item_data_lng[$lang['id_lang']]['name'])?$item_data_lng[$lang['id_lang']]['name']:Tools::getValue('name_'.$lang['id_lang']);
            $fields_description[$lang['id_lang']] = isset($item_data_lng[$lang['id_lang']]['description'])?$item_data_lng[$lang['id_lang']]['description']:Tools::getValue('description_'.$lang['id_lang']);
        }

        $is_category = $data_in['is_category'];
        $selected_products = $data_in['selected_products'];

        $criterion_type = 3;
        if(count($selected_products)>0){
            $criterion_type = 1;
        }
        if($is_category){
            $criterion_type = 2;
        }

        $config_array = array(
            'name' => $fields_name,
            'description' => $fields_description,
            'active' => $active,
            'criterion_type'=>$criterion_type,
        );

        return $config_array;
    }

    private function _displayReviewcriteriaGrid16(){
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');

        ## add information ##
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $_data = $obj_spmgsnipreviewhelp->getReviewCriteriaItems(array('start' => 0,'step'=>1000));
        $_items = $_data['items'];

        if(sizeof($_items)>0) {

            foreach ($_items as $_k=>$_item) {
                ## languages ##
                $ids_lng = isset($_item['ids_lng']) ? $_item['ids_lng'] : array();
                $lang_for_item = array();
                foreach ($ids_lng as $lng_id) {
                    $data_lng = Language::getLanguage($lng_id);
                    $lang_for_item[] = $data_lng['iso_code'];
                }
                $lang_for_item = implode(",", $lang_for_item);

                $_items[$_k]['ids_lng'] = $lang_for_item;
                ## languages ##

                ## shops ##
                $ids_shops = explode(",", $_item['id_shop']);

                $shops = Shop::getShops();
                $name_shop = array();
                foreach ($shops as $_shop) {
                    $id_shop_lists = $_shop['id_shop'];
                    if (in_array($id_shop_lists, $ids_shops))
                        $name_shop[] = $_shop['name'];
                }

                $name_shop = implode(", ", $name_shop);

                $_items[$_k]['id_shop'] = $name_shop;
                ## shops ##

                $id_product_tmp = explode(",", $_item['id_product']);
                $id_product_tmp = implode(", ", $id_product_tmp);

                $_items[$_k]['id_product'] = $id_product_tmp;
                $id_cat_tmp = explode(",", $_item['id_category']);
                $id_cat_tmp = implode(", ", $id_cat_tmp);
                $_items[$_k]['id_category'] = $id_cat_tmp;
            }
        }
        ## add information ##

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Review Criteria'),
                    'icon' => 'fa fa-reviews fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'cms_blocks_custom_product_reviews',
                        'label' => $this->l('Review Criteria'),
                        'name' => 'cms_blocks_custom',
                        'values' => $_items,
                        'prefix' => '',
                    )
                ),
                'buttons' => array(
                    'newBlock' => array(
                        'title' => $this->l('Add New Criterion'),
                        'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;addCriteria',
                        'class' => 'pull-right',
                        'icon' => 'process-icon-new'
                    )
                )
            )
        );

        $this->_display = 'index';

        $helper = new HelperForm();
        $helper->toolbar_scroll = true;
        $helper->toolbar_btn = $this->initToolbar();
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $this->identifier;
        $helper->submit_action = '';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        return $helper->generateForm(array($fields_form));
    }

    public function initToolbar()
    {
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (!isset($back) || empty($back)) {
            $back = $current_index . '&amp;configure=' . $this->name . '&token=' . $token . '&'.$this->name.'criteriaset=1';
        } else {
            $back = $back.'&'.$this->name.'criteriaset=1';
        }
        switch ($this->_display)
        {
            case 'add':
                $this->toolbar_btn['cancel'] = array(
                    'href' => $back,
                    'desc' => $this->l('Cancel')
                );
                break;
            case 'edit':
                $this->toolbar_btn['cancel'] = array(
                    'href' => $back,
                    'desc' => $this->l('Cancel')
                );
                break;
            case 'index':
                $this->toolbar_btn['new'] = array(
                    'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;addCriteria&'.$this->name.'criteriaset=1',
                    'desc' => $this->l('Add New Criterion')
                );
                break;
            default:
                break;
        }
        return $this->toolbar_btn;
    }

    private function _groductFeed(){

        require_once(_PS_MODULE_DIR_ . $this->name . '/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs();
        $reviews_admin_url = $data_seo_url['reviews_admin_url'];

        $delimeter_rewrite = "&";
        if(Configuration::get('PS_REWRITING_SETTINGS')){
            $delimeter_rewrite = "?";
        }
        $url_cron = $data_seo_url['cron_reviews_url'];

        $this->_data_translate_custom['_groductFeed_g_pr_feed'] = $this->l('Google Product Review Feeds for Google Shopping');
        $this->_data_translate_custom['_groductFeed_g_pr_feed_let_cont'] = $this->l('Google Product Review Feeds let content providers send product reviews to');
        $this->_data_translate_custom['_groductFeed_g_shop'] = $this->l('Google Shopping');
        $this->_data_translate_custom['_groductFeed_g_pr_rat_feed'] = $this->l('Google Product Ratings Feeds');
        $this->_data_translate_custom['_groductFeed_more_inf'] = $this->l('More info');
        $this->_data_translate_custom['_groductFeed_cron_to_call'] = $this->l('Your CRON URL to call');
        $this->_data_translate_custom['_groductFeed_regenerate'] = $this->l('Regenerate Google Product Ratings Feed');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        $_html .= $this->_cronhelp(array('url'=>'cronreviews'));

        return $_html;
    }

    private function _importcomments(){
        $_html = '';
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/importhelp.class.php');
        $obj = new importhelp();

        $this->_data_translate_custom['_importcomments_imp_pr_com'] = $this->l('Import Product Comments');
        $this->_data_translate_custom['_importcomments_your_database'] = $this->l('Your database no contains Product comments for imports');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }

    private function _rssfeed(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title_main'=>$this->l('Reviews RSS Feed'),
            'rsson'=>$this->l('Enable or Disable RSS Feed'),
            'rssname'=>$this->l('Title of your RSS Feed'),
            'rssdesc'=>$this->l('Description of your RSS Feed'),
            'number_rssitems'=>$this->l('Number of items in RSS Feed'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),

            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->rssfeed($data);
    }

    private function _starslistandsearch(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $this->_data_translate_custom['_starslistandsearch_faq'] = $this->l('Frequently Asked Questions');

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'starslistandsearch_faq'=>$this->_data_translate_custom['_starslistandsearch_faq'],
            'faqStars17_spmgsnipreview'=>$this->_faqStars17_spmgsnipreview(),

            'title_main'=>$this->l('Stars in Category and Search pages'),
            'starscat'=>$this->l('Enable or Disable stars on the category and search pages'),
            'is_starscat'=>$this->l('Hide stars when product no has reviews on the category and search pages'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),

            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->starslistandsearch($data);
    }


    private function _lastreviewsblock(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'customhookhelp'=>$this->_customhookhelp(),
            'title_main'=>$this->l('Last Reviews Block'),
            'is_blocklr'=>$this->l('Enable or Disable Block "Last Reviews"'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),

            'block_last_reviews'=>$this->l('Display block on the following pages'),
            'blocklr_home'=>$this->l('for home page'),
            'blocklr_cat'=>$this->l('for each category page'),
            'blocklr_man'=>$this->l('for each manufacturer/brand page'),
            'blocklr_prod'=>$this->l('for each product page'),
            'blocklr_oth'=>$this->l('for other pages'),
            'blocklr_chook'=>$this->l('for CUSTOM HOOK pages'),
            'top'=>$this->l('Top'),
            'bottom'=>$this->l('Bottom'),
            'leftcol'=>$this->l('Left Column'),
            'rightcol'=>$this->l('Right Column'),
            'home'=>$this->l('Home'),
            'chook'=>$this->l('Custom Hook'),

            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->lastreviewsblock($data);
    }

    private function _customhookhelp(){

        $this->_data_translate_custom['_customhookhelp_faq'] = $this->l('Frequently Asked Questions');
        $this->_data_translate_custom['_customhookhelp_c_h_help'] = $this->l('CUSTOM HOOK HELP:');
        $this->_data_translate_custom['_customhookhelp_cms'] = $this->l('How I can show block last reviews on a single page (CMS or blog for example) ?');
        $this->_data_translate_custom['_customhookhelp_just_place'] = $this->l('You just need to add a line of code to the tpl file of the page where you want to add the block last reviews.');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }

    private function _customeraccountreviewspage(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title_main'=>$this->l('Customer account reviews page'),
            'revperpagecus'=>$this->l('Number of reviews per page on Customer account page'),

            'update_title'=>$this->l('Update Settings'),
        );

        return $obj_spmgsnipreviewdraw->customeraccountreviewspage($data);
    }

    private function _reviewsmanagement(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'title_main'=>$this->l('Reviews management'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'is_approval'=>$this->l('Require Admin Approval'),
            'whocanadd'=>$this->l('Who can add review?'),
            'is_onerev'=>$this->l('The user can add more one review'),
            'rswitch_lng'=>$this->l('MULTILANGUAGE. Separates different languages comments depended on the language selected by the customer (e.g. only English comments on the English site)'),
            'is_sortallf'=>$this->l('Enable or Disable Sort by on the All Reviews page functional'),
            'is_sortfu'=>$this->l('Enable or Disable Sort by for Product Reviews on the User page functional'),
            'revperpageall'=>$this->l('Number of reviews per page on All Reviews page'),

            'update_title'=>$this->l('Update Settings'),

        );

        return $obj_spmgsnipreviewdraw->reviewsmanagement($data);
    }

    private function _productpage(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();


        $this->_data_translate_custom['_productpage_read'] = $this->l('Read Step 10 "How to configure product tabs for Prestashop 1.7.x.x ?" in the');

        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'productpage_read'=>$this->_data_translate_custom['_productpage_read'],
            'title_main'=>$this->l('Product page Settings'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'ptabs_type_title'=>$this->l('Product tabs'),
            'ptabs_type1'=>$this->l('Standard theme without Tabs'),
            'ptabs_type2'=>$this->l('Custom theme with tabs on product page for Prestashop 1.6'),
            'ptabs_type3'=>$this->l('Custom theme with tabs on product page for Prestashop 1.7'),
            'ptabs_type_desc1'=>$this->l('On a standard PrestaShop 1.6 and PrestaShop 1.7 theme, the product page no longer has tabs for the various sections.'),
            'ptabs_type_desc2'=>$this->l('But some custom themes have added back tabs on the product page. '),
            'is_sortf'=>$this->l('Enable or Disable Sort by on the product page functional'),
            'is_helpfulf'=>$this->l('Enable or Disable Helpful review functional'),
            'is_abusef'=>$this->l('Enable or Disable Report abuse functional'),
            'select_stars_title'=>$this->l('Style stars'),
            'select_stars_desc'=>$this->l('Choose your style for the stars icons'),
            'starratingon'=>$this->l('Enable or Disable stars for each reviews'),
            'hooktodisplay_title'=>$this->l('Hook to display block with ratings, number of reviews etc'),
            'extra_right'=>$this->l('displayLeftColumnProduct (Extra right)'),
            'extra_left'=>$this->l('displayRightColumnProduct (Extra Left)'),
            'product_actions'=>$this->l('displayProductButtons (Product actions)'),
            'product_footer'=>$this->l('displayFooterProduct (Product footer)'),
            'none'=>$this->l('None'),
            'hooktodisplay_desc'=>$this->l('Block with ratings, number of reviews, link to post a new review'),
            'revperpage'=>$this->l('Number of reviews per page on Product page'),
            'soc_buttons_title'=>$this->l('Social Buttons'),
            'rsoc_on_title'=>$this->l('Enable or Disable Social buttons'),
            'rsoc_on_desc'=>$this->l('Enable or Disable Social buttons for each Product Review'),
            'rsoccount_on'=>$this->l('Display count box'),
            'soc_buttons_desc1'=>$this->l('You can configure "Voucher, when a user share review on the Facebook" in the'),
            'soc_buttons_desc2'=>$this->l('Voucher Settings'),
            'soc_buttons_desc3'=>$this->l('Tab'),

            'update_title'=>$this->l('Update Settings'),

        );

        return $obj_spmgsnipreviewdraw->productpage($data);
    }

    private function _global(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();


        $data = array(
            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'prefix_review'=>$this->_prefix_review,

            'title_main'=>$this->l('Global Settings'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'rvis_on'=>$this->l('Enable or Disable Product Reviews and Ratings'),
            'ratings_on_title'=>$this->l('Enable or Disable "Rating/Review Criteria" field(s)'),
            'ratings_on_desc1'=>$this->l('You also can add'),
            'ratings_on_desc2'=>$this->l('Review Criteria'),
            'ratings_on_desc3'=>$this->l('If you delete/disable all Review criteria will be displayed only Rating field in Write Your Review form'),
            'is_avatarr' =>$this->l('Enable or Disable "Avatar" field'),
            'is_filesr'=>$this->l('Enable or Disable "Files" field'),
            'ruploadfiles'=>$this->l('Number of files user can add for review'),
            'title_on'=>$this->l('Enable or Disable "Title" field'),
            'text_on'=>$this->l('Enable or Disable "Text" field'),
            'rminc'=>$this->l('Minimum chars the user must write in the Text field for add review'),
            'ip_on_title'=>$this->l('Enable or Disable "IP" or "City and Country Name" field'),
            'ip_on_desc'=>((version_compare(_PS_VERSION_, '1.7', '>'))?$this->l('City and Country Name will be displayed, if your enable Geolocation in admin panel -> IMPROVE -> International -> Localization -> Geolocation'):$this->l('City and Country Name will be displayed, if your enable Geolocation in admin  panel -> Preferences -> Geolocation')),
            'is_captcha'=>$this->l('Enable or Disable Captcha'),
            'filter_title'=>$this->l('Filter by Ratings settings'),
            'is_filterp'=>$this->l('Enable or Disable Filter by Ratings on the Product page'),
            'is_filterall'=>$this->l('Enable or Disable Filter by Ratings on the All reviews page'),

            'update_title'=>$this->l('Update Settings'),

        );

        return $obj_spmgsnipreviewdraw->global_custom($data);
    }

    private function _blockallinfo(){
        $_block_allinfo = array(
            'type' => 'block_allinfo',
            'label' => $this->l('Display block on the following pages'),
            'name' => 'block_allinfo',
            'values'=> array(
                'allinfo_home' => array('name'=>$this->l('for home page'),
                    'status' => Configuration::get($this->name.'allinfo_home'),
                    'position'=>Configuration::get($this->name.'allinfo_home_pos'),
                    'width' => array('width' => (int)Configuration::get($this->name.'allinfo_home_w'), 'name'=>'allinfo_home_w')),
                'allinfo_cat' => array('name'=>$this->l('for each category page'),
                    'status' => Configuration::get($this->name.'allinfo_cat'),
                    'position'=>Configuration::get($this->name.'allinfo_cat_pos'),
                    'width' => array('width' => (int)Configuration::get($this->name.'allinfo_cat_w'), 'name'=>'allinfo_cat_w')),
                'allinfo_man' => array('name'=>$this->l('for each manufacturer/brand page'),
                    'status' => Configuration::get($this->name.'allinfo_man'),
                    'position'=>Configuration::get($this->name.'allinfo_man_pos'),
                    'width' => array('width' => (int)Configuration::get($this->name.'allinfo_man_w'), 'name'=>'allinfo_man_w')),
            ),
            'available_pos' => array(
                'top'=>$this->l('Top'),
                'bottom'=>$this->l('Bottom'),
                'leftcol'=>$this->l('Left Column'),
                'rightcol'=>$this->l('Right Column'),
            ),
            'available_pos_home' => array(
                'top'=>$this->l('Top'),
                'home'=>$this->l('Home'),
                'bottom'=>$this->l('Bottom'),
                'leftcol'=>$this->l('Left Column'),
                'rightcol'=>$this->l('Right Column'),

            ),
        );
        return $_block_allinfo;
    }

    private function _googlesnippets16(){

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewdraw.class.php');
        $obj_spmgsnipreviewdraw = new spmgsnipreviewdraw();

        $data = array(

            'table' => $this->table,
            'this'=>$this,
            'identifier' => $this->identifier,
            'tab' => $this->tab,
            'uri' => $this->getPathUri(),

            'blockallinfo'=>$this->_blockallinfo(),
            'helpRichPins'=>$this->_helpRichPins(),

            'title_snip'=>$this->l('Google rich snippets settings'),
            'yes_title'=>$this->l('Yes'),
            'no_title'=>$this->l('No'),
            'svis_on_title'=>$this->l('Enable or Disable Google Rich Snippets'),
            'svis_on_desc1'=>$this->l('Snippets based on the'),
            'svis_on_desc2'=>$this->l('Reviews'),
            'svis_on_desc3'=>$this->l('More info'),
            'svis_on_desc4'=>$this->l('AND'),
            'svis_on_desc5'=>$this->l('Aggregate Ratings'),
            'svis_on_desc6'=>$this->l('More info'),

            'breadcrumb_title'=>$this->l('Google Breadcrumbs settings'),
            'breadvis_on'=>$this->l('Enable or Disable Google Breadcrumbs'),
            'breadvis_on_desc'=>$this->l('Breadcrumbs'),

            'summary_title'=>$this->l('Block with summary info Settings'),
            'allinfo_on_desc'=>$this->l('Enable or Disable Block with summary info about product ratings and reviews'),

            'pins_title'=>$this->l('Rich Pins Settings'),
            'pins_desc'=>$this->l('Enable or Disable Rich Pins'),

            'pos_pin_button_title'=>$this->l('Position Pinterest Button'),
            'leftColumn'=>$this->l('Left column, only product page'),
            'rightColumn'=>$this->l('Right column, only product page'),
            'extraLeft'=>$this->l('displayRightColumnProduct (Extra Left)'),
            'extraRight'=>$this->l('displayLeftColumnProduct (Extra right)'),
            'productFooter'=>$this->l('displayFooterProduct (Product footer)'),
            'productActions'=>$this->l('displayProductButtons (Product actions)'),

            'block_radio_buttons_custom'=>$this->l('Pinterest Button style'),

            'update_title'=>$this->l('Update Settings'),

        );

        return $obj_spmgsnipreviewdraw->googlesnippets16($data);

    }

    private function _helpRichPins(){

        $this->_data_translate_custom['_helpRichPins_help'] = $this->l('Help');
        $this->_data_translate_custom['_helpRichPins_to_val'] = $this->l('To validate your shop, please follow these steps');
        $this->_data_translate_custom['_helpRichPins_cr_acc'] = $this->l('Create an account or login to pinterest on');
        $this->_data_translate_custom['_helpRichPins_open_another'] = $this->l('Open another tab in your browser and go to');
        $this->_data_translate_custom['_helpRichPins_insert_url'] = $this->l('Insert the url of one of your shop products in the text field and press "Validate" button');
        $this->_data_translate_custom['_helpRichPins_once_url'] = $this->l('Once your url has been processed you will see a page similar to the screenshot below with the data of your rich pin, now press on the "Apply now" button just below "Validate"');
        $this->_data_translate_custom['_helpRichPins_now_you_will'] = $this->l('Now you will be prompted to insert the domain where the rich pins will be validated and the data format for the rich pins.');
        $this->_data_translate_custom['_helpRichPins_the_domain'] = $this->l('The domain and the data format should be precompiled, just check if the domain is correct and that data format is "HTML Tags" and then click "Apply now"');
        $this->_data_translate_custom['_helpRichPins_the_process'] = $this->l('The process is complete, now it\'s only a matter of time to get your site enabled for rich pins.');
        $this->_data_translate_custom['_helpRichPins_reminder_that'] = $this->l('Remember that you need to validate only 1 pin to enable rich pins on your whole domain.');
        $this->_data_translate_custom['_helpRichPins_all_your_old'] = $this->l('All your old pins will be converted automatically to rich pins when the first pin is verified.');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }

    private function _cronhelp($data = null){
        $url_cron = isset($data['url'])?$data['url']:'';

        include_once(_PS_MODULE_DIR_ . $this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj_shopreviews = new spmgsnipreviewhelp();
        $data_seo_url = $obj_shopreviews->getSEOURLs();

        $delimeter_rewrite = "&";
        if(Configuration::get('PS_REWRITING_SETTINGS')){
            $delimeter_rewrite = "?";
        }

        if($url_cron == 'cron_shop_reviews'){
            $text_type_review = $this->l('STORE');
            $url_cron = $data_seo_url['cron_shop_reviews_url'];
        } else {
            $text_type_review = $this->l('PRODUCT');

            if($url_cron == "cronreviews") {
                $url_cron = $data_seo_url['cron_reviews_url'];
            } else {
                $url_cron = $data_seo_url['cron_url'];
            }
        }

        $this->_data_translate_custom['_cronhelp_cron_help'] = $this->l('CRON HELP');
        $this->_data_translate_custom['_cronhelp_reviews'] = $this->l('REVIEWS');
        $this->_data_translate_custom['_cronhelp_you_can_conf'] = $this->l('You can configure sending email messages through cron. You have 2 possibilities:');
        $this->_data_translate_custom['_cronhelp_you_can_enter'] = $this->l('You can enter the following url in your browser: ');
        $this->_data_translate_custom['_cronhelp_you_can_set'] = $this->l('You can set a cron\'s task (a recursive task that fulfills the sending of reminders)');
        $this->_data_translate_custom['_cronhelp_the_task_run'] = $this->l('The task run every hour');
        $this->_data_translate_custom['_cronhelp_how_to_conf'] = $this->l('How to configure a cron task ?');
        $this->_data_translate_custom['_cronhelp_on_your_serv'] = $this->l('On your server, the interface allows you to configure cron\'s tasks');
        $this->_data_translate_custom['_cronhelp_about_cron'] = $this->l('About CRON');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }
    
    private function _productComments(){
    	include_once(_PS_MODULE_DIR_.$this->name.'/classes/importhelp.class.php');
		$obj = new importhelp();

		$data_comments = $obj->getCountComments();
		
		$is_count_comments = $data_comments['is_count_comments'];
		$count_comments = $data_comments['comments'];

        $this->_data_translate_custom['_productComments_imp_pr_com'] = $this->l('Import Product Comments');
        $this->_data_translate_custom['_productComments_if_you_already'] = $this->l('If you are already using PrestaShop "Product comments" module, you can import all your existing ratings and comments so as not to lose any of your history. ');
        $this->_data_translate_custom['_productComments_you_have'] = $this->l('You have');
        $this->_data_translate_custom['_productComments_com'] = $this->l('comments');
        $this->_data_translate_custom['_productComments_imp_pr_com1'] = $this->l('Import Product comments');
        $this->_data_translate_custom['_productComments_no_com'] = $this->l('Your database no contains Product comments for imports');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }

  private function _help_documentation(){

      $this->_data_translate_custom['_help_documentation_h_d'] = $this->l('Help / Documentation');
      $this->_data_translate_custom['_help_documentation_mod_doc'] = $this->l('MODULE DOCUMENTATION ');
      $this->_data_translate_custom['_help_documentation_gsnip_rich'] = $this->l('GOOGLE RICH SNIPPETS TEST TOOL ');
      $this->_data_translate_custom['_help_documentation_h_to_conf'] = $this->l('Hot to configure CRON FOR PRODUCT REVIEWS ');
      $this->_data_translate_custom['_help_documentation_cron_help_pr'] = $this->l('CRON HELP PRODUCT REVIEWS');
      $this->_data_translate_custom['_help_documentation_cron_help_sr'] = $this->l('CRON HELP STORE REVIEWS');
      $this->_data_translate_custom['_help_documentation_h_to_conf_sr'] = $this->l('Hot to configure CRON FOR STORE REVIEWS ');

      ob_start();
      include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
      $_html = ob_get_clean();

      $_html .= $this->_faq16();

      return $_html;
    }

    private function _faq16(){

        $this->_data_translate_custom['_faq16_faq'] = $this->l('Frequently Asked Questions');
        $this->_data_translate_custom['_faq16_500'] = $this->l('I get 500 Internal Error, when try get Images (Avatar does not load and File upload does not work)');
        $this->_data_translate_custom['_faq16_int_500'] = $this->l('Internal 500 error - this error related with setting of your server.');
        $this->_data_translate_custom['_faq16_pr'] = $this->l('- The problem may be in the access rights to the folder /modules/'.$this->name.'/ Must be 0777 or 0755');
        $this->_data_translate_custom['_faq16_1'] = $this->l('- The problem may be in the .htaccess file in the folder /modules/ . Try delete/rename file .htaccess');
        $this->_data_translate_custom['_faq16_2'] = $this->l('- The problem may be in the index.php file in the folder /modules/ . Try delete/rename file index.php');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }

    private function _faqStars17_spmgsnipreview(){
        $_html = '';
        if(version_compare(_PS_VERSION_, '1.7', '>')) {



            $this->_data_translate_custom['_faqStars17_spmgsnipreview_how'] = $this->l('How can I add Stars in Category and Search pages?');
            $this->_data_translate_custom['_faqStars17_spmgsnipreview_you_just'] = $this->l('You just need to add a line of code in the tpl file /themes/{YOUR_CURRENT_THEME}/templates/catalog/_partials/miniatures/product.tpl.');

            ob_start();
            include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
            $_html = ob_get_clean();

        }

        return $_html;
    }
    
 private function _welcome(){

     $this->_data_translate_custom['_welcome_w'] = $this->l('Welcome');
     $this->_data_translate_custom['_welcome_w_and_thank_y'] = $this->l('Welcome and thank you for purchasing the module.');
     $this->_data_translate_custom['_welcome_to_conf'] = $this->l('To configure module please read');
     $this->_data_translate_custom['_welcome_h_doc'] = $this->l('Help / Documentation');

     ob_start();
     include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
     $data = ob_get_clean();

     return  $data;
    }

    public function renderUserAccount(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/useraccount.tpl');
    }

    public function renderUser(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/user.tpl');
    }

    public function renderUsers(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/users.tpl');
    }

    public function renderListUsers(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/list_users.tpl');
    }

    public function renderMyStoreReviews(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/my-storereviews.tpl');
    }

    public function renderListStoreReviews(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/list_storereviews.tpl');
    }

    public function renderListMyStoreReviews(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/list_mystorereviews.tpl');
    }

    public function renderListMyReviews(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/list_myreviews.tpl');
    }

    public function renderListAllReviews(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/list_allreviews.tpl');
    }

    public function renderListReviews(){
        return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/list_reviews.tpl');
    }

	public function renderMyReviews(){
		return $this->display(__FILE__.'/spmgsnipreview.php', 'views/templates/front/my-reviews.tpl');
	}

    public function renderReviewAbuseForm(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/review-abuse-form.tpl');
    }

    public function renderReviewAbuseAdmin(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/review-abuse-admin.tpl');
    }

    public function renderReviewChangedAdmin(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/review-changed-admin.tpl');
    }

    public function renderReviewChangedMy(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/review-changed-my.tpl');
    }

    public function renderReviewError(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/error.tpl');
    }

    public function renderReviewFacebookSuccess(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/success.tpl');
    }

    public function renderReviewCoupon(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/coupon.tpl');
    }

    public function renderReviewFacebookSuggestion(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/coupon-suggestion.tpl');
    }

    public function renderError(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/error.tpl');
    }

    public function renderSuccess(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/hooks/success.tpl');
    }

    public function renderTplItems(){
        return Module::display(_PS_MODULE_DIR_.$this->name.'/spmgsnipreview.php', 'views/templates/front/storereviews.tpl');
    }

	public function translateCustom(){
		$cookie = $this->context->cookie;
			// set discount
    		switch (Configuration::get($this->name.'discount_type'))
			{
				case 1:
					// percent
					$id_discount_type = 1;
					$value = Configuration::get($this->name.'percentage_val');
					break;
				case 2:
					// currency
					$id_discount_type = 2;
					$id_currency = (int)$cookie->id_currency;
					$value = Configuration::get('sdamount_'.(int)$id_currency);
				break;
				default:
					$id_discount_type = 2;
					$id_currency = (int)$cookie->id_currency;
					$value = Configuration::get('sdamount_'.(int)$id_currency);
			}
			$valuta = "%";
            $tax = '';

            if($id_discount_type == 2){
				$cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
			    foreach ($cur AS $_cur){
		    	if($id_currency == $_cur['id_currency']){
		    			$valuta = $_cur['sign'];
		    		}
		    	}
                $tax = (int)Configuration::get($this->name.'tax');
			}

            /* social share coupon */
            // set discount
            switch (Configuration::get($this->name.'discount_typefb'))
            {
                case 1:
                    // percent
                    $id_discount_type = 1;
                    $valuefb = Configuration::get($this->name.'percentage_valfb');
                    break;
                case 2:
                    // currency
                    $id_discount_type = 2;
                    $id_currency = (int)$cookie->id_currency;
                    $valuefb = Configuration::get('sdamountfb_'.(int)$id_currency);
                    break;
                default:
                    $id_discount_type = 2;
                    $id_currency = (int)$cookie->id_currency;
                    $valuefb = Configuration::get('sdamountfb_'.(int)$id_currency);
            }
            $valutafb = "%";
            $taxfb = '';

            if($id_discount_type == 2){
                $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
                foreach ($cur AS $_cur){
                    if($id_currency == $_cur['id_currency']){
                        $valutafb = $_cur['sign'];
                    }
                }
                $taxfb = (int)Configuration::get($this->name.'taxfb');
            }
            /* social share coupon */
			
		return array('page'=>$this->l('Page'),'reviews'=>$this->l('reviews'),'review'=>$this->l('review'),
					 'firsttext' => $this->l('You get voucher for discount'),
					 'secondtext' => $this->l('Here is you voucher code'),
					 'threetext' => $this->l('It is valid until'),

					 'discountvalue' => $value.$valuta,
                     'discountvaluefb' => $valuefb.$valutafb,

                     'valutafb'=>$valutafb,
                     'valuta' => $valuta,

                     'tax'=>$tax,
                     'taxfb'=>$taxfb,

					 'category' => $this->l('Category'),
					 'product'=> $this->l('Product'),
			         'rate_post'=>$this->l('Rate / post a review for this product on'),
					 'sent_cron_items'=>$this->l('Number of cron items sent'),
					 'delete_cron_items'=>$this->l('Number of cron items deleted'),
					 'no_sent_items'=>$this->l('No tasks sent'),
					 'review_reminder'=>$this->l('Please Enable Review reminder email to customers in admin panel'),

		  		     'review_text_voucher' => $this->l('submit a review'),
                     'facebook_text_voucher' => $this->l('share review on Facebook'),

                     'tax_included' => $this->l('Tax Included'),
                     'tax_excluded' => $this->l('Tax Excluded'),
                     'valid_day' => $this->l('day'),
                     'valid_days' => $this->l('days'),

					 'title'=>$this->l('Title'),
                     'review'=>$this->l('Review'),
                     'reg_customer'=>$this->l('Registered customer'),
                     'no_reg_customer'=>$this->l('Not registered customer'),

                     'helpfull_exists'=>$this->l('You have already voted.'),
                     'helpfull_success' => $this->l('Thank you for your feedback.'),

                     'coupon_success' => $this->l('Thank you for sharing this review on Facebook'),

                     'coupon_suggestion_title' => $this->l('Share your review on Facebook'),
                     'coupon_suggestion_msg' => $this->l('To share your review, simply use the Like button below your review on the product page'),
                     'already_get_coupon' => $this->l('You have already received a coupon with discount'),
                     'expiried_voucher' => $this->l('The validity of your coupon for discount has expired'),
                     'used_voucher' => $this->l('This voucher has already been used'),

                     'all_reviews_meta_title' => $this->l('All Reviews'),
                     'all_reviews_meta_description' => $this->l('All Reviews'),
                     'all_reviews_meta_keywords' => $this->l('All Reviews'),

                     'my_reviews_meta_title' => $this->l('My account - Product Reviews'),
                     'my_reviews_meta_description' => $this->l('My account - Product Reviews'),
                     'my_reviews_meta_keywords' => $this->l('My account - Product Reviews'),

                     'pending_review' => $this->l('The customer has rated the product but has not posted a review, or the review is pending moderation'),

                    'error_login' => $this->l('You must be a registered customer!'),

                    'orders_date_empty' => $this->l('The date is still empty, you should select a date first'),
                    'orders_date_start_more_end' => $this->l('Start date more than end date'),
                    'orders_date_not_exists' => $this->l('There are no orders to import'),
                    'orders_date_ok1' => $this->l('You have successfully imported your'),
                    'orders_date_ok2' => $this->l('old orders'),

                    'google_reviews_title'=>$this->l('Reviews Aggregator'),

                    'myr_msg1'=>$this->l('now active'),
                    'myr_msg2'=>$this->l('now inactive'),
                    'myr_msg3'=>$this->l('Settings updated'),
                    'myr_msg4'=>$this->l("e-mail reminders are"),

                    'ptc_msg1'=>$this->l('Please, enter the security code'),
                    'ptc_msg2'=>$this->l('Please, enter the text'),
                    'ptc_msg3'=>$this->l('Please, enter the name'),
                    'ptc_msg4'=>$this->l('Please, enter the email'),
                    'ptc_msg5'=>$this->l('Please, enter the title'),
                    'ptc_msg6'=>$this->l('Please, choose the rating for'),
                    'ptc_msg7'=>$this->l('Please, choose the rating'),
                    'ptc_msg8'=>$this->l('You entered the wrong security code'),
                    'ptc_msg9'=>$this->l('Please enter a valid email address. For example johndoe@domain.com.'),
                    'ptc_msg10'=>$this->l('You have already add review for this product'),
                    'ptc_msg11'=>$this->l('chars'),
                    'ptc_msg12'=>$this->l('Min'),

                    'ptc_msg13_1'=>$this->l('You can upload a maximum of'),
                    'ptc_msg13_2'=>$this->l('files'),

                    'ava_msg8'=>$this->l('Invalid file type, please try again!'),
                    'ava_msg9'=>$this->l('Wrong file format, please try again!'),

                    'raad_msg1'=>$this->l('Review is NOT Abusive.'),

                    'raf_msg5'=>$this->l('Your report has been taken into account and the merchant has been warned by e-mail.'),
                    'raf_msg8'=>$this->l('You cannot send report for this review because somebody has already posted a report.'),

                    'rca_msg1'=>$this->l('Please, enter your suggestion text'),
                    'rca_msg2'=>$this->l('The changed customer review is pending modification.'),

                    'rcmy_msg5'=>$this->l('Review already changed.'),

                    //for reminder
                    'review_reminder'=>$this->l('Please Enable Customer Reminder in admin panel'),
                    'review_reminder_second'=>$this->l('Please Enable review reminder email to customers a second time in admin panel'),
                    'review_reminder_customer_txt'=>$this->l('Customer already write review in shop'),
                    'review_reminder_customer'=>$this->l('Please Enable Send a review reminder by email to customer when customer already write review in shop in admin panel'),
                    'category' => $this->l('Category'),
                    'product'=> $this->l('Product'),
                    'rate_post'=>$this->l('Rate / post a review for this product on'),
                    'sent_cron_items'=>$this->l('Number of items sent'),
                    'delete_cron_items'=>$this->l('Number of items deleted'),
                    'no_sent_items'=>$this->l('No tasks sent'),
                    'sent_request'=>$this->l('The reviews requests emails have been sent for following orders'),
                    'subject_success_sent_email'=>$this->l('The emails requests on the reviews was successfully sent'),
                    'accepted_order_statuses' => $this->l('Accepted order statuses'),
                    'configure_order_statuses'=>$this->l('Configure order statuses here'),
                    'customer_reminder_settings'=>$this->l('Customer Reminder settings'),
                    'customer_reminder_error1_1'=>$this->l('Have passed less'),
                    'customer_reminder_error1_2'=>$this->l('days from Adding date'),
                    'customer_reminder_error2_2'=>$this->l('days after the first sending'),
                    'configure_reminder_delay_first'=>$this->l('Configure Delay for sending reminder by email here'),
                    'configure_reminder_delay_second'=>$this->l('Configure Days after the first emails were sent here'),
                    //for reminder

                    // for CSV
                    'A_name'=>$this->l('Language ID'),
                    'A_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
                    'B_name'=>$this->l('Rating'),
                    'B_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(1/5).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('5'),
                    'C_name'=>$this->l('Product ID'),
                    'C_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(1,2,3, .. etc).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
                    'D_name'=>$this->l('Customer ID'),
                    'D_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(0 - Guest; 1,2,3, .. etc - Customer).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
                    'E_name'=>$this->l('Customer full name'),
                    'E_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(need fill, required, if Customer ID = 0).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('John Doe'),
                    'F_name'=>$this->l('Customer email'),
                    'F_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(need fill, required, if Customer ID = 0).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('john@doe.com'),
                    'G_name'=>$this->l('Title'),
                    'G_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('Title review'),
                    'H_name'=>$this->l('Message'),
                    'H_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('Test message'),
                    'I_name'=>$this->l('Admin Response'),
                    'I_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('Test response by admin'),
                    'J_name'=>$this->l('Display "Admin response" on the site'),
                    'J_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(0 or 1).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
                    'K_name'=>$this->l('Date Add'),
                    'K_example'=> $this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(dd/mm/yyyy).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.date("d/m/Y"),
                    'L_name'=>$this->l('Status'),
                    'L_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(0 or 1).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
                    // for CSV

                    // user
                    'meta_title_shoppers'=>$this->l('All Users'),
                    'meta_description_shoppers'=>$this->l('All Users'),
                    'meta_keywords_shoppers'=>$this->l('All Users'),
                    'profile'=>$this->l('profile'),
                    'meta_title_myaccount'=>$this->l('My account - User Profile'),
                    'meta_description_myaccount'=>$this->l('My account - User Profile'),
                    'meta_keywords_myaccount'=>$this->l('My account - User Profile'),
                    'male'=>$this->l('Male'),
                    'female'=>$this->l('Female'),
                    // user
        );
	}

    private function _tags_add($data){
        $tag1 = isset($data['tag1'])?$data['tag1']:'';
        $tag1_txt = isset($data['tag1_txt'])?$data['tag1_txt']:'';

        $br_tag1 = isset($data['br_tag1'])?'<'.$data['br_tag1'].'>':'';
        $br_tag2 = isset($data['br_tag2'])?'<'.$data['br_tag2'].'>':'';

        return $br_tag1.$br_tag2.'<'.$tag1.'>'.$tag1_txt.'</'.$tag1.'>';
    }

    public function translateItems(){

        $cookie = $this->context->cookie;
        /* social share coupon */
        // set discount
        switch (Configuration::get($this->name.'discount_type'.$this->_prefix_shop_reviews))
        {
            case 1:
                // percent
                $id_discount_type = 1;
                $valuefb = Configuration::get($this->name.'percentage_val'.$this->_prefix_shop_reviews);
                break;
            case 2:
                // currency
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $valuefb = Configuration::get('sdamount'.$this->_prefix_shop_reviews.'_'.(int)$id_currency);
                break;
            default:
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $valuefb = Configuration::get('sdamount'.$this->_prefix_shop_reviews.'_'.(int)$id_currency);
        }
        $valutafb = "%";
        $taxfb = '';

        if($id_discount_type == 2){
            $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
            foreach ($cur AS $_cur){
                if($id_currency == $_cur['id_currency']){
                    $valutafb = $_cur['sign'];
                }
            }
            $taxfb = (int)Configuration::get($this->name.'tax'.$this->_prefix_shop_reviews);
        }
        /* social share coupon */

        return array(
            'discountvalue'.$this->_prefix_shop_reviews => $valuefb.$valutafb,

            'valuta'.$this->_prefix_shop_reviews => $valutafb,

            'tax'.$this->_prefix_shop_reviews => $taxfb,

            'page'=>$this->l('Page'),
            'subject'=>$this->l('New Store review from Your Customer'),
            'subject_response'=>$this->l('Response on the Store review'),
            'company'=>$this->l('Company'),
            'address'=>$this->l('Address'),
            'country'=>$this->l('Country'),
            'city'=>$this->l('City'),
            'web'=>$this->l('Web address'),
            'message'=>$this->l('Message'),

            'subject_thank_you'=>$this->l('Thank you for your review'),

            //for reminder
            'review_reminder'=>$this->l('Please Enable Review reminder email to customers in admin panel'),
            'review_reminder_second'=>$this->l('Please Enable review reminder email to customers a second time in admin panel'),
            'review_reminder_customer_txt'=>$this->l('Customer already write review in shop'),
            'review_reminder_customer'=>$this->l('Please Enable Send a review reminder by email to customer when customer already write review in shop in admin panel'),
            'category' => $this->l('Category'),
            'product'=> $this->l('Product'),
            'rate_post'=>$this->l('Rate / post a review for this product on'),
            'sent_cron_items'=>$this->l('Number of items sent'),
            'delete_cron_items'=>$this->l('Number of items deleted'),
            'no_sent_items'=>$this->l('No tasks sent'),
            'sent_request'=>$this->l('The reviews requests emails have been sent for following orders'),
            'subject_success_sent_email'=>$this->l('The emails requests on the reviews was successfully sent'),
            'accepted_order_statuses' => $this->l('Accepted order statuses'),
            'configure_order_statuses'=>$this->l('Configure order statuses here'),
            'customer_reminder_settings'=>$this->l('Customer Reminder settings'),
            'customer_reminder_error1_1'=>$this->l('Have passed less'),
            'customer_reminder_error1_2'=>$this->l('days from Adding date'),
            'customer_reminder_error2_2'=>$this->l('days after the first sending'),
            'configure_reminder_delay_first'=>$this->l('Configure Delay for sending reminder by email here'),
            'configure_reminder_delay_second'=>$this->l('Configure Days after the first emails were sent here'),
            //for reminder

            'orders_date_empty' => $this->l('The date is still empty, you should select a date first'),
            'orders_date_start_more_end' => $this->l('Start date more than end date'),
            'orders_date_not_exists' => $this->l('There are no orders to import'),
            'orders_date_ok1' => $this->l('You have successfully imported your'),
            'orders_date_ok2' => $this->l('old orders'),

            'meta_title_testimonials'=>$this->l('My account - Store reviews'),
            'meta_description_testimonials'=>$this->l('My account - Store reviews'),
            'meta_keywords_testimonials'=>$this->l('My account - Store reviews'),

            'meta_title_testimonials_all'=>$this->l('Store reviews'),
            'meta_description_testimonials_all'=>$this->l('Store reviews'),
            'meta_keywords_testimonials_all'=>$this->l('Store reviews'),

            'msg1'=>$this->l('Please, choose the rating.'),
            'msg1_2'=>$this->l('Please, choose the rating for'),
            'msg2'=>$this->l('Please, enter the Name.'),
            'msg3'=>$this->l('Please, enter the Email.'),
            'msg4'=>$this->l('Please, enter the Message.'),
            'msg5'=>$this->l('Please, enter the security code.'),
            'msg6'=>$this->l('Please enter a valid email address. For example johndoe@domain.com.'),
            'msg7'=>$this->l('You entered the wrong security code.'),
            'msg8'=>$this->l('Invalid file type, please try again!'),
            'msg9'=>$this->l('Wrong file format, please try again!'),
            'msg10'=>$this->l('Only customers that have purchased a product can give a review!'),





            // for CSV
            'A_name'=>$this->l('Language ID'),
            'A_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
            'B_name'=>$this->l('Rating'),
            'B_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(1/5).').$this->l('(1/5).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('5'),
            'C_name'=>$this->l('Customer ID'),
            'C_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(0 - Guest; 1,2,3, .. etc - Customer).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
            'D_name'=>$this->l('Customer full name'),
            'D_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(need fill, required, if Customer ID = 0).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('John Doe'),
            'E_name'=>$this->l('Customer email'),
            'E_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(need fill, required, if Customer ID = 0).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('john@doe.com'),
            'F_name'=>$this->l('Message'),
            'F_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('Test message'),
            'G_name'=>$this->l('Admin Response'),
            'G_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('Test response by admin'),
            'H_name'=>$this->l('Display "Admin response" on the site'),
            'H_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(0 or 1).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
            'I_name'=>$this->l('Date Add'),
            'I_example'=> $this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(dd/mm/yyyy).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.date("d/m/Y"),
            'J_name'=>$this->l('Status'),
            'J_example'=>$this->_tags_add(array('tag1'=>'b','tag1_txt'=>$this->l('Format'))).':&nbsp;'.$this->l('(0 or 1).').$this->_tags_add(array('br_tag1','br','br_tag2'=>'br','tag1'=>'b','tag1_txt'=>$this->l('Example'))).':&nbsp;'.$this->l('1'),
            // for CSV
        );
    }
	
	public function hookNewOrder($params)
	{
		return (
			$this->hookOrderConfirmation($params)
		);
	}
	
	public function hookActionValidateOrder($params)
	{
		return (
			$this->hookOrderConfirmation($params)
		);
	}

    public function hookOrderConfirmation($params){

        $this->collectOrdersForProductReviews($params);
        $this->collectOrdersForShopReviews($params);

        return $this->display(dirname(__FILE__).'/spmgsnipreview.php', 'views/templates/hooks/orderconfirmation.tpl');
    }

    private function collectOrdersForShopReviews($params){

        $cookie = $this->context->cookie;

        if(version_compare(_PS_VERSION_, '1.7', '>')){
            $params_obj_order = isset($params['order'])?$params['order']:null;
        } else {
            $params_obj_order = isset($params['objOrder'])?$params['objOrder']:null;
        }

        if (!empty($params_obj_order) && is_object($params_obj_order))
        {
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/featureshelptestim.class.php');
            $obj = new featureshelptestim();

            $guest = false;
            // check if customer is guest
            if (version_compare(_PS_VERSION_, '1.4', '>')) {
                $customer = new Customer($params_obj_order->id_customer);

                if (Validate::isLoadedObject($customer)) {
                    $guest = $customer->isGuest();
                }
                unset($customer);
            }

            if (false === $obj->isDataExist(
                    array('id_shop'=>$this->_id_shop,
                        'order_id'=>$params_obj_order->id
                    )
                )
                &&
                empty($guest)
            ) {

                if (Configuration::get($this->name.'reminder'.$this->_prefix_shop_reviews) && isset($params_obj_order->id_customer) &&
                    is_numeric($params_obj_order->id_customer)
                ) {

                    $status  = $obj->getStatus(
                        array('id_shop'=>$this->_id_shop,
                            'customer_id'=> $params_obj_order->id_customer
                        )
                    );

                    if (false === $status) {
                        $obj->addStatus(
                            array('id_shop'=>$this->_id_shop,
                                'customer_id'=> $params_obj_order->id_customer,
                                'status'=>1
                            )
                        );

                        $add_status = 1;
                    } else {
                        $add_status  = $status;
                    }

                    if (!empty($add_status)) {
                        $id_lang = $cookie->id_lang;
                        $products = $obj->getProductsInOrder(
                            array('order_id'=>$params_obj_order->id,
                                'id_lang' => $id_lang
                            )
                        );

                        if (!empty($products)) {
                            $data = array();
                            foreach ($products as $product) {

                                $product['rate'] = 0;
                                $attributes = Product::getProductProperties($id_lang, $product);
                                $data[] = array('title' => $attributes['name'],
                                    'category' => $attributes['category'],
                                    'link' => $attributes['link'],
                                    'id_lang' => $id_lang,
                                    'id_product' =>(int)$attributes['id_product'],
                                );

                                unset($attributes);
                            }

                            $obj->saveOrder(
                                array('id_shop'=>$this->_id_shop,
                                    'order_id' => $params_obj_order->id,
                                    'customer_id' => $params_obj_order->id_customer,
                                    'data' => $data
                                )
                            );

                            unset($data);
                        }
                    }
                }
            }
        }
    }

    private function collectOrdersForProductReviews($params){
        $cookie = $this->context->cookie;

        if(version_compare(_PS_VERSION_, '1.7', '>')){
            $params_obj_order = isset($params['order'])?$params['order']:null;
        } else {
            $params_obj_order = isset($params['objOrder'])?$params['objOrder']:null;
        }

        if (!empty($params_obj_order) && is_object($params_obj_order))
        {
            include_once(_PS_MODULE_DIR_.$this->name.'/classes/featureshelp.class.php');
            $obj = new featureshelp();

            $guest = false;

            // check if customer is guest
            if (version_compare(_PS_VERSION_, '1.4', '>')) {
                $customer = new Customer($params_obj_order->id_customer);

                if (Validate::isLoadedObject($customer)) {
                    $guest = $customer->isGuest();
                }
                unset($customer);
            }

            if (false === $obj->isDataExist(
                    array('id_shop'=>$this->_id_shop,
                        'order_id'=>$params_obj_order->id
                    )
                )
                &&
                empty($guest)
            ) {

                if (Configuration::get($this->name.'reminder') && isset($params_obj_order->id_customer) &&
                    is_numeric($params_obj_order->id_customer)
                ) {

                    $status  = $obj->getStatus(
                        array('id_shop'=>$this->_id_shop,
                            'customer_id'=> $params_obj_order->id_customer
                        )
                    );

                    if (false === $status) {
                        $obj->addStatus(
                            array('id_shop'=>$this->_id_shop,
                                'customer_id'=> $params_obj_order->id_customer,
                                'status'=>1
                            )
                        );

                        $add_status = 1;
                    } else {
                        $add_status  = $status;
                    }

                    if (!empty($add_status)) {
                        $id_lang = $cookie->id_lang;
                        $products = $obj->getProductsInOrder(
                            array('order_id'=>$params_obj_order->id,
                                'id_lang' => $id_lang
                            )
                        );

                        if (!empty($products)) {
                            $data = array();
                            foreach ($products as $product) {

                                $product['rate'] = 0;
                                $attributes = Product::getProductProperties($id_lang, $product);
                                $data[] = array('title' => $attributes['name'],
                                    'category' => $attributes['category'],
                                    'link' => $attributes['link'],
                                    'id_lang' => $id_lang,
                                    'id_product' =>$attributes['id_product'],
                                );

                                unset($attributes);
                            }

                            $obj->saveOrder(
                                array('id_shop'=>$this->_id_shop,
                                    'order_id' => $params_obj_order->id,
                                    'customer_id' => $params_obj_order->id_customer,
                                    'data' => $data
                                )
                            );

                            unset($data);
                        }
                    }
                }
            }
        }
    }
	
	
	public function hookLeftColumn($params)
	{
        $name_template = "left.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty = $this->context->smarty;
            $this->setLastReviewsBlockSettings(array('custom_page_name'=>'leftcol'));

            ## badges ###
            $this->badges(array('custom_page_name'=>'leftcol'));
            ## badges ###

			$_product_id = Tools::getValue("id_product");

			// pinterest
            $smarty->assign($this->name.'id_productpin', $_product_id);
			$smarty->assign($this->name.'pinvis_on', Configuration::get($this->name.'pinvis_on'));
			$smarty->assign($this->name.'pinterestbuttons', Configuration::get($this->name.'pinterestbuttons'));
			$smarty->assign($this->name.'_leftColumn', Configuration::get($this->name.'_leftColumn'));
			// pinterest

            ## user profile ##
            if(Configuration::get($this->name . $this->_prefix_review . 'adv_left') == 1) {
                $this->setuserprofilegSettings();
            }
            $smarty->assign($this->name . $this->_prefix_review . 'adv_left', Configuration::get($this->name . $this->_prefix_review . 'adv_left'));
            ## user profile ##

            ## store reviews ##
            if(Configuration::get($this->name . 't_left') == 1) {
                $this->setStoreReviewsColumnsSettings();
            }
            $smarty->assign($this->name . 't_left', Configuration::get($this->name . 't_left'));
            ## store reviews ##

            $smarty->assign($this->name.'is_mobile', $this->_is_mobile);
        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
	}
	
	public function hookRightColumn($params)
	{
        $name_template = "right.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty = $this->context->smarty;
            $this->setLastReviewsBlockSettings(array('custom_page_name'=>'rightcol'));

            ## badges ###
            $this->badges(array('custom_page_name'=>'rightcol'));
            ## badges ###
			
			$_product_id = Tools::getValue("id_product");

			// pinterest
            $smarty->assign($this->name.'id_productpin', $_product_id);
			$smarty->assign($this->name.'pinvis_on', Configuration::get($this->name.'pinvis_on'));
			$smarty->assign($this->name.'pinterestbuttons', Configuration::get($this->name.'pinterestbuttons'));
			$smarty->assign($this->name.'_rightColumn', Configuration::get($this->name.'_rightColumn'));
			// pinterest 

            ## user profile ##
            if(Configuration::get($this->name . $this->_prefix_review . 'adv_right') == 1) {
                $this->setuserprofilegSettings();
            }
            $smarty->assign($this->name . $this->_prefix_review . 'adv_right', Configuration::get($this->name . $this->_prefix_review . 'adv_right'));
            ## user profile ##

            ## store reviews ##
            if(Configuration::get($this->name . 't_right') == 1) {
                $this->setStoreReviewsColumnsSettings();
            }
            $smarty->assign($this->name . 't_right', Configuration::get($this->name . 't_right'));
            ## store reviews ##

            $smarty->assign($this->name.'is_mobile', $this->_is_mobile);
        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
				
	}

    public function badges($data){
        $custom_page_name = $data['custom_page_name'];
        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;
        $id_lang = (int)$cookie->id_lang;
        ## badges ##
        $allinfo_on = Configuration::get($this->name.'allinfo_on');
        $svis_on = Configuration::get($this->name.'svis_on');
        $data_badges = array();
        $name = '';
        $rev_all = '';
        ## home page ##

        ## prestashop 1.7 ##
        if(version_compare(_PS_VERSION_, '1.7', '>')){
            $page_name = $smarty->tpl_vars['page']->value['page_name'];
        ## prestashop 1.7 ##
        } else {
            if (defined('_MYSQL_ENGINE_')) {
                $page_name = $smarty->tpl_vars['page_name']->value;
            } else {
                $page_name = $smarty->_tpl_vars['page_name'];
            }
        }
        $is_home = 0;
        if ($page_name == 'index') {
            $is_home = 1;
        }
        ## home page ##
        $id_manufacturer = (int)Tools::getValue('id_manufacturer');
        $id_supplier = (int)Tools::getValue('id_supplier');
        $id_category = (int)Tools::getValue('id_category');

        include_once(_PS_MODULE_DIR_.$this->name . '/classes/spmgsnipreviewhelp.class.php');
        $obj = new spmgsnipreviewhelp();

        if($allinfo_on && $svis_on) {

            if($id_supplier || $id_category || $is_home || $id_manufacturer) {
                $data_badges = $obj->badges(array('id_supplier' => $id_supplier, 'id_category' => $id_category, 'id_manufacturer'=>$id_manufacturer));

                if($data_badges['total_rating'] == 0)
                    $allinfo_on = 0;

                if($id_category){
                    $cat_obj = new Category($id_category);
                    $name = $cat_obj->name[$id_lang];
                }

                if($id_supplier){
                    $sup_obj = new Supplier($id_supplier);
                    $name = $sup_obj->name;
                }

                if($id_manufacturer){
                    $sup_obj = new Manufacturer($id_manufacturer);
                    $name = $sup_obj->name;
                }

                if($is_home){
                    $name = Configuration::get('PS_SHOP_NAME');
                }

                $data_seo_url = $obj->getSEOURLs(array('id_lang'=>$id_lang));
                $rev_all = $data_seo_url['rev_all'];

            } else {
                $data_badges = array();
            }
        }
        /* home page */
        $is_home_custom_page_name =0;

        if($is_home) {
            $prefix_home = 'home';
            if(Configuration::get($this->name . 'allinfo_'.$prefix_home) == "allinfo_".$prefix_home && Configuration::get($this->name . 'allinfo_'.$prefix_home.'_pos') == $custom_page_name){
                $is_home_custom_page_name =1;
                $smarty->assign(
                    array(
                        $this->name . 'allinfoh_w' => Configuration::get($this->name . 'allinfo_'.$prefix_home.'_w'),
                    )
                );

            }
        }
        /* home page */

        /* category page */
        $is_cat_custom_page_name =0;
        if($id_category){
            $prefix_cat = 'cat';
            if(Configuration::get($this->name . 'allinfo_'.$prefix_cat) == "allinfo_".$prefix_cat && Configuration::get($this->name . 'allinfo_'.$prefix_cat.'_pos') == $custom_page_name){
                $is_cat_custom_page_name =1;

                $smarty->assign(
                    array(
                        $this->name . 'allinfoh_w' => Configuration::get($this->name . 'allinfo_'.$prefix_cat.'_w'),
                    )
                );

            }
        }
        /* category page */

        /* manufacturer/brand page */
        $is_man_custom_page_name =0;
        if($id_supplier || $id_manufacturer){
            $prefix_cat = 'man';
            if(Configuration::get($this->name . 'allinfo_'.$prefix_cat) == "allinfo_".$prefix_cat
                && Configuration::get($this->name . 'allinfo_'.$prefix_cat.'_pos') == $custom_page_name){
                $is_man_custom_page_name =1;

                $smarty->assign(
                    array(
                        $this->name . 'allinfoh_w' => Configuration::get($this->name . 'allinfo_'.$prefix_cat.'_w'),
                    )
                );
            }
        }
        /* manufacturer/brand page */

        $smarty->assign($this->name . 'is_home_b_'.$custom_page_name, $is_home_custom_page_name);
        $smarty->assign($this->name . 'is_cat_b_'.$custom_page_name, $is_cat_custom_page_name);
        $smarty->assign($this->name . 'is_man_b_'.$custom_page_name, $is_man_custom_page_name);
        $smarty->assign($this->name.'rev_all', $rev_all);
        $smarty->assign($this->name.'data_badges', $data_badges);
        $smarty->assign($this->name.'badges_name', $name);
        $smarty->assign($this->name.'allinfo_on', $allinfo_on);
        $smarty->assign($this->name.'svis_on', $svis_on);
        ## badges ##
    }

    public function hookTop($params)
    {
        $name_template = "top.tpl";
        //$id_product = (int)Tools::getValue('id_product');$cache_id = $this->name.'|'.$id_product;
        //if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {
            $this->badges(array('custom_page_name' => 'top'));
            $this->basicSettingsHook();
            $this->setLastReviewsBlockSettings(array('custom_page_name' => 'top'));
        /*}
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
        */
        return $this->display(__FILE__, $this->_template_path.$name_template);
    }

	public function hookhome($params)
	{
        $name_template = "home.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {
            $smarty = $this->context->smarty;
            ## badges ###
            $this->badges(array('custom_page_name' => 'home'));
            ## badges ###

            include_once(_PS_MODULE_DIR_.$this->name . '/classes/spmgsnipreviewhelp.class.php');
            $obj = new spmgsnipreviewhelp();
            $data = $obj->getHomeLastReviews(array('start' => 0,
                    'step' => Configuration::get($this->name . 'blocklr_home_ndr')
                )
            );

            $smarty->assign(array($this->name . 'reviews_home' => $data['reviews']));
            $smarty->assign($this->name . 'sr_sliderhr', (int)Configuration::get($this->name . 'sr_sliderhr'));
            $smarty->assign($this->name . 'sr_slhr', (int)Configuration::get($this->name . 'sr_slhr'));

            ### block last reviews ###
            $smarty->assign(
                array(
                    $this->name . 'is_blocklr' => Configuration::get($this->name . 'is_blocklr'),
                    $this->name . 'blocklr_home_pos' => Configuration::get($this->name . 'blocklr_home_pos'),
                    $this->name . 'blocklr_home_w' => Configuration::get($this->name . 'blocklr_home_w'),
                    $this->name . 'blocklr_home' => Configuration::get($this->name . 'blocklr_home'),
                    $this->name . 'blocklr_home_tr' => Configuration::get($this->name . 'blocklr_home_tr'),
                )
            );
            ### block last reviews ###

            $this->basicSettingsHook();
            $smarty->assign($this->name . 'rsson', Configuration::get($this->name . 'rsson'));
            $smarty->assign($this->name . 'is_ps15', $this->_is15);

            $cookie = $this->context->cookie;
            $id_lang = (int)$cookie->id_lang;

            $data_seo_url = $obj->getSEOURLs(array('id_lang' => $id_lang));
            $all = $data_seo_url['rev_all'];
            $smarty->assign($this->name . 'allr_url', $all);

            ## user profile ##
            if(Configuration::get($this->name . $this->_prefix_review . 'adv_home') == 1) {
                $this->setuserprofilegSettings();
            }
            $smarty->assign($this->name . $this->_prefix_review . 'adv_home', Configuration::get($this->name . $this->_prefix_review . 'adv_home'));
            ## user profile ##

            ## store reviews ##
            if(Configuration::get($this->name . 't_home') == 1) {
                $this->setStoreReviewsColumnsSettings();
            }
            $smarty->assign($this->name . 't_home', Configuration::get($this->name . 't_home'));
            ## store reviews ##

        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
	}
	
    public function setLastReviewsBlockSettings($data){

        $custom_page_name = $data['custom_page_name'];
        ## home page ##
        $smarty = $this->context->smarty;

        ## prestashop 1.7 ##
        if(version_compare(_PS_VERSION_, '1.7', '>')){
            $page_name = $smarty->tpl_vars['page']->value['page_name'];
         ## prestashop 1.7 ##
        } else {
            if (defined('_MYSQL_ENGINE_')) {
                $page_name = $smarty->tpl_vars['page_name']->value;
            } else {
                $page_name = $smarty->_tpl_vars['page_name'];
            }
        }
        $is_home = 0;
        if($page_name == 'index'){
            $is_home = 1;
        }
        ## home page ##

        $id_manufacturer = (int)Tools::getValue('id_manufacturer');
        $id_supplier = (int)Tools::getValue('id_supplier');
        $id_category = (int)Tools::getValue('id_category');
        $id_product = (int)Tools::getValue('id_product');

        include_once(_PS_MODULE_DIR_.$this->name . '/classes/spmgsnipreviewhelp.class.php');
        $obj = new spmgsnipreviewhelp();
        $this->basicSettingsHook();
        $smarty->assign($this->name . 'rsson', Configuration::get($this->name . 'rsson'));


        ## avg ##
        /*$avg_rating = $obj->getAvgReview();
        $count_reviews = $obj->getCountReviews();
        $smarty->assign(array(
            $this->name.'count_reviews_block' => $count_reviews,
            $this->name.'avg_rating_block'=>$avg_rating['avg_rating'],
            $this->name.'avg_decimal_block'=>$avg_rating['avg_rating_decimal'],

        ));*/
        ## avg ##

        /* home page */
        $is_home_custom_page_name =0;

        if($is_home) {
            $prefix_home = 'home';
            if(Configuration::get($this->name . 'blocklr_'.$prefix_home) == "blocklr_".$prefix_home && Configuration::get($this->name . 'blocklr_'.$prefix_home.'_pos') == $custom_page_name){
                $is_home_custom_page_name =1;
                $data = $obj->getHomeLastReviews(array('start' => 0,
                                                      'step' => Configuration::get($this->name . 'blocklr_'.$prefix_home.'_ndr')
                    )
                );
                $smarty->assign(array($this->name . 'reviews_block' => $data['reviews']));

                ### block last reviews ###
                $smarty->assign(
                    array(
                        $this->name . 'blocklr_w' => Configuration::get($this->name . 'blocklr_'.$prefix_home.'_w'),
                        $this->name . 'blocklr_tr' => Configuration::get($this->name . 'blocklr_'.$prefix_home.'_tr'),
                    )
                );
                ### block last reviews ###
            }
        }
        /* home page */

        /* category page */
        $is_cat_custom_page_name =0;
        if($id_category){
            $prefix_cat = 'cat';
            if(Configuration::get($this->name . 'blocklr_'.$prefix_cat) == "blocklr_".$prefix_cat && Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_pos') == $custom_page_name){
                $is_cat_custom_page_name =1;

                $data = $obj->getBlockLastReviews(array('start' => 0,
                        'step' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_ndr'),
                        'prefix' => $prefix_cat,
                    )
                );
                $smarty->assign(array($this->name . 'reviews_block' => $data['reviews']));

                ### block last reviews ###
                $smarty->assign(
                    array(
                        $this->name . 'blocklr_w' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_w'),
                        $this->name . 'blocklr_tr' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_tr'),
                    )
                );
                ### block last reviews ###
            }
        }
        /* category page */

        /* manufacturer/brand page */
        $is_man_custom_page_name =0;

        if($id_manufacturer || $id_supplier){

            $prefix_cat = 'man';
            if(Configuration::get($this->name . 'blocklr_'.$prefix_cat) == "blocklr_".$prefix_cat
                && Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_pos') == $custom_page_name){
                $is_man_custom_page_name =1;

                $data = $obj->getBlockLastReviews(array('start' => 0,
                        'step' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_ndr'),
                        'prefix' => $prefix_cat,
                    )
                );
                $smarty->assign(array($this->name . 'reviews_block' => $data['reviews']));

                ### block last reviews ###
                $smarty->assign(
                    array(
                        $this->name . 'blocklr_w' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_w'),
                        $this->name . 'blocklr_tr' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_tr'),
                    )
                );
                ### block last reviews ###
            }
        }
        /* manufacturer/brand page */

        /* product page */
        $is_prod_custom_page_name =0;
        if($id_product){
            $prefix_cat = 'prod';
            if(Configuration::get($this->name . 'blocklr_'.$prefix_cat) == "blocklr_".$prefix_cat && Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_pos') == $custom_page_name){
                $is_prod_custom_page_name =1;

                $data = $obj->getBlockLastReviews(array('start' => 0,
                        'step' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_ndr'),
                        'prefix' => $prefix_cat,
                    )
                );
                $smarty->assign(array($this->name . 'reviews_block' => $data['reviews']));

                ### block last reviews ###
                $smarty->assign(
                    array(
                        $this->name . 'blocklr_w' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_w'),
                        $this->name . 'blocklr_tr' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_tr'),
                    )
                );
                ### block last reviews ###
            }
        }
        /* product page */

        /* other page */
        $is_oth_custom_page_name =0;
        if(!$id_product && !$id_manufacturer && !$id_category && !$is_home){
            $prefix_cat = 'oth';
            if(Configuration::get($this->name . 'blocklr_'.$prefix_cat) == "blocklr_".$prefix_cat && Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_pos') == $custom_page_name){
                $is_oth_custom_page_name =1;

                $data = $obj->getBlockLastReviews(array('start' => 0,
                        'step' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_ndr'),
                        'prefix' => $prefix_cat,
                    )
                );
                $smarty->assign(array($this->name . 'reviews_block' => $data['reviews']));

                ### block last reviews ###
                $smarty->assign(
                    array(
                        $this->name . 'blocklr_w' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_w'),
                        $this->name . 'blocklr_tr' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_tr'),
                    )
                );
                ### block last reviews ###
            }
        }
        /* other page */

        $smarty->assign($this->name . 'is_blocklr', Configuration::get($this->name . 'is_blocklr'));
        $smarty->assign($this->name . 'is_home_'.$custom_page_name, $is_home_custom_page_name);
        $smarty->assign($this->name . 'is_cat_'.$custom_page_name, $is_cat_custom_page_name);
        $smarty->assign($this->name . 'is_man_'.$custom_page_name, $is_man_custom_page_name);
        $smarty->assign($this->name . 'is_prod_'.$custom_page_name, $is_prod_custom_page_name);
        $smarty->assign($this->name . 'is_oth_'.$custom_page_name, $is_oth_custom_page_name);

        $cookie = $this->context->cookie;
        $id_lang = (int)$cookie->id_lang;

        $data_seo_url = $obj->getSEOURLs(array('id_lang'=>$id_lang));

        $all = $data_seo_url['rev_all'];
        $rss_url = $data_seo_url['rss_url'];

        $smarty->assign($this->name . 'allr_url', $all);
        $smarty->assign($this->name . 'rss_url', $rss_url);

        $smarty->assign($this->name . 'sr_sliderr', (int)Configuration::get($this->name . 'sr_sliderr'));
        $smarty->assign($this->name . 'sr_slr', (int)Configuration::get($this->name . 'sr_slr'));
    }

    public function hooklastReviewsSPM($params)
    {
        $name_template = "lastreviewsspm.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty = $this->context->smarty;
            include_once(_PS_MODULE_DIR_.$this->name . '/classes/spmgsnipreviewhelp.class.php');
            $obj = new spmgsnipreviewhelp();
            $this->basicSettingsHook();
            $smarty->assign($this->name . 'rsson', Configuration::get($this->name . 'rsson'));

            $prefix_cat = 'chook';
            $data = $obj->getBlockLastReviews(array('start' => 0,
                    'step' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_ndr'),
                    'prefix' => $prefix_cat,
                )
            );

            $smarty->assign(array($this->name . 'reviews_block_chook' => $data['reviews']));

            ### block last reviews ###
            $smarty->assign(
                array(
                    $this->name . 'blocklr_w' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_w'),
                    $this->name . 'blocklr_tr' => Configuration::get($this->name . 'blocklr_'.$prefix_cat.'_tr'),
                    $this->name.'blocklr_'.$prefix_cat.'_pos' => Configuration::get($this->name.'blocklr_'.$prefix_cat.'_pos'),
                    $this->name.'blocklr_'.$prefix_cat.'' => Configuration::get($this->name.'blocklr_'.$prefix_cat.''),
                )
            );
            ### block last reviews ###

            $cookie = $this->context->cookie;
            $id_lang = (int)$cookie->id_lang;

            $data_seo_url = $obj->getSEOURLs(array('id_lang'=>$id_lang));

            $all = $data_seo_url['rev_all'];
            $rss_url = $data_seo_url['rss_url'];

            $smarty->assign($this->name . 'allr_url', $all);
            $smarty->assign($this->name . 'rss_url', $rss_url);
        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
    }

	private function _getDefaultCategory($id_category)
	{
		$cookie = $this->context->cookie;
		$_category = new Category($id_category);
		return $_category->getName((int)($cookie->id_lang));
	}

    public function setStarsImagesSetting(){
        $smarty = $this->context->smarty;

        switch(Configuration::get($this->name.'stylestars')){
            case 'style1':
                $smarty->assign($this->name.'activestar', 'star-active-yellow.png');
                $smarty->assign($this->name.'activestarh', 'star-half-active-yellow.png');
                $smarty->assign($this->name.'noactivestar', 'star-noactive-yellow.png');
                break;
            case 'style2':
                $smarty->assign($this->name.'activestar', 'star-active-green.png');
                $smarty->assign($this->name.'activestarh', 'star-half-active-green.png');
                $smarty->assign($this->name.'noactivestar', 'star-noactive-green.png');
                break;
            case 'style3':
                $smarty->assign($this->name.'activestar', 'star-active-blue.png');
                $smarty->assign($this->name.'activestarh', 'star-half-active-blue.png');
                $smarty->assign($this->name.'noactivestar', 'star-noactive-blue.png');
                break;
            default:
                $smarty->assign($this->name.'activestar', 'star-active-yellow.png');
                $smarty->assign($this->name.'activestarh', 'star-half-active-yellow.png');
                $smarty->assign($this->name.'noactivestar', 'star-noactive-yellow.png');
                break;
        }
    }

    public function basicSettingsHook(){
        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;
        $id_lang = (int)$cookie->id_lang;

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj = new spmgsnipreviewhelp();

        $hooktodisplay = Configuration::get($this->name.'hooktodisplay');
        $smarty->assign($this->name.'hooktodisplay', $hooktodisplay);

        $this->setStarsImagesSetting();

        $smarty->assign($this->name.'rvis_on', Configuration::get($this->name.'rvis_on'));
        $smarty->assign($this->name.'ratings_on', Configuration::get($this->name.'ratings_on'));

        $smarty->assign($this->name.'svis_on', Configuration::get($this->name.'svis_on'));
        $smarty->assign($this->name.'ip_on', Configuration::get($this->name.'ip_on'));
        $smarty->assign($this->name.'is_captcha', Configuration::get($this->name.'is_captcha'));

        $smarty->assign($this->name.'is_filterp', Configuration::get($this->name.'is_filterp'));
        $smarty->assign($this->name.'is_filterall', Configuration::get($this->name.'is_filterall'));

        $smarty->assign($this->name.'title_on', Configuration::get($this->name.'title_on'));
        $smarty->assign($this->name.'text_on', Configuration::get($this->name.'text_on'));

        $smarty->assign($this->name.'is_abusef',Configuration::get($this->name.'is_abusef'));
        $smarty->assign($this->name.'is_helpfulf',Configuration::get($this->name.'is_helpfulf'));

        $smarty->assign($this->name.'is_sortf',Configuration::get($this->name.'is_sortf'));

        $smarty->assign($this->name.'rsoc_on',Configuration::get($this->name.'rsoc_on'));
        $smarty->assign($this->name.'rsoccount_on',Configuration::get($this->name.'rsoccount_on'));

        $smarty->assign($this->name.'ptabs_type',Configuration::get($this->name.'ptabs_type'));
        $smarty->assign($this->name.'is_rtl',$this->_is_rtl);

        $smarty->assign($this->name.'is_avatar'.$this->_prefix_review, Configuration::get($this->name.'is_avatar'.$this->_prefix_review));
        $smarty->assign($this->name.'is_files'.$this->_prefix_review, Configuration::get($this->name.'is_files'.$this->_prefix_review));

        $smarty->assign($this->name.'ruploadfiles', Configuration::get($this->name.'ruploadfiles'));
        $smarty->assign($this->name.'rminc', Configuration::get($this->name.'rminc'));
        $smarty->assign($this->name.'fpath', $this->path_img_cloud);

        $smarty->assign($this->name.'is_uprof', Configuration::get($this->name.'is_uprof'));

        $data_seo_url = $obj->getSEOURLs(array('id_lang'=>$id_lang));
        $user_url = $data_seo_url['user_url'];
        $useraccount_url = $data_seo_url['useraccount_url'];

        $smarty->assign($this->name.'user_url', $user_url);
        $smarty->assign($this->name.'uacc_url', $useraccount_url);

        $smarty->assign($this->name.'is_storerev', Configuration::get($this->name.'is_storerev'));
        $store_reviews_account_url = $data_seo_url['store_reviews_account_url'];
        $smarty->assign($this->name.'mysr_url', $store_reviews_account_url);

        ## prestashop 1.7 ##
        if(version_compare(_PS_VERSION_, '1.7', '>')){
            $meta_description = isset($smarty->tpl_vars['page']->value['meta']['description'])?$smarty->tpl_vars['page']->value['meta']['description']:'';
            $smarty->assign('meta_description', $meta_description);
        }
        ## prestashop 1.7 ##

        $smarty->assign($this->name.'is_bug', $this->_is_bug_product_page);

        $smarty->assign($this->name.'min_star_par', $this->_min_star_param);
        $smarty->assign($this->name.'max_star_par', $this->_max_star_param);

        $smarty->assign($this->name.'is_mobile', $this->_is_mobile);

        $is_ajax = 0;
        $ajax = Tools::getValue('ajax');
        $action = Tools::getValue('action');
        $content_only = Tools::getValue('content_only');

        if($action == "quickview" || $ajax || $action == "add-to-cart" || $action == "refresh" || $content_only == 1) {
            //$this->context->controller->addJS($this->_path.'views/js/r_stars.js');
            $is_ajax = 1;
        }

        $smarty->assign($this->name.'ajax_action', $action);
        $smarty->assign($this->name.'ajax', $is_ajax);

        if($is_ajax && version_compare(_PS_VERSION_, '1.7', '>')) {
            $_product_id = Tools::getValue("id_product");
            if($_product_id) {
                $id_lang = (int)($cookie->id_lang);
                $product = new Product($_product_id,false,$id_lang);
                $sharing_url = addcslashes($this->context->link->getProductLink($product), "'");
            } else {
                $sharing_url = "";
            }

        } else {
            $_product_id = (int)Tools::getValue('id_product');
            $product = new Product($_product_id,false,(int)($id_lang));
            $data_product = $this->_productData(array('product'=>$product));
            $sharing_url = $data_product['product_url'];
            $sharing_url = str_replace("?content_only=1","",$sharing_url);
        }

        $smarty->assign($this->name.'sharing_url', $sharing_url);
    }

    public function settingsHooks()
    {
        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;

        $this->basicSettingsHook();

        $smarty->assign($this->name.'is16', $this->_is16);

        $smarty->assign($this->name.'starratingon', Configuration::get($this->name.'starratingon'));

        $id_customer = (int)$cookie->id_customer;
        $id_product = (int)Tools::getValue('id_product');

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj = new spmgsnipreviewhelp();

        $avg_rating = $obj->getAvgReview(array('id_product'=>$id_product));

        $smarty->assign($this->name.'avg_rating', $avg_rating['avg_rating']);
        $smarty->assign($this->name.'avg_decimal', $avg_rating['avg_rating_decimal']);

        $count_reviews = $obj->getCountReviews(array('id_product'=>$id_product));
        $smarty->assign($this->name.'count_reviews', $count_reviews);
        $smarty->assign($this->name.'text_reviews', $obj->number_ending($count_reviews, $this->l('reviews'), $this->l('review'), $this->l('reviews')));

        $data_rating = $obj->getCountRatingForItem();
        $smarty->assign($this->name.'one', $data_rating['one']);
        $smarty->assign($this->name.'two', $data_rating['two']);
        $smarty->assign($this->name.'three', $data_rating['three']);
        $smarty->assign($this->name.'four', $data_rating['four']);
        $smarty->assign($this->name.'five', $data_rating['five']);

        $smarty->assign($this->name.'is_onerev', Configuration::get($this->name.'is_onerev'));

        $smarty->assign($this->name.'is_sortallf', Configuration::get($this->name.'is_sortallf'));
        $smarty->assign($this->name.'is_sortfu', Configuration::get($this->name.'is_sortfu'));

        if(Configuration::get($this->name.'is_onerev') != 1) {
            $is_alreadyaddreview = $obj->checkIsUserAlreadyAddReview(array('id_product' => $id_product, 'id_customer' => $id_customer));

        } else {
            $is_alreadyaddreview = 0;
        }
        $smarty->assign(array($this->name.'is_add' => $is_alreadyaddreview));

        if(Configuration::get('PS_REWRITING_SETTINGS')){
            $smarty->assign($this->name.'is_rewrite', 1);
        } else {
            $smarty->assign($this->name.'is_rewrite',0);
        }

        $smarty->assign(
                array(
                    $this->name.'sh_name' => @Configuration::get('PS_SHOP_NAME'),
                     )
                );
        $smarty->assign($this->name.'is_ps15', $this->_is15);
    }

    public function setSettingsPinterest($params = null){
        $smarty = $this->context->smarty;
        $this->_infoAboutProductData($params);
        // pinterest
        $smarty->assign($this->name.'pinvis_on', Configuration::get($this->name.'pinvis_on'));
        $smarty->assign($this->name.'pinterestbuttons', Configuration::get($this->name.'pinterestbuttons'));
        // pinterest

        $is16_snippet = 0;
        if($this->_is16 == 1 && Configuration::get($this->name.'svis_on') == 1){
            $is16_snippet = 1;
        }
        $smarty->assign($this->name.'is16_snippet', $is16_snippet);
    }

    public function hookExtraRight($params){

        $id_product = (int)Tools::getValue('id_product');
        $name_template = "reviewsblockextraright.tpl";
        $cache_id = $this->name.'|'.$name_template.$id_product;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty = $this->context->smarty;

            $this->basicSettingsHook();
            $hooktodisplay = Configuration::get($this->name.'hooktodisplay');

            if($hooktodisplay == "extra_right") {
                $this->settingsHooks();
            }
            // pinterest
                $this->setSettingsPinterest($params);
                $smarty->assign($this->name . '_extraRight', Configuration::get($this->name . '_extraRight'));
            // pinterest
        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
    }

	public function hookExtraLeft($params){

        $id_product = (int)Tools::getValue('id_product');
        $name_template = "reviewsblockextraleft.tpl";
        $cache_id = $this->name.'|'.$name_template.$id_product;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {
			$smarty = $this->context->smarty;

            $this->basicSettingsHook();

            $hooktodisplay = Configuration::get($this->name.'hooktodisplay');

            if($hooktodisplay == "extra_left") {
                $this->settingsHooks();
            }
			// pinterest
            $this->setSettingsPinterest($params);
			$smarty->assign($this->name.'_extraLeft', Configuration::get($this->name.'_extraLeft'));
			// pinterest
        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
	}
	
	public function hookproductFooter($params){
        $ptabs_type = Configuration::get($this->name . 'ptabs_type');

        if(version_compare(_PS_VERSION_, '1.7', '>') && $ptabs_type != 3) {
            $id_product = (int)Tools::getValue('id_product');
            $name_template = "producttabcontent17.tpl";
            $cache_id = $this->name.'|'.$name_template.$id_product;

            if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

                ## prestashop 1.7 ##
                $this->setHookProductTabContentSettings($params);
                ## prestashop 1.7 ##

                $smarty = $this->context->smarty;
                // pinterest
                $this->setSettingsPinterest($params);
                $smarty->assign($this->name.'_productFooter', Configuration::get($this->name.'_productFooter'));
                // pinterest
            }
            return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
        } else {
            $id_product = (int)Tools::getValue('id_product');
            $name_template = "reviewsblockproductfooter.tpl";
            $cache_id = $this->name.'|'.$name_template.$id_product;

            if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

                $smarty = $this->context->smarty;

                $this->basicSettingsHook();

                $this->settingsHooks();

                // pinterest
                $this->setSettingsPinterest($params);
                $smarty->assign($this->name.'_productFooter', Configuration::get($this->name.'_productFooter'));
                // pinterest

                ## user profile ##
                if(Configuration::get($this->name . $this->_prefix_review . 'adv_footer') == 1) {
                    $this->setuserprofilegSettings();
                }
                ## user profile ##

            }
            return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
        }
	}

    public function hookdisplayProductButtons($params)
    {

            return $this->hookproductActions($params);

    }
	
	public function hookproductActions($params){
        $id_product = (int)Tools::getValue('id_product');
        $name_template = "reviewsblockproductactions.tpl";
        $cache_id = $this->name.'|'.$name_template.$id_product;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

			$smarty = $this->context->smarty;

            $this->basicSettingsHook();

            $hooktodisplay = Configuration::get($this->name.'hooktodisplay');

            if($hooktodisplay == "product_actions") {
                $this->settingsHooks();
            }

			// pinterest
            $this->setSettingsPinterest($params);
			$smarty->assign($this->name.'_productActions', Configuration::get($this->name.'_productActions'));
			// pinterest
        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
	}
    
    public function hookProductTab($params)
    {
        $id_product = (int)Tools::getValue('id_product');
        $name_template = "tab.tpl";
        $cache_id = $this->name.'|'.$name_template.$id_product;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty = $this->context->smarty;
            $cookie = $this->context->cookie;
            $smarty->assign($this->name.'is16', $this->_is16);

            $id_customer = (int)$cookie->id_customer;

            $smarty->assign(array($this->name.'id_customer' => $id_customer));

            $id_product = (int)Tools::getValue('id_product');

            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
            $obj = new spmgsnipreviewhelp();
            $count_reviews = $obj->getCountReviews(array('id_product'=>$id_product));
            $smarty->assign(array($this->name.'count_reviews' => $count_reviews));

            $is_alreadyaddreview = $obj->checkIsUserAlreadyAddReview(array('id_product'=>$id_product,'id_customer'=>$id_customer));
            $smarty->assign(array($this->name.'is_add' => $is_alreadyaddreview));

            $this->basicSettingsHook();
        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
	}

    public function setHookProductTabContentSettings($params){
        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;

        $id_product = isset($params['id_product'])?(int)$params['id_product']:(int)Tools::getValue('id_product');

        $smarty->assign($this->name.'is16', $this->_is16);
        $smarty->assign($this->name.'is15', $this->_is15);

        $smarty->assign($this->name.'d_eff_rev', Configuration::get($this->name.'d_eff_rev'));
        $smarty->assign($this->name.'ptabs_type',Configuration::get($this->name.'ptabs_type'));

        $this->settingsHooks();

        $this->setSEOUrls();

        if(Configuration::get('PS_REWRITING_SETTINGS')){
            $smarty->assign($this->name.'is_rewrite', 1);
        } else {
            $smarty->assign($this->name.'is_rewrite',0);
        }

        $id_lang = (int)$cookie->id_lang;
        $iso_lang = Language::getIsoById((int)($id_lang));
        $smarty->assign($this->name.'iso_lang', $iso_lang);

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj = new spmgsnipreviewhelp();

        $data_permissions_vouchers = $obj->getGroupPermissionsForVouchers();
        $is_show_voucher = $data_permissions_vouchers['is_show_voucher'];
        $is_show_fb_voucher = $data_permissions_vouchers['is_show_fb_voucher'];

        $params_obj_product = isset($params['product'])?$params['product']:null;
        $id_category_default = 0;

        if ((!empty($params_obj_product) && is_array($params_obj_product) && version_compare(_PS_VERSION_, '1.7', '<')) ||
            (!empty($params_obj_product) && is_object($params_obj_product) && version_compare(_PS_VERSION_, '1.7', '>'))
        )
        {
            $id_category_default = isset($params_obj_product->id_category_default)?$params_obj_product->id_category_default:(isset($params_obj_product['id_category_default'])?$params_obj_product['id_category_default']:0);
        }

        $smarty->assign(array(
            $this->name.'criterions' => $obj->getReviewCriteria(array('id_lang'=>$id_lang,'id_shop'=>$this->_id_shop,
                                                                      'id_category'=>$id_category_default,'id_product'=>$id_product)),
        ));

        $smarty->assign($this->name.'id_category', $id_category_default);

        $data_seo_url = $obj->getSEOURLs(array('id_lang'=>$id_lang));

        $my_account = $data_seo_url['my_account'];
        $rev_url = $data_seo_url['rev_url'];

        $smarty->assign($this->name.'m_acc', $my_account);
        $smarty->assign($this->name.'rev_url', $rev_url);

        $id_customer = (int)$cookie->id_customer;
        $is_logged = isset($cookie->id_customer)?$cookie->id_customer:0;

        $smarty->assign($this->name.'islogged', $is_logged);

        $customer_name = '';
        $customer_email = '';
        $avatar = '';
        if($is_logged) {
            $customer_data = $obj->getInfoAboutCustomer(array('id_customer' => $id_customer));
            $customer_name = $customer_data['customer_name'];
            $customer_email = $customer_data['email'];

            $data_avatar = $obj->getAvatarForCustomer(array('id_customer' => $id_customer));
            $avatar = $data_avatar['avatar'];
        }

        $smarty->assign(array($this->name.'c_avatar' => $avatar));
        $smarty->assign(array($this->name.'c_name' => $customer_name));
        $smarty->assign(array($this->name.'c_email' => $customer_email));

        $is_buy = $obj->checkProductBought(array('id_product'=>$id_product,'id_customer'=>$id_customer));
        $smarty->assign(array($this->name.'is_buy' => $is_buy));

        $smarty->assign($this->name.'is_onerev', Configuration::get($this->name.'is_onerev'));

        if(Configuration::get($this->name.'is_onerev') != 1) {
            $is_alreadyaddreview = $obj->checkIsUserAlreadyAddReview(array('id_product' => $id_product, 'id_customer' => $id_customer));
        } else {
            $is_alreadyaddreview = 0;
        }
        $smarty->assign(array($this->name . 'is_add' => $is_alreadyaddreview));
        $smarty->assign(array($this->name.'id_customer' => $id_customer));
        $smarty->assign(array($this->name.'id_product' => $id_product));
        $smarty->assign($this->name.'whocanadd', Configuration::get($this->name.'whocanadd'));

        $this->basicSettingsHook();

        // voucher //
        $smarty->assign($this->name.'vis_on', Configuration::get($this->name.'vis_on'));
        // set discount
        switch (Configuration::get($this->name.'discount_type'))
        {
            case 1:
                // percent
                $id_discount_type = 1;
                $value = Configuration::get($this->name.'percentage_val');
                $id_currency = (int)$cookie->id_currency;
                break;
            case 2:
                // currency
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get('sdamount_'.(int)$id_currency);
                break;
            default:
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get('sdamount_'.(int)$id_currency);
        }
        $valuta = "%";
        $tax = '';

        if($id_discount_type == 2){

            $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
            $id_currency = (int)$cookie->id_currency;
            foreach ($cur AS $_cur){
                if(
                    $id_currency == $_cur['id_currency']
                ){
                    $valuta = $_cur['sign'];
                }
            }
            $tax = (int)Configuration::get($this->name.'tax');
        }

        $smarty->assign($this->name.'tax', $tax);
        $smarty->assign($this->name.'discount', $value.' '.$valuta);
        $smarty->assign($this->name.'valuta', $valuta);
        $smarty->assign($this->name.'sdvvalid', Configuration::get($this->name.'sdvvalid'));
        $smarty->assign($this->name.'days', $obj->number_ending(Configuration::get($this->name.'sdvvalid'), $this->l('days'), $this->l('day'), $this->l('days')));

        // minimum checkout //
        $smarty->assign($this->name.'is_show_min', Configuration::get($this->name.'is_show_min'));
        $smarty->assign($this->name.'isminamount', Configuration::get($this->name.'isminamount'));
        $smarty->assign($this->name.'minamount', Configuration::get('sdminamount_'.(int)$id_currency));
        $fvaluta_min = '';

        $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
        $id_currency = (int)$cookie->id_currency;
        foreach ($cur AS $_cur){
            if(
                $id_currency == $_cur['id_currency']
            ){
                $fvaluta_min = $_cur['sign'];
            }
        }

        $smarty->assign($this->name.'curtxt', $fvaluta_min);
        // minimum checkout //

        $smarty->assign($this->name.'is_show_voucher', $is_show_voucher);

        // voucher //
        // voucher facebook //
        $smarty->assign($this->name.'vis_onfb', Configuration::get($this->name.'vis_onfb'));

        // set discount
        switch (Configuration::get($this->name.'discount_typefb'))
        {
            case 1:
                // percent
                $id_discount_type = 1;
                $value = Configuration::get($this->name.'percentage_valfb');
                $id_currency = (int)$cookie->id_currency;
                break;
            case 2:
                // currency
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get('sdamountfb_'.(int)$id_currency);
                break;
            default:
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get('sdamountfb_'.(int)$id_currency);
        }
        $valuta = "%";
        $tax = '';

        if($id_discount_type == 2){

            $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);

            $id_currency = (int)$cookie->id_currency;
            foreach ($cur AS $_cur){
                if(
                    $id_currency == $_cur['id_currency']
                ){
                    $valuta = $_cur['sign'];
                }
            }

            $tax = (int)Configuration::get($this->name.'taxfb');
        }

        $smarty->assign($this->name.'taxfb', $tax);
        $smarty->assign($this->name.'discountfb', $value.' '.$valuta);
        $smarty->assign($this->name.'valutafb', $valuta);
        $smarty->assign($this->name.'sdvvalidfb', Configuration::get($this->name.'sdvvalidfb'));
        $smarty->assign($this->name.'daysfb', $obj->number_ending(Configuration::get($this->name.'sdvvalidfb'), $this->l('days'), $this->l('day'), $this->l('days')));

        // minimum checkout //
        $smarty->assign($this->name.'is_show_minfb', Configuration::get($this->name.'is_show_minfb'));
        $smarty->assign($this->name.'isminamountfb', Configuration::get($this->name.'isminamountfb'));
        $smarty->assign($this->name.'minamountfb', Configuration::get('sdminamountfb_'.(int)$id_currency));
        $fvaluta_min = '';

        $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);

        $id_currency = (int)$cookie->id_currency;
        foreach ($cur AS $_cur){
            if(
                $id_currency == $_cur['id_currency']
            ){
                $fvaluta_min = $_cur['sign'];
            }
        }
        $smarty->assign($this->name.'curtxtfb', $fvaluta_min);
        // minimum checkout //

        $smarty->assign($this->name.'is_show_fb_voucher', $is_show_fb_voucher);
        // voucher facebook //
        ### reviews ###
        $gp = (int)Tools::getValue('gp');
        $step = (int)Configuration::get($this->name.'revperpage');
        $page = isset($params['page'])?$params['page']:null;

        if($page){
            // for ajax request
            $start = $page;
            // for ajax request
        } else {
            $start = (int)(($gp - 1) * $step);
        }
        if($start<0)
            $start = 0;

        $frat = isset($params['frat'])?$params['frat']:Tools::getValue('frat');

        $search = isset($params['search'])?$params['search']:Tools::getValue("search");
        $is_search = 0;

        ### search ###
        if(Tools::strlen($search)>0){
            $is_search = 1;

        }
        $this->context->smarty->assign($this->name.'is_search', $is_search);
        $this->context->smarty->assign($this->name.'search', $search);
        ## search ###

        $is_sort = isset($params['is_sort'])?$params['is_sort']:0;
        $sort_condition = isset($params['sort_condition'])?$params['sort_condition']:'';

        $reviews_data = $obj->getReviews(
                                        array(
                                               'id_product'=>$id_product,'start'=>$start,'frat'=>$frat,'is_search'=>$is_search,'search'=>$search,
                                               'is_sort'=>$is_sort,'sort_condition'=>$sort_condition
                                              )
                                        );
        $data = $reviews_data['reviews'];
        $count_reviews = $reviews_data['count_all_reviews'];


        $_obj_product = new Product($id_product,null,$id_lang);
        $data_product = $this->_productData(array('product'=>$_obj_product));

        $paging = $obj->paging17(
            array('start'=>$start,
                'step'=> $step,
                'count' => $count_reviews,
                'id_product'=>$id_product,
                'frat'=>$frat,'is_search'=>$is_search,'search'=>$search,'is_sort'=>$is_sort,
                'action'=>'productpagereviews'
            )
        );

        $data_translate = $this->translateCustom();
        $_data_translate = $this->translateItems();

        $smarty->assign(
            array('reviews' => $data,
                'paging' => $paging,
                $this->name.'page_text' => $this->l('Page'),
                $this->name.'gp' => $gp,
                $this->name.'frat' => $frat,
                $this->name.'product_url' => $data_product['product_url'],
                $this->name.'ptc_msg1'=>$data_translate['ptc_msg1'],
                $this->name.'ptc_msg2'=>$data_translate['ptc_msg2'],
                $this->name.'ptc_msg3'=>$data_translate['ptc_msg3'],
                $this->name.'ptc_msg4'=>$data_translate['ptc_msg4'],
                $this->name.'ptc_msg5'=>$data_translate['ptc_msg5'],
                $this->name.'ptc_msg6'=>$data_translate['ptc_msg6'],
                $this->name.'ptc_msg7'=>$data_translate['ptc_msg7'],
                $this->name.'ptc_msg8'=>$data_translate['ptc_msg8'],
                $this->name.'ptc_msg9'=>$data_translate['ptc_msg9'],
                $this->name.'ptc_msg10'=>$data_translate['ptc_msg10'],
                $this->name.'ptc_msg11'=>$data_translate['ptc_msg11'],
                $this->name.'ptc_msg12'=>$data_translate['ptc_msg12'],
                $this->name.'ptc_msg13_1'=>$data_translate['ptc_msg13_1'],
                $this->name.'ptc_msg13_2'=>$data_translate['ptc_msg13_2'],

                $this->name.'ava_msg8'=>$data_translate['ava_msg8'],
                $this->name.'ava_msg9'=>$data_translate['ava_msg9'],

                $this->name.'ptc_msg14'=>$_data_translate['msg10'],
            ));
        ### reviews ###

        $smarty->assign($this->name.'vis_on', Configuration::get($this->name.'vis_on'));
        $smarty->assign($this->name.'is_ps15',$this->_is15);

        $product_obj = new Product($id_product);
        $name_product = $product_obj->name[$id_lang];
        $smarty->assign($this->name.'nameprsnip',$name_product);

        $this->setSettingsPinterest($params);


        // gdpr
        $smarty->assign(array('id_module' => $this->id));
        // gdpr




        return array('paging'=>$paging); // for ajax




    }

    public function hookProductTabContent($params)
    {
        $id_product = (int)Tools::getValue('id_product');

        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $name_template = "producttabcontent17.tpl";
        } else {
            $name_template = "producttabcontent.tpl";
        }
        $cache_id = $this->name.'|'.$name_template.$id_product;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            ## prestashop 1.7 ##
            $this->setHookProductTabContentSettings($params);
            ## prestashop 1.7 ##

        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
    }

    public function hookdisplayProductListReviews($params){

        if(Configuration::get($this->name.'starscat') == 1 && Configuration::get($this->name.'rvis_on') == 1) {

            $id_product = isset($params['product']['id_product'])?(int)$params['product']['id_product']:(int)$params['product']->id;
            $name_template = "liststars.tpl";
            $cache_id = $this->name.'|'.$name_template.$id_product;

            if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {
                $smarty = $this->context->smarty;

                include_once(_PS_MODULE_DIR_.$this->name . '/classes/spmgsnipreviewhelp.class.php');
                $obj_reviewshelp = new spmgsnipreviewhelp();
                $count_review = $obj_reviewshelp->getCountReviews(array('id_product' => $id_product));
                $avg_rating = $obj_reviewshelp->getAvgReview(array('id_product' => $id_product));

                $smarty->assign(
                    array(
                        'id_product' => $id_product,
                        'avg_rating' => $avg_rating['avg_rating'],
                        'count_review' => $count_review,
                        $this->name.'min_star_par' => $this->_min_star_param,
                        $this->name.'max_star_par' => $this->_max_star_param,
                        $this->name.'is_starscat' => Configuration::get($this->name.'is_starscat')
                    )
                );
                $this->basicSettingsHook();
            }
            return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
        }
    }

    public function hookHeader($params){

             // for 16
            $this->context->controller->addCSS(($this->_path) . 'views/css/spmgsnipreview.css', 'all');

            if(version_compare(_PS_VERSION_, '1.7', '>')) {
                $this->context->controller->addCSS(($this->_path) . 'views/css/spmgsnipreview17.css', 'all');
            }

             if($this->_is_rtl)
                $this->context->controller->addCSS(($this->_path) . 'views/css/spmgsnipreview-rtl.css', 'all');

    		$this->context->controller->addJS($this->_path.'views/js/r_stars.js');
            $this->context->controller->addJS($this->_path.'views/js/spmgsnipreview.js');

            $id_product = Tools::getValue('id_product');

            if(Configuration::get($this->name.'is_files'.$this->_prefix_review) && $id_product){
                $this->context->controller->addJqueryUI(array('ui.widget'));
                $this->context->controller->addJS($this->_path.'views/js/jquery.fileupload.js');
                $this->context->controller->addJS($this->_path.'views/js/jquery.fileupload-process.js');
                $this->context->controller->addJS($this->_path.'views/js/jquery.fileupload-validate.js');
                $this->context->controller->addJS($this->_path.'views/js/main-fileupload.js');
                $this->context->controller->addCSS(($this->_path) . 'views/css/font-custom.min.css', 'all');
                if(version_compare(_PS_VERSION_, '1.7', '>')) {
                    $this->context->controller->addJqueryPlugin(array('fancybox'));
                }
            }

            if(Configuration::get($this->name.'is_uprof')){
                $this->context->controller->addCSS(($this->_path) . 'views/css/users.css', 'all');
                $this->context->controller->addJS($this->_path . 'views/js/users.js');
            }

            ## store reviews ##
            if(Configuration::get($this->name.'is_storerev')) {
                $this->context->controller->addCSS(($this->_path) . 'views/css/storereviews.css', 'all');
                $this->context->controller->addJS($this->_path . 'views/js/storereviews.js');
                $this->context->controller->addCSS(($this->_path).'views/css/widgets.css', 'all');
            }
            ## store reviews ##

            ### owl carousel ###
            $is_storerev = Configuration::get($this->name.'is_storerev');
            $sr_slider = Configuration::get($this->name . 'sr_slider');
            $sr_sliderh = Configuration::get($this->name . 'sr_sliderh');

            $sr_sliderr = Configuration::get($this->name . 'sr_sliderr');
            $sr_sliderhr = Configuration::get($this->name . 'sr_sliderhr');
            $rvis_on = Configuration::get($this->name . 'rvis_on');

            $sr_slideru = Configuration::get($this->name . 'sr_slideru');
            $sr_sliderhu = Configuration::get($this->name . 'sr_sliderhu');
            $is_uprof = Configuration::get($this->name.'is_uprof');

            if (
                ($sr_slider == 1 && $is_storerev) || ($sr_sliderh == 1 && $is_storerev) ||
                ($sr_sliderr == 1 && $rvis_on) || ($sr_sliderhr == 1 && $rvis_on) ||
                ($sr_slideru == 1 && $is_uprof) || ($sr_sliderhu == 1 && $is_uprof)
                ) {
                $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/js/owl.carousel.js');
                $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/css/owl.carousel.css');
                $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/css/owl.theme.default.css');
            }
            ### owl carousel ###

            // wow effects only for product page
            if($id_product && Configuration::get($this->name .'d_eff_rev') != "disable_all_effects" && $id_product){
                $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/js/wow.js');
                $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/css/animate.css');

            }
            // wow effects only for product page

        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $this->name . '/views/js/jquery.scrollTo.min.js');
        }



        $smarty = $this->context->smarty;

        $rid = (int)Tools::getValue('rid');
        $is_review_page = 0;

        if($rid){

            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
            $obj_reviewshelp = new spmgsnipreviewhelp();

            $data = $obj_reviewshelp->getOneReview(array('rid'=>$rid,));
            $smarty->assign($this->name.'name', $data['reviews'][0]['title_review']);
            $smarty->assign($this->name.'descr', strip_tags($data['reviews'][0]['text_review']));
            $smarty->assign($this->name.'img', $data['reviews'][0]['product_img']);
            $smarty->assign($this->name.'review_url', $data['reviews'][0]['review_url']);
            $is_review_page = 1;
        }
        $smarty->assign($this->name.'is_r_p', $is_review_page);


        $name_template = "head.tpl";
        $cache_id = $this->name.'|'.$name_template;

        if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $cookie = $this->context->cookie;

            $smarty->assign($this->name.'is15', $this->_is15);
            $smarty->assign('shop_name', @Configuration::get('PS_SHOP_NAME'));

            include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
            $obj_reviewshelp = new spmgsnipreviewhelp();

            $smarty->assign($this->name . 'is_files'.$this->_prefix_review, Configuration::get($this->name.'is_files'.$this->_prefix_review));

            if(Configuration::get($this->name.'is_uprof')) {
                include_once(_PS_MODULE_DIR_.$this->name . "/classes/userprofileg.class.php");
                $obj_userprofileg = new userprofileg();

                $info_customer = $obj_userprofileg->getCustomerInfo();
                $avatar_thumb = $info_customer['avatar_thumb'];
                $exist_avatar = $info_customer['exist_avatar'];
                $is_show = $info_customer['is_show'];

                $smarty->assign($this->name . 'avatar_thumb', $avatar_thumb);
                $smarty->assign($this->name . 'exist_avatar', $exist_avatar);
                $smarty->assign($this->name . 'is_show', $is_show);
                $is_logged = isset($cookie->id_customer)?$cookie->id_customer:0;
                $smarty->assign($this->name.'islogged', $is_logged);
            }
            $smarty->assign($this->name . 'is_uprof', Configuration::get($this->name.'is_uprof'));

            ## store reviews ##
            if(Configuration::get($this->name.'is_storerev')) {
                $smarty->assign($this->name.'rssontestim', Configuration::get($this->name.'rssontestim'));
                if($this->_is_mobile == 1) {
                    $data_fb = $obj_reviewshelp->getfacebooklib($cookie->id_lang);
                    if (
                        (@filesize(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 't-left' . $data_fb['lng_iso'] . '.png') > 0
                        )
                        &&
                        (@filesize(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 't-right' . $data_fb['lng_iso'] . '.png') > 0
                        )
                    )
                    {
                        $lng_custom = $data_fb['lng_iso'];
                    } else {
                        $lng_custom = "";
                    }

                    $smarty->assign($this->name . 'lang', $lng_custom);
                }

                $smarty->assign($this->name.'t_width', $this->_t_width);
                $smarty->assign($this->name.'BGCOLOR_T', Configuration::get($this->name.'BGCOLOR_T'));
                $smarty->assign($this->name.'BGCOLOR_TIT', Configuration::get($this->name.'BGCOLOR_TIT'));
                $smarty->assign($this->name.'is_mobile', $this->_is_mobile);
                $this->_setPositionsforStoreWidget();

            }
            $smarty->assign($this->name.'t_leftside', Configuration::get($this->name.'t_leftside'));
            $smarty->assign($this->name.'t_rightside', Configuration::get($this->name.'t_rightside'));
            $smarty->assign($this->name . 'is_storerev', Configuration::get($this->name.'is_storerev'));
            ## store reviews ##

            $rid = (int)Tools::getValue('rid');
            $is_review_page = 0;
            if($rid){
                $data = $obj_reviewshelp->getOneReview(array('rid'=>$rid,));
                $smarty->assign($this->name.'name', $data['reviews'][0]['title_review']);
                $smarty->assign($this->name.'descr', strip_tags($data['reviews'][0]['text_review']));
                $smarty->assign($this->name.'img', $data['reviews'][0]['product_img']);
                $smarty->assign($this->name.'review_url', $data['reviews'][0]['review_url']);
                $is_review_page = 1;
            }
            $smarty->assign($this->name.'is_r_p', $is_review_page);

            $smarty->assign($this->name.'rsoc_on', Configuration::get($this->name.'rsoc_on'));

            $data_fb = $obj_reviewshelp->getfacebooklib((int)$params['cookie']->id_lang);
            $smarty->assign($this->name.'fbliburl', $data_fb['url']);

            $smarty->assign($this->name.'is16', $this->_is16);

            $this->basicSettingsHook();

            $smarty->assign($this->name.'rsson', Configuration::get($this->name.'rsson'));

            switch(Configuration::get($this->name.'stylestars')){
                case 'style1':
                    $smarty->assign($this->name.'stylecolor', '#F7B900');
                    break;
                case 'style2':
                    $smarty->assign($this->name.'stylecolor', '#7BE408');
                    break;
                case 'style3':
                    $smarty->assign($this->name.'stylecolor', '#00ABEC');
                    break;
                default:
                    $smarty->assign($this->name.'stylecolor', '#F7B900');
                    break;
            }

            ### snippets and pins ####
            $_product_id = Tools::getValue("id_product");
            $this->_infoAboutProductData($params);
            // pinterest

            $smarty->assign($this->name.'is_product_page', $_product_id);
            $smarty->assign($this->name.'pinvis_on', Configuration::get($this->name.'pinvis_on'));

            $is_ssl = 0;
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (bool)Configuration::get('PS_SSL_ENABLED'))
                $is_ssl = 1;
            $smarty->assign($this->name.'is_ssl', $is_ssl);
            // pinterest
            ### snippets and pins ####

            $smarty->assign($this->name.'starscat', Configuration::get($this->name.'starscat'));

            $this->setSEOUrls();
        }
        return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
	}

    private function _infoAboutProductData($params){
        $_product_id = Tools::getValue("id_product");

        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;

        if($_product_id){
            $id_lang = (int)($cookie->id_lang);
            $_obj_product = new Product($_product_id);

            $data_product = $this->_productData(array('product'=>$_obj_product));
            $picture = $data_product['image_link'];

            $productname = addslashes($_obj_product->name[$id_lang]);

            $description = $_obj_product->description_short[$id_lang] .' '. $_obj_product->description[$id_lang];
            $description = strip_tags($description);
            $smarty->assign($this->name.'pindesc', $description);

            $smarty->assign($this->name.'picture', isset($picture)?$picture:'');
            $smarty->assign($this->name.'productname', $productname);

            //// new ///
            $product = new Product(Tools::getValue("id_product"),false,(int)($cookie->id_lang));

            $cart = $this->context->cart;
            $id_currency = $cart->id_currency;

            $currency = new Currency($id_currency);
            if (!$currency) {
                try {
                    $currency = Currency::getCurrencyInstance($cookie->id_currency);
                } catch (Exception $e) {
                }
            }
            $qty = $product->getQuantity(Tools::getValue("id_product"));
            $desc = ($product->description_short != "") ? $product->description_short : $product->description;

            $smarty->assign(array(
                'product_brand' => Manufacturer::getNameById($product->id_manufacturer),
                'product_name' => $product->name,
                'product_image' => $picture,
                'product_price_custom' => number_format($product->getPrice(),2,".",","),
                'product_description' => Tools::htmlentitiesUTF8(strip_tags($desc)),
                'product_category' => $this->_getDefaultCategory($product->id_category_default),
                'currency_custom' => $currency->iso_code,
                'quantity' => $qty,
                'stock_string' => ($qty > 0) ? 'in_stock' : 'out_of_stock'
            ));
            /// end new ///
        }
    }

    public function hookFooter($params){

    $name_template = "footer.tpl";
    $cache_id = $this->name.'|'.$name_template;

    if (!$this->isCached($this->_template_name.$name_template, $this->getCacheId($cache_id))) {

            $smarty = $this->context->smarty;
            $smarty->assign($this->name.'is16', $this->_is16);
            $_product_id = Tools::getValue("id_product");

            $this->_infoAboutProductData($params);

            $smarty->assign($this->name.'is_product_page', $_product_id);
            $smarty->assign($this->name.'pinvis_on', Configuration::get($this->name.'pinvis_on'));

            $this->setLastReviewsBlockSettings(array('custom_page_name'=>'bottom'));

            ## badges ###
            $this->badges(array('custom_page_name'=>'bottom'));
            ## badges ###

            if(Configuration::get($this->name.'breadvis_on')==1){

                $getTemplateVars_functions = 'getTemplateVars';
                if(!is_callable($smarty, $getTemplateVars_functions)){
                    $getTemplateVars = $getTemplateVars_functions;
                }

                if(version_compare(_PS_VERSION_, '1.7', '<')) {
                    if ($smarty->{$getTemplateVars}('path')) {
                        $path = $smarty->${'getTemplateVars'}('path');
                        $output = $path;

                        ob_start();
                        include(dirname(__FILE__) . '/views/templates/hooks/_breadcrumbs.phtml');
                        $output = ob_get_clean();

                        $smarty->assign($this->name . 'breadcustom', $output);
                    } else {
                        $smarty->assign($this->name . 'breadcustom', '');
                    }
                } else {
                    if ($smarty->{$getTemplateVars}('breadcrumb')) {
                        $path = $smarty->${'getTemplateVars'}('breadcrumb');
                        $path = $path['links'];
                        $output = $path;

                        ob_start();
                        include(dirname(__FILE__) . '/views/templates/hooks/_breadcrumbs.phtml');
                        $output = ob_get_clean();

                        $smarty->assign($this->name . 'breadcustom', $output);
                    } else {
                        $smarty->assign($this->name . 'breadcustom', '');
                    }
                }
            } else {
                $smarty->assign($this->name.'breadcustom','');
            }

            ## user profile ##
            if(Configuration::get($this->name . $this->_prefix_review . 'adv_footer') == 1) {
                $this->setuserprofilegSettings();
            }
            $smarty->assign($this->name . $this->_prefix_review . 'adv_footer', Configuration::get($this->name . $this->_prefix_review . 'adv_footer'));
            ## user profile ##

            ## store reviews ##
            if(Configuration::get($this->name . 't_footer') == 1 || Configuration::get($this->name . 't_leftside') == 1
                || Configuration::get($this->name . 't_rightside') == 1) {
                $this->setStoreReviewsColumnsSettings();
                $smarty->assign($this->name.'is_mobile', $this->_is_mobile);
                $smarty->assign($this->name.'t_width', $this->_t_width);
            }
            $smarty->assign($this->name . 't_footer', Configuration::get($this->name . 't_footer'));
            $smarty->assign($this->name . 't_leftside', Configuration::get($this->name . 't_leftside'));
            $smarty->assign($this->name . 't_rightside', Configuration::get($this->name . 't_rightside'));
            ## store reviews ##
    }
    return $this->display(__FILE__, $this->_template_path.$name_template, $this->getCacheId($cache_id));
	}
	
	public function hookCustomerAccount($params)
	{
        $cookie = $this->context->cookie;
        $is_logged = isset($cookie->id_customer)?$cookie->id_customer:0;

        if($is_logged) {
            $name_template = "my-account.tpl";
            $cache_id = $this->name .'|'. $name_template;

            if (!$this->isCached($this->_template_name . $name_template, $this->getCacheId($cache_id))) {

                $smarty = $this->context->smarty;

                $smarty->assign($this->name . 'is16', $this->_is16);

                $smarty->assign($this->name . 'is_ps15', $this->_is15);

                $this->basicSettingsHook();

                $smarty->assign($this->name . 'islogged', $is_logged);

                include_once(_PS_MODULE_DIR_.$this->name . '/classes/spmgsnipreviewhelp.class.php');
                $obj_reviewshelp = new spmgsnipreviewhelp();

                $id_lang = (int)($cookie->id_lang);
                $data_seo_url = $obj_reviewshelp->getSEOURLs(array('id_lang' => $id_lang));

                $account_url = $data_seo_url['account_url'];
                $smarty->assign($this->name . 'account_url', $account_url);
            }
            return $this->display(__FILE__, $this->_template_path . $name_template, $this->getCacheId($cache_id));
        }
	}
	
	public function hookMyAccountBlock($params)
	{
        $cookie = $this->context->cookie;
        $is_logged = isset($cookie->id_customer) ? $cookie->id_customer : 0;

        if($is_logged) {
            $name_template = "my-account-block.tpl";
            $cache_id = $this->name .'|'. $name_template;

            if (!$this->isCached($this->_template_name . $name_template, $this->getCacheId($cache_id))) {

                $smarty = $this->context->smarty;

                $smarty->assign($this->name . 'is16', $this->_is16);

                $smarty->assign($this->name . 'is_ps15', $this->_is15);

                $this->basicSettingsHook();
                $smarty->assign($this->name . 'islogged', $is_logged);

                include_once(_PS_MODULE_DIR_.$this->name . '/classes/spmgsnipreviewhelp.class.php');
                $obj_reviewshelp = new spmgsnipreviewhelp();

                $id_lang = (int)($cookie->id_lang);
                $data_seo_url = $obj_reviewshelp->getSEOURLs(array('id_lang' => $id_lang));

                $account_url = $data_seo_url['account_url'];

                $smarty->assign($this->name . 'account_url', $account_url);
            }
            return $this->display(__FILE__, $this->_template_path . $name_template, $this->getCacheId($cache_id));
        }
	}

	public function recurseCategoryIds($indexedCategories, $categories, $current, $id_category = 1, $id_category_default = NULL)
	{
		$done = $this->context->done;
		// set variables
		static $_idsCat;
		
		if ($id_category == 1) {
			$_idsCat = null;
		}
		if (!isset($done[$current['infos']['id_parent']]))
			$done[$current['infos']['id_parent']] = 0;
		$done[$current['infos']['id_parent']] += 1;
		$_idsCat[] = (string)$id_category;
		if (isset($categories[$id_category]))
			foreach ($categories[$id_category] AS $key => $row)
				if ($key != 'infos')
					$this->recurseCategoryIds($indexedCategories, $categories, $categories[$id_category][$key], $key, $id_category_default, $row);
		return $_idsCat;
	}
	
	public function getIdsCategories(){
		/// get all category ids ///
		$cookie = $this->context->cookie;
		$cat = new Category();
		$list_cat = $cat->getCategories($cookie->id_lang);
		$current_cat = Category::getRootCategory()->id;
		$cat_ids = $this->recurseCategoryIds($list_cat, $list_cat, $current_cat);
		$cat_ids = implode(",",$cat_ids);
		return $cat_ids;
		/// get all category ids ///
	}
    
	public function hideCategoryPosition($name)
	{
		return preg_replace('/^[0-9]+\./', '', $name);
	}

	public function _productData($data){
		$product = $data['product'];
		if(is_object($product) && !empty($product->id)){
		$cookie = $this->context->cookie;
		$id_lang = isset($data['id_lang'])?$data['id_lang']:(int)($cookie->id_lang);	
		
			/* Product URL */
			$link = Context::getContext()->link;

            $id_shop = Context::getContext()->shop->id;
            if (version_compare(_PS_VERSION_, '1.5.5', '>=') && version_compare(_PS_VERSION_, '1.7', '<')) {
                $product_url = $link->getProductLink((int)$product->id, null, null, null,
                    (int)$id_lang, (int)$id_shop, 0, false);

            } else {
                $id_product_attribute = $product->getDefaultAttribute((int)$product->id);
                $product_url = $link->getProductLink((int)$product->id, $product->link_rewrite[$id_lang], null, null, $id_lang, null, $id_product_attribute, false, false, true);
                $product_url = preg_replace('/#.*/im', '', $product_url);
            }
			/* Image */
			$image = Image::getCover((int)($product->id));

			if ($image)
			{
                $block = isset($data['block'])?$data['block']:'';
				$available_types = ImageType::getImagesTypes('products');
                switch($block){
                    case 'reminder':
                        $type_img = Configuration::get($this->name.'img_size_em');
                        break;
                    default;
                        foreach ($available_types as $type){
                            $width = $type['width'];
                            if($width>400){
                                $type_img = $type['name'];
                                break;
                            }
                        }
                    break;
                }

				$image_link = $link->getImageLink(@$product->link_rewrite[$id_lang], (int)($product->id).'-'.(int)($image['id_image']),$type_img);
			}
			else
			{
				$image_link = false;
				
			}
        }else {
			$image_link= false;
			$product_url = false;
		}     
        return array('product_url'=>$product_url,'image_link'=>$image_link);
	}

    public function getIdLang(){
        $cookie = $this->context->cookie;
        $id_lang = (int)($cookie->id_lang);
        return $id_lang;
    }

    public function getHttpost(){
            $custom_ssl_var = 0;
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (bool)Configuration::get('PS_SSL_ENABLED'))
                $custom_ssl_var = 1;

            if ($custom_ssl_var == 1)
                $_http_host = _PS_BASE_URL_SSL_.__PS_BASE_URI__;
            else
                $_http_host = _PS_BASE_URL_SSL_.__PS_BASE_URI__;

        return $_http_host;
    }

    public function setSEOUrls(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();
        $obj_spmgsnipreviewhelp->setSEOUrls();
        $smarty = $this->context->smarty;
        $smarty->assign($this->name.'is16', $this->_is16);
        $smarty->assign($this->name.'is_ps15', $this->_is15);
        $smarty->assign($this->name.'pic', $this->path_img_cloud);

        $is_rewrite = 0;
        if (Configuration::get('PS_REWRITING_SETTINGS')) {
            $is_rewrite = 1;
        }
        $smarty->assign($this->name.'is_rewrite', $is_rewrite);

    }

    public function setuserprofilegSettings(){

        $smarty = $this->context->smarty;
        $smarty->assign($this->name.'is_uprof', Configuration::get($this->name.'is_uprof'));

        if(Configuration::get($this->name.'is_uprof') == 1) {

            $this->setSEOUrls();

            include_once(_PS_MODULE_DIR_.$this->name . '/classes/userprofileg.class.php');
            $obj_userprofileg = new userprofileg();

            $info_customers = $obj_userprofileg->getShoppersBlock(
                array(
                    'start' => 0,
                    'step' => (int)Configuration::get($this->name . $this->_prefix_review . 'shoppers_blc')
                )
            );

            $smarty->assign(array(
                $this->name . 'customers_block' => $info_customers['customers']
            ));

            $smarty->assign($this->name . 'sr_sliderhu', (int)Configuration::get($this->name . 'sr_sliderhu'));
            $smarty->assign($this->name . 'sr_slhu', (int)Configuration::get($this->name . 'sr_slhu'));
            $smarty->assign($this->name . 'sr_slideru', (int)Configuration::get($this->name . 'sr_slideru'));
            $smarty->assign($this->name . 'sr_slu', (int)Configuration::get($this->name . 'sr_slu'));
        }
    }

    public function setStoreReviewsColumnsSettings(){
        $smarty = $this->context->smarty;
        $smarty->assign($this->name.'is_storerev', Configuration::get($this->name.'is_storerev'));

        if(Configuration::get($this->name.'is_storerev') == 1) {

            include_once(_PS_MODULE_DIR_.$this->name . '/classes/storereviews.class.php');
            $obj_storereviews = new storereviews();
            $_data_l = $obj_storereviews->getTestimonials(array('start' => 0, 'step' => Configuration::get($this->name . 'tlast_l')));
            $_data_ls = $obj_storereviews->getTestimonials(array('start' => 0, 'step' => Configuration::get($this->name . 'tlast_ls')));
            $_data_r = $obj_storereviews->getTestimonials(array('start' => 0, 'step' => Configuration::get($this->name . 'tlast_r')));
            $_data_rs = $obj_storereviews->getTestimonials(array('start' => 0, 'step' => Configuration::get($this->name . 'tlast_rs')));
            $_data_f = $obj_storereviews->getTestimonials(array('start' => 0, 'step' => Configuration::get($this->name . 'tlast_f')));
            $_data_h = $obj_storereviews->getTestimonials(array('start' => 0, 'step' => Configuration::get($this->name . 'tlast_h')));

            $smarty->assign(
                array(
                    $this->name . 'reviews_l' => $_data_l['reviews'],
                    $this->name . 'reviews_ls' => $_data_ls['reviews'],
                    $this->name . 'reviews_r' => $_data_r['reviews'],
                    $this->name . 'reviews_rs' => $_data_rs['reviews'],
                    $this->name . 'reviews_f' => $_data_f['reviews'],
                    $this->name . 'reviews_h' => $_data_h['reviews'],

                    $this->name . 'count_all_reviews' => $_data_l['count_all_reviews'])
            );

            $this->setStoreReviewsSettings();

            $this->_setPositionsforStoreWidget();

            $this->setSEOUrls();

            $smarty->assign($this->name . 'sr_slider', (int)Configuration::get($this->name . 'sr_slider'));
            $smarty->assign($this->name . 'sr_sl', (int)Configuration::get($this->name . 'sr_sl'));

            $smarty->assign($this->name . 'sr_sliderh', (int)Configuration::get($this->name . 'sr_sliderh'));
            $smarty->assign($this->name . 'sr_slh', (int)Configuration::get($this->name . 'sr_slh'));

        }
    }

    public function setStoreReviewsSettings(){
        $smarty = $this->context->smarty;
        $cookie = $this->context->cookie;

        $smarty->assign($this->name.'is_web', Configuration::get($this->name.'is_web'));
        $smarty->assign($this->name.'rssontestim', Configuration::get($this->name.'rssontestim'));
        $smarty->assign($this->name.'is_addr', Configuration::get($this->name.'is_addr'));
        $smarty->assign($this->name.'is_country', Configuration::get($this->name.'is_country'));
        $smarty->assign($this->name.'is_city', Configuration::get($this->name.'is_city'));
        $smarty->assign($this->name.'is_captcha', Configuration::get($this->name.'is_captcha'));
        $smarty->assign($this->name.'is_filterall'.$this->_prefix_shop_reviews, Configuration::get($this->name.'is_filterall'.$this->_prefix_shop_reviews));
        $smarty->assign($this->name.'is_sortf'.$this->_prefix_shop_reviews, Configuration::get($this->name.'is_sortf'.$this->_prefix_shop_reviews));
        $smarty->assign($this->name.'is_sortfu'.$this->_prefix_shop_reviews, Configuration::get($this->name.'is_sortfu'.$this->_prefix_shop_reviews));
        $smarty->assign($this->name.'is_company', Configuration::get($this->name.'is_company'));
        $smarty->assign($this->name.'is_avatar', Configuration::get($this->name.'is_avatar'));
        $smarty->assign($this->name.'whocanadd'.$this->_prefix_shop_reviews, Configuration::get($this->name.'whocanadd'.$this->_prefix_shop_reviews));

        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();
        $avg_rating = $obj_storereviews->getAvgReview();
        $smarty->assign($this->name.'avg_rating'.$this->_prefix_shop_reviews, $avg_rating['avg_rating']);
        $smarty->assign($this->name.'avg_decimal'.$this->_prefix_shop_reviews, $avg_rating['avg_rating_decimal']);

        $count_reviews = $obj_storereviews->getCountReviews();
        $smarty->assign($this->name.'count_reviews'.$this->_prefix_shop_reviews, $count_reviews);

        $smarty->assign($this->name.'sh_name'.$this->_prefix_shop_reviews, @Configuration::get('PS_SHOP_NAME'));

        $_http_host = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;

        $smarty->assign($this->name.'sh_url'.$this->_prefix_shop_reviews, $_http_host);
        $smarty->assign($this->name.'t_lefts', ($count_reviews == 0)?0:Configuration::get($this->name.'t_lefts'));
        $smarty->assign($this->name.'t_rights', ($count_reviews == 0)?0:Configuration::get($this->name.'t_rights'));
        $smarty->assign($this->name.'t_footers', ($count_reviews == 0)?0:Configuration::get($this->name.'t_footers'));
        $smarty->assign($this->name.'t_homes', ($count_reviews == 0)?0:Configuration::get($this->name.'t_homes'));
        $smarty->assign($this->name.'t_leftsides', ($count_reviews == 0)?0:Configuration::get($this->name.'t_leftsides'));
        $smarty->assign($this->name.'t_rightsides', ($count_reviews == 0)?0:Configuration::get($this->name.'t_rightsides'));
        $smarty->assign($this->name.'t_tpages', ($count_reviews == 0)?0:Configuration::get($this->name.'t_tpages'));
        $smarty->assign($this->name.'is_files'.$this->_prefix_shop_reviews, Configuration::get($this->name.'is_files'.$this->_prefix_shop_reviews));
        $smarty->assign($this->name.'ruploadfiles'.$this->_prefix_shop_reviews, Configuration::get($this->name.'ruploadfiles'.$this->_prefix_shop_reviews));
        $smarty->assign($this->name.'fpath'.$this->_prefix_shop_reviews, $this->path_img_cloud);

        $smarty->assign($this->name.'is_ps15', $this->_is15);
        $smarty->assign($this->name.'is15', $this->_is15);
        if(version_compare(_PS_VERSION_, '1.5', '<')){
            $smarty->assign($this->name.'is14', 1);
        } else {
            $smarty->assign($this->name.'is14', 0);
        }

        $this->_setPositionsforStoreWidget();
        $smarty->assign($this->name.'is_storerev', Configuration::get($this->name.'is_storerev'));

        // voucher //

        $data_permissions_vouchers = $obj_storereviews->getGroupPermissionsForVouchers();
        $is_show_voucher = $data_permissions_vouchers['is_show_voucher'];
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/spmgsnipreviewhelp.class.php');
        $obj = new spmgsnipreviewhelp();

        $smarty->assign($this->name.'vis_on'.$this->_prefix_shop_reviews, Configuration::get($this->name.'vis_on'.$this->_prefix_shop_reviews));

        // set discount
        switch (Configuration::get($this->name.'discount_type'.$this->_prefix_shop_reviews))
        {
            case 1:
                // percent
                $id_discount_type = 1;
                $value = Configuration::get($this->name.'percentage_val'.$this->_prefix_shop_reviews);
                $id_currency = (int)$cookie->id_currency;
                break;
            case 2:
                // currency
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get('sdamount'.$this->_prefix_shop_reviews.'_'.(int)$id_currency);
                break;
            default:
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get('sdamount'.$this->_prefix_shop_reviews.'_'.(int)$id_currency);
        }
        $valuta = "%";
        $tax = '';

        if($id_discount_type == 2){

            $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
            $id_currency = (int)$cookie->id_currency;
            foreach ($cur AS $_cur){
                if(
                    $id_currency == $_cur['id_currency']
                ){
                    $valuta = $_cur['sign'];
                }
            }
            $tax = (int)Configuration::get($this->name.'tax'.$this->_prefix_shop_reviews);
        }

        $smarty->assign($this->name.'tax'.$this->_prefix_shop_reviews, $tax);
        $smarty->assign($this->name.'discount'.$this->_prefix_shop_reviews, $value.' '.$valuta);
        $smarty->assign($this->name.'valuta'.$this->_prefix_shop_reviews, $valuta);
        $smarty->assign($this->name.'sdvvalid'.$this->_prefix_shop_reviews, Configuration::get($this->name.'sdvvalid'.$this->_prefix_shop_reviews));
        $smarty->assign($this->name.'days'.$this->_prefix_shop_reviews, $obj->number_ending(Configuration::get($this->name.'sdvvalid'.$this->_prefix_shop_reviews), $this->l('days'), $this->l('day'), $this->l('days')));
        // minimum checkout //
        $smarty->assign($this->name.'is_show_min'.$this->_prefix_shop_reviews, Configuration::get($this->name.'is_show_min'.$this->_prefix_shop_reviews));
        $smarty->assign($this->name.'isminamount'.$this->_prefix_shop_reviews, Configuration::get($this->name.'isminamount'.$this->_prefix_shop_reviews));
        $smarty->assign($this->name.'minamount'.$this->_prefix_shop_reviews, Configuration::get('sdminamount'.$this->_prefix_shop_reviews.'_'.(int)$id_currency));
        $fvaluta_min = '';

        $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
        $id_currency = (int)$cookie->id_currency;
        foreach ($cur AS $_cur){
            if(
                $id_currency == $_cur['id_currency']
            ){
                $fvaluta_min = $_cur['sign'];
            }
        }

        $smarty->assign($this->name.'curtxt'.$this->_prefix_shop_reviews, $fvaluta_min);
        // minimum checkout //
        $smarty->assign($this->name.'is_show_voucher'.$this->_prefix_shop_reviews, $is_show_voucher);
        // voucher //
    }

    private function _setPositionsforStoreWidget(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/storereviews.class.php');
        $obj_shopreviews = new storereviews();
        $obj_shopreviews->setPositionsforStoreWidget();
    }

    private function _effects($data){

        $value = $data['value'];
        $current_value = Configuration::get($this->name.$value);
        $all_effects = array();

        $all_effects[$this->l('Disable All Effects')] = array('disable_all_effects');
        $all_effects[$this->l('Attention Seekers')] = array('bounce','flash','pulse','rubberBand','shake','swing','tada','wobble','jello');
        $all_effects[$this->l('Bouncing Entrances')] = array('bounceIn','bounceInDown','bounceInLeft','bounceInRight','bounceInUp');
        $all_effects[$this->l('Bouncing Exits')] = array('bounceOut','bounceOutDown','bounceOutLeft','bounceOutRight','bounceOutUp');
        $all_effects[$this->l('Fading Entrances')] = array('fadeIn','fadeInDown','fadeInDownBig','fadeInLeft','fadeInLeftBig','fadeInRight','fadeInRightBig','fadeInUp','fadeInUpBig');
        $all_effects[$this->l('Flippers')] = array('flip','flipInX','flipInY','flipOutX','flipOutY');
        $all_effects[$this->l('Lightspeed')] = array('lightSpeedIn','lightSpeedOut');
        $all_effects[$this->l('Rotating Entrances')] = array('rotateIn','rotateInDownLeft','rotateInDownRight','rotateInUpLeft','rotateInUpRight');
        $all_effects[$this->l('Rotating Exits')] = array('rotateOut','rotateOutDownLeft','rotateOutDownRight','rotateOutUpLeft','rotateOutUpRight');
        $all_effects[$this->l('Sliding Entrances')] = array('slideInUp','slideInDown','slideInLeft','slideInRight');
        $all_effects[$this->l('Sliding Exits')] = array('slideOutUp','slideOutDown','slideOutLeft','slideOutRight');
        $all_effects[$this->l('Zoom Entrances')] = array('zoomIn','zoomInDown','zoomInLeft','zoomInRight','zoomInUp');
        $all_effects[$this->l('Zoom Exits')] = array('zoomOut','zoomOutDown','zoomOutLeft','zoomOutRight','zoomOutUp');
        $all_effects[$this->l('Specials')] = array('hinge','rollIn','rollOut');

        ob_start();
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();

        return $_html;
    }

    public function clearSmartyCacheItems(){
        include_once(_PS_MODULE_DIR_.$this->name.'/classes/cachespmgsnipreview.class.php');
        $cache = new cachespmgsnipreview();
        $cache->clearSmartyCacheModule();
    }

    private function _linkAHtml($data){

        $title_a = $data['title'];
        $href_a = $data['href'];
        $is_b = isset($data['is_b'])?$data['is_b']:0;
        $onclick = isset($data['onclick'])?$data['onclick']:0;
        $disable_target_blank = isset($data['disable_target_blank'])?$data['disable_target_blank']:0;
        $br_after_count = isset($data['br_after_count'])?$data['br_after_count']:0;

        $_html = '';
        include(dirname(__FILE__).'/views/templates/hooks/'.__FUNCTION__.'.phtml');


        return $_html;
    }
}


// stub for prestashop 1.7.4.0

if (!function_exists('idn_to_ascii'))
{
    function idn_to_ascii($email)
    {
        return $email;
    }
}