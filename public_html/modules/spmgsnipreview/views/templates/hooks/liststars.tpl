{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}

{if ($spmgsnipreviewis_starscat == 1 && $count_review > 0) || ($spmgsnipreviewis_starscat == 0)}
<div class="clear"></div>
<div class="reviews_list_stars">
    <span class="star_content clearfix">


        {assign var='test_rating' value=$avg_rating}

        {section name=ratid loop=5}
            {if $smarty.section.ratid.index < $avg_rating}

                {if $test_rating <= $spmgsnipreviewmax_star_par && $test_rating >= $spmgsnipreviewmin_star_par}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-category"
                         alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                {else}
                    <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-category"
                        alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                {/if}

            {else}
            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star-category"
                alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
            {/if}
            {$test_rating = $test_rating - 1}
        {/section}

    </span>
    <span>{$count_review|escape:'htmlall':'UTF-8'}&nbsp;{l s='Review(s)' mod='spmgsnipreview'}&nbsp</span>
</div>
{/if}


