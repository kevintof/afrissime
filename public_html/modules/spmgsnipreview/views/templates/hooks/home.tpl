{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewrvis_on == 1}
{if $spmgsnipreviewallinfo_on == 1 && $spmgsnipreviewis_home_b_home == 1 && $spmgsnipreviewsvis_on == 1}

    {if count($spmgsnipreviewdata_badges)>0}


        <div class="clear-spmgsnipreview"></div>
        <div class="badges" style="width:{$spmgsnipreviewallinfoh_w|escape:'htmlall':'UTF-8'}%">

            <strong class="title-badges">{l s='Review(s) and rating(s)' mod='spmgsnipreview'}</strong>
		<span itemscope itemtype="http://schema.org/Product">
			<meta content="{$spmgsnipreviewbadges_name|escape:'htmlall':'UTF-8'}" itemprop="name">
            <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
			<span>
                {section name=ratid loop=5}
                    {if $smarty.section.ratid.index <= $spmgsnipreviewdata_badges.total_rating|round}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                    {else}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                    {/if}

                {/section}
			</span>

			<meta content="1" itemprop="worstRating" />
			(<span itemprop="ratingValue">{$spmgsnipreviewdata_badges.total_rating|escape:'htmlall':'UTF-8'}</span>/<span itemprop="bestRating">5</span>)


			<strong>{if $spmgsnipreviewis_home_b_home != 0}{l s='Shop' mod='spmgsnipreview'}{/if}
                {if $spmgsnipreviewis_cat_b_home != 0}{l s='Category' mod='spmgsnipreview'}{/if}
                {if $spmgsnipreviewis_man_b_home != 0}{l s='Brand' mod='spmgsnipreview'}{/if} :</strong>
             <span itemprop="itemReviewed">{$spmgsnipreviewbadges_name|escape:'htmlall':'UTF-8'}</span> -
                {l s='Based on' mod='spmgsnipreview'} <span itemprop="ratingCount">{$spmgsnipreviewdata_badges.total_reviews|escape:'htmlall':'UTF-8'}</span> {l s='rating(s)' mod='spmgsnipreview'}
                {l s='and' mod='spmgsnipreview'} <span itemprop="reviewCount">{$spmgsnipreviewdata_badges.total_reviews|escape:'htmlall':'UTF-8'}</span> {l s='review(s)' mod='spmgsnipreview'}
		</span>
            </span>

            &nbsp; - &nbsp;<a href="{$spmgsnipreviewrev_all|escape:'htmlall':'UTF-8'}"
                              title="{l s='View All Reviews' mod='spmgsnipreview'}"><span>{l s='View All Reviews' mod='spmgsnipreview'}</span></a>



        </div>

    {/if}

{/if}
{/if}


{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}

{if $spmgsnipreviewis_blocklr == 1 && $spmgsnipreviewblocklr_home == "blocklr_home" && $spmgsnipreviewblocklr_home_pos == "home"}



    <div class="clear-spmgsnipreview"></div>
<div {if $spmgsnipreviewis_ps15 == 1 && $spmgsnipreviewis17 == 0}id="left_column"{/if}>


<div id="spmgsnipreview_block_left" class="block-last-spmgsnipreviews block
                                        {if $spmgsnipreviewsr_sliderhr == 1}owl_reviews_home_type_carousel{/if}
                                        blockmanufacturer {if $spmgsnipreviewis17 == 1}block-categories{/if}"
                                style="width:{$spmgsnipreviewblocklr_home_w|escape:'htmlall':'UTF-8'}%">

    	<h4 class="title_block {if $spmgsnipreviewis17 == 1}text-uppercase h6{/if}">
			<div class="spmgsnipreviews-float-left">
			{l s='Last Product Reviews' mod='spmgsnipreview'}
			</div>
			<div class="spmgsnipreviews-float-left margin-left-5">
			{if $spmgsnipreviewrsson == 1}
				<a href="{$spmgsnipreviewrss_url nofilter}" target="_blank" title="{l s='RSS Feed' mod='spmgsnipreview'}">
					<img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/feed.png" alt="{l s='RSS Feed' mod='spmgsnipreview'}" />
                </a>
			{/if}
			</div>
			<div class="spmgsnipreviews-clear"></div>
		</h4>
		<div class="block_content block-items-data row-custom">
			{if count($spmgsnipreviewreviews_home)>0}


            {if $spmgsnipreviewsr_sliderhr == 1}
            <ul class="owl-carousel owl-theme">
            {/if}

			{foreach from=$spmgsnipreviewreviews_home item=review name=myLoop}

                {if $spmgsnipreviewsr_sliderhr == 1}<div class="current-item-block">{/if}

                <div class="items-last-spmgsnipreviews ">
                {if $review.product_img}
                    <div class="img-block-spmgsnipreview col-sm-{if $spmgsnipreviewsr_sliderhr == 1}3{else}2{/if}-custom">
                        <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                           title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                >
                            <img src="{$review.product_img|escape:'htmlall':'UTF-8'}" title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                 alt = "{$review.product_name|escape:'htmlall':'UTF-8'}" class="border-image-review img-responsive" />
                        </a>
                    </div>
                {/if}
                <div class="body-block-spmgsnipreview col-sm-{if $spmgsnipreviewsr_sliderhr == 1}9{else}10{/if}-custom {if !$review.product_img}body-block-spmgsnipreview-100{/if}">
                    <div class="title-block-last-spmgsnipreview">


                    <div class="r-product">
                        {l s='By' mod='spmgsnipreview'}
                        {if $spmgsnipreviewis_avatarr == 1 && strlen($review.avatar)>0 && $review.is_show_ava == 1}

                            <span class="avatar-block-rev">
                                        <img alt="{$review.customer_name|escape:'htmlall':'UTF-8' nofilter}"
                                             src="{$review.avatar|escape:'htmlall':'UTF-8'}">
                            </span>

                        {/if}

                        {if strlen($review.customer_name)>0}
                            {if $spmgsnipreviewis_uprof && $review.id_customer > 0 && $review.is_show_ava == 1}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.customer_name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}<strong
                            >{$review.customer_name|escape:'htmlall':'UTF-8' nofilter}</strong>{if $spmgsnipreviewis_uprof && $review.id_customer > 0 && $review.is_show_ava == 1}</a>{/if}
                        {/if}
                        {if strlen($review.customer_name)>0}{l s='on' mod='spmgsnipreview'}{/if}&nbsp;<strong>{$review.time_add|date_format|escape:'html':'UTF-8'}</strong>


                    </div>

                        {if $review.is_active == 1}
                            {if $spmgsnipreviewratings_on == 1 && $review.rating != 0}
                            <div  class="rating-stars-total-block">
                               ({$review.rating|escape:'html':'UTF-8'}/5)
                            </div>
                            <div class="r-rating">
                                        {section name=ratid loop=5}
                                            {if $smarty.section.ratid.index < $review.rating}
                                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                            {else}
                                                <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" class="gsniprev-img-star" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                                            {/if}

                                        {/section}
                            </div>
                            {/if}
                        {/if}
                        <div class="clear-spmgsnipreview"></div>

                        {if $review.is_buy != 0}
                            <span class="spmgsnipreview-block-date float-left">
                                            <span class="is_buy_product is_buy_product_block">{l s='Verified Purchase' mod='spmgsnipreview'}</span>
                                        </span>
                            <div class="clear-spmgsnipreview"></div>
                        {/if}
                    </div>

                    <div class="title-block-r">
                        <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                           title="{$review.product_name|escape:'htmlall':'UTF-8'}"
                                >
                            {$review.product_name|escape:'htmlall':'UTF-8'}
                        </a>
                    </div>


                    {if $review.is_active == 1}

                    {if $spmgsnipreviewtext_on == 1 && strlen($review.text_review)>0}
                                <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
                                   title="{$review.text_review|escape:'quotes':'UTF-8':strip_tags|substr:0:$spmgsnipreviewblocklr_home_tr|escape:'quotes':'UTF-8'}"
                                   >
                                    {$review.text_review|strip_tags|substr:0:$spmgsnipreviewblocklr_home_tr|escape:'htmlall':'UTF-8' nofilter}{if strlen($review.text_review)>$spmgsnipreviewblocklr_home_tr}...{/if}
                                </a>
                    {/if}

                    {else}

                        {l s='The customer has rated the product but has not posted a review, or the review is pending moderation' mod='spmgsnipreview'}
	    			{/if}

                </div>
                    <div class="clear-spmgsnipreview"></div>
                </div>

                {if $spmgsnipreviewsr_sliderhr == 1}</div>{/if}

	    	{/foreach}

                {if $spmgsnipreviewsr_sliderhr == 1}
                    </ul>
                {/if}

                <div class="gsniprev-view-all float-right">
                    <a href="{$spmgsnipreviewallr_url|escape:'html':'UTF-8'}"
                       class="btn btn-default button button-small-spmgsnipreview"
                            >
                        <span>{l s='View All Reviews' mod='spmgsnipreview'}</span>
                    </a>
                </div>
                <div class="clear-spmgsnipreview"></div>

	    	{else}
	    		<div class="gsniprev-block-noreviews">
					{l s='There are not Product Reviews yet.' mod='spmgsnipreview'}
				</div>
	    	{/if}
	    </div>
</div>


</div>

{/if}

{/if}

{/if}




{if $spmgsnipreviewis_storerev == 1}
{if $spmgsnipreviewt_home == 1}
    {if ($spmgsnipreviewis_mobile == 1 && $spmgsnipreviewmt_home == 1) || (!$spmgsnipreviewis_mobile == 1 && $spmgsnipreviewst_home == 1)}
    <div {if $spmgsnipreviewis_ps15 == 1 && $spmgsnipreviewis17 == 0}id="left_column"{/if}>
        <div id="testimonials_block_left"
             class="block
                    {if $spmgsnipreviewsr_sliderh == 1}owl_storereviews_home_type_carousel{/if}
                    myaccount {if $spmgsnipreviewis16 == 1}spmgsnipreview-block16{/if}
                    margin-top-10 {if $spmgsnipreviewis17 == 1}block-categories{else}ps15-color-background{/if}">
            <h4 class="title_block {if $spmgsnipreviewis16 == 1}testimonials-block-h4{/if}
		{if $spmgsnipreviewis_ps15 == 1 && $spmgsnipreviewis16 == 0}testimonials-block-15{/if} {if $spmgsnipreviewis17 == 1}text-uppercase h6{/if}">
                <div class="float-left">
                    <a href="{$spmgsnipreviewstorereviews_url|escape:'htmlall':'UTF-8'}"
                            >{l s='Store Reviews' mod='spmgsnipreview'}&nbsp;(&nbsp;{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}&nbsp;)</a>

                </div>


                <div {if $spmgsnipreviewt_homes == 1}itemscope itemtype="http://schema.org/corporation"{/if} class="total-rating-items-block float-left margin-left-5 {if $spmgsnipreviewis16 == 1}margin-top-3{/if}">

                    {if $spmgsnipreviewt_homes == 1}
                        <meta itemprop="name" content="{$spmgsnipreviewsh_nameti|escape:'htmlall':'UTF-8'}">
                        <meta itemprop="url" content="{$spmgsnipreviewsh_urlti|escape:'htmlall':'UTF-8'}">
                    {/if}


                    <div {if $spmgsnipreviewt_homes == 1}itemtype="http://schema.org/AggregateRating" itemscope="" itemprop="aggregateRating"{/if}>

                        {if $spmgsnipreviewt_homes == 1}
                            <meta itemprop="worstRating" content="1">
                            <meta itemprop="ratingCount" content="{$spmgsnipreviewcount_reviewsti|escape:'htmlall':'UTF-8'}">
                        {/if}


                        {assign var='test_rating_store' value=$spmgsnipreviewavg_decimalti|replace:',':'.'}

                        {section name=ratid loop=5 start=0}
                            {if $smarty.section.ratid.index < $spmgsnipreviewavg_ratingti}

                                {if $test_rating_store <= $spmgsnipreviewmax_star_par && $test_rating_store >= $spmgsnipreviewmin_star_par}
                                    <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestarh|escape:'htmlall':'UTF-8'}"/>
                                {else}
                                    <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                {/if}

                            {else}
                                <img alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                            {/if}
                            {$test_rating_store = $test_rating_store - 1}
                        {/section}


                        <span {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}>
                        (<span {if $spmgsnipreviewt_homes == 1}itemprop="ratingValue"{/if} {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}
                                    >{$spmgsnipreviewavg_decimalti|escape:'htmlall':'UTF-8'}</span>/<span {if $spmgsnipreviewt_homes == 1}itemprop="bestRating"{/if} {if $spmgsnipreviewis_ps15 == 0}class="vertical-align-top"{/if}
                                    >5</span>)
                        </span>

                    </div>
                </div>

                <div class="float-left margin-left-5">
                    {if $spmgsnipreviewrssontestim == 1}
                        <a href="{$spmgsnipreviewrss_testimonials_url nofilter}" title="{l s='RSS Feed' mod='spmgsnipreview'}" target="_blank">
                            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/feed.png" alt="{l s='RSS Feed' mod='spmgsnipreview'}" />
                        </a>
                    {/if}
                </div>
                <div class="clear"></div>

            </h4>

            <div class="block_content products-block">
                {if count($spmgsnipreviewreviews_h) > 0}

                    {if $spmgsnipreviewsr_sliderh == 1}
                        <ul class="owl-carousel owl-theme">
                    {/if}

                    {foreach from=$spmgsnipreviewreviews_h item=review name=myLoop}


                    {if $spmgsnipreviewsr_sliderh == 1}<div class="current-item-block">{/if}

                        <div class="rItem testimonials-width-auto" >
                            <div class="ratingBox text-align-right">
                                <small>{l s='Review By' mod='spmgsnipreview'} <b>{if $spmgsnipreviewis_uprof && $review.is_show_ava && $review.id_customer > 0}<a href="{$spmgsnipreviewuser_url|escape:'htmlall':'UTF-8'}{$review.id_customer|escape:'htmlall':'UTF-8'}" title="{$review.name|escape:'htmlall':'UTF-8'}" class="user-link-to-profile">{/if}{$review.name|escape:'htmlall':'UTF-8' nofilter}{if $spmgsnipreviewis_uprof && $review.id_customer > 0}</a>{/if}</b></small>{if $spmgsnipreviewis_country == 1}{if strlen($review.country)>0}, <span class="fs-12">{$review.country|escape:'htmlall':'UTF-8' nofilter}</span>{/if}{/if}{if $spmgsnipreviewis_city == 1}{if strlen($review.city)>0}, <span class="fs-12">{$review.city|escape:'htmlall':'UTF-8' nofilter}</span>{/if}{/if}


                                {if $review.rating != 0}
                                    {section name=bar loop=5 start=0}
                                        {if $smarty.section.bar.index < $review.rating}
                                            <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" />
                                        {else}
                                            <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" />
                                        {/if}
                                    {/section}
                                {else}
                                    {section name=bar loop=5 start=0}
                                        <img alt="{$smarty.section.bar.index|escape:'htmlall':'UTF-8'}" src = "{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/rating_star2.png" />
                                    {/section}
                                {/if}

                            </div>


                            <div class="clear"></div>
                            <div>
                                {if $spmgsnipreviewis_avatar == 1 && $review.is_show_ava}
                                    <div class="float-left avatar-block-home">
                                        <img
                                                {if strlen($review.avatar)>0}
                                                    src="{$review.avatar|escape:'htmlall':'UTF-8'}"
                                                {else}
                                                    src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/avatar_m.gif"
                                                {/if}
                                                alt="{$review.name|escape:'htmlall':'UTF-8'}"
                                                />
                                    </div>
                                {/if}

                                <div class="{if $spmgsnipreviewis17 == 0}font-size-11{/if} float-left testimonial-block-text-home">
                                    {$review.message|substr:0:245|escape:'htmlall':'UTF-8' nofilter}
                                    {if strlen($review.message)>245}...{/if}

                                </div>
                                <div class="clear"></div>
                            </div>


                            <small class="float-right">{$review.date_add|date_format|escape:'htmlall':'UTF-8'}</small>


                            {if $spmgsnipreviewis_web == 1}
                                {if strlen($review.web)>0}
                                    <small class="float-right margin-right-10">
                                        <a title="http://{$review.web|escape:'htmlall':'UTF-8'}" rel="nofollow" href="http://{$review.web|escape:'htmlall':'UTF-8'}"
                                           target="_blank" class="testimonials-link-web"
                                                >http://{$review.web|escape:'htmlall':'UTF-8'}</a>
                                    </small>
                                {/if}
                            {/if}
                            <div class="clear"></div>
                            <span class="float-right">{if $review.is_buy != 0}<span class="is_buy">{l s='Verified Purchase' mod='spmgsnipreview'}</span>{/if}</span>
                            <div class="clear"></div>

                        </div>


                        {if $spmgsnipreviewsr_sliderh == 1}</div>{/if}

                    {/foreach}

                     {if $spmgsnipreviewsr_sliderh == 1}
                        </ul>
                     {/if}


                    <div class="gsniprev-view-all float-right">
                        <a href="{$spmgsnipreviewstorereviews_url|escape:'html':'UTF-8'}"
                           class="btn btn-default button button-small-spmgsnipreview"
                                >
                            <span>{l s='View All Store Reviews' mod='spmgsnipreview'}</span>
                        </a>
                    </div>
                    <div class="clear-spmgsnipreview"></div>
                {else}
                    <div class="rItem no-items-shopreviews testimonials-width-auto" >
                        {l s='There are not Store Reviews yet.' mod='spmgsnipreview'}
                    </div>
                {/if}




            </div>

        </div>

    </div>
{/if}
{/if}
{/if}




{if $spmgsnipreviewis17 == 1}<br/>{/if}
{if $spmgsnipreviewis_uprof == 1}
    {if $spmgsnipreviewradv_home == 1}

        {if $spmgsnipreviewis16 == 1 && $spmgsnipreviewis17 == 0}
            <div id="left_column">
        {/if}

        <div id="spmgsnipreview_block_home_users"
             class="block
                    {if $spmgsnipreviewsr_sliderhu == 1}owl_users_home_type_carousel{/if}
                    {if $spmgsnipreviewis16 == 1}blockmanufacturer16{else}blockmanufacturer{/if} {if $spmgsnipreviewis17 == 1}block-categories{/if}
                    ">
            <h4  class="title_block {if $spmgsnipreviewis17 == 1}text-uppercase h6{/if}" {if $spmgsnipreviewis16 != 1}align="center"{/if}>
                <a href="{$spmgsnipreviewshoppers_url|escape:'htmlall':'UTF-8'}"
                        >{l s='Users' mod='spmgsnipreview'}</a>
            </h4>
            <div class="block_content">
                {if count($spmgsnipreviewcustomers_block)>0}
                    <ul class="users-block-items {if $spmgsnipreviewsr_sliderhu == 1}owl-carousel owl-theme{else}home-shoppers{/if}">

                        {foreach from=$spmgsnipreviewcustomers_block item=customer name=myLoop}

                        {*{if $spmgsnipreviewsr_sliderhu == 1}<div class="current-item-block">{/if}*}

                            <li class="float-left border-bottom-none {if $spmgsnipreviewsr_sliderhu == 1}current-item-block"{/if}">
                                <img src="{$customer.avatar_thumb|escape:'htmlall':'UTF-8'}"
                                     class="user-img-spmgsnipreview"
                                     title="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}"
                                     alt = "{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}" />
                                <a href="{$spmgsnipreviewshopper_url|escape:'htmlall':'UTF-8'}{$customer.id_customer|escape:'htmlall':'UTF-8'}"
                                   title="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}">
                                    {$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}
                                </a>
                                <div class="clr"></div>
                            </li>


                            {*{if $spmgsnipreviewsr_sliderhu == 1}</div>{/if}*}

                        {/foreach}

                    </ul>
                    <div class="clr"></div>

                    <div class="gsniprev-view-all float-right">
                        <a href="{$spmgsnipreviewshoppers_url|escape:'html':'UTF-8'}"
                           class="btn btn-default button button-small-spmgsnipreview"
                                >
                            <span>{l s='View All Users' mod='spmgsnipreview'}</span>
                        </a>
                    </div>
                    <div class="clear-spmgsnipreview"></div>
                {else}
                    <div class="padding-10">
                        {l s='There are not users yet.' mod='spmgsnipreview'}
                    </div>
                {/if}

            </div>
        </div>

        {if $spmgsnipreviewis16 == 1 && $spmgsnipreviewis17 == 0}
            </div>
        {/if}

    {/if}
{/if}


{literal}
<script type="text/javascript">
{/literal}
{if $spmgsnipreviewis_storerev == 1}
    {if $spmgsnipreviewt_home == 1}
        {literal}
            var spmgsnipreview_number_storereviews_home_slider = {/literal}{$spmgsnipreviewsr_slh|escape:'htmlall':'UTF-8'}{literal};
        {/literal}
    {/if}
{/if}


{if $spmgsnipreviewrvis_on == 1}

    {if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}

        {if $spmgsnipreviewis_blocklr == 1 && $spmgsnipreviewblocklr_home == "blocklr_home" && $spmgsnipreviewblocklr_home_pos == "home"}
            {literal}
                var spmgsnipreview_number_reviews_home_slider = {/literal}{$spmgsnipreviewsr_slhr|escape:'htmlall':'UTF-8'}{literal};
            {/literal}
        {/if}

    {/if}

{/if}


{if $spmgsnipreviewis_uprof == 1}
    {if $spmgsnipreviewradv_home == 1}
        {literal}
            var spmgsnipreview_number_users_home_slider = {/literal}{$spmgsnipreviewsr_slhu|escape:'htmlall':'UTF-8'}{literal};
        {/literal}
    {/if}
{/if}


{literal}



</script>
{/literal}