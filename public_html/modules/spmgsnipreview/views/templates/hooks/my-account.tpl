{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewrvis_on == 1}

{if $spmgsnipreviewratings_on == 1 || $spmgsnipreviewtitle_on == 1 || $spmgsnipreviewtext_on == 1}

{if $spmgsnipreviewislogged !=0}

    {if $spmgsnipreviewis17 == 0}
    <li>
    {/if}

	
	<a href="{$spmgsnipreviewaccount_url|escape:'htmlall':'UTF-8'}" {if $spmgsnipreviewis17 == 1}class="col-lg-4 col-md-6 col-sm-6 col-xs-12"{/if}
	   title="{l s='Product Reviews' mod='spmgsnipreview'}">

       {if $spmgsnipreviewis17 == 1}<span class="link-item">{/if}

	   {if $spmgsnipreviewis16 == 1}<i {if $spmgsnipreviewis17 == 1}class="material-icons"{/if}>{/if}
            <img class="icon" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'html':'UTF-8'}" />
	   {if $spmgsnipreviewis16 == 1}</i>{/if}


	   {if $spmgsnipreviewis16 == 1 && $spmgsnipreviewis17 == 0}<span>{/if}
            {l s='Product Reviews' mod='spmgsnipreview'}
	   {if $spmgsnipreviewis16 == 1 && $spmgsnipreviewis17 == 0}</span>{/if}


       {if $spmgsnipreviewis17 == 1}</span>{/if}

	</a>


    {if $spmgsnipreviewis17 == 0}
    </li>
    {/if}


{/if}

{/if}

{/if}

{if $spmgsnipreviewis_uprof == 1}
{if $spmgsnipreviewislogged !=0}

    {if $spmgsnipreviewis17 == 0}
        <li>
    {/if}


        <a href="{$spmgsnipreviewuacc_url|escape:'htmlall':'UTF-8'}" {if $spmgsnipreviewis17 == 1}class="col-lg-4 col-md-6 col-sm-6 col-xs-12"{/if}
           title="{l s='User profile' mod='spmgsnipreview'}">

            {if $spmgsnipreviewis17 == 1}<span class="link-item">{/if}

            {if $spmgsnipreviewis16 == 1}<i {if $spmgsnipreviewis17 == 1}class="material-icons"{/if}>{/if}
                <img class="icon" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/users/users-logo.png" />
                {if $spmgsnipreviewis16 == 1}</i>{/if}

                {if $spmgsnipreviewis16 == 1  && $spmgsnipreviewis17 == 0}<span>{/if}
                    {l s='User profile' mod='spmgsnipreview'}
                {if $spmgsnipreviewis16 == 1  && $spmgsnipreviewis17 == 0}</span>{/if}

            {if $spmgsnipreviewis17 == 1}</span>{/if}

        </a>

    {if $spmgsnipreviewis17 == 0}
        </li>
    {/if}


{/if}
{/if}


{if $spmgsnipreviewis_storerev == 1}
    {if $spmgsnipreviewislogged !=0}

        {if $spmgsnipreviewis17 == 0}
            <li>
        {/if}


            <a href="{$spmgsnipreviewmysr_url|escape:'htmlall':'UTF-8'}" {if $spmgsnipreviewis17 == 1}class="col-lg-4 col-md-6 col-sm-6 col-xs-12"{/if}
               title="{l s='Store Reviews' mod='spmgsnipreview'}">

                {if $spmgsnipreviewis17 == 1}<span class="link-item">{/if}

                {if $spmgsnipreviewis16 == 1}<i {if $spmgsnipreviewis17 == 1}class="material-icons"{/if}>{/if}
                    <img class="icon" src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/storereviews-logo.png" />
                    {if $spmgsnipreviewis16 == 1}</i>{/if}

                {if $spmgsnipreviewis16 == 1}<span>{/if}
                    {l s='Store Reviews' mod='spmgsnipreview'}
                {if $spmgsnipreviewis16 == 1}</span>{/if}


                {if $spmgsnipreviewis17 == 1}</span>{/if}

            </a>


        {if $spmgsnipreviewis17 == 0}
            </li>
        {/if}

    {/if}
{/if}