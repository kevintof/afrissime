{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}

<div style="float: left" class="badge_header_main">

    <a href="{$spmgsnipreviewred_url|escape:'htmlall':'UTF-8'}" title="{$spmgsnipreviewred_url|escape:'htmlall':'UTF-8'}">
        <img src="{$spmgsnipreviewbase_dir|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/btn/ico-star.png" alt="" style="height:20px;"/>
                <span class="badge_header"
                      style="vertical-align: top; margin-left: -13px; margin-top: -2px;{if $spmgsnipreviewcount_new_reviews == 0}color:white;font-size:13px;background-color:#c3c3c3{/if}"
                        >({$spmgsnipreviewcount_new_reviews|escape:'htmlall':'UTF-8'})</span>
    </a>


        <a href="{$spmgsnipreviewred_url_store|escape:'htmlall':'UTF-8'}" title="{$spmgsnipreviewred_url_store|escape:'htmlall':'UTF-8'}">
            <img src="{$spmgsnipreviewbase_dir|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/storereviews-logo.png" alt="" style="height:20px;"/>
            <span class="badge_header"
                  style="vertical-align: top; margin-left: -13px; margin-top: -2px;{if $spmgsnipreviewcount_new_reviews == 0}color:white;font-size:13px;background-color:#c3c3c3{/if}"
                    >({$spmgsnipreviewcount_new_reviews_store|escape:'htmlall':'UTF-8'})</span>
        </a>

</div>
