{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{if $spmgsnipreviewis_abuse == 0}
<div id="add-review-form-review" class="popup-form font-family-custom">


    <div class="title-rev">
        <div class="title-form-text-left">
            <b class="title-form-custom">{l s='Review' mod='spmgsnipreview'}</b>:

        </div>

        <div class="clear-spmgsnipreview"></div>
    </div>

    <div id="body-add-review-form-review" class="text-align-left">
        <span class="label-span">{l s='Review URL' mod='spmgsnipreview'}:</span>
        <span class="badge">
            <a href="{$spmgsnipreviewabuserev.review_url|escape:'htmlall':'UTF-8'}" target="_blank">{$spmgsnipreviewabuserev.review_url|escape:'htmlall':'UTF-8'}</a>
        </span>
        <div class="clear-spmgsnipreview"></div>

        {if strlen($spmgsnipreviewabuserev.title_review)>0}
        <label for="subject-review" >{l s='Title' mod='spmgsnipreview'}:</label>
        <input disabled class="form-control disabled-values" id="disabledInput" type="text" value="{$spmgsnipreviewabuserev.title_review|escape:'htmlall':'UTF-8' nofilter}"  />
        {/if}

        {if strlen($spmgsnipreviewabuserev.text_review)>0}
        <label for="text-review" >{l s='Text' mod='spmgsnipreview'}:</label>
        <textarea disabled class="form-control disabled-values" id="disabledInput" cols="42" rows="7">{$spmgsnipreviewabuserev.text_review|escape:'htmlall':'UTF-8' nofilter}</textarea>
        {/if}
    </div>

    <div class="title-rev">
        <div class="title-form-text-left">
            <b class="title-form-custom">{l s='Abuse' mod='spmgsnipreview'}:</b>
        </div>

        <div class="clear-spmgsnipreview"></div>
    </div>
    <div id="body-add-review-form-review" class="text-align-left">

    {if $spmgsnipreviewabuserev.is_customer == 0}
            <label for="name-abuse" >{l s='Guest Name' mod='spmgsnipreview'}:</label>
            <input type="text" name="name-abuse" id="name-abuse" class="form-control disabled-values" disabled value="{$spmgsnipreviewabuserev.abuse_name|escape:'htmlall':'UTF-8'}" />

            <label for="email-abuse" >{l s='Guest Email' mod='spmgsnipreview'}:</label>
            <input type="text" name="email-abuse" id="email-abuse" class="form-control disabled-values" disabled  value="{$spmgsnipreviewabuserev.email|escape:'htmlall':'UTF-8'}" />
    {else}
        <span>{l s='Customer' mod='spmgsnipreview'}:</span>
        <span class="badge">
            <a href="{$spmgsnipreviewabuserev.url_to_customer|escape:'htmlall':'UTF-8' nofilter}" target="_blank">{$spmgsnipreviewabuserev.customer_name|escape:'htmlall':'UTF-8'}</a>
        </span>
        <div class="clear-spmgsnipreview"></div>
    {/if}

        <label for="text-abuse" >{l s='Abuse text' mod='spmgsnipreview'}:</label>
        <textarea id="text-abuse" name="text-abuse" cols="42" rows="7" class="form-control disabled-values" disabled>{$spmgsnipreviewabuserev.text_abuse|escape:'htmlall':'UTF-8'}</textarea>



    </div>
    <div id="footer-add-review-form-review">
        <button onclick="update_abuse()"  value="{l s='Set review is not abusive' mod='spmgsnipreview'}" class="btn btn-success">{l s='Set review is not abusive' mod='spmgsnipreview'}</button>
    </div>


    </div>

{literal}
<script type="text/javascript">


    function update_abuse(){
            var id_review = {/literal}{$spmgsnipreviewabuserev.id_review|escape:'htmlall':'UTF-8'}{literal};

            $('#abuseitem'+id_review).html('<img src="../img/admin/../../modules/spmgsnipreview/views/img/loader.gif" />');


            $.post('{/literal}{$spmgsnipreviewreviews_admin_url nofilter}{literal}',
                    {action:'status-abuse',
                    review_id:{/literal}{$spmgsnipreviewreview_id|escape:'htmlall':'UTF-8'}{literal},

                    },
                    function (data) {

                        if (data.status == 'success') {

                            $('#abuseitem'+id_review).html('');
                            var html = '<span class="label-tooltip" data-original-title="{/literal}{$spmgsnipreviewraad_msg1|escape:'htmlall':'UTF-8'}{literal}" data-toggle="tooltip">'+
                            '<img src="../img/admin/../../modules/spmgsnipreview/views/img/ok.gif" />'+
                                    '</span>';
                            $('#abuseitem'+id_review).html(html);

                             $('#fb-con-wrapper-admin').remove();
                             $('#fb-con').remove();



                        } else {

                            alert(data.message);

                        }
                    }, 'json');

        }


</script>
{/literal}


{else}
    <div>
        <div class="alert alert-warning form-warning">
            <p class="text-center">{l s='You cannot change status for this abuse because somebody has already changed status' mod='spmgsnipreview'}</p>
        </div>
    </div>

{/if}