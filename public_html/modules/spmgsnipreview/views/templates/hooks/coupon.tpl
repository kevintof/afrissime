{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 * 
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

<h4>
   <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/btn/{if isset($spmgsnipreviewis_facebook)}ico-facebook.png{else}ico-star.png{/if}"/>&nbsp;
   {$spmgsnipreviewfirsttext|escape:'htmlall':'UTF-8'}  {$spmgsnipreviewdiscountvalue|escape:'htmlall':'UTF-8'}
</h4>
<br/>
<div class="text-coupon-lines">{$spmgsnipreviewsecondtext|escape:'htmlall':'UTF-8'}: &nbsp;<b>{$spmgsnipreviewvoucher_code|escape:'htmlall':'UTF-8'}</b></div>
<br/>
<div class="text-coupon-lines">{$spmgsnipreviewthreetext|escape:'htmlall':'UTF-8'}: &nbsp;<b>{$spmgsnipreviewdate_until|escape:'htmlall':'UTF-8'}</b></div>


{literal}
    <script type="text/javascript">

        $('#fb-con-wrapper').css('height','auto');

        document.addEventListener("DOMContentLoaded", function(event) {
            $(document).ready(function(){

                $('#fb-con-wrapper').css('height','auto');

            });
        });

    </script>
{/literal}