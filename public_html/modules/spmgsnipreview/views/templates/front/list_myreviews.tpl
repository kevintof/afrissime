{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}

{foreach from=$spmgsnipreviewmy_reviews item=review}
    <tr class="{if $review.is_active == 0}no-active-my-spmgsnipreview{/if} pl-animater {$spmgsnipreviewd_eff_rev_my|escape:'htmlall':'UTF-8'}">
        <td class="center">
            <a href="{$review.product_link|escape:'htmlall':'UTF-8'}"
               title="{$review.product_name|escape:'htmlall':'UTF-8'}"
               style="text-decoration:underline" target="_blank"
                    >
                {$review.product_name|escape:'htmlall':'UTF-8'}
            </a>
        </td>
        {if $spmgsnipreviewratings_on == 1}
            <td class="center">
                {if $review.rating != 0}
                    {section name=ratid loop=5}
                        {if $smarty.section.ratid.index < $review.rating}
                            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                        {else}
                            <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}" alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                        {/if}
                    {/section}
                {else}
                    &nbsp;
                {/if}
            </td>
        {/if}

        {if $spmgsnipreviewtitle_on == 1}
            <td class="center">
                {if strlen($review.title_review)>0}
                    <a href="{$spmgsnipreviewrev_url|escape:'htmlall':'UTF-8'}?rid={$review.id|escape:'htmlall':'UTF-8'}"
                       title="{$review.title_review|escape:'htmlall':'UTF-8'}"
                       style="text-decoration:underline" target="_blank"
                            >
                        {$review.title_review|escape:'htmlall':'UTF-8' nofilter}
                    </a>
                {else}
                    &nbsp;
                {/if}
            </td>
        {/if}

        <td class="center">
            {if $review.is_bought == 1}
                <img alt="{l s='Enabled' mod='spmgsnipreview'}" title="{l s='Enabled' mod='spmgsnipreview'}"
                     src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/admin/enabled.gif">
            {else}
                <img alt="{l s='Disabled' mod='spmgsnipreview'}" title="{l s='Disabled' mod='spmgsnipreview'}"
                     src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/admin/disabled.gif">
            {/if}
        </td>

        <td class="center">
            {dateFormat date=$review.time_add|escape:'htmlall':'UTF-8' full=0}
        </td>

        <td class="center" id="changed_review{$review.id|escape:'htmlall':'UTF-8'}" {if $spmgsnipreviewis16 == 0}style="padding: 0px;text-align:center"{/if}>
            {if $review.is_changed == 1}
                <a class="{if $spmgsnipreviewis16 == 1}btn btn-success{else}button{/if}" title="{l s='Modify' mod='spmgsnipreview'}"
                   onclick="modify_review({$review.id|escape:'htmlall':'UTF-8'},'modify_my',{$review.id_customer|escape:'htmlall':'UTF-8'})"
                   href="javascript:void(0)">
                    <i class="fa fa-pencil-square-o"></i>

                    {l s='Modify' mod='spmgsnipreview'}
                </a>
            {elseif $review.is_changed == 2}
                <img alt="{l s='Review already changed' mod='spmgsnipreview'}" title="{l s='Review already changed' mod='spmgsnipreview'}"
                     src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/admin/enabled.gif"/>
            {else}
                {l s='No reply' mod='spmgsnipreview'}
            {/if}
        </td>


        <td class="center" >
            {if $review.is_active == 1}
                <img alt="{l s='Enabled' mod='spmgsnipreview'}" title="{l s='Enabled' mod='spmgsnipreview'}"
                     src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/admin/enabled.gif"/>
            {else}

                <i class="fa fa-clock-o fa-2x" style="color:#a94442"></i>

            {/if}
        </td>
    </tr>
{/foreach}


{if $spmgsnipreviewd_eff_rev_my != "disable_all_effects"}
{literal}
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            $(document).ready(function(){
                spmgsnipreview_init_effects_review();
            });
        });
    </script>
{/literal}
{/if}