{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}

{if count($spmgsnipreviewcustomers)>0}

{foreach from=$spmgsnipreviewcustomers item=customer name=myLoop}

    <li class="img-list-user pl-animateu {$spmgsnipreviewd_eff_shopu|escape:'htmlall':'UTF-8'}">
        <a href="{$spmgsnipreviewshopper_url|escape:'htmlall':'UTF-8'}{$customer.id_customer|escape:'htmlall':'UTF-8'}"
           title="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}">
            <img height="75" width="75" alt="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}"
                 title="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}"
                 src="{$customer.avatar_thumb|escape:'htmlall':'UTF-8'}"
                 {if $customer.exist_avatar == 0}class="profile-adv-user-img"{/if}>
        </a>
        <div class="b-name">
            <a href="{$spmgsnipreviewshopper_url|escape:'htmlall':'UTF-8'}{$customer.id_customer|escape:'htmlall':'UTF-8'}"
               title="{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}"
                    >{$customer.firstname|escape:'htmlall':'UTF-8'} {$customer.lastname|escape:'htmlall':'UTF-8'}</a>
        </div>

        {if $customer.country}
            <div class="b-from">
                {$customer.country|escape:'htmlall':'UTF-8'}
            </div>
        {/if}

    </li>


    {if $smarty.foreach.myLoop.last}
        <div class="clr"><!-- --></div>
    {/if}
{/foreach}


{else}
    <div class="spmgsnipreview-not-found">
        {l s='Users not found' mod='spmgsnipreview'}
    </div>
{/if}

{if $spmgsnipreviewd_eff_shopu != "disable_all_effects"}
{literal}
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            $(document).ready(function(){
                spmgsnipreview_init_effects_user();
            });
        });
    </script>
{/literal}
{/if}