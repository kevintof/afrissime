{*
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */
*}

{foreach from=$spmgsnipreviewmy_reviews item=review}
    <tr class="{if $review.active == 0}no-active-my-spmgsnipreview{/if} pl-animate {$spmgsnipreviewd_eff_shop_myti|escape:'htmlall':'UTF-8'}">
        <td class="center is_mobile_rating_my_storereviews">
            {if $review.rating != 0}
                {section name=ratid loop=5}
                    {if $smarty.section.ratid.index < $review.rating}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewactivestar|escape:'htmlall':'UTF-8'}"  alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                    {else}
                        <img src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}modules/spmgsnipreview/views/img/{$spmgsnipreviewnoactivestar|escape:'htmlall':'UTF-8'}"  alt="{$smarty.section.ratid.index|escape:'htmlall':'UTF-8'}"/>
                    {/if}
                {/section}
            {else}
                &nbsp;
            {/if}
        </td>

        <td class="center">
            {$review.message|escape:'htmlall':'UTF-8' nofilter}
        </td>

        <td class="center">
            {if $review.is_show == 1}
                <b>{$review.response|escape:'htmlall':'UTF-8' nofilter}</b>
            {else}
                {l s='No reply' mod='spmgsnipreview'}
            {/if}
        </td>


        <td class="center">
            {if $review.is_buy == 1}
                <img alt="{l s='Enabled' mod='spmgsnipreview'}" title="{l s='Enabled' mod='spmgsnipreview'}"
                     src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/admin/enabled.gif">
            {else}
                <img alt="{l s='Disabled' mod='spmgsnipreview'}" title="{l s='Disabled' mod='spmgsnipreview'}"
                     src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/admin/disabled.gif">
            {/if}
        </td>

        <td class="center">
            {dateFormat date=$review.date_add|escape:'htmlall':'UTF-8' full=0}
        </td>




        <td class="center" >
            {if $review.active == 1}
                <img alt="{l s='Enabled' mod='spmgsnipreview'}" title="{l s='Enabled' mod='spmgsnipreview'}"
                     src="{$base_dir_ssl|escape:'htmlall':'UTF-8'}img/admin/enabled.gif"/>
            {else}

                <i class="fa fa-clock-o fa-2x" style="color:#a94442"></i>

            {/if}
        </td>
    </tr>
{/foreach}

{if $spmgsnipreviewd_eff_shop_myti != "disable_all_effects"}
{literal}
    <script type="text/javascript">
        document.addEventListener("DOMContentLoaded", function(event) {
            $(document).ready(function(){
                spmgsnipreview_init_effects();
            });
        });
    </script>
{/literal}
{/if}