{*
/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

*}

{literal}
<script type="text/javascript">
//<![CDATA[
    var baseDir = '{/literal}{$base_dir_ssl|escape:'htmlall':'UTF-8'}{literal}';
//]]>
</script>
{/literal}

{if $spmgsnipreviewis16 == 1 && $spmgsnipreviewis17 ==0}
    {capture name=path}<a href="{$spmgsnipreviewmy_account|escape:'htmlall':'UTF-8'}">{l s='My account' mod='spmgsnipreview'}</a>
        <span class="navigation-pipe">{$navigationPipe|escape:'htmlall':'UTF-8'}</span>{l s='User profile' mod='spmgsnipreview'}{/capture}
{/if}

{if $spmgsnipreviewis17 == 1}
    <a href="{$spmgsnipreviewmy_account|escape:'htmlall':'UTF-8'}">{l s='My account' mod='spmgsnipreview'}</a>
        <span class="navigation-pipe"> > </span>{l s='User profile' mod='spmgsnipreview'}
{/if}

{if $spmgsnipreviewis16 == 1}
    <h3 class="page-product-heading">{l s='User profile' mod='spmgsnipreview'}</h3>

{else}

    {l s='User profile' mod='spmgsnipreview'}

{/if}





<form method="post" action="{$spmgsnipreviewreviews_url|escape:'htmlall':'UTF-8'}" enctype="multipart/form-data"
	 id="user_avatar_img" name="user_avatar_img"
	 >
    <input type="hidden" name="action" value="addavatar" />

<div class="b-info-block {if $spmgsnipreviewis17 == 1}block-categories{/if}">
	<div class="b-body">
		<dl>
			<dt class="check-box">
				<label class="check-box">{l s='Show my profile on the site' mod='spmgsnipreview'}</label>
			</dt>
			<dd class="padding-top-5">
					<input type="checkbox" name="show_my_profile" id="show_my_profile" class="check-box"
						   {if $spmgsnipreviewis_show == 1}checked="checked"{/if}
					/>
			</dd>
		</dl>
	
		<dl class="b-photo-ed">
			<dt><label for="n3">{l s='Avatar:' mod='spmgsnipreview'}</label></dt>
				<dd>
					<div class="b-avatar">
						<img class="profile-adv-user-img" src="{$spmgsnipreviewavatar_thumb|escape:'htmlall':'UTF-8'}"/>
					</div>
					
					<div class="b-edit">
                        {if $spmgsnipreviewexist_avatar == 1}
                           <input type="radio" name="post_images" checked="" style="display: none">
                        {/if}
						   <input type="file" name="avatar-review" id="avatar-review" />
					</div>
					<div class="clr"><!-- --></div>
					<div class="b-guide">
                        {l s='Allow formats' mod='spmgsnipreview'} *.jpg; *.jpeg; *.png; *.gif.
					</div>
                    <div class="errorTxtAdd" id="error_avatar-review"></div>
				</dd>
		</dl>
		
		<div class="b-buttons-save">
                    <button class="btn btn-success" value="Add review" type="submit">{l s='Save Changes' mod='spmgsnipreview'}</button>
		</div>
		
	</div>
	
</div>

</form>

    <br/>
    <ul class="footer_links clearfix">
        <li class="float-left margin-right-10">
            <a href="{$base_dir_ssl|escape:'htmlall':'UTF-8'}" class="btn btn-default button button-small-spmgsnipreview">
                <span><i class="icon-chevron-left"></i> {l s='Home' mod='spmgsnipreview'}</span>
            </a>
        </li>
        <li class="float-left">
            <a href="{$spmgsnipreviewmy_account|escape:'htmlall':'UTF-8'}" class="btn btn-default button button-small-spmgsnipreview">
			<span>
				<i class="icon-chevron-left"></i> {l s='Back to Your Account' mod='spmgsnipreview'}
			</span>
            </a>
        </li>

    </ul>




{literal}
<script type="text/javascript">

    document.addEventListener("DOMContentLoaded", function(event) {
    $(document).ready(function (e) {
        $("#user_avatar_img").on('submit',(function(e) {



                $('#user_avatar_img').css('opacity',0.5);

                e.preventDefault();
                $.ajax({
                    url: '{/literal}{$spmgsnipreviewreviews_url nofilter}{literal}',
                    type: "POST",
                    data:  new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    dataType: 'json',
                    success: function(data)
                    {

                        $('#user_avatar_img').css('opacity',1);

                        if (data.status == 'success') {

                            window.location.reload();


                        } else {

                            var error_type = data.params.error_type;

                            if(error_type == 8){
                                field_state_change('avatar-review','failed', '{/literal}{$spmgsnipreviewava_msg8|escape:'htmlall':'UTF-8'}{literal}');
                                return false;
                            } else if(error_type == 9){
                                field_state_change('avatar-review','failed', '{/literal}{$spmgsnipreviewava_msg9|escape:'htmlall':'UTF-8'}{literal}');
                                return false;
                            }

                        }

                    }
                });





        }));

    });

    });

</script>

{/literal}