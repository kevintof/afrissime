/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

document.addEventListener("DOMContentLoaded", function(event) {
    $(document).ready(function(){
        $('#searchti').val('');
    });


    // sorting
    $( "#select_spmgsnipreview_sortti" ).change(function() {
        var data_action = $('select#select_spmgsnipreview_sortti').attr('data-actionti');

        go_page_spmgsnipreviewti( 0, data_action , '', '' , 0, 1);
    });
    // sorting

});


function go_page_spmgsnipreviewti(page,type,rating,search,id_customer, is_sort){

    if(is_sort !== undefined){
        var sort_condition = $( "#select_spmgsnipreview_sortti option:selected" ).val();
    } else {
        var sort_condition = '';
    }


    if(search !== undefined && search !== ''){

        var searchti = $('#searchti').val();


    } else {
        var searchti = '';

    }

    if(rating !== undefined){

        var fratti = rating;
    } else {
        var fratti = '';

    }

    for (var rating_i = 1; rating_i < 6; rating_i++) {
        $('#storereviews-rating-'+rating_i).parent().removeClass('active-items-block');
    }


    if(id_customer === undefined){
        var id_customer = 0;
    }

    $('#storereviews-list').css('opacity',0.5);
    $.post(ajax_storereviews_url_spmgsnipreview, {
            action:type,
            page : page,
            fratti: fratti,
            searchti: searchti,
            uid:id_customer,
            sort_condition: sort_condition,
            is_sort:is_sort

        },
        function (data) {
            if (data.status == 'success') {

                $('#storereviews-list').css('opacity',1);

                $('#storereviews-list').html('');
                $('#storereviews-list').prepend(data.params.content);

                $('#page_nav').html('');
                $('#page_nav').prepend(data.params.page_nav);


                if(rating !== undefined && rating !== '' && rating !== 0){
                    $('#storereviews-rating-'+rating).parent().addClass('active-items-block');
                    $('#clear-rating-storereviews').removeClass('display-none');
                } else {
                    $('#clear-rating-storereviews').addClass('display-none');
                }

                if(search !== undefined && search !== '' && search !== 0){
                    $('#clear-search-storereviews').removeClass('display-none');
                } else {
                    $('#clear-search-storereviews').addClass('display-none');
                }

                if (document.getElementById('center_column') instanceof Object){
                        var id_scroll_to = "center_column";
                } else {
                        var id_scroll_to = "content";
                }





                $("html, body").animate({ scrollTop: $('#'+id_scroll_to).offset().top }, "slow");
                return false;


            } else {
                $('#storereviews-list').css('opacity',1);
                alert(data.message);
            }

        }, 'json');

}




function trim(str) {
    str = str.replace(/(^ *)|( *$)/,"");
    return str;
}


function show_testimonial_form(par){

    if(par == 1) {
        $('#add_testimonials').hide();
        $('#text-before-add-testimonial-form').show();
        $('#add-testimonial-form').show();
    } else {
        $('#add_testimonials').show();
        $('#text-before-add-testimonial-form').hide();
        $('#add-testimonial-form').hide();
    }
}




function field_state_change_store(field, state, err_text)
{

    if(typeof field_gdpr_change_store_spmgsnipreview === 'function') {
        //gdpr
        field_gdpr_change_store_spmgsnipreview();
        //gdpr
    }


    var field_label = $('label[for="'+field+'"]');
    var field_div_error = $('#'+field);

    if (state == 'success')
    {
        field_label.removeClass('error-label');
        field_div_error.removeClass('error-current-input');
    }
    else
    {
        field_label.addClass('error-label');
        field_div_error.addClass('error-current-input');
    }
    document.getElementById('error_'+field).innerHTML = err_text;

}


$(document).ready(function(){

if ($('.owl_storereviews_type_carousel ul').length > 0) {

    if ($('.owl_storereviews_type_carousel ul').length > 0) {

        if (typeof $('.owl_storereviews_type_carousel ul').owlCarousel === 'function') {

            $('.owl_storereviews_type_carousel ul').owlCarousel({
                items: 1,

                loop: true,
                responsive: true,
                nav: true,
                navRewind: false,
                margin: 20,
                dots: true,
                navText: [,],

                lazyLoad: true,
                lazyFollow: true,
                lazyEffect: "fade",
            });
        }
    }

}


    if ($('.owl_storereviews_home_type_carousel ul').length > 0) {

        if ($('.owl_storereviews_home_type_carousel ul').length > 0) {

            if (typeof $('.owl_storereviews_home_type_carousel ul').owlCarousel === 'function') {

                $('.owl_storereviews_home_type_carousel ul').owlCarousel({
                    items: spmgsnipreview_number_storereviews_home_slider,

                    loop: true,
                    responsive: true,
                    nav: true,
                    navRewind: false,
                    margin: 20,
                    dots: true,

                    lazyLoad: true,
                    lazyFollow: true,
                    lazyEffect: "fade",
                });

            }
        }

    }

});