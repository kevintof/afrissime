/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function init_tabs(id){
    $('document').ready( function() {

        if(id == 2){
            $('#navtabs16 a[href="#googlesnippets"]').tab('show');
        }

        if(id == 7){
            $('#navtabs16 a[href="#richpins"]').tab('show');
        }

        if(id == 31){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#globalsettings"]').tab('show');

        }

        if(id == 32){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#productpage"]').tab('show');

        }

        if(id == 33){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#reviewsmanagement"]').tab('show');

        }

        if(id == 34){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#reviewcriteria"]').tab('show');

        }

        if(id == 35){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#customeraccountreviewspage"]').tab('show');

        }

        if(id == 36){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#lastreviewsblock"]').tab('show');

        }

        if(id == 37){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#starslistandsearch"]').tab('show');

        }

        if(id == 38){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#rssfeed"]').tab('show');

        }

        if(id == 39){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#importcomments"]').tab('show');

        }


        if(id == 43){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#gproductfeed"]').tab('show');

        }


        if(id == 40){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#reviewsemailstab"]').tab('show');

        }

        if(id == 41){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#responseadminemails"]').tab('show');

        }

        if(id == 42){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#customerreminder"]').tab('show');

        }

        if(id == 44){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#customerreminderstat"]').tab('show');
        }
        if(id == 45){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#csvproductreviews"]').tab('show');
        }

        if(id == 5){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#reviewsvoucheraddreviewtab"]').tab('show');
        }

        if(id == 55){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#reviewsvouchersharereviewtab"]').tab('show');
        }

        if(id == 8){
            $('#navtabs16 a[href="#autoposts"]').tab('show');
        }

        if(id == 56){
            $('#navtabs16 a[href="#userprofileg"]').tab('show');
        }

        if(id == 57){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#owlcarousels"]').tab('show');
        }

        if(id == 58){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#woweffects"]').tab('show');
        }





        if(id == 72){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#reviewsemailstabshop"]').tab('show');
        }

        if(id == 73){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#mainsettings"]').tab('show');
        }

        if(id == 74){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#shopcustomerremindersettings"]').tab('show');
        }

        if(id == 75){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#emailsubjects"]').tab('show');
        }

        if(id == 76){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#reviewsvoucheraddreviewtabshop"]').tab('show');

        }

        if(id == 77){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#shopcustomerreminderstat"]').tab('show');
        }

        if(id == 78){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#csvstore"]').tab('show');
        }

        if(id == 79){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#shopreviewcriteria"]').tab('show');

        }

        if(id == 80){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#woweffectsshop"]').tab('show');

        }
        if(id == 81){
            $('#navtabs16 a[href="#shopreviews"]').tab('show');
            $('#storereviewsnavtabs16 a[href="#owlcarouselsshop"]').tab('show');

        }


    });
}


function tabs_custom(id){

        if(id == 114){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#customerreminder"]').tab('show');
        }

        if(id == 6){
            $('#navtabs16 a[href="#info"]').tab('show');
        }

        if(id == 101 || id == 104){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#cronhelp"]').tab('show');
            $( "a[href='#cronhelp']" ).css('display','block');

        }


        if(id == 102){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#reviewcriteria"]').tab('show');

        }

        if(id == 103){
            $('#navtabs16 a[href="#reviews"]').tab('show');
            $('#reviewsnavtabs16 a[href="#reviewsvouchersharereviewtab"]').tab('show');
        }


    if(id == 110){
        $('#navtabs16 a[href="#shopreviews"]').tab('show');
        $('#storereviewsnavtabs16 a[href="#cronhelpstore"]').tab('show');
    }

    if(id == 115){
        $('#navtabs16 a[href="#shopreviews"]').tab('show');
        $('#storereviewsnavtabs16 a[href="#shopcustomerremindersettings"]').tab('show');
    }



}