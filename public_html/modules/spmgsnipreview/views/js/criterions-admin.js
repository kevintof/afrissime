/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function initAccessoriesAutocomplete(){
    $('document').ready( function() {
        $('#product_autocomplete_input')
            .autocomplete(
            //'ajax_products_list.php',
            'index.php?token='+spmgsnipreview_token+'&ajax=1&controller=AdminProducts&action=productsList',
            {
                minChars: 1,
                autoFill: true,
                max:20,
                matchContains: true,
                mustMatch:true,
                scroll:false,
                cacheLength:0,
                formatItem: function(item) {
                    return item[1]+' - '+item[0];
                }
            }).result(addAccessory);

        $('#product_autocomplete_input').setOptions({
            extraParams: {
                excludeIds : getAccessoriesIds()
            }
        });
    });
}



function getAccessoriesIds()
{
    if ($('#inputAccessories').val() === undefined) return '';
    if ($('#inputAccessories').val() == '') return ',';
    ids = $('#inputAccessories').val().replace(/\-/g,',');

    return ids;
}

function addAccessory(event, data, formatted)
{
    if (data == null)
        return false;
    var productId = data[1];
    var productName = data[0];

    var $divAccessories = $('#divAccessories');
    var $inputAccessories = $('#inputAccessories');
    var $nameAccessories = $('#nameAccessories');

    /* delete product from select + add product line to the div, input_name, input_ids elements */
    $divAccessories.html($divAccessories.html() + productName + ' <span class="delAccessory" name="' + productId + '" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />');
    $nameAccessories.val($nameAccessories.val() + productName + '¤');
    $inputAccessories.val($inputAccessories.val() + productId + '-');
    $('#product_autocomplete_input').val('');
    $('#product_autocomplete_input').setOptions({
        extraParams: {excludeIds : getAccessoriesIds()}
    });
}

function delAccessory(id)
{
    var div = getE('divAccessories');
    var input = getE('inputAccessories');
    var name = getE('nameAccessories');

    // Cut hidden fields in array
    var inputCut = input.value.split('-');
    var nameCut = name.value.split('¤');

    if (inputCut.length != nameCut.length)
        return jAlert('Bad size');

    // Reset all hidden fields
    input.value = '';
    name.value = '';
    div.innerHTML = '';
    for (i in inputCut)
    {
        // If empty, error, next
        if (!inputCut[i] || !nameCut[i])
            continue ;

        // Add to hidden fields no selected products OR add to select field selected product
        if (inputCut[i] != id)
        {
            input.value += inputCut[i] + '-';
            name.value += nameCut[i] + '¤';
            div.innerHTML += nameCut[i] + ' <span class="delAccessory" name="' + inputCut[i] + '" style="cursor: pointer;"><img src="../img/admin/delete.gif" /></span><br />';
        }
        else
            $('#selectAccessories').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
    }

    $('#product_autocomplete_input').setOptions({
        extraParams: {excludeIds : getAccessoriesIds()}
    });
}