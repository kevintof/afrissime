/**
 * spm
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 *
 /*
 *
 * @author    spm
 * @category seo
 * @package spmgsnipreview
 * @copyright Copyright spm
 * @license   spm
 */

function spmgsnipreview_list_storereviews(id,action,value,type_action){

    if(action == 'active') {
        $('#activeitem' + id).html('<img src="../img/admin/../../modules/spmgsnipreview/views/img/loader.gif" />');
    }



    $.post(ajax_url_spmgsnipreview,
        { id:id,
            action:action,
            value: value,
            type_action: type_action
        },
        function (data) {
            if (data.status == 'success') {


                var data = data.params.content;

                var text_action = '';
                if(type_action == 'testimonial'){
                    text_action = 'testimonial';
                }

                if(action == 'active'){

                    $('#activeitem'+id).html('');
                    if(value == 0){
                        var img_ok = 'ok';
                        var action_value = 1;
                    } else {
                        var img_ok = 'no_ok';
                        var action_value = 0;
                    }
                    var html = '<span class="label-tooltip" data-original-title="Click here to activate or deactivate '+text_action+' on your site" data-toggle="tooltip">'+
                            '<a href="javascript:void(0)" onclick="spmgsnipreview_list_storereviews('+id+',\'active\', '+action_value+',\''+type_action+'\');" style="text-decoration:none">'+
                        '<img src="../img/admin/../../modules/spmgsnipreview/views/img/'+img_ok+'.gif" />'+
                        '</a>'+
                    '</span>';
                    $('#activeitem'+id).html(html);

                    // add code for alert message //
                    if(value == 0) {
                        var message_active = 'activated';
                    } else {
                        var message_active = 'deactivated';
                    }
                    $('.bootstrap .alert').remove();
                    $('.custom-success-message').remove();
                    var html_success = '<div class="custom-success-message flash-message-list alert alert-success">'+
                        '<ul>'+
                        '<li>Item #'+id+' successfully '+message_active+'.</li>'+
                        '</ul>'+
                        '</div>';
                    if($('#form-spmgsnipreview_storereviews').length>0)
                        $('#form-spmgsnipreview_storereviews').before(html_success);

                    if($('#form-spmgsnipreview').length>0)
                        $('#form-spmgsnipreview').before(html_success);
                    // add code for alert message //

                }

            } else {
                alert(data.message);

            }
        }, 'json');
}







function addAccessory(event, data, formatted)
{
    if (data == null)
        return false;
    var productId = data[data.length - 1];
    var productName = data[0];


    var $divAccessories = $('#divAccessories');
    var $inputAccessories = $('#inputAccessories');
    var $product_autocomplete_input = $('#product_autocomplete_input');

    $product_autocomplete_input.val('');
    $product_autocomplete_input.val(productName);

    $inputAccessories.val('');
    $inputAccessories.val(productId);


}

function getAccessoriesIds()
{
    if ($('#inputAccessories').val() === undefined) return '';
    if ($('#inputAccessories').val() == '') return ',';
    ids = $('#inputAccessories').val().replace(/\-/g,',');


    return ids;
}


function initCustomersAutocomplete(){
    $('document').ready( function() {
        $('#customer_autocomplete_input')
            .autocomplete(
            //'ajax-tab.php',
            'index.php?token='+spmgsnipreview_token_customer+'&ajax=1&controller=AdminCartRules&customerFilter=1',
            {
                minChars: 1,
                max: 20,
                width: 500,
                selectFirst: false,
                scroll: false,
                dataType: 'json',


                formatItem: function(data, i, max, value, term) {
                    return value;
                },
                parse: function(data) {
                    var items = new Array();
                    for (var i = 0; i < data.length; i++) {
                        items[items.length] = {
                            data: data[i],
                            value: data[i].cname + ' (' + data[i].email + ')'
                        };

                    }


                    return items;
                }

            }).result(function(event, data, formatted) {
                $('#inputCustomers').val(data.id_customer);
                $('#customer_autocomplete_input').val(data.cname + ' (' + data.email + ')');

            });

        var inputCustomersToken = $('#inputCustomersToken').val();
        $('#customer_autocomplete_input').setOptions({
            extraParams: {
                controller: 'AdminCartRules',
                customerFilter: 1,
                token: inputCustomersToken
            }
        });
    });
}



// remove add new comment button //
//$('document').ready( function() {

    //$('#desc-spmgsnipreview_storereviews-new').css('display','none');


//});
// remove add new comment button //


function delete_avatar_storereviews(item_id,id_customer){
    if(confirm("Are you sure you want to remove this item?"))
    {
        $('.avatar-form').css('opacity',0.5);
        $.post(ajax_url_spmgsnipreview, {
                action:'deleteimg',
                item_id : item_id,
                id_customer: id_customer
            },
            function (data) {
                if (data.status == 'success') {
                    $('.avatar-form').css('opacity',1);
                    $('.avatar-button15').remove(); // for ps 15,14
                    $('.avatar-form').html('');
                    $('.avatar-form').html('<img src = "../modules/spmgsnipreview/views/img/avatar_m.gif" />');


                } else {
                    $('.avatar-form').css('opacity',1);
                    alert(data.message);
                }

            }, 'json');
    }

}






function delete_file(item_id){
    if(confirm("Are you sure you want to remove this item?"))
    {
        $('#file-custom-'+item_id).css('opacity',0.5);
        $.post(upload_admin_url_spmgsnipreview, {
                action:'deletefile',
                item_id : item_id
            },
            function (data) {
                if (data.status == 'success') {

                    $('#file-custom-'+item_id).css('opacity',1);
                    $('#file-custom-'+item_id).remove();

                } else {

                    $('#file-custom-'+item_id).css('opacity',1);
                    alert(data.message);
                }

            }, 'json');
    }

}

