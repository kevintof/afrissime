<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function upgrade_module_1_5_8($module)
{
    $name_module = 'spmgsnipreview';

    $module->installCriteriaTableShopReviews();
    $module->installReviewCriteria(array('is_shop_reviews'=>1));

    $_prefix_shop_reviews = "ti";
    $_prefix_review = "r";

    Configuration::updateValue($name_module.'is_files'.$_prefix_shop_reviews, 1);
    Configuration::updateValue($name_module.'ruploadfiles'.$_prefix_shop_reviews, 7);

    $module->installFiles2ShopReviewTable();
    $module->createFolderAndSetPermissionsFilesShopReviews();


    ## ids_groups ##

    $cookie = Context::getContext()->cookie;
    $id_lang =  $cookie->id_lang;


    $ids_groups_array = array();
    foreach(Group::getGroups($id_lang) as $group_item){
        $id_group = $group_item['id_group'];
        $ids_groups_array[] = $id_group;
    }
    $ids_groups = implode(",",$ids_groups_array);


    Configuration::updateValue($name_module.'ids_groups', $ids_groups);
    Configuration::updateValue($name_module.'ids_groupsfb', $ids_groups);
    Configuration::updateValue($name_module.'ids_groups'.$_prefix_shop_reviews, $ids_groups);

    ## ids_groups ##



    // voucher settings - store reviews

    Configuration::updateValue($name_module.'vis_on'.$_prefix_shop_reviews, 1);

    Configuration::updateValue($name_module.'is_show_min'.$_prefix_shop_reviews, 1);


    $languages = Language::getLanguages(false);
    foreach ($languages as $language){
        $i = $language['id_lang'];
        $iso = Tools::strtoupper(Language::getIsoById($i));

        $coupondesc = 'Product, Shop Reviews, Reminder, Profile, Rich Snippets';
        Configuration::updateValue($name_module.'coupondesc'.$_prefix_shop_reviews.'_'.$i, $coupondesc.' '.$iso);
    }

    Configuration::updateValue($name_module.'vouchercode'.$_prefix_shop_reviews, "PRS");
    Configuration::updateValue($name_module.'discount_type'.$_prefix_shop_reviews, 2);
    Configuration::updateValue($name_module.'percentage_val'.$_prefix_shop_reviews, 1);

    Configuration::updateValue($name_module.'tax'.$_prefix_shop_reviews, 1);


    $cur = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);
    foreach ($cur AS $_cur){
        if(Configuration::get('PS_CURRENCY_DEFAULT') == $_cur['id_currency']){
            Configuration::updateValue('sdamount'.$_prefix_shop_reviews.'_'.(int)$_cur['id_currency'], 1);
        }
    }
    Configuration::updateValue($name_module.'sdvvalid'.$_prefix_shop_reviews, 365);

    // cumulable
    Configuration::updateValue($name_module.'cumulativeother'.$_prefix_shop_reviews, 0);
    Configuration::updateValue($name_module.'cumulativereduc'.$_prefix_shop_reviews, 0);
    // cumulable

    Configuration::updateValue($name_module.'highlight'.$_prefix_shop_reviews, 1);
    // categories
    Configuration::updateValue($name_module.'catbox'.$_prefix_shop_reviews, $module->getIdsCategories());
    // categories

    // voucher settings - store reviews


    $languages = Language::getLanguages(false);
    foreach ($languages as $language){
        $i = $language['id_lang'];
        Configuration::updateValue($name_module.'revvoucr'.$_prefix_shop_reviews.'_'.$i, 'You submit a review and get voucher for discount');
    }


    Configuration::updateValue($name_module.'d_eff_shop'.$_prefix_shop_reviews, 'flipInX');
    Configuration::updateValue($name_module.'d_eff_shop_my'.$_prefix_shop_reviews, 'flipInX');
    Configuration::updateValue($name_module.'d_eff_shop_u'.$_prefix_shop_reviews, 'flipInX');


    Configuration::updateValue($name_module.'sr_slider', 1);
    Configuration::updateValue($name_module.'sr_sl', 2);

    Configuration::updateValue($name_module.'sr_sliderh', 1);
    Configuration::updateValue($name_module.'sr_slh', 2);

    Configuration::updateValue($name_module.'tlast', 3);


    Configuration::updateValue($name_module.'perpagemy'.$_prefix_shop_reviews, 5);
    Configuration::updateValue($name_module.'perpageu'.$_prefix_shop_reviews, 5);

    Configuration::updateValue($name_module.'is_filterall'.$_prefix_shop_reviews, 1);



    ### product reviews sliders ###
    Configuration::updateValue($name_module.'sr_sliderr', 1);
    Configuration::updateValue($name_module.'sr_slr', 2);

    Configuration::updateValue($name_module.'sr_sliderhr', 1);
    Configuration::updateValue($name_module.'sr_slhr', 2);

    Configuration::updateValue($name_module.'d_eff_rev', 'flipInX');
    Configuration::updateValue($name_module.'d_eff_rev_my', 'flipInX');
    Configuration::updateValue($name_module.'d_eff_rev_u', 'flipInX');
    Configuration::updateValue($name_module.'d_eff_rev_all', 'flipInX');


    /// user profile ///

    Configuration::updateValue($name_module.'is_proftab', 1);

    // users sliders
    Configuration::updateValue($name_module.'sr_slideru', 1);
    Configuration::updateValue($name_module.'sr_slu', 3);

    Configuration::updateValue($name_module.'sr_sliderhu', 1);
    Configuration::updateValue($name_module.'sr_slhu', 3);

    Configuration::updateValue($name_module.'d_eff_shopu', 'flipInX');


    // filter enable / disable

    Configuration::updateValue($name_module.'is_filterp', 1);
    Configuration::updateValue($name_module.'is_filterall', 1);



    // added fileds in the table ps_spmgsnipreview_review_criterion //
    $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion`');
    if (is_array($list_fields))
    {
        foreach ($list_fields as $k => $field)
            $list_fields[$k] = $field['Field'];
        if (!in_array('id_product', $list_fields)) {
            if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion` ADD `id_product` text')) {
                return false;
            }

            if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('UPDATE `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion` SET `id_product` = \'\'')) {
                return false;
            }

        }
    }


    $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion`');
    if (is_array($list_fields))
    {
        foreach ($list_fields as $k => $field)
            $list_fields[$k] = $field['Field'];
        if (!in_array('id_category', $list_fields)) {
            if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion` ADD `id_category` text')) {
                return false;
            }

        }
    }

    $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion`');
    if (is_array($list_fields))
    {
        foreach ($list_fields as $k => $field)
            $list_fields[$k] = $field['Field'];
        if (!in_array('is_category', $list_fields)) {
            if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion` ADD `is_category` int(11) NOT NULL default \'0\'')) {
                return false;
            }

        }
    }
   // added fileds in the table ps_spmgsnipreview_review_criterion //


    Configuration::updateValue($name_module.'is_sortf', 1);
    Configuration::updateValue($name_module.'is_sortfu', 1);
    Configuration::updateValue($name_module.'is_sortallf', 1);

    Configuration::updateValue($name_module.'is_sortf'.$_prefix_shop_reviews, 1);
    Configuration::updateValue($name_module.'is_sortfu'.$_prefix_shop_reviews, 1);


    /* enable/disable emails */
    Configuration::updateValue($name_module.'is_emrem'.$_prefix_shop_reviews, 1);
    Configuration::updateValue($name_module.'is_reminderok'.$_prefix_shop_reviews, 1);
    Configuration::updateValue($name_module.'is_thankyou'.$_prefix_shop_reviews, 1);
    Configuration::updateValue($name_module.'is_newtest'.$_prefix_shop_reviews, 1);
    Configuration::updateValue($name_module.'is_resptest'.$_prefix_shop_reviews, 1);
    Configuration::updateValue($name_module.'is_revvoucr'.$_prefix_shop_reviews, 1);


    Configuration::updateValue($name_module.'is_emailreminder', 1);
    Configuration::updateValue($name_module.'is_reminderok'.$_prefix_review, 1);
    Configuration::updateValue($name_module.'is_thankyou'.$_prefix_review, 1);
    Configuration::updateValue($name_module.'is_newrev'.$_prefix_review, 1);
    Configuration::updateValue($name_module.'is_subresem', 1);
    Configuration::updateValue($name_module.'is_modrev'.$_prefix_review, 1);
    Configuration::updateValue($name_module.'is_subpubem', 1);
    Configuration::updateValue($name_module.'is_abuserev'.$_prefix_review, 1);
    Configuration::updateValue($name_module.'is_revvouc'.$_prefix_review, 1);
    Configuration::updateValue($name_module.'is_facvouc'.$_prefix_review, 1);
    Configuration::updateValue($name_module.'is_sugvouc'.$_prefix_review, 1);



    /* enable/disable emails */


    if(version_compare(_PS_VERSION_, '1.7', '>')) {
        $module->registerHook('displayProductButtons');
    }



    Configuration::updateValue($name_module.'rswitch_lng'.$_prefix_shop_reviews, 0);

    $module->installReminder2CustomerShopReviewTable();


    Configuration::updateValue($name_module.'perpageu'.$_prefix_review, 5);


    /// clear smarty cache ///

    Tools::clearSmartyCache();

    /// clear smarty cache ///

    return true;
}
?>