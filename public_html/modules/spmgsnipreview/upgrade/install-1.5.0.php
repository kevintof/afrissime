<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

function upgrade_module_1_5_0($module)
{
	$name_module = 'spmgsnipreview';

    Configuration::updateValue($name_module.'mt_left', 1);
    Configuration::updateValue($name_module.'mt_right', 1);
    Configuration::updateValue($name_module.'mt_footer', 1);
    Configuration::updateValue($name_module.'mt_home', 1);
    Configuration::updateValue($name_module.'mt_leftside', 1);
    Configuration::updateValue($name_module.'mt_rightside', 1);

    Configuration::updateValue($name_module.'st_left', 1);
    Configuration::updateValue($name_module.'st_right', 1);
    Configuration::updateValue($name_module.'st_footer', 1);
    Configuration::updateValue($name_module.'st_home', 1);
    Configuration::updateValue($name_module.'st_leftside', 1);
    Configuration::updateValue($name_module.'st_rightside', 1);


    return true;
}
?>