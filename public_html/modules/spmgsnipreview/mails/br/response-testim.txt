Hi {name},

Response on the testimonial in the {shop_name}

Details of message:

{text}

Response: {response}

See this testimonial here: {link}



{shop_url} powered by PrestaShop™