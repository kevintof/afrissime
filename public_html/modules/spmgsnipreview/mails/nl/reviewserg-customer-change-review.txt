Dear merchant, one of your customers has modified own product review in shop : {shop_name}

Customer: {customer_name}

Product: {product_name} - {product_link}


Here is the new review:

Rating new: {rating_new} / 5

Title new: {title_review_new}

Review new: {text_review_new}


Here is the old review:

Rating old: {rating_old} / 5

Title old: {title_review_old}

Review old: {text_review_old}



See this review here: {rev_url}

{shop_url} powered by PrestaShop™