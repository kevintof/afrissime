Dear {customer_name}, you {text_voucher_title} and get voucher for discount in the shop : {shop_name}:

{firsttext} {discountvalue}

{secondtext} {voucher_code}

{threetext} {date_until}

See your review here: {storereviews_url}

Your {shop_name} team