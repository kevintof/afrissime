New Review, waiting for your validation in your shop: {shop_name}

Customer: {customer_name}

Product: {product_name}

Rating: {rating} / 5

Title: {title}

Review: {review}

See this review here: {product_link}

{shop_url} powered by PrestaShop™