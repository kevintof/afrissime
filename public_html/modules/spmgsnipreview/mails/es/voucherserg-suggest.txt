Dear {customer_name}, share your review on Facebook and get voucher for discount in the shop : {shop_name}:

Product : {product_name}

You can also get an additional voucher for discount of <strong>{discountvalue}</strong> for your next purchase if you Like your review on Facebook.

Go to your review <strong>{rev_url}</strong> and click on the "Facebook Like" button below your review. (ONLY IF YOUR LOGGED IN YOUR ACCOUNT)

This voucher will be valid until {date_valid}

See your review here: {rev_url}

Your {shop_name} team