Dear {customer_name}, {publish_text} in shop : {shop_name}

Product: {product_name}

Rating: {rating} / 5

Title: {title}

Review: {review}

See this review here: {product_link}

{shop_url} powered by PrestaShop™