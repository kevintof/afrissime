<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewCronModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {

        $name = "spmgsnipreview";
        $token = Tools::getValue('token');
        include_once(_PS_MODULE_DIR_.$name.'/spmgsnipreview.php');
        $obj_main = new spmgsnipreview();

        $_token = $obj_main->getokencron();


        if($_token == $token){

            include_once(_PS_MODULE_DIR_.$name.'/classes/featureshelp.class.php');
            $obj = new featureshelp();
            $cron_on = (int)Configuration::get($name. 'reminder');

            if($cron_on==1){
                $obj->sendCronTab();
                exit;
            } else {
                echo 'Error: Enable CRON in the module settings';
            }

        } else {
            echo 'Error: Access denien! Invalid token!';
        }
        exit;
    }

}