<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewReviewsModuleFrontController extends ModuleFrontController
{
    public $php_self;
	public function init()
	{
		$module_name = 'spmgsnipreview';
		
		$rvis_on = Configuration::get($module_name.'rvis_on');
		$ratings_on = Configuration::get($module_name.'ratings_on');
		$title_on = Configuration::get($module_name.'title_on');
		$text_on = Configuration::get($module_name.'text_on');
		
		
		if ($rvis_on == 1){

		} else {
			Tools::redirect('index.php');
		}

		if($ratings_on == 1 || $title_on == 1 || $text_on == 1){

		} else {

			Tools::redirect('index.php');
		}
		parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();
        $this->addJqueryPlugin(array('fancybox'));


        $name_module = 'spmgsnipreview';
        if(Configuration::get($name_module.'d_eff_rev_all') != "disable_all_effects") {

            $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/js/wow.js');
            $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/css/animate.css');

        }

        $this->context->controller->addCSS(__PS_BASE_URI__.'modules/'.$name_module.'/views/css/font-custom.min.css');

	}

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
        $module_name = 'spmgsnipreview';
        $this->php_self = 'module-'.$module_name.'-reviews';

		parent::initContent();
		


		
		$gp = (int)Tools::getValue('gp');
        $step = (int)Configuration::get($module_name.'revperpageall');

        $start = (int)(($gp - 1)*$step);
        if($start<0)
            $start = 0;


        $frat = Tools::getValue('frat');


        $search = Tools::getValue("search");
        $is_search = 0;

        ### search ###
        if(Tools::strlen($search)>0){
            $is_search = 1;

        }
        $this->context->smarty->assign($module_name.'is_search', $is_search);
        $this->context->smarty->assign($module_name.'search', $search);
        $this->context->smarty->assign($module_name.'frat', $frat);


        include_once(_PS_MODULE_DIR_.$module_name.'/classes/spmgsnipreviewhelp.class.php');
        $obj = new spmgsnipreviewhelp();


		$data = $obj->getAllReviews(array('start'=>$start,'step'=>$step,'frat'=>$frat,'is_search'=>$is_search,'search'=>$search));

		include_once(_PS_MODULE_DIR_.$module_name.'/spmgsnipreview.php');
		$objspmgsnipreview = new spmgsnipreview();
		$data_translate = $objspmgsnipreview->translateCustom();

        $objspmgsnipreview->settingsHooks();

        $objspmgsnipreview->setSettingsPinterest();


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $data_translate['all_reviews_meta_title'];
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = $data_translate['all_reviews_meta_description'];
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $data_translate['all_reviews_meta_keywords'];
        }
			

		
		$this->context->smarty->assign('meta_title' , $data_translate['all_reviews_meta_title']);
		$this->context->smarty->assign('meta_description' , $data_translate['all_reviews_meta_description']);
		$this->context->smarty->assign('meta_keywords' , $data_translate['all_reviews_meta_keywords']);

        $cookie = Context::getContext()->cookie;
        $id_lang = (int)$cookie->id_lang;
        $data_seo_url = $obj->getSEOURLs(array('id_lang'=>$id_lang));

        $rev_url = $data_seo_url['rev_url'];
        $all = $data_seo_url['rev_all'];



        $paging = $obj->paging17(
            array('start'=>$start,
                  'step'=> $step,
                  'count' => $data['count_all'],
                  'frat'=>$frat,
                  'is_search'=>$is_search,
                  'search'=>$search,
                  'action'=>'allpagereviews'
            )
        );

        $objspmgsnipreview->basicSettingsHook();




        $avg_rating = $obj->getAvgReview();
        $count_reviews = $obj->getCountReviews();

        $this->context->smarty->assign($module_name.'d_eff_rev' , Configuration::get($module_name.'d_eff_rev_all'));



        // Smarty display
		$this->context->smarty->assign(array(
			'reviews_all' => $data['reviews'],

            $module_name . 'allr_url'=> $all,

            $module_name.'count_reviews' => $count_reviews,
            $module_name.'avg_rating'=>$avg_rating['avg_rating'],
            $module_name.'avg_decimal'=>$avg_rating['avg_rating_decimal'],


			$module_name.'paging' => $paging,
            $module_name.'page_text' => $data_translate['page'],
            $module_name.'rev_url' => $rev_url,

			$module_name.'criterions' => $obj->getReviewCriteria(array('id_lang'=>$objspmgsnipreview->getIdLang(),'id_shop'=>$obj->getIdShop())),
			));



        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:' . $module_name . '/views/templates/front/all17.tpl');
        }else {
            $this->setTemplate('all.tpl');
        }
	}
}