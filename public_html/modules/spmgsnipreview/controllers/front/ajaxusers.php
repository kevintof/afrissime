<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewAjaxusersModuleFrontController extends ModuleFrontController
{

    public function postProcess()
    {

        header("Access-Control-Allow-Origin: *");
        $HTTP_X_REQUESTED_WITH = isset($_SERVER['HTTP_X_REQUESTED_WITH']) ? $_SERVER['HTTP_X_REQUESTED_WITH'] : '';
        if ($HTTP_X_REQUESTED_WITH != 'XMLHttpRequest') {
            exit;
        }


        $name_module = 'spmgsnipreview';
        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();


        $token = Tools::getValue('token');
        $token_orig = $obj_spmgsnipreview->getokencron();
        if($token_orig !=$token)
            die('Invalid token.');




        ob_start();
        $status = 'success';
        $message = '';
        $html = '';
        $paging = '';

        $action = Tools::getValue('action');





        switch ($action){
            case 'allusers':




                $obj_spmgsnipreview->setSEOUrls();



                include_once(_PS_MODULE_DIR_.$name_module.'/classes/userprofileg.class.php');
                $obj = new userprofileg();




                $step = (int)Configuration::get($name_module.'rpage_shoppers');

                $start = (int) Tools::getValue('page');
                if($start<0)
                    $start = 0;


                $search = Tools::getValue("search");
                $is_search = 0;

                ### search ###
                if(Tools::strlen($search)>0){
                    $is_search = 1;

                }



                $info_customers = $obj->getShoppersList(array('start' => $start,'step'=>$step,'is_search'=>$is_search,'search'=>$search));


                include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmgsnipreviewhelp.class.php');
                $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

                $data_urls = $obj_spmgsnipreviewhelp->getSEOURLs();
                $users_url = $data_urls['users_url'];

                $paging = $obj->paging17(array('start'=>$start,
                        'step'=> $step,
                        'count' => $info_customers['data_count_customers'],
                        'is_search'=>$is_search,
                        'search'=>$search,
                        'action'=>'allusers',
                    )
                );


                $this->context->smarty->assign($name_module.'d_eff_shopu', Configuration::get($name_module.'d_eff_shopu'));

                $this->context->smarty->assign(array(
                    $name_module.'customers' => $info_customers['customers'],
                    $name_module.'data_count_customers' => $info_customers['data_count_customers'],
                    $name_module.'paging' => $paging,
                    $name_module.'users_url'=>$users_url,

                ));


                ob_start();

                echo $obj_spmgsnipreview->renderListUsers();

                $html = ob_get_clean();


            break;
            default:
                $status = 'error';
                $message = 'Unknown parameters!';
            break;
        }


        $response = new stdClass();
        $content = ob_get_clean();
        $response->status = $status;
        $response->message = $message;
        if($action == "allusers"){
            $response->params = array('content' => $html, 'page_nav' => $paging );
        } else {
            $response->params = array('content' => $content);
        }


        echo json_encode($response);
        exit;
    }
}