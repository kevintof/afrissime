<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewusersModuleFrontController extends ModuleFrontController
{
    public $php_self;
	public function init()
	{



		parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();

        $name_module = 'spmgsnipreview';
        if(Configuration::get($name_module.'d_eff_shopu') != "disable_all_effects") {

            $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/js/wow.js');
            $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/css/animate.css');
        }
    }

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
        $name_module = 'spmgsnipreview';
        $this->php_self = 'module-'.$name_module.'-users';

		parent::initContent();




        $is_uprof = Configuration::get($name_module.'is_uprof');
        if (!$is_uprof)
            Tools::redirect('index.php');

        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();


        $obj_spmgsnipreview->setSEOUrls();


        $_data_translate = $obj_spmgsnipreview->translateCustom();


        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $_data_translate['meta_title_shoppers'];
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = $_data_translate['meta_description_shoppers'];
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $_data_translate['meta_keywords_shoppers'];
        }

        $this->context->smarty->assign('meta_title' , $_data_translate['meta_title_shoppers']);
        $this->context->smarty->assign('meta_description' , $_data_translate['meta_description_shoppers']);
        $this->context->smarty->assign('meta_keywords' , $_data_translate['meta_keywords_shoppers']);


        include_once(_PS_MODULE_DIR_.$name_module.'/classes/userprofileg.class.php');
        $obj = new userprofileg();


        $step = (int)Configuration::get($name_module.'rpage_shoppers');


        $start = (int)Tools::getValue('gp');
        if($start<0)
            $start = 0;




        $search = Tools::getValue("search");
        $is_search = 0;

        ### search ###
        if(Tools::strlen($search)>0){
            $is_search = 1;

        }
        $this->context->smarty->assign($name_module.'is_search', $is_search);
        $this->context->smarty->assign($name_module.'search', $search);



        $info_customers = $obj->getShoppersList(array('start' => $start,'step'=>$step,'is_search'=>$is_search,'search'=>$search));


        include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $data_urls = $obj_spmgsnipreviewhelp->getSEOURLs();
        $users_url = $data_urls['users_url'];

        $paging = $obj->paging17(
            array('start'=>$start,
                'step'=> $step,
                'count' => $info_customers['data_count_customers'],
                'is_search'=>$is_search,
                'search'=>$search,
                'action'=>'allusers',
            )
        );

        $this->context->smarty->assign($name_module.'d_eff_shopu', Configuration::get($name_module.'d_eff_shopu'));

        $this->context->smarty->assign(array(
            $name_module.'customers' => $info_customers['customers'],
            $name_module.'data_count_customers' => $info_customers['data_count_customers'],
            $name_module.'paging' => $paging,
            $name_module.'users_url'=>$users_url,

        ));




        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:' . $name_module . '/views/templates/front/users17.tpl');
        }else {
            $this->setTemplate('users.tpl');
        }


    }
}