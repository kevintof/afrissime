<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewRssModuleFrontController extends ModuleFrontController
{

    public function initContent()
    {

        $_name = "spmgsnipreview";


        include_once(_PS_MODULE_DIR_.$_name.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();





        $cookie = Context::getContext()->cookie;

        $id_lang = (int)$cookie->id_lang;
        $data_language = $obj_spmgsnipreviewhelp->getfacebooklib($id_lang);
        $rss_title =  Configuration::get($_name.'rssname_'.$id_lang);
        $rss_description =  Configuration::get($_name.'rssdesc_'.$id_lang);

        $_http_host = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;


        // Lets build the page
        $rootURL = $_http_host."feeds/";
        $latestBuild = date("r");




        // Lets define the the type of doc we're creating.
        header('Content-Type:text/xml; charset=utf-8');
        $createXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";


        if(Configuration::get($_name.'rsson') == 1){



            $createXML .= "<rss version=\"0.92\">\n";
            $createXML .= "<channel>
	<title><![CDATA[".str_replace('&','&amp;', $rss_title)."]]></title>
	<link>$rootURL</link>
	<description>".str_replace('&','&amp;', $rss_description)."</description>
	<lastBuildDate>$latestBuild</lastBuildDate>
	<docs>http://backend.userland.com/rss092</docs>
	<language>".$data_language['rss_language_iso']."</language>
	<image>
			<title><![CDATA[".$rss_title."]]></title>
			<url>".$_http_host."img/logo.jpg</url>
			<link>$_http_host</link>
	</image>
";

            $data_rss_items = $obj_spmgsnipreviewhelp->getItemsForRSS();

//echo "<pre>"; var_dump($data_rss_items); exit;

            foreach($data_rss_items['items'] as $_item)
            {
                $page = $_item['page'];
                $description = $_item['seo_description'];
                $title = $_item['title'];
                $img = $_item['img'];

                $createXML .= $obj_spmgsnipreviewhelp->createRSSFile($title,$description,$page, $img);
            }
            $createXML .= "</channel>\n </rss>";
// Finish it up
        }

        echo $createXML;
        exit;
    }

}