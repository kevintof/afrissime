<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewmystorereviewModuleFrontController extends ModuleFrontController
{
    public $php_self;
	public function init()
	{
        $name_module = 'spmgsnipreview';

        $is_storerev = Configuration::get($name_module.'is_storerev');
        if (!$is_storerev)
            Tools::redirect('index.php');



        parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();

        $name_module = 'spmgsnipreview';

        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();

        if(Configuration::get($name_module.'d_eff_shop_my'.$_prefix) != "disable_all_effects") {

            $this->context->controller->addJs(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/js/wow.js');
            $this->context->controller->addCSS(__PS_BASE_URI__ . 'modules/' . $name_module . '/views/css/animate.css');

        }

        $this->context->controller->addCSS(__PS_BASE_URI__.'modules/'.$name_module.'/views/css/font-custom.min.css');

    }

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{

        $name_module = 'spmgsnipreview';
        $this->php_self = 'module-'.$name_module.'-mystorereviews';


        parent::initContent();



        $cookie = Context::getContext()->cookie;


        $id_customer = isset($cookie->id_customer)?$cookie->id_customer:0;
        if (!$id_customer)
            Tools::redirect('authentication.php');



        include_once(_PS_MODULE_DIR_.$name_module.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        $_data_translate = $obj_spmgsnipreview->translateItems();

        $_data_translate_custom = $obj_spmgsnipreview->translateCustom();

        $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();

        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $_data_translate['meta_title_testimonials'];
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = $_data_translate['meta_description_testimonials'];
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $_data_translate['meta_keywords_testimonials'];
        }

        $this->context->smarty->assign('meta_title' , $_data_translate['meta_title_testimonials']);
        $this->context->smarty->assign('meta_description' , $_data_translate['meta_description_testimonials']);
        $this->context->smarty->assign('meta_keywords' , $_data_translate['meta_keywords_testimonials']);



        include_once(_PS_MODULE_DIR_.$name_module.'/classes/storereviews.class.php');
        $obj_storereviews = new storereviews();


        include_once(_PS_MODULE_DIR_.$name_module.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $id_lang = (int)$cookie->id_lang;
        $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs(array('id_lang'=>$id_lang));

        $rev_url = $data_seo_url['store_reviews_account_url'];
        $my_account = $data_seo_url['my_account'];


        $obj_spmgsnipreview->settingsHooks();

        $obj_spmgsnipreview->setSEOUrls();

        $gp = (int)Tools::getValue('p'.$_prefix);
        $step = (int)$obj_storereviews->getStepForMyStoreReviews();

        $start = (int)(($gp - 1)*$step);
        if($start<0)
            $start = 0;

        $data_my_reviews = $obj_storereviews->getTestimonials(array('start'=>$start,'step'=>$step,'id_customer' => $id_customer,'is_my'=>1));



        $paging = $obj_storereviews->PageNav17($start,$data_my_reviews['count_all_reviews'],$step, array('prefix'=>$name_module.$_prefix,'action'=>'mystorereviews'));



        $this->context->smarty->assign($name_module.'d_eff_shop_my'.$_prefix, Configuration::get($name_module.'d_eff_shop_my'.$_prefix));


        /// set reminder status if customer not exists in table spmgsnipreview_reminder2customer ///
        $is_exists_reminder = $obj_storereviews->isExists(array('id_customer'=>$id_customer));
        if(!$is_exists_reminder){
            $data = array('id_customer'=>$id_customer,'reminder_status'=>1);
            $obj_storereviews->updateReminderForCustomer($data);
        }
        /// set reminder status if customer not exists in table spmgsnipreview_reminder2customer ///

        $reminder_status = $obj_storereviews->getStatus(array('id_customer'=>$id_customer));
        $is_reminder = Configuration::get($name_module.'reminder');





        $this->context->smarty->assign(array(
            $name_module.'my_reviews' => $data_my_reviews['reviews'],
            $name_module.'paging' => $paging,

            $name_module.'my_a_link'=> $my_account,

            $name_module.'rev_url' => $rev_url,


            $name_module.'is_reminder' => $is_reminder,
            $name_module.'rem_status'=>$reminder_status,

            $name_module.'myr_msg1'=>$_data_translate_custom['myr_msg1'],
            $name_module.'myr_msg2'=>$_data_translate_custom['myr_msg2'],
            $name_module.'myr_msg3'=>$_data_translate_custom['myr_msg3'],
            $name_module.'myr_msg4'=>$_data_translate_custom['myr_msg4'],



        ));






        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:' . $name_module . '/views/templates/front/my-storereviews17.tpl');
        }else {
            $this->setTemplate('my-storereviews.tpl');
        }


    }
}