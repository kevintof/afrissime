<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewReviewModuleFrontController extends ModuleFrontController
{
    public $php_self;
	public function init()
	{
		$module_name = 'spmgsnipreview';
		
		$rvis_on = Configuration::get($module_name.'rvis_on');
		$ratings_on = Configuration::get($module_name.'ratings_on');
		$title_on = Configuration::get($module_name.'title_on');
		$text_on = Configuration::get($module_name.'text_on');
		
		
		if ($rvis_on == 1){

		} else {
			Tools::redirect('index.php');
		}

		if($ratings_on == 1 || $title_on == 1 || $text_on == 1){

		} else {

			Tools::redirect('index.php');
		}
		parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();
        $this->addJqueryPlugin(array('fancybox'));

        $module_name = 'spmgsnipreview';
        $this->context->controller->addCSS(__PS_BASE_URI__.'modules/'.$module_name.'/views/css/font-custom.min.css');
	}

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{

        $module_name = 'spmgsnipreview';
        $this->php_self = 'module-'.$module_name.'-review';


        parent::initContent();
		



        $rid = (int)Tools::getValue('rid');

        include_once(_PS_MODULE_DIR_.$module_name.'/classes/spmgsnipreviewhelp.class.php');
        $obj = new spmgsnipreviewhelp();

        $data = $obj->getOneReview(array('rid'=>$rid));

        $is_active = $data['reviews'][0]['is_active'];

        $id_shop = $data['reviews'][0]['id_shop'];
        $id_lang = $data['reviews'][0]['id_lang'];
        $data_seo_url = $obj->getSEOURLs(array('id_lang'=>$id_lang,'id_shop'=>$id_shop));

        $rev_url = $data_seo_url['rev_url'];
        $all_url = $data_seo_url['rev_all'];


        $active_product = $data['reviews'][0]['active_product'];
        if($active_product == 0){
            Tools::redirect($all_url);
            exit;
        }

        //var_dump(empty($data['reviews'][0]));exit;
        if(!$rid
            //|| !$is_active
            || empty($data['reviews'][0])
            ){

            Tools::redirect($all_url);
        }


        //echo "<pre>"; var_dump($data);

		include_once(_PS_MODULE_DIR_.$module_name.'/spmgsnipreview.php');
		$objspmgsnipreview = new spmgsnipreview();

        $objspmgsnipreview->settingsHooks();

			

        if(!$is_active) {
            $data_translate = $objspmgsnipreview->translateCustom();

            $meta_title = $data_translate['pending_review'];
            $meta_description = $data_translate['pending_review'];
            $meta_keywords = $data_translate['pending_review'];
        } else {
            $meta_title = $data['reviews'][0]['title_review'];
            $meta_description = $data['reviews'][0]['text_review'];
            $meta_keywords = $data['reviews'][0]['title_review'];

        }

        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->context->smarty->tpl_vars['page']->value['meta']['title'] = $meta_title;
            $this->context->smarty->tpl_vars['page']->value['meta']['description'] = Tools::substr($meta_description,0,155);
            $this->context->smarty->tpl_vars['page']->value['meta']['keywords'] = $meta_keywords;
        }


		$this->context->smarty->assign('meta_title' ,$meta_title);
		$this->context->smarty->assign('meta_description' , Tools::substr($meta_description,0,155));
		$this->context->smarty->assign('meta_keywords' , $meta_keywords);





        $objspmgsnipreview->basicSettingsHook();


			
		// Smarty display
		$this->context->smarty->assign(array(
			$module_name.'reviews_all' => $data['reviews'],

		     $module_name.'rev_url' => $rev_url,

			$module_name.'criterions' => $obj->getReviewCriteria(array('id_lang'=>$objspmgsnipreview->getIdLang(),'id_shop'=>$obj->getIdShop())),
			));



        if(version_compare(_PS_VERSION_, '1.7', '>')) {
            $this->setTemplate('module:' . $module_name . '/views/templates/front/review17.tpl');
        }else {
            $this->setTemplate('review.tpl');
        }
	}
}