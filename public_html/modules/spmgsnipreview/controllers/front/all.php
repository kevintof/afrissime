<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class SpmgsnipreviewAllModuleFrontController extends ModuleFrontController
{
	
	public function init()
	{
		$http_referrer = isset($_SERVER['HTTP_REFERER'])?$_SERVER['HTTP_REFERER']:'';
		
		$faq_exists = stripos($http_referrer, 'testimonials');

		// faq
		if($faq_exists != false){
		
		include_once(dirname(__FILE__).'../../../classes/storereviews.class.php');
		$obj = new storereviews();
		$is_friendly_url = $obj->isURLRewriting();
		
		if($is_friendly_url){
			
		if(Tools::strlen($http_referrer)>0){
				
			$lang_iso_redirect = Language::getIsoById((int)(Tools::getValue("id_lang")));
		
			$languages = Language::getLanguages(false);
			foreach ($languages as $language){
				$iso_array = Language::getIsoById((int)($language['id_lang']));
				if(preg_match('/\/'.$iso_array.'\//i',$http_referrer)){
					$iso_code = $iso_array;
					break;
				}
			}
			$seo_friendly_url = '';
			$item_seo_url = '';
			
			
				$to = '/'.$iso_code.'/';
				$from = '/'.$lang_iso_redirect.'/';
				$http_referrer = str_replace($to,$from,$http_referrer);
				
				$to = $item_seo_url;
				$from = $seo_friendly_url;
				$http_referrer = str_replace($to,$from,$http_referrer);
				
			
		}
		
		}
		
		}
		
		// faq
		header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
							
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
	
		header("HTTP/1.1 301 Moved Permanently");
		Tools::redirect($http_referrer);
		parent::init();
	}
	
	public function setMedia()
	{
		parent::setMedia();
		
	}

	
	/**
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		
		parent::initContent();
		
		$this->setTemplate('all-store.tpl');
		
	}
}