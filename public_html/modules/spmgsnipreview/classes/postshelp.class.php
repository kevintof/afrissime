<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class postshelp  extends Module {

    private $_name = 'spmgsnipreview';
    private $_is_vk = 0;
    private $_is_instagram = 0;

    private $_name_module_wall_post='pstwitterpostspm';

    public function round($val, $precision = 2)
    {
        if (method_exists('Tools', 'ps_round')) {
            $val = Tools::ps_round($val, $precision);
        }
        else {
            $val = round($val, $precision);
        }

        return $val;
    }

	public function postToAPI($data){
		
		$customer_name = $data['customer_name'];
		$product_name = $data['product_name'];
		$product_link = $data['product_link'];
		$rating = (int)$data['rating'];
		$picture = $data['image'];
		$id_lang = $data['id_lang'];

        $id_lang_module_pstwitterpostspm = ((int)Configuration::get($this->_name_module_wall_post.'lang_id') == 0?$id_lang:(int)Configuration::get($this->_name_module_wall_post.'lang_id'));


        $id_shop = Context::getContext()->shop->id;


        $id_product = $data['id_product'];

        ## price ##
        $product = new Product($id_product, true, $id_lang_module_pstwitterpostspm);

        //echo "<pre>"; var_dump($product);exit;

        $price = '';
        $description = '';
        $image_path = '';
        if (Validate::isLoadedObject($product)) {

            /* Image */

            $image = Image::getCover((int)($product->id));

            if ($image)
            {
                $ImageObject = new Image($image['id_image']);
                $image_type = Configuration::get($this->_name_module_wall_post.'img_size_prod');
                $image_path = _PS_PROD_IMG_DIR_ . $ImageObject->getExistingImgPath() . ($image_type ? '-' . $image_type : '') . '.' . $ImageObject->image_format;
            }



            if (Configuration::get($this->_name_module_wall_post . 'is_price')) {
                $cookie = $this->context->cookie;

                $price = Product::getPriceStatic($product->id);

                $currency = Currency::getCurrencyInstance($cookie->id_currency);

                $price = $this->round($price, 2) . ' ' . $currency->iso_code;

                $price = " " . $price;
            }


            ## description ##

            if(Tools::strlen($product->description_short)>0){
                $description = $product->description_short;
            } else {
                $description = $product->description_short .' '. (is_array($product->description)?$product->description[$id_lang_module_pstwitterpostspm]:$product->description);
            }

            $description =  strip_tags($description);
            $description = str_replace("\r\n",'',$description);
            $description = str_replace("\n",'',$description);

            if(Tools::strlen(trim($description))==0){
                $description = $product->name;
            }
            ## description ##
        }
        ## price ##


        // product data for post
        $data_product = array();
        $data_product['data_product']['image_link'] = $picture;
        $data_product['data_product']['product_url'] = $product_link;
        $data_product['data_product']['price'] = $price;
        $data_product['data_product']['description'] = $description;
        $data_product['data_product']['product_name'] = $product_name;
        $data_product['data_product']['image_path'] = $image_path;
        // product data for post

		
		$stars = '';
		for($i=1;$i<=5;$i++){
			$active_star = '★ ';
			$no_active_star = '☆ ';
			
			if($i<=$rating){
				$stars .= $active_star;
			} else {
				$stars .= $no_active_star;
			}
		}
		
		
		
		$name = $data['name'];
		
		$data_requrements = $this->checkrequirements();
    	$is_pstwitterpost = $data_requrements['pstwitterpost'];
    	$is_psvkpost = $data_requrements['psvkpost'];

        // post to Twitter //
        $name_social_item = 'tw';
        if(Configuration::get($name.$name_social_item.'post_on') && $is_pstwitterpost){
            require_once(dirname(__FILE__).'/../../../modules/'.$this->_name_module_wall_post.'/'.$this->_name_module_wall_post.'.php');
            $obj_pstwitterpost = new $this->_name_module_wall_post();

            $status = $customer_name.' '.Configuration::get($name.$name_social_item.'desc'.'_'.$id_lang).': '.$stars.' - '.$product_name . $price .' - '.$product_link;


            $data_array_item = array('status' => $status, 'data_product' => $data_product);
            if (Configuration::get($name.'howtopost') == 1){
                $obj_pstwitterpost->postWithTwitter($data_array_item);
            } else {
                $obj_pstwitterpost->addDelayedPost(
                    array(
                        'id_product'=>$id_product,'id_shop'=>$id_shop,
                        'item_type'=>1,'item_data'=>$data_array_item
                    )
                );
            }
        }
        // post to Twitter //


        if($this->_is_instagram) {
            // post to Instagram //
            $name_social_item = 'i';
            if (Configuration::get($name . $name_social_item . 'post_on') && $is_pstwitterpost) {

                require_once(dirname(__FILE__) . '/../../../modules/' . $this->_name_module_wall_post . '/' . $this->_name_module_wall_post . '.php');
                $obj_pstwitterpost = new $this->_name_module_wall_post();

                $status = $customer_name . ' ' . Configuration::get($name . $name_social_item . 'desc' . '_' . $id_lang) . ': ' . $stars . ' - ' . $product_name . $price . ' - ' . $product_link;


                $data_array_item = array('status' => $status, 'data_product' => $data_product);
                if (Configuration::get($name . 'howtopost') == 1) {
                    $obj_pstwitterpost->postWithInstagram($data_array_item);
                } else {
                    $obj_pstwitterpost->addDelayedPost(
                        array(
                            'id_product' => $id_product, 'id_shop' => $id_shop,
                            'item_type' => 2, 'item_data' => $data_array_item
                        )
                    );
                }


            }
            // post to Instagram //
        }


        // post to Pinterst //
        $name_social_item = 'p';
        if(Configuration::get($name.$name_social_item.'post_on') && $is_pstwitterpost){
            require_once(dirname(__FILE__).'/../../../modules/'.$this->_name_module_wall_post.'/'.$this->_name_module_wall_post.'.php');
            $obj_pstwitterpost = new $this->_name_module_wall_post();

            $status = $customer_name.' '.Configuration::get($name.$name_social_item.'desc'.'_'.$id_lang).': '.$stars.' - '.$product_name . $price.' - '.$product_link;

            $data_array_item = array('status' => $status, 'data_product' => $data_product);
            if (Configuration::get($name.'howtopost') == 1){
                $obj_pstwitterpost->postWithPinterest($data_array_item);
            } else {
                $obj_pstwitterpost->addDelayedPost(
                    array(
                        'id_product'=>$id_product,'id_shop'=>$id_shop,
                        'item_type'=>3,'item_data'=>$data_array_item
                    )
                );
            }



        }
        // post to Pinterst //

		if($this->_is_vk) {
            // post to Vkontakte //
            if (Configuration::get($name . 'vkpost_on') && $is_psvkpost) {
                require_once(dirname(__FILE__) . '/../../../modules/psvkpost/psvkpost.php');
                $obj_psvkpost = new psvkpost();

                $status = $customer_name . ' ' . Configuration::get($name . 'vkdesc' . '_' . $id_lang) . ': ' . $stars . ' - ' . $product_name . $price. ' - ' . $product_link;
                $obj_psvkpost->postWithAPI(array('status' => $status, 'image' => $picture, 'product_url' => $product_link));
            }
            // post to Vkontakte //
        }
		
	}
	
	public function postsSettings($data){
    	$_html = '';
    	$title = $data['translate']['title']; 
    	$hint1 = $data['translate']['hint1']; 
    	$hint2 = $data['translate']['hint2']; 

    	$title_pstwitterpost = $data['translate']['title_pstwitterpost'];
    	$title_psvkpost = $data['translate']['title_psvkpost'];
    	$buy_module_psvkpost = $data['translate']['buy_module_psvkpost'];
    	$buy_module_pstwitterpost = $data['translate']['buy_module_pstwitterpost'];
    	

    	$data_requrements = $this->checkrequirements();
    	$is_pstwitterpost = $data_requrements['pstwitterpost'];
    	$is_psvkpost = $data_requrements['psvkpost'];


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/postsSettings_posthelp.phtml');
        $_html = ob_get_clean();






        return $_html;
    }

    private function _psvkform16($data){
        require_once(_PS_MODULE_DIR_.$this->_name .'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        return $obj_spmgsnipreview->psvkform16($data);

    }



    private function _pstwitterform16($data){
        require_once(_PS_MODULE_DIR_.$this->_name .'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        return $obj_spmgsnipreview->pstwitterform16($data);

    }




	public function checkrequirements(){
    		// Vkontakte Wall Post
    		$is_on_psvkpost = 0;
    		
    		if (file_exists(dirname(__FILE__).'/../../../modules/psvkpost/psvkpost.php')) 
			{
                    $_is_psvkpost_active = Module::isEnabled('psvkpost');
                    if ($_is_psvkpost_active)
                        $is_on_psvkpost = 1;

			}
			
			// Twitter Wall Post
    		$is_on_pstwitterpost = 0;
    		if (file_exists(dirname(__FILE__).'/../../../modules/'.$this->_name_module_wall_post.'/'.$this->_name_module_wall_post.'.php'))
			{
                $_is_psvkpost_active = Module::isEnabled($this->_name_module_wall_post);
				if($_is_psvkpost_active)
					$is_on_pstwitterpost = 1;

			}
			
			return array('psvkpost'=>$is_on_psvkpost,'pstwitterpost'=>$is_on_pstwitterpost);
    }



    public function psvkform16($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];

        $update_button = $data['translate']['translate']['update_button'];
        $enable_pstwitterpost = $data['translate']['translate']['enable_psvkpost'];

        $template_text = $data['translate']['translate']['template_text'];
        $name = $data['translate']['translate']['name'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $enable_pstwitterpost,
                    'icon' => 'fa fa-vk fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $enable_pstwitterpost,
                        'name' => 'vkpost_on',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text_autopost',
                        'label' => $template_text,
                        'name' => 'vkdesc',
                        'id' => 'vkdesc',
                        'lang' => TRUE,
                        'text_before' => '{John. D.}',
                        'text_after' => ' : ★★★★☆ - {Product name} - {Product URL}',
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_button,
                )
            ),
        );


        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'psvkpostsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$name.'&tab_module='.$tab.'&module_name='.$name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesPsvkpostsettingsSettings($name),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesPsvkpostsettingsSettings($name){
        $languages = Language::getLanguages(false);
        $fields_vkdesc = array();

        foreach ($languages as $lang)
        {
            $fields_vkdesc[$lang['id_lang']] =  Configuration::get($name.'vkdesc_'.$lang['id_lang']);
        }

        $data_config = array(
            'vkdesc' => $fields_vkdesc,
            'vkpost_on' => Configuration::get($name.'vkpost_on'),
        );

        return $data_config;
    }


    public function pstwitterform16($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];

        $update_button = $data['translate']['translate']['update_button'];

        $enable_pstwitterpost_main = $data['translate']['translate']['enable_pstwitterpost_main'];

        $enable_pstwitterpost = $data['translate']['translate']['enable_pstwitterpost'];

        $enable_pstwitterpost_i = $data['translate']['translate']['enable_pstwitterpost_i'];

        $enable_pstwitterpost_p = $data['translate']['translate']['enable_pstwitterpost_p'];

        $template_text = $data['translate']['translate']['template_text'];
        $name = $data['translate']['translate']['name'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $enable_pstwitterpost_main,
                    'icon' => 'fa fa-paper-plane fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $enable_pstwitterpost,
                        'name' => 'twpost_on',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text_autopost',
                        'label' => $template_text.':',
                        'name' => 'twdesc',
                        'id' => 'twdesc',
                        'lang' => TRUE,
                        'text_before' => '{John. D.}',
                        'text_after' => ' : ★★★★☆ - {Product name} - {Product URL}',
                        'desc'=>'Example: John D. rated product in our shop: ★★★★☆ - Test product - 10$ - https://www.test.com/testproduct',
                    ),


                    /*array(
                        'type' => 'switch',
                        'label' => $enable_pstwitterpost_i,
                        'name' => 'ipost_on',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text_autopost',
                        'label' => $template_text.':',
                        'name' => 'idesc',
                        'id' => 'idesc',
                        'lang' => TRUE,
                        'text_before' => '{John. D.}',
                        'text_after' => ' : ★★★★☆ - {Product name} - {Product URL}',
                        'desc'=>'Example: John D. rated product in our shop: ★★★★☆ - Test product - 10$ - https://www.test.com/testproduct',
                    ),*/



                    array(
                        'type' => 'switch',
                        'label' => $enable_pstwitterpost_p,
                        'name' => 'ppost_on',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text_autopost',
                        'label' => $template_text.':',
                        'name' => 'pdesc',
                        'id' => 'pdesc',
                        'lang' => TRUE,
                        'text_before' => '{John. D.}',
                        'text_after' => ' : ★★★★☆ - {Product name} - {Product URL}',
                        'desc'=>'Example: John D. rated product in our shop: ★★★★☆ - Test product - 10$ - https://www.test.com/testproduct',
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_button,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'pstwitterpostsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$name.'&tab_module='.$tab.'&module_name='.$name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesPstwitterpostsettingsSettings($name),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesPstwitterpostsettingsSettings($name){
        $languages = Language::getLanguages(false);
        $fields_twdesc = array();
        $fields_idesc = array();
        $fields_pdesc = array();

        foreach ($languages as $lang)
        {
            $fields_twdesc[$lang['id_lang']] =  Configuration::get($name.'twdesc_'.$lang['id_lang']);
            $fields_idesc[$lang['id_lang']] =  Configuration::get($name.'idesc_'.$lang['id_lang']);
            $fields_pdesc[$lang['id_lang']] =  Configuration::get($name.'pdesc_'.$lang['id_lang']);
        }

        $data_config = array(
            'twdesc' => $fields_twdesc,
            'twpost_on' => Configuration::get($name.'twpost_on'),

            /*'idesc' => $fields_idesc,
            'ipost_on' => Configuration::get($name.'ipost_on'),*/

            'pdesc' => $fields_pdesc,
            'ppost_on' => Configuration::get($name.'ppost_on'),
        );

        return $data_config;
    }
}