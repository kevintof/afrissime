<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class storereviews extends Module
{

    private $_width = 85;
    private $_height = 85;
    private $_name;
    private $_http_host;
    private $_is_cloud;


    private $_width_files = 800;
    private $_height_files = 800;
    private $_width_files_small = 100;
    private $_height_files_small = 100;


    private $_prefix;

    private $_accepted_files = array('png', 'jpg', 'gif', 'jpeg');

    private $path_img_cloud_site;

    private $_table_name = 'spmgsnipreview_storereviews';

    private $_accepted_sort_conditions = array('rating', 'date_add');
    private $_accepted_sort_way = array('desc', 'asc');

    public function __construct()
    {
        $this->_name = "spmgsnipreview";
        $this->_http_host = Tools::getShopDomainSsl(true, true) . __PS_BASE_URI__;


        if (defined('_PS_HOST_MODE_'))
            $this->_is_cloud = 1;
        else
            $this->_is_cloud = 0;


        // for test
        //$this->_is_cloud = 1;
        // for test

        if ($this->_is_cloud) {
            $this->path_img_cloud = DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "upload" . DIRECTORY_SEPARATOR;
            $this->path_img_cloud_site = "modules/" . $this->_name . "/upload/";
        } else {
            $this->path_img_cloud = DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . "upload" . DIRECTORY_SEPARATOR . $this->_name . DIRECTORY_SEPARATOR;
            $this->path_img_cloud_site = "upload/" . $this->_name . "/";

        }


        $this->initContext();
    }

    private function initContext()
    {
        $this->context = Context::getContext();
    }

    private function getPrefix()
    {
        include_once(_PS_MODULE_DIR_.$this->_name . '/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();
        return $_prefix;

    }


    public function getStepForMyStoreReviews()
    {
        $prefix = $this->getPrefix();
        return (int)Configuration::get($this->_name.'perpagemy'.$prefix);
    }

    public function saveTestimonial($_data)
    {

        $cookie = $this->context->cookie;

        $id_lang = (int)($cookie->id_lang);

        $id_customer = isset($cookie->id_customer) ? $cookie->id_customer : 0;

        $post_images = isset($_data['post_images']) ? $_data['post_images'] : null;
        $filesrev = $_data['filesrev'];

        $name = $_data['name'];
        $email = $_data['email'];
        $web = $_data['web'];
        $text_review = $_data['text_review'];
        $company = $_data['company'];
        $address = $_data['address'];
        //$rating = $_data['rating'];

        ## rating ##
        $ratings = $_data['rating'];
        $sizeof_rating = sizeof($ratings);
        $rating = 0;
        foreach ($ratings as $rating_value) {
            $rating = $rating + $rating_value;
        }
        $rating = round($rating / $sizeof_rating);
        ## rating ##


        $country = $_data['country'];
        $city = $_data['city'];

        $sql = 'INSERT into `' . _DB_PREFIX_ . '' . $this->_table_name . '` SET
							   `name` = \'' . pSQL($name) . '\',
							   `email` = \'' . pSQL($email) . '\',
							   `web` = \'' . pSQL($web) . '\',
							   `message` = \'' . pSQL($text_review) . '\',
							   `company` = \'' . pSQL($company) . '\',
							   `address` = \'' . pSQL($address) . '\',
							   `country` = \'' . pSQL($country) . '\',
							   `city` = \'' . pSQL($city) . '\',
							   `id_shop` = \'' . (int)($this->getIdShop()) . '\',
							   `id_lang` = \'' . (int)($id_lang) . '\',
							   `rating` = \'' . pSQL($rating) . '\',
							   `id_customer` = \'' . (int)($id_customer) . '\',
							   `is_new` = \'1\',
							   `date_add` = NOW()
							   ';
        Db::getInstance()->Execute($sql);

        $id_review = Db::getInstance()->Insert_ID();

        $this->saveFiles2Review(array('id_review' => $id_review, 'filesrev' => $filesrev));

        $this->saveImageAvatar(array('id' => $id_review, 'id_customer' => $id_customer, 'post_images' => $post_images, 'is_storereviews' => 1));

        include_once(_PS_MODULE_DIR_.$this->_name. '/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();
        $_prefix = $this->getPrefix();


        ### add criterions ###
        foreach ($ratings as $id_criterion => $rating_value) {
            if ($id_criterion > 0) {
                $sql = 'INSERT into `' . _DB_PREFIX_ . 'spmgsnipreview_review2criterion_' . pSQL($_prefix) . '` SET
						   id_review = ' . (int)($id_review) . ',
						   id_criterion = ' . (int)($id_criterion) . ',
						   rating = ' . (int)$rating_value . '
						   ';
                Db::getInstance()->Execute($sql);
            }
        }
        ### add criterions ###


        if (Configuration::get($this->_name . 'noti' . $_prefix) == 1) {


            $_data_translate = $obj_spmgsnipreview->translateItems();


            $subject = Configuration::get($this->_name . 'newtest' . $_prefix . '_' . $id_lang);


            $message = $this->_wrap_tag(array('text1'=>$_data_translate['message'],'text2'=>$text_review,'tag1'=>'span','tag2'=>'strong','style1'=>'color:#333'));



            if (Configuration::get($this->_name . 'is_web') == 1) {
                $web = isset($_data['web']) ? $_data['web'] : '';
                if (Tools::strlen($web) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['web'],'text2'=>$web,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }

            }

            if (Configuration::get($this->_name . 'is_company') == 1) {
                $company = isset($_data['company']) ? $_data['company'] : '';
                if (Tools::strlen($company) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['company'],'text2'=>$company,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }

            }

            if (Configuration::get($this->_name . 'is_addr') == 1) {
                $address = isset($_data['address']) ? $_data['address'] : '';
                if (Tools::strlen($address) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['address'],'text2'=>$address,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }

            }

            if (Configuration::get($this->_name . 'is_country') == 1) {
                $country = isset($_data['country']) ? $_data['country'] : '';
                if (Tools::strlen($country) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['country'],'text2'=>$country,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }

            }

            if (Configuration::get($this->_name . 'is_city') == 1) {
                $city = isset($_data['city']) ? $_data['city'] : '';
                if (Tools::strlen($city) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['city'],'text2'=>$city,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }

            }


            /* Email generation */
            $templateVars = array(
                '{email}' => $email,
                '{name}' => $name,

                '{text}' => $message,

            );

            $iso_lng = Language::getIsoById((int)($id_lang));

            $dir_mails = _PS_MODULE_DIR_ . '/' . $this->_name . '/' . 'mails/';

            if (is_dir($dir_mails . $iso_lng . '/')) {
                $id_lang_current = $id_lang;
            } else {
                $id_lang_current = Language::getIdByIso('en');
            }


            if(Configuration::get($this->_name.'is_newtest'.$this->getPrefix()) == 1) {
                $id_shop = (int)$this->getIdShop();
                /* Email sending */
                Mail::Send($id_lang_current, 'testimony', $subject, $templateVars,
                    Configuration::get($this->_name . 'mail' . $_prefix), 'Testimonial Form', $email, $name,
                    NULL, NULL, dirname(__FILE__) . '/../mails/', NULL, $id_shop);

            }


            ## send thank you email by customer ##
            $data_thank_you = array('name' => $name, 'email' => $email, 'id_lang' => $id_lang);
            $this->sendNotificationThankyouTestimonial($data_thank_you);
            ## send thank you email by customer ##


        }


        // send voucher with discount //
        $data_voucher = array();


        $data_permissions_vouchers = $this->getGroupPermissionsForVouchers(array('id'=>$id_review));
        $is_show_voucher = $data_permissions_vouchers['is_show_voucher'];

        $prefix = $this->getPrefix();

        if (Configuration::get($this->_name . 'vis_on' . $prefix) == 1 && $is_show_voucher == 1) {
            $data_voucher = $this->createVoucher(array('customer_id' => (int)$id_customer));


            $this->sendNotificationCreatedVoucher(
                array(
                    'email' => $email,
                    'name' => $name,
                    'data_voucher' => $data_voucher,
                    'id_lang' => $id_lang,

                )
            );

        }
        // send voucher with discount //


        $this->_clearSmartyCache();

        return $data_voucher;
    }

    private function _wrap_tag($data){

        $text1 = isset($data['text1'])?$data['text1']:'';
        $text2 = isset($data['text2'])?$data['text2']:'';
        $tag1 = isset($data['tag1'])?$data['tag1']:'';
        $tag2 = isset($data['tag2'])?$data['tag2']:'';
        $style1 = isset($data['style1'])?$data['style1']:'';
        $style2 = isset($data['style2'])?$data['style2']:'';

        $pre_tag1 = isset($data['pre_tag1'])?'<'.$data['pre_tag1'].'>':'';
        $pre_tag2 = isset($data['pre_tag2'])?'<'.$data['pre_tag2'].'>':'';

        return $pre_tag1.$pre_tag2.'<'.$tag1.' style="'.$style1.'"><'.$tag2.' style="'.$style2.'">'.$text1.': </'.$tag2.'></'.$tag1.'>'.$text2;
    }

    public function sendNotificationCreatedVoucher($data = null)
    {

        $prefix = $this->getPrefix();



        if (Configuration::get($this->_name . 'noti' . $prefix) == 1 && Configuration::get($this->_name.'is_revvoucr'.$prefix) == 1) {


            include_once(_PS_MODULE_DIR_ . $this->_name . '/spmgsnipreview.php');
            $obj = new spmgsnipreview();
            $data_translate = $obj->translateItems();

            $_data_translate_custom = $obj->translateCustom();


            $firsttext = $_data_translate_custom['firsttext'];

            $tax = $data_translate['tax' . $prefix];
            if ($tax) {
                $tax_text = ' (' . $_data_translate_custom['tax_included'] . ') ';
            } else {
                $tax_text = ' (' . $_data_translate_custom['tax_excluded'] . ') ';
            }


            $secondtext = $_data_translate_custom['secondtext'];
            $threetext = $_data_translate_custom['threetext'];


            $valuta = $data_translate['valuta' . $prefix];
            if ($valuta == "%") {
                $tax_text = "";
            }

            $discountvalue = $data_translate['discountvalue' . $prefix] . $tax_text;


            $text_voucher_title = $_data_translate_custom['review_text_voucher'];

            $email_customer = $data['email'];
            $customer_name = $data['name'];
            $voucher_code = $data['data_voucher']['voucher_code'];
            $date_until = $data['data_voucher']['date_until'];
            $id_lang = $data['id_lang'];

            include_once(_PS_MODULE_DIR_ . $this->_name . '/classes/spmgsnipreviewhelp.class.php');
            $obj_reviewshelp = new spmgsnipreviewhelp();
            $data_url = $obj_reviewshelp->getSEOURLs(array('id_lang' => $id_lang));
            $storereviews_url = $data_url['storereviews_url'];


            /* Email generation */
            $templateVars = array(
                '{firsttext}' => $firsttext,
                '{discountvalue}' => $discountvalue,
                '{secondtext}' => $secondtext,
                '{threetext}' => $threetext,
                '{voucher_code}' => $voucher_code,
                '{date_until}' => $date_until,
                '{customer_name}' => $customer_name,
                '{storereviews_url}' => $storereviews_url,
                '{text_voucher_title}' => $text_voucher_title,
            );


            $iso_lng = Language::getIsoById((int)($id_lang));

            $dir_mails = _PS_MODULE_DIR_ . '/' . $this->_name . '/' . 'mails/';

            if (is_dir($dir_mails . $iso_lng . '/')) {
                $id_lang_current = $id_lang;
            } else {
                $id_lang_current = Language::getIdByIso('en');
            }

            $subject_review_voucher = Configuration::get($this->_name . 'revvoucr' . $prefix . '_' . $id_lang);

            $id_shop = $this->getIdShop();

            /* Email sending */
            Mail::Send($id_lang_current, 'voucherserg-testim', $subject_review_voucher, $templateVars,
                $email_customer, 'Voucher Form', NULL, NULL,
                NULL, NULL, dirname(__FILE__) . '/../mails/', NULL, $id_shop);


        }

    }


    public function sendNotificationThankyouTestimonial($data = null)
    {
        if(Configuration::get($this->_name.'is_thankyou'.$this->getPrefix()) == 1) {

            $cookie = $this->context->cookie;

            $id_lang = isset($data['id_lang']) ? $data['id_lang'] : (int)($cookie->id_lang);


            $_prefix = $this->getPrefix();


            $subject_thank_you = Configuration::get($this->_name . 'thankyou' . $_prefix . '_' . $id_lang);

            $name = $data['name'];
            $email = $data['email'];
            /* Email generation */
            $templateVars = array(
                '{name}' => $name,

            );

            /* Email sending */

            $iso_lng = Language::getIsoById((int)($id_lang));

            $dir_mails = _PS_MODULE_DIR_ . '/' . $this->_name . '/' . 'mails/';

            if (is_dir($dir_mails . $iso_lng . '/')) {
                $id_lang_current = $id_lang;
            } else {
                $id_lang_current = Language::getIdByIso('en');
            }

            $id_shop = $this->getIdShop();

            Mail::Send($id_lang_current, 'testimony-thank-you', $subject_thank_you, $templateVars,
                $email, 'Thank you Form', NULL, NULL,
                NULL, NULL, dirname(__FILE__) . '/../mails/',NULL, $id_shop);


        }


    }

    public function getTestimonials($_data)
    {

        $start = $_data['start'];
        $step = $_data['step'];
        $admin = isset($_data['admin']) ? $_data['admin'] : null;
        $is_my = isset($_data['is_my']) ? $_data['is_my'] : null;


        //$cookie = $this->context->cookie;

        //$id_lang = (int)($cookie->id_lang);

        if ($admin) {
            $sql_admin = '
			SELECT pc.*,
			(SELECT ga2c.avatar_thumb from ' . _DB_PREFIX_ . '' . $this->_name . '_avatar2customer ga2c
                                                    WHERE ga2c.id_customer = pc.id_customer
                    ) as avatar_thumb
			FROM `' . _DB_PREFIX_ . '' . $this->_table_name . '` pc
			WHERE pc.`is_deleted` = 0
			ORDER BY pc.`date_add` DESC LIMIT ' . (int)($start) . ' ,' . (int)($step) . '';


            $reviews = Db::getInstance()->ExecuteS($sql_admin);


            foreach ($reviews as $i => $_item) {
                $id_customer = $_item['id_customer'];

                $avatar = $_item['avatar'];
                $info_path = $this->_getAvatarPath(array('avatar' => $avatar, 'id_customer' => $id_customer));
                $reviews[$i]['avatar'] = $info_path['avatar'];
                $reviews[$i]['is_show_ava'] = $info_path['is_show'];

            }


            $data_count_reviews = Db::getInstance()->getRow('
			SELECT COUNT(`id`) AS "count"
			FROM `' . _DB_PREFIX_ . '' . $this->_table_name . '`
			WHERE is_deleted = 0
			');
        } else {

            $frat = isset($_data['frat']) ? (int)$_data['frat'] : null;
            $sql_rating = '';
            if ($frat) {
                if ($frat > 5)
                    $frat = 5;
                $sql_rating = ' rating = ' . (int)$frat . ' AND ';
            }


            $is_search = isset($_data['is_search']) ? $_data['is_search'] : 0;
            $search = isset($_data['search']) ? $_data['search'] : '';


            $sql_condition_search = '';
            if ($is_search == 1) {
                $sql_condition_search = " (
	    		   LOWER(message) LIKE BINARY LOWER('%" . pSQL(trim($search)) . "%')
	    		   OR
	    		   LOWER(response) LIKE BINARY LOWER('%" . pSQL(trim($search)) . "%')
	    		   ) AND ";
            }

            $id_customer = isset($_data['id_customer']) ? $_data['id_customer'] : 0;
            $sql_customer = '';
            if ($id_customer) {
                $sql_customer = ' id_customer = ' . (int)($id_customer) . ' AND ';
            }


            ## sorting ##
            $sort_condition = isset($_data['sort_condition'])?$_data['sort_condition']:'';

            $sort_cond = 'pc.date_add DESC';

            if(Tools::strlen($sort_condition)>0) {
                $sort_condition = explode(":", $sort_condition);

                $name_condition = current($sort_condition);
                $name_condition = Tools::strtolower($name_condition);

                $order_way = end($sort_condition);
                $order_way = Tools::strtolower($order_way);

                if(!in_array($order_way,$this->_accepted_sort_way)){
                    $order_way = 'desc';
                }

                if (in_array($name_condition, $this->_accepted_sort_conditions)) {
                    $sort_cond = 'pc.'.$name_condition.' '.$order_way;
                }


            }
            ## sorting ##


            $sql_condition_lang_mutlistore = $this->getConditionMultilanguageAndMultiStore(array('and'=>0));

            $sql = '
			SELECT pc.*
			FROM `' . _DB_PREFIX_ . '' . $this->_table_name . '` pc
			WHERE '.(($is_my)?'':'pc.active = 1 AND').' pc.`is_deleted` = 0 AND ' . $sql_rating . ' ' . $sql_condition_search . ' ' . $sql_customer . '
			'.$sql_condition_lang_mutlistore.'
			ORDER BY '.pSQL($sort_cond).' LIMIT ' . (int)($start) . ' ,' . (int)($step) . '';


            $reviews = Db::getInstance()->ExecuteS($sql);


            $i = 0;
            foreach ($reviews as $k => $_item) {
                $id_customer = $_item['id_customer'];
                $is_buy = 0;
                if ($id_customer) {
                    $is_buy = $this->checkProductBought(array('id_customer' => $id_customer));
                } else {
                    $email = $_item['email'];
                    $is_buy = $this->checkProductBoughtByEmail(array('email' => $email));
                }
                //$is_buy = 1; //for test
                $reviews[$i]['is_buy'] = $is_buy;


                ## criterions ##
                $id_review = $reviews[$k]['id'];
                $id_lang_product_review = $reviews[$k]['id_lang'];
                $id_shop = $this->getIdShop();
                $data_criterions = $this->getCriterionsByProductReview(array('id_review' => $id_review, 'id_lang' => $id_lang_product_review, 'id_shop' => $id_shop));
                $reviews[$k]['criterions'] = $data_criterions;
                ## criterions ##

                $data_files = $this->getFiles2Review(array('id_review' => $id_review));
                $reviews[$k]['files'] = $data_files;


                $avatar = $_item['avatar'];
                $info_path = $this->_getAvatarPath(array('avatar' => $avatar, 'id_customer' => $id_customer));
                $reviews[$i]['avatar'] = $info_path['avatar'];
                $reviews[$i]['is_show_ava'] = $info_path['is_show'];

                $i++;
            }

            $sql_count = '
			SELECT COUNT(`id`) AS "count"
			FROM `' . _DB_PREFIX_ . '' . $this->_table_name . '`
			WHERE '.(($is_my)?'':'active = 1 AND').' is_deleted = 0 AND ' . $sql_rating . ' ' . $sql_condition_search . '  ' . $sql_customer . '
			'.$sql_condition_lang_mutlistore.'
			';

            $data_count_reviews = Db::getInstance()->getRow($sql_count);
        }
        return array('reviews' => $reviews, 'count_all_reviews' => $data_count_reviews['count']);
    }


    public function isExistsReviewByCustomer($data)
    {
        $is_customer = $data['id_customer'];

        $data_is_exists = Db::getInstance()->getRow('
			SELECT COUNT(`id`) AS "count"
			FROM `' . _DB_PREFIX_ . '' . $this->_table_name . '`
			WHERE is_deleted = 0 and id_customer = ' . (int)$is_customer . '
			');
        return $data_is_exists['count'];
    }

    public function getItem($_data)
    {
        $id = $_data['id'];
        $is_admin_tab = isset($_data['is_admin_tab'])?$_data['is_admin_tab']:0;

        $items = Db::getInstance()->ExecuteS('
			SELECT pc.*
			FROM `' . _DB_PREFIX_ . '' . $this->_table_name . '` pc
			WHERE pc.`is_deleted` = 0 AND pc.`id` = ' . (int)($id) . '');


        $cookie = $this->context->cookie;


        $i = 0;
        foreach ($items as $_item) {
            $id_lang = ($_item['id_lang'] != 0) ? $_item['id_lang'] : (int)($cookie->id_lang);

            $name_lang = Language::getLanguage((int)($id_lang));
            $items[$i]['name_lang'] = $name_lang['name'];


            $id_customer = isset($_item['id_customer']) ? $_item['id_customer'] : 0;
            $name = '';
            if ($id_customer) {
                $customer_data = $this->getInfoAboutCustomer(array('id_customer' => $id_customer, 'is_full' => 1));
                $name = $customer_data['customer_name'];
            }
            $items[$i]['customer_name'] = $name;


            ## criterions ##
            $id_review = $_item['id'];
            $id_lang_product_review = $_item['id_lang'];
            $id_shop = $_item['id_shop'];
            $data_criterions = $this->getCriterionsByProductReview(array('id_review' => $id_review, 'id_lang' => $id_lang_product_review, 'id_shop' => $id_shop));

            $items[$i]['criterions'] = $data_criterions;
            ## criterions ##

            ## files ##
            $data_files = $this->getFiles2Review(array('id_review' => $id_review,'is_admin_tab'=>$is_admin_tab));
            $items[$i]['files'] = $data_files;
            ## files ##

            ## user functional ###
            $user_url = '';
            if ($id_customer) {

                include_once(_PS_MODULE_DIR_.$this->_name . '/classes/spmgsnipreviewhelp.class.php');
                $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

                $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs(array('id_lang' => $id_lang));
                $user_url = $data_seo_url['user_url'];
            }

            $items[$i]['user_url'] = $user_url;


            include_once(_PS_MODULE_DIR_.$this->_name . '/classes/userprofileg.class.php');
            $obj_userprofileg = new userprofileg();

            $avatar = $_item['avatar'];

            $info_path = $obj_userprofileg->getAvatarPath(array('avatar' => $avatar, 'id_customer' => $id_customer));

            $items[$i]['avatar'] = $info_path['avatar'];
            $items[$i]['is_exist'] = $info_path['is_exist'];
            $items[$i]['is_show_ava'] = $info_path['is_show'];
            ## user functional ###

        }


        return array('reviews' => $items);
    }

    public function setPublsh($data)
    {
        $id = $data['id'];
        $active = $data['active'];

        $sql = 'UPDATE `' . _DB_PREFIX_ . '' . $this->_table_name . '`
	    				SET
				   		active = ' . (int)($active) . ',
				   		is_new = 0
				   		WHERE id = ' . (int)($id) . '
						   ';
        Db::getInstance()->Execute($sql);

        $this->_clearSmartyCache();
    }

    public function deteleItem($data)
    {
        $id = $data['id'];
        $sql = 'UPDATE `' . _DB_PREFIX_ . '' . $this->_table_name . '`
	    						SET
						   		is_deleted = 1, is_new = 0
						   		WHERE id = ' . (int)($id) . '';
        Db::getInstance()->Execute($sql);

        $this->_clearSmartyCache();
    }

    public function getIdShop()
    {
        $id_shop = 0;
        if (version_compare(_PS_VERSION_, '1.5', '>'))
            $id_shop = Context::getContext()->shop->id;
        return $id_shop;
    }


    public function saveItemAdmin($data){
        $id_customer = $data['id_customer'];


        $customer_data = $this->getInfoAboutCustomer(array('id_customer'=>$id_customer));
        $name = $customer_data['customer_name'];
        $email = $customer_data['email'];

        $id_lang = $data['id_lang'];
        $id_shop = $data['id_shop'];


        $message = $data['message'];






        $web = $data['web'];
        $address = $data['address'];
        $company = $data['company'];
        $country = $data['country'];
        $city = $data['city'];


        $sql_condition_web = '';
        if (Configuration::get($this->_name . 'is_web') == 1) {
            $sql_condition_web = '`web` = "' . pSQL($web) . '",';
        }

        $sql_condition_company = '';
        if (Configuration::get($this->_name . 'is_company') == 1) {
            $sql_condition_company = '`company` = "' . pSQL($company) . '",';
        }

        $sql_condition_address = '';
        if (Configuration::get($this->_name . 'is_addr') == 1) {
            $sql_condition_address = '`address` = "' . pSQL($address) . '",';
        }

        $sql_condition_country = '';
        if (Configuration::get($this->_name . 'is_country') == 1) {
            $sql_condition_country = '`country` = "' . pSQL($country) . '",';
        }

        $sql_condition_city = '';
        if (Configuration::get($this->_name . 'is_city') == 1) {
            $sql_condition_city = '`city` = "' . pSQL($city) . '",';
        }





        $response = $data['response'];
        $is_show = $data['is_show'];






        ## rating ##
        $ratings = $data['ratings'];
        $sizeof_rating = sizeof($ratings);
        $rating = 0;
        foreach($ratings as $rating_value){
            $rating = $rating + $rating_value;
        }
        $rating = round($rating/$sizeof_rating);
        ## rating ##


        $post_images = $data['post_images'];

        $filesrev = $data['filesrev'];





        $publish = $data['publish'];

        $date_add = date('Y-m-d H:i:s', strtotime($data['date_add']));




        $sql = 'INSERT into `' . _DB_PREFIX_ . '' . $this->_table_name . '`
	    						SET

	    						    `name` = "' . pSQL($name) . '",
						   			`email` = "' . pSQL($email) . '",
						   			id_customer = '.(int)($id_customer).',

						   			`rating` = "' . pSQL($rating) . '",
						   			`message` = "' . pSQL($message) . '",
						   			`date_add` = "' . pSQL($date_add) . '",
						   			`response` = "' . pSQL($response) . '",
						   			`is_show` = "' . pSQL($is_show) . '",
						   			' . $sql_condition_web . '
						   			' . $sql_condition_company . '
									' . $sql_condition_address . '
									' . $sql_condition_country . '
									' . $sql_condition_city . '
									id_lang = \''.(int)($id_lang).'\',
						            id_shop = \''.(int)($id_shop).'\',
						            `active` = ' . (int)($publish) . '
						   		';
        Db::getInstance()->Execute($sql);


        $id_review = Db::getInstance()->Insert_ID();


        include_once(_PS_MODULE_DIR_.$this->_name. '/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();

        $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();

        if ($obj_spmgsnipreview->is_demo == 0) {

            include_once(_PS_MODULE_DIR_.$this->_name . '/classes/userprofileg.class.php');
            $obj = new userprofileg();

            $obj->saveImageAvatar(array('id' => $id_review, 'post_images' => $post_images, 'id_customer' => $id_customer, 'is_storereviews' => 1));

            $this->saveFiles2Review(array('id_review'=>$id_review,'filesrev'=>$filesrev));

        }


        ### add criterions ###
        foreach($ratings as $id_criterion => $rating_value) {
            if($id_criterion > 0) {
                $sql = 'INSERT into `' . _DB_PREFIX_ . 'spmgsnipreview_review2criterion_'.pSQL($_prefix).'` SET
						   id_review = ' . (int)($id_review) . ',
						   id_criterion = ' . (int)($id_criterion) . ',
						   rating = '.(int)$rating_value.'
						   ';
                Db::getInstance()->Execute($sql);
            }
        }
        ### add criterions ###

        $this->_clearSmartyCache();

    }

    public function updateItem($data)
    {
        $name = $data['name'];
        $email = $data['email'];
        $web = $data['web'];
        $message = $data['message'];
        $publish = $data['publish'];
        $id = $data['id'];
        $company = $data['company'];
        $address = $data['address'];

        $country = $data['country'];
        $city = $data['city'];

        $response = $data['response'];
        $is_noti = $data['is_noti'];
        $is_show = $data['is_show'];

        $post_images = $data['post_images'];

        $filesrev = $data['filesrev'];

        $date_add = date('Y-m-d H:i:s', strtotime($data['date_add']));


        include_once(_PS_MODULE_DIR_.$this->_name. '/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();

        $_prefix = $obj_spmgsnipreview->getPrefixShopReviews();


        ## delete old criteria data ##
        $sql = 'DELETE FROM `'._DB_PREFIX_.'spmgsnipreview_review2criterion_'.pSQL($_prefix).'`
						WHERE id_review = '.(int)($id).'
						';
        Db::getInstance()->Execute($sql);


        ## rating ##
        $ratings = $data['ratings'];
        $sizeof_rating = sizeof($ratings);
        $rating = 0;
        foreach($ratings as $rating_value){
            $rating = $rating + $rating_value;
        }
        $rating_new = round($rating/$sizeof_rating);
        if($rating_new == 0){
            $rating_new = $data['rating_total'];
        }
        ## rating ##

        ## update new ratings ###
        foreach($ratings as $id_criterion => $rating_value) {
            if($id_criterion > 0) {
                $sql = 'INSERT into `' . _DB_PREFIX_ . 'spmgsnipreview_review2criterion_'.pSQL($_prefix).'` SET
						   id_review = ' . (int)($id) . ',
						   id_criterion = ' . (int)($id_criterion) . ',
						   rating = '.(int)$rating_value.'
						   ';
                Db::getInstance()->Execute($sql);
            }
        }
        ### update new ratings ###


        $sql_condition_web = '';
        if (Configuration::get($this->_name . 'is_web') == 1) {
            $sql_condition_web = '`web` = "' . pSQL($web) . '",';
        }

        $sql_condition_company = '';
        if (Configuration::get($this->_name . 'is_company') == 1) {
            $sql_condition_company = '`company` = "' . pSQL($company) . '",';
        }

        $sql_condition_address = '';
        if (Configuration::get($this->_name . 'is_addr') == 1) {
            $sql_condition_address = '`address` = "' . pSQL($address) . '",';
        }

        $sql_condition_country = '';
        if (Configuration::get($this->_name . 'is_country') == 1) {
            $sql_condition_country = '`country` = "' . pSQL($country) . '",';
        }

        $sql_condition_city = '';
        if (Configuration::get($this->_name . 'is_city') == 1) {
            $sql_condition_city = '`city` = "' . pSQL($city) . '",';
        }

        $sql = 'UPDATE `' . _DB_PREFIX_ . '' . $this->_table_name . '`
	    						SET `name` = "' . pSQL($name) . '",
						   			`email` = "' . pSQL($email) . '",
						   			`rating` = "' . pSQL($rating_new) . '",
						   			' . $sql_condition_web . '
						   			`message` = "' . pSQL($message) . '",
						   			`date_add` = "' . pSQL($date_add) . '",
						   			`response` = "' . pSQL($response) . '",
						   			`is_show` = "' . pSQL($is_show) . '",
						   			is_new = 0,
						   			' . $sql_condition_company . '
									' . $sql_condition_address . '
									' . $sql_condition_country . '
									' . $sql_condition_city . '
									`active` = ' . (int)($publish) . '
						   		WHERE id = ' . (int)($id) . '';
        Db::getInstance()->Execute($sql);



        if ($obj_spmgsnipreview->is_demo == 0) {



            include_once(_PS_MODULE_DIR_.$this->_name . '/classes/userprofileg.class.php');
            $obj = new userprofileg();

            $id_customer = isset($data['id_customer']) ? $data['id_customer'] : 0;
            $obj->saveImageAvatar(array('id' => $id, 'post_images' => $post_images, 'id_customer' => $id_customer, 'is_storereviews' => 1));


            $this->saveFiles2Review(array('id_review'=>$id,'filesrev'=>$filesrev));

        }

        if ($is_noti && Tools::strlen(trim($response)) > 0) {
            // send email
            $this->sendNotificationResponseTestimonial(array('id' => $id));
        }


        $this->_clearSmartyCache();

    }

    public function sendNotificationResponseTestimonial($data = null)
    {

        if(Configuration::get($this->_name.'is_resptest'.$this->getPrefix()) == 1) {

            include_once(_PS_MODULE_DIR_.$this->_name . '/spmgsnipreview.php');
            $obj_spmgsnipreview = new spmgsnipreview();
            $_data_translate = $obj_spmgsnipreview->translateItems();


            $_prefix = $this->getPrefix();

            $id = $data['id'];

            $_data_item_tmp = $this->getItem(array('id' => $id));
            $_data = $_data_item_tmp['reviews'][0];


            $cookie = $this->context->cookie;

            $id_lang = (int)($cookie->id_lang);
            $id_lang = isset($_data['id_lang']) ? $_data['id_lang'] : $id_lang;

            $name = isset($_data['name']) ? $_data['name'] : '';
            $email = isset($_data['email']) ? $_data['email'] : @Configuration::get('PS_SHOP_EMAIL');


            $iso_lng = Language::getIsoById((int)($id_lang));


            $data_url = $this->getSEOURLs(array('iso_lng' => $iso_lng));
            $items_url = $data_url['testimonials_url'];

            $response = isset($_data['response']) ? $_data['response'] : '';



            $message = isset($_data['message']) ? $this->_wrap_tag(array('text1'=>$_data_translate['message'],'text2'=>$_data['message'],'tag1'=>'span','tag2'=>'strong','style1'=>'color:#333')) : "";


            if (Configuration::get($this->_name . 'is_web') == 1) {
                $web = isset($_data['web']) ? $_data['web'] : '';
                if (Tools::strlen($web) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['web'],'text2'=>$web,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }

            }

            if (Configuration::get($this->_name . 'is_company') == 1) {
                $company = isset($_data['company']) ? $_data['company'] : '';
                if (Tools::strlen($company) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['company'],'text2'=>$company,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }

            }

            if (Configuration::get($this->_name . 'is_addr') == 1) {
                $address = isset($_data['address']) ? $_data['address'] : '';
                if (Tools::strlen($address) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['address'],'text2'=>$address,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }


            }

            if (Configuration::get($this->_name . 'is_country') == 1) {
                $country = isset($_data['country']) ? $_data['country'] : '';
                if (Tools::strlen($country) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['country'],'text2'=>$country,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }


            }

            if (Configuration::get($this->_name . 'is_city') == 1) {
                $city = isset($_data['city']) ? $_data['city'] : '';
                if (Tools::strlen($city) > 0){
                    $message .= $this->_wrap_tag(array('text1'=>$_data_translate['city'],'text2'=>$city,'tag1'=>'b','tag2'=>'span','pre_tag1'=>'br','pre_tag2'=>'br'));
                }


            }


            /* Email generation */
            $templateVars = array(
                '{name}' => $name,
                '{text}' => $message,
                '{response}' => $response,
                '{link}' => $items_url,

            );

            //echo "<pre>"; var_dump($templateVars); exit;

            /* Email sending */


            $dir_mails = _PS_MODULE_DIR_ . '/' . $this->_name . '/' . 'mails/';

            if (is_dir($dir_mails . $iso_lng . '/')) {
                $id_lang_current = $id_lang;
            } else {
                $id_lang_current = Language::getIdByIso('en');
            }

            $subject_response = Configuration::get($this->_name . 'resptest' . $_prefix . '_' . $id_lang);

            $id_shop = $this->getIdShop();

            Mail::Send($id_lang_current, 'response-testim', $subject_response, $templateVars,
                $email, 'Response Form', NULL, NULL,
                NULL, NULL, dirname(__FILE__) . '/../mails/', NULL, $id_shop);

        }

    }


    public function PageNav17($start, $count, $step, $_data = null){
            include_once(_PS_MODULE_DIR_.$this->_name.'/classes/pagenavhelp.class.php');
            $obj = new pagenavhelp();
            return $obj->pagenav($start, $count, $step, $_data);

    }


	
public function getfacebooklocale()
	{
        $locales = array();

        if (($xml=simplexml_load_file(_PS_MODULE_DIR_ . $this->_name."/lib/facebook_locales.xml")) === false)
            return $locales;

        for ($i=0;$i<sizeof($xml);$i++)
        {
            $locale = $xml->locale[$i]->codes->code->standard->representation;
            $locales[]= $locale;
        }

        return $locales;
	}
	
 	public function getfacebooklib($id_lang){
    	
    	$lang = new Language((int)$id_lang);
		
    	$lng_code = isset($lang->language_code)?$lang->language_code:$lang->iso_code;
    	if(strstr($lng_code, '-')){
			$res = explode('-', $lng_code);
			$language_iso = Tools::strtolower($res[0]).'_'.Tools::strtoupper($res[1]);
			$rss_language_iso = Tools::strtolower($res[0]);
		} else {
			$language_iso = Tools::strtolower($lng_code).'_'.Tools::strtoupper($lng_code);
			$rss_language_iso = $lng_code;
		}
			
			
		if (!in_array($language_iso, $this->getfacebooklocale()))
			$language_iso = "en_US";
		
		if (Configuration::get('PS_SSL_ENABLED') == 1)
			$url = "https://";
		else
			$url = "http://";
		
		
		
		return array('url'=>$url . 'connect.facebook.net/'.$language_iso.'/all.js#xfbml=1',
					  'lng_iso' => $language_iso, 'rss_language_iso' => $rss_language_iso);
    }
    
	public function createRSSFile($post_title,$post_description,$post_link,$post_pubdate)
	{
		
		
		$returnITEM = "<item>\n";
		# this will return the Title of the Article.
		$returnITEM .= "<title><![CDATA[".$post_title."]]></title>\n";
		# this will return the Description of the Article.
		$returnITEM .= "<description><![CDATA[".$post_description."]]></description>\n";
		# this will return the URL to the post.
		$returnITEM .= "<link>".$post_link."</link>\n";
		
		$returnITEM .= "<pubDate>".$post_pubdate."</pubDate>\n";
		$returnITEM .= "</item>\n";
		return $returnITEM;
	}
	
	public function getItemsForRSS(){
			
			$step = Configuration::get($this->_name.'n_rssitemst');

            $data_url = $this->getSEOURLs();
            $testimonials_url = $data_url['testimonials_url'];
			
			//$cookie = $this->context->cookie;
			//$current_language = (int)$cookie->id_lang;
			


            $sql_condition_lang_mutlistore = $this->getConditionMultilanguageAndMultiStore(array('and'=>0));

			$sql  = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.''.$this->_table_name.'` pc
			WHERE pc.active = 1 AND pc.`is_deleted` = 0 AND
			'.$sql_condition_lang_mutlistore.'
			ORDER BY pc.`date_add` DESC LIMIT '.(int)($step);
			
			$items = Db::getInstance()->ExecuteS($sql);

            if(!empty($items)) {

                foreach ($items as $k1 => $_item) {

                    //if ($current_language == $_item['id_lang']) {
                        $items[$k1]['title'] = $_item['name'];
                        $items[$k1]['seo_description'] = htmlspecialchars(strip_tags($_item['message']));
                        $items[$k1]['pubdate'] = date('D, d M Y H:i:s +0000', strtotime($_item['date_add']));

                        $items[$k1]['page'] = $testimonials_url;


                    //}


                }
            } else {
                $items = array();
            }
			
			
			return array('items' => $items);
	}
	
public function getLangISO(){
        $cookie = $this->context->cookie;
        $id_lang = (int)$cookie->id_lang;

        $all_laguages = Language::getLanguages(true);

        if(sizeof($all_laguages)>1)
            $iso_lang = Language::getIsoById((int)($id_lang))."/";
        else
            $iso_lang = '';

        return $iso_lang;
    	
    }
    


    public function getSEOURLs(){

        include_once(_PS_MODULE_DIR_.$this->_name.'/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $data_urls = $obj_spmgsnipreviewhelp->getSEOURLs();
        $testimonials_url = $data_urls['storereviews_url'];
        $my_account = $data_urls['my_account'];


        return array(
            'testimonials_url' => $testimonials_url,'my_account' => $my_account,

        );
    }

    public function getHttpost(){
        if(version_compare(_PS_VERSION_, '1.5', '>')){
            $custom_ssl_var = 0;
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (bool)Configuration::get('PS_SSL_ENABLED'))
                $custom_ssl_var = 1;


            if ($custom_ssl_var == 1)
                $_http_host = _PS_BASE_URL_SSL_.__PS_BASE_URI__;
            else
                $_http_host = _PS_BASE_URL_.__PS_BASE_URI__;

        } else {
            $_http_host = _PS_BASE_URL_.__PS_BASE_URI__;
        }
        return $_http_host;
    }



    public function deleteAvatar($data)
    {


        include_once(_PS_MODULE_DIR_.$this->_name . '/classes/userprofileg.class.php');
        $obj = new userprofileg();

        $id = (int)$data['id'];

        $info_post = $this->getItem(array('id'=>$id));
        $img = $info_post['reviews'][0]['avatar'];
        $data['avatar'] = $img;

        $data['is_storereviews'] = 1;

        $obj->deleteAvatar($data);

    }




    public function getInfoAboutCustomer($data=null){
        $id_customer = (int) $data['id_customer'];
        $is_full = isset($data['is_full'])?$data['is_full']:0;
        //get info about customer
            $sql = '
	        	SELECT * FROM `'._DB_PREFIX_.'customer`
		        WHERE `active` = 1 AND `id_customer` = \''.(int)($id_customer).'\'
		        AND `deleted` = 0 AND id_shop = '.(int)($this->getIdShop()).'  '.(defined(_MYSQL_ENGINE_)?"AND `is_guest` = 0":"").'
		        ';

        $result = Db::getInstance()->GetRow($sql);

            if(!$is_full)
                $lastname = Tools::strtoupper(Tools::substr($result['lastname'],0,1));
            else
                $lastname = $result['lastname'];

            $firstname = $result['firstname'];
            $customer_name = $firstname . " " . $lastname;
            $email = $result['email'];


        return array('customer_name' => $customer_name,'email'=>$email);
    }

    public function checkProductBought($data)
    {
        $id_customer = $data['id_customer'];

        if(!$id_customer){
            return 0;
        }

            $sql = 'SELECT count(o.id_order) as count FROM ' . _DB_PREFIX_ .'orders as o
					   WHERE o.id_customer = ' . (int)($id_customer) . '
					   AND o.id_shop = '.(int)($this->getIdShop());

        $result = Db::getInstance()->ExecuteS($sql);
        return (!empty($result[0]['count'])? 1 : 0);
    }


    public function checkProductBoughtByEmail($data)
    {
        $email = $data['email'];
        $customer = new Customer();
        $id_customer_data = Tools::strlen($email)>0?$customer->getByEmail($email):null;

        $id_customer = isset($id_customer_data->id)?$id_customer_data->id:0;

        if(!$id_customer){
            return 0;
        }

        $sql = 'SELECT count(o.id_order) as count FROM ' . _DB_PREFIX_ .'orders as o
					   WHERE o.id_customer = ' . (int)($id_customer) . '
					   AND o.id_shop = '.(int)($this->getIdShop());

        $result = Db::getInstance()->ExecuteS($sql);

        return (!empty($result[0]['count'])? 1 : 0);
    }


    public function getAvgReview(){

        $sql_condition = $this->getConditionMultilanguageAndMultiStore(array('and'=>1));

        $sql_cond_customer = '';
        $user_id = (int)Tools::getValue('uid');
        if($user_id){
            $sql_cond_customer = '`id_customer` = '.(int)($user_id).' AND ';
        }

        $result = Db::getInstance()->getRow('
		SELECT ceil(AVG(`rating`)) AS "avg_rating",  round(AVG(`rating`),1) AS "avg_rating_decimal"
		FROM `'._DB_PREFIX_.''.$this->_table_name.'` pc
		WHERE '.$sql_cond_customer.' active = 1 AND is_deleted = 0 AND rating != 0 '.$sql_condition.''
        );



        return array('avg_rating'=>(int)$result['avg_rating'],
                     'avg_rating_decimal'=>(isset($result['avg_rating_decimal'])?str_replace(".",",",$result['avg_rating_decimal']):0));
    }

    public function getCountReviews(){

        $sql_condition = $this->getConditionMultilanguageAndMultiStore(array('and'=>1));


        $sql_cond_customer = '';
        $user_id = (int)Tools::getValue('uid');
        if($user_id){
            $sql_cond_customer = '`id_customer` = '.(int)($user_id).' AND ';
        }

        $sql = 'SELECT COUNT(`id`) AS "count"
		FROM `'._DB_PREFIX_.''.$this->_table_name.'` pc
		WHERE '.$sql_cond_customer.' active = 1 AND is_deleted = 0 '.$sql_condition.' ';
        if (($result = Db::getInstance()->getRow($sql)) === false)
            return false;
        return (int)($result['count']);
    }

    public function getCountNewReviews(){

        $sql_condition = $this->getConditionMultilanguageAndMultiStore(array('and'=>1));

        $sql = 'SELECT COUNT(`id`) AS "count"
		FROM `'._DB_PREFIX_.''.$this->_table_name.'` pc
		WHERE is_deleted = 0 AND is_new = 1 '.$sql_condition.' ';

        if (($result = Db::getInstance()->getRow($sql)) === false)
            return false;
        return (int)($result['count']);
    }

    public function getCountRatingForItem(){


        $sql_condition = $this->getConditionMultilanguageAndMultiStore(array('and'=>1));

        $sql_cond_customer = '';
        $user_id = (int)Tools::getValue('uid');
        if($user_id){
            $sql_cond_customer = '`id_customer` = '.(int)($user_id).' AND ';
        }


        $data_return = array();
        // one
        $sql = 'select count(*) as count
					   FROM `'._DB_PREFIX_.''.$this->_table_name.'`
					   WHERE '.$sql_cond_customer.'  active = 1 AND is_deleted = 0 AND rating = 1  '.$sql_condition.'
					   ';
        $result = Db::getInstance()->getRow($sql);
        $data_return['one'] = (int)$result['count'];

        // two
        $sql = 'select count(*) as count
					   FROM `'._DB_PREFIX_.''.$this->_table_name.'`
					   WHERE '.$sql_cond_customer.' active = 1 AND is_deleted = 0 AND rating = 2 '.$sql_condition.'
					   ';
        $result = Db::getInstance()->getRow($sql);
        $data_return['two'] = (int)$result['count'];

        // three
        $sql = 'select count(*) as count
					   FROM `'._DB_PREFIX_.''.$this->_table_name.'`
					   WHERE '.$sql_cond_customer.' active = 1 AND is_deleted = 0 AND rating = 3 '.$sql_condition.'
					   ';
        $result = Db::getInstance()->getRow($sql);
        $data_return['three'] = (int)$result['count'];

        // four
        $sql = 'select count(*) as count
					   FROM `'._DB_PREFIX_.''.$this->_table_name.'`
					   WHERE '.$sql_cond_customer.' active = 1 AND is_deleted = 0 AND rating = 4 '.$sql_condition.'
					   ';
        $result = Db::getInstance()->getRow($sql);
        $data_return['four'] = (int)$result['count'];

        // five
        $sql = 'select count(*) as count
					   FROM `'._DB_PREFIX_.''.$this->_table_name.'`
					   WHERE '.$sql_cond_customer.' active = 1 AND is_deleted = 0 AND rating = 5 '.$sql_condition.'
					   ';
        $result = Db::getInstance()->getRow($sql);
        $data_return['five'] = (int)$result['count'];

        return $data_return;

    }

    private function getConditionMultilanguageAndMultiStore($data){

        $and = ($data['and']==1)?'AND':'';

        $id_shop = $this->getIdShop();
        $_prefix = $this->getPrefix();



        if(Configuration::get($this->_name.'rswitch_lng'.$_prefix) == 1){
            $cookie = $this->context->cookie;
            $id_lang = (int)($cookie->id_lang);
            $sql_condition = $and.'  id_lang = '.(int)($id_lang).' AND id_shop = '.(int)($id_shop).'';
        } else {
            $sql_condition = $and.'  id_shop = '.(int)($id_shop).'';
        }


        return $sql_condition;
    }


    private function _getAvatarPath($data){


        include_once(_PS_MODULE_DIR_.$this->_name.'/classes/userprofileg.class.php');
        $obj = new userprofileg();

        return $obj->getAvatarPath($data);
    }

    public function getAvatarForCustomer($data){
        include_once(_PS_MODULE_DIR_.$this->_name.'/classes/userprofileg.class.php');
        $obj = new userprofileg();

        return $obj->getAvatarForCustomer($data);
    }

    public function saveImageAvatar($data = null){
        include_once(_PS_MODULE_DIR_.$this->_name.'/classes/userprofileg.class.php');
        $obj = new userprofileg();

        return $obj->saveImageAvatar($data);


    }






    public  function getReviewCriteria($data)
    {
        $id_lang = (int)$data['id_lang'];
        $id_shop = (int)$data['id_shop'];

        $prefix = $this->getPrefix();

        $sql = '
			SELECT pc.id_spmgsnipreview_review_criterion, pcl.name, pcl.description
			FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_'.pSQL($prefix).'` pc
			    LEFT JOIN `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.pSQL($prefix).'` pcl
			    on(pcl.id_spmgsnipreview_review_criterion = pc.id_spmgsnipreview_review_criterion)
			WHERE FIND_IN_SET('.(int)($id_shop).',pc.id_shop)  AND pc.active = 1 AND pcl.id_lang = '.(int)$id_lang.'';

        $items = Db::getInstance()->executeS($sql);
        foreach($items as $k=>$item){

            $description = isset($item['description'])?$item['description']:'';
            $description = str_replace("\n","<br/>", $description);


            $items[$k]['description'] = $description;
        }

        return $items;
    }



    public function saveReviewCriteriaItem($data){

        $active = $data['active'];

        $ids_shops = implode(",",$data['cat_shop_association']);

        $prefix = $this->getPrefix();


        $sql = 'INSERT into `'._DB_PREFIX_.'spmgsnipreview_review_criterion_'.pSQL($prefix).'` SET
							   `active` = \''.(int)($active).'\',
							   `id_shop` = \''.pSQL($ids_shops).'\'
							   ';
        Db::getInstance()->Execute($sql);

        $id_block = Db::getInstance()->Insert_ID();

        foreach($data['data_content_lang'] as $language => $item){

            $description = $item['description'];
            $name = $item['name'];
            $sql = 'INSERT into `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.pSQL($prefix).'` SET
							   `id_lang` = \''.(int)($language).'\',
							   `name` = "'.pSQL($name,true).'",
							   `description` = "'.pSQL($description,true).'",
							   `id_spmgsnipreview_review_criterion` = \''.(int)($id_block).'\'
							   ';

            Db::getInstance()->Execute($sql);

        }


        $this->_clearSmartyCache();

        return $id_block;

    }


    public function updateReviewCriteriaItem($data){

        $prefix = $this->getPrefix();

        $active = $data['active'];
        $ids_shops = implode(",",$data['cat_shop_association']);
        $id = $data['id'];

        // update
        $sql = 'UPDATE `'._DB_PREFIX_.'spmgsnipreview_review_criterion_'.pSQL($prefix).'` SET
					     `active` = \''.(int)($active).'\',
					     `id_shop` = \''.pSQL($ids_shops).'\'
					    WHERE id_spmgsnipreview_review_criterion = '.(int)($id).'';

        Db::getInstance()->Execute($sql);


        $sql = 'DELETE FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.pSQL($prefix).'`
						WHERE id_spmgsnipreview_review_criterion = '.(int)($id).'
						';
        Db::getInstance()->Execute($sql);


        foreach($data['data_content_lang'] as $language => $item){

            $description = $item['description'];
            $name = $item['name'];

            $sql = 'INSERT into `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.pSQL($prefix).'` SET
							   `id_lang` = \''.(int)($language).'\',
							   `description` = "'.pSQL($description, true).'",
							   `name` = "'.pSQL($name, true).'",
							   `id_spmgsnipreview_review_criterion` = \''.(int)($id).'\'
							   ';
            Db::getInstance()->Execute($sql);

        }

        $this->_clearSmartyCache();

    }


    public function deleteReviewCriteriaItem($data){

        $prefix = $this->getPrefix();

        $id = $data['id'];


        $sql = 'DELETE FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_'.pSQL($prefix).'`
					   WHERE id_spmgsnipreview_review_criterion ='.(int)($id).'';
        Db::getInstance()->Execute($sql);


        $sql = 'DELETE FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.pSQL($prefix).'`
					   WHERE id_spmgsnipreview_review_criterion ='.(int)($id).'
					   ';
        Db::getInstance()->Execute($sql);

        $sql = 'DELETE FROM `'._DB_PREFIX_.'spmgsnipreview_review2criterion_'.pSQL($prefix).'`
					   WHERE id_criterion ='.(int)($id).'
					   ';
        Db::getInstance()->Execute($sql);


        $this->_clearSmartyCache();

    }


    public function getReviewCriteriaItem($_data = null){
        $id= (int)$_data['id'];

        $prefix = $this->getPrefix();

        $sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_'.pSQL($prefix).'` pc
			WHERE id_spmgsnipreview_review_criterion = '.(int)($id).'';

        $items = Db::getInstance()->ExecuteS($sql);

        foreach($items as $_item){
            $sql_data = '
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.pSQL($prefix).'` pc
				WHERE pc.id_spmgsnipreview_review_criterion = '.(int)($_item['id_spmgsnipreview_review_criterion']).'';

            $items_data = Db::getInstance()->ExecuteS($sql_data);

            foreach ($items_data as $item_data){
                $items['data'][$item_data['id_lang']]['description'] = $item_data['description'];
                $items['data'][$item_data['id_lang']]['name'] = $item_data['name'];

            }

        }
        return array('item' => $items);
    }

    public function getCriterionsByProductReview($data){
        $id_review = $data['id_review'];
        $id_lang = $data['id_lang'];
        $id_shop = $data['id_shop'];

        $prefix = $this->getPrefix();

        $sql = '
			SELECT grcl.id_spmgsnipreview_review_criterion, grcl.name, g2c.rating, grcl.description
			FROM `'._DB_PREFIX_.'spmgsnipreview_review2criterion_'.pSQL($prefix).'` g2c
			left join  '._DB_PREFIX_.'spmgsnipreview_review_criterion_'.pSQL($prefix).' grc
			on(g2c.id_criterion = grc.id_spmgsnipreview_review_criterion)
			left join '._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.pSQL($prefix).' grcl
			on(grcl.id_spmgsnipreview_review_criterion = grc.id_spmgsnipreview_review_criterion)
            where g2c.id_review = '.(int)$id_review.' AND grcl.id_lang = '.(int)$id_lang.'
            AND FIND_IN_SET('.(int)($id_shop).',grc.id_shop)
			ORDER BY grcl.`name` ASC
			';
        return Db::getInstance()->ExecuteS($sql);
    }


    public function getReviewCriteriaItems($_data = null){
        $start = isset($_data['start'])?$_data['start']:0;
        $step = $_data['step'];


        $prefix = $this->getPrefix();

        $sql = '
			SELECT pc.*
			FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_'.pSQL($prefix).'` pc
			ORDER BY pc.`id_spmgsnipreview_review_criterion` DESC
			LIMIT '.(int)($start).' ,'.(int)($step).'';
        $items = Db::getInstance()->ExecuteS($sql);


        foreach($items as $k => $_item){

            $items_data = Db::getInstance()->ExecuteS('
				SELECT pc.*
				FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.pSQL($prefix).'` pc
				WHERE pc.id_spmgsnipreview_review_criterion = '.(int)($_item['id_spmgsnipreview_review_criterion']).'
				');

            $cookie = $this->context->cookie;
            $defaultLanguage =  $cookie->id_lang;

            $tmp_title = '';
            // languages
            $languages_tmp_array = array();


            foreach ($items_data as $item_data){
                $languages_tmp_array[] = $item_data['id_lang'];

                $title = isset($item_data['name'])?$item_data['name']:'';
                if(Tools::strlen($tmp_title)==0){
                    if(Tools::strlen($title)>0)
                        $tmp_title = $title;
                }


                if($defaultLanguage == $item_data['id_lang']){
                    $items[$k]['name'] = $item_data['name'];
                }
            }

            // languages
            $items[$k]['ids_lng'] = $languages_tmp_array;


            $name_criteria = isset($items[$k]['name'])?$items[$k]['name']:'';
            if(Tools::strlen($name_criteria)==0)
                $items[$k]['name'] = $tmp_title;

        }

        $data_count = Db::getInstance()->getRow('
			SELECT COUNT(`id_spmgsnipreview_review_criterion`) AS "count"
			FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_'.pSQL($prefix).'`');

        return array('items' => $items, 'count_all' => $data_count['count']);
    }



    private function _eraseTmpDirectoryFromOldFiles($data){
        $path = $data['path'];

        $prev_cwd = getcwd();

        @chdir($path);
        $all_files = glob("*");

        $now  = date('Y-m-d H:i:s');
        $now = strtotime($now);


        foreach ($all_files as $filename) {


            $time_modified = filemtime($path.$filename);
            $time_modified = $time_modified+(86400*3); // delete files old than 3 days

            if($now > $time_modified){
                unlink($path.$filename); // delete old files
            }

        }


        @chdir($prev_cwd);



    }



    public function uploadTmpFile($data){
        $files = $data['files'];

        $message = '';
        $status = '';
        $name_file = '';
        $size_file = '';
        $is_error = 0;

        $dir_name_to_upload = dirname(__FILE__).$this->path_img_cloud.'tmpshopreviews'.DIRECTORY_SEPARATOR;

        // delete old files , clear directory tmp from spam :)
        $this->_eraseTmpDirectoryFromOldFiles(array('path'=>$dir_name_to_upload));
        // delete old files , clear directory tmp from spam :)

        $allowed = $this->_accepted_files;

        if (isset($files) && $files['error'][0] == 0) {


            $extension = pathinfo($files['name'][0], PATHINFO_EXTENSION);

            if (!in_array(Tools::strtolower($extension), $allowed)) {
                $message = 'Wrong file format, please try again!';
                $is_error = 1;
                $status = 'error';
            }


            if (!is_dir($dir_name_to_upload) && $is_error = 0) {
                $message = 'Wrong directory: ' . $dir_name_to_upload . ', please try again!';
                $is_error = 1;
                $status = 'error';

            }

            if ($is_error == 0) {
                move_uploaded_file($files['tmp_name'][0], $dir_name_to_upload . $files['name'][0]);

                $size_file = filesize($dir_name_to_upload . $files['name'][0]);
                $name_file = $files['name'][0];

            }

        } else {

            $message = $this->fileErrorMsg(array('error' => $files['error'][0]));
            $is_error = 1;
            $status = 'error';
        }

        return array('is_error'=>$is_error,'status'=>$status,'message'=>$message, 'size_file'=>$size_file,'name_file'=>$name_file);
    }


    public function deleteTmpFile($data){
        $name = $data['name'];
        $old_file_location = dirname(__FILE__).$this->path_img_cloud.'tmpshopreviews'.DIRECTORY_SEPARATOR;
        unlink($old_file_location.$name);
    }



    public function deleteFile($data){

        $prefix = $this->getPrefix();

        $id = $data['id'];
        $sql = '
                SELECT pc.full_path FROM `'._DB_PREFIX_.'spmgsnipreview_files2review_'.$prefix.'` pc WHERE pc.id_spmgsnipreview_files2review = '.(int)$id.'
                ';

        $item = Db::getInstance()->ExecuteS($sql);
        $full_path = isset($item[0]['full_path'])?$item[0]['full_path']:'';

        $path = dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR.$full_path;
        unlink($path);


        $sql_delete = '
                DELETE FROM `'._DB_PREFIX_.'spmgsnipreview_files2review_'.$prefix.'` WHERE id_spmgsnipreview_files2review = '.(int)$id.'
                ';
        Db::getInstance()->Execute($sql_delete);


    }


    public function saveFiles2Review($data){

        $filesrev = $data['filesrev'];

        if(!empty($filesrev)){

            if(count($filesrev)>0) {

                $id_review = $data['id_review'];


                foreach ($filesrev as $file) {
                    $this->saveFile2Review(array('id_review' => $id_review, 'file' => $file));
                }
            }
        }

    }


    public function saveFile2Review($data = null){

        $id_review = $data['id_review'];
        $file_name = $data['file'];


        $old_file_location = dirname(__FILE__).$this->path_img_cloud.'tmpshopreviews'.DIRECTORY_SEPARATOR;



        // create folders //

        $prev_cwd = getcwd();

        $dir_name_to_upload_file = dirname(__FILE__).$this->path_img_cloud.'filesshopreviews'.DIRECTORY_SEPARATOR;
        @chdir($dir_name_to_upload_file);

        $module_dir_files_shopreviews = $dir_name_to_upload_file.$id_review.DIRECTORY_SEPARATOR;
        @mkdir($module_dir_files_shopreviews, 0777);
        @chdir($module_dir_files_shopreviews);

        @chdir($prev_cwd);

        // create folders //


        srand((double)microtime()*1000000);
        $uniq_name_image = uniqid(rand());
        $uniq_name_image = $id_review.'-'.$uniq_name_image;



        if(!file_exists(($old_file_location.$file_name))) return;

        // copy from "tmptmpshopreviews" folder to "files" folder
        copy($old_file_location.$file_name,$module_dir_files_shopreviews.$file_name);


        $this->copyImage(
            array(
                'dir_without_ext'=>$module_dir_files_shopreviews.$uniq_name_image,
                'name'=>$module_dir_files_shopreviews.$file_name,
                'width'=>$this->_width_files,
                'height'=>$this->_height_files,
            )
        );

        $this->saveFile2ReviewInDB(array(

                'full_path' => $this->path_img_cloud_site.'filesshopreviews/'.$id_review.'/'.
                    $uniq_name_image.'-'.$this->_width_files.'x'.$this->_height_files.'.jpg',

                'id_review'=>$id_review,
            )
        );

        //delete old files
        unlink($old_file_location.$file_name);
        unlink($module_dir_files_shopreviews.$file_name);


    }


    public function saveFile2ReviewInDB($data){

        $prefix = $this->getPrefix();
        $full_path = $data['full_path'];
        $id_review = $data['id_review'];

        $sql = 'INSERT into `' . _DB_PREFIX_ . 'spmgsnipreview_files2review_'.$prefix.'` SET
						   id_review = ' . (int)($id_review) . ',
						   full_path = "' . pSQL($full_path) . '"
						   ';
        Db::getInstance()->Execute($sql);
    }




    public function copyImage($data){

        $filename = $data['name'];
        $dir_without_ext = $data['dir_without_ext'];

        $is_height_width = 0;
        if(isset($data['width']) && isset($data['height'])){
            $is_height_width = 1;
        }


        $width = isset($data['width'])?$data['width']:$this->_width_files_small;
        $height = isset($data['height'])?$data['height']:$this->_height_files_small;

        $width_orig_custom = $width;
        $height_orig_custom = $height;

        if (!$width){ $width = 85;}
        if (!$height){ $height = 85;}

        if(!file_exists(($filename))) return;
        // Content type
        $size_img = getimagesize($filename);
        // Get new dimensions
        list($width_orig, $height_orig) = getimagesize($filename);


        ## rotated ##
        $exif = exif_read_data($filename);

        if(!empty($exif['Orientation'])) {
            switch($exif['Orientation']) {
                case 6:
                    list($height_orig, $width_orig) = getimagesize($filename);
                    break;
            }
        }
        ## rotated ##

        $ratio_orig = $width_orig/$height_orig;

        if($width_orig>$height_orig){
            $height =  $width/$ratio_orig;
        }else{
            $width = $height*$ratio_orig;
        }
        if($width_orig<$width){
            $width = $width_orig;
            $height = $height_orig;
        }

        $image_p = imagecreatetruecolor($width, $height);
        $bgcolor=ImageColorAllocate($image_p, 255, 255, 255);
        //
        imageFill($image_p, 5, 5, $bgcolor);

        if ($size_img[2]==2){ $image = imagecreatefromjpeg($filename);}
        else if ($size_img[2]==1){  $image = imagecreatefromgif($filename);}
        else if ($size_img[2]==3) { $image = imagecreatefrompng($filename); }

        ## rotated ##
        if(!empty($exif['Orientation'])) {
            switch($exif['Orientation']) {
                case 6:
                    $image = imagerotate($image,-90,0);
                    break;
            }
        }
        ## rotated ##

        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
        // Output

        if ($is_height_width)
            $users_img = $dir_without_ext.'-'.$width_orig_custom.'x'.$height_orig_custom.'.jpg';
        else
            $users_img = $dir_without_ext.'.jpg';

        if ($size_img[2]==2)  imagejpeg($image_p, $users_img, 100);
        else if ($size_img[2]==1)  imagejpeg($image_p, $users_img, 100);
        else if ($size_img[2]==3)  imagejpeg($image_p, $users_img, 100);
        imageDestroy($image_p);
        imageDestroy($image);
        //unlink($filename);

    }


    public function getFiles2Review($data){

        $prefix = $this->getPrefix();
        $id_review = $data['id_review'];
        $is_admin_tab = isset($data['is_admin_tab'])?$data['is_admin_tab']:0;

        $sql = '
			SELECT pc.id_spmgsnipreview_files2review as id , pc.full_path
			FROM `'._DB_PREFIX_.'spmgsnipreview_files2review_'.$prefix.'` pc
            WHERE pc.id_review = '.(int)$id_review.'
			ORDER BY pc.`id_spmgsnipreview_files2review` DESC

			';
        $items = Db::getInstance()->ExecuteS($sql);


        ## resize image ##
        foreach($items as $_k => $_item){
            $full_path = $_item['full_path'];

            $small_image_tmp = explode("/",$full_path);
            $small_image_tmp = end($small_image_tmp);


            $new_small_file_name_path = current(explode(".",$small_image_tmp));

            $new_small_file_name = $new_small_file_name_path."-small-".$this->_width_files_small."x".$this->_height_files_small.".jpg";

            $module_dir_files_category_product_review = str_replace($small_image_tmp,"",$full_path);

            $pre_path = $module_dir_files_category_product_review;



            $path_to_imageon_the_site = $module_dir_files_category_product_review.$new_small_file_name;

            $files_structure_path = dirname(__FILE__).DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR;
            $module_dir_files_category_product_review = $files_structure_path.$module_dir_files_category_product_review;


            $path_to_small_img = $module_dir_files_category_product_review.$new_small_file_name;

            if(!file_exists(($path_to_small_img))) {

                $this->copyImage(
                    array(
                        'dir_without_ext' => $module_dir_files_category_product_review.$new_small_file_name_path."-small",
                        'name' => $files_structure_path.$pre_path.$small_image_tmp,
                        'width' => $this->_width_files_small,
                        'height' => $this->_height_files_small,
                    )
                );
            }


            $items[$_k]['small_path']=$path_to_imageon_the_site;


            if($is_admin_tab) {
                $id = $_item['id'];
                $items[$_k]['id'] = $id;

                $items[$_k]['image_name'] = $small_image_tmp;
                $image_to_get_size = $files_structure_path . $pre_path . $small_image_tmp;

                if(file_exists($image_to_get_size)) {
                    $file_size_image = filesize($image_to_get_size);
                    $file_size_image = $this->humanizeSize($file_size_image);
                } else {
                    $file_size_image = 0;
                }
                $items[$_k]['filesize'] = $file_size_image;
            }
        }
        ## resize image ##

        return $items;
    }

    private function humanizeSize($bytes)
    {
        /*if (gettype($bytes) !== 'number') {
        return '';
        }*/

        if ($bytes >= 1000000000) {
            return round(($bytes / 1000000000),2) . ' GB';
        }

        if ($bytes >= 1000000) {
            return round(($bytes / 1000000),2) . ' MB';
        }

        return round(($bytes / 1000),2) . ' KB';
    }

    public function getGroupPermissionsForVouchers($data = null){

        $id = isset($data['id'])?$data['id']:0;
        $prefix = $this->getPrefix();

        $cookie = $this->context->cookie;
        $id_customer_cookie = isset($cookie->id_customer)?$cookie->id_customer:0;




        ## group ##
        if(!$id_customer_cookie) {
            $_id_group = (int) Group::getCurrent()->id;
        } else {

            $_data_item_tmp = $this->getItem(array('id' => $id));

            $_data = isset($_data_item_tmp['reviews'][0])?$_data_item_tmp['reviews'][0]:array();

            $id_customer = isset($_data['id_customer']) ? $_data['id_customer'] : $id_customer_cookie;
            $_id_group = Customer::getDefaultGroupId($id_customer);
        }
        ## group ##




        $ids_groups = Configuration::get($this->_name.'ids_groups'.$prefix);
        $ids_groups = explode(",",$ids_groups);
        if(in_array($_id_group,$ids_groups)){
            $is_show_voucher = 1;
        } else {
            $is_show_voucher = 0;
        }

        return array('is_show_voucher'=>$is_show_voucher);
    }


    private function idGuest(){
        $cookie = $this->context->cookie;


        $id_guest = (int)$cookie->id_guest;
        return $id_guest;
    }

    public function createVoucher($data){


        $prefix = $this->getPrefix();

        $cookie = $this->context->cookie;
        $name_module = $this->_name;
        $code_module = Configuration::get($name_module.'vouchercode'.$prefix);

        if(!$data['customer_id']){
            $id_guest = $this->idGuest();

            // id_customer
            $sql_customer = 'SELECT id_customer FROM '._DB_PREFIX_.'guest WHERE id_guest='.(int)($id_guest);
            $uid = (int)Db::getInstance()->getValue($sql_customer);
        } else {
            $uid = $data['customer_id'];
        }



        Db::getInstance()->Execute('BEGIN');

        $code_v = '';
        $different = strtotime(date('Y-m-d H:i:s'));

        $id_currency = null;
        switch (Configuration::get($this->_name.'discount_type'.$prefix))
        {
            case 1:
                // percent
                $id_discount_type = 1;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get($this->_name.'percentage_val'.$prefix);
                break;
            case 2:
                // currency
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get('sdamount'.$prefix.'_'.(int)$id_currency);
                break;
            default:
                $id_discount_type = 2;
                $id_currency = (int)$cookie->id_currency;
                $value = Configuration::get('sdamount'.$prefix.'_'.(int)$id_currency);
        }




        $current_language = (int)$cookie->id_lang;

        $coupon = new CartRule();

        $gen_pass = Tools::strtoupper(Tools::passwdGen(8));

            foreach (Language::getLanguages() AS $language){
                $coupon->name[(int)$language['id_lang']] = $code_module.'-'.$gen_pass;
            }
            $coupon->description = Configuration::get($name_module.'coupondesc'.$prefix.'_'.$current_language);


        $codename = $code_module.'-'.$gen_pass;
        $category = explode(",",Configuration::get($name_module.'catbox'.$prefix));


            $coupon->code = $codename;
            $type = $id_discount_type == 2? 'reduction_amount' : 'reduction_percent';

            $coupon->$type = ($value);

            $coupon->reduction_currency = (int)($id_currency);
            if(Configuration::get($name_module.'isminamount'.$prefix) == true ||
                Configuration::get($name_module.'isminamount'.$prefix) == 1){
                $coupon->minimum_amount = (int)(Configuration::get('sdminamount'.$prefix.'_'.(int)$id_currency));
                $coupon->minimum_amount_currency = (int)($id_currency);
                $coupon->minimum_amount_tax= (int)Configuration::get($name_module.'tax'.$prefix);
            }

            if($id_discount_type == 2)
                $coupon->reduction_tax = (int)Configuration::get($name_module.'tax'.$prefix);


            if (sizeof($category)>0) {
                $coupon->product_restriction = 1;

                if($id_discount_type == 1){
                    $coupon->reduction_product = -2;
                }
            }





        // shared data
        $coupon->value = ($value);
        $coupon->id_customer = $uid;
        $coupon->quantity = 1;
        $coupon->quantity_per_user = 1;

        // cumulable

        $coupon->cart_rule_restriction = ((Configuration::get($name_module.'cumulativeother'.$prefix))==0?1:0);

        $coupon->cumulable = (int)(Configuration::get($name_module.'cumulativeother'.$prefix));

        $coupon->highlight = (int)(Configuration::get($name_module.'highlight'.$prefix));

        $coupon->cumulable_reduction = (int)(Configuration::get($name_module.'cumulativereduc'.$prefix));
        // cumulable


        $coupon->active = 1;

        $start_date = date('Y-m-d H:i:s');
        $coupon->date_from = $start_date;

        $different = strtotime(date('Y-m-d H:i:s')) + Configuration::get($this->_name.'sdvvalid'.$prefix)*24*60*60;
        $end_date = date('Y-m-d H:i:s',$different);
        $coupon->date_to = $end_date;


        $is_voucher_create = false;

            $is_voucher_create = $coupon->add(true, false);

            if ($is_voucher_create && sizeof($category)>0)
            {
                // add a cart rule
                $is_voucher_create = $this->addProductRule($coupon->id, 1, 'categories', $category);
            }


        if (!$is_voucher_create){
            Db::getInstance()->Execute('ROLLBACK');
        }

        $code_v = $codename;



        Db::getInstance()->Execute('COMMIT');



        return array('voucher_code'=>$code_v,'date_until' => date('d/m/Y H:i:s',$different));
    }


    public function addProductRule($cart_rule_id, $qty, $type, array $ids)
    {
        $insert = false;

        // set transaction
        Db::getInstance()->Execute('BEGIN');

        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'cart_rule_product_rule_group (id_cart_rule, quantity) VALUES('
            . (int)($cart_rule_id) . ', ' . (int)($qty) . ')';

        // only if group rule is added
        if (Db::getInstance()->Execute($sql)) {

            $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'cart_rule_product_rule (id_product_rule_group, type) VALUES('
                . (int)(Db::getInstance()->Insert_ID()) . ', "' . pSQL($type) . '")';

            // only if product rule is added
            if (Db::getInstance()->Execute($sql)) {

                if (!empty($ids)) {
                    $insert = true;

                    $iLastInsertId = Db::getInstance()->Insert_ID();

                    foreach ($ids as $id) {
                        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'cart_rule_product_rule_value (id_product_rule, id_item) VALUES('
                            . (int)($iLastInsertId) . ', ' . (int)($id) . ')';

                        if (!Db::getInstance()->Execute($sql)) {
                            $insert = false;
                        }
                    }
                }
            }
        }
        // commit or rollback transaction
        $insert = ($insert)? Db::getInstance()->Execute('COMMIT') : Db::getInstance()->Execute('ROLLBACK');

        return $insert;
    }




    /* reminder status */
    public function updateReminderForCustomer($data){
        $id_customer = $data['id_customer'];
        $reminder_status = $data['reminder_status'];

        $id_shop = $this->getIdShop();

        $_prefix = $this->getPrefix();

        $is_exists = $this->isExists(array('id_customer'=>$id_customer));
        if($is_exists){

            // update
            $sql = 'UPDATE `'._DB_PREFIX_.'spmgsnipreview_reminder2customer_'.$_prefix.'` SET
						   status = '.(int)$reminder_status.'
						   WHERE id_customer = '.(int)$id_customer.' and id_shop = '.(int)$id_shop;

        } else {
            // insert
            $sql = 'INSERT into `'._DB_PREFIX_.'spmgsnipreview_reminder2customer_'.$_prefix.'` SET
						   id_customer = '.(int)($id_customer).',
						   id_shop = '.(int)($id_shop).',
						   status = \''.(int)$reminder_status.'\'
						   ';
        }

        Db::getInstance()->Execute($sql);



    }

    public function isExists($data){
        $id_customer = $data['id_customer'];
        $id_shop = $this->getIdShop();
        $_prefix = $this->getPrefix();

        $sql = '
			SELECT count(*) as count
			FROM `'._DB_PREFIX_.'spmgsnipreview_reminder2customer_'.$_prefix.'` pc
			WHERE pc.id_customer = '.(int)$id_customer.' and pc.id_shop = '.(int)$id_shop;

        $is_exists = Db::getInstance()->getRow($sql);
        return $is_exists['count'];
    }

    public function getStatus($data){
        $id_customer = $data['id_customer'];
        $id_shop = $this->getIdShop();

        $_prefix = $this->getPrefix();

        $sql = '
			SELECT status
			FROM `'._DB_PREFIX_.'spmgsnipreview_reminder2customer_'.$_prefix.'` pc
			WHERE pc.id_customer = '.(int)$id_customer.' and pc.id_shop = '.(int)$id_shop;

        $is_exists = Db::getInstance()->getRow($sql);
        return $is_exists['status'];
    }

    /* reminder status */



    private function _clearSmartyCache(){

        include_once(_PS_MODULE_DIR_.$this->_name.'/classes/cachespmgsnipreview.class.php');
        $obj = new cachespmgsnipreview();
        $obj->clearSmartyCacheModule(array('is_shop_reviews'=>1));
    }



    public function shopreviews16($data){

        $_prefix_shop_reviews = $data['prefix'];

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];


        $title = $data['title'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $is_storerev_title = $data['is_storerev_title'];
        $perpage_title = $data['perpage_title'];
        $perpagemy_title = $data['perpagemy_title'];
        $rswitch_lng_title = $data['rswitch_lng_title'];
        $whocanadd_title = $data['whocanadd_title'];
        $is_avatar_title = $data['is_avatar_title'];
        $is_captcha_title = $data['is_captcha_title'];
        $is_web_title = $data['is_web_title'];
        $is_company_title = $data['is_company_title'];
        $is_addr_title = $data['is_addr_title'];
        $is_country_title = $data['is_country_title'];
        $is_city_title = $data['is_city_title'];
        $is_files_title = $data['is_files_title'];
        $ruploadfiles_title = $data['ruploadfiles_title'];
        $is_sortf_title = $data['is_sortf_title'];
        $is_sortfu_title = $data['is_sortfu_title'];

        $title1 = $data['title1'];
        $is_filterall_title = $data['is_filterall_title'];

        $title2 = $data['title2'];
        $rssontestim_title = $data['rssontestim_title'];
        $n_rssitemst_title = $data['n_rssitemst_title'];

        $update_title = $data['update_title'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title,
                    'icon' => 'fa fa-cogs fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $is_storerev_title,
                        'name' => 'is_storerev',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),






                    array(
                        'type' => 'text',
                        'label' => $perpage_title,
                        'name' => 'perpage'.$_prefix_shop_reviews,
                        'class' => ' fixed-width-sm',

                    ),

                    array(
                        'type' => 'text',
                        'label' => $perpagemy_title,
                        'name' => 'perpagemy'.$_prefix_shop_reviews,
                        'class' => ' fixed-width-sm',

                    ),




                    array(
                        'type' => 'block_radio_buttons_reviews_custom',
                        'label' => $whocanadd_title,

                        'name' => 'block_radio_buttons_reviews_custom',
                        'values'=> array(
                            'value' => Configuration::get($this->_name.'whocanadd'.$_prefix_shop_reviews)
                        ),

                    ),

                    array(
                        'type' => 'switch',
                        'label' => $rswitch_lng_title,
                        'name' => 'rswitch_lng'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_avatar_title,
                        'name' => 'is_avatar',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_captcha_title,
                        'name' => 'is_captcha'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_web_title,
                        'name' => 'is_web',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_company_title,
                        'name' => 'is_company',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_addr_title,
                        'name' => 'is_addr',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_country_title,
                        'name' => 'is_country',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_city_title,
                        'name' => 'is_city',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),


                    array(
                        'type' => 'switch',
                        'label' => $is_files_title,
                        'name' => 'is_files'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $ruploadfiles_title,
                        'name' => 'ruploadfiles'.$_prefix_shop_reviews,
                        'class' => ' fixed-width-sm',


                    ),


                    array(
                        'type' => 'switch',
                        'label' =>$is_sortf_title,
                        'name' => 'is_sortf'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_sortfu_title,
                        'name' => 'is_sortfu'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),



                ),
            ),
        );


        $fields_form1 = array(
            'form'=> array(

                'legend' => array(
                    'title' => $title1,
                    'icon' => 'fa fa-filter fa-lg'
                ),
                'input' => array(


                    array(
                        'type' => 'switch',
                        'label' => $is_filterall_title,
                        'name' => 'is_filterall'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),



                ),



            ),


        );



        $fields_form2 = array(
            'form'=> array(

                'legend' => array(
                    'title' => $title2,
                    'icon' => 'fa fa-rss fa-lg'
                ),
                'input' => array(


                    array(
                        'type' => 'switch',
                        'label' =>$rssontestim_title,
                        'name' => 'rssontestim',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $n_rssitemst_title,
                        'name' => 'n_rssitemst',
                        'class' => ' fixed-width-sm',

                    ),

                ),



            ),


        );

        $fields_form3 = array(
            'form' => array(


                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();



        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'submit_testimonials';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesTestimonialsSettings(array('prefix'=>$_prefix_shop_reviews)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );



        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_mainsettings16_js.phtml');
        $_html = ob_get_clean();




        return  $_html . $helper->generateForm(array($fields_form,$fields_form1, $fields_form2,$fields_form3));
    }

    public function getConfigFieldsValuesTestimonialsSettings($data){

        $_prefix_shop_reviews = $data['prefix'];

        $data_config = array(
            'is_storerev'=> Configuration::get($this->_name.'is_storerev'),


            'perpage'.$_prefix_shop_reviews => Configuration::get($this->_name.'perpage'.$_prefix_shop_reviews),
            'perpagemy'.$_prefix_shop_reviews => Configuration::get($this->_name.'perpagemy'.$_prefix_shop_reviews),


            'is_avatar'=>Configuration::get($this->_name.'is_avatar'),
            'is_captcha'.$_prefix_shop_reviews=>Configuration::get($this->_name.'is_captcha'.$_prefix_shop_reviews),
            'is_web'=>Configuration::get($this->_name.'is_web'),
            'is_company'=>Configuration::get($this->_name.'is_company'),
            'is_addr'=>Configuration::get($this->_name.'is_addr'),
            'is_country'=>Configuration::get($this->_name.'is_country'),
            'is_city'=>Configuration::get($this->_name.'is_city'),


            'rssontestim'=> Configuration::get($this->_name.'rssontestim'),
            'n_rssitemst'=> Configuration::get($this->_name.'n_rssitemst'),

            'rswitch_lng'.$_prefix_shop_reviews=> Configuration::get($this->_name.'rswitch_lng'.$_prefix_shop_reviews),


            'is_files'.$_prefix_shop_reviews => Configuration::get($this->_name.'is_files'.$_prefix_shop_reviews),
            'ruploadfiles'.$_prefix_shop_reviews => (int)Configuration::get($this->_name.'ruploadfiles'.$_prefix_shop_reviews),

            'is_filterall'.$_prefix_shop_reviews => (int)Configuration::get($this->_name.'is_filterall'.$_prefix_shop_reviews),

            'is_sortf'.$_prefix_shop_reviews=> (int)Configuration::get($this->_name.'is_sortf'.$_prefix_shop_reviews),
            'is_sortfu'.$_prefix_shop_reviews=> (int)Configuration::get($this->_name.'is_sortfu'.$_prefix_shop_reviews),

        );

        return $data_config;

    }



    public function owlcarouselsshop($data){


        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $title = $data['title'];

        $tlast_title_l = $data['tlast_title_l'];
        $tlast_title_ls = $data['tlast_title_ls'];
        $tlast_title_r = $data['tlast_title_r'];
        $tlast_title_rs = $data['tlast_title_rs'];
        $tlast_title_f = $data['tlast_title_f'];
        $tlast_title_h = $data['tlast_title_h'];


        $BGCOLOR_TIT_title = $data['BGCOLOR_TIT_title'];
        $BGCOLOR_TIT_desc = $data['BGCOLOR_TIT_desc'];
        $BGCOLOR_T_title = $data['BGCOLOR_T_title'];
        $BGCOLOR_T_desc = $data['BGCOLOR_T_desc'];

        $r_pos_store_reviews_block_title = $data['r_pos_store_reviews_block_title'];

        $t_left = $data['t_left'];
        $t_right = $data['t_right'];
        $t_footer = $data['t_footer'];
        $t_home = $data['t_home'];
        $t_leftside = $data['t_leftside'];
        $t_rightside = $data['t_rightside'];

        $r_google_rich_snippets_in_places = $data['r_google_rich_snippets_in_places'];
        $t_tpages = $data['t_tpages'];

        $title1 = $data['title1'];
        $sr_slider = $data['sr_slider'];
        $yes_title= $data['yes_title'];
        $no_title = $data['no_title'];
        $sr_sl = $data['sr_sl'];

        $title2 = $data['title2'];
        $sr_sliderh = $data['sr_sliderh'];
        $sr_slh = $data['sr_slh'];

        $update_title = $data['update_title'];

        $fields_form = array(
            'form'=> array(

                'legend' => array(
                    'title' => $title,
                    'icon' => 'fa fa-list-alt fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'text',
                        'label' => $tlast_title_l,
                        'name' => 'tlast_l',
                        'class' => ' fixed-width-sm',

                    ),

                    array(
                        'type' => 'text',
                        'label' => $tlast_title_ls,
                        'name' => 'tlast_ls',
                        'class' => ' fixed-width-sm',

                    ),

                    array(
                        'type' => 'text',
                        'label' => $tlast_title_r,
                        'name' => 'tlast_r',
                        'class' => ' fixed-width-sm',

                    ),

                    array(
                        'type' => 'text',
                        'label' => $tlast_title_rs,
                        'name' => 'tlast_rs',
                        'class' => ' fixed-width-sm',

                    ),

                    array(
                        'type' => 'text',
                        'label' => $tlast_title_f,
                        'name' => 'tlast_f',
                        'class' => ' fixed-width-sm',

                    ),

                    array(
                        'type' => 'text',
                        'label' => $tlast_title_h,
                        'name' => 'tlast_h',
                        'class' => ' fixed-width-sm',

                    ),

                    array(
                        'type' => 'color',
                        'lang' => true,
                        'label' => $BGCOLOR_TIT_title,
                        'name' => $this->_name.'BGCOLOR_TIT',
                        'desc' => $BGCOLOR_TIT_desc
                    ),

                    array(
                        'type' => 'color',
                        'lang' => true,
                        'label' => $BGCOLOR_T_title,
                        'name' => $this->_name.'BGCOLOR_T',
                        'desc' => $BGCOLOR_T_desc
                    ),


                    array(
                        'type' => 'checkbox_custom_blocks_store',
                        'label' => $r_pos_store_reviews_block_title,
                        'name' => 'r_pos_store_reviews_block',
                        'values' => array(
                            'query' => array(

                                array(
                                    'id' => 't_left',
                                    'name' => $t_left,
                                    'val' => 1,
                                    'mobile'=>Configuration::get($this->_name.'mt_left'),
                                    'site'=>Configuration::get($this->_name.'st_left'),
                                ),


                                array(
                                    'id' => 't_right',
                                    'name' => $t_right,
                                    'val' => 1,
                                    'mobile'=>Configuration::get($this->_name.'mt_right'),
                                    'site'=>Configuration::get($this->_name.'st_right'),
                                ),


                                array(
                                    'id' => 't_footer',
                                    'name' => $t_footer,
                                    'val' => 1,
                                    'mobile'=>Configuration::get($this->_name.'mt_footer'),
                                    'site'=>Configuration::get($this->_name.'st_footer'),
                                ),

                                array(
                                    'id' => 't_home',
                                    'name' => $t_home,
                                    'val' => 1,
                                    'mobile'=>Configuration::get($this->_name.'mt_home'),
                                    'site'=>Configuration::get($this->_name.'st_home'),
                                ),
                                array(
                                    'id' => 't_leftside',
                                    'name' => $t_leftside,
                                    'val' => 1,
                                    'mobile'=>Configuration::get($this->_name.'mt_leftside'),
                                    'site'=>Configuration::get($this->_name.'st_leftside'),
                                ),

                                array(
                                    'id' => 't_rightside',
                                    'name' => $t_rightside,
                                    'val' => 1,
                                    'mobile'=>Configuration::get($this->_name.'mt_rightside'),
                                    'site'=>Configuration::get($this->_name.'st_rightside'),
                                ),





                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),

                    array(
                        'type' => 'checkbox_custom_blocks',
                        'label' => $r_google_rich_snippets_in_places,
                        'name' => 'r_google_rich_snippets_in_places',
                        'values' => array(
                            'query' => array(

                                array(
                                    'id' => 't_lefts',
                                    'name' => $t_left,
                                    'val' => 1
                                ),


                                array(
                                    'id' => 't_rights',
                                    'name' => $t_right,
                                    'val' => 1
                                ),


                                array(
                                    'id' => 't_footers',
                                    'name' => $t_footer,
                                    'val' => 1
                                ),

                                array(
                                    'id' => 't_homes',
                                    'name' => $t_home,
                                    'val' => 1
                                ),
                                array(
                                    'id' => 't_leftsides',
                                    'name' => $t_leftside,
                                    'val' => 1
                                ),

                                array(
                                    'id' => 't_rightsides',
                                    'name' => $t_rightside,
                                    'val' => 1
                                ),
                                array(
                                    'id' => 't_tpages',
                                    'name' => $t_tpages,
                                    'val' => 1
                                ),


                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),

                    ),



                ),



            ),


        );

        $fields_form1 = array(
            'form'=> array(

                'legend' => array(
                    'title' => $title1,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(



                    array(
                        'type' => 'switch',
                        'label' => $sr_slider,
                        'name' => 'sr_slider',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $sr_sl,
                        'name' => 'sr_sl',
                        'id' => 'sr_sl',
                        'lang' => FALSE,
                    ),

                ),



            ),


        );



        $fields_form2 = array(
            'form'=> array(

                'legend' => array(
                    'title' => $title2,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(



                    array(
                        'type' => 'switch',
                        'label' => $sr_sliderh,
                        'name' => 'sr_sliderh',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'text',
                        'label' => $sr_slh,
                        'name' => 'sr_slh',
                        'id' => 'sr_slh',
                        'lang' => FALSE,
                    ),

                ),



            ),


        );



        $fields_form10 = array(
            'form' => array(


                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );




        $helper = new HelperForm();



        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'owlcarouselsshopsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesOwlcarouselsshopSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );



        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_owlcarouselsshop_js.phtml');
        $_html = ob_get_clean();



        return  $_html.$helper->generateForm(array($fields_form,$fields_form1,$fields_form2, $fields_form10));
    }

    public function getConfigFieldsValuesOwlcarouselsshopSettings(){

        $data_config = array(

            $this->_name.'BGCOLOR_T'=>Configuration::get($this->_name.'BGCOLOR_T'),
            $this->_name.'BGCOLOR_TIT'=>Configuration::get($this->_name.'BGCOLOR_TIT'),

            'tlast_l'=> Configuration::get($this->_name.'tlast_l'),
            'tlast_ls'=> Configuration::get($this->_name.'tlast_ls'),
            'tlast_r'=> Configuration::get($this->_name.'tlast_r'),
            'tlast_rs'=> Configuration::get($this->_name.'tlast_rs'),
            'tlast_f'=> Configuration::get($this->_name.'tlast_f'),
            'tlast_h'=> Configuration::get($this->_name.'tlast_h'),


            't_left'=>Configuration::get($this->_name.'t_left'),
            't_right'=>Configuration::get($this->_name.'t_right'),
            't_footer'=>Configuration::get($this->_name.'t_footer'),
            't_home'=>Configuration::get($this->_name.'t_home'),
            't_leftside'=>Configuration::get($this->_name.'t_leftside'),
            't_rightside'=>Configuration::get($this->_name.'t_rightside'),

            't_lefts'=>Configuration::get($this->_name.'t_lefts'),
            't_rights'=>Configuration::get($this->_name.'t_rights'),
            't_footers'=>Configuration::get($this->_name.'t_footers'),
            't_homes'=>Configuration::get($this->_name.'t_homes'),
            't_leftsides'=>Configuration::get($this->_name.'t_leftsides'),
            't_rightsides'=>Configuration::get($this->_name.'t_rightsides'),
            't_tpages'=>Configuration::get($this->_name.'t_tpages'),


            'sr_slider'=> (int)Configuration::get($this->_name.'sr_slider'),
            'sr_sl'=> (int)Configuration::get($this->_name.'sr_sl'),

            'sr_sliderh'=> (int)Configuration::get($this->_name.'sr_sliderh'),
            'sr_slh'=> (int)Configuration::get($this->_name.'sr_slh'),
        );

        return $data_config;

    }



    public function woweffectsshop($data){

        $_prefix_shop_reviews = $data['prefix'];

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $d_eff_shop_val = $data['d_eff_shop_val'];
        $d_eff_shop_my_val = $data['d_eff_shop_my_val'];
        $d_eff_shop_u_val = $data['d_eff_shop_u_val'];

        $update_title = $data['update_title'];
        $title = $data['title'];
        $d_eff_shop_title = $data['d_eff_shop_title'];
        $d_eff_shop_my_title = $data['d_eff_shop_my_title'];
        $d_eff_shop_u_title = $data['d_eff_shop_u_title'];

        $fields_form = array(
            'form'=> array(

                'legend' => array(
                    'title' => $title,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(


                    array(
                        'type' => 'wow_display_effect',
                        'label' => $d_eff_shop_title,
                        'name' => 'd_eff_shop'.$_prefix_shop_reviews,
                        'id' => 'd_eff_shop'.$_prefix_shop_reviews,
                        'lang' => FALSE,
                        'value' => $d_eff_shop_val
                    ),


                    array(
                        'type' => 'wow_display_effect',
                        'label' => $d_eff_shop_my_title,
                        'name' => 'd_eff_shop_my'.$_prefix_shop_reviews,
                        'id' => 'd_eff_shop_my'.$_prefix_shop_reviews,
                        'lang' => FALSE,
                        'value' => $d_eff_shop_my_val
                    ),

                    array(
                        'type' => 'wow_display_effect',
                        'label' => $d_eff_shop_u_title,
                        'name' => 'd_eff_shop_u'.$_prefix_shop_reviews,
                        'id' => 'd_eff_shop_u'.$_prefix_shop_reviews,
                        'lang' => FALSE,
                        'value' => $d_eff_shop_u_val
                    ),

                ),



            ),


        );

        $fields_form1 = array(
            'form' => array(


                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );




        $helper = new HelperForm();



        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'woweffectsshopsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesWoweffectsshopSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );




        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesWoweffectsshopSettings(){

        $data_config = array(

        );

        return $data_config;

    }


    public function voucherwhenaddreviewshopsettings16($data){


        $_prefix_shop_reviews = $data['prefix'];

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];


        $title_translate = $data['title'];
        $vis_on_title = $data['vis_on_title'];
        $vis_on_desc = $data['vis_on_desc'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $ids_groups_title = $data['ids_groups_title'];
        $coupondesc_title = $data['coupondesc_title'];
        $coupondesc_desc = $data['coupondesc_desc'];
        $vouchercode_title = $data['vouchercode_title'];
        $vouchercode_desc = $data['vouchercode_desc'];
        $discount_type_title = $data['discount_type_title'];
        $discount_type1 = $data['discount_type1'];
        $discount_type2 = $data['discount_type2'];
        $tax_title = $data['tax_title'];
        $tax1 = $data['tax1'];
        $tax2 = $data['tax2'];
        $percentage_val = $data['percentage_val'];
        $isminamount = $data['isminamount'];
        $is_show_min_title = $data['is_show_min_title'];
        $is_show_min_desc = $data['is_show_min_desc'];
        $select_cat_title = $data['select_cat_title'];
        $select_cat_desc = $data['select_cat_desc'];
        $sdvvalid_title = $data['sdvvalid_title'];
        $sdvvalid_desc = $data['sdvvalid_desc'];
        $highlight_title = $data['highlight_title'];
        $highlight_desc = $data['highlight_desc'];
        $cumulativeother_title = $data['cumulativeother_title'];
        $cumulativereduc_title = $data['cumulativereduc_title'];
        $save_title = $data['save_title'];

        $selected_categories = $data['selected_categories'];


        $cookie = $this->context->cookie;
        $curs = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);

        $discount_type_currency_value = array();
        $min_checkout_value = array();

        foreach ($curs AS $_cur){

            $discount_type_currency_value[$_cur['id_currency']] = array('amount' => Tools::getValue('sdamount'.$_prefix_shop_reviews.'['.(int)($_cur['id_currency']).']', Configuration::get('sdamount'.$_prefix_shop_reviews.'_'.(int)($_cur['id_currency']))),
                'currency'=>$_cur['sign'],
                'name'=>htmlentities($_cur['name'], ENT_NOQUOTES, 'utf-8'),
                'name_item' => 'sdamount'.$_prefix_shop_reviews
            );

            $min_checkout_value[$_cur['id_currency']]  = array('amount' => Tools::getValue('sdminamount'.$_prefix_shop_reviews.'['.(int)($_cur['id_currency']).']', Configuration::get('sdminamount'.$_prefix_shop_reviews.'_'.(int)($_cur['id_currency']))),
                'currency'=>$_cur['sign'],
                'name'=>htmlentities($_cur['name'], ENT_NOQUOTES, 'utf-8'),
                'name_item' => 'sdminamount'.$_prefix_shop_reviews
            );
        }

        $id_lang =  $cookie->id_lang;



        $ids_groups = Configuration::get($this->_name.'ids_groups'.$_prefix_shop_reviews);
        $ids_groups = $ids_groups?explode(",",$ids_groups):array();


        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $title_translate,
                    'icon' => 'fa fa-reviews fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $vis_on_title,
                        'name' => 'vis_on'.$_prefix_shop_reviews,
                        'desc' => '<b style="color:red">'.$vis_on_desc.'</b>',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'group_association',
                        'label' => $ids_groups_title,
                        'name' => 'ids_groups'.$_prefix_shop_reviews,
                        'values'=>Group::getGroups($id_lang),
                        'selected_data'=>$ids_groups,
                        'required' => TRUE,
                        'desc' => $ids_groups_title,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $coupondesc_title,
                        'name' => 'coupondesc'.$_prefix_shop_reviews,
                        'lang' => true,
                        'hint' => $coupondesc_desc,
                        'desc' => $coupondesc_desc
                    ),

                    array(
                        'type' => 'text',
                        'label' => $vouchercode_title,
                        'name' => 'vouchercode'.$_prefix_shop_reviews,
                        'desc' => $vouchercode_desc,
                    ),

                    array(
                        'type' => 'select',
                        'label' => $discount_type_title,
                        'name' => 'discount_type'.$_prefix_shop_reviews,
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '1',
                                    'name' => $discount_type1
                                ),

                                array(
                                    'id' => '2',
                                    'name' => $discount_type2,
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'cms_pages',
                        'label' => '',
                        'name' => 'currency_val'.$_prefix_shop_reviews,
                        'values'=> $discount_type_currency_value,

                    ),

                    array(
                        'type' => 'select',
                        'label' => $tax_title,
                        'name' => 'tax'.$_prefix_shop_reviews,
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '0',
                                    'name' =>$tax1
                                ),

                                array(
                                    'id' => '1',
                                    'name' => $tax2,
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),

                    array(
                        'type' => 'text_custom',
                        'label' => $percentage_val,
                        'name' => 'percentage_val'.$_prefix_shop_reviews,
                        'value'=> Configuration::get($this->_name.'percentage_val'.$_prefix_shop_reviews),
                    ),

                    array(
                        'type' => 'checkbox_custom',
                        'label' => $isminamount,
                        'name' => $this->_name.'isminamount'.$_prefix_shop_reviews,
                        'hint' => $isminamount,
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => $this->_name.'isminamount'.$_prefix_shop_reviews,
                                    'name' => '',
                                    'val' => 1
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_show_min_title,
                        'name' => 'is_show_min'.$_prefix_shop_reviews,
                        'desc' => $is_show_min_desc,
                        'hint' => $is_show_min_desc,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'cms_pages',
                        'label' => '',
                        'name' => 'mincheckout_val'.$_prefix_shop_reviews,
                        'values'=> $min_checkout_value,

                    ),

                    array(
                        'type' => 'cms_categories',
                        'label' => $select_cat_title,
                        'hint' => $select_cat_desc,
                        'desc' => $select_cat_desc,
                        'name' => 'select_cat'.$_prefix_shop_reviews,
                        'values'=> $selected_categories,

                    ),

                    array(
                        'type' => 'text_validity',
                        'label' => $sdvvalid_title,
                        'name' => 'sdvvalid'.$_prefix_shop_reviews,
                        'value'=> Configuration::get($this->_name.'sdvvalid'.$_prefix_shop_reviews),
                        'desc' =>$sdvvalid_desc,
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $highlight_title,
                        'name' => 'highlight'.$_prefix_shop_reviews,
                        'desc'=>$highlight_desc,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $cumulativeother_title,
                        'name' => 'cumulativeother'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),

                    array(
                        'type' => 'switch',
                        'label' => $cumulativereduc_title,
                        'name' => 'cumulativereduc'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),

                'submit' => array(
                    'title' => $save_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'vouchersettings'.$_prefix_shop_reviews;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesCouponShopSettings(array('prefix'=>$_prefix_shop_reviews)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        $currency_discount_amount = (int)Configuration::get($this->_name.'discount_type'.$_prefix_shop_reviews);

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_voucherwhenaddreviewshopsettings16_js.phtml');
        $_html = ob_get_clean();

        return $_html . $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValuesCouponShopSettings($data){

        $_prefix_shop_reviews = $data['prefix'];

        $languages = Language::getLanguages(false);
        $fields_fcoupondesc = array();

        foreach ($languages as $lang)
        {
            $fields_fcoupondesc[$lang['id_lang']] = Configuration::get($this->_name.'coupondesc'.$_prefix_shop_reviews.'_'.$lang['id_lang']);
        }

        $data_config = array(
            'vis_on'.$_prefix_shop_reviews => Configuration::get($this->_name.'vis_on'.$_prefix_shop_reviews),
            'vouchercode'.$_prefix_shop_reviews => Configuration::get($this->_name.'vouchercode'.$_prefix_shop_reviews),
            'discount_type'.$_prefix_shop_reviews => Configuration::get($this->_name.'discount_type'.$_prefix_shop_reviews),

            'coupondesc'.$_prefix_shop_reviews => $fields_fcoupondesc,

            'tax'.$_prefix_shop_reviews =>  Configuration::get($this->_name.'tax'.$_prefix_shop_reviews),

            $this->_name.'isminamount'.$_prefix_shop_reviews =>  Configuration::get($this->_name.'isminamount'.$_prefix_shop_reviews),

            'is_show_min'.$_prefix_shop_reviews => Configuration::get($this->_name.'is_show_min'.$_prefix_shop_reviews),

            'cumulativeother'.$_prefix_shop_reviews => Configuration::get($this->_name.'cumulativeother'.$_prefix_shop_reviews),

            'cumulativereduc'.$_prefix_shop_reviews =>  Configuration::get($this->_name.'cumulativereduc'.$_prefix_shop_reviews),
            'highlight'.$_prefix_shop_reviews =>  Configuration::get($this->_name.'highlight'.$_prefix_shop_reviews),
        );
        return $data_config;
    }


    public function reviewsemailsshop($data){

        $_prefix_shop_reviews = $data['prefix'];

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $title = $data['title'];
        $mail_title = $data['mail_title'];
        $noti_title = $data['noti_title'];

        $update_title = $data['update_title'];

        $fields_form = array(
            'form'=> array(

                'legend' => array(
                    'title' => $title,
                    'icon' => 'fa fa-envelope-o fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'text',
                        'label' => $mail_title,
                        'name' => 'mail'.$_prefix_shop_reviews,
                        'id' => 'mail'.$_prefix_shop_reviews,
                        'lang' => FALSE,

                    ),
                    array(
                        'type' => 'checkbox_custom_store',
                        'label' => $noti_title,
                        'name' => 'noti'.$_prefix_shop_reviews,
                        'values' => array(
                            'value' => (int)Configuration::get($this->_name.'noti'.$_prefix_shop_reviews)
                        ),
                    ),
                ),

            ),
        );

        $fields_form1 = array(
            'form' => array(


                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'reviewsemailssettingsshop';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesReviewsemailsShopSettings(array('prefix'=>$_prefix_shop_reviews)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesReviewsemailsShopSettings($data){
        $_prefix_shop_reviews = $data['prefix'];
        $data_config = array(
            'mail'.$_prefix_shop_reviews=>Configuration::get($this->_name.'mail'.$_prefix_shop_reviews),
        );

        return $data_config;
    }



    public function shopcustomerremindersettings16($data){

        include_once(_PS_MODULE_DIR_ . $this->_name.'/classes/featureshelptestim.class.php');
        $obj_featureshelp = new featureshelptestim();
        $cookie = $this->context->cookie;
        $id_lang = (int)($cookie->id_lang);

        require_once(_PS_MODULE_DIR_ . $this->_name . '/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        include_once(_PS_MODULE_DIR_ . $this->_name.'/spmgsnipreview.php');
        $obj_spmgsnipreview = new spmgsnipreview();


        $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs();
        $ajax_url = $data_seo_url['ajax_url'];

        $delimeter_rewrite = "&";
        if(Configuration::get('PS_REWRITING_SETTINGS')){
            $delimeter_rewrite = "?";
        }
        $url_cron = $data_seo_url['cron_shop_reviews_url'];



        $_prefix_shop_reviews = $data['prefix'];

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $update_title = $data['update_title'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $title = $data['title'];
        $reminder_title = $data['reminder_title'];
        $reminder_desc1 = $data['reminder_desc1'];
        $reminder_desc2 = $data['reminder_desc2'];
        $reminder_desc3 = $data['reminder_desc3'];
        $reminder_desc4 = $data['reminder_desc4'];
        $reminder_desc5 = $data['reminder_desc5'];
        $crondelay_title = $data['crondelay_title'];
        $crondelay_desc = $data['crondelay_desc'];
        $cronnpost_title = $data['cronnpost_title'];
        $cronnpost_desc = $data['cronnpost_desc'];
        $delay_title = $data['delay_title'];
        $delay_desc = $data['delay_desc'];
        $orders_import_storereviews_title = $data['orders_import_storereviews_title'];
        $orders_import_storereviews_desc = $data['orders_import_storereviews_desc'];
        $sel_statuses_title = $data['sel_statuses_title'];
        $sel_statuses_desc = $data['sel_statuses_desc'];
        $remrevsec_title = $data['remrevsec_title'];
        $remindersec_title = $data['remindersec_title'];
        $remindersec_desc = $data['remindersec_desc'];
        $delaysec_title = $data['delaysec_title'];


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/shopcustomerremindersettings16_desc1.phtml');
        $desc1 = ob_get_clean();

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title,
                    'icon' => 'fa fa-bell-o fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $reminder_title,
                        'name' => 'reminder'.$_prefix_shop_reviews,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $reminder_desc1.$desc1,
                    ),
                    array(
                        'type' => 'text_custom_delay',
                        'label' => $crondelay_title,
                        'name' => 'crondelay'.$_prefix_shop_reviews,
                        'value'=>(int)Configuration::get($this->_name.'crondelay'.$_prefix_shop_reviews),
                        'desc' => $crondelay_desc
                    ),
                    array(
                        'type' => 'text',
                        'label' => $cronnpost_title,
                        'name' => 'cronnpost'.$_prefix_shop_reviews,
                        'desc' => $cronnpost_desc
                    ),
                    array(
                        'type' => 'text_custom_delay_reminder',
                        'label' =>$delay_title,
                        'name' => 'delay'.$_prefix_shop_reviews,
                        'value'=> Configuration::get($this->_name.'delay'.$_prefix_shop_reviews),
                        'desc'=>$delay_desc
                    ),
                    array(
                        'type' => 'text_custom_orders_import_storereviews',
                        'label' =>$orders_import_storereviews_title,
                        'name' => 'orders_import_storereviews',
                        'end_date' =>date('Y-m-d H:i:s'),
                        'host_url'=>$obj_spmgsnipreview->getURLMultiShop(),
                        'ajax_url'=>$ajax_url,
                        'desc'=>$orders_import_storereviews_desc
                    ),
                    array(
                        'type' => 'text_custom_order_statuses',
                        'label' =>$sel_statuses_title,
                        'name' => 'sel_statuses'.$_prefix_shop_reviews,
                        'value'=> $obj_featureshelp->getOrderStatuses(array('id_lang'=>$id_lang)),
                        'orderstatuses'=> explode(",",Configuration::get($this->_name.'orderstatuses'.$_prefix_shop_reviews)),
                        'desc'=>$sel_statuses_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $remrevsec_title,
                        'name' => 'remrevsec'.$_prefix_shop_reviews,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $remindersec_title,
                        'name' => 'remindersec'.$_prefix_shop_reviews,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $remindersec_desc
                    ),
                    array(
                        'type' => 'text_custom_delay_reminder',
                        'label' => $delaysec_title,
                        'name' => 'delaysec'.$_prefix_shop_reviews,
                        'value'=> Configuration::get($this->_name.'delaysec'.$_prefix_shop_reviews),
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'shopcustomerremindersettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesCustomerreminderShopSettings(array('prefix'=>$_prefix_shop_reviews)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_shopcustomerremindersettings16_js.phtml');
        $_html = ob_get_clean();

        return  $_html.$helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesCustomerreminderShopSettings($data){

        $_prefix_shop_reviews = $data['prefix'];
        $data_config = array(
            'reminder'.$_prefix_shop_reviews => Configuration::get($this->_name.'reminder'.$_prefix_shop_reviews),
            'remindersec'.$_prefix_shop_reviews => Configuration::get($this->_name.'remindersec'.$_prefix_shop_reviews),
            'remrevsec'.$_prefix_shop_reviews => Configuration::get($this->_name.'remrevsec'.$_prefix_shop_reviews),
            'cronnpost'.$_prefix_shop_reviews=>(int)Configuration::get($this->_name.'cronnpost'.$_prefix_shop_reviews),
        );

        return $data_config;
    }


    private function _emailsubjects16_desc($data){

        $desc = isset($data['desc'])?$data['desc']:'';
        $name_template = isset($data['name'])?$data['name']:'';

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/'.__FUNCTION__.'.phtml');
        $_html = ob_get_clean();


        return $_html;
    }

    public function emailsubjects16($data){

        $_prefix_shop_reviews = $data['prefix'];

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $update_title = $data['update_title'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $title = $data['title'];
        $is_emrem = $data['is_emrem'];
        $em_desc1 = $data['em_desc1'];
        $em_desc2 = $data['em_desc2'];
        $em_desc3 = $data['em_desc3'];
        $em_desc4 = $data['em_desc4'];
        $em_desc5 = $data['em_desc5'];
        $emrem = $data['emrem'];
        $is_reminderok = $data['is_reminderok'];
        $reminderok = $data['reminderok'];
        $is_thankyou = $data['is_thankyou'];
        $thankyou = $data['thankyou'];
        $is_newtest= $data['is_newtest'];
        $newtest = $data['newtest'];
        $is_resptest = $data['is_resptest'];
        $resptest = $data['resptest'];
        $is_revvoucr = $data['is_revvoucr'];
        $revvoucr = $data['revvoucr'];

        $fields_form = array(
            'form'=> array(
                //'tinymce' => FALSE,
                'legend' => array(
                    'title' => $title,
                    'icon' => 'fa fa-envelope-o fa-lg'
                ),
                'input' => array(

                    array(
                        'type' => 'switch',
                        'label' => $is_emrem,
                        'name' => 'is_emrem'.$_prefix_shop_reviews,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),

                        'desc' => $is_emrem,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $emrem,
                        'name' => 'emrem'.$_prefix_shop_reviews,
                        'id' => 'emrem'.$_prefix_shop_reviews,
                        'lang' => TRUE,
                        'desc' => $em_desc1.' '
                            . $em_desc2
                            .'  "mails" '
                            . $em_desc3
                            .' "'.$this->_name.'" '
                            .$em_desc4
                            .$this->_emailsubjects16_desc(array('name'=>'customer-reminder-ti','desc'=>$em_desc5))

                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_reminderok,
                        'name' => 'is_reminderok'.$_prefix_shop_reviews,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_reminderok,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $reminderok,
                        'name' => 'reminderok'.$_prefix_shop_reviews,
                        'id' => 'reminderok'.$_prefix_shop_reviews,
                        'lang' => TRUE,
                        'desc' => $em_desc1.' '
                            . $em_desc2
                            .'  "mails" '
                            . $em_desc3
                            .' "'.$this->_name.'" '
                            .$em_desc4
                            .$this->_emailsubjects16_desc(array('name'=>'customer-reminder-admin-ti','desc'=>$em_desc5))

                    ),

                    array(
                        'type' => 'switch',
                        'label' => $is_thankyou,
                        'name' => 'is_thankyou'.$_prefix_shop_reviews,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_thankyou,
                    ),

                    array(
                        'type' => 'text',
                        'label' => $thankyou,
                        'name' => 'thankyou'.$_prefix_shop_reviews,
                        'id' => 'thankyou'.$_prefix_shop_reviews,
                        'lang' => TRUE,
                        'desc' => $em_desc1.' '
                            . $em_desc2
                            .'  "mails" '
                            . $em_desc3
                            .' "'.$this->_name.'" '
                            .$em_desc4
                            .$this->_emailsubjects16_desc(array('name'=>'testimony-thank-you','desc'=>$em_desc5))

                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_newtest,
                        'name' => 'is_newtest'.$_prefix_shop_reviews,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_newtest,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $newtest,
                        'name' => 'newtest'.$_prefix_shop_reviews,
                        'id' => 'newtest'.$_prefix_shop_reviews,
                        'lang' => TRUE,
                        'desc' => $em_desc1.' '
                            . $em_desc2
                            .'  "mails" '
                            . $em_desc3
                            .' "'.$this->_name.'" '
                            .$em_desc4
                            .$this->_emailsubjects16_desc(array('name'=>'testimony','desc'=>$em_desc5))

                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_resptest,
                        'name' => 'is_resptest'.$_prefix_shop_reviews,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_resptest,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $resptest,
                        'name' => 'resptest'.$_prefix_shop_reviews,
                        'id' => 'resptest'.$_prefix_shop_reviews,
                        'lang' => TRUE,
                        'desc' => $em_desc1.' '
                            . $em_desc2
                            .'  "mails" '
                            . $em_desc3
                            .' "'.$this->_name.'" '
                            .$em_desc4
                            .$this->_emailsubjects16_desc(array('name'=>'response-testim','desc'=>$em_desc5))

                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_revvoucr,
                        'name' => 'is_revvoucr'.$_prefix_shop_reviews,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_revvoucr,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $revvoucr,
                        'name' => 'revvoucr'.$_prefix_shop_reviews,
                        'id' => 'revvoucr'.$_prefix_shop_reviews,
                        'lang' => TRUE,
                        'desc' => $em_desc1.' '
                            . $em_desc2
                            .'  "mails" '
                            . $em_desc3
                            .' "'.$this->_name.'" '
                            .$em_desc4
                            .$this->_emailsubjects16_desc(array('name'=>'voucherserg-testim','desc'=>$em_desc5))

                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'emailsubjectssettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesEmailSubjectsSettings(array('prefix'=>$_prefix_shop_reviews)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesEmailSubjectsSettings($data){

        $_prefix_shop_reviews = $data['prefix'];

        $languages = Language::getLanguages(false);
        $fields_emrem = array();
        $fields_thankyou = array();
        $fields_reminderok = array();
        $fields_newtest = array();
        $fields_resptest = array();
        $fields_revvoucr = array();

        foreach ($languages as $lang)
        {
            $fields_emrem[$lang['id_lang']] =  Configuration::get($this->_name.'emrem'.$_prefix_shop_reviews.'_'.$lang['id_lang']);
            $fields_thankyou[$lang['id_lang']] =  Configuration::get($this->_name.'thankyou'.$_prefix_shop_reviews.'_'.$lang['id_lang']);
            $fields_reminderok[$lang['id_lang']] =  Configuration::get($this->_name.'reminderok'.$_prefix_shop_reviews.'_'.$lang['id_lang']);
            $fields_newtest[$lang['id_lang']] =  Configuration::get($this->_name.'newtest'.$_prefix_shop_reviews.'_'.$lang['id_lang']);
            $fields_resptest[$lang['id_lang']] =  Configuration::get($this->_name.'resptest'.$_prefix_shop_reviews.'_'.$lang['id_lang']);
            $fields_revvoucr[$lang['id_lang']] =  Configuration::get($this->_name.'revvoucr'.$_prefix_shop_reviews.'_'.$lang['id_lang']);
        }

        $data_config = array(
            'emrem'.$_prefix_shop_reviews => $fields_emrem,
            'thankyou'.$_prefix_shop_reviews => $fields_thankyou,
            'reminderok'.$_prefix_shop_reviews => $fields_reminderok,
            'newtest'.$_prefix_shop_reviews => $fields_newtest,
            'resptest'.$_prefix_shop_reviews => $fields_resptest,
            'revvoucr'.$_prefix_shop_reviews => $fields_revvoucr,

            'is_emrem'.$_prefix_shop_reviews => Configuration::get($this->_name.'is_emrem'.$_prefix_shop_reviews),
            'is_reminderok'.$_prefix_shop_reviews => Configuration::get($this->_name.'is_reminderok'.$_prefix_shop_reviews),
            'is_thankyou'.$_prefix_shop_reviews => Configuration::get($this->_name.'is_thankyou'.$_prefix_shop_reviews),
            'is_newtest'.$_prefix_shop_reviews => Configuration::get($this->_name.'is_newtest'.$_prefix_shop_reviews),
            'is_resptest'.$_prefix_shop_reviews => Configuration::get($this->_name.'is_resptest'.$_prefix_shop_reviews),
            'is_revvoucr'.$_prefix_shop_reviews => Configuration::get($this->_name.'is_revvoucr'.$_prefix_shop_reviews),
        );
        return $data_config;
    }


    public function installFiles2ShopReviewTable(){

        $prefix_shop_reviews = $this->getPrefix();


        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_files2review_' . $prefix_shop_reviews . ' TO '._DB_PREFIX_.'spmgsnipreview_files2review_' . $prefix_shop_reviews . '';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_files2review_' . $prefix_shop_reviews . '`');
            if (is_array($list_fields))
            {
                foreach ($list_fields as $k => $field)
                    $list_fields[$k] = $field['Field'];
                // change filed with same name , not !in_array(), use in_array
                if (in_array('id_gsnipreview_files2review', $list_fields)) {
                    if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_files2review_' . $prefix_shop_reviews . '` CHANGE `id_gsnipreview_files2review` `id_spmgsnipreview_files2review` int(10) unsigned NOT NULL auto_increment;')) {
                        return false;
                    }

                }
            }


            $sql = 'SELECT * FROM `'._DB_PREFIX_.'spmgsnipreview_files2review_' . $prefix_shop_reviews . '`';
            $items = Db::getInstance()->ExecuteS($sql);

            foreach($items as $_item){

                $id_spmgsnipreview_files2review = $_item['id_spmgsnipreview_files2review'];
                $full_path = str_replace("gsnipreview","spmgsnipreview",$_item['full_path']);
                $sql = 'UPDATE `'._DB_PREFIX_.'spmgsnipreview_files2review_' . $prefix_shop_reviews . '` SET
					     `full_path`="'.pSQL($full_path).'"
		                WHERE id_spmgsnipreview_files2review = '.(int)($id_spmgsnipreview_files2review).'';
                Db::getInstance()->Execute($sql);
            }


        } else {


            $db = Db::getInstance();
            $query = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_files2review_' . $prefix_shop_reviews . '` (
                              `id_spmgsnipreview_files2review` int(10) unsigned NOT NULL auto_increment,
							  `id_review` int(11) NOT NULL,
							  `full_path` text,
							 PRIMARY KEY (`id_spmgsnipreview_files2review`)
							) ENGINE=' . (defined('_MYSQL_ENGINE_') ? _MYSQL_ENGINE_ : "MyISAM") . ' DEFAULT CHARSET=utf8';
            $db->Execute($query);
        }
        return true;

    }

    public function installReminder2CustomerShopReviewTable(){
        $prefix_shop_reviews = $this->getPrefix();


        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_reminder2customer_' . $prefix_shop_reviews . ' TO '._DB_PREFIX_.'spmgsnipreview_reminder2customer_' . $prefix_shop_reviews . '';
            if (!Db::getInstance()->Execute($sql))
                return false;


        } else {

            $db = Db::getInstance();

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_reminder2customer_' . $prefix_shop_reviews . '` (
						  `id_customer` int(10) unsigned NOT NULL,
                          `id_shop` int(10) NOT NULL DEFAULT \'0\',
                          `status` INT(11) NOT NULL DEFAULT \'0\',
                           KEY `id_c2id_shop` (`id_customer`, `id_shop`),
                          KEY `id_customer` (`id_shop`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);
        }
        return true;
    }

    public function createShopReviewTable()
    {

        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_storereviews TO '._DB_PREFIX_. $this->_name . '_storereviews';
            if (!Db::getInstance()->Execute($sql))
                return false;


            $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_storereviews`');
            if (is_array($list_fields))
            {
                foreach ($list_fields as $k => $field)
                    $list_fields[$k] = $field['Field'];
                if (!in_array('is_new', $list_fields)) {
                    if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_storereviews` ADD `is_new` int(11) NOT NULL default \'0\'')) {
                        return false;
                    }

                }
            }


        } else {
            $db = Db::getInstance();

            $query = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . $this->_name . '_storereviews` (
							  `id` int(11) NOT NULL auto_increment,
							  `name` varchar(500) NOT NULL,
							  `email` varchar(500) NOT NULL,
							  `id_customer` int(11) NOT NULL default \'0\',
							  `avatar` text,
							  `web` varchar(500) default NULL,
							  `company` varchar(500) default NULL,
							  `address` varchar(500) default NULL,
							  `message` text NOT NULL,
							  `response` text,
							  `is_show` int(11) NOT NULL default \'0\',
							  `rating` int(11) NOT NULL,
							  `country` varchar(500) default NULL,
							  `city` varchar(500) default NULL,
							  `id_shop` int(11) NOT NULL default \'0\',
							  `id_lang` int(11) NOT NULL default \'0\',
							  `active` int(11) NOT NULL default \'0\',
							  `is_deleted` int(11) NOT NULL default \'0\',
							  `date_add` timestamp NOT NULL default CURRENT_TIMESTAMP,
							  `is_new` int(11) NOT NULL default \'0\',
							  PRIMARY KEY  (`id`)
							) ENGINE=' . (defined('_MYSQL_ENGINE_') ? _MYSQL_ENGINE_ : "MyISAM") . ' DEFAULT CHARSET=utf8;';
            $db->Execute($query);
        }
        return true;
    }

    public function createReminderShopReviewsTable(){
        $prefix_shop_reviews = $this->getPrefix();

        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.  'gsnipreview_data_order_' . $prefix_shop_reviews.' TO '._DB_PREFIX_.  $this->_name . '_data_order_' . $prefix_shop_reviews ;
            if (!Db::getInstance()->Execute($sql))
                return false;

            $sql = 'RENAME TABLE '._DB_PREFIX_. 'gsnipreview_customer_' . $prefix_shop_reviews.' TO '._DB_PREFIX_.  $this->_name . '_customer_' . $prefix_shop_reviews ;
            if (!Db::getInstance()->Execute($sql))
                return false;


        } else {


            $db = Db::getInstance();
            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . '' . $this->_name . '_data_order_' . $prefix_shop_reviews . '` (
					  `id` int(10) NOT NULL AUTO_INCREMENT,
					  `id_shop` int(11) NOT NULL default \'0\',
					  `order_id` int(10) NOT NULL,
					  `date_add` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
					  `status` int(10) NOT NULL default \'0\',
					  `customer_id` int(11) NOT NULL default \'0\',
					  `data` text,
					  `date_send` timestamp NULL,
					  `date_send_second` timestamp NULL,
                      `count_sent` int(10) NOT NULL default \'0\',
					  PRIMARY KEY (`id`)
					) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';
            $db->Execute($sql);

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . '' . $this->_name . '_customer_' . $prefix_shop_reviews . '` (
						  `id_shop` int(11) NOT NULL default \'0\',
						  `customer_id` int(11) NOT NULL default \'0\',
						  `status` int(10) NOT NULL default \'0\',
						  KEY (`id_shop`,`customer_id`),
						  KEY `shop_status` (`id_shop`,`status`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';
            $db->Execute($sql);
        }
        return true;
    }

    public function installCriteriaTableShopReviews(){
        $prefix_shop_reviews = $this->getPrefix();


        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_review_criterion_' . $prefix_shop_reviews . ' TO '._DB_PREFIX_.'spmgsnipreview_review_criterion_' . $prefix_shop_reviews . '';
            if (!Db::getInstance()->Execute($sql))
                return false;



            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_review_criterion_lang_' . $prefix_shop_reviews . ' TO '._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_' . $prefix_shop_reviews . '';
            if (!Db::getInstance()->Execute($sql))
                return false;


            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_review2criterion_' . $prefix_shop_reviews . ' TO '._DB_PREFIX_.'spmgsnipreview_review2criterion_' . $prefix_shop_reviews . '';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_' . $prefix_shop_reviews . '`');
            if (is_array($list_fields))
            {
                foreach ($list_fields as $k => $field)
                    $list_fields[$k] = $field['Field'];
                // change filed with same name , not !in_array(), use in_array
                if (in_array('id_gsnipreview_review_criterion', $list_fields)) {
                    if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion_' . $prefix_shop_reviews . '` CHANGE `id_gsnipreview_review_criterion` `id_spmgsnipreview_review_criterion` int(10) unsigned NOT NULL auto_increment;')) {
                        return false;
                    }

                }
            }


            $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_' . $prefix_shop_reviews . '`');
            if (is_array($list_fields))
            {
                foreach ($list_fields as $k => $field)
                    $list_fields[$k] = $field['Field'];
                // change filed with same name , not !in_array(), use in_array
                if (in_array('id_gsnipreview_review_criterion', $list_fields)) {
                    if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion_lang_' . $prefix_shop_reviews . '` CHANGE `id_gsnipreview_review_criterion` `id_spmgsnipreview_review_criterion` INT(11) UNSIGNED NOT NULL;')) {
                        return false;
                    }

                }
            }


        } else {

            $db = Db::getInstance();
            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion_' . $prefix_shop_reviews . '` (
						  `id_spmgsnipreview_review_criterion` int(10) unsigned NOT NULL auto_increment,
						  `id_shop` varchar(1024) NOT NULL default \'0\',
                          `active` tinyint(1) NOT NULL,
                          PRIMARY KEY (`id_spmgsnipreview_review_criterion`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion_lang_' . $prefix_shop_reviews . '` (
						  `id_spmgsnipreview_review_criterion` INT(11) UNSIGNED NOT NULL ,
                          `id_lang` INT(11) UNSIGNED NOT NULL ,
                          `name` VARCHAR(255) NOT NULL ,
                          `description` text,
                          KEY ( `id_spmgsnipreview_review_criterion` , `id_lang` )
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);


            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_review2criterion_' . $prefix_shop_reviews . '` (
						  `id_review` int(10) unsigned NOT NULL,
                          `id_criterion` int(10) unsigned NOT NULL,
                          `rating` INT(11) NOT NULL DEFAULT \'0\',
                           KEY(`id_review`, `id_criterion`),
                          KEY `id_criterion` (`id_criterion`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);
        }

        return true;
    }


    public function uninstallTable() {

        $prefix_shop_reviews = $this->getPrefix();

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.$this->_name.'_storereviews');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.$this->_name.'_data_order_'.$prefix_shop_reviews);
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.$this->_name.'_customer_'.$prefix_shop_reviews);

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_files2review_'.$prefix_shop_reviews);

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_reminder2customer_'.$prefix_shop_reviews);


        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_review_criterion_'.$prefix_shop_reviews);
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_review_criterion_lang_'.$prefix_shop_reviews);
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_review2criterion_'.$prefix_shop_reviews);


    }


    public function setPositionsforStoreWidget(){
        $smarty = $this->context->smarty;
        $smarty->assign($this->_name.'st_left', Configuration::get($this->_name.'st_left'));
        $smarty->assign($this->_name.'st_right', Configuration::get($this->_name.'st_right'));
        $smarty->assign($this->_name.'st_footer', Configuration::get($this->_name.'st_footer'));
        $smarty->assign($this->_name.'st_home', Configuration::get($this->_name.'st_home'));
        $smarty->assign($this->_name.'st_leftside', Configuration::get($this->_name.'st_leftside'));
        $smarty->assign($this->_name.'st_rightside', Configuration::get($this->_name.'st_rightside'));

        $smarty->assign($this->_name.'mt_left', Configuration::get($this->_name.'mt_left'));
        $smarty->assign($this->_name.'mt_right', Configuration::get($this->_name.'mt_right'));
        $smarty->assign($this->_name.'mt_footer', Configuration::get($this->_name.'mt_footer'));
        $smarty->assign($this->_name.'mt_home', Configuration::get($this->_name.'mt_home'));
        $smarty->assign($this->_name.'mt_leftside', Configuration::get($this->_name.'mt_leftside'));
        $smarty->assign($this->_name.'mt_rightside', Configuration::get($this->_name.'mt_rightside'));
    }



    public function deleteGDPRCustomerData($email){

        $data_customer = Customer::getCustomersByEmail($email);
        if(count($data_customer)>0) {
            $id_customer = $data_customer[0]['id_customer'];


            $prefix_shop_reviews = $this->getPrefix();


            // spmgsnipreview_storereviews get data
            $sql = 'SELECT * FROM `'._DB_PREFIX_.$this->_name.'_storereviews`
		        	WHERE  `id_customer` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            $result_spmgsnipreview_storeviews = Db::getInstance()->ExecuteS($sql);
            // spmgsnipreview_storereviews get data




            // spmgsnipreview_storereviews
            $sql = 'DELETE FROM `'._DB_PREFIX_.$this->_name.'_storereviews`
		        	WHERE  `id_customer` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            Db::getInstance()->Execute($sql);

            // spmgsnipreview
            $sql = 'DELETE FROM `'._DB_PREFIX_.$this->_name.'_storereviews`
		        	WHERE  `email` = \''.pSQL($email).'\'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            Db::getInstance()->Execute($sql);


            // spmgsnipreview reminder
            $sql = 'DELETE FROM `'._DB_PREFIX_.''.$this->_name.'_data_order_'.$prefix_shop_reviews.'`
		        	WHERE   `customer_id` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            Db::getInstance()->Execute($sql);


            // spmgsnipreview reminder
            $sql = 'DELETE FROM `'._DB_PREFIX_.''.$this->_name.'_customer_'.$prefix_shop_reviews.'`
		        	WHERE   `customer_id` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            Db::getInstance()->Execute($sql);



            // spmgsnipreview files2review

            foreach($result_spmgsnipreview_storeviews as $review_item) {

                $id_review = $review_item['id'];


                $sql = 'select full_path, id_spmgsnipreview_files2review FROM `' . _DB_PREFIX_ . '' . $this->_name . '_files2review_' . $prefix_shop_reviews . '`
		        	WHERE  `id_review` = ' . (int)$id_review . '
		        	';
                $full_path_data = Db::getInstance()->ExecuteS($sql);
                foreach($full_path_data as $full_path_item) {
                    $full_path = $full_path_item['full_path'];
                    $id_spmgsnipreview_files2review = $full_path_item['id_spmgsnipreview_files2review'];

                    $path = dirname(__FILE__) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . $full_path;
                    @unlink($path);


                    $sql_delete = '
                                    DELETE FROM `' . _DB_PREFIX_ . '' . $this->_name . '_files2review_' . $prefix_shop_reviews . '`
                                    WHERE id_spmgsnipreview_files2review = ' . (int)$id_spmgsnipreview_files2review . '
                                    ';
                    Db::getInstance()->Execute($sql_delete);
                }
            }


            // spmgsnipreview reminder2customer
            $sql = 'DELETE FROM `'._DB_PREFIX_.''.$this->_name.'_reminder2customer_'.$prefix_shop_reviews.'`
		        	WHERE   `id_customer` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            Db::getInstance()->Execute($sql);


        }

        return true;
    }

    public function getGDPRCustomerData($email){

        $data_customer = Customer::getCustomersByEmail($email);
        $customer_data = array();
        if(count($data_customer)>0) {
            $id_customer = $data_customer[0]['id_customer'];


            $prefix_shop_reviews = $this->getPrefix();

            // spmgsnipreview_storereviews
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'spmgsnipreview_storereviews`
		        	WHERE  `id_customer` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            $result_spmgsnipreview = Db::getInstance()->ExecuteS($sql);
            if(count($result_spmgsnipreview)>0)
                $customer_data[$this->_name.'storereviews'] = serialize($result_spmgsnipreview);


            // spmgsnipreview
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'spmgsnipreview_data_order_'.$prefix_shop_reviews.'`
		        	WHERE  `customer_id` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            $result_spmgsnipreview_data_order = Db::getInstance()->ExecuteS($sql);
            if(count($result_spmgsnipreview_data_order)>0)
                $customer_data[$this->_name.'_data_order_reminder'.$prefix_shop_reviews] = serialize($result_spmgsnipreview_data_order);

            // spmgsnipreview
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'spmgsnipreview_customer_'.$prefix_shop_reviews.'`
		        	WHERE  `customer_id` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            $result_spmgsnipreview_customer = Db::getInstance()->ExecuteS($sql);
            if(count($result_spmgsnipreview_customer)>0)
                $customer_data[$this->_name.'_customer_reminder'.$prefix_shop_reviews] = serialize($result_spmgsnipreview_customer);


            // spmgsnipreview_files2review_ti

            // spmgsnipreview_storereviews get data
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'spmgsnipreview_storereviews`
		        	WHERE  `id_customer` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            $result_spmgsnipreview_storeviews = Db::getInstance()->ExecuteS($sql);
            // spmgsnipreview_storereviews get data


            $files2review_data = array();
            foreach($result_spmgsnipreview_storeviews as $review_item) {

                $id_review = $review_item['id'];

                $sql = 'select full_path FROM `' . _DB_PREFIX_ . 'spmgsnipreview_files2review_' . $prefix_shop_reviews . '`
		        	WHERE  `id_review` = ' . (int)$id_review . '
		        	';
                $full_path_data = Db::getInstance()->ExecuteS($sql);
                foreach($full_path_data as $full_path_item) {
                    $full_path = $full_path_item['full_path'];

                    $path = $this->getHttpost() . $full_path;
                    $files2review_data[$id_review][] = $path;
                }



            }


            if(count($files2review_data)>0)
                $customer_data[$this->_name.'_files2review_'.$prefix_shop_reviews] = serialize($files2review_data);
            // spmgsnipreview_files2review_ti




            // spmgsnipreview_reminder2customer_ti
            $sql = 'SELECT * FROM `'._DB_PREFIX_.'spmgsnipreview_reminder2customer_'.$prefix_shop_reviews.'`
		        	WHERE  `id_customer` = '.(int)$id_customer.'
		        	AND `id_shop` = '.(int)$this->getIdShop().'
		        	';
            $result_spmgsnipreview_reminder2customer = Db::getInstance()->ExecuteS($sql);
            if(count($result_spmgsnipreview_reminder2customer)>0)
                $customer_data[$this->_name.'_reminder2customer_'.$prefix_shop_reviews] = serialize($result_spmgsnipreview_reminder2customer);

            // spmgsnipreview_reminder2customer_ti



        }

        return $customer_data;

    }
}