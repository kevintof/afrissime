<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class spmgsnipreviewdraw extends Module{

    private $_name = 'spmgsnipreview';


    public function googlesnippets16($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $blockallinfo = $data['blockallinfo'];
        $helpRichPins = $data['helpRichPins'];

        $title_snip = $data['title_snip'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $svis_on_title = $data['svis_on_title'];
        $svis_on_desc1 = $data['svis_on_desc1'];
        $svis_on_desc2 = $data['svis_on_desc2'];
        $svis_on_desc3 = $data['svis_on_desc3'];
        $svis_on_desc4 = $data['svis_on_desc4'];
        $svis_on_desc5 = $data['svis_on_desc5'];
        $svis_on_desc6 = $data['svis_on_desc6'];
        $breadcrumb_title = $data['breadcrumb_title'];
        $breadvis_on = $data['breadvis_on'];
        $breadvis_on_desc = $data['breadvis_on_desc'];
        $summary_title = $data['summary_title'];
        $allinfo_on_desc = $data['allinfo_on_desc'];
        $pins_title =$data['pins_title'];
        $pins_desc = $data['pins_desc'];
        $pos_pin_button_title = $data['pos_pin_button_title'];
        $leftColumn = $data['leftColumn'];
        $rightColumn = $data['rightColumn'];
        $extraLeft = $data['extraLeft'];
        $extraRight = $data['extraRight'];
        $productFooter = $data['productFooter'];
        $productActions = $data['productActions'];
        $block_radio_buttons_custom = $data['block_radio_buttons_custom'];
        $update_title = $data['update_title'];


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/googlesnippets16_desc1.phtml');
        $desc1 = ob_get_clean();

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/googlesnippets16_desc2.phtml');
        $desc2 = ob_get_clean();


        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_snip,
                    'icon' => 'fa fa-snippets fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $svis_on_title,
                        'name' => 'svis_on',
                        'hint' => $svis_on_title,
                        'desc' => $svis_on_title.$desc1,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $breadcrumb_title,
                    'icon' => 'fa fa-snippets fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $breadvis_on,
                        'name' => 'breadvis_on',
                        'hint' => $breadvis_on,
                        'desc' => $breadvis_on.$desc2,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),
            ),
        );

        $fields_form2 = array(
            'form' => array(
                'legend' => array(
                    'title' => $summary_title,
                    'icon' => 'fa fa-reviews fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $allinfo_on_desc,
                        'name' => 'allinfo_on',
                        'desc' => $allinfo_on_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    $blockallinfo,
                ),
            ),
        );

        $fields_form3 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $pins_title,
                    'icon' => 'fa fa-richpins fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $pins_desc,
                        'name' => 'pinvis_on',
                        'desc' => $pins_desc,
                        'hint' => $pins_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'checkbox_custom',
                        'label' => $pos_pin_button_title,
                        'name' => 'pos_pin_button',
                        'hint' => $pos_pin_button_title,
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'leftColumn',
                                    'name' => $leftColumn,
                                    'val' => 'leftColumn'
                                ),
                                array(
                                    'id' => 'rightColumn',
                                    'name' => $rightColumn,
                                    'val' => 'rightColumn'
                                ),
                                array(
                                    'id' => 'extraLeft',
                                    'name' => $extraLeft,
                                    'val' => 'extraLeft'
                                ),
                                array(
                                    'id' => 'extraRight',
                                    'name' => $extraRight,
                                    'val' => 'extraRight'
                                ),
                                array(
                                    'id' => 'productFooter',
                                    'name' => $productFooter,
                                    'val' => 'productFooter'
                                ),
                                array(
                                    'id' => 'productActions',
                                    'name' => $productActions,
                                    'val' => 'productActions'
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'block_radio_buttons_custom',
                        'label' => $block_radio_buttons_custom,
                        'name' => 'block_radio_buttons_custom',
                        'values'=> array(
                            'style' => Configuration::get($this->_name.'pinterestbuttons')
                        ),
                    ),
                ),
            ),
        );

        $fields_form4 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'snippetssettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesGooglesnippetsSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_googlesnippets16_js.phtml');
        $_html = ob_get_clean();

        return $_html . $helper->generateForm(array($fields_form,$fields_form1,$fields_form2,$fields_form3,$fields_form4)).$helpRichPins;
    }

    public function getConfigFieldsValuesGooglesnippetsSettings(){

        $data_config = array(
            'svis_on' => Configuration::get($this->_name.'svis_on'),
            'allinfo_on' => Configuration::get($this->_name.'allinfo_on'),
            'breadvis_on' => Configuration::get($this->_name.'breadvis_on'),
            'pinvis_on' => Configuration::get($this->_name.'pinvis_on'),
            'leftColumn' => Configuration::get($this->_name.'_leftColumn'),
            'extraLeft' => Configuration::get($this->_name.'_extraLeft'),
            'productFooter' => Configuration::get($this->_name.'_productFooter'),
            'rightColumn' => Configuration::get($this->_name.'_rightColumn'),
            'extraRight' => Configuration::get($this->_name.'_extraRight'),
            'productActions' => Configuration::get($this->_name.'_productActions'),
        );

        return $data_config;
    }


    public function global_custom($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $prefix_review = $data['prefix_review'];

        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $update_title = $data['update_title'];
        $title_main =$data['title_main'];
        $rvis_on = $data['rvis_on'];
        $ratings_on_title = $data['ratings_on_title'];
        $ratings_on_desc1 = $data['ratings_on_desc1'];
        $ratings_on_desc2 = $data['ratings_on_desc2'];
        $ratings_on_desc3 = $data['ratings_on_desc3'];
        $is_avatarr = $data['is_avatarr'];
        $is_filesr = $data['is_filesr'];
        $ruploadfiles = $data['ruploadfiles'];
        $title_on = $data['title_on'];
        $text_on = $data['text_on'];
        $rminc = $data['rminc'];
        $ip_on_title = $data['ip_on_title'];
        $ip_on_desc = $data['ip_on_desc'];
        $is_captcha = $data['is_captcha'];
        $filter_title = $data['filter_title'];
        $is_filterp = $data['is_filterp'];
        $is_filterall = $data['is_filterall'];


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/global_custom_desc1.phtml');
        $desc1 = ob_get_clean();


        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-cogs fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $rvis_on,
                        'name' => 'rvis_on',
                        'desc' => $rvis_on,
                        'hint' => $rvis_on,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $ratings_on_title,
                        'name' => 'ratings_on',
                        'desc' => $ratings_on_desc1.$desc1.$ratings_on_desc3,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_avatarr,
                        'name' => 'is_avatar'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_filesr,
                        'name' => 'is_files'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $ruploadfiles,
                        'name' => 'ruploadfiles',
                        'class' => ' fixed-width-sm',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $title_on,
                        'name' => 'title_on',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $text_on,
                        'name' => 'text_on',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $rminc,
                        'name' => 'rminc',
                        'class' => ' fixed-width-sm',
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $ip_on_title,
                        'name' => 'ip_on',
                        'desc' => $ip_on_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_captcha,
                        'name' => 'is_captcha',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $filter_title,
                    'icon' => 'fa fa-filter fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $is_filterp,
                        'name' => 'is_filterp',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_filterall,
                        'name' => 'is_filterall',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),
            ),
        );

        $fields_form2 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'globalsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesGlobalSettings(array('prefix_review'=>$prefix_review)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_global_js.phtml');
        $_html = ob_get_clean();

        return $_html . $helper->generateForm(array($fields_form,$fields_form1,$fields_form2));
    }

    public function getConfigFieldsValuesGlobalSettings($data){

        $prefix_review = $data['prefix_review'];

        $data_config = array(
            'rvis_on' => Configuration::get($this->_name.'rvis_on'),
            'is_avatar'.$prefix_review => Configuration::get($this->_name.'is_avatar'.$prefix_review),
            'is_files'.$prefix_review => Configuration::get($this->_name.'is_files'.$prefix_review),
            'title_on' => Configuration::get($this->_name.'title_on'),
            'text_on' => Configuration::get($this->_name.'text_on'),
            'ip_on' => Configuration::get($this->_name.'ip_on'),
            'is_captcha' => Configuration::get($this->_name.'is_captcha'),
            'ratings_on' => Configuration::get($this->_name.'ratings_on'),
            'ruploadfiles'=> (int)Configuration::get($this->_name.'ruploadfiles'),
            'rminc'=> (int)Configuration::get($this->_name.'rminc'),
            'is_filterp'=> (int)Configuration::get($this->_name.'is_filterp'),
            'is_filterall'=> (int)Configuration::get($this->_name.'is_filterall'),
        );

        return $data_config;
    }



    public function productpage($data){


        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $productpage_read = $data['productpage_read'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $update_title = $data['update_title'];
        $title_main = $data['title_main'];
        $ptabs_type_title = $data['ptabs_type_title'];
        $ptabs_type1 = $data['ptabs_type1'];
        $ptabs_type2 = $data['ptabs_type2'];
        $ptabs_type3 = $data['ptabs_type3'];
        $ptabs_type_desc1 = $data['ptabs_type_desc1'];
        $ptabs_type_desc2 = $data['ptabs_type_desc2'];
        $is_sortf = $data['is_sortf'];
        $is_helpfulf = $data['is_helpfulf'];
        $is_abusef = $data['is_abusef'];
        $select_stars_title = $data['select_stars_title'];
        $select_stars_desc = $data['select_stars_desc'];
        $starratingon = $data['starratingon'];
        $hooktodisplay_title = $data['hooktodisplay_title'];
        $hooktodisplay_desc = $data['hooktodisplay_desc'];
        $extra_right = $data['extra_right'];
        $extra_left = $data['extra_left'];
        $product_actions = $data['product_actions'];
        $product_footer = $data['product_footer'];
        $none = $data['none'];
        $revperpage = $data['revperpage'];
        $soc_buttons_title = $data['soc_buttons_title'];
        $rsoc_on_title = $data['rsoc_on_title'];
        $rsoc_on_desc = $data['rsoc_on_desc'];
        $rsoccount_on = $data['rsoccount_on'];
        $soc_buttons_desc1 = $data['soc_buttons_desc1'];
        $soc_buttons_desc2 = $data['soc_buttons_desc2'];
        $soc_buttons_desc3 = $data['soc_buttons_desc3'];


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/productpage_desc1.phtml');
        $desc1 = ob_get_clean();


        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa icon-AdminCatalog fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'select',
                        'label' => $ptabs_type_title,
                        'name' => 'ptabs_type',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 1,
                                    'name' => $ptabs_type1,
                                ),
                                array(
                                    'id' => 2,
                                    'name' => $ptabs_type2,
                                ),
                                array(
                                    'id' => 3,
                                    'name' => $ptabs_type3,
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'desc' => $ptabs_type_desc1.
                            '&nbsp;'.$ptabs_type_desc2
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_sortf,
                        'name' => 'is_sortf',
                        'desc' => $is_sortf,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_helpfulf,
                        'name' => 'is_helpfulf',
                        'desc' => $is_helpfulf,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_abusef,
                        'name' => 'is_abusef',
                        'desc' => $is_abusef,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'select_stars',
                        'label' => $select_stars_title,
                        'name' => 'select_stars',
                        'values' => array(
                            'stylestars'=>Configuration::get($this->_name.'stylestars'),
                        ),
                        'desc' => $select_stars_desc
                    ),
                    array(
                        'type' => 'select_stars_custom',
                        'label' => $starratingon,
                        'name' => 'starratingon',
                        'values' => array(
                            'value'=>Configuration::get($this->_name.'starratingon'),
                            'stylestars'=>Configuration::get($this->_name.'stylestars'),
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $hooktodisplay_title,
                        'name' => 'hooktodisplay',
                        'class' => ' fixed-width-xxl',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => 'extra_right',
                                    'name' => $extra_right,
                                ),
                                array(
                                    'id' => 'extra_left',
                                    'name' => $extra_left,
                                ),
                                array(
                                    'id' => 'product_actions',
                                    'name' => $product_actions,
                                ),
                                array(
                                    'id' => 'product_footer',
                                    'name' => $product_footer,
                                ),
                                array(
                                    'id' => 'none',
                                    'name' => $none,
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                        'desc' => $hooktodisplay_desc,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $revperpage,
                        'name' => 'revperpage',
                        'class' => ' fixed-width-sm',
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $soc_buttons_title,
                    'icon' => 'fa fa-facebook fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $rsoc_on_title,
                        'name' => 'rsoc_on',
                        'desc' => $rsoc_on_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $rsoccount_on,
                        'name' => 'rsoccount_on',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),
                'desc' => $soc_buttons_desc1.$desc1.$soc_buttons_desc3,
            ),
        );

        $fields_form2 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'productpagesettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesProductpageSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_productpage_js.phtml');
        $_html = ob_get_clean();

        return  $_html . $helper->generateForm(array($fields_form,$fields_form1,$fields_form2));
    }

    public function getConfigFieldsValuesProductpageSettings(){

        $data_config = array(
            'ptabs_type' => Configuration::get($this->_name.'ptabs_type'),
            'hooktodisplay' => Configuration::get($this->_name.'hooktodisplay'),
            'revperpage' => Configuration::get($this->_name.'revperpage'),
            'rsoc_on' => Configuration::get($this->_name.'rsoc_on'),
            'rsoccount_on' => Configuration::get($this->_name.'rsoccount_on'),
            'is_abusef' => Configuration::get($this->_name.'is_abusef'),
            'is_helpfulf' => Configuration::get($this->_name.'is_helpfulf'),
            'is_sortf' => Configuration::get($this->_name.'is_sortf'),
        );

        return $data_config;
    }

    public function reviewsmanagement($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $update_title = $data['update_title'];
        $title_main = $data['title_main'];
        $is_approval = $data['is_approval'];
        $whocanadd = $data['whocanadd'];
        $is_onerev = $data['is_onerev'];
        $rswitch_lng = $data['rswitch_lng'];
        $is_sortallf = $data['is_sortallf'];
        $is_sortfu = $data['is_sortfu'];
        $revperpageall = $data['revperpageall'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-reviews fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $is_approval,
                        'name' => 'is_approval',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'block_radio_buttons_reviews_custom',
                        'label' => $whocanadd,
                        'name' => 'block_radio_buttons_reviews_custom',
                        'values'=> array(
                            'value' => Configuration::get($this->_name.'whocanadd')
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_onerev,
                        'name' => 'is_onerev',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $rswitch_lng,
                        'name' => 'rswitch_lng',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_sortallf,
                        'name' => 'is_sortallf',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_sortfu,
                        'name' => 'is_sortfu',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $revperpageall,
                        'name' => 'revperpageall',
                        'class' => ' fixed-width-sm',
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'reviewsmanagementsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesReviewsmanagementSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_reviewsmanagement_js.phtml');
        $_html = ob_get_clean();

        return  $_html.$helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesReviewsmanagementSettings(){

        $data_config = array(
            'is_approval' => Configuration::get($this->_name.'is_approval'),
            'rswitch_lng' => Configuration::get($this->_name.'rswitch_lng'),
            'revperpageall' => Configuration::get($this->_name.'revperpageall'),
            'is_onerev' => Configuration::get($this->_name.'is_onerev'),
            'is_sortallf'=> Configuration::get($this->_name.'is_sortallf'),
            'is_sortfu'=> Configuration::get($this->_name.'is_sortfu'),
        );

        return $data_config;
    }

    public function customeraccountreviewspage($data){
        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $update_title = $data['update_title'];
        $title_main = $data['title_main'];
        $revperpagecus = $data['revperpagecus'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa icon-AdminParentCustomer fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $revperpagecus,
                        'name' => 'revperpagecus',
                        'class' => ' fixed-width-sm',
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'customeraccountreviewspagesettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesCustomeraccountreviewspageSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesCustomeraccountreviewspageSettings(){

        $data_config = array(
            'revperpagecus' => (int)Configuration::get($this->_name.'revperpagecus'),
        );

        return $data_config;
    }


    public function woweffects($data){
        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $update_title = $data['update_title'];
        $title_main = $data['title_main'];
        $d_eff_rev = $data['d_eff_rev'];
        $d_eff_rev_title = $data['d_eff_rev_title'];
        $d_eff_rev_my = $data['d_eff_rev_my'];
        $d_eff_rev_my_title = $data['d_eff_rev_my_title'];
        $d_eff_rev_u = $data['d_eff_rev_u'];
        $d_eff_rev_u_title = $data['d_eff_rev_u_title'];
        $d_eff_rev_all = $data['d_eff_rev_all'];
        $d_eff_rev_all_title = $data['d_eff_rev_all_title'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'wow_display_effect',
                        'label' => $d_eff_rev_title,
                        'name' => 'd_eff_rev',
                        'id' => 'd_eff_rev',
                        'lang' => FALSE,
                        'value' => $d_eff_rev
                    ),
                    array(
                        'type' => 'wow_display_effect',
                        'label' => $d_eff_rev_my_title,
                        'name' => 'd_eff_rev_my',
                        'id' => 'd_eff_rev_my',
                        'lang' => FALSE,
                        'value' => $d_eff_rev_my,
                    ),
                    array(
                        'type' => 'wow_display_effect',
                        'label' =>$d_eff_rev_u_title,
                        'name' => 'd_eff_rev_u',
                        'id' => 'd_eff_rev_u',
                        'lang' => FALSE,
                        'value' => $d_eff_rev_u
                    ),
                    array(
                        'type' => 'wow_display_effect',
                        'label' => $d_eff_rev_all_title,
                        'name' => 'd_eff_rev_all',
                        'id' => 'd_eff_rev_all',
                        'lang' => FALSE,
                        'value' => $d_eff_rev_all,
                    ),
                ),
            ),
        );
        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'woweffectssettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesWoweffectsSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesWoweffectsSettings(){
        $data_config = array(
        );
        return $data_config;
    }


    public function owlcarousels($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $update_title = $data['update_title'];
        $title_main = $data['title_main'];
        $sr_sliderr = $data['sr_sliderr'];
        $sr_slr = $data['sr_slr'];
        $title_main_home = $data['title_main_home'];
        $sr_sliderhr = $data['sr_sliderhr'];
        $sr_slhr = $data['sr_slhr'];

        $fields_form1 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $sr_sliderr,
                        'name' => 'sr_sliderr',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $sr_slr,
                        'name' => 'sr_slr',
                        'id' => 'sr_slr',
                        'lang' => FALSE,
                    ),
                ),
            ),
        );

        $fields_form2 = array(
            'form'=> array(

                'legend' => array(
                    'title' => $title_main_home,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $sr_sliderhr,
                        'name' => 'sr_sliderhr',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $sr_slhr,
                        'name' => 'sr_slhr',
                        'id' => 'sr_slhr',
                        'lang' => FALSE,
                    ),
                ),
            ),
        );

        $fields_form10 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'owlcarouselssettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesOwlcarouselsSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_owlcarousels_js.phtml');
        $_html = ob_get_clean();

        return  $_html.$helper->generateForm(array($fields_form1,$fields_form2, $fields_form10));
    }

    public function getConfigFieldsValuesOwlcarouselsSettings(){

        $data_config = array(
            'sr_sliderr'=> (int)Configuration::get($this->_name.'sr_sliderr'),
            'sr_slr'=> (int)Configuration::get($this->_name.'sr_slr'),
            'sr_sliderhr'=> (int)Configuration::get($this->_name.'sr_sliderhr'),
            'sr_slhr'=> (int)Configuration::get($this->_name.'sr_slhr'),
        );

        return $data_config;
    }


    public function lastreviewsblock($data){
        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $customhookhelp = $data['customhookhelp'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $update_title = $data['update_title'];
        $title_main = $data['title_main'];
        $is_blocklr = $data['is_blocklr'];
        $data_translate = $data;

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-list-alt fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $is_blocklr,
                        'name' => 'is_blocklr',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    $this->block_last_reviews($data_translate),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'lastreviewsblocksettings';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesLastreviewsblockpageSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_lastreviewsblock_js.phtml');
        $_html = ob_get_clean();

        return  $_html . $helper->generateForm(array($fields_form,$fields_form1)).$customhookhelp;
    }

    public function getConfigFieldsValuesLastreviewsblockpageSettings(){

        $data_config = array(
            'is_blocklr' => (int)Configuration::get($this->_name.'is_blocklr'),
        );

        return $data_config;
    }

    private function block_last_reviews($data){

        $block_last_reviews = $data['block_last_reviews'];
        $blocklr_home = $data['blocklr_home'];
        $blocklr_cat = $data['blocklr_cat'];
        $blocklr_man = $data['blocklr_man'];
        $blocklr_prod = $data['blocklr_prod'];
        $blocklr_oth = $data['blocklr_oth'];
        $blocklr_chook = $data['blocklr_chook'];
        $top = $data['top'];
        $bottom = $data['bottom'];
        $leftcol = $data['leftcol'];
        $rightcol = $data['rightcol'];
        $home = $data['home'];
        $chook = $data['chook'];

        $data_img_sizes = array();

        $available_types = ImageType::getImagesTypes('products');

        foreach ($available_types as $type){
            $id = $type['name'];
            $name = $type['name'].' ('.$type['width'].' x '.$type['height'].')';
            $data_item_size = array(
                'id' => $id,
                'name' => $name,
            );
            array_push($data_img_sizes,$data_item_size);
        }

        $block_last_reviews = array(
            'type' => 'block_last_reviews',
            'label' => $block_last_reviews,
            'name' => 'block_last_reviews',
            'values'=> array(
                'blocklr_home' => array('name'=>$blocklr_home,
                    'status' => Configuration::get($this->_name.'blocklr_home'),
                    'position'=>Configuration::get($this->_name.'blocklr_home_pos'),
                    'number_display_reviews' => array('number_display_reviews' => (int)Configuration::get($this->_name.'blocklr_home_ndr'), 'name'=>'blocklr_home_ndr'),
                    'truncate' => array('truncate' => (int)Configuration::get($this->_name.'blocklr_home_tr'), 'name'=>'blocklr_home_tr'),
                    'imsize' => array('imsize' => Configuration::get($this->_name.'blocklr_home_im'), 'name'=>'blocklr_home_im'),
                    'width' => array('width' => (int)Configuration::get($this->_name.'blocklr_home_w'), 'name'=>'blocklr_home_w')),
                'blocklr_cat' => array('name'=>$blocklr_cat,
                    'status' => Configuration::get($this->_name.'blocklr_cat'),
                    'position'=>Configuration::get($this->_name.'blocklr_cat_pos'),
                    'number_display_reviews' => array('number_display_reviews' => (int)Configuration::get($this->_name.'blocklr_cat_ndr'), 'name'=>'blocklr_cat_ndr'),
                    'truncate' => array('truncate' => (int)Configuration::get($this->_name.'blocklr_cat_tr'), 'name'=>'blocklr_cat_tr'),
                    'imsize' => array('imsize' => Configuration::get($this->_name.'blocklr_cat_im'), 'name'=>'blocklr_cat_im'),
                    'width' => array('width' => (int)Configuration::get($this->_name.'blocklr_cat_w'), 'name'=>'blocklr_cat_w')),
                'blocklr_man' => array('name'=>$blocklr_man,
                    'status' => Configuration::get($this->_name.'blocklr_man'),
                    'position'=>Configuration::get($this->_name.'blocklr_man_pos'),
                    'number_display_reviews' => array('number_display_reviews' => (int)Configuration::get($this->_name.'blocklr_man_ndr'), 'name'=>'blocklr_man_ndr'),
                    'truncate' => array('truncate' => (int)Configuration::get($this->_name.'blocklr_man_tr'), 'name'=>'blocklr_man_tr'),
                    'imsize' => array('imsize' => Configuration::get($this->_name.'blocklr_man_im'), 'name'=>'blocklr_man_im'),
                    'width' => array('width' => (int)Configuration::get($this->_name.'blocklr_man_w'), 'name'=>'blocklr_man_w')),
                'blocklr_prod' => array('name'=>$blocklr_prod,
                    'status' => Configuration::get($this->_name.'blocklr_prod'),
                    'position'=>Configuration::get($this->_name.'blocklr_prod_pos'),
                    'number_display_reviews' => array('number_display_reviews' => (int)Configuration::get($this->_name.'blocklr_prod_ndr'), 'name'=>'blocklr_prod_ndr'),
                    'truncate' => array('truncate' => (int)Configuration::get($this->_name.'blocklr_prod_tr'), 'name'=>'blocklr_prod_tr'),
                    'imsize' => array('imsize' => Configuration::get($this->_name.'blocklr_prod_im'), 'name'=>'blocklr_prod_im'),
                    'width' => array('width' => (int)Configuration::get($this->_name.'blocklr_prod_w'), 'name'=>'blocklr_prod_w')),
                'blocklr_oth' => array('name'=>$blocklr_oth,
                    'status' => Configuration::get($this->_name.'blocklr_oth'),
                    'position'=>Configuration::get($this->_name.'blocklr_oth_pos'),
                    'number_display_reviews' => array('number_display_reviews' => (int)Configuration::get($this->_name.'blocklr_oth_ndr'), 'name'=>'blocklr_oth_ndr'),
                    'truncate' => array('truncate' => (int)Configuration::get($this->_name.'blocklr_oth_tr'), 'name'=>'blocklr_oth_tr'),
                    'imsize' => array('imsize' => Configuration::get($this->_name.'blocklr_oth_im'), 'name'=>'blocklr_oth_im'),
                    'width' => array('width' => (int)Configuration::get($this->_name.'blocklr_oth_w'), 'name'=>'blocklr_oth_w')),
                'blocklr_chook' => array('name'=>$blocklr_chook,
                    'status' => Configuration::get($this->_name.'blocklr_chook'),
                    'position'=>Configuration::get($this->_name.'blocklr_chook_pos'),
                    'number_display_reviews' => array('number_display_reviews' => (int)Configuration::get($this->_name.'blocklr_chook_ndr'), 'name'=>'blocklr_chook_ndr'),
                    'truncate' => array('truncate' => (int)Configuration::get($this->_name.'blocklr_chook_tr'), 'name'=>'blocklr_chook_tr'),
                    'imsize' => array('imsize' => Configuration::get($this->_name.'blocklr_chook_im'), 'name'=>'blocklr_chook_im'),
                    'width' => array('width' => (int)Configuration::get($this->_name.'blocklr_chook_w'), 'name'=>'blocklr_chook_w')),
            ),
            'available_pos' => array(
                'top'=>$top,
                'bottom'=>$bottom,
                'leftcol'=>$leftcol,
                'rightcol'=>$rightcol,

            ),
            'available_pos_home' => array(
                'top'=>$top,
                'home'=>$home,
                'bottom'=>$bottom,
                'leftcol'=>$leftcol,
                'rightcol'=>$rightcol,

            ),
            'available_pos_chook' => array(
                'chook'=>$chook,
            ),
            'image_sizes' => $data_img_sizes,
        );

        return $block_last_reviews;
    }


    public function starslistandsearch($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $update_title = $data['update_title'];
        $title_main = $data['title_main'];
        $starscat = $data['starscat'];
        $is_starscat = $data['is_starscat'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-bars fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $starscat,
                        'name' => 'starscat',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_starscat,
                        'name' => 'is_starscat',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'starslistandsearchsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesStarslistandsearchSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        $_html = '';
        if (version_compare(_PS_VERSION_, '1.7', '>')) {

            $starslistandsearch_faq = $data['starslistandsearch_faq'];
            $faqStars17_spmgsnipreview = $data['faqStars17_spmgsnipreview'];


            ob_start();
            include(dirname(__FILE__).'/../views/templates/hooks/_starslistandsearch.phtml');
            $_html = ob_get_clean();
        }

        return  $helper->generateForm(array($fields_form,$fields_form1)).$_html;
    }

    public function getConfigFieldsValuesStarslistandsearchSettings(){
        $data_config = array(
            'starscat' => (int)Configuration::get($this->_name.'starscat'),
            'is_starscat'=> (int)Configuration::get($this->_name.'is_starscat'),
        );

        return $data_config;
    }


    public function rssfeed($data){
        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $update_title = $data['update_title'];
        $title_main = $data['title_main'];
        $rsson = $data['rsson'];
        $rssname = $data['rssname'];
        $rssdesc = $data['rssdesc'];
        $number_rssitems = $data['number_rssitems'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-rss fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $rsson,
                        'name' => 'rsson',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $rssname,
                        'name' => 'rssname',
                        'id' => 'rssname',
                        'lang' => TRUE,
                        'size' => 50,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $rssdesc,
                        'name' => 'rssdesc',
                        'id' => 'rssdesc',
                        'lang' => TRUE,
                        'size' => 50,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $number_rssitems,
                        'name' => 'number_rssitems',
                        'class' => ' fixed-width-sm',
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'rssfeedsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesRssfeedSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesRssfeedSettings(){
        $languages = Language::getLanguages(false);
        $fields_rssname = array();
        $fields_rssdesc = array();

        foreach ($languages as $lang)
        {
            $fields_rssname[$lang['id_lang']] =  Configuration::get($this->_name.'rssname_'.$lang['id_lang']);
            $fields_rssdesc[$lang['id_lang']] =  Configuration::get($this->_name.'rssdesc_'.$lang['id_lang']);
        }

        $data_config = array(
            'rsson' => Configuration::get($this->_name.'rsson'),
            'rssname' => $fields_rssname,
            'rssdesc' => $fields_rssdesc,
            'number_rssitems' => (int)Configuration::get($this->_name.'number_rssitems'),
        );

        return $data_config;
    }



    public function voucherwhenaddreviewsettings16($data){


        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];


        $title_translate = $data['title'];
        $vis_on_title = $data['vis_on_title'];
        $vis_on_desc = $data['vis_on_desc'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $ids_groups_title = $data['ids_groups_title'];
        $coupondesc_title = $data['coupondesc_title'];
        $coupondesc_desc = $data['coupondesc_desc'];
        $vouchercode_title = $data['vouchercode_title'];
        $vouchercode_desc = $data['vouchercode_desc'];
        $discount_type_title = $data['discount_type_title'];
        $discount_type1 = $data['discount_type1'];
        $discount_type2 = $data['discount_type2'];
        $tax_title = $data['tax_title'];
        $tax1 = $data['tax1'];
        $tax2 = $data['tax2'];
        $percentage_val = $data['percentage_val'];
        $isminamount = $data['isminamount'];
        $is_show_min_title = $data['is_show_min_title'];
        $is_show_min_desc = $data['is_show_min_desc'];
        $select_cat_title = $data['select_cat_title'];
        $select_cat_desc = $data['select_cat_desc'];
        $sdvvalid_title = $data['sdvvalid_title'];
        $sdvvalid_desc = $data['sdvvalid_desc'];
        $highlight_title = $data['highlight_title'];
        $highlight_desc = $data['highlight_desc'];
        $cumulativeother_title = $data['cumulativeother_title'];
        $cumulativereduc_title = $data['cumulativereduc_title'];
        $save_title = $data['save_title'];

        $selected_categories = $data['selected_categories'];



        $cookie = $this->context->cookie;

        $curs = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);

        $discount_type_currency_value = array();
        $min_checkout_value = array();

        foreach ($curs AS $_cur){

            $discount_type_currency_value[$_cur['id_currency']] = array('amount' => Tools::getValue('sdamount['.(int)($_cur['id_currency']).']', Configuration::get('sdamount_'.(int)($_cur['id_currency']))),
                'currency'=>$_cur['sign'],
                'name'=>htmlentities($_cur['name'], ENT_NOQUOTES, 'utf-8'),
                'name_item' => 'sdamount'
            );

            $min_checkout_value[$_cur['id_currency']]  = array('amount' => Tools::getValue('sdminamount['.(int)($_cur['id_currency']).']', Configuration::get('sdminamount_'.(int)($_cur['id_currency']))),
                'currency'=>$_cur['sign'],
                'name'=>htmlentities($_cur['name'], ENT_NOQUOTES, 'utf-8'),
                'name_item' => 'sdminamount'
            );
        }

        // select categories


        $id_lang =  $cookie->id_lang;
        $ids_groups = Configuration::get($this->_name.'ids_groups');
        $ids_groups = $ids_groups?explode(",",$ids_groups):array();



        // select categories

        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $title_translate,
                    'icon' => 'fa fa-reviews fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $vis_on_title,
                        'name' => 'vis_on',
                        'desc' => '<b style="color:red">'.$vis_on_desc.'</b>',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'group_association',
                        'label' => $ids_groups_title,
                        'name' => 'ids_groups',
                        'values'=>Group::getGroups($id_lang),
                        'selected_data'=>$ids_groups,
                        'required' => TRUE,
                        'desc' => $ids_groups_title,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $coupondesc_title,
                        'name' => 'coupondesc',
                        'lang' => true,
                        'hint' => $coupondesc_desc,
                        'desc' => $coupondesc_desc
                    ),
                    array(
                        'type' => 'text',
                        'label' => $vouchercode_title,
                        'name' => 'vouchercode',
                        'desc' => $vouchercode_desc,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $discount_type_title,
                        'name' => 'discount_type',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '1',
                                    'name' => $discount_type1
                                ),
                                array(
                                    'id' => '2',
                                    'name' => $discount_type2,
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'cms_pages',
                        'label' => '',
                        'name' => 'currency_val',
                        'values'=> $discount_type_currency_value,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $tax_title,
                        'name' => 'tax',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '0',
                                    'name' => $tax1
                                ),
                                array(
                                    'id' => '1',
                                    'name' => $tax2,
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text_custom',
                        'label' => $percentage_val,
                        'name' => 'percentage_val',
                        'value'=> Configuration::get($this->_name.'percentage_val'),
                    ),
                    array(
                        'type' => 'checkbox_custom',
                        'label' => $isminamount,
                        'name' => $this->_name.'isminamount',
                        'hint' => $isminamount,
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => $this->_name.'isminamount',
                                    'name' => '',
                                    'val' => 1
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_show_min_title,
                        'name' => 'is_show_min',
                        'desc' => $is_show_min_desc,
                        'hint' => $is_show_min_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'cms_pages',
                        'label' => '',
                        'name' => 'mincheckout_val',
                        'values'=> $min_checkout_value,
                    ),
                    array(
                        'type' => 'cms_categories',
                        'label' => $select_cat_title,
                        'hint' => $select_cat_desc,
                        'desc' => $select_cat_desc,
                        'name' => 'select_cat',
                        'values'=> $selected_categories,

                    ),
                    array(
                        'type' => 'text_validity',
                        'label' => $sdvvalid_title,
                        'name' => 'sdvvalid',
                        'value'=> Configuration::get($this->_name.'sdvvalid'),
                        'desc' =>$sdvvalid_desc,
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $highlight_title,
                        'name' => 'highlight',
                        'desc'=>$highlight_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $cumulativeother_title,
                        'name' => 'cumulativeother',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $cumulativereduc_title,
                        'name' => 'cumulativereduc',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),

                'submit' => array(
                    'title' => $save_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'vouchersettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesCouponSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        $currency_discount_amount = (int)Configuration::get($this->_name.'discount_type');

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_voucherwhenaddreviewsettings16_js.phtml');
        $_html = ob_get_clean();

        return $_html . $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValuesCouponSettings(){
        $languages = Language::getLanguages(false);
        $fields_fcoupondesc = array();

        foreach ($languages as $lang)
        {
            $fields_fcoupondesc[$lang['id_lang']] = Configuration::get($this->_name.'coupondesc_'.$lang['id_lang']);
        }

        $data_config = array(
            'vis_on' => Configuration::get($this->_name.'vis_on'),
            'vouchercode' => Configuration::get($this->_name.'vouchercode'),
            'discount_type' => Configuration::get($this->_name.'discount_type'),
            'coupondesc' => $fields_fcoupondesc,
            'tax' =>  Configuration::get($this->_name.'tax'),
            $this->_name.'isminamount' =>  Configuration::get($this->_name.'isminamount'),

            'is_show_min' => Configuration::get($this->_name.'is_show_min'),

            'cumulativeother' => Configuration::get($this->_name.'cumulativeother'),

            'cumulativereduc' =>  Configuration::get($this->_name.'cumulativereduc'),
            'highlight' =>  Configuration::get($this->_name.'highlight'),
        );
        return $data_config;
    }



    public function voucherwhensharereviewsettings16($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];


        $title_translate = $data['title_fb'];
        $vis_on_title = $data['vis_on_title'];
        $vis_on_desc_fb = $data['vis_on_desc_fb'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $ids_groups_title = $data['ids_groups_title'];
        $coupondesc_title = $data['coupondesc_title'];
        $coupondesc_desc = $data['coupondesc_desc'];
        $vouchercode_title = $data['vouchercode_title'];
        $vouchercode_desc = $data['vouchercode_desc'];
        $discount_type_title = $data['discount_type_title'];
        $discount_type1 = $data['discount_type1'];
        $discount_type2 = $data['discount_type2'];
        $tax_title = $data['tax_title'];
        $tax1 = $data['tax1'];
        $tax2 = $data['tax2'];
        $percentage_val = $data['percentage_val'];
        $isminamount = $data['isminamount'];
        $is_show_min_title = $data['is_show_min_title'];
        $is_show_min_desc = $data['is_show_min_desc'];
        $select_cat_title = $data['select_cat_title'];
        $select_cat_desc = $data['select_cat_desc'];
        $sdvvalid_title = $data['sdvvalid_title'];
        $sdvvalid_desc = $data['sdvvalid_desc'];
        $highlight_title = $data['highlight_title'];
        $highlight_desc = $data['highlight_desc'];
        $cumulativeother_title = $data['cumulativeother_title'];
        $cumulativereduc_title = $data['cumulativereduc_title'];
        $save_title = $data['save_title'];

        $selected_categories = $data['selected_categories'];

        $cookie = $this->context->cookie;

        $curs = Currency::getCurrenciesByIdShop(Context::getContext()->shop->id);

        $discount_type_currency_value = array();
        $min_checkout_value = array();

        foreach ($curs AS $_cur){

            $discount_type_currency_value[$_cur['id_currency']] = array('amount' => Tools::getValue('sdamountfb['.(int)($_cur['id_currency']).']', Configuration::get('sdamountfb_'.(int)($_cur['id_currency']))),
                'currency'=>$_cur['sign'],
                'name'=>htmlentities($_cur['name'], ENT_NOQUOTES, 'utf-8'),
                'name_item' => 'sdamountfb'
            );

            $min_checkout_value[$_cur['id_currency']]  = array('amount' => Tools::getValue('sdminamountfb['.(int)($_cur['id_currency']).']', Configuration::get('sdminamountfb_'.(int)($_cur['id_currency']))),
                'currency'=>$_cur['sign'],
                'name'=>htmlentities($_cur['name'], ENT_NOQUOTES, 'utf-8'),
                'name_item' => 'sdminamountfb'
            );
        }

        // select categories

        $id_lang =  $cookie->id_lang;
        $ids_groups = Configuration::get($this->_name.'ids_groupsfb');
        $ids_groups = $ids_groups?explode(",",$ids_groups):array();


        // select categories
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $title_translate,
                    'icon' => 'fa fa-facebook fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $vis_on_title,
                        'name' => 'vis_onfb',
                            'desc' => '<b style="color:red">'.$vis_on_desc_fb.'</b>',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'group_association',
                        'label' => $ids_groups_title,
                        'name' => 'ids_groupsfb',
                        'values'=>Group::getGroups($id_lang),
                        'selected_data'=>$ids_groups,
                        'required' => TRUE,
                        'desc' => $ids_groups_title,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $coupondesc_title,
                        'name' => 'coupondescfb',
                        'lang' => true,
                        'hint' => $coupondesc_desc,
                        'desc' => $coupondesc_desc
                    ),
                    array(
                        'type' => 'text',
                        'label' => $vouchercode_title,
                        'name' => 'vouchercodefb',
                        'desc' => $vouchercode_desc,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $discount_type_title,
                        'name' => 'discount_typefb',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '1',
                                    'name' => $discount_type1
                                ),
                                array(
                                    'id' => '2',
                                    'name' => $discount_type2,
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'cms_pages',
                        'label' => '',
                        'name' => 'currency_valfb',
                        'values'=> $discount_type_currency_value,
                    ),
                    array(
                        'type' => 'select',
                        'label' => $tax_title,
                        'name' => 'taxfb',
                        'options' => array(
                            'query' => array(
                                array(
                                    'id' => '0',
                                    'name' => $tax1
                                ),
                                array(
                                    'id' => '1',
                                    'name' => $tax2,
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                    array(
                        'type' => 'text_custom',
                        'label' => $percentage_val,
                        'name' => 'percentage_valfb',
                        'value'=> Configuration::get($this->_name.'percentage_valfb'),
                    ),
                    array(
                        'type' => 'checkbox_custom',
                        'label' => $isminamount,
                        'name' => $this->_name.'isminamountfb',
                        'hint' => $isminamount,
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => $this->_name.'isminamountfb',
                                    'name' => '',
                                    'val' => 1
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_show_min_title,
                        'name' => 'is_show_minfb',
                        'desc' => $is_show_min_desc,
                        'hint' => $is_show_min_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'cms_pages',
                        'label' => '',
                        'name' => 'mincheckout_valfb',
                        'values'=> $min_checkout_value,
                    ),
                    array(
                        'type' => 'cms_categories',
                        'label' => $select_cat_title,
                        'hint' => $select_cat_desc,
                        'desc' => $select_cat_desc,
                        'name' => 'select_catfb',
                        'values'=> $selected_categories,
                    ),
                    array(
                        'type' => 'text_validity',
                        'label' => $sdvvalid_title,
                        'name' => 'sdvvalidfb',
                        'value'=> Configuration::get($this->_name.'sdvvalidfb'),
                        'desc' =>$sdvvalid_desc,
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $highlight_title,
                        'name' => 'highlightfb',
                        'desc'=>$highlight_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $cumulativeother_title,
                        'name' => 'cumulativeotherfb',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $cumulativereduc_title,
                        'name' => 'cumulativereducfb',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                ),
                'submit' => array(
                    'title' => $save_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'vouchersettingsfb';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesCouponWhenShareReviewSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        $currency_discount_amount = (int)Configuration::get($this->_name.'discount_typefb');

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_voucherwhensharereviewsettings16_js.phtml');
        $_html = ob_get_clean();

        return $_html . $helper->generateForm(array($fields_form));
    }

    public function getConfigFieldsValuesCouponWhenShareReviewSettings(){

        $languages = Language::getLanguages(false);
        $fields_fcoupondesc = array();

        foreach ($languages as $lang)
        {
            $fields_fcoupondesc[$lang['id_lang']] = Configuration::get($this->_name.'coupondescfb_'.$lang['id_lang']);
        }

        $data_config = array(
            'vis_onfb' => Configuration::get($this->_name.'vis_onfb'),
            'vouchercodefb' => Configuration::get($this->_name.'vouchercodefb'),
            'discount_typefb' => Configuration::get($this->_name.'discount_typefb'),
            'coupondescfb' => $fields_fcoupondesc,
            'taxfb' =>  Configuration::get($this->_name.'taxfb'),

            $this->_name.'isminamountfb' =>  Configuration::get($this->_name.'isminamountfb'),

            'is_show_minfb' => Configuration::get($this->_name.'is_show_minfb'),

            'cumulativeotherfb' => Configuration::get($this->_name.'cumulativeotherfb'),

            'cumulativereducfb' =>  Configuration::get($this->_name.'cumulativereducfb'),

            'highlightfb' => Configuration::get($this->_name.'highlightfb'),
        );

        return $data_config;
    }


    public function reviewsemails($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $title_main = $data['title_main'];
        $mail = $data['mail'];
        $noti_title = $data['noti_title'];
        $noti_desc  = $data['noti_desc'];
        $img_size_em_title = $data['img_size_em_title'];
        $img_size_em_desc = $data['img_size_em_desc'];
        $update_title = $data['update_title'];

        $data_img_sizes = array();
        $available_types = ImageType::getImagesTypes('products');

        foreach ($available_types as $type){

            $id = $type['name'];
            $name = $type['name'].' ('.$type['width'].' x '.$type['height'].')';

            $data_item_size = array(
                'id' => $id,
                'name' => $name,
            );
            array_push($data_img_sizes,$data_item_size);
        }

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-envelope-o fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'text_custom_email',
                        'label' => $mail,
                        'name' => 'mail',
                        'value'=> Configuration::get($this->_name.'mail'),
                    ),
                    array(
                        'type' => 'checkbox_custom_email',
                        'label' =>$noti_title,
                        'name' => 'noti',
                        'desc' => $noti_desc,
                        'values' => array(
                            'value' => (int)Configuration::get($this->_name.'noti')
                        ),
                    ),
                    array(
                        'type' => 'select',
                        'label' => $img_size_em_title,
                        'name' => 'img_size_em',
                        'desc' => $img_size_em_desc,
                        'options' => array(
                            'query' => $data_img_sizes,
                            'id' => 'id',
                            'name' => 'name'
                        )
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'reviewsemailssettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesReviewsemailsSettings(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesReviewsemailsSettings(){

        $data_config = array(
            'img_size_em' => Configuration::get($this->_name.'img_size_em'),
        );

        return $data_config;
    }


    public function responseadminemails($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $prefix_review = $data['prefix_review'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $title_main = $data['title_main'];
        $is_emailreminder_title = $data['is_emailreminder_title'];
        $emailreminder_title = $data['emailreminder_title'];
        $emailreminder_desc = $data['emailreminder_desc'];
        $is_reminderok = $data['is_reminderok'];
        $reminderok_title = $data['reminderok_title'];
        $reminderok_desc = $data['reminderok_desc'];
        $is_thankyou = $data['is_thankyou'];
        $thankyou_title = $data['thankyou_title'];
        $thankyou_desc = $data['thankyou_desc'];
        $is_newrev = $data['is_newrev'];
        $newrev_title = $data['newrev_title'];
        $newrev_desc = $data['newrev_desc'];
        $is_subresem = $data['is_subresem'];
        $subresem_title = $data['subresem_title'];
        $subresem_desc = $data['subresem_desc'];
        $textresem_title = $data['textresem_title'];
        $textresem_desc = $data['textresem_desc'];
        $is_modrev = $data['is_modrev'];
        $modrev_title = $data['modrev_title'];
        $modrev_desc = $data['modrev_desc'];
        $is_subpubem = $data['is_subpubem'];
        $subpubem_title=$data['subpubem_title'];
        $subpubem_desc=$data['subpubem_desc'];
        $is_abuserev = $data['is_abuserev'];
        $abuserev_title = $data['abuserev_title'];
        $abuserev_desc = $data['abuserev_desc'];
        $is_revvouc = $data['is_revvouc'];
        $revvouc_title = $data['revvouc_title'];
        $revvouc_desc = $data['revvouc_desc'];
        $is_facvouc = $data['is_facvouc'];
        $facvouc_title = $data['facvouc_title'];
        $facvouc_desc = $data['facvouc_desc'];
        $is_sugvouc = $data['is_sugvouc'];
        $sugvouc_title = $data['sugvouc_title'];
        $sugvouc_desc = $data['sugvouc_desc'];
        $update_title = $data['update_title'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-envelope-o fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $is_emailreminder_title,
                        'name' => 'is_emailreminder',

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_emailreminder_title,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $emailreminder_title,
                        'name' => 'emailreminder',
                        'id' => 'emailreminder',
                        'lang' => TRUE,
                        'desc' => $emailreminder_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_reminderok,
                        'name' => 'is_reminderok'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_reminderok,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $reminderok_title,
                        'name' => 'reminderok'.$prefix_review,
                        'id' => 'reminderok'.$prefix_review,
                        'lang' => TRUE,
                        'desc' => $reminderok_desc,
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_thankyou,
                        'name' => 'is_thankyou'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_thankyou,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $thankyou_title,
                        'name' => 'thankyou'.$prefix_review,
                        'id' => 'thankyou'.$prefix_review,
                        'lang' => TRUE,
                        'desc' => $thankyou_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_newrev,
                        'name' => 'is_newrev'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_newrev,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $newrev_title,
                        'name' => 'newrev'.$prefix_review,
                        'id' => 'newrev'.$prefix_review,
                        'lang' => TRUE,
                        'desc' => $newrev_desc,
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_subresem,
                        'name' => 'is_subresem',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_subresem,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $subresem_title,
                        'name' => 'subresem',
                        'id' => 'subresem',
                        'lang' => TRUE,
                        'desc' => $subresem_desc,
                    ),
                    array(
                        'type' => 'textarea',
                        'label' => $textresem_title,
                        'name' => 'textresem',
                        'autoload_rte' => FALSE,
                        'lang' => TRUE,
                        'class'=>'spm-textarea-email',
                        'desc' => $textresem_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_modrev,
                        'name' => 'is_modrev'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_modrev,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $modrev_title,
                        'name' => 'modrev'.$prefix_review,
                        'id' => 'modrev'.$prefix_review,
                        'lang' => TRUE,
                        'desc' => $modrev_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_subpubem,
                        'name' => 'is_subpubem',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_subpubem,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $subpubem_title,
                        'name' => 'subpubem',
                        'id' => 'subpubem',
                        'lang' => TRUE,
                        'desc' => $subpubem_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_abuserev,
                        'name' => 'is_abuserev'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_abuserev,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $abuserev_title,
                        'name' => 'abuserev'.$prefix_review,
                        'id' => 'abuserev'.$prefix_review,
                        'lang' => TRUE,
                        'desc' => $abuserev_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_revvouc,
                        'name' => 'is_revvouc'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_revvouc,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $revvouc_title,
                        'name' => 'revvouc'.$prefix_review,
                        'id' => 'revvouc'.$prefix_review,
                        'lang' => TRUE,
                        'desc' => $revvouc_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_facvouc,
                        'name' => 'is_facvouc'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),

                        'desc' => $is_facvouc,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $facvouc_title,
                        'name' => 'facvouc'.$prefix_review,
                        'id' => 'facvouc'.$prefix_review,
                        'lang' => TRUE,
                        'desc' => $facvouc_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_sugvouc,
                        'name' => 'is_sugvouc'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $is_sugvouc,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $sugvouc_title,
                        'name' => 'sugvouc'.$prefix_review,
                        'id' => 'sugvouc'.$prefix_review,
                        'lang' => TRUE,
                        'desc' => $sugvouc_desc,
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'responseadminemailssettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesResponseadminemailsSettings(array('prefix_review'=>$prefix_review)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        return  $helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesResponseadminemailsSettings($data){

        $prefix_review = $data['prefix_review'];

        $languages = Language::getLanguages(false);
        $fields_subresem = array();
        $fields_textresem = array();
        $fields_emailreminder = array();
        $fields_reminderok = array();
        $fields_thankyou = array();
        $fields_newrev = array();
        $fields_subpubem = array();
        $fields_modrev = array();
        $fields_abuserev = array();
        $fields_facvouc = array();
        $fields_revvouc = array();
        $fields_sugvouc = array();

        foreach ($languages as $lang)
        {
            $fields_subresem[$lang['id_lang']] =  Configuration::get($this->_name.'subresem_'.$lang['id_lang']);
            $fields_textresem[$lang['id_lang']] =  Configuration::get($this->_name.'textresem_'.$lang['id_lang']);
            $fields_emailreminder[$lang['id_lang']] =  Configuration::get($this->_name.'emailreminder_'.$lang['id_lang']);
            $fields_reminderok[$lang['id_lang']] =  Configuration::get($this->_name.'reminderok'.$prefix_review.'_'.$lang['id_lang']);
            $fields_thankyou[$lang['id_lang']] =  Configuration::get($this->_name.'thankyou'.$prefix_review.'_'.$lang['id_lang']);
            $fields_newrev[$lang['id_lang']] =  Configuration::get($this->_name.'newrev'.$prefix_review.'_'.$lang['id_lang']);
            $fields_subpubem[$lang['id_lang']] =  Configuration::get($this->_name.'subpubem_'.$lang['id_lang']);
            $fields_modrev[$lang['id_lang']] =  Configuration::get($this->_name.'modrev'.$prefix_review.'_'.$lang['id_lang']);
            $fields_abuserev[$lang['id_lang']] =  Configuration::get($this->_name.'abuserev'.$prefix_review.'_'.$lang['id_lang']);
            $fields_facvouc[$lang['id_lang']] =  Configuration::get($this->_name.'facvouc'.$prefix_review.'_'.$lang['id_lang']);
            $fields_revvouc[$lang['id_lang']] =  Configuration::get($this->_name.'revvouc'.$prefix_review.'_'.$lang['id_lang']);
            $fields_sugvouc[$lang['id_lang']] =  Configuration::get($this->_name.'sugvouc'.$prefix_review.'_'.$lang['id_lang']);
        }

        $data_config = array(
            'subresem' => $fields_subresem,
            'textresem' => $fields_textresem,
            'emailreminder' => $fields_emailreminder,
            'reminderok'.$prefix_review => $fields_reminderok,
            'thankyou'.$prefix_review => $fields_thankyou,
            'newrev'.$prefix_review => $fields_newrev,
            'subpubem' =>$fields_subpubem,
            'modrev'.$prefix_review => $fields_modrev,
            'abuserev'.$prefix_review => $fields_abuserev,

            'facvouc'.$prefix_review => $fields_facvouc,
            'revvouc'.$prefix_review => $fields_revvouc,
            'sugvouc'.$prefix_review => $fields_sugvouc,

            'is_emailreminder' => Configuration::get($this->_name.'is_emailreminder'),
            'is_reminderok'.$prefix_review => Configuration::get($this->_name.'is_reminderok'.$prefix_review),
            'is_thankyou'.$prefix_review => Configuration::get($this->_name.'is_thankyou'.$prefix_review),
            'is_newrev'.$prefix_review => Configuration::get($this->_name.'is_newrev'.$prefix_review),
            'is_subresem' => Configuration::get($this->_name.'is_subresem'),
            'is_modrev'.$prefix_review => Configuration::get($this->_name.'is_modrev'.$prefix_review),
            'is_subpubem' => Configuration::get($this->_name.'is_subpubem'),
            'is_abuserev'.$prefix_review => Configuration::get($this->_name.'is_abuserev'.$prefix_review),
            'is_revvouc'.$prefix_review => Configuration::get($this->_name.'is_revvouc'.$prefix_review),
            'is_facvouc'.$prefix_review => Configuration::get($this->_name.'is_facvouc'.$prefix_review),
            'is_sugvouc'.$prefix_review => Configuration::get($this->_name.'is_sugvouc'.$prefix_review),

        );

        return $data_config;
    }



    public function customerreminder($data){

        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];
        $prefix_review = $data['prefix_review'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $title_main = $data['title_main'];
        $token_cron = $data['token_cron'];
        $url_multishop = $data['url_multishop'];

        $reminder_title = $data['reminder_title'];
        $reminder_desc1 = $data['reminder_desc1'];
        $reminder_desc2 = $data['reminder_desc2'];
        $reminder_desc3 = $data['reminder_desc3'];
        $reminder_desc4 = $data['reminder_desc4'];
        $reminder_desc5 = $data['reminder_desc5'];
        $crondelay_title = $data['crondelay_title'];
        $crondelay_desc = $data['crondelay_desc'];
        $cronnpost_title = $data['cronnpost_title'];
        $cronnpost_desc = $data['cronnpost_desc'];
        $delay_title = $data['delay_title'];
        $delay_desc = $data['delay_desc'];
        $orders_import_storereviews_title = $data['orders_import_storereviews_title'];
        $orders_import_storereviews_desc = $data['orders_import_storereviews_desc'];
        $sel_statuses_title = $data['sel_statuses_title'];
        $sel_statuses_desc = $data['sel_statuses_desc'];
        $remrevsec_title = $data['remrevsec_title'];
        $remindersec_title = $data['remindersec_title'];
        $remindersec_desc = $data['remindersec_desc'];
        $delaysec_title = $data['delaysec_title'];
        $update_title = $data['update_title'];


        include_once(_PS_MODULE_DIR_ . $this->_name.'/classes/featureshelp.class.php');
        $obj_featureshelp = new featureshelp();
        $cookie = $this->context->cookie;
        $id_lang = (int)($cookie->id_lang);

        require_once(_PS_MODULE_DIR_ . $this->_name . '/classes/spmgsnipreviewhelp.class.php');
        $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

        $data_seo_url = $obj_spmgsnipreviewhelp->getSEOURLs();
        $reviews_admin_url = $data_seo_url['reviews_admin_url'];

        $delimeter_rewrite = "&";
        if(Configuration::get('PS_REWRITING_SETTINGS')){
            $delimeter_rewrite = "?";
        }
        $url_cron = $data_seo_url['cron_url'];


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/customerreminder_desc1.phtml');
        $desc1 = ob_get_clean();


        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $title_main,
                    'icon' => 'fa fa-bell-o fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $reminder_title,
                        'name' => 'reminder',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $reminder_desc1.$desc1,
                    ),
                    array(
                        'type' => 'text_custom_delay',
                        'label' => $crondelay_title,
                        'name' => 'crondelay'.$prefix_review,
                        'value'=>(int)Configuration::get($this->_name.'crondelay'.$prefix_review),
                        'desc' => $crondelay_desc,
                    ),
                    array(
                        'type' => 'text',
                        'label' => $cronnpost_title,
                        'name' => 'cronnpost'.$prefix_review,
                        'desc' => $cronnpost_desc,
                    ),
                    array(
                        'type' => 'text_custom_delay_reminder',
                        'label' => $delay_title,
                        'name' => 'delay',
                        'value'=> Configuration::get($this->_name.'delay'),
                        'desc'=>$delay_desc
                    ),
                    array(
                        'type' => 'text_custom_orders_import',
                        'label' => $orders_import_storereviews_title,
                        'name' => 'orders_import',
                        'end_date' =>date('Y-m-d H:i:s'),
                        'host_url'=>$url_multishop,
                        'reviews_admin_url'=>$reviews_admin_url,
                        'desc'=>$orders_import_storereviews_desc
                    ),
                    array(
                        'type' => 'text_custom_order_statuses',
                        'label' => $sel_statuses_title,
                        'name' => 'sel_statuses',
                        'value'=> $obj_featureshelp->getOrderStatuses(array('id_lang'=>$id_lang)),
                        'orderstatuses'=> explode(",",Configuration::get($this->_name.'orderstatuses')),
                        'desc'=>$sel_statuses_desc
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $remrevsec_title,
                        'name' => 'remrevsec'.$prefix_review,

                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $remindersec_title,
                        'name' => 'remindersec'.$prefix_review,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                        'desc' => $remindersec_desc,
                    ),
                    array(
                        'type' => 'text_custom_delay_reminder',
                        'label' => $delaysec_title,
                        'name' => 'delaysec'.$prefix_review,
                        'value'=> Configuration::get($this->_name.'delaysec'.$prefix_review),
                    ),
                ),
            ),
        );

        $fields_form1 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'customerremindersettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesCustomerreminderSettings(array('prefix_review'=>$prefix_review)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );

        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_customerreminder_js.phtml');
        $_html = ob_get_clean();

        return  $_html.$helper->generateForm(array($fields_form,$fields_form1));
    }

    public function getConfigFieldsValuesCustomerreminderSettings($data){
        $prefix_review = $data['prefix_review'];

        $data_config = array(
            'reminder' => Configuration::get($this->_name.'reminder'),
            'remindersec'.$prefix_review => Configuration::get($this->_name.'remindersec'.$prefix_review),
            'remrevsec'.$prefix_review => Configuration::get($this->_name.'remrevsec'.$prefix_review),
            'cronnpost'.$prefix_review=>(int)Configuration::get($this->_name.'cronnpost'.$prefix_review),
        );
        return $data_config;
    }


    public function installTable()
    {

        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview TO '._DB_PREFIX_.'spmgsnipreview';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_data_order TO '._DB_PREFIX_.'spmgsnipreview_data_order';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_customer TO '._DB_PREFIX_.'spmgsnipreview_customer';
            if (!Db::getInstance()->Execute($sql))
                return false;


            $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview`');
            if (is_array($list_fields))
            {
                foreach ($list_fields as $k => $field)
                    $list_fields[$k] = $field['Field'];
                if (!in_array('is_new', $list_fields)) {
                    if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview` ADD `is_new` int(11) NOT NULL default \'0\'')) {
                        return false;
                    }

                }
            }


        } else {

            $db = Db::getInstance();

            $query = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview` (
							  `id` int(11) NOT NULL auto_increment,
							  `id_product` int(11) NOT NULL,
							  `id_customer` int(11) NOT NULL default \'0\',
							  `customer_name` varchar(500) default NULL,
							  `title_review` varchar(5000) default NULL,
							  `text_review` text,
							  `rating` int(11) NOT NULL default \'0\',

							  `title_review_old` varchar(5000) default NULL,
							  `text_review_old` text,
							  `rating_old` text,
							  `admin_response` text,
							  `is_changed` int(11) NOT NULL default \'0\',
							  `is_display_old` int(11) NOT NULL default \'0\',
							  `is_count_sending_suggestion` int(11) NOT NULL default \'0\',
							  `review_date_update` timestamp DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
							  `avatar` text,
							  `email` VARCHAR(255) NOT NULL,
							  `ip` varchar(255) default NULL,
							  `id_shop` int(11) NOT NULL default \'0\',
							  `id_lang` int(11) NOT NULL default \'0\',
							  `is_abuse` int(11) NOT NULL default \'0\',
							  `is_active` int(11) NOT NULL default \'1\',
							  `time_add` timestamp NULL,
							  `is_import` int(11) NOT NULL default \'0\',
							  `is_new` int(11) NOT NULL default \'0\',
							  PRIMARY KEY  (`id`),
							  KEY `id_product` (`id_product`),
							  KEY `id_customer` (`id_customer`)
							) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';
            $db->Execute($query);

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_data_order` (
					  `id` int(10) NOT NULL AUTO_INCREMENT,
					  `id_shop` int(11) NOT NULL default \'0\',
					  `order_id` int(10) NOT NULL,
					  `date_add` timestamp NOT NULL DEFAULT \'0000-00-00 00:00:00\',
					  `status` int(10) NOT NULL default \'0\',
					  `customer_id` int(11) NOT NULL default \'0\',
					  `data` text,
					  `date_send` timestamp NULL,
					  `date_send_second` timestamp NULL,
                      `count_sent` int(10) NOT NULL default \'0\',
					  PRIMARY KEY (`id`)
					) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';
            $db->Execute($sql);

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_customer` (
						  `id_shop` int(11) NOT NULL default \'0\',
						  `customer_id` int(11) NOT NULL default \'0\',
						  `status` int(10) NOT NULL default \'0\',
						   KEY (`id_shop`,`customer_id`),
						  KEY `shop_status` (`id_shop`,`status`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';
            $db->Execute($sql);

        }

        return true;
    }


    public function installSocialShare(){


        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_socialshare TO '._DB_PREFIX_.'spmgsnipreview_socialshare';
            if (!Db::getInstance()->Execute($sql))
                return false;


        } else {

            $db = Db::getInstance();

            $sql_coupon = '
				CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_socialshare` (
					`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
					`id_discount` INT( 11 ) NOT NULL ,
					`ip_adress` VARCHAR(255) NOT NULL,
					`id_guest` INT( 11 ) NOT NULL ,
					`id_customer` INT( 11 ) NOT NULL ,
					`id_review` INT( 11 ) NOT NULL ,
					`type` int(11) NOT NULL default \'0\'
					  COMMENT \'1 - Facebook, 2 - Google  \',
				    INDEX (`id_discount`)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;
				';
            $db->Execute($sql_coupon);


        }
        return true;
    }

    public function installReviewHelpfull(){

        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_review_helpfull TO '._DB_PREFIX_.'spmgsnipreview_review_helpfull';
            if (!Db::getInstance()->Execute($sql))
                return false;


        } else {
            $db = Db::getInstance();
            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_review_helpfull` (
						  `id_customer` int(11)  NOT NULL,
                          `review_id` int(11) NOT NULL,
                          `is_guest` int(11) NOT NULL default \'0\',
                          `ip` varchar(255) default NULL,
                          `helpfull` tinyint(1) NOT NULL,
                          KEY (`id_customer`,`review_id`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);
        }
        return true;
    }

    public function installReviewAbuse(){
        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_review_abuse TO '._DB_PREFIX_.'spmgsnipreview_review_abuse';
            if (!Db::getInstance()->Execute($sql))
                return false;


        } else {
            $db = Db::getInstance();
            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_review_abuse` (
						  `review_id` int UNSIGNED NOT NULL DEFAULT \'0\',
                          `id_customer` INT UNSIGNED NOT NULL,
                          `name` VARCHAR(500)  default NULL ,
                          `email` VARCHAR(255) default NULL ,
                          `text_abuse` text,
                          `is_customer` int(11) NOT NULL default \'0\',
                          KEY (`review_id`,`id_customer`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);
        }
        return true;
    }

    public function installCriteriaTable(){
        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_review_criterion TO '._DB_PREFIX_.'spmgsnipreview_review_criterion';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_review_criterion_lang TO '._DB_PREFIX_.'spmgsnipreview_review_criterion_lang';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_review2criterion TO '._DB_PREFIX_.'spmgsnipreview_review2criterion';
            if (!Db::getInstance()->Execute($sql))
                return false;



            $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion`');
            if (is_array($list_fields))
            {
                foreach ($list_fields as $k => $field)
                    $list_fields[$k] = $field['Field'];
                // change filed with same name , not !in_array(), use in_array
                if (in_array('id_gsnipreview_review_criterion', $list_fields)) {
                    if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion` CHANGE `id_gsnipreview_review_criterion` `id_spmgsnipreview_review_criterion` int(10) unsigned NOT NULL auto_increment;')) {
                        return false;
                    }

                }
            }


            $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_review_criterion_lang`');
            if (is_array($list_fields))
            {
                foreach ($list_fields as $k => $field)
                    $list_fields[$k] = $field['Field'];
                // change filed with same name , not !in_array(), use in_array
                if (in_array('id_gsnipreview_review_criterion', $list_fields)) {
                    if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion_lang` CHANGE `id_gsnipreview_review_criterion` `id_spmgsnipreview_review_criterion` INT(11) UNSIGNED NOT NULL;')) {
                        return false;
                    }

                }
            }


        } else {

            $db = Db::getInstance();
            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion` (
						  `id_spmgsnipreview_review_criterion` int(10) unsigned NOT NULL auto_increment,
						  `id_shop` varchar(1024) NOT NULL default \'0\',
						  id_product text,
						  `id_category` text,
						  `is_category` int(11) NOT NULL default \'0\',
                          `active` tinyint(1) NOT NULL,
                          PRIMARY KEY (`id_spmgsnipreview_review_criterion`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_review_criterion_lang` (
						  `id_spmgsnipreview_review_criterion` INT(11) UNSIGNED NOT NULL ,
                          `id_lang` INT(11) UNSIGNED NOT NULL ,
                          `name` VARCHAR(255) NOT NULL ,
                          `description` text,
                          KEY ( `id_spmgsnipreview_review_criterion` , `id_lang` )
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_review2criterion` (
						  `id_review` int(10) unsigned NOT NULL,
                          `id_criterion` int(10) unsigned NOT NULL,
                          `rating` INT(11) NOT NULL DEFAULT \'0\',
                           KEY(`id_review`, `id_criterion`),
                          KEY `id_criterion` (`id_criterion`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);
        }

        return true;
    }


    public function installReminder2CustomerTable(){

        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_reminder2customer TO '._DB_PREFIX_.'spmgsnipreview_reminder2customer';
            if (!Db::getInstance()->Execute($sql))
                return false;


        } else {
            $db = Db::getInstance();

            $sql = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_reminder2customer` (
						  `id_customer` int(10) unsigned NOT NULL,
                          `id_shop` int(10) NOT NULL DEFAULT \'0\',
                          `status` INT(11) NOT NULL DEFAULT \'0\',
                           KEY `id_c2id_shop` (`id_customer`, `id_shop`),
                          KEY `id_customer` (`id_shop`)
						) ENGINE=' . (defined(_MYSQL_ENGINE_) ? _MYSQL_ENGINE : "MyISAM") . ' DEFAULT CHARSET=utf8;';

            $db->Execute($sql);
        }
        return true;
    }

    public function installUserTable(){

        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_avatar2customer TO '._DB_PREFIX_.'spmgsnipreview_avatar2customer';
            if (!Db::getInstance()->Execute($sql))
                return false;


        } else {
            $db = Db::getInstance();
            $query = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_avatar2customer` (
							  `id_customer` int(11) NOT NULL,
							  `avatar_thumb` text,
							  `is_show` int(11) NOT NULL default \'1\',
							  KEY `id_customer` (`id_customer`)
							) ENGINE=' . (defined('_MYSQL_ENGINE_') ? _MYSQL_ENGINE_ : "MyISAM") . ' DEFAULT CHARSET=utf8';
            $db->Execute($query);

        }

        return true;
    }

    public function installFiles2ReviewTable(){

        if(Module::isInstalled('gsnipreview')){
            $sql = 'RENAME TABLE '._DB_PREFIX_.'gsnipreview_files2review TO '._DB_PREFIX_.'spmgsnipreview_files2review';
            if (!Db::getInstance()->Execute($sql))
                return false;

            $list_fields = Db::getInstance()->executeS('SHOW FIELDS FROM `'._DB_PREFIX_.'spmgsnipreview_files2review`');
            if (is_array($list_fields))
            {
                foreach ($list_fields as $k => $field)
                    $list_fields[$k] = $field['Field'];
                // change filed with same name , not !in_array(), use in_array
                if (in_array('id_gsnipreview_files2review', $list_fields)) {
                    if (!Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'spmgsnipreview_files2review` CHANGE `id_gsnipreview_files2review` `id_spmgsnipreview_files2review` int(10) unsigned NOT NULL auto_increment;')) {
                        return false;
                    }

                }
            }


            $sql = 'SELECT * FROM `'._DB_PREFIX_.'spmgsnipreview_files2review`';
            $items = Db::getInstance()->ExecuteS($sql);

            foreach($items as $_item){

                $id_spmgsnipreview_files2review = $_item['id_spmgsnipreview_files2review'];
                $full_path = str_replace("gsnipreview","spmgsnipreview",$_item['full_path']);
                $sql = 'UPDATE `'._DB_PREFIX_.'spmgsnipreview_files2review` SET
					     `full_path`="'.pSQL($full_path).'"
		                WHERE id_spmgsnipreview_files2review = '.(int)($id_spmgsnipreview_files2review).'';
                Db::getInstance()->Execute($sql);
            }


        } else {
            $db = Db::getInstance();
            $query = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'spmgsnipreview_files2review` (
                              `id_spmgsnipreview_files2review` int(10) unsigned NOT NULL auto_increment,
							  `id_review` int(11) NOT NULL,
							  `full_path` text,
							 PRIMARY KEY (`id_spmgsnipreview_files2review`)
							) ENGINE=' . (defined('_MYSQL_ENGINE_') ? _MYSQL_ENGINE_ : "MyISAM") . ' DEFAULT CHARSET=utf8';
            $db->Execute($query);


        }
        return true;
    }

    public function uninstallTable() {
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_data_order');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_customer');

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_review_criterion');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_review_criterion_lang');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_review2criterion');

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_review_abuse');

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_review_helpfull');

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_socialshare');

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_reminder2customer');

        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_avatar2customer');
        Db::getInstance()->Execute('DROP TABLE IF EXISTS '._DB_PREFIX_.'spmgsnipreview_files2review');



    }
}