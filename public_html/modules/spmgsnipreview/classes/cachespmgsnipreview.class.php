<?php
/**
 * 2012 - 2018 spm LLC.
 *
 * MODULE spmgsnipreview
 *
 * @author    spm <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, spm
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class cachespmgsnipreview extends Module{



    public function clearSmartyCacheModule($data = null){

        $name_module = "spmgsnipreview";


        if(version_compare(_PS_VERSION_, '1.6', '>')) {

            $id_product_current = isset($data['id_product'])?$data['id_product']:0;
            $is_shop_reviews = isset($data['is_shop_reviews'])?$data['is_shop_reviews']:0;

            $skip_files = array(".","..","index.php",".DS_Store");

            // !!! only hooks, which appears on the product page !!! //
            $product_page_files = array("liststars.tpl","producttabcontent.tpl","producttabcontent17.tpl","reviewsblockextraleft.tpl","reviewsblockextraright.tpl",
                                        "reviewsblockproductactions.tpl","reviewsblockproductfooter.tpl","tab.tpl");
            // !!! only hooks, which appears on the product page !!! //



            // clear cache for hooks //
            $template_path_hooks = "views".DIRECTORY_SEPARATOR."templates".DIRECTORY_SEPARATOR."hooks".DIRECTORY_SEPARATOR;
            $dir_hooks = _PS_MODULE_DIR_.$name_module.DIRECTORY_SEPARATOR."views".DIRECTORY_SEPARATOR."templates".DIRECTORY_SEPARATOR."hooks";

            $dh  = opendir($dir_hooks);
            while (false !== ($filename = readdir($dh))) {

                if(in_array($filename,$skip_files))
                    continue;

                $cache_id = $name_module.'|'.$filename;
                $template_name = $template_path_hooks.$filename;



                if(in_array($filename,$product_page_files)){


                    if(!$is_shop_reviews) { // exclude shop reviews


                        if ($id_product_current) {

                            $cache_id = $name_module .'|' . $filename.$id_product_current;
                            $this->_clearCache($template_name, $cache_id);

                        } else {
                            /*$cookie = Context::getContext()->cookie;
                            $id_lang = (int)($cookie->id_lang);
                            $productObj = new Product();
                            $products = $productObj->getProducts($id_lang, 0, 0, 'id_product', 'DESC');
                            foreach ($products as $product) {
                                $id_product = isset($product['id_product']) ? $product['id_product'] : 0;

                                $cache_id = $name_module . $filename . $id_product;

                                $this->_clearCache($template_name, $cache_id);
                            }*/
                        }


                    }


                } else {
                    $this->_clearCache($template_name, $cache_id);
                }



            }


            // clear cache on the home page only for ps 17 //
            if(version_compare(_PS_VERSION_, '1.7', '>')) {
                $template_name = "module:ps_featuredproducts/views/templates/hook/ps_featuredproducts.tpl";
                $cache_id = "ps_featuredproducts";
                $this->_clearCache($template_name, $cache_id);
            }
            // clear cache on the home page only for ps 17 //






            // clear cache for hooks //


        }


    }




}