<?php
/**
 * 2012 - 2018 SPM
 *
 * MODULE spmgsnipreview
 *
 * @author    SPM <spm.presto@gmail.com>
 * @copyright Copyright (c) permanent, SPM
 * @license   Addons PrestaShop license limitation
 * @version   1.6.0
 * @link      https://addons.prestashop.com/en/2_community-developer?contributor=790166
 *
 * NOTICE OF LICENSE
 *
 * Don't use this module on several shops. The license provided by PrestaShop Addons
 * for all its modules is valid only once for a single shop.
 */

class userprofileg extends Module {
	
	private $_step;
	private $_http;

    private $_name;
    private $_http_host;
    private $_is_cloud;
    private $_prefix = "r";
    private $_id_shop;
	
	
	public function __construct(){
		parent::__construct();

        $this->_name = "spmgsnipreview";


		$this->_step = (int)Configuration::get($this->_name.$this->_prefix.'page_shoppers');
		$this->_http = $this->_http();


        $this->_http_host = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;

        $this->_id_shop = Context::getContext()->shop->id;


        if (defined('_PS_HOST_MODE_'))
            $this->_is_cloud = 1;
        else
            $this->_is_cloud = 0;


        // for test
        //$this->_is_cloud = 1;
        // for test

        if($this->_is_cloud){
            $this->path_img_cloud = DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR;
        } else {
            $this->path_img_cloud = DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."upload".DIRECTORY_SEPARATOR.$this->_name.DIRECTORY_SEPARATOR;

        }

		

		
		$this->initContext();
	}
	
	private function initContext()
	{
		$this->context = Context::getContext();
	}

    public function getObjectParent(){
        include_once(_PS_MODULE_DIR_.$this->_name .'/spmgsnipreview.php');
        $obj = new spmgsnipreview();
        return $obj;
    }


    public function getShoppersList($data = null){
		
		
		$start = $data['start'];
		$step = isset($data['step'])?$data['step']:$this->_step;

		$search = isset($data['search'])?$data['search']:0;
        $is_search = isset($data['is_search'])?$data['is_search']:0;

        $sql_shop = '';
        if($this->_id_shop){
            $sql_shop .= ' AND pc.id_shop = '.(int)$this->_id_shop;
        }

        $sql_condition_search = '';
        if($is_search == 1){
            $sql_condition_search = " AND
                    (
                    LOWER(pc.lastname) LIKE BINARY LOWER('%".pSQL($search)."%')
                      OR
                     LOWER(pc.firstname) LIKE BINARY LOWER('%".pSQL($search)."%')
                    )  ";
        }


		$sql = '
		SELECT pc.*, a2c.avatar_thumb
		FROM `'._DB_PREFIX_.'customer` pc LEFT JOIN `'._DB_PREFIX_.'spmgsnipreview_avatar2customer` a2c
		on(a2c.id_customer = pc.id_customer) 
		WHERE pc.active = 1 AND pc.deleted = 0 '.$sql_shop.' AND (a2c.`is_show` = 1 OR a2c.`is_show` IS NULL) '.$sql_condition_search.'
		ORDER BY pc.`id_customer` ASC LIMIT '.(int)$start.' ,'.(int)$step.'';


		$customers = Db::getInstance()->ExecuteS($sql);


		$i=0;
		foreach($customers as $_item_customer){
			
			$avatar_thumb = $_item_customer['avatar_thumb'];
			$id_gender = $_item_customer['id_gender'];
			$id_customer = $_item_customer['id_customer'];
			
			// addresses
			$info_addresses = $this->_getAddresses(array('id_customer'=>$id_customer));
			$address_item = end($info_addresses['multipleAddressesFormated']);
			$customers[$i]['country'] = @$address_item['country'];
			
			// user with avatar
			$info_path = $this->getAvatarPath(array('id_gender' => $id_gender,'avatar' => $avatar_thumb, 'id_customer' =>$id_customer,'is_user'=>1 ));


			$customers[$i]['avatar_thumb'] = $info_path['avatar'];
			$customers[$i]['exist_avatar'] = $info_path['is_exist'];
			
			$i++;
		}
		
		$data_count_customers = Db::getInstance()->getRow('
		SELECT COUNT(pc.id_customer) AS "count"
		FROM `'._DB_PREFIX_.'customer` pc LEFT JOIN `'._DB_PREFIX_.'spmgsnipreview_avatar2customer` a2c
		on(a2c.id_customer = pc.id_customer) 
		WHERE pc.active = 1 AND pc.deleted = 0 '.$sql_shop.' AND (a2c.`is_show` = 1 OR a2c.`is_show` IS NULL) '.$sql_condition_search.'
		');
		

		
		return array('customers' => $customers, 
					'data_count_customers' => $data_count_customers['count'],
					);
	}
	
	
	public function getShoppersBlock($data = null){

        $sql_shop = '';
        if($this->_id_shop){
            $sql_shop .= ' AND pc.id_shop = '.(int)$this->_id_shop;
        }
		
		$start = $data['start'];
		$step = isset($data['step'])?$data['step']:$this->_step;
		
		$sql_customer = '
		SELECT pc.*, a2c.avatar_thumb
		FROM `'._DB_PREFIX_.'customer` pc LEFT JOIN `'._DB_PREFIX_.'spmgsnipreview_avatar2customer` a2c
		on(a2c.id_customer = pc.id_customer)
		WHERE pc.active = 1 AND pc.deleted = 0 '.$sql_shop.' AND (a2c.`is_show` = 1 OR a2c.`is_show` IS NULL)
		ORDER BY RAND()
		LIMIT '.(int)$start.' ,'.(int)$step.'';


		$customers = Db::getInstance()->ExecuteS($sql_customer);
		$i=0;
		foreach($customers as $_item_customer){
			
			$avatar_thumb = $_item_customer['avatar_thumb'];
			$id_gender = $_item_customer['id_gender'];
            $id_customer = $_item_customer['id_customer'];
			
			// user with avatar
			$info_path = $this->getAvatarPath(array('id_gender' => $id_gender,'avatar' => $avatar_thumb, 'id_customer' =>$id_customer,'is_user'=>1  ));
			
			$customers[$i]['avatar_thumb'] = $info_path['avatar'];
			$customers[$i]['exist_avatar'] = $info_path['is_exist'];
			
			$i++;
		}
		
		
		return array('customers' => $customers
					);
	}
	

	
	public function getShopperInfo($data = null){
		
		$cookie = $this->context->cookie;
		
		$shopper_id = isset($data['shopper_id'])?(int)$data['shopper_id']:0;

        $sql_shop = '';
        if($this->_id_shop){
            $sql_shop .= ' AND pc.id_shop = '.(int)$this->_id_shop;
        }


        $customers = Db::getInstance()->ExecuteS('
		SELECT pc.*, a2c.avatar_thumb
		FROM `'._DB_PREFIX_.'customer` pc LEFT JOIN `'._DB_PREFIX_.'spmgsnipreview_avatar2customer` a2c
		on(a2c.id_customer = pc.id_customer) 
		WHERE pc.active = 1 AND pc.deleted = 0 '.$sql_shop.' AND (a2c.`is_show` = 1 OR a2c.`is_show` IS NULL)
		 AND pc.id_customer = '.(int)$shopper_id);	
		$i=0;
		foreach($customers as $_item_customer){
			
			$avatar_thumb = $_item_customer['avatar_thumb'];
			$id_gender = $_item_customer['id_gender'];
			$id_customer = $_item_customer['id_customer'];
			
			// addresses
			$info_addresses = $this->_getAddresses(array('id_customer'=>$id_customer));
			$customers[$i]['addresses'] = $info_addresses['multipleAddressesFormated']; 
			$address_item = end($info_addresses['multipleAddressesFormated']);
			$customers[$i]['country'] = @$address_item['country'];
			
			// user with avatar
			$info_path = $this->getAvatarPath(array('id_gender' => $id_gender,'avatar' => $avatar_thumb, 'id_customer' =>$id_customer,'is_user'=>1  ));

			$customers[$i]['avatar_thumb'] = $info_path['avatar'];
			$customers[$i]['exist_avatar'] = $info_path['is_exist'];
			$customers[$i]['gender_txt'] = $info_path['gender_txt'];
			
			
			// load stats for customer
			$customer_obj = new Customer($id_customer);
			$stats = $customer_obj->getStats();
			$stats_tmp = array();
			foreach($stats as $_key_stat => $_item_stat){
				switch($_key_stat) {
					case 'last_visit':
						$_item_stat = ($_item_stat ? @Tools::displayDate($_item_stat, (int)($cookie->id_lang), true) : $this->l('never'));
					break;
					
				}
				$stats_tmp[$_key_stat] = $_item_stat; 
			}
			$customers[$i]['stats'] = $stats;
			
			
			$i++;
		}
		
		
		
		return array('customer' => $customers);
	}
	
	private function _getAddresses($data){
			$cookie = $this->context->cookie;
		
			$id_customer = $data['id_customer'];
			// adresses
			$customer = new Customer($id_customer);
			$customerAddressesDetailed = $customer->getAddresses($cookie->id_lang);
		
			return array('multipleAddressesFormated' => $customerAddressesDetailed);
	}

    public function getAvatarPath($data){


        $avatar_thumb = $data['avatar'];

        $is_exists = 0;
        $gender_txt = '';


        $is_user_functional = isset($data['is_user'])?$data['is_user']:0;

        $id_customer = $data['id_customer'];
        $is_show = 1;

        if($id_customer){
            $customer_data = $this->getCustomerInfo(array('id_customer' => $id_customer));
            $id_gender = $customer_data['id_gender'];

            $query = 'SELECT avatar_thumb, is_show from '._DB_PREFIX_.''.$this->_name.'_avatar2customer
												WHERE id_customer = '.(int)$id_customer;

            $result = Db::getInstance()->ExecuteS($query);


            $avatar = isset($result[0]['avatar_thumb'])?$result[0]['avatar_thumb']:'';
            $is_show = isset($result[0]['is_show'])?$result[0]['is_show']:1;
            if(Tools::strlen($avatar)){
                $avatar_thumb = $avatar;

            }


        } else {
            $id_gender = 0;
        }




        // user with avatar

        $obj = $this->getObjectParent();
        $http = $obj->getHttpost();

        $_data_translate = $obj->translateCustom();
        $male = $_data_translate['male'];
        $female = $_data_translate['female'];


        if(Tools::strlen($avatar_thumb)>0){

            if($this->_is_cloud){
                $path_img_cloud = "modules/".$this->_name."/upload/" . $this->_name . "/avatar/";
            } else {
                $path_img_cloud = "upload/" . $this->_name . "/avatar/";

            }


            $avatar_thumb =  $http.$path_img_cloud.$avatar_thumb;
            $is_exists = 1;

            switch($id_gender){
                case 1:
                    //male
                    $gender_txt =$male;
                    break;
                case 2:
                    //female
                    $gender_txt = $female;
                    break;
            }

        } else {

            if($is_show || $is_user_functional) {
                // user without avatar
                switch ($id_gender) {
                    case 1:
                        //male
                        $avatar_thumb = $http . "modules/" . $this->_name . "/views/img/avatar_m.gif";
                        $gender_txt =$male;
                        break;
                    case 2:
                        //female
                        $avatar_thumb = $http . "modules/" . $this->_name . "/views/img/avatar_w.gif";
                        $gender_txt = $female;
                        break;
                    default:
                        //unknown
                        //$avatar_thumb = $http . "modules/" . $this->_name . "/views/img/avatar_n.gif";
                        $avatar_thumb = $http . "modules/" . $this->_name . "/views/img/avatar_m.gif";
                        break;
                }
            }
        }
        return array('avatar'=> $avatar_thumb, 'is_exist' => $is_exists,'is_show'=>$is_show, 'gender_txt'=>$gender_txt);
    }




    public function saveImageAvatar($data = null){




        $files = Tools::fileAttachment('avatar-review');
        $id_customer = $data['id_customer'];

        $show_my_profile = isset($data['show_my_profile'])?$data['show_my_profile']:2;


        $item_id = isset($data['id'])?$data['id']:0;
        $post_images = isset($data['post_images'])?$data['post_images']:'';

        $is_storereviews = isset($data['is_storereviews'])?$data['is_storereviews']:0;



        ############### files ###############################
        if(!empty($files['name']))
        {

            if(!$files['error'])
            {

                include_once(_PS_MODULE_DIR_.$this->_name  . '/classes/spmgsnipreviewhelp.class.php');
                $obj_spmgsnipreviewhelp = new spmgsnipreviewhelp();

                if($item_id) {

                    if($is_storereviews){

                        include_once(_PS_MODULE_DIR_.$this->_name  . '/classes/storereviews.class.php');
                        $obj_storereviews = new storereviews();

                        $info_post = $obj_storereviews->getItem(array('id' => $item_id));
                        $post_item = $info_post['reviews'][0];
                        $img_post = $post_item['avatar'];

                    } else {
                        $info_post = $obj_spmgsnipreviewhelp->getItem(array('id' => $item_id));
                        $post_item = $info_post['reviews'][0];
                        $img_post = $post_item['avatar'];
                    }

                } else {
                    $_info = $this->getCustomerInfo();
                    $img_post = isset($_info['avatar_thumb']) ? $_info['avatar_thumb'] : '';
                }


                if(Tools::strlen($img_post)>0){

                    // delete old avatars
                    $name_thumb = explode("/",$img_post);
                    $name_thumb = end($name_thumb);

                    @unlink(dirname(__FILE__).$this->path_img_cloud."avatar".DIRECTORY_SEPARATOR.$name_thumb);


                }


                $type_one = $files['mime'];


                srand((double)microtime()*1000000);
                $uniq_name_image = uniqid(rand());
                $type_one = Tools::substr($type_one,6,Tools::strlen($type_one)-6);
                $filename = $uniq_name_image.'.'.$type_one;



                move_uploaded_file($files['tmp_name'], dirname(__FILE__).$this->path_img_cloud."avatar".DIRECTORY_SEPARATOR.$filename);



                $obj_spmgsnipreviewhelp->copyImage(array('dir_without_ext'=>dirname(__FILE__).$this->path_img_cloud."avatar".DIRECTORY_SEPARATOR.$uniq_name_image,
                        'name'=>dirname(__FILE__).$this->path_img_cloud."avatar".DIRECTORY_SEPARATOR.$filename)
                );


                $this->saveAvatar(array(
                        'avatar' => $uniq_name_image.'.jpg',
                        'id'=>isset($data['id'])?$data['id']:0,
                        'id_customer'=>$id_customer,
                        'show_my_profile' => $show_my_profile,

                        'is_storereviews'=>$is_storereviews,
                    )
                );

                @unlink(dirname(__FILE__).$this->path_img_cloud."avatar".DIRECTORY_SEPARATOR.$uniq_name_image.".".$type_one);


            }

        }else {


            if($post_images != "on"){


                $img_post = '';
                if($show_my_profile != 2) {
                    $_info = $this->getCustomerInfo();
                    $img_post = isset($_info['avatar_thumb']) ? $_info['avatar_thumb'] : '';

                    $img_post = explode("/",$img_post);
                    $img_post = end($img_post);


                    ## fixed bug, when customer try update "Show my profile on the site" without upload avatar in the My account -> User Profile section
                    $standard_avatars = array("avatar_n.gif","avatar_m.gif","avatar_w.gif");
                    if(in_array($img_post,$standard_avatars)){
                        $img_post = "";
                    }
                    ## fixed bug, when customer try update "Show my profile on the site" without upload avatar in the My account -> User Profile section

                }



                $this->saveAvatar(array(
                        'avatar' => "",
                        'id'=>$item_id,
                        'id_customer'=>$id_customer,
                        'show_my_profile' => $show_my_profile,
                        'avatar' =>$img_post,

                        'is_storereviews'=>$is_storereviews,
                    )
                );
            }
        }

    }





    public function saveAvatar($data){
        $avatar = $data['avatar'];
        $id = $data['id'];
        $id_customer = $data['id_customer'];
        $is_storereviews = $data['is_storereviews'];

        $show_my_profile = isset($data['show_my_profile'])?$data['show_my_profile']:0;
        $update_sql_cond = '';
        if($show_my_profile != 2) {
            switch($show_my_profile){
                case 'on':
                    $show_my_profile = 1;
                    break;
                default:
                    $show_my_profile = 0;
                    break;
            }
            $update_sql_cond = ' , is_show = '.(int)$show_my_profile.'';
        } else {
            $show_my_profile = 1;
        }


        if($id_customer){
            // if exist record
            $query = 'SELECT COUNT(*) as count from '._DB_PREFIX_.''.$this->_name.'_avatar2customer
												WHERE id_customer = '.(int)$id_customer;

            $result = Db::getInstance()->GetRow($query);
            $exist_record = $result['count'];

            if($exist_record){
                //update
                $query = 'UPDATE '._DB_PREFIX_.''.$this->_name.'_avatar2customer SET avatar_thumb = "'.pSQL($avatar).'" '.$update_sql_cond.'
                            WHERE id_customer = '.(int)$id_customer;
            } else {
                // insert
                $query = 'INSERT INTO '._DB_PREFIX_.''.$this->_name.'_avatar2customer (id_customer, avatar_thumb,is_show)
                             VALUES ('.(int)$id_customer.', "'.pSQL($avatar).'", '.(int)$show_my_profile.') ';
            }

        } else {

            if($is_storereviews){
                //update
                $query = 'UPDATE ' . _DB_PREFIX_ . 'spmgsnipreview_storereviews SET avatar =  "' . pSQL($avatar) . '" WHERE id = ' . (int)$id;
            } else {
                //update
                $query = 'UPDATE ' . _DB_PREFIX_ . 'spmgsnipreview SET avatar =  "' . pSQL($avatar) . '" WHERE id = ' . (int)$id;
            }

        }
        Db::getInstance()->Execute($query);


        if(Tools::strlen($avatar)==0)
            @unlink(dirname(__FILE__).$this->path_img_cloud."avatar".DIRECTORY_SEPARATOR.$avatar);


        $this->_clearSmartyCache();


    }

	
	private function _http(){


        if(version_compare(_PS_VERSION_, '1.6', '>')){
            $http = Tools::getShopDomainSsl(true, true).__PS_BASE_URI__;
        } else {
            $http = _PS_BASE_URL_.__PS_BASE_URI__;
        }


		return $http;
	}




	public function getCustomerInfo(){
		$cookie = $this->context->cookie;
		
		$exist_avatar = 0;
		$is_show = 0;
        $id_gender = 0;
		
		if($cookie->logged){
			$id_customer = $cookie->id_customer;
			
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'spmgsnipreview_avatar2customer`
		        WHERE `id_customer` = '.(int)$id_customer.'';
    	$result = Db::getInstance()->GetRow($sql);

    	$avatar_thumb = $result['avatar_thumb'];
    	$is_show = isset($result['is_show'])?$result['is_show']:1;


        $info_customer_db = $this->_getInfoCustomerDB(array('id_customer' => $id_customer));
        $id_gender = $info_customer_db['id_gender'];


    	
		// user with avatar
		if(Tools::strlen($avatar_thumb)>0){
			//$avatar_thumb = $this->_http."upload/".$avatar_thumb;

            if ($this->_is_cloud) {
                $path_img_cloud = "modules/" . $this->_name . "/upload/" . $this->_name . "/avatar/";
            } else {
                $path_img_cloud = "upload/" . $this->_name . "/avatar/";

            }


            $avatar_thumb = $this->_http.$path_img_cloud.$avatar_thumb;

			$exist_avatar = 1;
		} else {
			// user without avatar
			$info_customer_db = $this->_getInfoCustomerDB(array('id_customer' => $id_customer));
			switch($info_customer_db['id_gender']){
				case 1:
					//male
					$avatar_thumb = $this->_http."modules/" . $this->_name . "/views/img/avatar_m.gif";
				break;
				case 2:
					//female
					$avatar_thumb = $this->_http."modules/" . $this->_name . "/views/img/avatar_w.gif";
				break;
				default:
					//unknown
					$avatar_thumb = $this->_http."modules/" . $this->_name . "/views/img/avatar_n.gif";
				break;
				
			}
			
		}
		 
    		return array('id_customer'=>$id_customer, 'id_gender'=>$id_gender,
						 'avatar_thumb' => $avatar_thumb, 'exist_avatar' => $exist_avatar, 'is_show' => $is_show);
		} else {
			return array('id_customer'=>0,'avatar' => '','avatar_thumb' => '', 'exist_avatar' => $exist_avatar
						 , 'is_show' => $is_show, 'id_gender'=>$id_gender);
		}
	}
	
	private function _getInfoCustomerDB($data){
		$id_customer = $data['id_customer'];
		$sql = 'SELECT * FROM `'._DB_PREFIX_.'customer` 
		        WHERE `id_customer` = '.(int)$id_customer.'';
    	$result = Db::getInstance()->GetRow($sql);
    	
    	return $result;
	}

    public function deleteAvatar($data){
        $id = $data['id'];
        $id_customer = $data['id_customer'];
        $is_storereviews = isset($data['is_storereviews'])?$data['is_storereviews']:0;

        $img = $data['avatar'];

        $this->saveAvatar(array(
                'avatar' => "",
                'id'=>$id,
                'id_customer' => $id_customer,
                'is_storereviews' => $is_storereviews,
            )
        );

        $img = explode("/",$img);
        $img = end($img);
        @unlink(dirname(__FILE__).$this->path_img_cloud."avatar".DIRECTORY_SEPARATOR.$img);
    }




    public function getHttpost(){
        if(version_compare(_PS_VERSION_, '1.5', '>')){
            $custom_ssl_var = 0;
            if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (bool)Configuration::get('PS_SSL_ENABLED'))
                $custom_ssl_var = 1;


            if ($custom_ssl_var == 1)
                $_http_host = _PS_BASE_URL_SSL_.__PS_BASE_URI__;
            else
                $_http_host = _PS_BASE_URL_.__PS_BASE_URI__;

        } else {
            $_http_host = _PS_BASE_URL_.__PS_BASE_URI__;
        }
        return $_http_host;
    }


    public function getAvatarForCustomer($data){
        $id_customer = $data['id_customer'];

        $query = 'SELECT avatar_thumb, is_show from '._DB_PREFIX_.''.$this->_name.'_avatar2customer
												WHERE id_customer = '.(int)$id_customer;

        $result = Db::getInstance()->ExecuteS($query);


        $avatar = isset($result[0]['avatar_thumb'])?$result[0]['avatar_thumb']:'';

        if(Tools::strlen($avatar)>0) {

            if ($this->_is_cloud) {
                $path_img_cloud = "modules/" . $this->_name . "/upload/" . $this->_name . "/avatar/";
            } else {
                $path_img_cloud = "upload/" . $this->_name . "/avatar/";

            }

            $obj = $this->getObjectParent();
            $http = $obj->getHttpost();


            $avatar = $http . $path_img_cloud . $avatar;
        }

        return array('avatar'=>$avatar);
    }


    public function paging17($data)
    {
        $start = $data['start'];
        $count = $data['count'];
        $step = $data['step'];
        $data['prefix'] = $this->_name.'u';



        include_once(_PS_MODULE_DIR_ . $this->_name . '/classes/pagenavhelp.class.php');
        $obj = new pagenavhelp();
        return $obj->pagenav($start, $count, $step, $data);
    }

    private function _clearSmartyCache(){

        include_once(_PS_MODULE_DIR_.$this->_name.'/classes/cachespmgsnipreview.class.php');
        $obj = new cachespmgsnipreview();
        $obj->clearSmartyCacheModule();
    }


    public function userprofileg16($data){

        $prefix_shop_reviews = $data['prefix_shop_reviews'];
        $prefix_review = $data['prefix_review'];
        $d_eff_shopu_effects = $data['d_eff_shopu_effects'];
        $table = $data['table'];
        $this_data = $data['this'];
        $identifier = $data['identifier'];
        $tab = $data['tab'];
        $uri = $data['uri'];

        $update_title = $data['update_title'];
        $yes_title = $data['yes_title'];
        $no_title = $data['no_title'];
        $uprofile_title = $data['uprofile_title'];
        $is_uprof_title= $data['is_uprof_title'];
        $is_uprof_desc = $data['is_uprof_desc'];
        $is_proftab = $data['is_proftab'];
        $rshoppers_blc = $data['rshoppers_blc'];
        $rproft_left = $data['rproft_left'];
        $radv_left = $data['radv_left'];
        $radv_right = $data['radv_right'];
        $radv_footer = $data['radv_footer'];
        $radv_home = $data['radv_home'];
        $rpage_shoppers = $data['rpage_shoppers'];
        $perpageuti = $data['perpageuti'];
        $perpageur = $data['perpageur'];
        $owl_title = $data['owl_title'];
        $sr_slideru = $data['sr_slideru'];
        $sr_slu = $data['sr_slu'];
        $owl_title_home = $data['owl_title_home'];
        $sr_sliderhu = $data['sr_sliderhu'];
        $sr_slhu = $data['sr_slhu'];
        $wow_title = $data['wow_title'];
        $d_eff_shopu = $data['d_eff_shopu'];

        $fields_form = array(
            'form'=> array(
                'legend' => array(
                    'title' => $uprofile_title,
                    'icon' => 'fa fa-users fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $is_uprof_title,
                        'name' => 'is_uprof',
                        'desc' => $is_uprof_desc,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'switch',
                        'label' => $is_proftab,
                        'name' => 'is_proftab',
                        'desc' => $is_proftab,
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$rshoppers_blc,
                        'name' => 'rshoppers_blc',
                        'class' => ' fixed-width-sm',

                    ),
                    array(
                        'type' => 'checkbox_custom_blocks',
                        'label' => $rproft_left,
                        'name' => 'rproft_left',
                        'values' => array(
                            'query' => array(
                                array(
                                    'id' => 'radv_left',
                                    'name' =>$radv_left,
                                    'val' => 1
                                ),
                                array(
                                    'id' => 'radv_right',
                                    'name' => $radv_right,
                                    'val' => 1
                                ),
                                array(
                                    'id' => 'radv_footer',
                                    'name' => $radv_footer,
                                    'val' => 1
                                ),
                                array(
                                    'id' => 'radv_home',
                                    'name' =>$radv_home,
                                    'val' => 1
                                ),
                            ),
                            'id' => 'id',
                            'name' => 'name'
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $rpage_shoppers,
                        'name' => 'rpage_shoppers',
                        'class' => ' fixed-width-sm',
                    ),
                    array(
                        'type' => 'text',
                        'label' => $perpageuti,
                        'name' => 'perpageu'.$prefix_shop_reviews,
                        'class' => ' fixed-width-sm',
                    ),
                    array(
                        'type' => 'text',
                        'label' =>$perpageur,
                        'name' => 'perpageu'.$prefix_review,
                        'class' => ' fixed-width-sm',
                    ),

                ),
            ),
        );

        $fields_form1 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $owl_title,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $sr_slideru,
                        'name' => 'sr_slideru',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $sr_slu,
                        'name' => 'sr_slu',
                        'id' => 'sr_slu',
                        'lang' => FALSE,
                    ),
                ),
            ),
        );

        $fields_form2 = array(
            'form'=> array(

                'legend' => array(
                    'title' => $owl_title_home,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'switch',
                        'label' => $sr_sliderhu,
                        'name' => 'sr_sliderhu',
                        'values' => array(
                            array(
                                'id' => 'active_on',
                                'value' => 1,
                                'label' => $yes_title
                            ),
                            array(
                                'id' => 'active_off',
                                'value' => 0,
                                'label' => $no_title
                            )
                        ),
                    ),
                    array(
                        'type' => 'text',
                        'label' => $sr_slhu,
                        'name' => 'sr_slhu',
                        'id' => 'sr_slhu',
                        'lang' => FALSE,
                    ),
                ),
            ),
        );

        $fields_form3 = array(
            'form'=> array(
                'legend' => array(
                    'title' => $wow_title,
                    'icon' => 'fa fa-sliders fa-lg'
                ),
                'input' => array(
                    array(
                        'type' => 'wow_display_effect',
                        'label' => $d_eff_shopu,
                        'name' => 'd_eff_shopu',
                        'id' => 'd_eff_shopu',
                        'lang' => FALSE,
                        'value' => $d_eff_shopu_effects
                    ),
                ),
            ),
        );

        $fields_form10 = array(
            'form' => array(
                'submit' => array(
                    'title' => $update_title,
                )
            ),
        );

        $helper = new HelperForm();
        $helper->table = $table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->module = $this_data;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $helper->identifier = $identifier;
        $helper->submit_action = 'userprofilegsettings';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->_name.'&tab_module='.$tab.'&module_name='.$this->_name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'uri' => $uri,
            'fields_value' => $this->getConfigFieldsValuesUserSettings(array('prefix_shop_reviews'=>$prefix_shop_reviews,'prefix_review'=>$prefix_review)),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );


        ob_start();
        include(dirname(__FILE__).'/../views/templates/hooks/_userprofileg16_js.phtml');
        $_html = ob_get_clean();

        return  $_html.$helper->generateForm(array($fields_form,$fields_form1,$fields_form2,$fields_form3,$fields_form10));
    }

    public function getConfigFieldsValuesUserSettings($data){

        $prefix_shop_reviews = $data['prefix_shop_reviews'];
        $prefix_review = $data['prefix_review'];


        $data_config = array(
            'is_uprof' =>Configuration::get($this->_name.'is_uprof'),

            'rshoppers_blc' => Configuration::get($this->_name.'rshoppers_blc'),
            'rpage_shoppers'=> Configuration::get($this->_name.'rpage_shoppers'),

            'radv_left'=>Configuration::get($this->_name.'radv_left'),
            'radv_right'=>Configuration::get($this->_name.'radv_right'),
            'radv_footer'=>Configuration::get($this->_name.'radv_footer'),
            'radv_home'=>Configuration::get($this->_name.'radv_home'),

            'is_proftab'=>Configuration::get($this->_name.'is_proftab'),
            'perpageu'.$prefix_shop_reviews => (int)Configuration::get($this->_name.'perpageu'.$prefix_shop_reviews),
            'perpageu'.$prefix_review => (int)Configuration::get($this->_name.'perpageu'.$prefix_review),

            'sr_slideru'=> (int)Configuration::get($this->_name.'sr_slideru'),
            'sr_slu'=> (int)Configuration::get($this->_name.'sr_slu'),
            'sr_sliderhu'=> (int)Configuration::get($this->_name.'sr_sliderhu'),
            'sr_slhu'=> (int)Configuration::get($this->_name.'sr_slhu'),
        );

        return $data_config;
    }
}