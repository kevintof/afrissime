{*
* PrestaHost.cz / PrestaHost.eu
*
*
*  @author prestahost.eu <info@prestahost.cz>
*  @copyright  2014  PrestaHost.eu, Vaclav Mach
*  @license    http://prestahost.eu/prestashop-modules/en/content/3-terms-and-conditions-of-use
*}
   
 
   {l s='You pay for the merchandise upon delivery' mod='cashondeliveryplus'} 
            <br />
             {if isset($codfee) && $codfee}
               {l s='The COD fee is' mod='cashondeliveryplus'}   <span class="price">{$codfee|escape:'html':'UTF-8'}</span> {if isset($tax)}{$tax|escape:'html':'UTF-8'}{/if}
            {else}
                {l s='Free' mod='cashondeliveryplus'}
            {/if}
         
             
 

