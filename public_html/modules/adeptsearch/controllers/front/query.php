<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Arthur Cai
 * @copyright 2018 Adeptmind
 * @license   LICENSE
 */

class AdeptSearchQueryModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        parent::initContent();
        if (version_compare(_PS_VERSION_, '1.7', '>=')) {
            $this->setTemplate('module:adeptsearch/views/templates/front/query_1_7.tpl');
        } else {
            $this->setTemplate('query.tpl');
        }
    }
}
