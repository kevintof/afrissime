{*!
* Adeptmind search admin for prestashop
* https://github.com/adeptmind adept-platform-prestashop-module.git
*
* Copyright (C) 2018 Adeptmind
*}
<style>
  .{$positionClass|escape:'htmlall':'UTF-8'}.adept-search-box__container{
    padding-bottom: 15px;
    width: 100%;
  }
  .{$positionClass|escape:'htmlall':'UTF-8'} .adept-search-box__container * {
    box-sizing: border-box;
  }

  .{$positionClass|escape:'htmlall':'UTF-8'} .adept-search-box__form{
    display:flex;
  }
  .{$positionClass|escape:'htmlall':'UTF-8'} .adept-search-box__input{
    background: {$fieldBackground|escape:'htmlall':'UTF-8'};
    border: solid 1px {$fieldBorderColor|escape:'htmlall':'UTF-8'};
    border-right: none;
    color: {$fieldColor|escape:'htmlall':'UTF-8'};
    padding: 10px;
    flex: 1;
  }
  .{$positionClass|escape:'htmlall':'UTF-8'} .adept-search-box__button{
    background: {$buttonBackground|escape:'htmlall':'UTF-8'};
    color: {$buttonColor|escape:'htmlall':'UTF-8'};
    border: solid 1px {$fieldBorderColor|escape:'htmlall':'UTF-8'};
    padding: 10px;
  }
</style>
<div id="search_block_top" class="adept-search-box__container {$positionClass|escape:'htmlall':'UTF-8'}">
  <form class='adept-search-box__form' method="get" action="{$SEARCH_URL|escape:'htmlall':'UTF-8'}">
    <input class='adept-search-box__input adept-search-box search_query' placeholder="{$placeholderText|escape:'htmlall':'UTF-8'}" type='text' name='q' />
    <button type="submit" name="submit_search" class="adept-search-box__button button-search">
      <span>Search</span>
    </button>
  </form>
</div>
