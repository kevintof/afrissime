<?php
/**
 * NOTICE OF LICENSE
 *
 * This file is licenced under the Software License Agreement.
 * With the purchase or the installation of the software in your application
 * you accept the licence agreement.
 *
 * You must not modify, adapt or create derivative works of this source code
 *
 * @author    Arthur Cai
 * @copyright 2018 Adeptmind
 * @license   LICENSE
 */

class AdeptSearchRescrapeQueue
{
    public static function install()
    {
        self::uninstall();
        Db::getInstance()->execute(
            'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'adept_search_rescrape_queue` (
                `id_queue_item` INT(11) NOT NULL auto_increment,
                `id_product` INT(11) NOT NULL,
                `is_deleted` TINYINT(1) DEFAULT 0,
                `created_at` DATETIME NOT NULL,
                PRIMARY KEY (`id_queue_item`)
            ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=UTF8;'
        );
        Configuration::updateValue('ADEPTMIND_RESCRAPE_QUEUE_CREATED', true);
        return true;
    }

    public static function uninstall()
    {
        Db::getInstance()->execute(
            'DROP TABLE IF EXISTS `'._DB_PREFIX_.'adept_search_rescrape_queue`;'
        );
        Configuration::deleteByName('ADEPTMIND_RESCRAPE_QUEUE_CREATED');
        return true;
    }

    public static function insert($id_product, $is_deleted = false)
    {
        if (!Configuration::get('ADEPTMIND_RESCRAPE_QUEUE_CREATED')) {
            self::install();
        }
        Db::getInstance()->insert(
            'adept_search_rescrape_queue',
            array(
                array(
                    'id_product' => $id_product,
                    'is_deleted' => $is_deleted ? 1 : 0,
                    'created_at' => date('Y-m-d H-i-s')
                )
            )
        );
        return true;
    }

    public static function emptyQueue()
    {
        if (!Configuration::get('ADEPTMIND_RESCRAPE_QUEUE_CREATED')) {
            self::install();
        }
        Db::getInstance()->execute('LOCK TABLES `'._DB_PREFIX_.'adept_search_rescrape_queue` WRITE;');
        $queue = Db::getInstance()->executeS(
            'SELECT * FROM `'._DB_PREFIX_.'adept_search_rescrape_queue` ORDER BY `created_at`;'
        );
        Db::getInstance()->delete('adept_search_rescrape_queue');
        Db::getInstance()->execute('UNLOCK TABLES;');
        $deduped_queue = array();
        foreach ($queue as $queue_item) {
            $deduped_queue[$queue_item['id_product']] = $queue_item;
        }
        return array_values($deduped_queue);
    }
}
