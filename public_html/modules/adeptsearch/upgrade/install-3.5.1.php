<?php
/**
 * 2007-2016 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2016 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/util.php';
include_once _PS_MODULE_DIR_ . 'adeptsearch/classes/shop.php';

function upgrade_module_3_5_1($module)
{
    Configuration::updateValue('ADEPTMIND_LAST_DELTA_RESCRAPE_TIME', date('Y-m-d H:i:s'));
    try {
        AdeptUtil::callPlatformManagerPut(
            '/shops/'.Configuration::get('ADEPTMIND_SHOP_UUID'),
            array(
                'country' => Context::getContext()->country->iso_code,
                'city' => Configuration::get('PS_SHOP_CITY') || '',
            )
        );
    } catch (Exception $e) {
        AdeptShop::reportException('PRESTASHOP_UPGRADE_3_5_1_FAILED', $e);
    }
    return true;
}
