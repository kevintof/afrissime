<?php
/**
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2019 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
**/

use PrestaShop\PrestaShop\Core\Payment\PaymentOption;

if (!defined('_PS_VERSION_')) {
    exit;
}

class WeCashUp extends PaymentModule
{
    public $hookName;
    public function __construct()
    {
        $this->name = 'wecashup';
        $this->tab = 'payments_gateways';
        $this->version = '2.0.1';
        $this->author = 'INFINITY SPACE';
        $this->controllers = array('validation', 'callback', 'payment');
        $this->need_instance = 0;
        $this->is_eu_compatible = 1;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        $this->currencies = true;
        $this->currencies_mode = 'checkbox';
        $this->module_key = 'f2040f804fa1afde4f1aa48c77d2fac6';
        parent::__construct();
        $this->displayName = $this->l('WeCashUp Payment');
        $this->description = $this->l('Accept Mobile Money, Cash &amp; Card Payments online everywhere in Africa.');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        if (!Configuration::get('WECASHUP_TITLE')) {
            $this->warning = $this->l('No name provided.');
        }
        if (!Configuration::get('WECASHUP_DESC')) {
            $this->warning = $this->l('No Description provided.');
        }
        if (!Configuration::get('WECASHUP_MUID')) {
            $this->warning = $this->l('No Merchant UID provided.');
        }
        if (!Configuration::get('WECASHUP_PKEY')) {
            $this->warning = $this->l('No Merchant Public Key provided.');
        }
        if (!Configuration::get('WECASHUP_SKEY')) {
            $this->warning = $this->l('No Merchant Secret Key provided.');
        }
//    if (!Configuration::get('WECASHUP_MCURR')) {
//      $this->warning = $this->l('Select merchant currency.');
//    }
        if (!Configuration::get('WECASHUP_CPAY')) {
            $this->warning = $this->l('Select enable cash payment method option.');
        }
        if (!Configuration::get('WECASHUP_TELECOM')) {
            $this->warning = $this->l('Select enable telecom payment method option.');
        }
        if (!Configuration::get('WECASHUP_MWALLET')) {
            $this->warning = $this->l('Select m-wallet payment method option.');
        }
        if (!Configuration::get('WECASHUP_SPAY')) {
            $this->warning = $this->l('Select Split payment method option.');
        }
        if (!Configuration::get('WECASHUP_PBNAME')) {
            $this->warning = $this->l('Add payment box name.');
        }
        if (!Configuration::get('WECASHUP_PBLAN')) {
            $this->warning = $this->l('Select payment box language.');
        }
        if (!Configuration::get('WECASHUP_PBLOGO')) {
            $this->warning = $this->l('Select payment box logo.');
        }
    }
    public function install()
    {
        $makeTbl = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'wecashup_data(
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `user_id` int(11) NOT NULL,
            `order_id` int(11) NOT NULL,
            `transaction_uid` varchar(255) NOT NULL,
            `transaction_token` varchar(255) NOT NULL,
            `transaction_provider_name` varchar(255) NOT NULL,
            `transaction_confirmation_code` varchar(255) NOT NULL,
            `status` tinyint(1) NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
            Db::getInstance()->execute($makeTbl);

        if (extension_loaded('curl') == false || $this->checkApi() != '') {
            $this->_errors[] = Tools::displayError(
                $this->l('You have to enable the cURL extension on your server to install this module.')
            );
            $this->_errors[] = $this->checkApi();
            $this->log('Install', 'Installation failed: cURL Requirement.');
            return false;
        }
        if (!parent::install() || !$this->registerHook('payment') || !$this->registerHook('paymentOptions')) {
            return false;
        }

        return true;
    }

    public function checkApi()
    {
        if (!ini_get('allow_url_fopen')) {
            return $this->l('Acces to PaygreenApi fail.');
        }
        return '';
    }
    public function uninstall()
    {
        if (!parent::uninstall()) {
            $this->l('Uninstallation failed.');
            return false;
        }
        Db::getInstance()->execute("DROP TABLE IF EXISTS "._DB_PREFIX_."wecashup_data");
        $this->l('Uninstall', 'Uninstallation complete.');
        return true;
    }
    public function getContent()
    {
        $output = null;
        if (Tools::isSubmit('submit'.$this->name)) {
            Tools::getProtocol(Tools::usingSecureMode()).$_SERVER['HTTP_HOST'].$this->getPathUri();
            $callurl = $this->context->link->getModuleLink($this->name, 'validation', array(), true);
            $webhook = $this->context->link->getModuleLink($this->name, 'callback', array(), true);
            $wecashup_name = (string)Tools::getValue('WECASHUP_TITLE');
            $wecashup_desc = (string)Tools::getValue('WECASHUP_DESC');
            $wecashup_uid = (string)Tools::getValue('WECASHUP_MUID');
            $wecashup_pkey = (string)Tools::getValue('WECASHUP_PKEY');
            $wecashup_skey = (string)Tools::getValue('WECASHUP_SKEY');
        //$wecashup_curr = cast(Tools::getValue('WECASHUP_MCURR'));
            $wecashup_cpay = (string)Tools::getValue('WECASHUP_CPAY');
            $wecashup_telcom = (string)Tools::getValue('WECASHUP_TELECOM');
            $wecashup_mwallet = (string)Tools::getValue('WECASHUP_MWALLET');
            $wecashup_spay = (string)Tools::getValue('WECASHUP_SPAY');
            $wecashup_pbox = (string)Tools::getValue('WECASHUP_PBNAME');
            $wecashup_plan = (string)Tools::getValue('WECASHUP_PBLAN');
            $wecashup_plogo = (string)Tools::getValue('WECASHUP_PBLOGO');
            if (!$wecashup_name || empty($wecashup_name) || !Validate::isGenericName($wecashup_name) ||
            !$wecashup_uid || empty($wecashup_uid) || !Validate::isGenericName($wecashup_uid) ||
            !$wecashup_pkey || empty($wecashup_pkey) || !Validate::isGenericName($wecashup_pkey) ||
            !$wecashup_skey || empty($wecashup_skey) || !Validate::isGenericName($wecashup_skey)) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                Configuration::updateValue('WECASHUP_TITLE', $wecashup_name);
                Configuration::updateValue('WECASHUP_DESC', $wecashup_desc);
                Configuration::updateValue('WECASHUP_MUID', $wecashup_uid);
                Configuration::updateValue('WECASHUP_PKEY', $wecashup_pkey);
                Configuration::updateValue('WECASHUP_SKEY', $wecashup_skey);
            //Configuration::updateValue('WECASHUP_MCURR', $wecashup_curr);
                Configuration::updateValue('WECASHUP_CPAY', $wecashup_cpay);
                Configuration::updateValue('WECASHUP_TELECOM', $wecashup_telcom);
                Configuration::updateValue('WECASHUP_MWALLET', $wecashup_mwallet);
                Configuration::updateValue('WECASHUP_SPAY', $wecashup_spay);
                Configuration::updateValue('WECASHUP_PBNAME', $wecashup_pbox);
                Configuration::updateValue('WECASHUP_PBLAN', $wecashup_plan);
                Configuration::updateValue('WECASHUP_PBLOGO', $wecashup_plogo);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
                $url ='https://www.wecashup.com/api/v2.0/merchants/'.$wecashup_uid;
                $url .= '?merchant_public_key='.$wecashup_pkey;
                $fields = array(
                'merchant_secret' => urlencode($wecashup_skey),
                'merchant_default_webhook_url' => urlencode($webhook),
                'merchant_callback_url' => urlencode($callurl),
                '_method' => urlencode('PATCH')
                );
                $fields_string = '';
                foreach ($fields as $key => $value) {
                    $fields_string .= $key.'='.$value.'&';
                }
                rtrim($fields_string, '&');
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, count($fields));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                // $server_output = curl_exec($ch);
                curl_exec($ch);
                curl_close($ch);
            }
        }
        return $output.$this->displayForm();
    }
    public function displayForm()
    {
            // Get default language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
        $truefalse = array(
            array(
                'id_option' => 'true',
                'name' => $this->l('True')
            ),
            array(
                'id_option' => 'false',
                'name' => $this->l('False')
            )
        );

        $pboxlang = array(
            array(
                'id_option' => 'en',
                'name' => $this->l('English')
            ),
            array(
                'id_option' => 'fr',
                'name' => $this->l('French')
            ),
            array(
                'id_option' => 'auto',
                'name' => $this->l('Automatic')
            )
        );

    /*$countries = array(
            array(
                'id_option' => 'AOA',
                'name' => $this->l('Kwanza')
            ),
            array(
                'id_option' => 'BIF',
                'name' => $this->l('Burundi Franc')
            ),
            array(
                'id_option' => 'BWP',
                'name' => $this->l('Pula')
            ),
            array(
                'id_option' => 'CDF',
                'name' => $this->l('Franc Congolais')
            ),
            array(
                'id_option' => 'CVE',
                'name' => $this->l('Cape Verde Escudo')
            ),
            array(
                'id_option' => 'DJF',
                'name' => $this->l('Djibouti Franc')
            ),
            array(
                'id_option' => 'DZD',
                'name' => $this->l('Algerian Dinar')
            ),
            array(
                'id_option' => 'EGP',
                'name' => $this->l('Egyptian Pound')
            ),
            array(
                'id_option' => 'ERN',
                'name' => $this->l('Nakfa')
            ),
            array(
                'id_option' => 'ETB',
                'name' => $this->l('Ethiopian Birr')
            ),
            array(
                'id_option' => 'GHS',
                'name' => $this->l('Ghanaian Cedi')
            ),
            array(
                'id_option' => 'GMD',
                'name' => $this->l('Dalasi')
            ),
            array(
                'id_option' => 'GNF',
                'name' => $this->l('Guinea Franc')
            ),
            array(
                'id_option' => 'KES',
                'name' => $this->l('Kenyan Shilling')
            ),
            array(
                'id_option' => 'KMF',
                'name' => $this->l('Comoro Franc')
            ),
            array(
                'id_option' => 'LRD',
                'name' => $this->l('Liberian Dollar')
            ),
            array(
                'id_option' => 'LOTI',
                'name' => $this->l('loti')
            ),
            array(
                'id_option' => 'LYD',
                'name' => $this->l('Lybian Dinar')
            ),
            array(
                'id_option' => 'MAD',
                'name' => $this->l('Moroccan Dirham')
            ),
            array(
                'id_option' => 'MGA',
                'name' => $this->l('Malagasy Ariary')
            ),
            array(
                'id_option' => 'MRO',
                'name' => $this->l('Ouguiya')
            ),
            array(
                'id_option' => 'MUR',
                'name' => $this->l('Mauritius Rupee')
            ),
            array(
                'id_option' => 'MWK',
                'name' => $this->l('Kwacha')
            ),
            array(
                'id_option' => 'MZN',
                'name' => $this->l('Mozambican Metical')
            ),
            array(
                'id_option' => 'NDN',
                'name' => $this->l('Rand Namibia Dollar')
            ),
            array(
                'id_option' => 'NOK',
                'name' => $this->l('Norwegian Krone')
            ),
            array(
                'id_option' => 'RWF',
                'name' => $this->l('Rwanda Franc')
            ),
            array(
                'id_option' => 'SCR',
                'name' => $this->l('Seychelles Rupee')
            ),
            array(
                'id_option' => 'SDG',
                'name' => $this->l('Sudanese Dinar')
            ),
            array(
                'id_option' => 'SHP',
                'name' => $this->l('Saint Helena Pound')
            ),
            array(
                'id_option' => 'SSL',
                'name' => $this->l('Leone')
            ),
            array(
                'id_option' => 'SOL',
                'name' => $this->l('Somali Shilling')
            ),
            array(
                'id_option' => 'SSP',
                'name' => $this->l('South Sudanese pound')
            ),
            array(
                'id_option' => 'STD',
                'name' => $this->l('Sao Tomean Dobra')
            ),
            array(
                'id_option' => 'SZL',
                'name' => $this->l('Lilangeni')
            ),
            array(
                'id_option' => 'TND',
                'name' => $this->l('Tunisian Dinar')
            ),
            array(
                'id_option' => 'TZS',
                'name' => $this->l('Tanzanian Shilling')
            ),
            array(
                'id_option' => 'UGX',
                'name' => $this->l('Uganda Shilling')
            ),
            array(
                'id_option' => 'XAP',
                'name' => $this->l('CFA Franc BEAC')
            ),
            array(
                'id_option' => 'XOF',
                'name' => $this->l('CFA Franc BCEAO')
            ),
            array(
                'id_option' => 'ZAR',
                'name' => $this->l('Rand Namibia Dollar')
            ),
            array(
                'id_option' => 'ZMW',
                'name' => $this->l('Zambian Kwacha')
            ),
            array(
                'id_option' => 'ZWL',
                'name' => $this->l('Zimbabwean dollar')
            ),
            array(
                'id_option' => 'EUR',
                'name' => $this->l('Euro')
            ),
            array(
                'id_option' => 'GBP',
                'name' => $this->l('Pound Sterling')
            ),
            array(
                'id_option' => 'ROL',
                'name' => $this->l('Leu')
            ),
            array(
                'id_option' => 'ALL',
                'name' => $this->l('Albania')
            ),
            array(
                'id_option' => 'INR',
                'name' => $this->l('Indian Rupee')
            ),
            array(
                'id_option' => 'PHP',
                'name' => $this->l('Philippine Peso')
            ),
            array(
                'id_option' => 'ARS',
                'name' => $this->l('Argentine Peso')
            ),
            array(
                'id_option' => 'BOB',
                'name' => $this->l('Boliviano')
            ),
            array(
                'id_option' => 'BRL',
                'name' => $this->l('Brazilian Real')
            ),
            array(
                'id_option' => 'CLP',
                'name' => $this->l('Chilean Peso')
            ),
            array(
                'id_option' => 'COP',
                'name' => $this->l('Colombian Peso')
            ),
            array(
                'id_option' => 'CUC',
                'name' => $this->l('Cuban Convertible Peso')
            ),
            array(
                'id_option' => 'GTQ',
                'name' => $this->l('Quetzal')
            ),
            array(
                'id_option' => 'PEN',
                'name' => $this->l('Nuevo Sol')
            ),
            array(
                'id_option' => 'PYG',
                'name' => $this->l('Guarani')
            ),
            array(
                'id_option' => 'SRD',
                'name' => $this->l('Surinamese Dollar')
            ),
            array(
                'id_option' => 'UYU',
                'name' => $this->l('Peso Uruguayo')
            ),
            array(
                'id_option' => 'VEF',
                'name' => $this->l('Venezuelan Bolívar')
            ),
            array(
                'id_option' => 'MXN',
                'name' => $this->l('Mexican Peso Mexican Unidad de Inversion (UDI)')
            ),
            array(
                'id_option' => 'NIO',
                'name' => $this->l('Cordoba Oro')
            ),
            array(
                'id_option' => 'CAD',
                'name' => $this->l('Canadian Dollar')
            ),
            array(
                'id_option' => 'USD',
                'name' => $this->l('US Dollar')
            )

        ); */
    // Init Fields form array
        $fields_form = array();
        $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Settings'),
        ),
        'input' => array(
            array(
                'type' => 'text',
                'label' => $this->l('Main title'),
                'name' => 'WECASHUP_TITLE',
                'size' => 20,
                'required' => true
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Description'),
                'name' => 'WECASHUP_DESC',
                'size' => 20,
                'required' => true
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Merchant UID'),
                'name' => 'WECASHUP_MUID',
                'desc' => 'This is the merchant UID, received from WeCashUp.',
                'size' => 20,
                'required' => true
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Merchant Public Key'),
                'name' => 'WECASHUP_PKEY',
                'desc' => 'This is the merchant Public Key, received from WeCashUp.',
                'size' => 20,
                'required' => true
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Merchant Secret Key'),
                'name' => 'WECASHUP_SKEY',
                'desc' => 'This is the merchant Secret Key, received from WeCashUp.',
                'size' => 20,
                'required' => true
            ),
//            array(
//                'type' => 'select',
//                'label' => $this->l('Merchant Currency'),
//                'name' => $this->l('WECASHUP_MCURR'),
//                'options' => array(
//                            'query' => $countries,
//                            'id' => 'id_option',
//                            'name' => 'name'
//                        ),
//                'required' => true
//
//                    ),
            array(
                'type' => 'select',
                'label' => $this->l('Enable cash payment'),
                'name' => $this->l('WECASHUP_CPAY'),
                'desc' => $this->l('Select true/false to enable this method.'),
                        'options' => array(
                            'query' => $truefalse,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                'required' => true
                    ),
            array(
                'type' => 'select',
                'label' => $this->l('Enable Telecom Payment method'),
                'name' => $this->l('WECASHUP_TELECOM'),
                'desc' => $this->l('Select true/false to enable this method.'),
                'options' => array(
                            'query' => $truefalse,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                'required' => true
                    ),
            array(
                'type' => 'select',
                'label' => $this->l('Enable M-wallet Payment method'),
                'name' => $this->l('WECASHUP_MWALLET'),
                'desc' => $this->l('Select true/false to enable this method.'),
                'options' => array(
                            'query' => $truefalse,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                'required' => true
                    ),
            array(
                'type' => 'select',
                'label' => $this->l('Split payment'),
                'name' => $this->l('WECASHUP_SPAY'),
                'desc' => $this->l('Select true/false to enable this method.'),
                'options' => array(
                            'query' => $truefalse,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                'required' => true
                    ),
            array(
                'type' => 'text',
                'label' => $this->l('Payment Box name'),
                'name' => 'WECASHUP_PBNAME',
                'desc' => 'This is heading used to display in payment box.',
                'size' => 20,
                'required' => true
            ),
            array(
                'type' => 'select',
                'label' => $this->l('Payment Box Language'),
                'name' => $this->l('WECASHUP_PBLAN'),
                'desc' => $this->l('Select language for payment box.'),
                'options' => array(
                            'query' => $pboxlang,
                            'id' => 'id_option',
                            'name' => 'name'
                        ),
                'required' => true
                    ),
            array(
                'type' => 'text',
                'label' => $this->l('Payment Box logo url'),
                'name' => 'WECASHUP_PBLOGO',
                'desc' => 'This is logo used in payment box.',
                'size' => 20,
                'required' => true
            )
        ),
        'submit' => array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right'
        )
        );

        $helper = new HelperForm();
    // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
    // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
    // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
        'save' =>
        array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        )
        );
    // Load current value
        $helper->fields_value['WECASHUP_TITLE'] = Configuration::get('WECASHUP_TITLE');
        $helper->fields_value['WECASHUP_DESC'] = Configuration::get('WECASHUP_DESC');
        $helper->fields_value['WECASHUP_MUID'] = Configuration::get('WECASHUP_MUID');
        $helper->fields_value['WECASHUP_PKEY'] = Configuration::get('WECASHUP_PKEY');
        $helper->fields_value['WECASHUP_SKEY'] = Configuration::get('WECASHUP_SKEY');
    //$helper->fields_value['WECASHUP_MCURR'] = Configuration::get('WECASHUP_MCURR');
        $helper->fields_value['WECASHUP_CPAY'] = Configuration::get('WECASHUP_CPAY');
        $helper->fields_value['WECASHUP_TELECOM'] = Configuration::get('WECASHUP_TELECOM');
        $helper->fields_value['WECASHUP_MWALLET'] = Configuration::get('WECASHUP_MWALLET');
        $helper->fields_value['WECASHUP_SPAY'] = Configuration::get('WECASHUP_SPAY');
        $helper->fields_value['WECASHUP_PBNAME'] = Configuration::get('WECASHUP_PBNAME');
        $helper->fields_value['WECASHUP_PBLAN'] = Configuration::get('WECASHUP_PBLAN');
        $helper->fields_value['WECASHUP_PBLOGO'] = Configuration::get('WECASHUP_PBLOGO');
        return $helper->generateForm($fields_form);
    }
    public function hookPaymentOptions($params)
    {
        if (!$this->active) {
            return;
        }
        $payment_options = array(
            $this->getEmbeddedPaymentOption()
        );
        return $payment_options;
    }

    // public function hookPayment($params)
    // {
    //     if (!$this->active) {
    //         return;
    //     }
    //     if (!$this->checkCurrency($params['cart'])) {
    //         return;
    //     }

    //     $this->smarty->assign(array(
    //         'this_path' => $this->_path,
    //         'this_path_bw' => $this->_path,
    //         'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
    //     ));

    //     return $this->display(__FILE__, 'payment.tpl');
    // }

    public function checkCurrency($cart)
    {
        $currency_order = new Currency($cart->id_currency);
        $currencies_module = $this->getCurrency($cart->id_currency);

        if (is_array($currencies_module)) {
            foreach ($currencies_module as $currency_module) {
                if ($currency_order->id == $currency_module['id_currency']) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getEmbeddedPaymentOption()
    {
        $_tpl = 'module:wecashup/views/templates/front/wecashup_info.tpl';
        $embeddedOption = new PaymentOption();
        $embeddedOption->setCallToActionText($this->l('Pay via Cash,Mobile money and M-wallet'))
                       ->setForm($this->generateForm())
                       ->setModuleName($this->name)
                       ->setAdditionalInformation($this->context->smarty->fetch($_tpl));
        return $embeddedOption;
    }
    public function myfun($v, $k)
    {
        return sprintf('%s=%s', $k, $v);
    }
    protected function generateForm()
    {
        $cart = $this->context->cart;
        $products = $this->context->cart->getProducts(true);
        $count = 1;
        $adr = array();
        if (is_array($products) || is_object($products)) {
            foreach ($products as $val) {
                     $name  = str_replace(array( ' ', ' ' ), '-', $val['name']);
//                 $description  = str_replace(array( ' ', ' ' ), '', $val['description_short']);
                     $adr[] = array('data-product-'.$count.'-name' => $name,
                                    'data-product-'.$count.'-quantity' => $val['cart_quantity'],
                                    'data-product-'.$count.'-unit-price' => $val['price'],
                                    'data-product-'.$count.'-reference' => $val['unique_id'],
                                    'data-product-'.$count.'-category' => $val['category'],
//                                  'data-product-'.$count.'-description' => $description
                               );
                     $count++;
            }
        }
        $lo = array_reduce($adr, 'array_merge', array());
        $OUTPUT = implode(' ', array_map(array($this, 'myfun'), $lo, array_keys($lo)));
        $cookie = $this->context->cookie;
        $currency = new Currency($cookie->id_currency);
        $my_currency_iso_code = $currency->iso_code;
        $MUID = Configuration::get('WECASHUP_MUID');
        $MPK = Configuration::get('WECASHUP_PKEY');
        $MSK = Configuration::get('WECASHUP_SKEY');
        $MCUR = $my_currency_iso_code;
        $CPAY = Configuration::get('WECASHUP_CPAY');
        $TELL = Configuration::get('WECASHUP_TELECOM');
        $MWALL = Configuration::get('WECASHUP_MWALLET');
        $SPLIT = Configuration::get('WECASHUP_SPAY');
        $APPN = Configuration::get('WECASHUP_PBNAME');
        $CUSTE = $this->context->customer->email;
        $LAN = Configuration::get('WECASHUP_PBLAN');
        $DIMG = Configuration::get('WECASHUP_PBLOGO');
        $this->context->smarty->assign(array(
            'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true),
            'MUID' => $MUID,
            'MPK' => $MPK,
            'MSK' => $MSK,
            'MCUR' => $MCUR,
            'CPAY' => $CPAY,
            'TELL' => $TELL,
            'MWALL' => $MWALL,
            'SPLIT' => $SPLIT,
            'APPN' => $APPN,
            'CUSTE' => $CUSTE,
            'LAN' => $LAN,
            'OUTPUT' => $OUTPUT,
            'DIMG' => $DIMG,
            'BASEURL' => _PS_BASE_URL_.__PS_BASE_URI__,
            'PO' => _PS_MODULE_DIR_,
            'AMT' => (float)$cart->getOrderTotal(true, Cart::BOTH)
        ));
        return $this->context->smarty->fetch('module:wecashup/views/templates/front/wecashup_form.tpl');
    }
}
