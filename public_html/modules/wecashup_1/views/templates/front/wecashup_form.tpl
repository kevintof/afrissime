{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2019 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<form action="{$action|escape:'htmlall':'UTF-8'}" method="POST">
<script async="" src="https://www.wecashup.cloud/library/MobileMoney.js" class="wecashup_button"
data-demo
data-receiver-uid="{$MUID|escape:'htmlall':'UTF-8'}"
data-receiver-public-key="{$MPK|escape:'htmlall':'UTF-8'}"
data-transaction-receiver-total-amount="{$AMT|escape:'htmlall':'UTF-8'}"
data-transaction-receiver-reference=""
data-transaction-sender-reference="{$CUSTE|escape:'htmlall':'UTF-8'}"

data-image="{$DIMG|escape:'htmlall':'UTF-8'}"
data-transaction-method="pull"

data-name="{$APPN|escape:'htmlall':'UTF-8'}"
data-cash="{$CPAY|escape:'htmlall':'UTF-8'}"
data-telecom="{$TELL|escape:'htmlall':'UTF-8'}"
data-m-wallet="{$MWALL|escape:'htmlall':'UTF-8'}"
data-split="{$SPLIT|escape:'htmlall':'UTF-8'}"
data-sender-lang="{$LAN|escape:'htmlall':'UTF-8'}"

data-marketplace-mode="false"
{$OUTPUT|escape:'htmlall':'UTF-8'} >
</script>
</form>