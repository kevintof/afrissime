<?php
/**
* 2007-2016 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2016 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

/**
 * @since 1.5.0
 */
class WeCashUpPaymentModuleFrontController extends ModuleFrontController
{
    public $ssl = true;
    public $display_column_left = false;

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        $cart = $this->context->cart;
        if (!$this->module->checkCurrency($cart)) {
            Tools::redirect('index.php?controller=order');
        }

        $products = $this->context->cart->getProducts(true);
        $count = 1;
        $adr = array();
        if (is_array($products) || is_object($products)) {
            foreach ($products as $val) {
                $name  = str_replace(array( ' ', ' ' ), '-', $val['name']);
                $adr[] = array('data-product-'.$count.'-name' => $name,
                'data-product-'.$count.'-quantity' => $val['cart_quantity'],
                'data-product-'.$count.'-unit-price' => $val['price'],
                'data-product-'.$count.'-reference' => $val['unique_id'],
                'data-product-'.$count.'-category' => $val['category'],
                );
                $count++;
            }
        }

        $lo = array_reduce($adr, 'array_merge', array());
        $OUTPUT = implode(' ', array_map(array($this, 'myfun'), $lo, array_keys($lo)));
        $cookie = $this->context->cookie;
        $currency = new Currency($cookie->id_currency);
        $my_currency_iso_code = $currency->iso_code;
        $MUID = Configuration::get('WECASHUP_MUID');
        $MPK = Configuration::get('WECASHUP_PKEY');
        $MSK = Configuration::get('WECASHUP_SKEY');
        $MCUR = $my_currency_iso_code;
        $CPAY = Configuration::get('WECASHUP_CPAY');
        $TELL = Configuration::get('WECASHUP_TELECOM');
        $MWALL = Configuration::get('WECASHUP_MWALLET');
        $SPLIT = Configuration::get('WECASHUP_SPAY');
        $APPN = Configuration::get('WECASHUP_PBNAME');
        $CUSTE = $this->context->customer->email;
        $LAN = Configuration::get('WECASHUP_PBLAN');
        $DIMG = Configuration::get('WECASHUP_PBLOGO');

        $this->context->smarty->assign(array(
            'nbProducts' => $cart->nbProducts(),
            'cust_currency' => $cart->id_currency,
            'currencies' => $this->module->getCurrency((int)$cart->id_currency),
            'total' => $cart->getOrderTotal(true, Cart::BOTH),
            'this_path' => $this->module->getPathUri(),
            'this_path_bw' => $this->module->getPathUri(),
            'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->module->name.'/',
            'MUID' => $MUID,
            'MPK' => $MPK,
            'MSK' => $MSK,
            'MCUR' => $MCUR,
            'CPAY' => $CPAY,
            'TELL' => $TELL,
            'MWALL' => $MWALL,
            'SPLIT' => $SPLIT,
            'APPN' => $APPN,
            'CUSTE' => $CUSTE,
            'LAN' => $LAN,
            'OUTPUT' => $OUTPUT,
            'DIMG' => $DIMG,
            'BASEURL' => _PS_BASE_URL_.__PS_BASE_URI__,
            'PO' => _PS_MODULE_DIR_,
            'AMT' => (float)$cart->getOrderTotal(true, Cart::BOTH)
        ));

        $this->setTemplate('payment_execution.tpl');
    }

    public function myfun($v, $k)
    {
        return sprintf('%s=%s', $k, $v);
    }
}
