<?php
/**
* 2007-2017 PrestaShop
*
* Jms Page Builder
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

include_once(_PS_MODULE_DIR_.'jmspagebuilder/jmsHomepage.php');
class JmsPageBuilderInstall
{
    public function createTable()
    {
        $sql = array();
        $sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'jmspagebuilder`;
                    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'jmspagebuilder` (
                    `id_homepage` int(10) unsigned NOT NULL AUTO_INCREMENT,
                    `id_shop` int(10) unsigned NOT NULL,
                    PRIMARY KEY (`id_homepage`,`id_shop`)
                ) ENGINE='._MYSQL_ENGINE_.'  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        $sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'jmspagebuilder_homepages`;
                CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'jmspagebuilder_homepages` (
                  `id_homepage` int(11) NOT NULL AUTO_INCREMENT,
                  `title` varchar(100) NOT NULL,
                  `css_file` varchar(30) NOT NULL,
                  `js_file` varchar(30) NOT NULL,
                  `home_class` varchar(100) NOT NULL,
                  `params` mediumtext NOT NULL,
                  `active` tinyint(1) NOT NULL,
                  `ordering` int(11) NOT NULL,
                  PRIMARY KEY (`id_homepage`)
                ) ENGINE='._MYSQL_ENGINE_.'  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        foreach ($sql as $s) {
            if (!Db::getInstance()->execute($s)) {
                return false;
            }
        }
    }
    public function _addHomePage($title, $importfile, $ordering, $css_file = '', $js_file = '', $home_class = '')
    {
        $homepage = new JmsHomepage();
        $homepage->title = $title;
        $homepage->css_file = $css_file;
        $homepage->js_file = $js_file;
        $homepage->home_class = $home_class;
        $homepage->ordering = $ordering;
        $homepage->active = 1;
        $jsonfile = fopen(_PS_ROOT_DIR_.'/modules/jmspagebuilder/json/'.$importfile, "r") or die("Unable to open file!");
        $jsontext = fread($jsonfile, filesize(_PS_ROOT_DIR_.'/modules/jmspagebuilder/json/'.$importfile));
        $homepage->params = $jsontext;
        $homepage->add();
        return $homepage->id;
    }
    public function installDemo()
    {
        $home1_id = $this->_addHomePage('Basel - Home 1', 'basel_home_1.txt', 0, 'home1.css', 'home1.js', 'home1');
		Configuration::updateValue('JPB_HOMEPAGE', $home1_id);
		$home2_id = $this->_addHomePage('Basel - Home 2', 'basel_home_2.txt', 1, 'home2.css', 'home2.js', 'home2');
		$home3_id = $this->_addHomePage('Basel - Home 3', 'basel_home_3.txt', 2, 'home3.css', 'home3.js', 'home3');
		$home4_id = $this->_addHomePage('Basel - Home 4', 'basel_home_4.txt', 3, 'home4.css', 'home4.js', 'home4');
		$home5_id = $this->_addHomePage('Basel - Home 5', 'basel_home_5.txt', 4, 'home5.css', 'home5.js', 'home5');
		$home6_id = $this->_addHomePage('Basel - Home 6', 'basel_home_6.txt', 5, 'home6.css', 'home6.js', 'home6');
		$home7_id = $this->_addHomePage('Basel - Home 7', 'basel_home_7.txt', 6, 'home7.css', 'home7.js', 'home7');
		$home8_id = $this->_addHomePage('Basel - Home 8', 'basel_home_8.txt', 7, 'home8.css', 'home8.js', 'home8');
		$home9_id = $this->_addHomePage('Basel - Home 9', 'basel_home_9.txt', 8, 'home9.css', 'home9.js', 'home9');
		$home10_id = $this->_addHomePage('Basel - Home 10', 'basel_home_10.txt', 9, 'home10.css', 'home10.js', 'home10');
    }
}
