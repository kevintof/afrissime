<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_473e1b8e3d2c54d2b831bb559877588e'] = 'Veuillez saisir au moins 3 caractères';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_763dab47c9a9febb38cd90224d9f138a'] = 'Aucun résultat pour cette recherche';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_2110f74617fdabfa778c48806fdcc9a8'] = 'Que recherchez vous?';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_d8ddceb83a1e4a10d11d2c0dbfd2aaaf'] = 'JMS AJAX Search';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_8f31eb2413dea86c661532d4cf973d2f'] = 'An invalid number of products has been specified.';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_f4f70727dc34561dfde1a3c529b6205c'] = 'Réglages';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_d44168e17d91bac89aab3f38d8a4da8e'] = 'Number of products to be displayed';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_d80a06fda058b02ca2ab481cb6c6526b'] = 'Afficher la description';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_31dcfe3fddb9b5ba296be87d87b1a0a3'] = 'Le nombre de caractère minimum pour la description';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_3b916fc8872fea5743ec9d4b4fee2de3'] = 'Afficher le prix';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_67d32efccb13e15ce36e763c7ae970f5'] = 'Afficher l\'image';
$_MODULE['<{jmsajaxsearch}prestashop>jmsajaxsearch_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
