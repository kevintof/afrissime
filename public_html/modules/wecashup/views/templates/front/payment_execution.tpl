{*
* 2007-2019 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2019 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}
    <a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Go back to the Checkout' mod='wecashup'}">{l s='Checkout' mod='wecashup'}</a><span class="navigation-pipe">{$navigationPipe|escape:'html':'UTF-8'}</span>{l s='WeCashUp payment' mod='wecashup'}
{/capture}

<h1 class="page-heading">{l s='Order summary' mod='wecashup'}</h1>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
    <p class="warning">{l s='Your shopping cart is empty.' mod='wecashup'}</p>
{else}

<form action="{$link->getModuleLink('wecashup', 'validation', [], true)|escape:'html'}" method="post">
    <div class="box">
        <h3 class="page-subheading">{l s='WeCashUp payment' mod='wecashup'}</h3>
        <p>
            <img src="{$this_path_bw|escape:'htmlall':'utf-8'}logo.png" alt="{l s='WeCashUp' mod='wecashup'}" width="86" height="49" style="float:left; margin: 0px 10px 5px 0px;" />
            {l s='You have chosen to pay by WeCashUp.' mod='wecashup'}
            <br/><br />
            {l s='Here is a short summary of your order:' mod='wecashup'}
        </p>
        <p style="margin-top:20px;">
            - {l s='The total amount of your order is' mod='wecashup'}
            <span id="amount" class="price">{displayPrice price=$total}</span>
            {if $use_taxes == 1}
                {l s='(tax incl.)' mod='wecashup'}
            {/if}
        </p>

        <div class="form-group">
            {if $currencies|@count > 1}
                {l s='We allow several currencies to be sent via WeCashUp.' mod='wecashup'}
                <br /><br />
                <label>{l s='Choose one of the following:' mod='wecashup'}</label>
                <select id="currency_payement" name="currency_payement" style="width: 267px;" class="form-control" onchange="setCurrency($('#currency_payement').val());">
                    {foreach from=$currencies item=currency}
                        <option value="{$currency.id_currency|escape:'htmlall':'utf-8'}" {if $currency.id_currency == $cust_currency}selected="selected"{/if}>{$currency.name|escape:'htmlall':'utf-8'}</option>
                    {/foreach}
                </select>
            {else}
                {l s='We allow the following currency to be sent via WeCashUp:' mod='wecashup'}&nbsp;<b>{$currencies.0.name|escape:'htmlall':'utf-8'}</b>
                <input type="hidden" name="currency_payement" value="{$currencies.0.id_currency|escape:'htmlall':'utf-8'}" />
            {/if}
        </div>

        <p>
            {l s='WeCashUp account information will be displayed on the next page.' mod='wecashup'}
        </p>
    </div>


    <p class="cart_navigation clearfix" id="payment-confirmation">
        <a class="button-exclusive btn btn-default" href="{$link->getPageLink('order', true, NULL, " step=3")|escape:'html'}">
            <i class="icon-chevron-left"></i>{l s='Other payment methods' mod='wecashup'}
        </a>

        <script async="" src="https://www.wecashup.cloud/library/MobileMoney.js" class="wecashup_button"
        data-receiver-uid="{$MUID|escape:'htmlall':'UTF-8'}"
        data-receiver-public-key="{$MPK|escape:'htmlall':'UTF-8'}"
        data-transaction-receiver-total-amount="{$AMT|escape:'htmlall':'UTF-8'}"
        data-transaction-receiver-reference=""
        data-transaction-sender-reference="{$CUSTE|escape:'htmlall':'UTF-8'}"

        data-image="{$DIMG|escape:'htmlall':'UTF-8'}"
        data-transaction-method="pull"

        data-name="{$APPN|escape:'htmlall':'UTF-8'}"
        data-cash="{$CPAY|escape:'htmlall':'UTF-8'}"
        data-telecom="{$TELL|escape:'htmlall':'UTF-8'}"
        data-m-wallet="{$MWALL|escape:'htmlall':'UTF-8'}"
        data-split="{$SPLIT|escape:'htmlall':'UTF-8'}"
        data-sender-lang="{$LAN|escape:'htmlall':'UTF-8'}"

        data-marketplace-mode="false"
        {$OUTPUT|escape:'htmlall':'UTF-8'} >
        </script>
    </p>
</form>
{/if}
