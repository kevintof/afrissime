<?php
/**
* 2007-2017 PrestaShop
*
* Slider Layer module for prestashop
*
*  @author    Joommasters <joommasters@gmail.com>
*  @copyright 2007-2017 Joommasters
*  @license   license http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*  @Website: http://www.joommasters.com
*/

$query = "DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `class_suffix` varchar(100) NOT NULL,
  `bg_type` int(10) NOT NULL DEFAULT '1',
  `bg_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bg_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '#FFF',
  `slide_link` varchar(100) NOT NULL,
  `order` int(10) NOT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_slide`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jms_slides` (`id_slide`, `title`, `class_suffix`, `bg_type`, `bg_image`, `bg_color`, `slide_link`, `order`, `status`) VALUES
(7, 'Home 1 - Slider 1', '', 1, '5355bcb7df9d789a6846418f9051769d.jpg', '', '', 0, 1),
(8, 'Home 1 - Slider 2', '', 1, '2046927ccd7a0687b261f3be3d8e1b71.jpg', '', '', 1, 1),
(10, 'Home 2 - Slider 1', '', 1, 'ca395d8418e6036edb3ef403179b29e9.jpg', '', '', 2, 1),
(11, 'Home 2 - Slider 2', '', 1, '8323d3f8c727c4c476dafe6aa884b385.jpg', '', '', 3, 1),
(12, 'Home 3 - Slider 1', '', 0, '', '#000000', '', 4, 1),
(13, 'Home 3 - Slider 2', '', 1, 'a2d3af73561cb6133a8f5e2131755188.jpg', '', '', 5, 1),
(14, 'Home 4 - Slider 1', '', 1, '9c72d85cc28617263f35b1d927e466f7.jpg', '', '', 6, 1),
(15, 'Home 4 - Slider 2', '', 1, 'f029f7eaafa138035ccd1fb04b708ee7.jpg', '', '', 7, 1),
(16, 'Home 5 - Slider 1', '', 1, '140595c4fcf1ef05166b89b4bf4bd683.jpg', '', '', 8, 1),
(17, 'Home 5 - Slider 2', '', 1, 'dcf1a7980e2d0bfc2ce72aadca79f618.jpg', '', '', 9, 1),
(18, 'Home 6 - Slider 1', '', 0, '', '#f4f4f4', '', 10, 1),
(19, 'Home 6 - Slider 2', '', 0, '', '#f4f4f4', '', 11, 1),
(20, 'Home 7 - Slider 1', '', 1, 'cab5d76280cc0ef071418aa70b97700f.jpg', '', '', 12, 1),
(21, 'Home 7 - Slider 2', '', 1, '13009a80350a9d42024e2d6f06a61fa6.jpg', '', '', 13, 1),
(22, 'Home 9 - Slider 1', '', 1, 'fec98280ebad1c3950f461b1c0ce4c8a.jpg', '', '', 16, 1),
(23, 'Home 9 - Slider 2', '', 1, 'ffe561cbef1242d2454df273a56c008e.jpg', '', '', 17, 1),
(24, 'Home 10 - Slider 1', '', 1, '8c02767c173acdfde0df7c3d64ee903e.jpg', '', '', 18, 1),
(25, 'Home 10 - Slider 2', '', 1, '3a6d25861ad7f81bf1598312ce303004.jpg', '', '', 19, 1),
(26, 'Home 8 - Slider 1', '', 1, 'd4003a372ce6bcdf683be44425800099.jpg', '', '', 14, 1),
(27, 'Home 8 - Slider 2', '', 1, '44f841973245c3ae35cebe3484afb343.jpg', '', '', 15, 1);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_lang`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_lang` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) NOT NULL,
  PRIMARY KEY (`id_slide`,`id_lang`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jms_slides_lang` (`id_slide`, `id_lang`) VALUES
(7, 0),
(8, 0),
(10, 0),
(11, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 0),
(24, 0),
(25, 0),
(26, 0),
(27, 0);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_layers`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_layers` (
  `id_layer` int(10) NOT NULL AUTO_INCREMENT,
  `id_slide` int(10) NOT NULL,
  `data_title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data_class_suffix` varchar(50) NOT NULL,
  `data_fixed` int(10) NOT NULL DEFAULT '0',
  `data_delay` int(10) NOT NULL DEFAULT '1000',
  `data_time` int(10) NOT NULL DEFAULT '1000',
  `data_x` int(10) NOT NULL DEFAULT '0',
  `data_y` int(10) NOT NULL DEFAULT '0',
  `data_in` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'left',
  `data_out` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'right',
  `data_ease_in` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'linear',
  `data_ease_out` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'linear',
  `data_step` int(10) NOT NULL DEFAULT '0',
  `data_special` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'cycle',
  `data_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `data_image` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data_html` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data_video` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `data_video_controls` int(10) NOT NULL DEFAULT '1',
  `data_video_muted` int(10) NOT NULL DEFAULT '0',
  `data_video_autoplay` int(10) NOT NULL DEFAULT '1',
  `data_video_loop` int(10) NOT NULL DEFAULT '1',
  `data_video_bg` int(10) NOT NULL DEFAULT '0',
  `data_font_size` int(10) NOT NULL DEFAULT '14',
  `data_line_height` int(10) NOT NULL,
  `data_style` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT 'normal',
  `data_color` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '#FFFFFF',
  `data_width` int(10) NOT NULL,
  `data_height` int(10) NOT NULL,
  `data_order` int(10) NOT NULL,
  `data_status` int(10) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_layer`,`id_slide`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=163 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jms_slides_layers` (`id_layer`, `id_slide`, `data_title`, `data_class_suffix`, `data_fixed`, `data_delay`, `data_time`, `data_x`, `data_y`, `data_in`, `data_out`, `data_ease_in`, `data_ease_out`, `data_step`, `data_special`, `data_type`, `data_image`, `data_html`, `data_video`, `data_video_controls`, `data_video_muted`, `data_video_autoplay`, `data_video_loop`, `data_video_bg`, `data_font_size`, `data_line_height`, `data_style`, `data_color`, `data_width`, `data_height`, `data_order`, `data_status`) VALUES
(86, 7, 'Large Text', 'h1-large-text', 0, 1000, 1500, 395, 130, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '2017\r\n', '', 0, 0, 0, 0, 0, 250, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(87, 7, 'Medium Text', 'h1-medium-text', 0, 2000, 2500, 392, 215, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Man Bags\r\n', '', 0, 0, 0, 0, 0, 120, 0, 'normal', '#ffffff', 200, 50, 1, 1),
(88, 7, 'Button', 'h1-button', 0, 2000, 3000, 392, 422, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn btn-active btn-effect\">Learn more</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 3, 1),
(89, 7, 'Image', 'slide1-img', 0, 1000, 1500, 1080, -80, 'fade', 'fade', 'linear', 'linear', 0, '', 'image', 'main-big-baner-1-01.png', '', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#FFFFFF', 417, 650, 4, 1),
(90, 7, 'Small Text', 'h1-small-text', 0, 2000, 2500, 392, 328, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', 'New Men\'s Collection', '', 0, 0, 0, 0, 0, 73, 0, 'normal', '#ffffff', 200, 50, 2, 1),
(91, 8, 'Large text', 'h1-large-text', 0, 1000, 1500, 716, 126, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '2017', '', 0, 0, 0, 0, 0, 250, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(92, 8, 'Medium Text', 'h1-medium-text', 0, 2000, 2700, 640, 213, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'New Style', '', 0, 0, 0, 0, 0, 120, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(93, 8, 'Small Text', 'h1-small-text', 0, 2000, 2700, 640, 328, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', 'Boots Collection For Men', '', 0, 0, 0, 0, 0, 73, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(94, 10, 'Medium Text', 'h2-medium-text', 0, 1000, 1500, 840, 152, 'bottom', 'top', 'easeInQuart', 'easeInQuart', 0, '', 'text', '', '<div class=\"custom\">\r\n<span class=\"text\">New Collections</span>\r\n<span class=\"b-title_separator\"><span></span></span>\r\n</div>', '', 0, 0, 0, 0, 0, 39, 0, 'normal', '#ee4684', 200, 50, 0, 1),
(95, 10, 'Large-text', 'h2-large-text', 0, 1400, 2000, 549, 247, 'bottom', 'top', 'easeInQuart', 'easeInQuad', 0, '', 'text', '', 'BRAND YOU MUST HAVE', '', 0, 0, 0, 0, 0, 72, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(96, 10, 'Small Text', 'h2-small-text', 0, 2400, 3000, 640, 323, 'bottom', 'top', 'easeInQuart', 'easeInQuart', 0, '', 'text', '', 'Suspendisse ridiculus parturient ac a dui cursus interdum dignissim netus habitant                    ', '', 0, 0, 0, 0, 0, 16, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(97, 10, 'Button', 'h2-button', 0, 2900, 3500, 870, 370, 'bottom', 'top', 'easeInQuart', 'easeInQuart', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">Learn more</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(98, 11, 'Medium Text', 'h2-medium-text', 0, 1000, 1500, 840, 152, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', '<div class=\"custom\">\r\n<span class=\"text\">New Collections</span>\r\n<span class=\"b-title_separator\"><span></span></span>\r\n</div>', '', 0, 0, 0, 0, 0, 39, 0, 'normal', '#ee4684', 200, 50, 0, 1),
(99, 11, 'Large Text', 'h2-large-text', 0, 1400, 2000, 549, 247, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'BRAND YOU MUST HAVE', '', 0, 0, 0, 0, 0, 72, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(100, 11, 'Small Text', 'h2-small-text', 0, 1200, 2300, 640, 323, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', 'Suspendisse ridiculus parturient ac a dui cursus interdum dignissim netus habitant', '', 0, 0, 0, 0, 0, 16, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(101, 11, 'Button', 'h2-button', 0, 1000, 1500, 870, 370, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">Learn more</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(102, 12, 'Image Text', '', 0, 1400, 2000, 572, 127, 'fade', 'fade', 'linear', 'linear', 0, '', 'image', 'logo-maurice-lacroix.png', '', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#FFFFFF', 200, 20, 0, 1),
(103, 12, 'Large Text', 'slide1-large-text', 0, 1900, 2500, 435, 170, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', 'Time Is Now', '', 0, 0, 0, 0, 0, 70, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(104, 12, 'Small Text', 'slide1-small-text', 0, 1900, 2700, 492, 265, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', 'Ultrices ipsum duis ante non accumsan parturient\r\n<br />\r\nultrices quis nisi justo habitant id taciti.', '', 0, 0, 0, 0, 0, 16, 0, 'normal', '#ffffff', 372, 44, 0, 1),
(105, 12, 'Button', 'h3-button', 0, 2400, 3000, 590, 350, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">Learn more</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(106, 12, 'Watch', '', 0, 1000, 1500, 870, 40, 'bottom', 'top', 'easeInBounce', 'easeInBounce', 0, '', 'image', 'h3_1.jpg', '', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#FFFFFF', 808, 605, 0, 1),
(107, 13, 'Large Text', 'slide2-large-text', 0, 1500, 2000, 455, 35, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'time you enjoy wasting isn\'t wasted time.', '', 0, 0, 0, 0, 0, 39, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(108, 13, 'Small Text', 'slide2-small-text', 0, 1000, 1500, 882, 90, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', '- John Lennon', '', 0, 0, 0, 0, 0, 24, 0, 'italic', '#ffffff', 200, 50, 0, 1),
(109, 14, 'Small Text', 'h4-small-text', 0, 1500, 2000, 220, 200, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Latest summer collection from', '', 0, 0, 0, 0, 0, 20, 0, 'normal', '#adabab', 200, 50, 0, 1),
(110, 14, 'Large Text', 'h4-large-text', 0, 1000, 1500, 140, 230, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', 'DUSTY CROMWELL\r\n<br />\r\nSUNGLASSES', '', 0, 0, 0, 0, 0, 52, 0, 'normal', '#000000', 20000, 50, 0, 1),
(111, 14, 'Button', 'h4-button', 0, 1500, 2000, 292, 385, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">Buy Now</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(112, 15, 'Small Text', 'h4-small-text', 0, 1000, 1500, 220, 200, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', 'Latest summer collection from', '', 0, 0, 0, 0, 0, 20, 0, 'normal', '#adabab', 200, 50, 0, 1),
(113, 15, 'Large Text', 'h4-large-text', 0, 1500, 2000, 140, 230, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', 'DUSTY CROMWELL\r\n<br />\r\ncap styles', '', 0, 0, 0, 0, 0, 52, 0, 'normal', '#000000', 200, 50, 0, 1),
(114, 15, 'Button', 'h4-button', 0, 2000, 2500, 292, 385, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">Buy Now</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(115, 16, 'Medium Text', 'h5-medium-text', 0, 1500, 2000, 240, 176, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Shop & save today', '', 0, 0, 0, 0, 0, 39, 0, 'normal', '#bc9c72', 200, 50, 0, 1),
(116, 16, 'Large Text', 'h5-large-text', 0, 1000, 1500, 100, 232, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'New Collection <br />\r\njewellery', '', 0, 0, 0, 0, 0, 52, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(117, 16, 'Small Text', 'h5-small-text', 0, 1000, 1500, 135, 357, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', 'Vitae diam parturient eu vestibulum parturient nulla neque <br />\r\neu vestibulum parturient nulla', '', 0, 0, 0, 0, 0, 15, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(118, 16, 'Button', 'h5-button', 0, 1500, 2000, 240, 435, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">Learn More</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(119, 17, 'Medium Text', 'h5-medium-text', 0, 1500, 2000, 240, 176, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Chanel flowerbomb', '', 0, 0, 0, 0, 0, 39, 0, 'normal', '#bd9c72', 200, 50, 0, 1),
(120, 17, 'Large Text', 'h5-large-text', 0, 1000, 1500, 100, 232, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Latest designer <br />\r\nperfumes', '', 0, 0, 0, 0, 0, 52, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(121, 17, 'Small Text', 'h5-small-text', 0, 1000, 1500, 135, 357, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', 'Vitae diam parturient eu vestibulum parturient nulla neque <br />\r\neu vestibulum parturient nulla', '', 0, 0, 0, 0, 0, 15, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(122, 17, 'Button', 'h5-button', 0, 1500, 2000, 240, 435, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">BUY NOW</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(123, 18, 'Image', '', 0, 1000, 1500, 280, 50, 'fade', 'fade', 'linear', 'linear', 0, '', 'image', 'furniture-easy-chair.png', '', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#FFFFFF', 536, 402, 0, 1),
(124, 18, 'Small Text', 'h6-small-text', 0, 2000, 2500, 50, 110, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Best furniture for your castle', '', 0, 0, 0, 0, 0, 20, 0, 'italic', '#2f8653', 200, 50, 0, 1),
(125, 18, 'Large Text', 'h6-large-text', 0, 1500, 2000, 25, 150, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', 'YELLOW SOFA', '', 0, 0, 0, 0, 0, 48, 0, 'bold', '#2d2d2d', 200, 50, 0, 1),
(126, 18, 'Price', 'h6-price', 0, 1500, 2000, 45, 204, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', '$799', '', 0, 0, 0, 0, 0, 120, 0, 'normal', '#e5e5e5', 200, 50, 0, 1),
(127, 18, 'link', 'h6-link', 0, 2000, 2500, 143, 325, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" class=\"other-link\">Shop other</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#6d6d6d', 200, 50, 0, 1),
(128, 19, 'Image', '', 0, 1000, 1500, 380, 60, 'fade', 'fade', 'linear', 'linear', 0, '', 'image', 'furniture-easy-chair-wood.png', '', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#FFFFFF', 350, 357, 0, 1),
(129, 19, 'Small Text', 'h6-small-text', 0, 2500, 3000, 70, 110, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Best furniture for your castle', '', 0, 0, 0, 0, 0, 20, 0, 'italic', '#2f8653', 200, 50, 0, 1),
(130, 19, 'Large Text', 'h6-large-text', 0, 2000, 2500, 25, 150, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'WOODEN CHAIR', '', 0, 0, 0, 0, 0, 48, 0, 'bold', '#2d2d2d', 200, 50, 0, 1),
(131, 19, 'Price', 'h6-price', 0, 2000, 2500, 65, 204, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '$797', '', 0, 0, 0, 0, 0, 120, 0, 'normal', '#e5e5e5', 200, 50, 0, 1),
(132, 19, 'Link', 'h6-link', 0, 2500, 3000, 163, 325, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" class=\"other-link\">Shop other</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(133, 20, 'Medium Text', 'h2-medium-text', 0, 1500, 2000, 840, 152, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', '<div class=\"custom\">\r\n<span class=\"text\">New Collections</span>\r\n<span class=\"b-title_separator\"><span></span></span>\r\n</div>', '', 0, 0, 0, 0, 0, 39, 0, 'normal', '#b63d44', 200, 50, 0, 1),
(134, 20, 'Large Text', 'h2-large-text', 0, 1000, 1500, 549, 247, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'BRAND YOU MUST HAVE', '', 0, 0, 0, 0, 0, 72, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(135, 20, 'Small Text', 'h2-small-text', 0, 1000, 1500, 640, 323, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', 'Suspendisse ridiculus parturient ac a dui cursus interdum dignissim netus habitant       ', '', 0, 0, 0, 0, 0, 16, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(136, 20, 'Button', 'h2-button', 0, 1500, 2000, 870, 370, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">Learn more</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(137, 21, 'Medium Text', 'h2-medium-text', 0, 1500, 2000, 840, 152, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', '<div class=\"custom\">\r\n<span class=\"text\">New Collections</span>\r\n<span class=\"b-title_separator\"><span></span></span>\r\n</div>', '', 0, 0, 0, 0, 0, 39, 0, 'normal', '#ee4682', 200, 50, 0, 1),
(138, 21, 'Large Text', 'h2-large-text', 0, 1000, 1500, 569, 247, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'BEST PRODUCTS EVER', '', 0, 0, 0, 0, 0, 72, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(139, 21, 'Small Text', 'h2-small-text', 0, 1000, 1500, 640, 323, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', 'Suspendisse ridiculus parturient ac a dui cursus interdum dignissim netus habitant', '', 0, 0, 0, 0, 0, 16, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(140, 21, 'Button', 'h2-button', 0, 1500, 2000, 870, 370, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" title=\"Shop\" class=\"btn\">Learn more</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(141, 22, 'Small Text', 'h9-small-text', 0, 1500, 2000, 633, 215, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'Shop & save today', '', 0, 0, 0, 0, 0, 22, 0, 'normal', '#000000', 200, 50, 0, 1),
(142, 22, 'Large Text', 'h9-large-text', 0, 1000, 1500, 432, 262, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', 'Pink and gold lipsticks for every\r\n<br />\r\nskintone new trending in 2016 season', '', 0, 0, 0, 0, 0, 30, 0, 'bold', '#000000', 200, 50, 0, 1),
(143, 22, 'Button', 'h9-button', 0, 1500, 2000, 682, 384, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" class=\"btn\">LEARN MORE</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(144, 23, 'Small Text', 'h9-small-text', 0, 1500, 2000, 633, 215, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'Shop & save today', '', 0, 0, 0, 0, 0, 22, 0, 'normal', '#000000', 200, 50, 0, 1),
(145, 23, 'Large Text', 'h9-large-text', 0, 1000, 1500, 432, 262, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', 'Pink and gold lipsticks for every\r\n<br />\r\nskintone new trending in 2016 season', '', 0, 0, 0, 0, 0, 30, 0, 'normal', '#000000', 200, 50, 0, 1),
(146, 23, 'Button', 'h9-button', 0, 1500, 2000, 682, 384, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" class=\"btn\">LEARN MORE</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(147, 24, 'Image', '', 0, 1000, 1500, 1160, 0, 'fade', 'fade', 'linear', 'linear', 0, '', 'image', 'dark-sld1.png', '', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#FFFFFF', 311, 600, 0, 1),
(148, 24, 'Medium Text', 'h10-medium-text', 0, 2000, 2500, 570, 182, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Chanel flowerbomb', '', 0, 0, 0, 0, 0, 40, 0, 'normal', '#bc9c72', 200, 50, 0, 1),
(149, 24, 'Large Text', 'h10-large-text', 0, 1500, 2000, 433, 243, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Latest designer <br />\r\nperfumes', '', 0, 0, 0, 0, 0, 54, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(150, 24, 'Small Text', 'h10-small-text', 0, 1500, 2000, 460, 375, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', 'Vitae diam parturient eu vestibulum parturient nulla neque <br />\r\neu vestibulum parturient nulla', '', 0, 0, 0, 0, 0, 16, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(151, 24, 'Button', 'h10-button', 0, 2000, 2500, 605, 450, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" class=\"btn\">Buy Now</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(152, 25, 'Image', '', 0, 1000, 1500, 990, 130, 'fade', 'fade', 'linear', 'linear', 0, '', 'image', 'dark-sld2.png', '', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#FFFFFF', 500, 311, 0, 1),
(153, 25, 'Medium Text', 'h10-medium-text', 0, 2000, 2500, 580, 182, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'Shop & save today', '', 0, 0, 0, 0, 0, 40, 0, 'normal', '#bc9c72', 200, 50, 0, 1),
(154, 25, 'Large Text', 'h10-large-text', 0, 1500, 2000, 433, 243, 'top', 'bottom', 'linear', 'linear', 0, '', 'text', '', 'New Collection <br />\r\njewellery', '', 0, 0, 0, 0, 0, 54, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(155, 25, 'Small Text', 'h10-small-text', 0, 1500, 2000, 460, 375, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', 'Vitae diam parturient eu vestibulum parturient nulla neque <br />\r\neu vestibulum parturient nulla', '', 0, 0, 0, 0, 0, 16, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(156, 25, 'Button', 'h10-button', 0, 2000, 2500, 605, 450, 'bottom', 'top', 'linear', 'linear', 0, '', 'text', '', '<a href=\"#\" class=\"btn\">Buy Now</a>', '', 0, 0, 0, 0, 0, 14, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(157, 26, 'Large Text', 'h8-large-text', 0, 1000, 1500, 100, 135, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', 'New <br />\r\nArrivals', '', 0, 0, 0, 0, 0, 80, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(158, 26, 'Medium Text', 'h8-medium-text', 0, 1500, 2000, 155, 330, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'SUMMER COLLECTION', '', 0, 0, 0, 0, 0, 33, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(159, 26, 'Small Text', 'h8-small-text', 0, 1500, 2000, 220, 390, 'right', 'left', 'linear', 'linear', 0, '', 'text', '', 'Hot Trending', '', 0, 0, 0, 0, 0, 40, 0, 'normal', '#fb9090', 200, 50, 0, 1),
(160, 27, 'Large Text', 'h8-large-text', 0, 1000, 1500, 170, 140, 'fade', 'fade', 'linear', 'linear', 0, '', 'text', '', 'New <br />\r\nArrivals', '', 0, 0, 0, 0, 0, 80, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(161, 27, 'Medium Text', 'h8-medium-text', 0, 1500, 2000, 220, 330, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'SUMMER COLLECTION', '', 0, 0, 0, 0, 0, 33, 0, 'normal', '#ffffff', 200, 50, 0, 1),
(162, 27, 'Small Text', 'h8-small-text', 0, 1500, 2000, 280, 390, 'left', 'right', 'linear', 'linear', 0, '', 'text', '', 'Hot Trending', '', 0, 0, 0, 0, 0, 40, 0, 'normal', '#fb9090', 200, 50, 0, 1);

DROP TABLE IF EXISTS `_DB_PREFIX_jms_slides_shop`;
CREATE TABLE IF NOT EXISTS `_DB_PREFIX_jms_slides_shop` (
  `id_slide` int(10) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) NOT NULL,
  PRIMARY KEY (`id_slide`,`id_shop`)
) ENGINE=_MYSQL_ENGINE_ AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

INSERT INTO `_DB_PREFIX_jms_slides_shop` (`id_slide`, `id_shop`) VALUES
(7, 1),
(8, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1);
";
