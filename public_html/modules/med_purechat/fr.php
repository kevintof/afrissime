<?php //modified by module sd_translate version 1.0.3.0 on Fri, 16 Oct 2015 17:32:29 +0200

global $_MODULE;
$_MODULE = array();
$_MODULE['<{med_purechat}prestashop>med_purechat_e75140210378b21812bae5db85560751'] = 'Intégration de PureChat';
$_MODULE['<{med_purechat}prestashop>med_purechat_4f6ac4553de104768fda6f6676ed5b40'] = 'Intégrer le script Purechat sur votre site.';
$_MODULE['<{med_purechat}prestashop>med_purechat_c888438d14855d7d96a2724ee9c306bd'] = 'Paramètres mis à jour';
$_MODULE['<{med_purechat}prestashop>med_purechat_a8bf906a35ef0cc9970ebeadb7a0a626'] = 'Avant toute chose, vous devez ouvrir un compte gratuit sur';
$_MODULE['<{med_purechat}prestashop>med_purechat_f4f70727dc34561dfde1a3c529b6205c'] = 'Configuration';
$_MODULE['<{med_purechat}prestashop>med_purechat_7bfa4b10ff46ff3037b2cfadaca2d7c3'] = 'Solutions de chat en direct de PureChat : Créer votre compte';
$_MODULE['<{med_purechat}prestashop>med_purechat_2f44aaad410f03e67db38744ab7a3769'] = 'PureChat solutions de chat en direct';
$_MODULE['<{med_purechat}prestashop>med_purechat_81eeab9506186e2dca8faefa78d54067'] = 'Exemple :';
$_MODULE['<{med_purechat}prestashop>med_purechat_75d81c29140ff78050d77bfbdcad4623'] = 'Pour configurer ce module, après votre inscription sur';
$_MODULE['<{med_purechat}prestashop>med_purechat_a291e0a15469cbdb91f1e0d7084f0c60'] = 'récupérer le code d\'insertion du script et renseignez l\'identifiant de votre site représentez en gras et rouge sur l\'exemple ci-dessous. Inscrivez cet identifiant ci-dessus.';
$_MODULE['<{med_purechat}prestashop>med_purechat_c4c95c36570d5a8834be5e88e2f0f6b2'] = 'Informations';
$_MODULE['<{med_purechat}prestashop>med_purechat_13b9211b0d45657918eb30b4a6c0826c'] = 'Découvrez nos autre dev sur :';
$_MODULE['<{med_purechat}prestashop>med_purechat_3a2d5fe857d8f9541136a124c2edec6c'] = 'Ou';
$_MODULE['<{med_purechat}prestashop>med_purechat_1aa2e2ff6972016c11c692bfb5c43909'] = 'Publicité';
$_MODULE['<{med_purechat}prestashop>med_purechat_95d6681479d6e18c6482789a04aa18cb'] = 'Liste des Modules';
$_MODULE['<{med_purechat}prestashop>med_purechat_2d5013daeb0b3825bf72a1cdda4f5223'] = 'Remplacer le flux des produits Addons par PrestatoolBox';
$_MODULE['<{med_purechat}prestashop>med_purechat_93cba07454f06a4a960172bbd6e2a435'] = 'Oui';
$_MODULE['<{med_purechat}prestashop>med_purechat_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Non';
$_MODULE['<{med_purechat}prestashop>med_purechat_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';

?>