<?php
/**
 * 2008-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * Read in the module
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mediacom87 <support@mediacom87.net>
 * @copyright 2008-2015 Mediacom87
 * @license   define in the module
 * @version 1.1.0
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

/**
 * upgrade_module_1_1_0 function.
 *
 * @access public
 * @param mixed $module
 * @return void
 */
function upgrade_module_1_1_0($module)
{
    Configuration::updateValue('MED_MODULES_LIST', true);
    return true; // Return true if success.
}
