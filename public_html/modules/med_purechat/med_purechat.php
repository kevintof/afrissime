<?php
/**
 * 2008-2015 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * Read in the module
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mediacom87 <support@mediacom87.net>
 * @copyright 2008-2015 Mediacom87
 * @license   define in the module
 * @version 1.1.0
 */

class med_purechat extends Module
{
    /**
     * __construct function.
     *
     * @access public
     * @return void
     */
    public function __construct()
    {
        $this->name = 'med_purechat';
        $this->tab = 'advertising_marketing';
        $this->version = '1.1.0';
        $this->need_instance = 1;
        $this->author = 'Mediacom87';
        parent::__construct();
        $this->displayName = $this->l('PureChat integration');
        $this->description = $this->l('Integrate your Purechat script on your site.');

        $this->affiliateurl = 'http://bit.ly/aff_purechat';

        $this->confirmUninstall = $this->l('Are you sure you want to delete your details ?');

        $this->bootstrap = true;
        if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
            $this->moduleList();
        }
    }

    /**
     * install function.
     *
     * @access public
     * @return void
     */
    public function install()
    {
        $this->addAsTrusted();
        if (!parent::install() || !$this->registerHook('footer') || !Configuration::updateValue('MED_PURECHAT', '') || !Configuration::updateValue('MED_MODULES_LIST', true) || !Configuration::updateValue('MED_MODULE_TIME', 0)) {
            return false;
        }
        return true;
    }

    /**
     * uninstall function.
     *
     * @access public
     * @return void
     */
    public function uninstall()
    {
        if (!Configuration::deleteByName('MED_PURECHAT') || !Configuration::deleteByName('MED_MODULE_TIME') || !parent::uninstall()) {
            return false;
        }
        return true;
    }

    /**
     * getContent function.
     *
     * @access public
     * @return void
     */
    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('saveModule')) {
            if (Configuration::get('MED_MODULES_LIST') != Tools::getValue('MED_MODULES_LIST')) {
                Configuration::deleteByName('MED_MODULE_TIME');
            }
            if (Configuration::updateValue('MED_MODULES_LIST', Tools::getValue('MED_MODULES_LIST'))) {
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }
        if (Tools::isSubmit('submitConf')) {
            if (Configuration::updateValue('MED_PURECHAT', trim(Tools::getValue('purechatscript')))) {
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }
        $output .= '
            <h2>'.$this->displayName.'</h2>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        ';
        return $output.$this->displayForm();
    }

    /**
     * displayForm function.
     *
     * @access public
     * @return void
     */
    public function displayForm()
    {
        //return '<p>'.$this->l('Above all things, subscribe to').' <a href="'.$this->affiliateurl.'" title="'.$this->l('Above all things, subscribe to').' PURECHAT" target="_blank" style="color:orange"><b>PURECHAT</b></a></p>';

        return '
			'.$this->panelHeading($this->l('Settings')).'
			<form method="post">
				<label><a href="'.$this->affiliateurl.'" target="_blank" title="'.$this->l('PureChat live chat solutions Create your account').'"><img src="'.$this->_path.'logo.png" alt="'.$this->l('PureChat live chat solutions').'" /></a></label>
				<div class="margin-form"><input type="text" name="purechatscript" value="'.Configuration::get('MED_PURECHAT').'" placeholder="'.$this->l('Example:').' 8266ec11-4efa-403c-bb2f-504a3e19cf7d" size="50" />
					<p>'.$this->l('To configure this module, after registering on').' <a href="'.$this->affiliateurl.'" title="'.$this->l('Above all things, subscribe to').' PURECHAT" target="_blank" style="color:orange"><b>PURECHAT</b></a>, '.$this->l('get code to insert the script and find the ID of your site in bold red represent the example below. Enter the ID above.').'</p>
					<p class="clear">&lt;script type=\'text/javascript\' data-cfasync=\'false\'&gt;window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement(\'script\'); script.async = true; script.type = \'text/javascript\'; script.src = \'https://app.purechat.com/VisitorWidget/WidgetScript\'; document.getElementsByTagName(\'HEAD\').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done &amp;&amp; (!this.readyState || this.readyState == \'loaded\' || this.readyState == \'complete\')) { var w = new PCWidget({c: \'<b><span style="color:#900" title="\'.$this->l(\'Copy your site ID represented like this one\').\'">8266ec11-4efa-403c-bb2f-504a3e19cf7d</span></b>\', f: true }); done = true; } }; })();&lt;/script&gt;</p>
				</div>
				'.$this->submitButton('submitConf').'
            </form>
            '.$this->panelEnding().'

            '.$this->panelHeading($this->l('Informations'), 'fa-info-circle').'
			<h3 style="text-align:center">'.$this->l('Discover our other dev on:').'</h3>
			<p style="text-align:center"><a href="http://www.prestatoolbox.'.$this->isoCode(true).'/1_mediacom87" target="_blank"><img src="http://i.imgur.com/JK49LYo.png" alt="PrestaToolbox" height="100" /></a> '.$this->l('Or').' <a href="http://addons.prestashop.com/'.$this->isoCode().'/2_community?contributor=322" target="_blank" title="PrestaShop Addons"><img src="http://i.imgur.com/9pVjllc.png" alt="PrestaShop Addons" /></a></p>
			'.$this->displayPaypalButton('FE676S2WTUG8U').'

		'.$this->panelEnding().'

		'.$this->panelHeading($this->l('Ads'), 'fa-plus-square').'
			<p style="text-align:center">
				'.$this->displayGoogleAds('4031245955').'
			</p>
		'.$this->panelEnding().'

		'.$this->displayListModules();
    }

    /**
     * hookFooter function.
     *
     * @access public
     * @param mixed $params
     * @return void
     */
    public function hookFooter($params)
    {
        if (version_compare(_PS_VERSION_, '1.5.0.0', '>='))
            $smarty = $this->smarty;
        else
            global $smarty;
        $zopim = Configuration::get('MED_PURECHAT');
        if ($zopim) {
            $smarty->assign(array('purechat' => $zopim));
            return $this->display(__FILE__, $this->name.'.tpl');
        }
    }

    /**
     * addAsTrusted function.
     *
     * @access public
     * @return void
     */
    public function addAsTrusted()
    {
        if (defined('self::CACHE_FILE_TRUSTED_MODULES_LIST') == true) {
            if (isset($this->context->controller->controller_name) && $this->context->controller->controller_name == 'AdminModules') {
                $sxe = new SimpleXMLElement('<theme/>');

                $modules = $sxe->addChild('modules');
                $module = $modules->addChild('module');
                $module->addAttribute('action', 'install');
                $module->addAttribute('name', $this->name);

                $trusted = $sxe->saveXML();
                file_put_contents(_PS_ROOT_DIR_ . '/config/xml/themes/' . $this->name . '.xml', $trusted);
                if (is_file(_PS_ROOT_DIR_ . Module::CACHE_FILE_UNTRUSTED_MODULES_LIST)) {
                    Tools::deleteFile(_PS_ROOT_DIR_ . Module::CACHE_FILE_UNTRUSTED_MODULES_LIST);
                }
            }
        }
    }

    /**
     * displayListModules function.
     *
     * @access private
     * @return void
     */
    private function displayListModules()
    {
        if (version_compare(_PS_VERSION_, '1.5.0.0', '>=')) {
            return $this->panelHeading($this->l('Listing Modules')).'
    		    <form method="post" class="form-horizontal">
                    <div class="form-group">
    					<label class="control-label col-sm-2">'.$this->l('Replace the flow of products Addons by PrestatoolBox').'</label>
    					<div class="margin-form col-sm-9">
    						'.$this->radioButton('MED_MODULES_LIST', $this->l('Yes'), $this->l('No')).'
    					</div>
    				</div>

    				'.$this->submitButton('saveModule').'
                </form>
    		'.$this->panelEnding();
        }
    }

    /**
     * displayPaypalButton function.
     *
     * @access private
     * @param mixed $id
     * @return void
     */
    private function displayPaypalButton($id)
    {
        return '
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" style="text-align:center" target="_blank">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="hosted_button_id" value="'.$id.'">
                <input type="image" src="https://www.paypalobjects.com/fr_FR/FR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - la solution de paiement en ligne la plus simple et la plus sécurisée !">
                <img alt="" border="0" src="https://www.paypalobjects.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
            </form>
        ';
    }

    /**
     * displayGoogleAds function.
     *
     * @access private
     * @param mixed $slot
     * @return void
     */
    private function displayGoogleAds($slot)
    {
        return '
            <script type="text/javascript"><!--
				google_ad_client = "ca-pub-1663608442612102";
				google_ad_slot = "'.$slot.'";
				google_ad_width = 728;
				google_ad_height = 90;
				//-->
			</script>
			<script type="text/javascript"
			src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
			</script>
        ';
    }

    /**
     * panelHeading function.
     *
     * @access private
     * @param mixed $title
     * @param string $icon (default: 'fa-cog')
     * @return void
     */
    private function panelHeading($title, $icon = 'fa-cog')
    {
        if (version_compare(_PS_VERSION_, '1.6.0.0', '<')) {
            return '<fieldset class="space"><legend><i class="fa '.$icon.'"></i> '.$title.'</legend>';
        } else {
            return '<div class="panel"><div class="panel-heading"><i class="fa '.$icon.'"></i> '.$title.'</div>';
        }
    }

    /**
     * moduleList function.
     *
     * @access private
     * @return void
     */
    private function moduleList()
    {
        $conf = Configuration::getMultiple(array('MED_MODULES_LIST', 'MED_MODULE_TIME'));
        if (isset($conf['MED_MODULES_LIST']) && $conf['MED_MODULES_LIST']) {
            $time = time() - (23 * 60 * 60);
            if ($time > $conf['MED_MODULE_TIME']) {
                $must_have_content = Tools::file_get_contents('http://xml-feed.mediacom87.netdna-cdn.com/'.$this->isoCode().'/must_have_modules_list.xml');
                $must_have_file = _PS_ROOT_DIR_.self::CACHE_FILE_MUST_HAVE_MODULES_LIST;
                if (file_put_contents($must_have_file, $must_have_content)) {
                    Configuration::updateValue('MED_MODULE_TIME', time());
                }
            }
        }
    }

    /**
     * panelEnding function.
     *
     * @access private
     * @return void
     */
    private function panelEnding()
    {
        if (version_compare(_PS_VERSION_, '1.6.0.0', '>=')) {
            return '</div>';
        } else {
            return '</fieldset>';
        }
    }


    /**
     * submitButton function.
     *
     * @access private
     * @param string $name (default: 'save')
     * @return void
     */
    private function submitButton($name = 'save')
    {
        if (version_compare(_PS_VERSION_, '1.6.0.0', '<')) {
            return '<p><input type="submit" class="button" name="'.$name.'" value="'.$this->l('Save').'" /></p>';
        } else {
            return '<div class="panel-footer"><button class="btn btn-default" name="'.$name.'" type="submit"><i class="process-icon-save"></i> '.$this->l('Save').'</button></div>';
        }
    }

    /**
     * isoCode function.
     *
     * @access private
     * @param bool $domain (default: false)
     * @return void
     */
    private function isoCode($domain = false)
    {
        if (version_compare(_PS_VERSION_, '1.5.0.0', '<')) {
            global $cookie;
            $language = new Language($cookie->id_lang);
            $iso = $language->iso_code;
        } else {
            $iso = $this->context->language->iso_code;
        }

        if ($iso == 'fr') {
            return 'fr';
        } else if ($domain) {
                return 'com';
            } else {
            return 'en';
        }
    }

    /**
     * radioButton function.
     *
     * @access private
     * @param mixed $sauv
     * @param mixed $ok
     * @param mixed $ko
     * @return void
     */
    private function radioButton($sauv, $ok, $ko)
    {
        $result = Configuration::get($sauv);
        if (!isset($result)) {
            $result = 0;
        }

        if (version_compare(_PS_VERSION_, '1.6.0.0', '<')) {
            return '
    			<input type="radio" name="'.$sauv.'" id="'.$sauv.'_on" value="1" '.($result ? 'checked="checked" ' : '').'/>
    			<label class="t" for="'.$sauv.'_on"> <img src="../img/admin/enabled.gif" alt="'.$ok.'" title="'.$ok.'" /></label>
    			<input type="radio" name="'.$sauv.'" id="'.$sauv.'_off" value="0" '.(!$result ? 'checked="checked" ' : '').'/>
    			<label class="t" for="'.$sauv.'_off"> <img src="../img/admin/disabled.gif" alt="'.$ko.'" title="'.$ko.'" /></label>
    		';
        } else {
            return '
                <span class="switch prestashop-switch fixed-width-lg">
        			<input type="radio" name="'.$sauv.'" id="'.$sauv.'_on" value="1" '.($result ? 'checked="checked" ' : '').'/>
        			<label class="t" for="'.$sauv.'_on">'.$ok.'</label>
        			<input type="radio" name="'.$sauv.'" id="'.$sauv.'_off" value="0" '.(!$result ? 'checked="checked" ' : '').'/>
        			<label class="t" for="'.$sauv.'_off">'.$ko.'</label>
        			<a class="slide-button btn"></a>
                </span>
    		';
        }
    }
}
