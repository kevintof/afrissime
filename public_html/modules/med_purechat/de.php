<?php //modified by module sd_translate version 1.0.3.0 on Fri, 16 Oct 2015 17:33:01 +0200

global $_MODULE;
$_MODULE = array();
$_MODULE['<{med_purechat}prestashop>med_purechat_e75140210378b21812bae5db85560751'] = 'PureChat integration';
$_MODULE['<{med_purechat}prestashop>med_purechat_4f6ac4553de104768fda6f6676ed5b40'] = 'Integrieren Sie Ihr Purechat-Skript auf Ihrer Website.';
$_MODULE['<{med_purechat}prestashop>med_purechat_c888438d14855d7d96a2724ee9c306bd'] = 'Einstellungen aktualisiert';
$_MODULE['<{med_purechat}prestashop>med_purechat_a8bf906a35ef0cc9970ebeadb7a0a626'] = 'Vor allem abonnieren Dinge,';
$_MODULE['<{med_purechat}prestashop>med_purechat_f4f70727dc34561dfde1a3c529b6205c'] = 'Einstellungen';
$_MODULE['<{med_purechat}prestashop>med_purechat_7bfa4b10ff46ff3037b2cfadaca2d7c3'] = 'PureChat live-Chat-Lösungen erstellen Sie Ihr Konto';
$_MODULE['<{med_purechat}prestashop>med_purechat_2f44aaad410f03e67db38744ab7a3769'] = 'PureChat live-Chat-Lösungen';
$_MODULE['<{med_purechat}prestashop>med_purechat_81eeab9506186e2dca8faefa78d54067'] = 'Beispiel:';
$_MODULE['<{med_purechat}prestashop>med_purechat_75d81c29140ff78050d77bfbdcad4623'] = 'So konfigurieren dieses Modul nach der Registrierung auf';
$_MODULE['<{med_purechat}prestashop>med_purechat_a291e0a15469cbdb91f1e0d7084f0c60'] = 'das Skript einfügen und findet die ID Ihrer Website Fett rot dar folgenden Beispiel bekommen. Geben Sie die ID oben.';
$_MODULE['<{med_purechat}prestashop>med_purechat_c4c95c36570d5a8834be5e88e2f0f6b2'] = 'Informationen';
$_MODULE['<{med_purechat}prestashop>med_purechat_13b9211b0d45657918eb30b4a6c0826c'] = 'Entdecken Sie unsere anderen Dev auf:';
$_MODULE['<{med_purechat}prestashop>med_purechat_3a2d5fe857d8f9541136a124c2edec6c'] = 'Oder';
$_MODULE['<{med_purechat}prestashop>med_purechat_1aa2e2ff6972016c11c692bfb5c43909'] = 'Anzeigen';
$_MODULE['<{med_purechat}prestashop>med_purechat_95d6681479d6e18c6482789a04aa18cb'] = 'Liste der Module';
$_MODULE['<{med_purechat}prestashop>med_purechat_2d5013daeb0b3825bf72a1cdda4f5223'] = 'Ersetzen Sie den Fluss der Produkte Addons von PrestatoolBox';
$_MODULE['<{med_purechat}prestashop>med_purechat_93cba07454f06a4a960172bbd6e2a435'] = 'Ja';
$_MODULE['<{med_purechat}prestashop>med_purechat_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Nein';
$_MODULE['<{med_purechat}prestashop>med_purechat_c9cc8cce247e49bae79f15173ce97354'] = 'Sie sparen';

?>