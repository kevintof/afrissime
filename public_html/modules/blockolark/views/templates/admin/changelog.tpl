{*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade this module to newer
* versions in the future. If you wish to customize this module for your
* needs please refer to http://doc.prestashop.com/display/PS15/Overriding+default+behaviors
* #Overridingdefaultbehaviors-Overridingamodule%27sbehavior for more information.
*
* @author Mediacom87 <support@mediacom87.net>
* @copyright  Mediacom87
* @license    commercial license see tab in the module
*}

<div class="medChangelog">

    <ps-panel header="1.5.0 - {dateFormat date='2018-04-17' full=0}">

        <ul>
            <li>Reordering the code to fix a Chrome bug</li>
            <li>Fixed display on PrestaShop 1.5</li>
        </ul>

    </ps-panel>

    <ps-panel header="1.4.0 - {dateFormat date='2018-04-09' full=0}">

        <ul>
            <li>Add translations</li>
            <li>Add thirtybees compatibility</li>
        </ul>

    </ps-panel>

</div>
