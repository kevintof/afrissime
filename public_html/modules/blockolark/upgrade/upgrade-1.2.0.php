<?php
/**
 * Mediacom87
 *
 * NOTICE OF LICENSE
 *
 * Read in the module
 *
 * @author    Mediacom87 <support@mediacom87.net>
 * @copyright Mediacom87
 * @license   define in the module
 * @version 1.2.0
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_1_2_0($module)
{
    $conf = array();
    $conf['olarkid'] = Configuration::get('OLARK');

    if (!Configuration::updateValue($module->name, serialize($conf))) {

        return false;

    } else {

        Configuration::deleteByName('OLARK');
        return true;

    }

    return false;
}
