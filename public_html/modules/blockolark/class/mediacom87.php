<?php
/**
 * 2008-today Mediacom87
 *
 * NOTICE OF LICENSE
 *
 * Read in the module
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    Mediacom87 <support@mediacom87.net>
 * @copyright 2008-today Mediacom87
 * @license   define in the module
 * @version 1.4.0
 */

class BlockOlarkClass
{
    /**
     * __construct function.
     *
     * @access public
     * @param mixed $module
     * @return void
     */
    public function __construct($module)
    {
        $this->module = $module;
    }
    /**
     * isoCode function.
     *
     * @access public
     * @param bool $domain (default: false)
     * @return void
     */
    public function isoCode($domain = false)
    {
        $iso = $this->module->context->language->iso_code;

        if ($iso == 'fr') {
            return 'fr';
        } elseif ($domain) {
            return 'com';
        } else {
            return 'en';
        }
    }
}
