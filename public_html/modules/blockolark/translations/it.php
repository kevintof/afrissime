<?php //modified by module sd_translate version 1.10.0 on Mon, 09 Apr 2018 18:06:38 +0200

global $_MODULE;
$_MODULE = array();
$_MODULE['<{blockolark}prestashop>blockolark_eeec9168ebaaaec1ba8341e55c12b112'] = 'Olark LiveChat';
$_MODULE['<{blockolark}thirtybees>blockolark_eeec9168ebaaaec1ba8341e55c12b112'] = 'Olark LiveChat';
$_MODULE['<{blockolark}prestashop>blockolark_766570102220d89deff2f1d24789cb67'] = 'Integrare il tuo script Olark Livechat nel tuo negozio.';
$_MODULE['<{blockolark}thirtybees>blockolark_766570102220d89deff2f1d24789cb67'] = 'Integrare il tuo script Olark Livechat nel tuo negozio.';
$_MODULE['<{blockolark}prestashop>blockolark_92d947bcc4952fbade3e77eb0026713f'] = 'Se si disinstalla questo componente aggiuntivo si perderanno tutto della sua configurazione. Sei sicuro di che voler disinstallare?';
$_MODULE['<{blockolark}thirtybees>blockolark_92d947bcc4952fbade3e77eb0026713f'] = 'Se si disinstalla questo componente aggiuntivo si perderanno tutto della sua configurazione. Sei sicuro di che voler disinstallare?';
$_MODULE['<{blockolark}prestashop>blockolark_271f9bc9cd243d25cc7530ef104bb79d'] = 'Errore durante il salvataggio delle impostazioni';
$_MODULE['<{blockolark}thirtybees>blockolark_271f9bc9cd243d25cc7530ef104bb79d'] = 'Errore durante il salvataggio delle impostazioni';
$_MODULE['<{blockolark}prestashop>blockolark_c888438d14855d7d96a2724ee9c306bd'] = 'Impostazioni aggiornate';
$_MODULE['<{blockolark}thirtybees>blockolark_c888438d14855d7d96a2724ee9c306bd'] = 'Impostazioni aggiornate';
$_MODULE['<{blockolark}prestashop>admin_8524de963f07201e5c086830d370797f'] = 'Caricamento...';
$_MODULE['<{blockolark}thirtybees>admin_8524de963f07201e5c086830d370797f'] = 'Caricamento...';
$_MODULE['<{blockolark}prestashop>admin_254f642527b45bc260048e30704edb39'] = 'Configurazione';
$_MODULE['<{blockolark}thirtybees>admin_254f642527b45bc260048e30704edb39'] = 'Configurazione';
$_MODULE['<{blockolark}prestashop>admin_ead03db6961dfa2d083adb1f61bfcdc2'] = 'Prima di tutto, iscriviti alla:';
$_MODULE['<{blockolark}thirtybees>admin_ead03db6961dfa2d083adb1f61bfcdc2'] = 'Prima di tutto, iscriviti alla:';
$_MODULE['<{blockolark}prestashop>admin_c1d0b2d43e633432faa6de3b85be2f08'] = 'Il tuo ID di sito Olark';
$_MODULE['<{blockolark}thirtybees>admin_c1d0b2d43e633432faa6de3b85be2f08'] = 'Il tuo ID di sito Olark';
$_MODULE['<{blockolark}prestashop>admin_be7ce5d30765caaf68595aeebf5e58fe'] = 'Incolla il tuo ID di sito come (1234-123-12-1234)';
$_MODULE['<{blockolark}thirtybees>admin_be7ce5d30765caaf68595aeebf5e58fe'] = 'Incolla il tuo ID di sito come (1234-123-12-1234)';
$_MODULE['<{blockolark}prestashop>admin_65092f340c281f6d56637fe5ae70cfab'] = 'Si può prendere il tuo account:';
$_MODULE['<{blockolark}thirtybees>admin_65092f340c281f6d56637fe5ae70cfab'] = 'Si può prendere il tuo account:';
$_MODULE['<{blockolark}prestashop>admin_339505853d26f48928d0340041edf72c'] = 'Aiutaci';
$_MODULE['<{blockolark}thirtybees>admin_339505853d26f48928d0340041edf72c'] = 'Aiutaci';
$_MODULE['<{blockolark}prestashop>admin_1aa66488198c6865bfbab324d9710188'] = 'Questo modulo ha preso qualche tempo per sviluppare e testare.';
$_MODULE['<{blockolark}thirtybees>admin_1aa66488198c6865bfbab324d9710188'] = 'Questo modulo ha preso qualche tempo per sviluppare e testare.';
$_MODULE['<{blockolark}prestashop>admin_d8873f824b07a772c6437532a0fb66a7'] = 'Potete aiutarmi cliccando proprio qui sotto.';
$_MODULE['<{blockolark}thirtybees>admin_d8873f824b07a772c6437532a0fb66a7'] = 'Potete aiutarmi cliccando proprio qui sotto.';
$_MODULE['<{blockolark}prestashop>admin_7af18e5e53c1572c1da6841be9ca6738'] = 'Esso non costa nulla, ma mi permetterà di continuare a mantenere questo modulo funzionale e gratuito.';
$_MODULE['<{blockolark}thirtybees>admin_7af18e5e53c1572c1da6841be9ca6738'] = 'Esso non costa nulla, ma mi permetterà di continuare a mantenere questo modulo funzionale e gratuito.';
$_MODULE['<{blockolark}prestashop>admin_7846a3f23cb8177db154ff0126378885'] = 'È inoltre possibile donare direttamente con PayPal.';
$_MODULE['<{blockolark}thirtybees>admin_7846a3f23cb8177db154ff0126378885'] = 'È inoltre possibile donare direttamente con PayPal.';
$_MODULE['<{blockolark}prestashop>admin_8d4f7c3f711953a6c738839be572c0f6'] = 'Donare € 5';
$_MODULE['<{blockolark}thirtybees>admin_8d4f7c3f711953a6c738839be572c0f6'] = 'Donare € 5';
$_MODULE['<{blockolark}prestashop>admin_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{blockolark}thirtybees>admin_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{blockolark}prestashop>admin_d24e75d6b562ac9ec37dfcd41c907728'] = 'Più moduli';
$_MODULE['<{blockolark}thirtybees>admin_d24e75d6b562ac9ec37dfcd41c907728'] = 'Più moduli';
$_MODULE['<{blockolark}prestashop>admin_794df3791a8c800841516007427a2aa3'] = 'Licenza';
$_MODULE['<{blockolark}thirtybees>admin_794df3791a8c800841516007427a2aa3'] = 'Licenza';
$_MODULE['<{blockolark}prestashop>licence_0fb7bdfb09d0e74edc6c492c52d778cd'] = 'Riepilogo licenze';
$_MODULE['<{blockolark}thirtybees>licence_0fb7bdfb09d0e74edc6c492c52d778cd'] = 'Riepilogo licenze';
$_MODULE['<{blockolark}prestashop>licence_829f96defc7c51f96edf84b190ab864a'] = 'Termini e condizioni';
$_MODULE['<{blockolark}thirtybees>licence_829f96defc7c51f96edf84b190ab864a'] = 'Termini e condizioni';
$_MODULE['<{blockolark}prestashop>modules_40d4f92527f9c91762ec5c5a797a0a8a'] = 'Ti piace questo modulo?';
$_MODULE['<{blockolark}thirtybees>modules_40d4f92527f9c91762ec5c5a797a0a8a'] = 'Ti piace questo modulo?';
$_MODULE['<{blockolark}prestashop>modules_b82c31c2d2b3832eef4e0c33e4765801'] = 'Ottenere altri quelli direttamente su';
$_MODULE['<{blockolark}thirtybees>modules_b82c31c2d2b3832eef4e0c33e4765801'] = 'Ottenere altri quelli direttamente su';
$_MODULE['<{blockolark}prestashop>modules_a8dd22d2bc2ee8dcd1e3ca62914502c9'] = 'O on';
$_MODULE['<{blockolark}thirtybees>modules_a8dd22d2bc2ee8dcd1e3ca62914502c9'] = 'O on';
